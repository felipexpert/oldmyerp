<?Php		
		
	if(isset($_POST['btn_enviar'])){
			
		$vendaDecimal 		= $_POST['sel_venda_decimal'];
		$compraDecimal 		= $_POST['sel_compra_decimal'];
		$quantidadeDecimal 	= $_POST['sel_quantidade_decimal'];
        $barcodeTamanho     = $_POST['sel_barcode_tamanho'];
		
        fnc_sistema_update('venda_casas_decimais',  $vendaDecimal);
        fnc_sistema_update('compra_casas_decimais', $compraDecimal);
        fnc_sistema_update('quantidade_casas_decimais', $quantidadeDecimal);
		fnc_sistema_update('produto_barcode_tamanho', $barcodeTamanho);
	
	############################################################################################################

		if(!mysql_error()){
?>			<div class="alert" style="margin-top: 5px">
        		<p class="ok">Configura&ccedil;&otilde;es atualizadas!</p>
    		</div>
<?		}
		else{
?>			<div class="alert">
				<p class="erro">N&atilde;o foi poss&iacute;vel gravar os dados</p>
			</div>
<?			echo mysql_error();
		}
	}

	$vendaDecimal 		= fnc_sistema('venda_casas_decimais');
	$compraDecimal 		= fnc_sistema('compra_casas_decimais');
	$quantidadeDecimal 	= fnc_sistema('quantidade_casas_decimais');
    $barcodeTamanho     = fnc_sistema('produto_barcode_tamanho');  
	
?>		<div class="form">
			<h3>Exibir casas decimais</h3>
            <form id="frm_vendas" class="frm_detalhe" style="width:960px" action="" method="post">
                <ul>
                	<li style=" padding:8px; background:#D7D7D7"> 
                        <label for="sel_compra_decimal">Valor de compra</label>
                        <select style="width:120px;" id="sel_compra_decimal" name="sel_compra_decimal" class="sel_compra_decimal" >
                            <option <?=($compraDecimal == '2') ? 'selected="selected"' : '' ?> value="2">2 casas</option>
                            <option <?=($compraDecimal == '3') ? 'selected="selected"' : '' ?> value="3">3 casas</option>
                            <option <?=($compraDecimal == '4') ? 'selected="selected"' : '' ?> value="4">4 casas</option>
                            <option <?=($compraDecimal == '5') ? 'selected="selected"' : '' ?> value="5">5 casas</option>
                            <option <?=($compraDecimal == '6') ? 'selected="selected"' : '' ?> value="6">6 casas</option>
                		</select>
                    </li>
                	<li style=" padding:8px; background:#D7D7D7"> 
                        <label for="sel_venda_decimal">Valor de venda</label>
                        <select style="width:120px;" id="sel_venda_decimal" name="sel_venda_decimal" class="sel_venda_decimal" >
                            <option <?=($vendaDecimal == '2') ? 'selected="selected"' : '' ?> value="2">2 casas</option>
                            <option <?=($vendaDecimal == '3') ? 'selected="selected"' : '' ?> value="3">3 casas</option>
                            <option <?=($vendaDecimal == '4') ? 'selected="selected"' : '' ?> value="4">4 casas</option>
                            <option <?=($vendaDecimal == '5') ? 'selected="selected"' : '' ?> value="5">5 casas</option>
                            <option <?=($vendaDecimal == '6') ? 'selected="selected"' : '' ?> value="6">6 casas</option>
                		</select>
                    </li>
                    
                	<li style=" padding:8px; background:#D7D7D7"> 
                        <label for="sel_quantidade_decimal">Decimais em quantidade</label>
                        <select style="width:150px;" id="sel_quantidade_decimal" name="sel_quantidade_decimal" class="sel_quantidade_decimal" >
                            <option <?=($quantidadeDecimal == '2') ? 'selected="selected"' : '' ?> value="2">2 casas</option>
                            <option <?=($quantidadeDecimal == '3') ? 'selected="selected"' : '' ?> value="3">3 casas</option>
                            <option <?=($quantidadeDecimal == '4') ? 'selected="selected"' : '' ?> value="4">4 casas</option>
                            <option <?=($quantidadeDecimal == '5') ? 'selected="selected"' : '' ?> value="5">5 casas</option>
                            <option <?=($quantidadeDecimal == '6') ? 'selected="selected"' : '' ?> value="6">6 casas</option>
                		</select>
                    </li>
        		</ul>

                <h3 style="display:table; width:950px; padding-top:10px">Código de Barras</h3>

                <ul>
                    <li style=" padding:8px; background:#D7D7D7"> 
                        <label for="sel_barcode_tamanho">Tamanho</label>
                        <select style="width:155px;" id="sel_barcode_tamanho" name="sel_barcode_tamanho" class="sel_barcode_tamanho" >
                            <option <?=($barcodeTamanho == '11') ? 'selected="selected"' : '' ?> value="11">20x4 (11 caracteres)</option>
                            <option <?=($barcodeTamanho == '13') ? 'selected="selected"' : '' ?> value="13">22x3 (13 caracteres)</option>
                            <option <?=($barcodeTamanho == '18') ? 'selected="selected"' : '' ?> value="18">18 caracteres</option>
                        </select>
                    </li>
                </ul>

            	<input style="float:right; margin-right: 20px" type="submit" class="btn_enviar" name="btn_enviar" value="Gravar" >
            </form>
            
		</div>
      