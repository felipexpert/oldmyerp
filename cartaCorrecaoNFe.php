<?php
	require('fpdf/fpdf.php');
	require("inc/con_db.php");

	
	


    $pdf = new FPDF('P', 'mm', 'A4');
	
	
	$pdf->AliasNbPages();
	
	
	$pdf->SetMargins(6,4,6);
	
	
	$pdf->AddPage();
	$pdf->SetFont('Arial', 'B',12);

	$pdf-> Cell(198,10,"CC-e: Carta de Correção Eletrônica",1,'','C');
	$pdf->Ln(20);
	$pdf->Image("image/layout/logo_empresa.jpg",7,24,41,26);
	$pdf->SetX(50);
	$pdf->SetFont('Arial', 'B',12);
	$pdf->Cell(30,6,"Razão Social:",'LT','','');
	$pdf->SetFont('Arial', '',12);
	$pdf->Cell(124,6,"Razão Social:",'TR','','');
	$pdf->Ln(5);
	$pdf->SetX(50);
	$pdf->SetFont('Arial', 'B',12);
	$pdf->Cell(13,6,"CNPJ:",'L');
	$pdf->SetFont('Arial', '',12);
	$pdf->Cell(50,6,"<CNPJ>");
	$pdf->SetFont('Arial', 'B',12);
	$pdf->Cell(30,6,"Insc. Estadual");
	$pdf->SetFont('Arial', '',12);
	$pdf->Cell(61,6,"<Insc. Estadual>",'R');

	$pdf->Ln(5);
	$pdf->SetX(50);
	$pdf->SetFont('Arial', '',12);
	$pdf->Cell(109,6,"<endereço>",'L');
	$pdf->Cell(45,6,"<bairro>",'R');

	$pdf->Ln(5);
	$pdf->SetX(50);
	$pdf->Cell(75,6,"<cidade>",'L');
	$pdf->Cell(50,6,"<estado>");
	$pdf->Cell(29,6,"<CEP>",'R');
	$pdf->Ln(5);
	$pdf->SetX(50);
	$pdf->Cell(154,6,"<Telefone>",'LBR');

	$pdf->Ln(10);
	$pdf->SetFont('Arial', 'B',12);
	$pdf->Cell(28,3,"Destinatário");
	$pdf->Cell(170,3,"",'B');
	$pdf->Ln();
	$pdf->Cell(30,7,"Razão Social:",'L');
	$pdf->SetFont('Arial', '',12);
	$pdf->Cell(168,7,"<Razão Social>",'R');
	$pdf->Ln();
	$pdf->SetFont('Arial', 'B',12);
	$pdf->Cell(13,6,"CNPJ:",'L');
	$pdf->SetFont('Arial', '',12);
	$pdf->Cell(77,6,"<CNPJ>",'');
	$pdf->SetFont('Arial', 'B',12);
	$pdf->Cell(31,6,"Insc. Estadual:",'');
	$pdf->SetFont('Arial', '',12);
	$pdf->Cell(77,6,"<Insc estadual>",'R');
	$pdf->Ln(5);
	$pdf->SetFont('Arial', 'B',12);
	$pdf->Cell(22,6,"Endereço:",'BL');
	$pdf->SetFont('Arial', '',12);
	$pdf->Cell(176,6,"<endereço>",'BR');

	$pdf->Ln(10);
	$pdf->SetFont('Arial', 'B',12);
	$pdf->Cell(37,3,"Chave de Acesso");
	$pdf->Cell(161,3,"",'B');
	$pdf->Ln();
	$pdf->SetFont('Arial', '',12);
	$pdf->Cell(198,7,"<Chave de Acesso>",'LBR');
	
	$pdf->Ln(10);
	$pdf->SetFont('Arial', 'B',12);
	$pdf->Cell(35,3,"Número da NF-e");
	$pdf->Cell(24,3,"",'B');
	$pdf->setX(80);
	$pdf->Cell(13,3,"Série");
	$pdf->Cell(41,3,"",'B');
	$pdf->setX(150);
	$pdf->Cell(17,3,"Modelo");
	$pdf->Cell(37,3,"",'B');

	$pdf->Ln();
	$pdf->SetFont('Arial', '',12);
	$pdf->Cell(59,7,"<numero da nota>",'LBR','','C');
	$pdf->setX(80);
	$pdf->Cell(54,7,"<serie>",'LBR','','C');
	$pdf->setX(150);
	$pdf->Cell(54,7,"<modelo>",'LBR','','C');
	$pdf->Ln();


	$pdf->Ln(5);
	$pdf->SetFont('Arial', 'B',12);
	$pdf->Cell(30,3,"Data Emissão");
	$pdf->Cell(29,3,"",'B');
	$pdf->setX(80);
	$pdf->Cell(46,3,"Seuqencial do Evento");
	$pdf->Cell(8,3,"",'B');
	$pdf->setX(150);
	$pdf->Cell(15,3,"Status");
	$pdf->Cell(39,3,"",'B');

	$pdf->Ln();
	$pdf->SetFont('Arial', '',12);
	$pdf->Cell(59,7,"<data emissão>",'LBR','','C');
	$pdf->setX(80);
	$pdf->Cell(54,7,"<sequencial evento>",'LBR','','C');
	$pdf->setX(150);
	$pdf->Cell(54,7,"<status>",'LBR','','C');
	$pdf->Ln();

	$pdf->Ln(5);
	$pdf->SetFont('Arial', 'B',12);
	$pdf->Cell(56,3,"Hora/Data Carta de Correção");
	$pdf->Cell(30,3,"",'B');
	$pdf->setX(108);
	$pdf->Cell(46,3,"Número do Protocólo");
	$pdf->Cell(50,3,"",'B');
	$pdf->setX(150);
	

	$pdf->Ln();
	$pdf->SetFont('Arial', '',12);
	$pdf->Cell(86,7,"<data><hora>",'LBR');
	$pdf->setX(108);
	$pdf->Cell(96,7,"<numero do protocolo>",'LBR','','C');
	
	

	$pdf->Ln(13);
	$pdf->SetFont('Arial', 'B',12);
	$pdf->Cell(57,3,"Texto da Carta de Correção");
	$pdf->Cell(141,3,"",'B');
	$pdf->Ln();
	$pdf->SetFont('Arial', '',12);
	$pdf->MultiCell(198,7,"<texto carta correção>",'LBR');
	

	$pdf->Ln(13);
	$pdf->SetFont('Arial', 'B',12);
	$pdf->Cell(83,3,"Condiçoes de Uso da Carta de Correção");
	$pdf->Cell(115,3,"",'B');
	$pdf->Ln();
	$pdf->SetFont('Arial', '',11);
	$pdf->MultiCell(198,6,"A Carta de Correção é disciplinada pelo § 1º-A do art. 7º do Convênio S/N, de 15 de dezembro de 1970 e pode ser utilizada a regularização de erro ocorrido na emissão de documento fiscal, desde que o erro não esteja relacionado com:
	I- as variáveis que determinam o valor do imposto tais como: base de cálculo, alíquota, diferença de preço, quantidade,valor da operação ou da prestação; 
	II- a correção de dados cadastrais que implique mudança do remetente ou do destinatário; 
	III- a data de emissão ou de saída.",'LBR');
	


	



    
       



	$pdf->Output();
            

  ?>