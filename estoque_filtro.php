<?php
	$filtro = '';
	//recebendo a data do calendário de período
	if(isset($_POST['txt_calendario_data_inicial']) && isset($_POST['txt_calendario_data_final'])){
	    $_SESSION['txt_data1'] = (isset($_POST['txt_calendario_data_inicial'])) ? $_POST['txt_calendario_data_inicial'] : '';
	    $_SESSION['txt_data2'] = (isset($_POST['txt_calendario_data_final']))	? $_POST['txt_calendario_data_final'] 	: '';
	}
	
	$_SESSION['txt_data2'] 	= (isset($_SESSION['txt_data2']))	? $_SESSION['txt_data2'] : date('d/m/Y');
	

	//memorizar os filtros para exibição nos selects
	if(isset($_POST['btn_limpar'])){
		$_SESSION['txt_estoque_produto_cod']			= "";
		$_SESSION['txt_estoque_produto'] 				= "";
		$_SESSION['sel_estoque_fornecedor'] 			= "";
		$_SESSION['txt_estoque_quantidade_menor'] 		= "";
		$_SESSION['txt_estoque_quantidade_maior']	 	= "";
		$_SESSION['txt_estoque_quantidade'] 			= "";
		$_SESSION['sel_estoque_marca'] 					= "";
		$_SESSION['sel_estoque_categoria'] 				= "";
		$_SESSION['sel_estoque_produto_tipo'] 			= "";
		$_SESSION['sel_estoque_produto_compra']			= "";
		$_SESSION['txt_estoque_enderecamento_sigla']	= "";
		$_SESSION['hid_enderecamento_nivel_id']			= "";
		$_POST['chk_fornecedor'] 						=  false;
		$_SESSION['txt_data1'] 							= '';
		$_SESSION['txt_data2'] 							= date('d/m/Y');
	}
	else{
		$_SESSION['txt_estoque_produto_cod'] 			= (isset($_POST['txt_estoque_produto_cod'])			? $_POST['txt_estoque_produto_cod'] 		: $_SESSION['txt_estoque_produto_cod']);
		$_SESSION['txt_estoque_produto']	 			= (isset($_POST['txt_estoque_produto']) 			? $_POST['txt_estoque_produto']				: $_SESSION['txt_estoque_produto']);
		$_SESSION['sel_estoque_fornecedor']  			= (isset($_POST['sel_estoque_fornecedor']) 			? $_POST['sel_estoque_fornecedor'] 			: $_SESSION['sel_estoque_fornecedor']);
		$_SESSION['txt_estoque_quantidade_menor']  		= (isset($_POST['txt_estoque_quantidade_menor'])	? $_POST['txt_estoque_quantidade_menor']	: $_SESSION['txt_estoque_quantidade_menor']);
		$_SESSION['txt_estoque_quantidade_maior']  		= (isset($_POST['txt_estoque_quantidade_maior'])	? $_POST['txt_estoque_quantidade_maior']	: $_SESSION['txt_estoque_quantidade_maior']);
		$_SESSION['sel_estoque_marca'] 		 			= (isset($_POST['sel_estoque_marca']) 				? $_POST['sel_estoque_marca'] 				: $_SESSION['sel_estoque_marca']);
		$_SESSION['sel_estoque_categoria'] 				= (isset($_POST['sel_estoque_categoria']) 			? $_POST['sel_estoque_categoria'] 			: $_SESSION['sel_estoque_categoria']);
		$_SESSION['sel_estoque_produto_tipo'] 			= (isset($_POST['sel_estoque_produto_tipo'])		? $_POST['sel_estoque_produto_tipo']		: $_SESSION['sel_estoque_produto_tipo']);
		$_SESSION['sel_estoque_produto_compra']			= (isset($_POST['sel_estoque_produto_compra'])		? $_POST['sel_estoque_produto_compra']		: $_SESSION['sel_estoque_produto_compra']);
		$_SESSION['txt_estoque_enderecamento_sigla'] 	= (isset($_POST['txt_estoque_enderecamento_sigla'])	? $_POST['txt_estoque_enderecamento_sigla']	: $_SESSION['txt_estoque_enderecamento_sigla']);
		$_SESSION['txt_data1'] 							= (isset($_POST['txt_data1'])						? $_POST['txt_data1']						: $_SESSION['txt_data1']);
		$_SESSION['txt_data2'] 							= (isset($_POST['txt_data2'])						? $_POST['txt_data2']						: $_SESSION['txt_data2']);
		$_SESSION['hid_enderecamento_nivel_id'] 		= (isset($_POST['hid_enderecamento_nivel_id'])		? $_POST['hid_enderecamento_nivel_id']		: $_SESSION['hid_enderecamento_nivel_id']);
	}

?>
<form id="frm-filtro" action="index.php?p=estoque" method="post">
	<fieldset>
  	<legend>Buscar por:</legend>
    <ul>
		<li class="large_height">
<?			$codigo = ($_SESSION['txt_estoque_produto_cod'] ? $_SESSION['txt_estoque_produto_cod'] : "c&oacute;d.");
			($_SESSION['txt_estoque_produto_cod'] == "cód.") ? $_SESSION['txt_estoque_produto_cod'] = '' : '';
?>      	<input style="width: 70px" type="text" name="txt_estoque_produto_cod" id="txt_estoque_produto_cod" onfocus="limpar (this,'c&oacute;d.');" onblur="mostrar (this, 'c&oacute;d.');" value="<?=$codigo?>"/>
		</li>
		<li class="large_height">
        	<input type="checkbox" name="chk_produto" id="chk_produto" <?=($_POST['chk_produto'] == true ? print 'checked ="checked"' : '')?>/>
<?			$produto = ($_SESSION['txt_estoque_produto'] ? $_SESSION['txt_estoque_produto'] : "produto");
			($_SESSION['txt_estoque_produto'] == "produto") ? $_SESSION['txt_estoque_produto'] = '' : '';
?>     		<input style="width: 200px" type="text" name="txt_estoque_produto" id="txt_estoque_produto" onfocus="limpar (this,'produto');" onblur="mostrar (this, 'produto');" value="<?=$produto?>"/>
			<small>marque para qualquer parte do campo</small>
        </li>
		<li class="large_height">
            <select style="width: 180px" id="sel_estoque_fornecedor" name="sel_estoque_fornecedor" >
                <option value="">Fornecedor</option>
<?				$rsFornecedor = mysql_query("select * from tblfornecedor where fldDisabled = '0' ORDER BY fldNomeFantasia ASC");
				while($rowFornecedor= mysql_fetch_array($rsFornecedor)){
?>					<option <?=($_SESSION['sel_estoque_fornecedor'] == $rowFornecedor['fldId']) ? 'selected="selected"' : '' ?> value="<?=$rowFornecedor['fldId']?>"><?=$rowFornecedor['fldNomeFantasia']?></option>
<?				}
?>			</select>
		</li>
		<li class="large_height">
            <select style="width: 150px" id="sel_estoque_categoria" name="sel_estoque_categoria" >
                <option value="">Categoria</option>
<?				$rsCategoria = mysql_query("select * from tblcategoria where fldExcluido = '0' and fldDisabled = '0' ORDER BY fldNome ASC");
				while($rowCategoria= mysql_fetch_array($rsCategoria)){
?>					<option <?=($_SESSION['sel_estoque_categoria'] == $rowCategoria['fldId']) ? 'selected="selected"' : '' ?> value="<?=$rowCategoria['fldId']?>"><?=$rowCategoria['fldNome']?></option>
<?				}
?>			</select>
		</li>
		<li class="large_height">
            <select style="width: 150px" id="sel_estoque_marca" name="sel_estoque_marca" >
                <option value="">Marca</option>
<?				$rsMarca = mysql_query("select * from tblmarca where fldDisabled = '0' ORDER BY fldNome ASC");
				while($rowMarca= mysql_fetch_array($rsMarca)){
?>					<option <?=($_SESSION['sel_estoque_marca'] == $rowMarca['fldId']) ? 'selected="selected"' : '' ?> value="<?=$rowMarca['fldId']?>"><?=$rowMarca['fldNome']?></option>
<?				}
?>			</select>
		</li>
        <li class="large_height" style="margin-right:0">
            <select style="width: 100px" id="sel_estoque_produto_tipo" name="sel_estoque_produto_tipo" >
                <option value="">Tipo</option>
<?				$rsTipo = mysql_query("select * from tblproduto_tipo ORDER BY fldTipo ASC");
				while($rowTipo= mysql_fetch_array($rsTipo)){
?>					<option <?=($_SESSION['sel_estoque_produto_tipo'] == $rowTipo['fldId']) ? 'selected="selected"' : '' ?> value="<?=$rowTipo['fldId']?>"><?=$rowTipo['fldTipo']?></option>
<?				}
?>			</select>
		</li>
        <li>
            <select style="width:150px" id="sel_estoque_produto_compra" name="sel_estoque_produto_compra" >
                <option value="">Todos</option>
                <option <?=($_SESSION['sel_estoque_produto_compra'] == 'compra') ? 'selected="selected"' : '' ?> value="compra">necessidade de compra</option>
			</select>
		</li>
        <li>
<?			$quantidade_menor = ($_SESSION['txt_estoque_quantidade_menor'] ? $_SESSION['txt_estoque_quantidade_menor'] : "menor que");
			($_SESSION['txt_estoque_quantidade_menor'] == "menor que") ? $_SESSION['txt_estoque_quantidade_menor'] = '' : '';
?>      	<input style="width:65px" type="text" name="txt_estoque_quantidade_menor" id="txt_estoque_quantidade_menor" onfocus="limpar (this,'menor que');" onblur="mostrar (this, 'menor que');" value="<?=$quantidade_menor?>"/>
		</li>
        <li>
<?			$quantidade_maior = ($_SESSION['txt_estoque_quantidade_maior'] ? $_SESSION['txt_estoque_quantidade_maior'] : "maior que");
			($_SESSION['txt_estoque_quantidade_maior'] == "maior que") ? $_SESSION['txt_estoque_quantidade_maior'] = '' : '';
?>      	<input style="width:60px" type="text" name="txt_estoque_quantidade_maior" id="txt_estoque_quantidade_maior" onfocus="limpar (this,'maior que');" onblur="mostrar (this, 'maior que');" value="<?=$quantidade_maior?>"/>
		</li>
<?      if(fnc_sistema_tela_ativo('estoque_enderecamento') != '0'){ ?>
            <li>
                <input style="width:130px" type="text" name="txt_estoque_enderecamento_sigla" id="txt_estoque_enderecamento_sigla" value="<?=$_SESSION['txt_estoque_enderecamento_sigla']?>" placeholder="Endereçamento" readonly="readonly" />
                <a href="estoque_enderecamento_filtro,<?=$_SESSION['sel_estoque_id']?>" title="Localizar" class="modal" rel="360-380" style="float:right"><img style="margin:0 0 0 3px;" src="image/layout/search.gif" alt="localizar" /></a>
                <input type="hidden" name="hid_enderecamento_nivel_id" id="hid_enderecamento_nivel_id" value="<?=$_SESSION['hid_enderecamento_nivel_id']?>" />
                
            </li>
<? 		} ?>		
        <li>
<?			$data1 = ($_SESSION['txt_data1'] ? $_SESSION['txt_data1'] : "");
?>     		<input title="Data inicial" style="text-align:center;width: 70px" type="text" name="txt_data1" id="txt_data1" class="calendario-mask" value="<?=$data1?>"/>
      	</li>
		
		<li>
<?			$data2 = ($_SESSION['txt_data2'] ? $_SESSION['txt_data2'] : "");
?>     		<input title="Data final" style="text-align:center;width: 70px" type="text" name="txt_data2" id="txt_data2" class="calendario-mask" value="<?=$data2?>"/>
			<a href="calendario_periodo,<?=format_date_in($data1) . ',' . format_date_in($data2)?>,pedido" id="exibir-calendario" title="Exibir calend&aacute;rio" class="modal calendario-modal" rel="600-320"></a>
      	</li>
        
        <button style="margin-top:0" type="submit" name="btn_exibir" title="Exibir">Exibir</button>
		<button style="margin-top:0" type="submit" name="btn_limpar" title="Limpar Filtro">Limpar filtro</button>
    </ul>

  </fieldset>
</form>

<?
	
	if($_SESSION['txt_estoque_produto_cod'] != ""){
		
		$filtro .= " and tblproduto.fldCodigo like '%".ltrim($_SESSION['txt_estoque_produto_cod'],"0")."' ";
	}

	if($_SESSION['txt_estoque_produto'] != ""){
		$produto = addslashes($_SESSION['txt_estoque_produto']); // no caso de aspas, pra nao dar erro na consulta
		if($_POST['chk_produto'] == true){
			$filtro .= "and tblproduto.fldNome like '%".$produto."%' ";
		}else{
			$filtro .= " and tblproduto.fldNome like '".$_SESSION['txt_estoque_produto']."%' ";
		}
	}
	
	if($_SESSION['sel_estoque_fornecedor'] != ""){
		
		$filtro .= " and tblproduto.fldFornecedor_Id = ".$_SESSION['sel_estoque_fornecedor'];
	}

	if($_SESSION['sel_estoque_categoria'] != ""){
		
		$filtro .= " and tblproduto.fldCategoria_Id =".$_SESSION['sel_estoque_categoria'];
	}
	
	if($_SESSION['sel_estoque_marca'] != ""){
		
		$filtro .= " and tblproduto.fldMarca_Id = ".$_SESSION['sel_estoque_marca'];
	}

	if($_SESSION['sel_estoque_produto_tipo'] != ""){
		$filtro .= " and tblproduto.fldTipo_Id = ".$_SESSION['sel_estoque_produto_tipo'];
	}
	

	if($_SESSION['hid_enderecamento_nivel_id'] != ""){
		$ids = $_SESSION['hid_enderecamento_nivel_id'].getSubNiveis_Id($_SESSION['hid_enderecamento_nivel_id'], $_SESSION['sel_estoque_id']);
		$filtro .= " and tblproduto_estoque_enderecamento.fldEnderecamento_Id IN ($ids) AND tblproduto_estoque_enderecamento.fldEstoque_Id = ".$_SESSION['sel_estoque_id'];
	}
	
	
	if($_SESSION['sel_estoque_produto_compra'] != ""){
		$_SESSION['txt_estoque_quantidade'] = " HAVING (fldEstoqueQuantidade <= tblproduto.fldEstoque_Minimo)";
	}
	
	if($_SESSION['txt_estoque_quantidade_menor'] != "" || $_SESSION['txt_estoque_quantidade_maior'] != ''){
		if($_SESSION['txt_estoque_quantidade_maior'] == ''){
			$_SESSION['txt_estoque_quantidade'] = " HAVING (fldEstoqueQuantidade < ".$_SESSION['txt_estoque_quantidade_menor']." OR fldEstoqueQuantidade IS NULL  )";
		}
		elseif($_SESSION['txt_estoque_quantidade_menor'] == ''){
			$_SESSION['txt_estoque_quantidade'] = " HAVING (fldEstoqueQuantidade > ".$_SESSION['txt_estoque_quantidade_maior']."  )";
		}
		else{
			$_SESSION['txt_estoque_quantidade'] = " HAVING (fldEstoqueQuantidade < ".$_SESSION['txt_estoque_quantidade_menor']." AND fldEstoqueQuantidade > ".$_SESSION['txt_estoque_quantidade_maior']."  )";
		}
	}
	
	if(format_date_in($_SESSION['txt_data1']) != "" || format_date_in($_SESSION['txt_data2']) != ""){
		
		if($_SESSION['txt_data1'] != "" && $_SESSION['txt_data2'] != ""){
			$filtro .= " AND tblproduto_estoque_movimento.fldData BETWEEN '".format_date_in($_SESSION['txt_data1'])."' AND '".format_date_in($_SESSION['txt_data2'])."'";
		}elseif($_SESSION['txt_data1'] != "" && $_SESSION['txt_data2'] == ""){
			$filtro .= " AND tblproduto_estoque_movimento.fldData >= '".format_date_in($_SESSION['txt_data1'])."'";
		}elseif($_SESSION['txt_data2'] != "" && $_SESSION['txt_data1'] == ""){
			$filtro .= " AND tblproduto_estoque_movimento.fldData <= '".format_date_in($_SESSION['txt_data2'])."'";
		}
	}
	
	//transferir para a sessão
	if(isset($_POST['btn_exibir'])){
		$_SESSION['filtro_estoque'] = $filtro;
	}
	elseif(isset($_POST['btn_limpar'])){
		$_SESSION['filtro_estoque'] = "";
	}
	
?>
<script language="javascript">
	

	$("#sel_estoque_produto_compra").change(function(){
		if($(this).val() == 'compra'){
			$("#txt_estoque_quantidade_maior").attr('disabled', 'disabled');
			$("#txt_estoque_quantidade_menor").attr('disabled', 'disabled');
		}else{
			$("#txt_estoque_quantidade_maior").attr('disabled', '');
			$("#txt_estoque_quantidade_menor").attr('disabled', '');
		}
	})
	
	$("#sel_estoque_produto_compra").change();

	$('#txt_estoque_enderecamento_sigla').live('keyup', function(event){
		if (event.keyCode == '120'){
			anchor = $(this).next('a');
			winModal(anchor);
		}
	});
	
	
        
	
</script>