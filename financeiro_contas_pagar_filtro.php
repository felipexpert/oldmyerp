<?php
	
	//inicializando com o dia final e inicial do mês
	$_SESSION['txt_contas_pagar_data_inicial'] 	= (isset($_SESSION['txt_contas_pagar_data_inicial']))	? $_SESSION['txt_contas_pagar_data_inicial'] : '';
	$_SESSION['txt_contas_pagar_data_final'] 	= (isset($_SESSION['txt_contas_pagar_data_final']))		? $_SESSION['txt_contas_pagar_data_final'] 	: format_date_out(date('Y') . '-' . date('m') . '-' . date("d", mktime(0, 0, 0, date('m') + 1, 1, date('Y')) - 1));
	
	$_SESSION['txt_contas_pagar_data_inicial'] 	= (isset($_POST['txt_calendario_data_inicial'])) 	 	? $_POST['txt_calendario_data_inicial'] 	: $_SESSION['txt_contas_pagar_data_inicial'];
	$_SESSION['txt_contas_pagar_data_final'] 	= (isset($_POST['txt_calendario_data_final'])) 			? $_POST['txt_calendario_data_final'] 	: $_SESSION['txt_contas_pagar_data_final'];
	
	
	//memorizar os filtros para exibição nos campos
	if(isset($_POST['btn_limpar'])){
		
		$_SESSION['txt_contas_pagar_data_inicial'] 	= "";
		$_SESSION['txt_contas_pagar_data_final'] 		= format_date_out(date('Y') . '-' . date('m') . '-' . date("d", mktime(0, 0, 0, date('m') + 1, 1, date('Y')) - 1));
		$_SESSION['sel_financeiro_contas_pagar_status'] 						= "";
		$_SESSION['txt_contas_pagar_descricao'] 		= "";
		$_SESSION['sel_contas_pagar_tipo_pagamento'] 	= "";
		$_SESSION['chk_contas_pagar_descricao'] 		= false;
		$_SESSION['sel_conta_tipo'] 		 			= 'todos';
		$_SESSION['sel_contas_pagar_marcador']			= "";
	}
	else{
		$_SESSION['txt_contas_pagar_data_inicial']  	= (isset($_POST['txt_contas_pagar_data_inicial']) 	? $_POST['txt_contas_pagar_data_inicial'] 	: $_SESSION['txt_contas_pagar_data_inicial']);
		$_SESSION['txt_contas_pagar_data_final']  		= (isset($_POST['txt_contas_pagar_data_final']) 	? $_POST['txt_contas_pagar_data_final'] 	: $_SESSION['txt_contas_pagar_data_final']);
		$_SESSION['sel_financeiro_contas_pagar_status']							= (isset($_POST['sel_financeiro_contas_pagar_status']) 						? $_POST['sel_financeiro_contas_pagar_status'] 						: $_SESSION['sel_financeiro_contas_pagar_status']);
		$_SESSION['sel_contas_pagar_marcador']  	  	= (isset($_POST['sel_contas_pagar_marcador']) 		? $_POST['sel_contas_pagar_marcador'] 		: $_SESSION['sel_contas_pagar_marcador']);
		$_SESSION['txt_contas_pagar_descricao']  		= (isset($_POST['txt_contas_pagar_descricao']) 		? $_POST['txt_contas_pagar_descricao']		: $_SESSION['txt_contas_pagar_descricao']);
		$_SESSION['sel_contas_pagar_tipo_pagamento'] 	= (isset($_POST['sel_contas_pagar_tipo_pagamento']) ? $_POST['sel_contas_pagar_tipo_pagamento'] : $_SESSION['sel_contas_pagar_tipo_pagamento']);
		$_SESSION['sel_conta_tipo'] 					= (isset($_POST['sel_conta_tipo']) 					? $_POST['sel_conta_tipo'] 					: $_SESSION['sel_conta_tipo']);
	}
	
?>

<form id="frm-filtro" action="?p=financeiro_contas_pagar" method="post">
	<fieldset>
  	<legend>Buscar por:</legend>
    <ul>
		<li>
			<label for="txt_contas_pagar_data_inicial">Per&iacute;odo: </label>
            
<?			$dataInicialFiltro = $_SESSION['txt_contas_pagar_data_inicial'];
?>      	<input title="Data inicial" style="width: 80px; text-align: center;" type="text" class="calendario-mask" name="txt_contas_pagar_data_inicial" id="txt_contas_pagar_data_inicial" value="<?=$dataInicialFiltro?>" />
		</li>
		<li>
<?			$dataFinalFiltro = $_SESSION['txt_contas_pagar_data_final'];
?>      	<input title="Data final" style="width: 80px; text-align: center;" type="text" class="calendario-mask" name="txt_contas_pagar_data_final" id="txt_contas_pagar_data_final" value="<?=$dataFinalFiltro?>" />
			<a href="calendario_periodo,<?=format_date_in($_SESSION['txt_contas_pagar_data_inicial']) . ',' . format_date_in($_SESSION['txt_contas_pagar_data_final'])?>,financeiro_contas_pagar" id="exibir-calendario" title="Exibir calend&aacute;rio" class="modal calendario-modal" rel="600-320"></a>
		</li>
        <li>
			<select style="width: 140px;" id="sel_conta_tipo" name="sel_conta_tipo">
                <option value="todos" <? ($_SESSION['sel_conta_tipo'] == "todos") ? print "selected" : "" ?>>tipo de conta</option>
            	<option value="programadas" <? ($_SESSION['sel_conta_tipo'] == "programadas") ? print "selected" : "" ?>>contas programadas</option>
                <option value="fornecedor" 	<? ($_SESSION['sel_conta_tipo'] == "fornecedor")  ? print "selected" : "" ?>>pagamento de fornecedor</option>
        	</select>
		</li>
		<li>
			<select style="width: 140px;" id="sel_contas_pagar_marcador" name="sel_contas_pagar_marcador" >
                <option value="">Marcador</option>
<?				$rsMarcador = mysql_query("SELECT * FROM tblfinanceiro_conta_fluxo_marcador ORDER BY fldMarcador ASC");
				while($rowMarcador= mysql_fetch_array($rsMarcador)){
?>					<option <?=($_SESSION['sel_contas_pagar_marcador'] == $rowMarcador['fldId']) ? 'selected="selected"' : '' ?> value="<?=$rowMarcador['fldId']?>"><?=$rowMarcador['fldMarcador']?></option>
<?				}
?>			</select>
		</li>
        <li>
			<select style="width: 100px;" id="sel_financeiro_contas_pagar_status" name="sel_financeiro_contas_pagar_status">
            	<option value="em aberto"	<? ($_SESSION['sel_financeiro_contas_pagar_status'] == "em aberto") ? print "selected" : "" ?>>em aberto</option>
                <option value="pago" 		<? ($_SESSION['sel_financeiro_contas_pagar_status'] == "pago") 		? print "selected" : "" ?>>pago</option>
                <option value="todos" 		<? ($_SESSION['sel_financeiro_contas_pagar_status'] == "todos")		? print "selected" : "" ?>>todos</option>
        	</select>
		</li>
    	<li>
			<input type="checkbox" name="chk_contas_pagar_descricao" id="chk_contas_pagar_descricao" <?=($_POST['chk_contas_pagar_descricao'] == true ? print 'checked ="checked"' : '')?>/>
<?			$conta = ($_SESSION['txt_contas_pagar_descricao']) ? $_SESSION['txt_contas_pagar_descricao'] : 'descri&ccedil;&atilde;o';
			($_SESSION['txt_contas_pagar_descricao'] == "descrição") ? $_SESSION['txt_contas_pagar_descricao'] = '' : '';
?>     		<input style="width: 200px" type="text" name="txt_contas_pagar_descricao" id="txt_contas_pagar_descricao" onfocus="limpar(this,'descri&ccedil;&atilde;o');" onblur="mostrar(this, 'descri&ccedil;&atilde;o');" value="<?=$conta?>"/>
			<small>marque para qualquer parte do campo</small>
		</li>
		<li style="margin-left: 65px">
			<select style="width: 140px;" id="sel_contas_pagar_tipo_pagamento" name="sel_contas_pagar_tipo_pagamento" >
                <option value="">tipo de pagamento</option>
<?				$rsTipoPagamento = mysql_query("SELECT * FROM tblpagamento_tipo ORDER BY fldTipo ASC");
				while($rowTipoPagamento= mysql_fetch_array($rsTipoPagamento)){
?>					<option <?=($_SESSION['sel_contas_pagar_tipo_pagamento'] == $rowTipoPagamento['fldId']) ? 'selected="selected"' : '' ?> value="<?=$rowTipoPagamento['fldId']?>"><?=$rowTipoPagamento['fldTipo']?></option>
<?				}
?>			</select>
		</li>
		<li style="float:right">
        	<button type="submit" name="btn_exibir" title="Exibir">Exibir</button>
        </li>
		<li style="float:right">
    		<button type="submit" name="btn_limpar" title="Limpar filtro">Limpar filtro</button>
        </li>
    </ul>
    
    
  </fieldset>
</form>

<?
	$filtro = array();
	
	if(($_SESSION['txt_contas_pagar_descricao']) != ""){
		if($_POST['chk_contas_pagar_descricao'] == true) {
			$filtro['Programadas'] 	.= "AND tblfinanceiro_conta_pagar_programada.fldNome like '%".$_SESSION['txt_contas_pagar_descricao']."%'";
			$filtro['Compras'] 		.= "AND tblfornecedor.fldNomeFantasia like '%".$_SESSION['txt_contas_pagar_descricao']."%'";
		}
		else {
			$filtro['Programadas'] 	.= "AND tblfinanceiro_conta_pagar_programada.fldNome like '".$_SESSION['txt_contas_pagar_descricao']."%'";
			$filtro['Compras'] 		.= "AND tblfornecedor.fldNomeFantasia like '".$_SESSION['txt_contas_pagar_descricao']."%'";
		}
	}
	
	if(($_SESSION['sel_contas_pagar_tipo_pagamento']) != ""){
		
		$filtro['Programadas'] 	.= "AND tblfinanceiro_conta_pagar_programada.fldPagamento_Id = '".$_SESSION['sel_contas_pagar_tipo_pagamento']."'";
		$filtro['Compras']		.= "AND tblcompra_parcela.fldPagamento_Id = '".$_SESSION['sel_contas_pagar_tipo_pagamento']."'";
	}
	
	if(($_SESSION['sel_contas_pagar_marcador']) != ""){
		$filtro['Programadas'] 	.= "AND tblfinanceiro_conta_pagar_programada.fldMarcador = '".$_SESSION['sel_contas_pagar_marcador']."'";
		$filtro['Compras'] 	.= "AND tblcompra.fldMarcador = '".$_SESSION['sel_contas_pagar_marcador']."'";
	}
		
	//transferir para a sessão
	if(isset($_POST['btn_exibir'])){
		$_SESSION['filtro_contas_pagar_programadas'] = $filtro['Programadas'];
		$_SESSION['filtro_contas_pagar_compras'] 	 = $filtro['Compras'];
		$_SESSION['filtro_contas_pagar_status'] 	 = $_SESSION['sel_financeiro_contas_pagar_status'];
	}
	elseif(isset($_POST['btn_limpar'])){
		$_SESSION['filtro_contas_pagar_programadas'] = "";
		$_SESSION['filtro_contas_pagar_compras'] 	 = "";
		$_SESSION['filtro_contas_pagar_status']		 = "";
	}
	else{
		$_SESSION['filtro_contas_pagar_programadas'] = $filtro['Programadas'];
		$_SESSION['filtro_contas_pagar_compras'] 	 = $filtro['Compras'];
		$_SESSION['filtro_contas_pagar_status'] 	 = $_SESSION['sel_financeiro_contas_pagar_status'];
	}
?>
