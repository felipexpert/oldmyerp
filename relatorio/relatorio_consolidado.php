
<?	

	
	############################################## REQUIRES ######################################################
	require("inc/relatorio.php");
	require("inc/con_db.php");
	require("inc/fnc_general.php");
	
	$pdf = new PDF('P', 'mm', 'A4');
	//pode ser P (portrait) ou L (landscape)
	
	$pdf->AliasNbPages();
	//isso faz com que funcione o total de paginas
	
	$pdf->SetMargins(10,11);
	//defino as margens do documento...
	
	$pdf->AddPage();

	/*//comando para add a pagina
	$pdf->SetFont('Arial', '', 8);
	$pdf->Cell(50);
	$pdf->Cell(60, 5, "nome da empresa: {$dados_empresa['fldNome_Fantasia']} (".format_cnpj_out($dados_empresa['fldCPF_CNPJ']).")", 0, 0, 'R');
	$pdf->Cell(30, 5, "usuario: $usuario", 0, 0, 'R');
	$pdf->Cell(50, 5, "gerado em: ".date('d/m/Y h:i:s'), 0, 0, 'R');
	$pdf->Ln(6);*/
	
	$pdf->SetFont('Arial', '',8);
	$pdf->Cell(50);
	$pdf->Cell(60, 5, "nome da empresa: {$dados_empresa['fldNome_Fantasia']} (".format_cnpj_out($dados_empresa['fldCPF_CNPJ']).")", 0, 0, 'R');
	$pdf->Cell (30,5,"usuário: $usuario",0,0,'R');
	$pdf->Cell(50,5,"gerado em: ".date('d/m/Y h:i:s'), 0,0,'R');
	$pdf->Ln(20);
	
	
	######################### TITULO DO RELATORIO #####################################################
	$pdf->SetTextColor(0, 0, 0);
	$pdf->SetFillColor(255, 255, 255);
	$pdf->SetFont('Arial', 'B', 14);
	$pdf->Cell(190, 8, "Relatório Consolidado de Produtos Vendidos", 1, 0, 'C', true);
	$pdf->SetTextColor(0, 0, 0); //ja reseto a cor da fonte para nao ter problema
	$pdf->Ln(20);
	##################################################################################################
	
	$dataInicial=	$_GET["dataInicial"];
	$dataFinal=	$_GET["dataFinal"];
	
	$dataInicial_convertida = format_date_in($dataInicial);
	$dataFinal_convertida = format_date_in($dataFinal);
	
	
	############################################## PROCESSAMENTO DE DADOS ########################################
	
	################################## CATEGORIA ########################################################
	
	$sqlConsulta = mysql_query ("SELECT
				tblproduto.fldCategoria_Id,
				tblcategoria.fldNome as Categoria,
				SUM(tblpedido_item.fldValor) as Total,
				SUM((tblpedido_item.fldValor - tblpedido_item.fldValor_Compra - (tblpedido_item.fldValor * tblpedido_item.fldDesconto / 100 ) ) * tblpedido_item.fldQuantidade)-(tblpedido_item.fldValor * tblpedido.fldDesconto/100)-(tblpedido.fldDescontoReais)as Lucro
				FROM tblpedido
				INNER JOIN tblpedido_item
				ON tblpedido.fldId= tblpedido_item.fldPedido_Id
				LEFT JOIN tblproduto
				ON tblpedido_item.fldProduto_Id = tblproduto.fldId
				LEFT JOIN tblcategoria
				ON tblproduto.fldCategoria_Id = tblcategoria.fldId 				
				WHERE tblpedido.fldExcluido=0
				and tblpedido_item.fldExcluido=0
				and tblpedido.fldStatus !=1
				and tblpedido.fldPedido_Destino_Nfe_Id is null
				and fldPedidoData BETWEEN '$dataInicial_convertida' and '$dataFinal_convertida'
				GROUP BY tblproduto.fldCategoria_Id
				ORDER BY tblcategoria.fldNome");
	
	
		
		$pdf->SetY(50);
		$pdf->SetX(70);
		$pdf-> SetFont('Arial','b',11);
		$pdf->Cell(63,10,"Período: $dataInicial a $dataFinal","","1");
		$pdf->Ln(13);
		$pdf->SetX(10);
		$pdf->SetFont('Arial','b',10);		
		$pdf->Cell (96,7, "Categoria","");                                                                                                                  
		$pdf->Cell (47,7, "Total",",","","R");
		$pdf->Cell (47,7, "Lucro","","","R");
		$pdf->Ln(10);
		
		$resultado=array();
		
		$somatotal=0;
		$somalucro=0;
		
		while($rowResultado = mysql_fetch_assoc($sqlConsulta)){ // exibe conteúdo da consulta enquanto tiver conteúdo a ser mostrado.
	
			$categoriaId=	$rowResultado['fldCategoria_Id'];
			$categoria=	$rowResultado['Categoria'];
			$total	=	format_number_out($rowResultado['Total']);
			$lucro=		format_number_out($rowResultado['Lucro']);
			
			
			$pdf->SetFont('Arial','',10);
			$pdf->SetTextColor(0,0,0);
				if($categoria =='' && $categoria== null){
					$pdf->Cell(96,7,"Produto sem categoria","T","","");
					$pdf->Cell (47,7,"$total","T","","R");
					$pdf->Cell (47,7,"$lucro","T","","R");
					$pdf->Ln(7);
				}
					else{
						$pdf->Cell (96,7,$categoria,"T","");
						$pdf->Cell (47,7,"$total","T","","R");
						$pdf->Cell (47,7,"$lucro","T","","R");
						$pdf->Ln(7);
					}
	################################################ SUBCATEGORIA ####################################################		
		
		$sqlConsulta2 = mysql_query("SELECT
				tblproduto.fldSubCategoria_Id,
				tblsubcategoria.fldNome as SubCategoria,
				SUM(tblpedido_item.fldValor) as Total_Subcategoria,
				SUM((tblpedido_item.fldValor - tblpedido_item.fldValor_Compra - (tblpedido_item.fldValor * tblpedido_item.fldDesconto / 100 ) ) * tblpedido_item.fldQuantidade)-(tblpedido_item.fldValor * tblpedido.fldDesconto/100)-(tblpedido.fldDescontoReais)as Lucro_Subcategoria
				FROM tblpedido
				INNER JOIN tblpedido_item
				ON tblpedido.fldId= tblpedido_item.fldPedido_Id
				LEFT JOIN tblproduto
				ON tblpedido_item.fldProduto_Id = tblproduto.fldId
				LEFT JOIN tblsubcategoria
				ON tblproduto.fldSubCategoria_Id = tblsubcategoria.fldId 				
				WHERE tblpedido.fldExcluido=0
				and tblpedido_item.fldExcluido=0
				and tblpedido.fldStatus != 1
				and tblpedido.fldPedido_Destino_Nfe_Id is null
				and tblproduto.fldSubCategoria_Id !=  0
				and fldPedidoData BETWEEN  '$dataInicial_convertida' and '$dataFinal_convertida'
				AND tblsubcategoria.fldCategoria_Id = $categoriaId
				GROUP BY tblproduto.fldSubCategoria_Id
				ORDER BY tblsubcategoria.fldNome");
	
		
		
		
			
		$resultadosub=array();
		
		$somatotalsub=0;
		$somalucrosub=0;
		
		while($rowResultado2 = mysql_fetch_assoc($sqlConsulta2)){ // exibe conteúdo da consulta enquanto tiver conteúdo a ser mostrado.
	
		     
			$subcategoria=	$rowResultado2['SubCategoria'];
			$total_sub	=	format_number_out($rowResultado2['Total_Subcategoria']);
			$lucro_sub=		format_number_out($rowResultado2['Lucro_Subcategoria']);
		  
			$pdf->SetX(14);
			$pdf->SetFont('Arial', '',10);
			$pdf->SetTextColor(70,70,110);
			$pdf->Cell (96,7,$subcategoria);
			$pdf->Cell (43,7,"$total_sub","","","R");
			$pdf->Cell (47,7,"$lucro_sub","","","R");
			$pdf->Ln(7);
			
			/*como declarado antes do while $somatotal=0, somo linha do resultado a variavél $somatotal
			e guardo o resultado da soma na mesma $somatotal, e assim continua até que nao
			tenha mais conteúdo a ser somado*/
			$somatotalsub =$somatotalsub + $rowResultado2['Total_Subcategoria'];
			$somalucrosub =$somalucrosub + $rowResultado2['Lucro_Subcategoria'];// o memso procedimento da $somatotal.
		         
			$somatotalsub_formatada=	format_number_out($somatotalsub); //formato o resultado para aparecer separador de milhar.
			$somalucrosub_formatada= 	format_number_out($somalucrosub);
		   
		}
	################################################### FIM SUBCATEGORIA #################################################		
		  
			/*como declarado antes do while $somatotal=0, somo linha do resultado a variavél $somatotal
			e guardo o resultado da soma na mesma $somatotal, e assim continua até que nao
			tenha mais conteúdo a ser somado*/
			$somatotal =$somatotal + $rowResultado['Total'];
			$somalucro =$somalucro + $rowResultado['Lucro'];// o memso procedimento da $somatotal.
		         
			$somatotal_formatada=	format_number_out($somatotal); //formato o resultado para aparecer separador de milhar.
			$somalucro_formatada= 	format_number_out($somalucro);
		   
		}
		
	
		
		
		$pdf->Ln(10);
		$pdf->SetX(80);
		$pdf->SetFont('Arial','b',10);
		$pdf->SetTextColor(0,0,0);
		$pdf->Cell (58,7, "Total Geral:   R$ $somatotal_formatada",1,"","R");
		$pdf->Cell (58,7, "Total de Lucro:   R$ $somalucro_formatada",1,"","R");
		$pdf->Ln(10);
		
		

	#################################################### FIM DA CATEGORIA ########################################
	
	
	
	########################################FIM DO PROCESSAMENTO DOS DADOS########################################
	
	
	
	
	$pdf->Output();
?>

