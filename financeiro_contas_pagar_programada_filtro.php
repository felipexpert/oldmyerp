<?php

	//memorizar os filtros para exibição nos selects
	if(isset($_POST['btn_limpar'])){
		$_SESSION['txt_contas_programada_id'] 		 	   = "";
		$_SESSION['txt_contas_programada_nome'] 	 	   = "";
		$_SESSION['sel_contas_programada_marcador']  	   = "";
		$_SESSION['sel_contas_programada_tipo_pagamento']  = "";
		$_SESSION['sel_contas_programada_intervalo'] 	   = "";
		$_POST['chk_contas_programada_nome'] 		 	   = false;
	}
	else{
		$_SESSION['txt_contas_programada_id'] 		 	  = (isset($_POST['txt_contas_programada_id']) ? $_POST['txt_contas_programada_id'] : $_SESSION['txt_contas_programada_id']);
		$_SESSION['txt_contas_programada_nome'] 	 	  = (isset($_POST['txt_contas_programada_nome']) ? $_POST['txt_contas_programada_nome'] : $_SESSION['txt_contas_programada_nome']);
		$_SESSION['sel_contas_programada_marcador']  	  = (isset($_POST['sel_contas_programada_marcador']) ? $_POST['sel_contas_programada_marcador'] : $_SESSION['sel_contas_programada_marcador']);
		$_SESSION['sel_contas_programada_tipo_pagamento'] = (isset($_POST['sel_contas_programada_tipo_pagamento']) ? $_POST['sel_contas_programada_tipo_pagamento'] : $_SESSION['sel_contas_programada_tipo_pagamento']);
		$_SESSION['sel_contas_programada_intervalo'] 	  = (isset($_POST['sel_contas_programada_intervalo']) ? $_POST['sel_contas_programada_intervalo'] : $_SESSION['sel_contas_programada_intervalo']);
	}
	
?>

<form id="frm-filtro" action="?p=financeiro_contas_pagar&amp;modo=programada" method="post">
	<fieldset>
  	<legend>Buscar por:</legend>
    <ul>
    	<li>
<?			$codigo = ($_SESSION['txt_contas_programada_id']) ? $_SESSION['txt_contas_programada_id'] : 'C&oacute;digo';
			($_SESSION['txt_contas_programada_id'] == "Código") ? $_SESSION['txt_contas_programada_id'] = '' : '';
?>      	<input style="width: 124px" type="text" name="txt_contas_programada_id" id="txt_contas_programada_id" onfocus="limpar(this,'Código');" onblur="mostrar(this, 'Código');" value="<?=$codigo?>"/>
		</li>
    	<li>
			<input type="checkbox" name="chk_contas_programada_nome" id="chk_contas_programada_nome" <?=($_POST['chk_contas_programada_nome'] == true ? print 'checked ="checked"' : '')?>/>
<?			$conta = ($_SESSION['txt_contas_programada_nome']) ? $_SESSION['txt_contas_programada_nome'] : 'Nome da conta';
			($_SESSION['txt_contas_programada_nome'] == "Nome da conta") ? $_SESSION['txt_contas_programada_nome'] = '' : '';
?>     		<input style="width: 165px" type="text" name="txt_contas_programada_nome" id="txt_contas_programada_nome" onfocus="limpar(this,'Nome da conta');" onblur="mostrar(this, 'Nome da conta');" value="<?=$conta?>"/>
			<small>marque para qualquer parte do campo</small>
		</li>
		<li>
			<select id="sel_contas_programada_marcador" name="sel_contas_programada_marcador" >
                <option value="">Marcador</option>
<?				$rsMarcador = mysql_query("SELECT * FROM tblfinanceiro_conta_fluxo_marcador ORDER BY fldMarcador ASC");
				while($rowMarcador= mysql_fetch_array($rsMarcador)){
?>					<option <?=($_SESSION['sel_contas_programada_marcador'] == $rowMarcador['fldId']) ? 'selected="selected"' : '' ?> value="<?=$rowMarcador['fldId']?>"><?=$rowMarcador['fldMarcador']?></option>
<?				}
?>			</select>
		</li>
		<li>
			<select id="sel_contas_programada_tipo_pagamento" name="sel_contas_programada_tipo_pagamento" >
                <option value="">Tipo de pagamento</option>
<?				$rsPagamento = mysql_query("SELECT * FROM tblpagamento_tipo ORDER BY fldTipo ASC");
				while($rowPagamento = mysql_fetch_array($rsPagamento)){
?>					<option <?=($_SESSION['sel_contas_programada_tipo_pagamento'] == $rowPagamento['fldId']) ? 'selected="selected"' : '' ?> value="<?=$rowPagamento['fldId']?>"><?=$rowPagamento['fldTipo']?></option>
<?				}
?>			</select>
		</li>
		<li>
			<select id="sel_contas_programada_intervalo" name="sel_contas_programada_intervalo" >
                <option value="">Frequ&ecirc;ncia</option>
<?				$rsCalendario = mysql_query("SELECT * FROM tblsistema_calendario_intervalo ORDER BY fldNome ASC");
				while($rowCalendario= mysql_fetch_array($rsCalendario)){
?>					<option <?=($_SESSION['sel_contas_programada_intervalo'] == $rowCalendario['fldId']) ? 'selected="selected"' : '' ?> value="<?=$rowCalendario['fldId']?>"><?=$rowCalendario['fldNome']?></option>
<?				}
?>			</select>
		</li>
    </ul>
    
    <button type="submit" name="btn_exibir" title="Exibir">Exibir</button>
    <button type="submit" name="btn_limpar" title="Limpar filtro">Limpar filtro</button>
  </fieldset>
</form>

<?
	
	if(($_SESSION['txt_contas_programada_id']) != ""){
		
		$filtro .= "AND tblfinanceiro_conta_pagar_programada.fldId = '".$_SESSION['txt_contas_programada_id']."'";
	}

	if(($_SESSION['txt_contas_programada_nome']) != ""){
		if($_POST['chk_contas_programada_nome'] == true) {
			$filtro .= "AND tblfinanceiro_conta_pagar_programada.fldNome like '%".$_SESSION['txt_contas_programada_nome']."%'";
		}
		else {
			$filtro .= "AND tblfinanceiro_conta_pagar_programada.fldNome like '".$_SESSION['txt_contas_programada_nome']."%'";
		}
	}
	
	if(($_SESSION['sel_contas_programada_marcador']) != ""){
		
		$filtro .= "AND tblfinanceiro_conta_pagar_programada.fldMarcador = '".$_SESSION['sel_contas_programada_marcador']."'";
	}
	
	if(($_SESSION['sel_contas_programada_tipo_pagamento']) != ""){
		
		$filtro .= "AND tblfinanceiro_conta_pagar_programada.fldPagamento_Id = '".$_SESSION['sel_contas_programada_tipo_pagamento']."'";
	}
	
	if(($_SESSION['sel_contas_programada_intervalo']) != ""){
		
		$filtro .= "AND tblfinanceiro_conta_pagar_programada.fldIntervalo_Tipo = '".$_SESSION['sel_contas_programada_intervalo']."'";
	}
	
	//transferir para a sessão
	if(isset($_POST['btn_exibir'])){
		$_SESSION['filtro_contas_programada'] = $filtro;
	}
	elseif(isset($_POST['btn_limpar'])){
		$_SESSION['filtro_contas_programada'] = "";
	}
	else{
		$_SESSION['filtro_contas_programada'] = $filtro;
	}
?>
