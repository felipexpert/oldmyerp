<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
      	<title>myERP - Relat&oacute;rio de Estoque</title>
        <link rel="stylesheet" type="text/css" href="style/style_relatorio.css" />
        <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="style/style_filtro.css" />
        
	</head>
	<body>
   		<div id="no-print">
            <ul style="width:800px; display:inline">
                <li><a class="print" href="#" onClick="window.print()">imprimir</a></li>
            </ul>
        </div>
<?
		ob_start();
		session_start();
		
		require("inc/con_db.php");
		require("inc/fnc_general.php");
		
		$rowDados		= mysql_fetch_array(mysql_query("SELECT * FROM tblempresa_info"));
		$CPF_CNPJDados 	= formatCPFCNPJTipo_out($rowDados['fldCPF_CNPJ'], $rowDados['fldTipo']);
		$rowUsuario		= mysql_fetch_array(mysql_query("SELECT * FROM tblusuario WHERE fldId=".$_SESSION['usuario_id']));
		
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			$agrupar 		= $_POST['sel_relatorio_agrupar'];
			$tipo	 		= $_POST['sel_relatorio_tipo'];
			$data_inicial	= format_date_in($_POST['txt_periodo_inicio']);
			$data_final 	= format_date_in($_POST['txt_periodo_final']);
			
			if($tipo == 'lucro'){
				require('estoque_relatorio_lucro.php');
			}else{
				require('estoque_relatorio_simples.php');
			}

		}
?>		
	</body>
</html>