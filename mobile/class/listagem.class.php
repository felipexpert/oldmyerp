<?php
	
    class listagem
    {
	
		public 	$table_class 		= null;
		public 	$rows_per_page 		= null;
		public 	$extra_col			= null;
		public 	$dados 				= null;
		
		//seta a classe padr�o quando constroi o objeto
		function __construct(){
			$this->table_class 		= "table table-hover table-striped table-bordered";
		}
		
		//funcao que cria o thead da table
		function create_head(){
			$html = "<thead>";
			foreach($this->dados['thead'] as $head){
				$html .= "<th>".$head."</th>";
			}
			$html .= "</thead>";			
			return $html;
		}
		
		//funcao que formata os dados brutos para a listagem
		/*function format_dados(){
			for($i = 0; $i < count($this->body); $i++){
				$keys = array_keys($this->body[$i]);
				foreach($keys as $key){
					$tipo = $this->head[$key]['tipo'];
					switch($tipo){
						case 'moeda':
							$this->body[$i][$key] = "R$ <span style='float:right'>".format_number_out($this->body[$i][$key])."</span>";
							break;
						case 'data':
							$this->body[$i][$key] = "<span style='text-align:center; display:block'>".format_data_out($this->body[$i][$key])."</span>";
							break;
						case 'cpf_cnpj':
							$this->body[$i][$key] = format_cpf_out($this->body[$i][$key]);
							break;
						default:
							$this->body[$i][$key] = $this->body[$i][$key];
					}
				}
			}
		}*/
		
		//funcao que cria o body
		function create_body(){
				//formata os dados
				#$this->format_dados();
				
				$html = "<tbody>";
				foreach($this->dados['tbody'] as $row){

					$html .= "<tr>";

						foreach($row as $valor){
							$html .= "<td>$valor</td>";							
						}

						//adiciona as cols extras
						if($this->extra_col){
							foreach($this->extra_col as $extra_col){
								$tag = "/{: ?.*(.*)}/";
								preg_match($tag, $extra_col, $resultado);
								
								//placeholder
								if($resultado){
									$campo 	= str_replace("{:", 	"", $resultado[0]);
									$campo 	= str_replace("}", 	"", $campo);
									$extra_col = str_replace($resultado[0], $row[$campo], $extra_col);
								}
								
								$html .= "<td style='width:1px'>$extra_col</td>";
							}
						}

					$html .= "</tr>";
				}
				$html .= "</tbody>";

				return $html;
		}
		
		function createTable(){
			$html = "<div class='table-responsive'><table class='".$this->table_class."'>";
			$html .= $this->create_head();
			$html .= $this->create_body();
			$html .= "</table></div>";
			echo $html;
		}
		
		function createPagination($rowCount){
			$url 	= $_SERVER['PHP_SELF'];
			$page 	= isset($_GET['page']) ? $_GET['page'] : 1;
			
			$total 	= ceil($rowCount / $this->rows_per_page);
			
			$previous 	= ($page - 1) > 1 ? ($page - 1) : 1;
			$next 		= ($page + 1) < $total ? ($page + 1) : $total;
			
			$html = '<ul class="pagination pagination-sm" style="margin:0; float:right">';
				
				$html .= "<li><a href='$url?p=listagem&page=$previous'>&laquo;</a></li>";
				
				for($n=1;$n<=$total;$n++){
					if($n==$page){
						$html .= "<li class='active'><a href='$url?p=listagem&page=$n'>$n</a></li>";
					}
					else{
						$html .= "<li><a href='$url?p=listagem&page=$n'>$n</a></li>";
					}
				}
				
				$html .= "<li><a href='$url?p=listagem&page=$next'>&raquo;</a></li>";
				
			$html .= '</ul>';
			
			echo $html;
		}
		
    }
    
?>