<?php
	
    class pedido extends dados
    {

		var $table 		= "tblpedido";

		#carrega os produtos do pedido
		function load_produtos(){
			parent::load_all("*", "pedido_id = {$this->id}", "view_pedido_produto");
			return $this->dados;
		}
		
		//intervem a funcao load original
		function load($fields = null, $where = null, $table = null){
			parent::load('*', "id = '{$this->id}'");
		}
		
		//intervem a funcao load original
		function load_pedido_produto($fields = null, $where = null, $table = null){
			parent::load('*', "pedido_produto_id = '{$this->pedido_produto_id}'", "view_pedido_produto");
		}
		
		#CARREGA UMA COMANDA EM ESPECIFICO
		function load_comanda($comanda_id){
			//se existir,
			//apenas pega o id do pedido
			//senao
			//cria um pedido novo
			$where = "comanda = '$comanda_id' and finalizada = '0'";
			$comanda = parent::load('*', $where);
			if($comanda){
				return $comanda['id'];
			}else{
				$this->comanda 		= $comanda_id;
				$this->finalizada 	= 0;
				$this->garcom_id 	= 1;
				return parent::store();
			}
		}

		#CARREGA TODAS AS COMANDAS
		function load_comandas(){
			$total_comandas = parent::load('valor', 'parametro = "total_comandas"', 'tblparametros');
			$total_comandas = $total_comandas['valor'];

			$comandas 				= array();
			$comandas_utilizadas 	= array();
			$comandas_minhas 		= array();
			
			parent::load_all("*", array("finalizada = 0 and garcom_id != 1"));
			foreach($this->dados as $dados){
				array_push($comandas_utilizadas, $dados['comanda']);
			}
			
			parent::load_all("*", array("finalizada = 0 and garcom_id = 1"));
			foreach($this->dados as $dados){
				array_push($comandas_minhas, $dados['comanda']);
			}

			//"varre" comanda por comanda
			for($i = 1; $i <= $total_comandas; $i++){
				if(in_array($i, $comandas_utilizadas)){
					$status = "utilizadas";
				}
				
				else if(in_array($i, $comandas_minhas)){
					$status = "minhas";
				}

				else {
					$status = "disponiveis";
				}

				$comanda = array("numero" => $i, "status" => $status);
				array_push($comandas, $comanda);
			}

			return $comandas;
		}
		
		#SALVA OS PRODUTOS
		function store_produto(){
			parent::store('tblpedido_produto');
		}
		
		#APAGA OS PRODUTOS
		function trash_produto(){
			parent::trash($this->id, "tblpedido_produto");
		}
		
    }
    
?>