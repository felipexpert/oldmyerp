<?php
	//retrocompatibilidade - estarei limpando as depend�ncias do redbean em dados.class
    require_once('redbean/rb.php');
    R::setup('mysql:host=localhost; dbname=myerp','root','');
    R::freeze(true);
    R::setStrictTyping(false);
	//
    require_once('pdo.class.php');
	
    abstract class dados
    {
		private $db; //definido no construct usando pdo
		public 	$PK;
		var 	$fields 		= array();
		var 	$dados 			= null;
		var		$header 		= array();
		private $isLoaded 		= false;
		public 	$query 			= null;
		public 	$rowCount 		= -1;
		public 	$rows_per_page 		= 10;

        function __construct(){
	    //$fields = R::getAll("SHOW FULL COLUMNS FROM {$this->table}");
	    //database est� em pdo.class
	    //$db = database::getInstance();
	    
	    //Instanciar uma conex?o
	    $this->db 	= new DB();
	    
	    //preparar par�metros para consulta
	    $sql = "SHOW FULL COLUMNS FROM {$this->table}";
	    $consulta = $this->db->query($sql);
	    $fields = $consulta->fetchAll(pdo::FETCH_ASSOC);
	    
	    
	    
	    foreach($fields as $field){
		    $this->fields[$field['Field']]['type'] 		= $field["Type"];
		    $this->fields[$field['Field']]['value'] 	= null;
		    $this->fields[$field['Field']]['label']		= $field["Comment"];
		    $this->fields[$field['Field']]['field'] 	= $field["Field"];
			
			// definir campo chave prim�ria como id da classe
			if($field['Key'] == 'PRI'){
				$this->PK = $field["Field"];
			}
	    }
	}
	
	public function __set($name, $value)
	{
		$this->fields[$name]['value'] = $value;
		if(!isset($this->fields[$name]['field']))
			$this->fields[$name]['field'] = $name;
	}

	public function __get($name)
	{
		if(array_key_exists($name, $this->fields)){
			return $this->fields[$name]['value'];
		}else{				
			return null;
		}
	}
	
	function load($where = 1, $fields = "*", $table = null){
		if($table == null){$table = $this->table;}
		
		if(is_array($fields)){$fields = implode(',',$fields);}
		
		/*
		$consulta = R::$f->begin()
					->select($fields)->from($table)->where($where)->get('row');
		*/	
		
		$sql = "SELECT * FROM $table WHERE $where";
		$consulta = $this->db->query($sql)->fetch(PDO::FETCH_ASSOC);
		
		//print_r($consulta);
		//die();
		
		if(is_array($consulta)){
			$keys = array_keys($consulta);
			foreach($keys as $key){
				$this->fields[$key]['value'] = $consulta[$key];
			}
			
			$this->isLoaded = true;
		}
		
		else{ $this->isLoaded = false; }
		
		return $consulta;
	}
	
	function isLoaded(){
		return $this->isLoaded;
	}

	function load_all($fields = "*", $filtro = null, $table = null){
		if($table == null){$table = $this->table;}
	
		$sql_extra 		= "";
		$page = isset($_GET['page']) ? $_GET['page'] - 1 : 0;
		$offset = $page * 10;

		if($_SERVER['REQUEST_METHOD'] == "POST"){ 
			if(isset($_POST['filtro'])){ 
				$filtros 		= $_POST['filtro'];
				$filtros_key 	= array_keys($filtros);
				
				for($i = 0; $i < count($filtros); $i++){
					if($filtros[$filtros_key[$i]]){
						$query 		= str_replace("{:value}", $filtros[$filtros_key[$i]], $filtros_key[$i]);
						$sql_extra .= " and $query";
					}
				}
			}else{
				$filtros = null;
			}
		}else{
			$filtros = null;
		}
		
		//filtro que � passado por parametro
		if($filtro){
			if(is_array($filtro)){
				foreach($filtro as $query){
					$sql_extra .= " and $query";
				}
			} else {
				$sql_extra = " and $filtro";
			}
		}
	
		//agora os campos vem em array, aqui converte em string (campo1,campo2,campo3)
		if(is_array($fields)){ $fields = implode(',',$fields); }

		//se nao estiver filtrando
		if(!$filtros and !$filtro){
			/*
			$this->dados = R::$f->begin()
				->select($fields)->from($table)->get();
			*/
			$sql = "SELECT $fields FROM $table";
			$this->dados = $this->db->query($sql)->fetchAll(PDO::FETCH_ASSOC);
			
		}

		//se houver filtros
		else{
			//executa a query com os filtros
			$this->dados = R::$f->begin()
				->select($fields)->from($table)->where('1=1')->addSQL($sql_extra)->get();
		}
		
		
		
		//se nao estiver filtrando
		if(!$filtros and !$filtro){
			//this code replaces the buildQuery, at least for while
			$this->dados = R::$f->begin()
				->select($fields)->from($table)
				->limit($this->rows_per_page)
				->offset($offset)->get();
		}

		//se houver filtros
		else{
			//executa a query com os filtros
			$this->dados = R::$f->begin()
				->select($fields)->from($table)->where('1=1')->addSQL($sql_extra)
				->limit($this->rows_per_page)
				->offset($offset)->get();
		}
		
		//ja cria o header usado para as listagens!
		if(!is_array($fields)){ $fields = explode(',',$fields); }
		foreach($fields as $field){
			if(array_key_exists($field, $this->fields)){
				$field = $this->fields[$field]['label'];
			}
			array_push($this->header, $field);
		}
		
		
		$this->rowCount = count($this->dados);

	}
    
	
	// retorna o id do registro criado ou atualizado
	// force_insert � usado para tabelas que n�o possuem autoincrement
	// como a tblproduto_fiscal que � uma extens�o da tblproduto
	function store($force_insert = false){
	    //if(!$table){$table = $this->table;} // antes tinha um $table como par�metro, agora pega direto da classe
	
	    //preparar string sql com os campos recebidos
	    foreach($this->fields as $field){
		if(isset($field['value'])){
			//ignorar id
			//if($field['field'] != $this->PK){
			$sql_set[] = "{$field['field']} = '{$field['value']}'";
			//}
			
		}
	    }
	    
	    
	    $sql_set = implode(',', $sql_set);
	    //inserir se n�o houver valor de PK ou se estiver com flag para for�ar insert
	    if(!$this->{$this->PK} or $force_insert){
		//echo '<p>insert</p>';
		$sql = "INSERT INTO {$this->table} SET $sql_set";
	    }
	    else{
		//echo '<p>update</p>';
		$sql = "UPDATE {$this->table} SET $sql_set where {$this->PK} = {$this->{$this->PK}}";
	    }
	    //echo "\n  $sql \n";
		
		
		
	    $this->db->exec($sql);
		
		if($this->{$this->PK}){
			return $this->{$this->PK};
		}
		else{
			return $this->db->lastInsertId();
		}
	    
		
		
	}
	
	function trash($id, $table = null){
		if(!$table){$table = $this->table;}
		$object 	= R::load($table, $id);
		R::trash($object);
	}
	
	function prepareList(){
		//na verdade aqui s� reune todos os dados j� processados em uma array.
		$dados = array();
		//cabe�alho
		$dados['thead'] = $this->header;
		//dados brutos
		$dados['tbody'] = $this->dados;
		return $dados;
	}

    }
?>