<?php
    /* Listar todos os clientes em JSON */
    
    require_once 'class/dados.class.php';
    require_once 'class/cliente.class.php';
     
    // array for JSON response
    $response = array();
     
    $cliente = new cliente();
    
    $cliente->rows_per_page = 10000;
    $cliente->load_all();
    
    if($cliente->rowCount){
        // success
        $response['dados'] = $cliente->dados;
        $response["success"] = 1;
    }
    else{
        // sem registros
        $response["success"] = 0;
        $response["message"] = "No products found";
    }

    echo json_encode($response);

?>