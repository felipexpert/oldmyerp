<?php
    /* Inserir ou atualizar clientes */
    
    require_once 'class/dados.class.php';
    require_once 'class/cliente.class.php';
    require_once '../inc/fnc_general.php';
    //var_dump($_POST);
    
    function fncUTF8toLatin1($valor){
        $valor = mb_convert_encoding($valor,"Latin1","UTF-8");
        //echo $valor . "\n";
        return addslashes($valor);;
        
    }
    
    $json = utf8_encode($_POST['json']);
    //$json = mb_convert_encoding($json,"ISO-8859-1","UTF-8");
    
    $dados = json_decode($json, true);
    
    $response['retornoNovo'] = 0;
    $response['dados'] = array();
    //echo "<hr>";
    //var_dump($dados);
    
    //echo "<hr>";
    
    //aqui parece redundante o nome do array porque n�o consegui melhorar no lado do android
    foreach($dados['dados'] as $row){
            $novo = false;
            $cliente = new cliente();
            
            //se receber server_id, carrega para atualizar
            $id = fncUTF8toLatin1($row['fldIdServidor']);
            $modificacao_data_hora = fncUTF8toLatin1($row['fldModificacaoDataHora']);
            if(!$id){
                $novo = true;
            }
            else{
                if($cliente->load("fldId = $id")){
                    if($modificacao_data_hora > $cliente->fldModificacaoDataHora){
                        $atualizar = true;
                    }
                    else{
                        $atualizar = false;
                    }
                }
            }
            //
            //echo $cliente->fldNomeFantasia;
            
            if($atualizar or $novo){
                if($novo){
                    $cliente->fldCadastroData = fncUTF8toLatin1($row['fldCadastroData']);
                    //Cliente ativo
                    $cliente->fldStatus_Id = 4;
                }
                
                // Tipo 3 - identifica importa��o de Mobile
                $cliente->fldOrigem_Id = 3;
                
                
                
                
                $cliente->fldModificacaoDataHora = fncUTF8toLatin1($row['fldModificacaoDataHora']);
                
                $cliente->fldNome = fncUTF8toLatin1($row['fldNome']);
                $cliente->fldNomeFantasia = fncUTF8toLatin1($row['fldNomeFantasia']);
                
                //tipo baseado no CPF ou CNPJ
                
                $cliente->fldCPF_CNPJ = fncUTF8toLatin1($row['fldCPF_CNPJ']);
                $cliente->fldRG_IE = fncUTF8toLatin1($row['fldRG_IE']);
                
                $cliente->fldEmail = fncUTF8toLatin1($row['fldEmail']);
                
                $cliente->fldEndereco = fncUTF8toLatin1($row['fldEndereco']);
                $cliente->fldNumero = fncUTF8toLatin1($row['fldNumero']);
                $cliente->fldComplemento = fncUTF8toLatin1($row['fldComplemento']);
                $cliente->fldBairro = fncUTF8toLatin1($row['fldBairro']);
                
                // cep
                $cep = fncUTF8toLatin1($row['fldCEP']);
                $cep = fnc_number_only(trim($cep));
                if(strlen($cep)){
                    $cep = substr($cep, -8, 2) . '.' . substr($cep, -6, 3) . '-' . substr($cep, -3);
                }
                $cliente->fldCEP = $cep;
                //
                
                //telefone1
                $telefone1 = fncUTF8toLatin1($row['fldTelefone1']);
                $telefone1 = fnc_number_only(trim($telefone1));
                if(strlen($telefone1) == 11) {
                    $telefone1 = '(' . substr($telefone1, 0, 2) . ') ' . substr($telefone1, -9, 5) . '-' . substr($telefone1, -4);
                }
                elseif(strlen($telefone1) == 10) {
                    $telefone1 = '(' . substr($telefone1, 0, 2) . ') ' . substr($telefone1, -8, 4) . '-' . substr($telefone1, -4);
                }
                else{
                    $telefone1 = fncUTF8toLatin1($row['fldTelefone1']);
                }
                $cliente->fldTelefone1 = $telefone1;
                
                
                $cliente->fldTelefone2 = fncUTF8toLatin1($row['fldTelefone2']);
                $cliente->fldFax = fncUTF8toLatin1($row['fldFax']);
                
                
                
                //$cliente->fldNomeFantasia = fncUTF8toLatin1($json);
                
                $idServidor = $cliente->store();
                
                
                if($novo){
                    //flag para o mobo saber que tem que receber ids
                    $response['retornoNovo'] = 1;
                    //retornar o id para armazenar no mobile
                    $novovalor = array();
                    $novovalor['fldId'] = $row['fldId'];
                    $novovalor['fldIdServidor'] = $idServidor;
                    array_push($response['dados'],$novovalor);
                    
                    //gravar o c�digo igual ao id para os novos
                    $cliente->load("fldId = $idServidor");
                    $cliente->fldCodigo = $idServidor;
                    $cliente->store();
                }
                
                
            }
            
    }
    
    if(1){
        // success
        $response["success"] = 1;
        $response['message'] = "Cliente armazenado";
    }
    else{
        // sem registros
        $response["success"] = 0;
        $response["message"] = "Erro";
    }
    
    echo json_encode($response);
    
?>