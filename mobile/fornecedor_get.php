<?php
    /* Listar todos os produtos em JSON */
    
    require_once 'class/dados.class.php';
    require_once 'class/fornecedor.class.php';
     
    // array for JSON response
    $response = array();
     
    $fornecedor = new fornecedor();
    
    $fornecedor->rows_per_page = 10000;
    $fornecedor->load_all();
    
    if($fornecedor->rowCount){
        // success
        $response['dados'] = $fornecedor->dados;
        $response["success"] = 1;
    }
    else{
        // sem registros
        $response["success"] = 0;
        $response["message"] = "Sem registros";
    }

    echo json_encode($response);

?>