<?php
    /* Listar todos os produtos em JSON */
    
    require_once 'class/dados.class.php';
    require_once 'class/produto_tabela.class.php';
     
    // array for JSON response
    $response = array();
     
    $produto = new produto_tabela();
    
    
    $produto->rows_per_page = 10000;
    $produto->load_all('*', 'fldExcluido = 0'); //ignorar exclu�dos
    
    if($produto->rowCount){
        // success
        $response['dados'] = $produto->dados;
        $response["success"] = 1;
    }
    else{
        // sem registros
        $response["success"] = 0;
        $response["message"] = "Sem registros";
    }

    echo json_encode($response);

?>