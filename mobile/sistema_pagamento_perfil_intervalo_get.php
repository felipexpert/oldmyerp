<?php
    /* Listar em JSON */
    
    require_once 'class/dados.class.php';
    require_once 'class/sistema_pagamento_perfil_intervalo.class.php';
     
    // array for JSON response
    $response = array();
     
    $sistema_pagamento_perfil_intervalo = new sistema_pagamento_perfil_intervalo();
    
    $sistema_pagamento_perfil_intervalo->rows_per_page = 10000;
    $sistema_pagamento_perfil_intervalo->load_all();
    
    if($sistema_pagamento_perfil_intervalo->rowCount){
        // success
        $response['dados'] = $sistema_pagamento_perfil_intervalo->dados;
        $response["success"] = 1;
    }
    else{
        // sem registros
        $response["success"] = 0;
        $response["message"] = "Sem registros";
    }

    echo json_encode($response);

?>