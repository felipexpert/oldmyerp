<?php
    /* Listar todos os produtos em JSON */
    
    require_once 'class/dados.class.php';
    require_once 'class/funcionario.class.php';
     
    // array for JSON response
    $response = array();
     
    $funcionario = new funcionario();
    
    $funcionario->rows_per_page = 10000;
    $funcionario->load_all();
    
    if($funcionario->rowCount){
        // success
	$data = array();
	forEach($funcionario->dados as $row) {
	  $func = array('id' => intVal($row["fldId"]), 'name' => $row['fldNome']);
	  array_push($data, $func);
	}
        $response['data'] = $data;
        $response["success"] = TRUE;
    }
    else{
        // sem registros
        $response["success"] = FALSE;
        $response["message"] = "Sem registros";
    }

    header('Content-Type: application/json;UTF-8');
    echo json_encode($response);

?>
