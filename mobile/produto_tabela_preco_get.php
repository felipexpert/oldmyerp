<?php
    /* Listar todos os produtos em JSON */
    
    require_once 'class/dados.class.php';
    require_once 'class/produto_tabela_preco.class.php';
     
    // array for JSON response
    $response = array();
     
    $produto_tabela_preco = new produto_tabela_preco();
    
    
    $produto_tabela_preco->rows_per_page = 10000;
    $produto_tabela_preco->load_all();
    
    if($produto_tabela_preco->rowCount){
        // success
        $response['dados'] = $produto_tabela_preco->dados;
        $response["success"] = 1;
    }
    else{
        // sem registros
        $response["success"] = 0;
        $response["message"] = "Sem registros";
    }

    echo json_encode($response);

?>