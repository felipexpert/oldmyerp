<?php
    //header('Content-type: application/json; charset=utf-8');  
    /* Inserir ou atualizar clientes */
    
    require_once 'class/dados.class.php';
    require_once 'class/cliente.class.php';
    
    
    function fncUTF8toLatin1($valor){
        return mb_convert_encoding($valor,"Latin1","UTF-8");
    }
    //tentativas de normalizar os acentos:
    //$json = (mb_convert_encoding(mysql_real_escape_string($_POST['json']),"Latin1","UTF-8"));
    //$json = mb_convert_encoding($json,"ISO-8859-1","UTF-8");
    //$json = utf8_encode(($_POST['json']));
    
    $json = utf8_encode($_POST['json']);
    
    $dados = json_decode($json, true);
    
    echo "<hr>dados:";
    var_dump($dados);
    
    echo "<hr>";
    
    //aqui parece redundante o nome do array porque n�o consegui melhorar no lado do android
    foreach($dados['dados'] as $row){
            $cliente = new cliente();
            
            //se receber server_id, carrega para atualizar
            $id = $row['fldId'];
            if($id){
                $cliente->load("fldId = $id");
            }
            //
            
            $cliente->fldNome = fncUTF8toLatin1($row['fldNome']); //mb_convert_encoding($row['fldNome'],"Latin1","UTF-8");
            $cliente->fldNomeFantasia = fncUTF8toLatin1($json); //mb_convert_encoding($json,"Latin1","UTF-8");
            $cliente->fldCPF_CNPJ = fncUTF8toLatin1($row['fldEndereco']); //mb_convert_encoding($row['fldEndereco'],"Latin1","UTF-8");
            $cliente->store();
    }
    
    if(1){
        // success
        $response["success"] = 1;
        $response['message'] = "Cliente armazenado";
    }
    else{
        // sem registros
        $response["success"] = 0;
        $response["message"] = "Erro";
    }
    
    echo json_encode($response);
    
?>