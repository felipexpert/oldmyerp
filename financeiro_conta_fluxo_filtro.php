<?php
	$filtro = '';
	
	if($_SESSION['txt_conta_data1'] == ""){
		$_SESSION['txt_conta_data1'] = format_date_out($data_caixa);
	}
	if($_SESSION['txt_conta_data2'] == ""){
		$_SESSION['txt_conta_data2'] = format_date_out($data_caixa);
	}
	
	//memorizar os filtros para exibição nos selects
	if(isset($_POST['btn_limpar'])){
		$_SESSION['txt_movimento_id'] 		= "";
		$_SESSION['sel_movimento_fluxo'] 	= "";
		$_SESSION['sel_tipo_fluxo']			= "";
		$_SESSION['sel_marcador_fluxo'] 	= "";
		$_SESSION['sel_pagamento_fluxo'] 	= "";
		$_SESSION['txt_conta_data1']		= format_date_out($data_caixa);
		$_SESSION['txt_conta_data2'] 		= format_date_out($data_caixa);
		$_SESSION['txt_entidade'] 			= "";
		$_POST['chk_entidade_nome'] 		=  false;
	}
	else{
		$_SESSION['txt_movimento_id'] 		= (isset($_POST['txt_movimento_id']) 	? $_POST['txt_movimento_id'] 	: $_SESSION['txt_movimento_id']);
		$_SESSION['sel_movimento_fluxo'] 	= (isset($_POST['sel_movimento_fluxo']) ? $_POST['sel_movimento_fluxo'] : $_SESSION['sel_movimento_fluxo']);
		$_SESSION['sel_tipo_fluxo'] 		= (isset($_POST['sel_tipo_fluxo']) 		? $_POST['sel_tipo_fluxo'] 		: $_SESSION['sel_tipo_fluxo']);
		$_SESSION['sel_marcador_fluxo'] 	= (isset($_POST['sel_marcador_fluxo']) 	? $_POST['sel_marcador_fluxo'] 	: $_SESSION['sel_marcador_fluxo']);
		$_SESSION['sel_pagamento_fluxo'] 	= (isset($_POST['sel_pagamento_fluxo']) ? $_POST['sel_pagamento_fluxo'] : $_SESSION['sel_pagamento_fluxo']);
		$_SESSION['txt_conta_data1'] 		= (isset($_POST['txt_conta_data1']) 	? $_POST['txt_conta_data1'] 	: $_SESSION['txt_conta_data1']);
		$_SESSION['txt_conta_data2'] 		= (isset($_POST['txt_conta_data2']) 	? $_POST['txt_conta_data2'] 	: $_SESSION['txt_conta_data2']);
		$_SESSION['txt_entidade'] 			= (isset($_POST['txt_entidade']) 		? $_POST['txt_entidade'] 		: $_SESSION['txt_entidade']);
	}

	//passar para proximo dia ou anterior
	if(isset($_GET['prox'])){
		$data = format_date_in($_SESSION['txt_conta_data1']);
		$_SESSION['txt_conta_data1'] = date('d/m/Y',strtotime( "+1 day", strtotime($data)));
		
		$data = format_date_in($_SESSION['txt_conta_data2']);
		$_SESSION['txt_conta_data2'] = date('d/m/Y',strtotime( "+1 day", strtotime($data)));
	};
	
	if(isset($_GET['ant'])){
		$data = format_date_in($_SESSION['txt_conta_data1']);
		$_SESSION['txt_conta_data1'] = date('d/m/Y',strtotime( "-1 day", strtotime($data)));
		
		$data = format_date_in($_SESSION['txt_conta_data2']);
		$_SESSION['txt_conta_data2'] = date('d/m/Y',strtotime( "-1 day", strtotime($data)));
	};
	
?>
<form id="frm-filtro" action="index.php?p=financeiro&modo=conta_fluxo" method="post">
	<fieldset>
  	<legend>Buscar por:</legend>
    <ul>
        <li class="large_height">
      		<label for="txt_movimento_id"></label>
<?			$id = ($_SESSION['txt_movimento_id'] ? $_SESSION['txt_movimento_id'] : "ID");
			($_SESSION['txt_movimento_id'] == "ID") ? $_SESSION['txt_movimento_id'] = '' : '';
?>     		<input style="width:35px" type="text" name="txt_movimento_id" id="txt_movimento_id" value="<?=$id?>"/>
      	</li>
        <li class="large_height">
      		<label for="txt_data_conta"></label>
<?				$data1 = (isset($_POST['txt_calendario_data_inicial'])) ? $_POST['txt_calendario_data_inicial'] : $_SESSION['txt_conta_data1'];
?>     		<input style="text-align:center;width: 70px" type="text" name="txt_conta_data1" id="txt_conta_data1" class="calendario-mask" value="<?=$data1?>"/>
      	</li>
        <li class="large_height">
      		<label for="txt_data_conta"></label>
<?			$data2 = (isset($_POST['txt_calendario_data_final'])) ? $_POST['txt_calendario_data_final'] : $_SESSION['txt_conta_data2'];
?>     		<input style="text-align:center;width: 70px" type="text" name="txt_conta_data2" id="txt_conta_data2" class="calendario-mask" value="<?=$data2?>"/>
			<a href="calendario_periodo,<?=format_date_in($data1) . ',' . format_date_in($data2)?>,financeiro&modo=conta_fluxo" id="exibir-calendario" title="Exibir calend&aacute;rio" class="modal calendario-modal" rel="600-320"></a>
      	</li>
		<li>
			<input type="checkbox" name="chk_entidade_nome" id="chk_entidade_nome" <?=($_POST['chk_entidade_nome'] == true ? print 'checked ="checked"' : '')?>/>
<?			$entidade = ($_SESSION['txt_entidade'] ? $_SESSION['txt_entidade'] : "entidade");
			($_SESSION['txt_entidade'] == "entidade") ? $_SESSION['txt_entidade'] = '' : '';
?>     		<input style="width: 160px" type="text" name="txt_entidade" id="txt_entidade" onfocus="limpar (this,'entidade');" onblur="mostrar (this, 'entidade');" value="<?=$entidade?>"/>
			<small>marque para qualquer parte do campo</small>
		</li>
        <li class="large_height">
            <select id="sel_movimento_fluxo" name="sel_movimento_fluxo" style="width: 150px;">
                <option value="">TODOS</option>
<?				$rsTipo = mysql_query("SELECT * FROM tblfinanceiro_conta_fluxo_tipo ORDER BY fldTipo ASC");
                while($rowTipo = mysql_fetch_array($rsTipo)){
?>					<option <?=($_SESSION['sel_movimento_fluxo'] == $rowTipo['fldId']) ? 'selected="selected"' : '' ?> title="<?=$rowTipo['fldTipo'] ?>" value="<?=$rowTipo['fldId']?>"><?=$rowTipo['fldTipo']?></option>
<? 				}
?>				<option <?=($_SESSION['sel_movimento_fluxo'] == "pedidoAvista") ? 'selected="selected"' : '' ?> title="Recebimentos &agrave; vista" value="pedidoAvista">Recebimentos de parcela &agrave; vista</option>
				<option <?=($_SESSION['sel_movimento_fluxo'] == "pedidoAPrazo") ? 'selected="selected"' : '' ?> title="Recebimentos a prazo" value="pedidoAPrazo">Recebimentos de parcela a prazo</option>
			</select>
        </li>
        <li class="large_height">
            <select style="width:70px" id="sel_tipo_fluxo" name="sel_tipo_fluxo" >
                <option value=""> TIPO</option>
                <option <?=($_SESSION['sel_tipo_fluxo'] == 1) ? 'selected="selected"' : '' ?> value="1">CR&Eacute;DITO</option>
                <option <?=($_SESSION['sel_tipo_fluxo'] == 2) ? 'selected="selected"' : '' ?> value="2">D&Eacute;BITO</option>
			</select>
		</li> 
        <li class="large_height">
            <select id="sel_marcador_fluxo" name="sel_marcador_fluxo" style="width: 100px;">
                <option value="">MARCADOR</option>
<?				$rsMarcador  = mysql_query("SELECT * FROM tblfinanceiro_conta_fluxo_marcador ORDER BY fldMarcador");
        	    while($rowMarcador = mysql_fetch_array($rsMarcador)){
?>					<option <?=($_SESSION['sel_marcador_fluxo'] == $rowMarcador['fldId']) ? 'selected="selected"' : '' ?> title="<?=$rowMarcador['fldDescricao'] ?>" value="<?=$rowMarcador['fldId'] ?>"><?=$rowMarcador['fldMarcador'] ?></option>
<? 				}
?>			</select>
        </li>
        <li class="large_height">
            <select id="sel_pagamento_fluxo" name="sel_pagamento_fluxo" style="width: 120px;">
                <option value="">FORMA DE PAG.</option>
<?				$rsPagamento  = mysql_query("SELECT * FROM tblpagamento_tipo ORDER BY fldTipo");
                while($rowPagamento = mysql_fetch_array($rsPagamento)){
?>					<option <?=($_SESSION['sel_pagamento_fluxo'] == $rowPagamento['fldId']) ? 'selected="selected"' : '' ?> title="<?=$rowPagamento['fldSigla'] ?>" value="<?=$rowPagamento['fldId'] ?>"><?=$rowPagamento['fldTipo'] ?></option>
<? 				}
?>			</select>
        </li>
        <li style="float:right">
      	    <button type="submit" name="btn_exibir" title="Exibir">Exibir</button>
    		<button type="submit" name="btn_limpar" title="Limpar Filtro">Limpar filtro</button>
        </li>
    </ul>
    
  </fieldset>
</form>

<?
	
	if(($_SESSION['txt_movimento_id']) != ""){
		$filtro .= " AND vwfinanceiro_conta_fluxo.fldId =".$_SESSION['txt_movimento_id'];
	}
	
	if(($_SESSION['sel_movimento_fluxo']) != ""){
		if($_SESSION['sel_movimento_fluxo'] == "pedidoAvista" || $_SESSION['sel_movimento_fluxo'] == "pedidoAPrazo"){
			$filtro .= " AND vwfinanceiro_conta_fluxo.fldMovimento_Tipo = 3";
		}
		else{
			$filtro .= " AND vwfinanceiro_conta_fluxo.fldMovimento_Tipo =".$_SESSION['sel_movimento_fluxo'];
		}
	}
	
	if(($_SESSION['sel_tipo_fluxo']) != ""){
		
		// verificando se é credito ou debito
		if($_SESSION['sel_tipo_fluxo'] == 1){
			$sel_tipo = 'vwfinanceiro_conta_fluxo.fldCredito';
		}elseif($_SESSION['sel_tipo_fluxo'] == 2){
			$sel_tipo = 'vwfinanceiro_conta_fluxo.fldDebito';
		}
		
		$filtro .= " AND ".$sel_tipo." > '0'";
	}
	
	if(($_SESSION['sel_marcador_fluxo']) != ""){
		
		$filtro .= " AND vwfinanceiro_conta_fluxo.fldMarcador_Id =".$_SESSION['sel_marcador_fluxo'];
	}
	
	if(($_SESSION['sel_pagamento_fluxo']) != ""){
		
		$filtro .= " AND vwfinanceiro_conta_fluxo.fldPagamento_Tipo_Id =".$_SESSION['sel_pagamento_fluxo'];
	}
	
	if(($_SESSION['txt_entidade']) != ""){
		$entidade = addslashes($_SESSION['txt_entidade']);
		if($_POST['chk_entidade_nome'] == true){
			$filtro .= " AND vwfinanceiro_conta_fluxo.Entidade LIKE '%$entidade%'";
		}else{
			$filtro .= " AND vwfinanceiro_conta_fluxo.Entidade LIKE '$entidade%'";
		}
	}
	
	
	//transferir para a sessão
	if(isset($_POST['btn_exibir'])){
		$_SESSION['filtro_conta_fluxo'] = $filtro;
	}
	elseif(isset($_POST['btn_limpar'])){
		$_SESSION['filtro_conta_fluxo'] = "";
	}

?>
