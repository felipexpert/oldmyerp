<?php

	require("lib/WideImage/WideImage.php");
	//usar biblioteca WideImage para redimensionamento e afins;
	
	$source = $_GET['src'];
	$width 	= $_GET['w'];
	$height = $_GET['h'];
	$w_half = "50% - ".$width / 2;
	$h_half = "50% - ".$height / 2;

	if(!file_exists($source)){ $source = 'image/null.jpg'; }

	$imagem = WideImage::load($source)->resize($width, $height, 'outside')->crop($w_half, $h_half, $width, $height)->output('jpg');

?>