<div id="voltar">
    <p><a href="index.php?p=financeiro_contas_pagar&modo=programada">n&iacute;vel acima</a></p>
</div>	

<h2>Nova conta programada</h2>

<div id="principal">
<?
	//GRAVANDO NO BANCO DE DADOS
	if(isset($_POST["btn_enviar"])):
		//corrigir o tipo de dados que vem do select
		$sqlInsert = "INSERT INTO tblfinanceiro_conta_pagar_programada (fldNome, fldDescricao, fldMarcador, fldIntervalo_Tipo, fldIntervalo_Frequencia, fldData_Inicio, fldData_Termino, fldPagamento_Id, fldValor) VALUES(
		'" . mysql_real_escape_string($_POST['txt_nome']) . "',
		'" . mysql_real_escape_string($_POST['txt_descricao']) . "',
		'" . $_POST['sel_marcador'] . "', 
		'" . $_POST['sel_tipo_frequencia'] . "', 
		'" . (($_POST['txt_intervalo'] == '' or $_POST['txt_intervalo'] == 0) ? 1 : $_POST['txt_intervalo']) . "',
		'" . format_date_in($_POST['txt_data_inicio']) . "',
		'" . (($_POST['txt_data_termino']) ? format_date_in($_POST['txt_data_termino']) : '') . "',
		'" . $_POST['sel_pagamento'] . "', 
		'" . format_number_in($_POST['txt_valor']) . "'
		)";
		
		if(mysql_query($sqlInsert)):
			header("location:index.php?p=financeiro_contas_pagar&modo=programada&mensagem=ok");
		else:
?>
			<div class="alert">
				<p class="erro">N&atilde;o foi poss&iacute;vel gravar os dados!</p>
			</div>
<?php
echo mysql_error();
		endif;
		
	endif;
	
	//SELECIONANDO O TIPO DE INTERVALO PARA EXIBIR NO FORMULÁRIO
	$rsTipoFrequencia  = mysql_query("SELECT fldId, fldNome FROM tblsistema_calendario_intervalo");
	
	//SELECIONANDO OS MARCADORES PARA EXIBIR NO FORMULÁRIO
	$rsMarcador  = mysql_query("SELECT * FROM tblfinanceiro_conta_fluxo_marcador");
	
	//SELECIONANDO O TIPO DE PAGAMENTO PARA EXIBIR NO FORMULÁRIO
	$rsPagamento  = mysql_query("SELECT * FROM tblpagamento_tipo");
	
?>	<div class="form">
        <form class="frm_detalhe" id="frm_conta_programada" action="" method="post">
            <ul>
                <li>
                    <label for="txt_nome">Nome</label>
                    <input style="width: 300px;" type="text" id="txt_nome" name="txt_nome" value="" />
                </li>
                <li>
                    <label for="txt_descricao">Descri&ccedil;&atilde;o</label>
                    <input style="width: 420px;" type="text" id="txt_descricao" name="txt_descricao" value="" />
                </li>
				<li>
					<label for="sel_marcador">Marcador</label>
					<select id="sel_marcador" name="sel_marcador">
						<?php
						while($rowMarcador = mysql_fetch_array($rsMarcador)): ?>
						<option title="<?= $rowMarcador['fldDescricao'] ?>" value="<?= $rowMarcador['fldId'] ?>"><?= $rowMarcador['fldMarcador'] ?></option>
						<?php endwhile; ?>
					</select>
				</li>
				<li>
					<label for="sel_tipo_frequencia">Frequ&ecirc;ncia</label>
					<select id="sel_tipo_frequencia" name="sel_tipo_frequencia">
						<?php
						while($rowTipoFrequencia = mysql_fetch_array($rsTipoFrequencia)): ?>
						<option value="<?= $rowTipoFrequencia['fldId'] ?>"><?= $rowTipoFrequencia['fldNome'] ?></option>
						<?php endwhile; ?>
					</select>
				</li>
				<li>
                    <label for="txt_intervalo">Intervalo</label>
                    <input style="width: 100px; text-align: center;" type="text" id="txt_intervalo" name="txt_intervalo" value="1" />
                </li>
				<li>
					<label for="txt_data_inicio">In&iacute;cio em</label>
                    <input style="width: 80px; text-align: center" type="text" class="calendario-mask" id="txt_data_inicio" name="txt_data_inicio" value="<?= date('d/m/Y') ?>" />
					<a href="#" title="Exibir calend&aacute;rio" class="exibir-calendario-data-atual"></a>
				</li>
				<li>
					<label for="sel_data_termino">Termina em</label>
					<select id="sel_data_termino" name="sel_data_termino">
						<option value="0">N&atilde;o termina</option>
						<option value="1">Escolher uma data</option>
					</select>
					<input style="width: 100px; margin: 0 0 0 10px;  text-align: center" class="hidden calendario-mask" type="text" id="txt_data_termino" name="txt_data_termino" value="" />
					<a href="#" id="hidden-calendar" title="Exibir calend&aacute;rio" class="hidden exibir-calendario-data-atual"></a>
				</li>
			</ul>
			<ul style="width: 800px;">
				<li>
					<label for="sel_pagamento">Tipo de pagamento</label>
					<select style="width: 125px;" id="sel_pagamento" name="sel_pagamento">
						<?php
						while($rowPagamento = mysql_fetch_array($rsPagamento)): ?>
						<option value="<?= $rowPagamento['fldId'] ?>"><?= $rowPagamento['fldTipo'] ?></option>
						<?php endwhile; ?>
					</select>
				</li>
				<li>
					<label for="txt_valor">Valor aproximado</label>
                    <input style="width: 110px; text-align: center" type="text" id="txt_valor" name="txt_valor" value="0,00" />
				<li>
			</ul>
			<ul style="float: right;">
				<li><input type="submit" style="margin-top: 12px" class="btn_enviar" name="btn_enviar" id="btn_enviar" value="Salvar" title="Salvar" /></li>
				<li><a style="margin-top: 12px;" class="btn_cancel" href="?p=financeiro_contas_pagar&amp;modo=programada" title="Cancelar opera&ccedil;&atilde;o">Cancelar</a></li>
			</ul>
        </form>
	</div>
	
</div>