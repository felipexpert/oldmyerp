<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
      	<title>myERP - Relat&oacute;rio de Funcion&aacute;rios</title>
        <link rel="stylesheet" type="text/css" href="style/style_relatorio.css" />
        <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />

	</head>
	<body>
      
            
        <div id="no-print">
            <a class="print" href="#" onClick="window.print()">imprimir</a>
        </div>  
<?		
		ob_start();
		session_start();
		
		require("inc/con_db.php");
		require("inc/fnc_general.php");
		require("inc/fnc_ibge.php");
		
		$rsDados 		= mysql_query("select * from tblempresa_info");
		$rowDados 		= mysql_fetch_array($rsDados);
		
		$CPF_CNPJDados 	= formatCPFCNPJTipo_out($rowDados['fldCPF_CNPJ'], $rowDados['fldTipo']);

		$rowUsuario 	= mysql_fetch_array(mysql_query("select * from tblusuario where fldId=".$_SESSION['usuario_id']));
		$rsFuncionario 	= mysql_query($_SESSION['funcionario_relatorio']);
		$rowFuncionarioTotal = mysql_num_rows($rsFuncionario);
		echo mysql_error();
		$limite = 6;
		$n = 1;
		
		$pgTotal = $rowFuncionarioTotal / $limite;
		$p = 1;
		
		$tabelaCabecalho = '<tr style="border-bottom: 2px solid">
                <td style="width: 600px"><h1>Relat&oacute;rio de Funcion&aacute;rios</h1></td>
                <td style="width: 200px"><p class="pag">p&aacute;g. '.$p.' de '.ceil($pgTotal).'</p></td>
            </tr>
            <tr>
                <td>
                    <table style="width: 580px" name="table_relatorio_dados" class="table_relatorio_dados" summary="Relat&oacute;rio">
                        <tr>
                            <td style="width: 320px;">Raz&atilde;o Social: '.$rowDados['fldNome'].'</td>
                            <td style="width: 200px;">Nome Fantasia: '.$rowDados['fldNome_Fantasia'].'</td>
                            <td style="width: 320px;">CPF/CNPJ: '.$CPF_CNPJDados.'</td>
                            <td style="width: 200px;">Telefone: '.$rowDados['fldTelefone1'].'</td>
                        </tr>
                    </table>	
                </td>
                <td>        
                    <table class="dados_impressao">
                        <tr>
                            <td><b>Data: </b><span>'.format_date_out(date("Y-m-d")).'</span></td>
                            <td><b>Hora: </b><span>'.format_time_short(date("H:i:s")).'</span></td>
                            <td><b>Usu&aacute;rio: </b><span>'.$rowUsuario['fldUsuario'].'</span></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table name="table_relatorio" class="table_relatorio" summary="Relat&oacute;rio">';
?>
		<table class="relatorio_print">
<?		print $tabelaCabecalho;
		$tabelaCabecalho = '<table class="relatorio_print">'.$tabelaCabecalho;
            
		while($rowFuncionario = mysql_fetch_array($rsFuncionario)){
			$x+= 1;
			
			$Municipio 	= fnc_ibge_municipio($rowFuncionario['fldMunicipio_Codigo']);
			$SiglaUF	= fnc_ibge_uf_sigla($rowFuncionario['fldMunicipio_Codigo']);
			$CPF_CNPJ 	= formatCPFCNPJTipo_out($rowFuncionario['fldCPF_CNPJ'], $rowFuncionario['fldTipo']);
?>	
            <tr class="relatorio_dados">
                <td><b>Funcion&aacute;rio: </b><?=$rowFuncionario['fldNome']?></td>
                <td><b>C&oacute;digo: 	</b><?=$rowFuncionario['fldId']?></td>
                <td><b>Apelido: 		</b><?=$rowFuncionario['fldApelido']?></td>
                <td><b>Cadastrado em: 	</b><?= format_date_out($rowFuncionario['fldCadastroData'])?></td>
                <td><b>Endere&ccedil;o: </b><?=$rowFuncionario['fldEndereco']?>, <?=$rowFuncionario['fldNumero']?></td>
                <td><b>Bairro: 			</b><?=$rowFuncionario['fldBairro']?></td>
                <td><b>CEP: 			</b><?=$rowFuncionario['fldCep']?></td>
                <td><b>Cidade: 			</b><?=$Municipio?></td>
                <td><b>UF: 				</b><?=$SiglaUF?></td>
                <td><b>Telefone 1: 		</b><?=$rowFuncionario['fldTelefone1']?></td>
                <td><b>Telefone 2: 		</b><?=$rowFuncionario['fldTelefone2']?></td>
                <td><b>E-mail: 			</b><?=$rowFuncionario['fldEmail']?></td>
                <td><b>Msn: 			</b><?=$rowFuncionario['fldMsn']?></td>
                <td><b>Skype: 			</b><?=$rowFuncionario['fldSkype']?></td>
                <td><b>CPF/CNPJ: 		</b><?=$CPF_CNPF?></td>
                <td><b>RG/IE: 			</b><?=$rowFuncionario['fldRG_IE']?></td>
            </tr>

<?			if($n == $limite){
        		$n = 1;
?>							</table>    
						</td>
					</tr>
				</table>
<?				if($x < $rowFuncionarioTotal){
					$p += 1;
					print $tabelaCabecalho;
				}
			}else{
				$n += 1;
			}
		}
?>
	</body>
</html>
