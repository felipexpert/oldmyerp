<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="myERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
      	<title>myERP - Relat&oacute;rio de Endere&ccedil;os</title>
        <link rel="stylesheet" type="text/css" href="style/style_relatorio_endereco.css" />
        <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />

	</head>
	<body>
      
        <div id="no-print">
            <a class="print" href="#" onClick="window.print()">imprimir</a>
        </div>
<? 
		ob_start();
		session_start();
		
		require("inc/con_db.php");
		require("inc/fnc_general.php");
		
		$rsDados  		= mysql_query("SELECT * FROM tblempresa_info");
		$rowDados 		= mysql_fetch_array($rsDados);
		$CPF_CNPJDados 	= formatCPFCNPJTipo_out($rowDados['fldCPF_CNPJ'], $rowDados['fldTipo']);
		
		$perfil 		= $_POST['sel_relatorio_perfil'];
		$setor 			= $_POST['sel_relatorio_setor'];
		
		$rsUsuario  	= mysql_query("SELECT * FROM tblusuario WHERE fldId=".$_SESSION['usuario_id']);
		$rowUsuario 	= mysql_fetch_array($rsUsuario);
		
		$rsRelatorio	= mysql_query("SELECT tblcliente.fldNome, tblcliente.fldId as fldCliente_Id, tblcliente.fldCodigo, tblcliente.fldNumero, tblendereco.fldRua, tblendereco.fldOrdem, tblendereco.fldId as fldRua_Id, 
								   tblendereco_bairro.fldBairro, tblendereco_bairro.fldId as fldBairroId, tblendereco_bairro.fldSetor
									FROM tblendereco 
									INNER JOIN tblcliente on tblcliente.fldEndereco_Id = tblendereco.fldId
									INNER JOIN tblendereco_bairro on tblendereco.fldBairro_Id = tblendereco_bairro.fldId
									WHERE tblendereco_bairro.fldSetor = '$setor' 
									ORDER BY tblendereco.fldOrdem+0,  tblendereco.fldRua, tblcliente.fldNumero asc");
		$rowRelatorioTotal = mysql_num_rows($rsRelatorio);
		echo mysql_error();
		#total de produtos por pagina = 50
		# 10produtos = 5por pagina
		# 8 produtos = 6por pagina
		# 6 produtos = 7por pagina
		# 4 produtos = 8por pagina
		# 2 produtos = 11por pagina
		
		$limite = 4;
		$n 		= 1;
		$p 		= 1;
		$tabelaCabecalho = '<tr style="border-bottom: 2px solid">
                    <td style="width: 550px"><h1>Relat&oacute;rio de Endere&ccedil;os</h1></td>
                    <td style="width: 200px"></td>
                </tr>
                <tr>
                    <td>
                        <table name="table_relatorio_dados" class="table_relatorio_dados" summary="Relat&oacute;rio">
                            <tr>
                                <td style="width: 300px;">Raz&atilde;o Social: '.$rowDados['fldNome'].'</td>
                                <td style="width: 200px;">Nome Fantasia: '.$rowDados['fldNome_Fantasia'].'</td>
                                <td style="width: 300px;">CPF/CNPJ: '.$CPF_CNPJDados.'</td>
                                <td style="width: 200px;">Telefone: '.$rowDados['fldTelefone1'].'</td>
                            </tr>
                        </table>	
                    </td>
                    <td>        
                        <table class="dados_impressao">
                            <tr>
                                <td><b>Data: </b><span>'.format_date_out(date("Y-m-d")).'</span></td>
                                <td><b>Hora: </b><span>'.format_time_short(date("H:i:s")).'</span></td>
                                <td><b>Usu&aacute;rio: </b><span>'.$rowUsuario['fldUsuario'].'</span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table name="table_relatorio" class="table_relatorio" summary="Relat&oacute;rio">
		';
?>		<table class="relatorio_print" style="page-break-before:avoid">  
<?			print $tabelaCabecalho;
			$tabelaCabecalho = '<table class="relatorio_print">'.$tabelaCabecalho;
			while($rowRelatorio = mysql_fetch_array($rsRelatorio)){
				$cliente_id = $rowRelatorio['fldCliente_Id'];
				$x+= 1;
					if($ruaId != $rowRelatorio['fldRua_Id'] && $n != 1 && $x){
        				$n 	= 1;
						$p += 1;
?>									</table>    
	            	    		</td>
            	            </tr>
        	            </table>
<?						print $tabelaCabecalho;
					}

					if($n == 1){
						if($ruaId != $rowRelatorio['fldRua_Id']){ #pra colocar no indice
							$controle +=1;
							$indice[$controle] = array('pagina' => $p, 'rua' => $rowRelatorio['fldRua']); 
						}
?>	
                        <tr class="relatorio_dados" style="border:0; margin:0">
                            <td style="width:740px;font-size:15px;border:0">SETOR: <?=str_pad($rowRelatorio['fldSetor'], 6, "0", STR_PAD_LEFT)?> - <?=$rowRelatorio['fldBairro']?></td>
                        </tr>
                        <tr class="relatorio_dados" style="border:0">
                            <td style="width:450px;font-size:15px;border:0">Rua: <?=$rowRelatorio['fldRua']?></td>
                            <td style="width:200px;font-size:15px;border:0">Sequencia: <?=str_pad($rowRelatorio['fldOrdem'], 4, "0", STR_PAD_LEFT)?></td>
                            <td style="width:60px; font-size:15px;border:0">P&aacute;g. <?=$p?></td>
                        </tr>
<?					}					
?>
                    <tr class="relatorio_dados" style="border:0;border-top:1px dashed;margin-top:3px">
                        <td style="width:450px;border:0;font-size:15px;text-transform:uppercase; margin-top:3px; font-weight: bold;"><?=str_pad($rowRelatorio['fldCodigo'], 6, "0", STR_PAD_LEFT)?> - <?=$rowRelatorio['fldNome']?></td>
                        <td style="width:150px;border:0;font-size:15px;text-transform:uppercase; margin-top:3px; font-weight: bold;">casa n&deg; <?=str_pad($rowRelatorio['fldNumero'], 4, "0", STR_PAD_LEFT);?></td>
<?						$rsProduto = mysql_query("SELECT tblproduto.fldId, fldNome FROM tblproduto LEFT JOIN tblendereco_relatorio_perfil_produto
												 ON tblproduto.fldId = tblendereco_relatorio_perfil_produto.fldProduto_Id
												 WHERE tblendereco_relatorio_perfil_produto.fldPerfil_Id = $perfil ORDER BY fldNome LIMIT 10");
						$rows = mysql_num_rows($rsProduto);
						echo mysql_error();
						$valor_devedor =0;
						while($rowProduto = mysql_fetch_array($rsProduto)){
							$rsItem = mysql_query("SELECT tblpedido_item.fldValor,
												  	SUM(tblpedido_item.fldQuantidade * (tblpedido_item.fldExcluido * -1 + 1))as fldQuantidade,
													(SELECT SUM(tblpedido_item_pago.fldQuantidade * (tblpedido_item_pago.fldExcluido * -1 + 1))
													FROM tblpedido_item RIGHT JOIN tblpedido_item_pago ON tblpedido_item_pago.fldItem_Id = tblpedido_item.fldId
													RIGHT JOIN tblpedido ON tblpedido.fldId = tblpedido_item.fldPedido_Id
													WHERE tblpedido.fldCliente_Id = $cliente_id
													AND tblpedido_item.fldProduto_Id = ".$rowProduto['fldId'].")as fldQuantidadePaga
													FROM tblpedido_item
													RIGHT JOIN tblpedido ON tblpedido.fldId = tblpedido_item.fldPedido_Id
													WHERE tblpedido.fldCliente_Id = $cliente_id AND tblpedido_item.fldProduto_Id = ".$rowProduto['fldId']." AND tblpedido.fldExcluido = '0'");
							$rowItem = mysql_fetch_array($rsItem);
							$quantidade_devedor  = $rowItem['fldQuantidade'] - $rowItem['fldQuantidadePaga'];
							$valor_devedor 		+= $quantidade_devedor * $rowItem['fldValor'];
							
?>                        	<td style="text-transform:uppercase"><?=$rowProduto['fldNome'].' : '.(($quantidade_devedor > 0)? $quantidade_devedor : '') ?></td>
<?						}

						$rsDevedor = mysql_query("SELECT SUM(tblpedido_parcela.fldValor * (tblpedido_parcela.fldExcluido * -1 + 1)) as fldValorTotal,
														 SUM(tblpedido.fldDescontoReais * (tblpedido.fldExcluido * -1 + 1)) as fldTotalDescontoReais,
														 (SELECT SUM(tblpedido_parcela_baixa.fldValor * (tblpedido_parcela_baixa.fldExcluido * -1 + 1))
															FROM  tblpedido_parcela
															INNER JOIN tblpedido ON tblpedido.fldId = tblpedido_parcela.fldPedido_Id
															LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
															WHERE tblpedido.fldCliente_Id = $cliente_id
															GROUP BY tblpedido.fldCliente_Id
														 ) as fldValorBaixa,
														 (SELECT SUM(tblpedido_parcela_baixa.fldDesconto * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) 
															FROM  tblpedido_parcela
															INNER JOIN tblpedido ON tblpedido.fldId = tblpedido_parcela.fldPedido_Id
															LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
															WHERE tblpedido.fldCliente_Id = $cliente_id
															GROUP BY tblpedido.fldCliente_Id
														 ) as fldValorDesconto
														 FROM  tblpedido
														 INNER JOIN tblpedido_parcela ON tblpedido.fldId = tblpedido_parcela.fldPedido_Id
														 WHERE tblpedido.fldCliente_Id = $cliente_id");
						$rowDevedor = mysql_fetch_array($rsDevedor);
						echo mysql_error();
						$parcela_devedor =($rowDevedor['fldValorTotal'] - $rowDevedor['fldValorDesconto']) - $rowDevedor['fldValorBaixa'];
						
						$total_devedor 	 = $parcela_devedor - $valor_devedor;
?>						<td>DEVEDOR : <?=format_number_out($total_devedor)?></td>
						<td style="width:735px">&nbsp;</td>
                    </tr>
<?				
					if($n == $limite || $x == $rowRelatorioTotal){
        				$n = 1;
?>									</table>    
	            	    		</td>
            	            </tr>
        	            </table>
<?					
						if($x < $rowRelatorioTotal){
							$p += 1;
							print $tabelaCabecalho;
						}
					}else{
						$n += 1;
					}
					$ruaId = $rowRelatorio['fldRua_Id'];
				}
				# INDICE DE RUAS #####################################################################################################################################################
				print $tabelaCabecalho;
				$x = 1;
				$totalRuas 	= count($indice);
				$limite 	= 36;
				while($indice[$x]){
					$pagina = str_pad($indice[$x]['pagina'], 4, "0", STR_PAD_LEFT);			
					if($n == 1){
?>						<tr class="relatorio_dados" style="border-bottom:1px dashed; margin-bottom:10px">
                            <td style="border:0; width:300px">P&aacute;gina</td>
                            <td style="border:0; width:420px; mar">Rua</td>
                        </tr>
<?					}
?>					<tr class="relatorio_dados" style="border:0">
                        <td style="border:0; width:280px"><?=str_pad($pagina, 50, ".", STR_PAD_RIGHT)?></td>
                        <td style="border:0; width:410px"><?=$indice[$x]['rua']?></td>
                    </tr>
<?					if($n == $limite || $x == $totalRuas){
						$n = 1;
?>									</table>    
								</td>
							</tr>
						</table>
<?						if($x < $totalRuas){
							$p += 1;
							print $tabelaCabecalho;
						}
					}else{
						$n += 1;
					}
					$x +=1;
				}
?>					
	</body>
</html>