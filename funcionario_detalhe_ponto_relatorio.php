<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
      	<title>myERP - Relat&oacute;rio de Ponto</title>
        <link rel="stylesheet" type="text/css" href="style/style_relatorio.css" />
        <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="style/style_filtro.css" />
        
	</head>
	<body>
   		<div id="no-print">
            <ul style="width:800px; display:inline">
                <li><a class="print" href="#" onClick="window.print()">imprimir</a></li>
            </ul>
        </div>
<?
		ob_start();
		session_start();
		
		require("inc/con_db.php");
		require("inc/fnc_general.php");
		
		$rowDados		= mysql_fetch_array(mysql_query("SELECT * FROM tblempresa_info"));
		$CPF_CNPJDados 	= formatCPFCNPJTipo_out($rowDados['fldCPF_CNPJ'], $rowDados['fldTipo']);
		$rowUsuario		= mysql_fetch_array(mysql_query("SELECT * FROM tblusuario WHERE fldId=".$_SESSION['usuario_id']));
		
		#ABAIXO CRIO O CABECALHO #########################################################################################################################################################
		$tabelaCabecalho =' 
			<tr style="border-bottom: 2px solid">
                <td style="width: 600px"><h1>Relat&oacute;rio de Ponto</h1></td>';
		$tabelaCabecalho2 =' 
            </tr>
            <tr>
                <td>
                    <table style="width: 580px" class="table_relatorio_dados" summary="Relat&oacute;rio">
                        <tr>
                            <td style="width: 320px;">Raz&atilde;o Social: '.$rowDados['fldNome'].'</td>
                            <td style="width: 200px;">Nome Fantasia: '.$rowDados['fldNome_Fantasia'].'</td>
                            <td style="width: 320px;">CPF/CNPJ: '.$CPF_CNPJDados.'</td>
                            <td style="width: 200px;">Telefone: '.$rowDados['fldTelefone1'].'</td>
                        </tr>
                    </table>	
                </td>
                <td>        
                    <table class="dados_impressao">
                        <tr>
                            <td><b>Data: 			</b><span>'.format_date_out(date("Y-m-d")).'</span></td>
                            <td><b>Hora: 			</b><span>'.format_time_short(date("H:i:s")).'</span></td>
                            <td><b>Usu&aacute;rio: 	</b><span>'.$rowUsuario['fldUsuario'].'</span></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="border-top: 1px solid">
				<td style="width:135px;text-align:center">Data</td>
				<td style="width:95px;text-align:center">Entrada 1</td>
				<td style="width:95px;text-align:center">Sa&iacute;da 1</td>
				<td style="width:95px;text-align:center">Entrada 2</td>
				<td style="width:95px;text-align:center">Sa&iacute;da 2</td>
				<td style="width:95px;text-align:center">Entrada Extra 1</td>
				<td style="width:95px;text-align:center">Sa&iacute;da Extra 2</td>
				
				<td style="width:95px;text-align:center">Total Di&aacute;rio</td>
				
			  
			</tr>';
			$tabelaCabecalho3 ='
			<tr>
				<td>
					<table class="table_relatorio" summary="Relat&oacute;rio">';
		##########################################################################################################################################################################################
			$n	 			= 1; #DEFINE O NUMERO DA DO BLOCO
			$countRegistro  = 1; #DEFINE CONTAGEM DE ITENS NO WHILE PARA QUEBRA DE BLOCO AO ATINGIR LIMITE DE 20
			$x				= 1; #DEFINE CONTAGEM DE ITENS TOTAIS, PRA SABER SE JA TERMINOU WHILE MAS AINDA FALTA ESPACO
			$limite 		= 32;
			$total_array	= count($_SESSION['funcionario_ponto_relatorio']);

			$pgTotal 		= ceil($total_array / $limite);
			$p = 1;

			$rsFuncionario 	= mysql_query('SELECT fldNome FROM tblfuncionario WHERE fldId ='.$_GET['id']);
			$rowFuncionario = mysql_fetch_array($rsFuncionario);
			$funcionario	= $rowFuncionario['fldNome'];
			
			
			foreach($_SESSION['funcionario_ponto_relatorio'] as $resultado){	
				unset($normal_diaria);
				unset($extra_diaria);
				unset($soma_diaria);
				
				
				$intervalo1[$x] 	 = (!empty($resultado[1]["fldHora"]) && !empty($resultado[2]["fldHora"])) ? fnc_intervalo_hora($resultado[1]["fldHora"], $resultado[2]["fldHora"]) : '00:00';
				$intervalo2[$x] 	 = (!empty($resultado[3]["fldHora"]) && !empty($resultado[4]["fldHora"])) ? fnc_intervalo_hora($resultado[3]["fldHora"], $resultado[4]["fldHora"]) : '00:00';
				$intervalo_extra =(!empty($resultado[5]["fldHora"]) && !empty($resultado[6]["fldHora"])) ? fnc_intervalo_hora($resultado[5]["fldHora"], $resultado[6]["fldHora"]) : '00:00';
				
				$intervalo_normal	 = $intervalo1[$x].','.$intervalo2[$x].',';
				$intervalo_extra2	 = $intervalo_extra.',';
				
				$soma_normal 		.= $intervalo_normal;
				$soma_extra	 		.= $intervalo_extra2;
				
				$normal_diaria		= fnc_soma_hora(substr($intervalo_normal,0,-1));
				$extra_diaria		= fnc_soma_hora($intervalo_extra2);
				$soma_diaria		= fnc_soma_hora($normal_diaria.','.$extra_diaria);
				
				
				$pagina[$n] .='
					<tr>
						<td style="width:125px;text-align:center">'.format_date_out($resultado['fldData']).'</td>
						<td style="width:85px;text-align:center">'.format_time_short(($resultado[1]["fldHora"]) ? $resultado[1]["fldHora"] : "00:00:00").'</td>
						<td style="width:85px;text-align:center">'.format_time_short(($resultado[2]["fldHora"]) ? $resultado[2]["fldHora"] : "00:00:00").'</td>
						<td style="width:85px;text-align:center">'.format_time_short(($resultado[3]["fldHora"]) ? $resultado[3]["fldHora"] : "00:00:00").'</td>
						<td style="width:85px;text-align:center">'.format_time_short(($resultado[4]["fldHora"]) ? $resultado[4]["fldHora"] : "00:00:00").'</td>
						<td style="width:85px;text-align:center">'.format_time_short(($resultado[5]["fldHora"]) ? $resultado[5]["fldHora"] : "00:00:00").'</td>
						<td style="width:85px;text-align:center">'.format_time_short(($resultado[6]["fldHora"]) ? $resultado[6]["fldHora"] : "00:00:00").'</td>
						
						
						<td style="width:85px;text-align:center">'.format_time_short(($soma_diaria) ? $soma_diaria : "00:00:00").'</td>
					</tr>';
				
				
				#SE CHEGAR A 20 LINHAS, MUDA DE 'BLOCO' E RECMECA CONTAGEM
				if($countRegistro == $limite && $total_array != $x){
					$countRegistro = 1;
					$n ++;
				}elseif($total_array == $x && $countRegistro < $limite){ #SE JA TERMINOU O WHILE DE REGISTROS MAS AINDA NAO ATINGIU 20 LINHAS, CONTINUAR CRIANDO LINHAS ATE O LIMITE
					while($countRegistro <= $limite){ $pagina[$n] .='<tr style="border:0; width:800px"></tr>'; $countRegistro++;}
				}else{
					$countRegistro ++;
				}
				$x ++;
			}
			
			
			
			
			   
			
			$tempo_normal	 = fnc_soma_hora(substr($soma_normal,0,-1));
			$tempo_extra	 = fnc_soma_hora($soma_extra);
			$carga_mensal	= format_time_in(219.59);
			$normal_extra	=fnc_soma_hora($tempo_normal.','.$tempo_extra);
			$calc_horas	=format_time_in($normal_extra-$carga_mensal);
			
			
			   
			
		#AGORA MANDO GERAR NA TELA PARA IMPRESSAO ############################################################################################################################################
		$x = 1;
		while($x <= $n){
			$tabelaCabecalho1 = ($x == 1)? '<table class="relatorio_print" style="page-break-before:avoid">'.$tabelaCabecalho : '<table class="relatorio_print">'.$tabelaCabecalho;
			#PRIMEIRO BLOCO (LANCADOS) ###################################################################################################################################################
				print $tabelaCabecalho1;
				
                print '<td style="width: 200px"><p class="pag">'.$p.' de '.$pgTotal.'</p></td>';
				print $tabelaCabecalho2;
				print $tabelaCabecalho3;
				echo  $pagina[$x];
?>
						</table>
					</td >
				</tr>
<?				if($p == $pgTotal){  //se chegar ao final da listagem printar o rodape               
?>
					<tr style="width:800px">
						<td style="width:755px;font-size:16px;margin-left:20px;border-bottom:1px solid #666">Funcion&aacute;rio: <?=$funcionario?></td>
					</tr>
					<tr style="width:800px;margin-top:10px">
						<td style="font-size:14px;margin-left:20px;font-weight:bold">Total de Horas</td>
					</tr>
					<tr style="width:800px">
						<td style="width:100px;margin-left:20px;font-size:14px">Horas Normais: </td>
						<td style="width:150px;font-size:14px;text-align:right;border-bottom:1px solid"><?=$tempo_normal?></td>
						<td style="width:150px;margin-left:100px;font-size:14px">Carga Hor&aacute;ria Mensal: </td>
						<td style="width:150px;font-size:14px;text-align:right;border-bottom:1px solid">220:00:00</td>
						
					</tr>
					<tr style="width:800px">
						<td style="width:100px;margin-left:20px;font-size:14px">Ent./Sa&iacute. Extra: </td>
						<td style="width:150px;font-size:14px;text-align:right;border-bottom:1px solid"><?=$tempo_extra?></td>
						<td style="width:150px;margin-left:100px;font-size:14px"> Horas Extras/Devidas: </td>
						<td style="width:150px;font-size:14px;text-align:right;border-bottom:1px solid"><?=$calc_horas?></td>
					</tr>
					
					</tr>
					<tr style="width:800px">
						<td style="width:100px;margin-left:20px;font-size:14px">Total: </td>
						<td style="width:150px;font-size:14px;text-align:right;border-bottom:1px solid"><?=$normal_extra?></td>
					</tr>
					
					
					
					
					
					<tr style="width:800px">
						<td style="width:800px;text-align:center;font-size:14px">Data: <?=date('d/m/Y')?></td>
					</tr>
					<tr style="width:800px;margin-top:10px">
						<td style="width:800px;text-align:center;font-size:14px">Confirmo as informa&ccedil;&otilde;es acima</td>
					</tr>
					<tr style="width:800px;margin-top:70px">
						<td style="width:250px">&nbsp;</td>
						<td style="width:300px;text-align:center;border-top:1px solid">Assinatura</td>
						<td style="width:250px">&nbsp;</td>
					</tr>
<?				}
?>                
			</table>			
<?			$x ++;
			$p ++;
		}
?>        

	</body>
</html>