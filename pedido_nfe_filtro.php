<?php

	$statusFiltro = fnc_sistema('pedido_filtro'); 
	
	// pra pegar os valores padrao do bd
	$_SESSION['sel_pedido_nfe_status_pagamento'] =  (isset($_SESSION['sel_pedido_nfe_status_pagamento'])) ? $_SESSION['sel_pedido_nfe_status_pagamento'] : $statusFiltro;

	$_SESSION['txt_data1'] 	= (isset($_SESSION['txt_data1']))	? $_SESSION['txt_data1'] : date('d') . '/' . date('m/Y', mktime(0, 0, 0, date('m') - 3));
	$_SESSION['txt_data2'] 	= (isset($_SESSION['txt_data2']))	? $_SESSION['txt_data2'] : date('d/m/Y');
	
	//memorizar os filtros para exibição nos selects
	if(isset($_POST['btn_limpar'])){
		$_SESSION['txt_pedido_nfe_ref_venda'] 		 = "";
		$_SESSION['txt_pedido_nfe_cod_venda'] 		 = "";
		$_SESSION['txt_pedido_nfe_numero'] 			 = "";
		$_SESSION['sel_pedido_nfe_status_pagamento'] = $statusFiltro;
		$_SESSION['txt_pedido_nfe_cliente'] 		 = "";
		$_SESSION['sel_pedido_nfe_funcionario'] 	 = "";
		$_SESSION['sel_pedido_nfe_marcador'] 		 = "";
		$_SESSION['txt_pedido_nfe_data1'] 			 = date('d') . '/' . date('m/Y', mktime(0, 0, 0, date('m') - 3));
		$_SESSION['txt_pedido_nfe_data2'] 			 = date('d/m/Y');
		$_POST['chk_cliente']						 = false;
		$_SESSION['sel_pedido_nfe_fatura'] 			 = "";
		$_SESSION['sel_pedido_nfe_convertidas'] 	 = "";
	}else{
		$_SESSION['txt_pedido_nfe_ref_venda'] 		= (isset($_POST['txt_pedido_nfe_ref_venda']) 		? $_POST['txt_pedido_nfe_ref_venda'] 		: $_SESSION['txt_pedido_nfe_ref_venda']);
		$_SESSION['txt_pedido_nfe_cod_venda'] 		= (isset($_POST['txt_pedido_nfe_cod_venda'])		? $_POST['txt_pedido_nfe_cod_venda']		: $_SESSION['txt_pedido_nfe_cod_venda']);
		$_SESSION['txt_pedido_nfe_numero'] 			= (isset($_POST['txt_pedido_nfe_numero'])			? $_POST['txt_pedido_nfe_numero']			: $_SESSION['txt_pedido_nfe_numero']);
		$_SESSION['txt_pedido_nfe_cliente'] 		= (isset($_POST['txt_pedido_nfe_cliente']) 			? $_POST['txt_pedido_nfe_cliente']			: $_SESSION['txt_pedido_nfe_cliente']);
		$_SESSION['sel_pedido_nfe_funcionario'] 	= (isset($_POST['sel_pedido_nfe_funcionario']) 		? $_POST['sel_pedido_nfe_funcionario'] 		: $_SESSION['sel_pedido_nfe_funcionario']);
		$_SESSION['txt_pedido_nfe_data1'] 			= (isset($_POST['txt_pedido_nfe_data1']) 			? $_POST['txt_pedido_nfe_data1'] 			: $_SESSION['txt_pedido_nfe_data1']);
		$_SESSION['txt_pedido_nfe_data2'] 			= (isset($_POST['txt_pedido_nfe_data2']) 			? $_POST['txt_pedido_nfe_data2'] 			: $_SESSION['txt_pedido_nfe_data2']);
		$_SESSION['sel_pedido_nfe_status_pagamento']= (isset($_POST['sel_pedido_nfe_status_pagamento']) ? $_POST['sel_pedido_nfe_status_pagamento'] : $_SESSION['sel_pedido_nfe_status_pagamento']);
		$_SESSION['sel_pedido_nfe_marcador'] 		= (isset($_POST['sel_pedido_nfe_marcador']) 		? $_POST['sel_pedido_nfe_marcador'] 		: $_SESSION['sel_pedido_nfe_marcador']);
		$_SESSION['sel_pedido_nfe_fatura'] 			= (isset($_POST['sel_pedido_nfe_fatura'])			? $_POST['sel_pedido_nfe_fatura']			: $_SESSION['sel_pedido_nfe_fatura']);
		$_SESSION['sel_pedido_nfe_convertidas'] 	= (isset($_POST['sel_pedido_nfe_convertidas'])		? $_POST['sel_pedido_nfe_convertidas']		: $_SESSION['sel_pedido_nfe_convertidas']);
	}

?>
<form id="frm-filtro" action="index.php?p=pedido_nfe" method="post">
	<fieldset>
  	<legend>Buscar por:</legend>
    <ul>
		<li style="height:40px">
<?			$codigo_pedido = ($_SESSION['txt_pedido_nfe_cod_venda'] ? $_SESSION['txt_pedido_nfe_cod_venda'] : "c&oacute;digo");
			($_SESSION['txt_pedido_nfe_cod_venda'] == "código") ? $_SESSION['txt_pedido_nfe_cod_venda'] = '' : '';
?>      	<input style="width:80px" type="text" name="txt_pedido_nfe_cod_venda" id="txt_pedido_nfe_cod_venda" onfocus="limpar (this,'c&oacute;digo');" onblur="mostrar (this, 'c&oacute;digo');" value="<?=$codigo_pedido?>"/>
		</li>
		<li style="height:40px">
<?			$numero_nota = ($_SESSION['txt_pedido_nfe_numero'] ? $_SESSION['txt_pedido_nfe_numero'] : "NFe");
			($_SESSION['txt_pedido_nfe_numero'] == "NFe") ? $_SESSION['txt_pedido_nfe_numero'] = '' : '';
?>      	<input style="width: 80px" type="text" name="txt_pedido_nfe_numero" id="txt_pedido_nfe_numero" onfocus="limpar (this,'NFe');" onblur="mostrar (this, 'NFe');" value="<?=$numero_nota?>"/>
		</li>
		<li style="height:40px">
            <select id="sel_pedido_nfe_status_pagamento" name="sel_pedido_nfe_status_pagamento" style="width: 115px" >
                <option <?=($_SESSION['sel_pedido_nfe_status_pagamento'] == "aberto") 	? 'selected="selected"' : '' ?> value="aberto">em aberto</option>
                <option <?=($_SESSION['sel_pedido_nfe_status_pagamento'] == "total") 	? 'selected="selected"' : '' ?> value="total">pago total</option>
                <option <?=($_SESSION['sel_pedido_nfe_status_pagamento'] == "parcial") 	? 'selected="selected"' : '' ?> value="parcial">pago parcial</option>
                <option <?=($_SESSION['sel_pedido_nfe_status_pagamento'] == "vencidos") ? 'selected="selected"' : '' ?> value="vencidos">vencidos</option>
                <option <?=($_SESSION['sel_pedido_nfe_status_pagamento'] == "todos") 	? 'selected="selected"' : '' ?> value="todos">todos</option>
			</select>
		</li>
		<li style="height:40px">
        	<input type="checkbox" name="chk_cliente" id="chk_cliente" <?=(isset($_POST['chk_cliente']) && $_POST['chk_cliente'] == true) ? print 'checked ="checked"' : ''?>/>
<?			$cliente = ($_SESSION['txt_pedido_nfe_cliente'] 	? $_SESSION['txt_pedido_nfe_cliente'] : "cliente");
			($_SESSION['txt_pedido_nfe_cliente'] == "cliente") 	? $_SESSION['txt_pedido_nfe_cliente'] = '' : '';
?>      	<input style="width: 200px" type="text" name="txt_pedido_nfe_cliente" id="txt_pedido_nfe_cliente" onfocus="limpar (this,'cliente');" onblur="mostrar (this, 'cliente');" value="<?=$cliente?>"/>
			<small>marque para qualquer parte do campo</small>
        </li>
		<li style="height:40px">
            <select style="width:120px" id="sel_pedido_nfe_funcionario" name="sel_pedido_nfe_funcionario" >
                <option value="">Funcion&aacute;rio</option>
<?				$rsFuncionario = mysql_query("select * from tblfuncionario ORDER BY fldNome ASC");
				while($rowFuncionario= mysql_fetch_array($rsFuncionario)){
?>					<option <?=($_SESSION['sel_pedido_nfe_funcionario'] == $rowFuncionario['fldId']) ? 'selected="selected"' : '' ?> value="<?= $rowFuncionario['fldId'] ?>"><?= $rowFuncionario['fldNome'] ?></option>
<?				}
?>			</select>
		</li>
		<li style="height:40px">
      		<label for="txt_pedido_nfe_data1">Per&iacute;odo: </label>
<?			$data1 = ($_SESSION['txt_pedido_nfe_data1'] ? $_SESSION['txt_pedido_nfe_data1'] : "");
?>     		<input title="Data inicial" style="text-align:center;width: 65px" type="text" name="txt_pedido_nfe_data1" id="txt_pedido_nfe_data1" class="calendario-mask" value="<?=$data1?>"/>
      	</li>
		<li style="height:40px">
<?			$data2 = ($_SESSION['txt_pedido_nfe_data2'] ? $_SESSION['txt_pedido_nfe_data2'] : "");
?>     		<input title="Data final" style="text-align:center;width: 65px" type="text" name="txt_pedido_nfe_data2" id="txt_pedido_nfe_data2" class="calendario-mask" value="<?=$data2?>"/>
			<a href="calendario_periodo,<?=format_date_in($data1) . ',' . format_date_in($data2)?>,pedido_nfe" id="exibir-calendario" title="Exibir calend&aacute;rio" class="modal calendario-modal" rel="600-320"></a>
      	</li>
        <li>
<?			$referencia_pedido = ($_SESSION['txt_pedido_nfe_ref_venda'] ? $_SESSION['txt_pedido_nfe_ref_venda'] : "referencia");
			($_SESSION['txt_pedido_nfe_ref_venda'] == "referencia") ? $_SESSION['txt_pedido_nfe_ref_venda'] = '' : '';
?>      	<input style="width: 80px" type="text" name="txt_pedido_nfe_ref_venda" id="txt_pedido_ref_venda" onfocus="limpar (this,'referencia');" onblur="mostrar (this, 'referencia');" value="<?=$referencia_pedido?>"/>
		</li>
        <li>
            <select style="width:185px" id="sel_pedido_nfe_marcador" name="sel_pedido_nfe_marcador" >
                <option value="">Marcador</option>
<?				$rsMarcador = mysql_query("SELECT * FROM tblpedido_marcador WHERE fldExcluido = 0 ORDER BY fldMarcador ASC");
				while($rowMarcador = mysql_fetch_array($rsMarcador)){
?>					<option <?=($_SESSION['sel_pedido_nfe_marcador'] == $rowMarcador['fldId']) ? 'selected="selected"' : '' ?> value="<?= $rowMarcador['fldId'] ?>"><?= $rowMarcador['fldMarcador'] ?></option>
<?				}
?>			</select>
		</li>
        <li>
            <select id="sel_pedido_nfe_fatura" name="sel_pedido_nfe_fatura" style="width: 80px" >
                <option value="">fatura</option>
                <option <?=($_SESSION['sel_pedido_nfe_fatura'] == "1") ? 'selected="selected"' : '' ?> value="1">faturado</option>
                <option <?=($_SESSION['sel_pedido_nfe_fatura'] == "0") ? 'selected="selected"' : '' ?> value="0">não faturado</option>
			</select>
		</li>
        <li>
            <select id="sel_pedido_nfe_convertidas" name="sel_pedido_nfe_convertidas" style="width: 130px" >
                <option value="">todas as notas</option>
                <option <?=($_SESSION['sel_pedido_nfe_convertidas'] == "diretas") ? 'selected="selected"' : '' ?> value="diretas">notas diretas</option>
                <option <?=($_SESSION['sel_pedido_nfe_convertidas'] == "importadas") ? 'selected="selected"' : '' ?> value="importadas">notas importadas</option>
			</select>
		</li>
        <li style="float:right;">
	        <button type="submit" name="btn_exibir" style="margin:0" title="Exibir">Exibir</button>
        </li>
        <li style="float:right;">
        	<button type="submit" name="btn_limpar" style="margin:0" title="Limpar Filtro">Limpar filtro</button>
        </li>
  </ul>
  </fieldset>
</form>

<?
	
	if(format_date_in($_SESSION['txt_pedido_nfe_data1']) != "" || format_date_in($_SESSION['txt_pedido_nfe_data2']) != ""){
		if($_SESSION['txt_pedido_nfe_data1'] != "" && $_SESSION['txt_pedido_nfe_data2'] != ""){
			$filtro .= " AND tblpedido.fldPedidoData between '".format_date_in($_SESSION['txt_pedido_nfe_data1'])."' AND '".format_date_in($_SESSION['txt_pedido_nfe_data2'])."'";
		}elseif($_SESSION['txt_pedido_nfe_data1'] != "" && $_SESSION['txt_pedido_nfe_data2'] == ""){
			$filtro .= " AND tblpedido.fldPedidoData >= '".format_date_in($_SESSION['txt_pedido_nfe_data1'])."'";
		}elseif($_SESSION['txt_pedido_nfe_data2'] != "" && $_SESSION['txt_pedido_nfe_data1'] == ""){
			$filtro .= " AND tblpedido.fldPedidoData <= '".format_date_in($_SESSION['txt_pedido_nfe_data2'])."'";
		}
	}
	
	if(($_SESSION['txt_pedido_nfe_cod_venda']) != ""){
		$filtro .= " and tblpedido.fldId = '".$_SESSION['txt_pedido_nfe_cod_venda']."'";
	}
	
	if(($_SESSION['txt_pedido_nfe_ref_venda']) != ""){
		
		$filtro .= " AND fldReferencia = '".$_SESSION['txt_pedido_nfe_ref_venda']."'";
	}
	
	if(($_SESSION['txt_pedido_nfe_numero']) != ""){
		$filtro .= " and tblpedido_fiscal.fldNumero = '".$_SESSION['txt_pedido_nfe_numero']."'";
	}
	
	if(($_SESSION['sel_pedido_nfe_fatura']) != ""){
		
		$filtro .= "and fldFaturado = '".$_SESSION['sel_pedido_nfe_fatura']."'";
	}
	
	if(($_SESSION['sel_pedido_nfe_convertidas']) != ""){
		switch($_SESSION['sel_pedido_nfe_convertidas']){
			case "diretas":
				$filtro .= "AND tblpedido.fldId NOT IN (SELECT fldPedido_Destino_Nfe_Id FROM tblpedido WHERE tblpedido.fldPedido_Destino_Nfe_Id > 0)";
				break;
			case "importadas":
				$filtro .= "AND tblpedido.fldId IN (SELECT fldPedido_Destino_Nfe_Id FROM tblpedido WHERE tblpedido.fldPedido_Destino_Nfe_Id > 0)";
				break;
		}
	}
	
	if(($_SESSION['sel_pedido_nfe_status_pagamento']) != ""){
		//$filtro_excluido .= " and (tblpedido_nfe_parcela.fldExcluido is null or tblpedido_nfe_parcela.fldExcluido = 0)";
		switch($_SESSION['sel_pedido_nfe_status_pagamento']){
			case "aberto":
				$filtro_status = " HAVING (fldValorBaixa = 0 or fldValorBaixa is NULL)";
			break;
			
			case "total":
				$filtro_status = " HAVING fldValorBaixa >= fldTotalParcelas";
			break;

			case "parcial":
				$filtro_status = " HAVING fldTotalParcelas > fldValorBaixa AND fldValorBaixa > 0";
			break;

			case "vencidos":
				$filtro_status = " HAVING ((fldTotalParcelas > fldValorBaixa) or fldValorBaixa IS NULL) AND fldVencimento < '".date("Y-m-d")."'";
			break;

			case "todos":
				$filtro_status = "";
			break;
		}
	}
	
	if(($_SESSION['txt_pedido_nfe_cliente']) != ""){
		$cliente = addslashes($_SESSION['txt_pedido_nfe_cliente']); // no caso de aspas, pra nao dar erro na consulta
		if($_POST['chk_cliente'] == true){
			$filtro .= "AND (tblNome1.fldNome like '%$cliente%'	OR tblNome1.fldNomeFantasia like '%$cliente%' OR tblNome2.fldNome LIKE '%$cliente%'	OR tblNome2.fldNomeFantasia LIKE '%$cliente%')";
		}else{
			$filtro .= "AND (tblNome1.fldNome like '$cliente%' 	OR tblNome1.fldNomeFantasia like '$cliente%'  OR tblNome2.fldNome LIKE '$cliente%' 	OR tblNome2.fldNomeFantasia LIKE '$cliente%')";
		}
	}
				
	if(($_SESSION['sel_pedido_nfe_funcionario']) != ""){
		$filtro .= "and tblpedido_funcionario_servico.fldFuncionario_Id = '".$_SESSION['sel_pedido_nfe_funcionario']."'";
	}
	
	if(($_SESSION['sel_pedido_nfe_marcador']) != ""){
		$filtro .= "and tblpedido.fldMarcador_Id = '".$_SESSION['sel_pedido_marcador']."'";
	}
	
	$filtro = $filtro." GROUP BY tblpedido.fldId ".$filtro_status;
	
	//transferir para a sessão
	$_SESSION['filtro_pedido'] = $filtro;

?>