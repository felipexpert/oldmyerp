       
<?
	require("cliente_pedido_filtro.php");

/**************************** ORDER BY *******************************************/
	$filtroOrder  = 'tblpedido.fldId ';
	$class 		  = 'asc';
	$order_sessao = explode(" ", $_SESSION['order_cliente_pedido']);
	if(isset($_GET['order'])){
		switch($_GET['order']){
			
			case 'codigo'		:  $filtroOrder = "tblpedido.fldId";   			break;
			case 'nota'			:  $filtroOrder = "fldNotaNumeros";				break;
			case 'funcionario'	:  $filtroOrder = "fldFuncionarioNome";		 	break;
			case 'data'			:  $filtroOrder = "tblpedido.fldPedidoData";	break;
			case 'placa'		:  $filtroOrder = "tblpedido.fldVeiculo_Id";	break;
		}
		if($order_sessao[0] == $filtroOrder){
			$class = ($order_sessao[1] == 'asc') ? 'desc' : 'asc';
		}
	}
	
	//definir icone para ordem
	$_SESSION['order_cliente_pedido'] = (!$_SESSION['order_cliente_pedido'] || $_GET['order']) ? $filtroOrder.' '.$class : $_SESSION['order_cliente_pedido'];
	$pag	= ($_GET['pagina'])? '&pagina='.$_GET['pagina'] : ''; 
	$raiz	= "index.php?p=cliente_detalhe&id=$cliente_id&&modo=pedido&aba=listar$pag&amp;order=";
	
	$order_sessao = explode(" ", $_SESSION['order_cliente_pedido']);
	$filtroOrder  = $order_sessao[0]; //pra poder comparar na listagem e exibir a class
	
/**************************** PAGINA��O *******************************************/
	
	$sSQL = "SELECT 
			tblNota1.fldnumero as fldNumero,
			tblNota2.fldnumero as fldNotaDestino,
			tblpedido.fldTipo_Id,
			tblpedido.fldDesconto,
			tblpedido.fldDescontoReais,
			tblpedido.fldPedidoData,
			tblpedido.fldComissao,
			tblpedido.fldTipo_Id,
			tblpedido.fldVeiculo_Id,
			tblpedido.fldVeiculo_Km,
			tblpedido.fldValor_Terceiros,
			tblpedido.fldComanda_Numero,
			tblpedido.fldObservacao AS fldPedidoObs,
			tblpedido.fldPedido_Destino_Nfe_Id,
			tblpedido.fldId AS fldPedidoId, 
			tblpedido.fldStatus AS fldStatus, 
			tblpedido_parcela.fldValor, 
			tblpedido_parcela.fldVencimento, 
			SUM(tblpedido_parcela_baixa.fldValor	* (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldValorBaixa,
			SUM(tblpedido_parcela_baixa.fldDesconto * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldBaixaDesconto,
			
			(SELECT fldnumero FROM tblpedido_fiscal WHERE fldPedido_Id = tblpedido.fldId 
			 UNION
			 SELECT fldnumero FROM tblpedido_fiscal WHERE fldPedido_Id in (tblpedido.fldPedido_Destino_Nfe_Id )) as fldNotaNumeros,
			 
			(SELECT SUM(fldValor) FROM
				tblpedido_funcionario_servico WHERE tblpedido_funcionario_servico.fldFuncao_Tipo = 2 
				AND tblpedido_funcionario_servico.fldPedido_Id = tblpedido.fldId) AS fldValorServico,
			
			(SELECT fldNome FROM
				tblpedido_funcionario_servico LEFT JOIN tblfuncionario ON tblpedido_funcionario_servico.fldFuncionario_Id = tblfuncionario.fldId
				WHERE tblpedido_funcionario_servico.fldFuncao_Tipo = 1 
				AND tblpedido_funcionario_servico.fldPedido_Id = tblpedido.fldId LIMIT 1) AS fldFuncionarioNome,
			
			(SELECT tblpedido_parcela.fldVencimento FROM tblpedido_parcela
				LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
				WHERE tblpedido_parcela.fldExcluido <> 1 AND
				tblpedido_parcela.fldPedido_Id = tblpedido.fldId
				ORDER BY fldVencimento LIMIT 1) AS fldVencimento,
			
			SUM(tblpedido_parcela.fldValor	* (tblpedido_parcela.fldExcluido * -1 + 1)) AS fldTotalParcelas,
			
			tblcliente.fldNome  AS fldNomeCliente,
			tblcliente.fldCPF_CNPJ,
			tblcliente.fldTipo as fldCPF_Tipo
		FROM (tblpedido_parcela LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id) 
			RIGHT JOIN tblpedido ON tblpedido.fldId 					= tblpedido_parcela.fldPedido_Id 
			LEFT JOIN tblpedido_funcionario_servico ON tblpedido.fldId 	= tblpedido_funcionario_servico.fldPedido_Id 
			
			LEFT JOIN tblpedido_fiscal tblNota1 ON tblpedido.fldId 							= tblNota1.fldpedido_id 
			LEFT JOIN tblpedido_fiscal tblNota2 ON tblpedido.fldPedido_Destino_Nfe_Id 		= tblNota2.fldpedido_id 
			
			INNER JOIN tblcliente ON tblpedido.fldCliente_Id 			= tblcliente.fldId 
			WHERE tblpedido.fldExcluido = 0 AND tblpedido.fldCliente_Id = '$cliente_id'
		". $_SESSION['filtro_cliente_pedido']." ORDER BY " . $_SESSION['order_cliente_pedido'];
			
			
	$rsTotal = mysql_query($sSQL);
	echo mysql_error();
	$rowsTotal = mysql_num_rows($rsTotal);
	$_SESSION['cliente_pedido_relatorio'] = $sSQL;
	
	//defini��o dos limites
	$limite = 90;
	$n_paginas = 7;
	$total_paginas = ceil(mysql_num_rows($rsTotal) / $limite);
	
	if(isset($_GET["pagina"]) && $_GET["pagina"] > $total_paginas){
		$inicio = 0;
	}elseif(isset($_GET['pagina'])){
		$inicio = ($_GET['pagina'] - 1) * $limite;
	}else{
		$inicio = 0;
	}
	
	$sSQL .= " limit " . $inicio . "," . $limite;
	$rsPedido = mysql_query($sSQL);
	$pagina = ($_GET['pagina'] ? $_GET['pagina'] : "1");
	
#########################################################################################

?>
    <form class="table_form" id="frm_pedido" action="" method="post">
    	<div id="table">
            <div id="table_cabecalho">
                <ul class="table_cabecalho">
                    <li class="order" style="width:55px">
                    	<a <?= ($filtroOrder == 'tblpedido.fldId') 				? "class='$class'" : '' ?> style="width:35px" href="<?=$raiz?>&order=codigo">C&oacute;d.</a>
                    </li>
                    <li class="order" style="width:40px">
<?					if($_SESSION["sistema_nfe"] > 0){                    
?>						<a <?= ($filtroOrder == 'fldNotaNumeros') 				? "class='$class'" : '' ?> style="width:25px" href="<?=$raiz?>&order=nota">NFe</a>
<?					}
?>                  </li>
					<li class="order" style="width:150px">
                    	<a <?= ($filtroOrder == 'fldFuncionarioNome') 			? "class='$class'" : '' ?> style="width:135px" href="<?=$raiz?>&order=funcionario">Funcion&aacute;rio</a>
                    </li>
                    <li style="width:285px">Descri&ccedil;&atilde;o</li>
<?					if($_SESSION['sistema_tipo'] == 'automotivo'){
?>						<li class="order" style="width:70px; text-align:right">
                            <a <?= ($filtroOrder == 'tblpedido.fldVeiculo_Id') 	? "class='$class'" : '' ?> style="width:55px" href="<?=$raiz?>&order=placa">Placa</a>
                        </li>
						<li style="width:70px; text-align:right">KM</li>
<?					}else{				
?>                  	<li style="width:140px">Observa&ccedil;&atilde;o</li>
<?					}
?>					<li class="order" style="width:60px">
                    	<a <?= ($filtroOrder == 'tblpedido.fldPedidoData') 		? "class='$class'" : '' ?> style="width:45px" href="<?=$raiz?>&order=data">Emiss&atilde;o</a>
                    </li>
                    <li style="width:40px; text-align:right">Itens</li>
                    <li style="width:60px; text-align:right">Total</li>
                    <li style="width:30px; padding-left:5px">Parc.</li>
                    <li style="width:40px">&nbsp;</li>
                </ul>
            </div>
            <div id="table_container">       
                <table id="table_general" class="table_general" summary="Lista de pedidos">
                	<tbody>
<?					
                        $id_array = array();
                        $n = 0;
                        
                        $linha 	= "row";
                        $rows 	= mysql_num_rows($rsPedido);
                        while($rowPedido = mysql_fetch_array($rsPedido)){
                          
                            $item = 0;
                            $subTotalPedido = 0;
                            $id_array[$n] 	= $rowPedido["fldPedidoId"];
                            $n 				+= 1;
                            
                            $rsParcela 			= mysql_query("SELECT * FROM tblpedido_parcela WHERE fldPedido_Id = '".$rowPedido['fldPedidoId']."'");
                            $rowParcela 		= mysql_num_rows($rsParcela);
                            $descricaoPedido	= '';
                            $rsItem 			= mysql_query("SELECT * FROM tblpedido_item WHERE fldPedido_Id = ".$rowPedido['fldPedidoId']);
                            while($rowItem 		= mysql_fetch_array($rsItem)){
                            
                                $valor_item 	= $rowItem['fldValor'];
                                $qtd			= $rowItem['fldQuantidade'];
                                $total_item 	= $valor_item * $qtd;
                                $desconto		= $rowItem['fldDesconto'];
                                $totalDesconto 	= ($total_item * $desconto)/100;
                                $totalValor 	= $total_item - $totalDesconto;
                                
                                //total pedido
                                $subTotalPedido += $totalValor;
                                $item 			+= $rowItem['fldQuantidade'];	
								$descricaoPedido .= $rowItem['fldDescricao'].", ";
                            }
							
                            $total_itens 	 = $item;
                            $descPedido 	 = $rowPedido['fldDesconto'];
                            $descPedidoReais = $rowPedido['fldDescontoReais'];
                            $descontoPedido  = ($subTotalPedido * $descPedido) / 100;
                            $totalPedido 	 = $subTotalPedido 	- $descontoPedido;
                            $totalPedido	 = $totalPedido 	- $descPedidoReais;
							
							$rowVeiculo = mysql_fetch_array(mysql_query("SELECT fldPlaca FROM tblcliente_veiculo WHERE fldId =".fncNullId($rowPedido['fldVeiculo_Id'])));
							if(isset($rowPedido['fldNotaDestino'])){
								$numero_nota = $rowPedido['fldNotaDestino'];
							}else{
								$numero_nota = $rowPedido['fldNumero'];
							}
							//linkar para o detalhe da venda ou NFe
							$paginaLink 	 = redirecionarPagina($rowPedido['fldTipo_Id'], 'venda');
						
?>							<tr class="<?= $linha; ?>">
                                <td style="width:55px;"><span title="<?= fnc_status_pedido($rowPedido['fldPedidoId'], "tblpedido")?>" class="<?=fnc_status_pedido($rowPedido['fldPedidoId'], "tblpedido")?>"><?=str_pad($rowPedido['fldPedidoId'], 4, "0", STR_PAD_LEFT)?></span></td>
<?								if($_SESSION["sistema_nfe"] > 0){                    
?>                               	<td	style="width:35px; padding-right:3px; text-align:center; border: 1px solid #ddd; border-bottom: none; border-top: none; font-weight: bold; color:#C90"><?=str_pad($numero_nota, 4, "0", STR_PAD_LEFT)?></td>
<?								} else{
?>									<td	style="width:40px"></td>
<?								}
?>                            	<td style="width:150px"><?=substr($rowPedido['fldFuncionarioNome'],0,20)?></td>
                                <td style="width:285px"><?=substr($descricaoPedido,0,110)?></td>
<?								if($_SESSION['sistema_tipo'] == 'automotivo'){
?>									<td style="width:70px; text-align:right"><?=$rowVeiculo['fldPlaca']?></td>
									<td style="width:70px; text-align:right"><?=$rowPedido['fldVeiculo_Km']?></td>
<?								}else{				
?>                              	<td style="width:140px"><?=substr($rowPedido['fldPedidoObs'],0,18)?></td>
<?								}
?>								<td style="width:60px; text-align:right"><?=format_date_out3($rowPedido['fldPedidoData'])?></td>
                                <td style="width:40px; text-align:right"><?=format_number_out($total_itens)?></td>
                                <td style="width:60px; text-align:right"><?=format_number_out($totalPedido)?></td>
                                <td style="width:30px; padding-right:5px;text-align:right"><?=str_pad($rowParcela, 2, "0", STR_PAD_LEFT);?></td>
                                <td style="width:15px">
<?									if($rowPedido['fldPedido_Destino_Nfe_Id'] == 0 && $rowPedido['fldTipo_Id'] == 1){                                
?>                                		<a class="btn_duplicar modal" href="pedido_duplica_tipo,<?=$rowPedido['fldPedidoId']?>" rel="380-130" title="duplicar venda"></a>
<?									}
?>								</td>
                                <td style="width:15px">
                              		<a class="edit" href="index.php?p=<?=$paginaLink;?>&id=<?=$rowPedido['fldPedidoId']?>" title="editar"></a></td>
								</td>
                       		</tr>
<?                     		$linha = ($linha == "row") ? "dif-row" : "row";
                		}
?>		 			</tbody>
				</table>
            </div>
           <div id="table_action">
                <ul id="action_button">
                	<li><button id="btn_print" name="btn_action" type="submit" value="imprimir" title="Imprimir relat&oacute;rio de registro(s) selecionado(s)" onclick="window.open('cliente_pedido_relatorio.php?id=<?=$_GET['id'];?>','_blank')"></button></li>
                </ul>
        	</div>
            <div id="table_paginacao">
<?				$paginacao_destino = "?p=cliente_detalhe&id=$cliente_id&modo=pedido&aba=listar";
				include("paginacao.php")
?>          </div>   
            <div class="table_registro">
            	<span>Exibindo registros <?=($pagina*$limite-$limite+1).' a '.($pagina*$limite-$limite+$rows)?> do total de <?=$rowsTotal?></span>
            </div>     
        </div>
	</form>
    <ul class="status_legenda">
    	<li><span class="orcamento">or&ccedil;amento</span></li>
        <li><span class="andamento">em andamento</span></li>
        <li><span class="finalizado">finalizado</span></li>
        <li><span class="entregue">entregue</span></li>
        <li><span class="recusado">recusado</span></li>
        <li><span class="convertida" style="font-weight:normal">convertidas</span></li>
        <li><span class="nfe" style="font-weight:normal">NFex</span></li>
    </ul>