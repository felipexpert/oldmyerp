<?
	
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');
	
	//classes html para destacar certos dias
	$eventoClass	= 'class="evento"';
	
	//para não ficar repetindo, ainda mais, a consulta sql para preencher o calendário
	function preencherCalendario($dataInicial, $dataFinal) {
		//contas programadas
		$sSQLContaProgramada = "SELECT tblfinanceiro_conta_pagar_programada.*, tblsistema_calendario_intervalo.fldNome AS fldCalendarioNome, tblsistema_calendario_intervalo.fldValor AS StringTime, tblfinanceiro_conta_fluxo_marcador.fldMarcador AS Marcador, tblpagamento_tipo.fldTipo AS TipoPagamento
								FROM tblfinanceiro_conta_pagar_programada
								INNER JOIN tblsistema_calendario_intervalo ON tblfinanceiro_conta_pagar_programada.fldIntervalo_Tipo = tblsistema_calendario_intervalo.fldId
								INNER JOIN tblfinanceiro_conta_fluxo_marcador ON tblfinanceiro_conta_pagar_programada.fldMarcador = tblfinanceiro_conta_fluxo_marcador.fldId
								INNER JOIN tblpagamento_tipo ON tblfinanceiro_conta_pagar_programada.fldPagamento_Id = tblpagamento_tipo.fldId 
								WHERE tblfinanceiro_conta_pagar_programada.fldDisabled = '0' AND tblfinanceiro_conta_pagar_programada.fldExcluido = '0' 
								AND (tblfinanceiro_conta_pagar_programada.fldData_Termino >= '0000-00-00' OR tblfinanceiro_conta_pagar_programada.fldData_Termino BETWEEN '$dataInicial' AND '$dataFinal')";
		
		//executando a consulta de contas programadas
		$rowContaProgramada = mysql_query($sSQLContaProgramada);
		
		//colocando os dados em uma matriz
		while($contaProgramada = mysql_fetch_array($rowContaProgramada)){
			
			//montando a string para usar no calculo da função strtotime() - assim para gerar a data de vencimento
			$stringTimeCompleta = '+' . $contaProgramada['fldIntervalo_Frequencia'] . ' ' . $contaProgramada['StringTime'];
			
			//data cadastrada como inicial para a 1º parcela
			if($contaProgramada['fldData_Inicio'] >= $dataInicial){
				$vencimento = $contaProgramada['fldData_Inicio'];
			}
			else{
				$vencimento	= date('Y-m-d',strtotime($stringTimeCompleta, strtotime($contaProgramada['fldData_Inicio'])));
			}
			
			while($vencimento <= $dataFinal){
				
				if($vencimento >= $dataInicial and ($vencimento <= $contaProgramada['fldData_Termino'] or $contaProgramada['fldData_Termino'] == '0000-00-00')){
					
					//procedimento para exibir contas programadas que não foram dado baixa
					$sSQLBaixaProgramada = "SELECT fldId, fldValor FROM tblfinanceiro_conta_pagar_programada_baixa WHERE fldContaProgramada_Id = " . $contaProgramada['fldId'] . " AND fldVencimento = '" . $vencimento . "'";
					$baixaProgramada = mysql_num_rows(mysql_query($sSQLBaixaProgramada));
					
					if($baixaProgramada == 0){
						
						$contasCalendario[$x]['vencimento'] = $vencimento;
						
					}
					
				}
				
				//incrementando a data para seguir no loop mais interno
				$vencimento	= date('Y-m-d', strtotime($stringTimeCompleta, strtotime($vencimento)));
				
				//incrementando o indice
				$x++;
				
			}
			
		}
		
		//compras
		$sSQLCompras = "SELECT tblfornecedor.fldId as ForncedorId, tblfornecedor.fldNomeFantasia as FornecedorNome, tblpagamento_tipo.fldTipo as TipoPagamento, COUNT(tblcompra_parcela_baixa.fldValor) AS QtdBaixas, SUM(tblcompra_parcela_baixa.fldValor) AS ValorBaixa, tblcompra_parcela.* FROM tblcompra_parcela
						INNER JOIN tblcompra ON tblcompra_parcela.fldCompra_Id = tblcompra.fldId
						INNER JOIN tblfornecedor ON tblfornecedor.fldId = tblcompra.fldFornecedor_Id
						INNER JOIN tblpagamento_tipo ON tblpagamento_tipo.fldId = tblcompra_parcela.fldPagamento_Id
						LEFT JOIN tblcompra_parcela_baixa ON tblcompra_parcela.fldId = tblcompra_parcela_baixa.fldParcela_Id
						WHERE tblcompra_parcela.fldExcluido = '0' AND tblcompra_parcela.fldVencimento BETWEEN '$dataInicial' AND '$dataFinal' 
						GROUP BY tblcompra_parcela.fldId HAVING ValorBaixa < tblcompra_parcela.fldValor OR QtdBaixas = 0 ORDER BY tblcompra_parcela.fldParcela";
		
		//executando a consulta em compras
		$rowCompra = mysql_query($sSQLCompras);
		
		//colocando os dados em uma matriz
		while($contaFornecedor = mysql_fetch_array($rowCompra)){
			
			$contasCalendario[$x]['vencimento'] = $contaFornecedor['fldVencimento'];
			//incrementando o indice
			$x++;
			
		}
		
		if(sizeof($contasCalendario) < 1){
			
			//caso não haja registros retorna falso para não escrever nada no calendário
			return false;
			
		}
		else{
			
			//'destrinchando' a matriz para devolver outra apenas com as datas de vencimento
			foreach($contasCalendario as $conta){
				foreach($conta as $c){
					$contas[] = $c;
				}
			}
			
		}
		
		unset($contasCalendario, $conta, $c);
		
		return $contas;
		
	}
	
	//para mostrar o mês por extenso
	function meses($a) {
		switch($a) {
			case 1:  $mes = "Janeiro";   	break;
			case 2:  $mes = "Fevereiro"; 	break;
			case 3:  $mes = "Mar&ccedil;o"; break;
			case 4:  $mes = "Abril";     	break;
			case 5:  $mes = "Maio";      	break;
			case 6:  $mes = "Junho";     	break;
			case 7:  $mes = "Julho";     	break;
			case 8:  $mes = "Agosto";    	break;
			case 9:  $mes = "Setembro";  	break;
			case 10: $mes = "Outubro";   	break;
			case 11: $mes = "Novembro";  	break;
			case 12: $mes = "Dezembro";  	break;
		}
		
		return $mes;
	}
	
?>

<div id="calendarios-container">
	
	<div id="calendario-inicial">
		<?
			
			//--------------------------------------------------------------------------------------------------------
			//data parâmetro para navegar entre os meses, vindo via ajax deste mesmo arquivo
			//cal-inicial[0] = anterior ou proximo
			//cal-inicial[1] = data parâmetro para incrementar ou decrementar um mês
			//$_POST['params'] = parâmetro via ajax vindo da página de financeiro_contas_pagar_listar
			
			$data = $dataParametro = (!isset($_POST['cal-inicial'])) ? $_POST['params'][1] : $_POST['cal-inicial'][1];
			
			if(isset($_POST['cal-inicial']) and $_POST['cal-inicial'][0] == 'anterior') {
				$data = date('Y-m-d',strtotime("-1 month", strtotime($dataParametro)));
			}
			elseif(isset($_POST['cal-inicial']) and $_POST['cal-inicial'][0] == 'proximo') {
				$data = date('Y-m-d',strtotime("+1 month", strtotime($dataParametro)));
			}
			
			$data = explode('-', $data);
			$dia = $data[2];
			$mes = $data[1];
			$ano = $data[0];
			
			//parametros para passar a data selecionada no calendario, padrão mês atual, para verificar quais os dias tem contas para pagar
			$dataInicialParam 	= $ano . '-' . $mes . '-' . date("d", mktime(0, 0, 0, date('m'), 1, date('Y')) + 1);
			$dataFinalParam		= $ano . '-' . $mes . '-' . date("d", mktime(0, 0, 0, date('m') + 1, 1, date('Y')) - 1);
			
			$vencimento 		= preencherCalendario($dataInicialParam, $dataFinalParam);
			
			if($vencimento != false){
				$totalRegistros	= count($vencimento);
				
				for($y=0; $y<$totalRegistros; $y++){
					$vencimentoExplode[$y] 	= explode('-', $vencimento[$y]);
					$vencimentoDia[$y] 		= $vencimentoExplode[$y][2];
				}
			}
			
		?>
		
		<table border="0" summary="Calend&aacute;rio" class="calendario_contas">
			<caption><a href="<?='anterior,'.$ano.'-'.$mes.'-'.$dia?>" title="M&ecirc;s anterior" id="cal_inicial-ant">&laquo;</a> <?=meses($mes) . " " . $ano ?> <a href="<?='proximo,'.$ano.'-'.$mes.'-'.$dia?>" title="Pr&oacute;ximo m&ecirc;s" id="cal_inicial-pro">&raquo;</a></caption>
			<thead>
			<tr>
				<th abbr="Domingo" title="Domingo" class="domingo">D</th>
				<th abbr="Segunda" title="Segunda">S</th>
				<th abbr="Ter&ccedil;a" title="Ter&ccedil;a">T</th>
				<th abbr="Quarta" title="Quarta">Q</th>
				<th abbr="Quinta" title="Quinta">Q</th>
				<th abbr="Sexta" title="Sexta">S</th>
				<th abbr="S&aacute;bado" title="S&aacute;bado">S</th>
			</tr>
			</thead>
			<tbody>
			<?php
				
				$data		  = strtotime($mes."/".$dia."/".$ano);
				$diaSemana	  = date('w', strtotime(date('n/\0\1\/Y', $data)));
				$totalDiasMes = date('t', $data);
				
				for($i=1,$d=1;$d<=$totalDiasMes;) {
					
					echo("<tr>");
					
					for($x=1;$x<=7 && $d <= $totalDiasMes;$x++,$i++) {
						
						if ($i > $diaSemana) {
							
							if(@in_array($d, $vencimentoDia)){
								echo('<td '. $eventoClass .'><a class="link-dia" href="'. str_pad($d, 2, "0", STR_PAD_LEFT).'/'.$mes.'/'.$ano .'">'.$d++.'</a></td>');
							}
							else{
								echo('<td><a class="link-dia" href="'. str_pad($d, 2, "0", STR_PAD_LEFT).'/'.$mes.'/'.$ano .'">'.$d++.'</a></td>');
							}
							
						}
						
						else { echo("<td></td>"); }
						
					}
					
					for(;$x<=7;$x++) { echo("<td></td>"); }
					echo("</tr>");
					
				}
				
				unset($data, $diaSemana, $totalDiasMes, $totalRegistros, $i, $x, $y, $d, $vencimento, $vencimentoDia, $ano, $mes, $dia, $vencimentoExplode, $dataParametro);
				
			?>
			</tbody>
		</table>
	</div>
	
	
	
	<div id="calendario-final">
		<?
			//--------------------------------------------------------------------------------------------------------
			//data parâmetro para navegar entre os meses, vindo via ajax deste mesmo arquivo
			//cal-final[0] = anterior ou proximo
			//cal-final[1] = data parâmetro para incrementar ou decrementar um mês
			//$_POST['params'] = parâmetro via ajax vindo da página de financeiro_contas_pagar_listar
			
			$data = $dataParametro = (!isset($_POST['cal-final'])) ? $_POST['params'][2] : $_POST['cal-final'][1];
			
			if(isset($_POST['cal-final']) and $_POST['cal-final'][0] == 'anterior') {
				$data = date('Y-m-d',strtotime( "-1 month", strtotime($dataParametro)));
			}
			elseif(isset($_POST['cal-final']) and $_POST['cal-final'][0] == 'proximo') {
				$data = date('Y-m-d',strtotime( "+1 month", strtotime($dataParametro)));
			}
			
			$data = explode('-', $data);
			$dia = $data[2];
			$mes = $data[1];
			$ano = $data[0];
			
			//parametros para passar a data selecionada no calendario, padrão mês atual, para verificar quais os dias tem contas para pagar
			$dataInicialParam 	= $ano . '-' . $mes . '-' . date("d", mktime(0, 0, 0, date('m'), 1, date('Y')) + 1);
			$dataFinalParam		= $ano . '-' . $mes . '-' . date("d", mktime(0, 0, 0, date('m') + 1, 1, date('Y')) - 1);
			
			$vencimento 		= preencherCalendario($dataInicialParam, $dataFinalParam);
			
			if($vencimento != false){
				$totalRegistros	= count($vencimento);
				
				for($y=0; $y<$totalRegistros; $y++){
					$vencimentoExplode[$y] 	= explode('-', $vencimento[$y]);
					$vencimentoDia[$y] 		= $vencimentoExplode[$y][2];
				}
			}
			
		?>
		
		<table border="0" summary="Calend&aacute;rio" class="calendario_contas">
			<caption><a href="<?='anterior,'.$ano.'-'.$mes.'-'.$dia?>" title="M&ecirc;s anterior" id="cal_final-ant">&laquo;</a> <?=meses($mes) . " " . $ano ?> <a href="<?='proximo,'.$ano.'-'.$mes.'-'.$dia?>" title="Pr&oacute;ximo m&ecirc;s" id="cal_final-pro">&raquo;</a></caption>
			<thead>
			<tr>
				<th abbr="Domingo" title="Domingo" class="domingo">D</th>
				<th abbr="Segunda" title="Segunda">S</th>
				<th abbr="Ter&ccedil;a" title="Ter&ccedil;a">T</th>
				<th abbr="Quarta" title="Quarta">Q</th>
				<th abbr="Quinta" title="Quinta">Q</th>
				<th abbr="Sexta" title="Sexta">S</th>
				<th abbr="S&aacute;bado" title="S&aacute;bado">S</th>
			</tr>
			</thead>
			<tbody>
			<?php
				
				$data		  = strtotime($mes."/".$dia."/".$ano);
				$diaSemana	  = date('w', strtotime(date('n/\0\1\/Y', $data)));
				$totalDiasMes = date('t', $data);
				
				for($i=1,$d=1;$d<=$totalDiasMes;) {
					
					echo("<tr>");
					
					for($x=1;$x<=7 && $d <= $totalDiasMes;$x++,$i++) {
						
						if ($i > $diaSemana) {
							
							if(@in_array($d, $vencimentoDia)){
								echo('<td '. $eventoClass .'><a class="link-dia" href="'. str_pad($d, 2, "0", STR_PAD_LEFT).'/'.$mes.'/'.$ano .'">'.$d++.'</a></td>');
							}
							else{
								echo('<td><a class="link-dia" href="'. str_pad($d, 2, "0", STR_PAD_LEFT).'/'.$mes.'/'.$ano .'">'.$d++.'</a></td>');
							}
							
						}
						
						else { echo("<td></td>"); }
						
					}
					
					for(;$x<=7;$x++) { echo("<td></td>"); }
					echo("</tr>");
					
				}
				
				unset($data, $diaSemana, $totalDiasMes, $totalRegistros, $i, $x, $y, $d, $vencimento, $vencimentoDia, $ano, $mes, $dia, $vencimentoExplode, $dataParametro);
				
			?>
			</tbody>
		</table>
	</div>
	
	<form action="?p=financeiro_contas_pagar" method="post" class="frm_detalhe" style=" width: 320px; clear: both; float: none; margin: 0 0 0 -160px; position: absolute; left: 50%; bottom: 0;">
	
		<ul>
			<li>
				<input type="text" id="txt_calendario_data_inicial" name="txt_calendario_data_inicial" value="<?=format_date_out($_POST['params'][1])?>" readonly="readonly" style="width: 80px; text-align: center;" />
			</li>
			
			<li>
				<input type="text" id="txt_calendario_data_final" name="txt_calendario_data_final" value="<?=format_date_out($_POST['params'][2])?>" readonly="readonly" style="width: 80px; text-align: center;" />
			</li>
			
			<li style="margin: 0;"><input style="margin: 4px 0 0 0;" type="submit" class="btn_enviar" name="btn_enviar" id="btn_enviar" value="Salvar" title="Salvar" /></li>
		</ul>
	
	</form>
	
</div>

<script type="text/javascript">
	
	//controlar a navegação do calendário inicial
	$('a#cal_inicial-ant, a#cal_inicial-pro').live('click', function(e){
		e.preventDefault();
		
		var params = $(this).attr('href').split(',');
		$('#calendario-inicial').load('modal/financeiro_contas_pagar_calendario.php #calendario-inicial', {'cal-inicial' : params});
		
	});
	
	//controlar a navegação do calendário final
	$('a#cal_final-ant, a#cal_final-pro').live('click', function(e){
		e.preventDefault();
		
		var params = $(this).attr('href').split(',');
		$('#calendario-final').load('modal/financeiro_contas_pagar_calendario.php #calendario-final', {'cal-final' : params});
		
	});
	
	//passar a data do calendario para a caixa de texto
	$('#calendario-inicial a.link-dia').live('click', function(e){
		e.preventDefault();
		
		$('#txt_calendario_data_inicial').val($(this).attr('href'));
	});
	
	$('#calendario-final a.link-dia').live('click', function(e){
		e.preventDefault();
		
		$('#txt_calendario_data_final').val($(this).attr('href'));
	});
	
</script>