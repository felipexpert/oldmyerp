<?php
	
	session_start();
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');
	
	$data_atual		= date('Y-m-d');
	$hora_atual		= date('H:i:s');
	$usuario_id 	= $_SESSION['usuario_id'];
	$dia_atual		= date('D');
	if(isset($_POST['form'])){
		$serialize 	= $_POST['form'];	
		parse_str($serialize, $form);
		
		$funcionario_id	= $form['hid_funcionario_id'];
		$status_id		= $form['sel_status'];
		$data			= format_date_in($form['txt_data']);
		$hora			= $form['txt_hora'];
		
		
		echo mysql_error();
		$sql = 'INSERT INTO tblfuncionario_ponto (fldFuncionario_Id, fldData, fldHora, fldStatus_Id, fldDia_Semana) 
					VALUES ('.$funcionario_id.', "'.$data.'", "'.$hora.'", '.$status_id.', "'.$dia_atual.'" )';
		if(mysql_query($sql)){
			echo '<div class="alert" style="width:480px;margin-top:25px"><p class="ok" style="text-align:center">Registro inserido!</p></div>';
			echo '<a id="btn_fechar" href="#" style="color:#900;font-size:12px;text-decoration:underline; font-weight:bold">fechar</a>';
		}
	}else{
		
		####################################################################################################################3
		
		$funcionario_id 	= $_POST['params'][1];
		$rsFuncionario 		= mysql_query('SELECT fldNome FROM tblfuncionario WHERE fldId ='.$funcionario_id);
		//echo mysql_num_rows($rsFuncionario);
		$rowFuncionario   	= mysql_fetch_array($rsFuncionario);
		$funcionario_nome 	= $rowFuncionario['fldNome'];
		
		$rsStatus 		 	= mysql_query("SELECT tblfuncionario_ponto.fldData, tblfuncionario_ponto.fldHora, fldStatus_Id, fldStatus FROM tblfuncionario_ponto INNER JOIN tblfuncionario_ponto_status ON
											tblfuncionario_ponto.fldStatus_Id = tblfuncionario_ponto_status.fldId
											WHERE tblfuncionario_ponto.fldFuncionario_Id = $funcionario_id ORDER BY tblfuncionario_ponto.fldId DESC LIMIT 1");
		$rowStatus 			= mysql_fetch_array($rsStatus);
		$ultimo_status		= $rowStatus['fldStatus'];
		echo mysql_error();
		
		#PEGO OS IDS JA CADASTRADOS NAQUELE DIA, PARA NAO HAVER REPETICAO DE STATUS NA ESMA DATA
		$rsStatus_Dia	= mysql_query('SELECT group_concat(fldStatus_Id) as fldStatus_Cadastrados FROM tblfuncionario_ponto WHERE fldData = "'.date('Y-m-d').'" AND fldFuncionario_Id = '.$funcionario_id);
		$rowStatus_Dia	= mysql_fetch_array($rsStatus_Dia);
		if(!is_null($rowStatus_Dia['fldStatus_Cadastrados'])){
			$status_cadastrados = $rowStatus_Dia['fldStatus_Cadastrados'];
		}else{
			$status_cadastrados = '0';
		}
			

		
		/*		
		$hora_entrada1 	= $rowFuncionario['fldHora_Entrada1'];
		$hora_saida1 	= $rowFuncionario['fldHora_Saida1'];
		$hora_entrada2 	= $rowFuncionario['fldHora_Entrada2'];
		$hora_saida2 	= $rowFuncionario['fldHora_Saida2'];
		
		#AQUI FACO UMA VERIFICACAO APENAS BASEADA NO HORARIO QUE O FUNCIONARIO ESTA ENTRANDO, DE ACORDO COM O QUE ESTA CADASTRADO EM SUA CARGA HORARIA
		if($hora_atual > $hora_saida2 && $hora_atual < $hora_entrada1){
			$novo_status = '5,6'; #HORA EXTRA
		}elseif($hora_atual > $hora_saida2 && $hora_atual < $hora_saida1){
			$novo_status = '1'; #ENTRADA1
		}elseif($hora_atual > $hora_entrada1 && $hora_atual < $hora_entrada2){
			$novo_status = '2'; #SAIDA1
		}elseif($hora_atual > $hora_saida1 && $hora_atual < $hora_saida2){
			$novo_status = '3'; #ENTRADA2
		}elseif($hora_atual > $hora_entrada2 && $hora_atual < $hora_entrada1){
			$novo_status = '1'; #SAIDA2
		}		
		*/
		
?>	
		<form style="width:500px" class="frm_detalhe" id="frm_funcionario_ponto" action="" method="post">
			<input type="hidden" id="hid_funcionario_id" 	name="hid_funcionario_id" 	value="<?=$funcionario_id?>" />
			<input type="hidden" id="hid_ultimo_status_id" 	name="hid_ultimo_status_id" value="<?=$rowStatus['fldStatus_Id']?>" />
			<h4 style="width:490px; margin:5px; text-align:left"><?=$funcionario_nome?></h4>
			<fieldset style="width:500px; margin:5px;background:#FF9">
            	<legend>&uacute;ltimo status</legend>
				<ul>
					<li>
						<input type="text" style="width:80px;text-align:center;background:#FAFDB3" id="txt_data" name="txt_data" value="<?=format_date_out3($rowStatus['fldData'])?>" disabled="disabled" />
					</li>
					<li>
						<input type="text" style="width:80px;text-align:center;background:#FAFDB3" id="txt_hora" name="txt_hora" value="<?=$rowStatus['fldHora']?>" disabled="disabled" />
					</li>
					<li>
						<input type="text" style="width:240px;background:#FAFDB3" id="txt_hora" name="txt_hora" value="<?=$ultimo_status?>" disabled="disabled" />
					</li>
				</ul>
			</fieldset>
            <fieldset style="width:500px; margin:5px">
				<ul>
					<li>
						<label for="txt_data">Data</label>
						<input type="text" style="width:80px;height:30px;font-size:18px; text-align:center;background:#FFC" id="txt_data" name="txt_data" value="<?=format_date_out3(date("Y-m-d"))?>" readonly="readonly" />
					</li>
					<li>
						<label for="txt_hora">Hora</label>
						<input type="text" style="width:80px;height:30px;font-size:18px;text-align:center;background:#FFC" id="txt_hora" name="txt_hora" value="<?=date("H:i")?>" readonly="readonly" />
					</li>
					<li>
						<label for="sel_status">Descri&ccedil;&atilde;o</label>
						<select name="sel_status" id="sel_status" style="width:250px; height:30px;font-size:18px; background:#0F9">
                        	<option value="0">SELECIONE O STATUS</option>
<?							$rs  = "SELECT * FROM tblfuncionario_ponto_status WHERE fldId NOT IN ($status_cadastrados) ORDER BY fldId";
							$rs  = mysql_query($rs);
							while($row = mysql_fetch_array($rs)){
?>								<option value="<?=$row['fldId']?>"><?=$row['fldStatus']?></option>
<?							}     
?>						</select>
					</li>
					<li>
						<button style=" width:40px;height:30px; margin-top:16px" name="btn_inserir" id="btn_inserir" title="Inserir" >ok </button>
					</li>
                    
				</ul>
			</fieldset>
		</form>
<?	}
?>		
    <script type="text/javascript">
	
		//################################################################################################################################################################################################
		$('#frm_funcionario_ponto').submit(function(e){
			e.preventDefault();
			if($('#sel_status').val() > 0){
				$('#txt_ponto_funcionario_id').val('');
				var form 	= $(this).serialize();
				$('div.modal-conteudo').load('modal/funcionario_ponto_novo.php', {form : form});
				$('#btn_inserir').attr({'disabled' : 'disabled'});
			}
		});
		
		$('#btn_fechar').click(function(){
				$('div.modal-body:last').remove();
			$('#txt_ponto_funcionario_id').focus();
		});
		
	</script>
