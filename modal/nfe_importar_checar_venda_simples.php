<?php
    /*
     *  Verificar ao importar venda simples se o id digitado não é de uma venda que já foi convertida em NFe
     *  Esse arquivo é chamado através de ajax no arquivo modal nfe_importar.php
     */
    
    //carregando arquivos necessários para conexão com banco de dados e funções genéricas
    header('Content-type: application/json');
    session_start();
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');
    
    $pedido_id = $_GET['id'];
    $sql = "SELECT fldId FROM tblpedido WHERE fldId = $pedido_id AND (fldPedido_Destino_Nfe_Id IS NULL OR fldPedido_Destino_Nfe_Id <= 0)";
    
    if(mysql_num_rows(mysql_query($sql)) > 0) {
        $importar['prosseguir'] = true;
        $importar['NFenumero']  = 0;
    }
    else{
        $sql = "SELECT fldnumero FROM tblpedido_fiscal WHERE fldpedido_id = (SELECT fldPedido_Destino_Nfe_Id FROM tblpedido WHERE fldId = $pedido_id)";
        $NFenumero = mysql_fetch_array(mysql_query($sql));
        
        $importar['prosseguir'] = false;
        $importar['NFenumero']  = $NFenumero['fldnumero'];
    }
    
    if(isset($_GET['callback'])) echo $_GET['callback'] . '(' . json_encode($importar) . ')';
    else echo json_encode($importar);
?>