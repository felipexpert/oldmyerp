<?
	ob_start();
	session_start();
	//Fica lento com mais de 5 buscas, mas não dá problema com outras modais.
	//Ao fechar a tela e abrir novamente o peso na memória é aliviado momentaneamente
?>
	<script type="text/javascript">
		//evita que o enter faça alguma acao					   
		$("input#busca").keydown(function(event){
			if (event.keyCode == 13){
				return false;
			}			   
		});	
		
		$('document').ready(function(){
			$('#busca').focus();
			$('#busca').select();
			$('#busca').click(function(){
				$('#busca').val('');
			});
			$('#busca').keyup(busca_keyPress);
			
			function busca_Scroll(deslocamento){
				var cursorAtual 	= parseInt($('#hid_busca_cursor').val());
				var cursorLimite 	= parseInt($('#hid_busca_controle').text() - 1);
				var mover = false;
				if(deslocamento<0){
					if(cursorAtual > 0){mover = true;}
				}
				else{
					if(cursorAtual < cursorLimite){mover = true;}
				}
				if(mover){
					$('a[title=busca_lista_item]').removeClass('cursor');
					cursorNovo = cursorAtual + deslocamento;
					//corrigir cursor se ultrapassar limites
					if(cursorNovo < 0){cursorNovo = 0;}
					if(cursorNovo > cursorLimite){cursorNovo = cursorLimite;}
					
					$('a[title=busca_lista_item]:eq('+cursorNovo+')').addClass('cursor');
					$('#hid_busca_cursor').val(cursorNovo);
					var strNome = $('li[title=nome]:eq('+cursorNovo+')').text();
					$('#busca').val(strNome);
					
					//19 - altura da linha de exibição de cada registro
					$('#alvo').scrollTop($('#alvo').scrollTop() + 19 * deslocamento);
				}
			}
				
			function busca_keyPress (event) {
				console.log(this);
				//alert(event.keyCode);
				switch(event.keyCode){
					case 13: //enter
						var intCursor = $('#hid_busca_cursor').val();
						
						var codigo_produto =  $('a[title=busca_lista_item]:eq('+intCursor+')').attr('href');
						$('input#txt_componente_busca').val(codigo_produto);
						$('input#txt_componente_busca').focus();
						$('input#txt_componente_busca').blur();
						$('#txt_componente_produto_nome').focus();
						$('.frm_busca_produto').parents('.modal-body').remove();

					case 38: //up
					
						busca_Scroll(-1);
						break;
						
					case 40: //down
						busca_Scroll(1);
						break;
						
					case 33: //page up
						busca_Scroll(-10);
						break;
						
					case 34: //page down
						busca_Scroll(10);
						break;
					
					default: //busca
						//reset da posição do cursor para acompanhar o scroll
						$('#hid_busca_cursor').val(0);
						
						$.post('modal/produto_busca_consulta.php',
						{busca: $('#busca').val()+'%'},
						function(data){
							if ($('#busca').val()!=''){
								$('#alvo').show();
								$('#alvo').empty().html(data);
							}else{
								$('#alvo').empty();
							}
						});
						break;
				}
			}
			$('a.selecionar').live('click', function(e){
				e.preventDefault();
				var codigo_produto = $(this).attr('href');
				$('input#txt_componente_busca').val(codigo_produto);
				$('input#txt_componente_busca').focus();
				$('input#txt_componente_busca').blur();
				$('#txt_componente_produto_nome').focus();
				$('.frm_busca_produto').parents('.modal-body').remove();
				$('a.selecionar').die("click");
			});
		});
		
	</script>
   
	<? require("../inc/con_db.php"); ?>

    <form id="frm_busca" class="frm_busca_produto" style="width:940px">
        <fieldset>
            <legend>Busca de produto</legend>
            <input type="text" id="busca" name="busca" value="Digite o nome do produto" style='width:937px; margin-left:0px'>
            <ul id="busca_cabecalho" style="width:940px">
                <li style="width:112px">C&oacute;digo</li>
                <li style="width:562px">Produto</li>
                <li style="width:102px">Marca</li>
                <li style="width:72px">Valor</li>
				<li style="width:72px">Estoque</li>
            </ul>
            <input type="hidden" id="hid_busca_cursor" value="0" />
            <div id="alvo" style="width:940px"></div>
    	</fieldset>
	</form>
