
    <form id="frm_chamada_bina" name="frm_general" class="frm_detalhe" style="width:95%" action="" method="post">
    	<fieldset style="width:210px; height:116px;float:left; background:#EFEFEF">
        	<legend>chamando</legend>
            <ul>
                <li style="margin-right:5px">
                    <input type="text" style="width:158px;height:30px;font-size:22px;text-align:center;background:#FBF9C8" id="txt_telefone" name="txt_telefone" value="<?=$telefone?>" />
                </li>
                <li>
                    <input type="submit" class="btn_localizar" name="btn_localizar" id="btn_localizar" value="" title="" />
                </li>
                <li>
                    <label style="margin-top:3px" for="txt_linha">Linha</label>
                </li>
                <li>
                    <input type="text" style="width:115px" id="txt_linha" name="txt_linha" value="<?=$linha?>" />
                </li>
            </ul>
		</fieldset>
        <fieldset style="width:478px;float:left; margin-left:20px; background:#EFEFEF">
        	<legend>identifica&ccedil;&aring;o</legend>
            <div id="table" style="width:455px">
                <div id="table_cabecalho" style="width:455px">
                    <ul class="table_cabecalho" style="width:455px">
                        <li style="width:30px">C&oacute;d.</li>
                        <li style="width:120px">Nome</li>
                        <li style="width:120px">Nome Fantasia</li>
                        <li style="width:70px">Telefone 1</li>
                        <li style="width:70px">Telefone 2</li>
                    </ul>
                </div>
                <div style="width:455px; margin-bottom:10px;height:80px" id="table_container">       
                    <table style="width:435px" id="table_bina_listar" class="table_general" summary="Lista de cadastros">
                        <tbody>
                        </tbody>
                    </table>
                </div>
			</div>
		</fieldset>
        <fieldset style="height:280px; background:#EFEFEF">
        	<legend>cadastro</legend>
            <ul>
                <li style="margin-right: 505px">
                    <label for="txt_codigo">Codigo</label>
                    <input type="text" style="width:80px; text-align:center" id="txt_codigo" name="txt_codigo" onfocus="limpar (this,'auto')" onblur="mostrar (this, 'auto')" value="auto" />
                    <input type="hidden" id="hid_cliente_id" name="hid_cliente_id" value="" />
                </li>
                <li>
                    <label for="txt_cadastro">Cadastrado em</label>
                    <input type="text" style="width:95px;text-align:center" id="txt_cadastro" name="txt_cadastro" value="<?=date("d/m/Y")?>" readonly="readonly" />
                </li>
                <li>
                    <label for="txt_nome">Cliente</label>
                    <input type="text" style=" width:300px" id="txt_nome" name="txt_nome" value="" readonly="readonly" />
                </li>
                <li>
                    <label for="txt_nomefantasia">Nome Fantasia</label>
                    <input type="text" style=" width:270px;" id="txt_nome_fantasia" name="txt_nome_fantasia" value="" readonly="readonly" />
                </li>
                <li>
                    <label for="sel_genero">G&ecirc;nero</label>
                    <select style="width:100px" id="sel_genero" name="sel_genero" disabled="disabled" >
                        <option value="0">Selecionar</option>
                        <option  value="1">Feminino</option>
                        <option  value="2">Masculino</option>
                    </select>
                </li>
                <li>
                    <label for="sel_tipo">Tipo</label>
                    <select style="width:100px" id="sel_tipo" name="sel_tipo" disabled="disabled" >
                        <option value="1">F&iacute;sica</option>
                        <option value="2">Jur&iacute;dica</option>
                    </select>
                </li>
                <li>
                    <label for="txt_cpf_cnpj">CPF/CNPJ</label>
                    <input type="text" style=" width:145px" id="txt_cpf_cnpj" name="txt_cpf_cnpj" value="" readonly="readonly" />
                </li>
                <li>
                    <label for="txt_rg_ie">RG/IE</label>
                    <input type="text" style=" width:200px" id="txt_rg_ie" name="txt_rg_ie" value="" readonly="readonly" />
                </li>
                <li>
                    <label for="txt_telefone1">Telefone</label>
                    <input type="text" style=" width:100px" id="txt_telefone1" name="txt_telefone1" value="" readonly="readonly" />
                </li>
                <li>
                    <label for="txt_telefone2">Telefone 2</label>
                    <input type="text" style=" width:100px" id="txt_telefone2" name="txt_telefone2" value=""  readonly="readonly"/>
                </li>
<?				if($_SESSION["sistema_rota_controle"] == 1){					                   
?>                  <li>
                        <label for="txt_endereco_codigo">C&oacute;d. Endere&ccedil;o</label>
                        <input type="text" style=" width:95px" id="txt_endereco_codigo" name="txt_endereco_codigo" value="" />
                        <a href="endereco_busca" title="Localizar" class="modal" rel="680-380"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                    </li>
<?				}
?>                
                <li>
                    <label for="txt_endereco">Endere&ccedil;o</label>
                    <input type="text" style=" width:285px" id="txt_endereco" name="txt_endereco" value="" readonly="readonly" />
                </li>
                <li>
                    <label for="txt_numero">N&uacute;mero</label>
                    <input type="text" style=" width:45px" id="txt_numero" name="txt_numero" value="" readonly="readonly" />
                </li>
                <li>
                    <label for="txt_complemento">Complemento</label>
                    <input type="text" style=" width:205px" id="txt_complemento" name="txt_complemento" value="" readonly="readonly" />
                </li>
                <li>
                    <label for="txt_bairro">Bairro</label>
                    <input type="text" style=" width:155px" id="txt_bairro" name="txt_bairro" value=""  readonly="readonly"/>
                </li>
                <li>
                    <label for="txt_cep">CEP</label>
                    <input type="text" style=" width:110px" id="txt_cep" name="txt_cep" value="" readonly="readonly" />
                </li>
                <li>
                    <label for="txt_municipio_codigo">C&oacute;d. Munic&iacute;pio</label>
                    <input type="text" style=" width:100px" id="txt_municipio_codigo" name="txt_municipio_codigo" value="" readonly="readonly" />
                    <a href="municipio_busca" title="Localizar" class="modal" rel="680-380"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                </li>
                <li>
                    <label id="disabled" for="txt_municipio">Munic&iacute;pio</label>
                    <input type="text" style=" width:210px; background:#EAEAEA" id="txt_municipio" name="txt_municipio" readonly="readonly" value="" />
                </li>
                <li>
                    <label id="disabled" for="txt_uf">UF</label>
                    <input type="text" style=" width:40px; background:#EAEAEA;" id="txt_uf" name="txt_uf" readonly="readonly" value="" />
                </li>
                
                <li style="margin-left:310px">
				
                    <input type="submit" style="margin-top:14px" class="btn_novo" name="btn_novo" id="btn_novo" value="novo" title="Novo" />
				</li>
				<li>
                    <input type="submit" style="margin-top:14px" class="btn_enviar" name="btn_editar" id="btn_editar" value="editar" title="Editar" />
				</li>
				
                <li style="float:right">
                    <input type="submit" style="margin-top:14px" class="btn_enviar" name="btn_enviar" id="btn_enviar" value="salvar" title="Salvar" />
                </li>
			</ul>
		</fieldset>
    </form>


	<script type="text/javascript">
	
        $('#txt_telefone').focus();
		$('#frm_chamada_bina').submit(function(){
			var form 	= $(this).serialize();
			$('div.modal-conteudo').load('modal/bina_chamada.php', {form : form});
			return false;
		});
		
	</script>