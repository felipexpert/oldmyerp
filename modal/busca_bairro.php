	<script type="text/javascript">

		$('document').ready(function(){
			$('#buscar').focus();

			//$('#buscar').select();
			/*
			$('#buscar').click(function(){
				$('#buscar').val('');
			});
			*/
			
			$('#btn_buscar').click(function(e){
				e.preventDefault();
				busca_valor();
				$(this).hide();
				$('#loading').fadeIn();
			});
			
			$('#buscar').bind('keydown', busca_teclas);

			function busca_Scroll(deslocamento){
				var cursorAtual 	= parseInt($('#hid_busca_cursor').val());
				var cursorLimite 	= parseInt($('#hid_busca_controle').text() - 1);
				var mover = false;
				
				if(deslocamento<0){ if(cursorAtual > 0){mover = true;} }
				else{ if(cursorAtual < cursorLimite){mover = true;} }
				
				if(mover){
					$('a[title=busca_lista_item]').removeClass('cursor');
					cursorNovo = cursorAtual + deslocamento;
					//corrigir cursor se ultrapassar limites
					if(cursorNovo < 0){cursorNovo = 0;}
					if(cursorNovo > cursorLimite){cursorNovo = cursorLimite;}

					$('a[title=busca_lista_item]:eq('+cursorNovo+')').addClass('cursor');
					$('#hid_busca_cursor').val(cursorNovo);
					var strNome = $('li[title=nome]:eq('+cursorNovo+')').text();
					$('#buscar').val(strNome);

					//19 - altura da linha de exibição de cada registro
					$('#alvo').scrollTop($('#alvo').scrollTop() + 19 * deslocamento);
				}
			}
			
			function busca_teclas (event) {
				//console.log(this);
				//alert(event.keyCode);
				switch(event.keyCode){
					case 13: //enter
						$('#buscar').focus();
						//alert($('#hid_busca_controle').text());
						//verificar se tem resultado na busca
						if($('#hid_busca_controle').text() == 0){ return false; }
						else{
							var intCursor 	= $('#hid_busca_cursor').val();
							var id 			= $('a[title=busca_lista_item]:eq('+intCursor+')').attr('href');
							
							var codigo_bairro	= id;
							var bairro			= $('#'+id+'_bairro').val();
							var cod_myerp		= $('#'+id+'_cod_cidade').val();
			
							$('#txt_cod_bairro').val(codigo_bairro);
							$('#txt_bairro').val(bairro);
							$('#txt_municipio_codigo').val(cod_myerp)
							//remove tudo
							$('.frm_busca_produto').parents('.modal-body').remove();
							$('a.selecionar').die("click");
							$('#txt_municipio_codigo').focus().blur();
							$('#txt_cep').focus();
						}
						break;
					case 38: //up
						busca_Scroll(-1);
						break;
					case 40: //down
						busca_Scroll(1);
						break;
						
					case 33: //page up
						busca_Scroll(-10);
						break;
						
					case 34: //page down
						busca_Scroll(10);
						break;
				}
			}
			
			function busca_valor() {
				$('#hid_busca_cursor').val(0);
	
				//alert($('#chk_busca_qualquer_parte').val());
				var chkCampo = $('.chk_cep_campo:checked').val();
				
				$.post('cep/busca_cep.php',
				{tipo: 'busca_bairro', busca: $('#buscar').val(), campo: chkCampo},
				function(data){
					if ($('#buscar').val()!=''){
						$('#alvo').show();
						$('#alvo').empty().html(data);
					}else{
						$('#alvo').empty();
					}
					$('#btn_buscar').fadeIn();
					$('#loading').hide();
				});
				$('#buscar').focus();
			}
			
			$('a.selecionar').live('click', function(){
				var id 				= $(this).attr('href');

				var codigo_bairro	= id;
				var bairro			= $('#'+id+'_bairro').val();
				var cod_myerp		= $('#'+id+'_cod_cidade').val();

				$('#txt_cod_bairro').val(codigo_bairro);
				$('#txt_bairro').val(bairro);
				$('#txt_municipio_codigo').val(cod_myerp)
				//remove tudo
				$('.frm_busca_produto').parents('.modal-body').remove();
				$('a.selecionar').die("click");
				$('#txt_municipio_codigo').focus().blur();
				$('#txt_cep').focus();
			});		
		});	
		
	</script>

	<form id="frm_busca" name="frm_busca" class="frm_busca_produto" style="width:940px">
        <fieldset>
            <legend>Busca de produto</legend>
			<ul>
				<li style="float:left" >
					<input type="text" style="width: 875px; padding-left:10px" id="buscar" placeholder="Digite algum termo para pesquisa" size="80" >
                    <button type="button" id="btn_buscar">ok</button>
                    <img src="image/layout/loading.gif" id="loading" style="margin:15px 0 0 5px; float:right; display:none" />
				</li>
                <li style="float:left; margin:0 10px 12px 10px">
                	<input type="radio" style="width:0px; height:0; margin:3px 5px 0 0; float:left;" id="chk_bairro" name="chk_cep_campo" class="chk_cep_campo" value="tblendereco_bairro.bairro" checked="checked">
                	<label style="font-size:12px;" for="chk_bairro">bairro</label>
                </li>
                <li style="float:left; margin:0 10px 12px 0">
                	<input type="radio" style="width:0px; height:0; margin:3px 5px 0 0; float:left;" id="chk_cidade" name="chk_cep_campo" class="chk_cep_campo" value="tblendereco_cidade.cidade">
                	<label style="font-size:12px;" for="chk_cidade">cidade</label>
                </li>
			</ul>
		
            <ul id="busca_cabecalho" style="width:940px">
                <li style="width:590px">Bairro</li>
                <li style="width:300px">Cidade</li>
				<li style="width:20px;">UF</li>
            </ul>
            
            <input type="hidden" id="hid_busca_cursor" value="0" />
            
            <div id="alvo" style="width:940px">
            </div>
            
    	</fieldset>
	</form>
