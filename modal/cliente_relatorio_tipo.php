<script>
	$(document).ready(function(){
		$('#sel_pedido_relatorio').change(function(){
			var valor = $(this).find(':selected').attr('value');
			if(valor == "inativo"){
				$('.periodo').attr('disabled', '');
				$('.periodo:first').focus();
			} else {
				$('.periodo').attr('disabled', 'disabled');
			}
		});
	});
</script>

<form id="frm_cliente" name="frm_general" class="frm_detalhe" action="" method="post">
    <ul>
        <li class="form">
            <label for="txt_documento">Tipo de relat&oacute;rio:</label>
            <select id="sel_pedido_relatorio" name="sel_pedido_relatorio" style="width:130px">
                <option value="completo">completo</option>
                <option value="resumido">resumido</option>
                <option value="inativo">inativos</option>
            </select>
        </li>
        
        <li class="form">
            <label for="txt_documento">Per&iacute;odo</label>
            <input type="text" name="txt_periodo_inicio" 	style="width:80px; text-align:center" 
            disabled="disabled" class="calendario-mask periodo" />
            
            <input type="text" name="txt_periodo_termino" 	style="width:80px; margin-left:10px; text-align:center" 
            disabled="disabled" class="calendario-mask periodo" />
        </li>
        <input type="submit" style="float:right; margin:-10px 17px 0 0 !important" class="btn_enviar" name="btn_action" id="btn_action" value="imprimir" title="OK" />
    </ul>
</form>