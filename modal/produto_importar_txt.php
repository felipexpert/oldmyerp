
    <div class="alert" style="width: 500px">
        <p class="alert">Carregando... Esse processo pode demorar mais de 20min.</p>
        <img src="image/layout/carregando.gif" alt="carregando" />
    </div>

    <div id="produto_importar">
        <form id="form_produto_importar" action="index.php?p=produto" method="post" enctype="multipart/form-data" class="frm_detalhe">
            <ul style="width: 420px">
                <li>
                    <label for="file_txt">selecione o arquivo para importar</label>
                    <input type="file" name="file_txt" id="file_txt"  size="20"/> 
                </li>
                <li style="float:right"><input type="submit" class="btn_enviar" name="btn_produto_importar" id="btn_produto_importar" value="OK" title="OK" onclick="return confirm('Esse processo pode demorar e n&atilde;o deve ser interrompido. Deseja continuar?')" /></li>
            </ul>
        </form>
    </div>

	<script type="text/javascript">
		$('.alert').hide();
		$('#form_produto_importar').submit(function(){
			
			$('.alert').show();
			$('#produto_importar').hide();
		});
    </script>