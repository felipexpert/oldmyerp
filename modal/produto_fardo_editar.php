<?
	header("Content-Type: text/html; charset=ISO-8859-1", true); 
	
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');
	$produto_id = $_POST['params'][2];
	$fardo_id	= $_POST['params'][1];
	
	
	$sqlSaldo = "SELECT SUM(tblproduto_estoque_movimento.fldSaida) AS fldSaldo
				FROM tblproduto_estoque_movimento 
				INNER JOIN tblpedido_item ON tblproduto_estoque_movimento.fldReferencia_Id = tblpedido_item.fldId
				WHERE tblproduto_estoque_movimento.fldProduto_Id = $produto_id 
				AND tblproduto_estoque_movimento.fldTipo_Id in (1,5)
				AND tblpedido_item.fldProduto_Id = $produto_id and tblpedido_item.fldFardo_Id > 0 group by tblproduto_estoque_movimento.fldProduto_Id";
	$rsSaldoSaida = mysql_query($sqlSaldo);	
	$rowSaldoSaida = mysql_fetch_array($rsSaldoSaida);
	
	$sqlSaldo = "SELECT SUM(fldQuantidade) AS fldSaldo
	FROM tblproduto_fardo  
	WHERE fldProduto_Id = $produto_id AND fldExcluido = 0 group by fldProduto_Id";
	$rsSaldoFardo = mysql_query($sqlSaldo);	
	$rowSaldoFardo = mysql_fetch_array($rsSaldoFardo);
	
	$sqlSaldo = "SELECT SUM(fldEntrada - fldSaida) AS fldSaldo
	FROM tblproduto_estoque_movimento  
	WHERE fldProduto_Id = $produto_id  group by fldProduto_Id";
	$rsSaldoEntrada = mysql_query($sqlSaldo);	
	$rowSaldoEntrada = mysql_fetch_array($rsSaldoEntrada);
	
	echo mysql_error();
	$saldoEstoque 	= $rowSaldoSaida['fldSaldo'];
	
	$saldoFardo 	= $rowSaldoFardo['fldSaldo'];
	$saldoEntrada 	= $rowSaldoEntrada['fldSaldo'];
	$saldoSaida		= $rowSaldoSaida['fldSaldo'];
	
	$saldoTotal 	= ($saldoEntrada +  $saldoSaida) - $saldoFardo;
	
	
	$sqlSaldoFardo 	= "SELECT fldCodigo, fldFardo, SUM(fldQuantidade) AS fldSaldo
					FROM tblproduto_fardo
					WHERE fldProduto_Id = $produto_id AND fldId = $fardo_id";
	$rowSaldoFardo = mysql_fetch_array(mysql_query($sqlSaldoFardo));
	echo mysql_error();

	$codigo	= $rowSaldoFardo['fldCodigo'];
	$fardo 	= $rowSaldoFardo['fldFardo'];
				
				
?>
    <form action="" method="post" class="frm_detalhe">
        <ul>
        
            <li>
                <label for="txt_codigo">c&oacute;digo</label>
                <input style="width:100px" type="text" id="txt_codigo" name="txt_codigo" value="<?=$codigo?>" />
            </li>
            <li>
                <label for="txt_fardo">nome do fardo</label>
                <input style="width:250px" type="text" id="txt_fardo" name="txt_fardo" value="<?=($fardo)?>" />
            </li>
            
            
            <li style="width:255px; margin-top:10px">
                <label for="txt_qtd_sem_fardo">quantidade sem fardo definido</label>
            </li>
            <li>
                <input style="width:100px;text-align:right; color:#33F" type="text" id="txt_qtd_sem_fardo" name="txt_qtd_sem_fardo" value="<?=format_number_out($saldoTotal)?>" />
            </li>
            <li style="width: 255px">
                <label for="txt_qtd_fardo">quantidade deste fardo</label>
            </li>
            <li>
                <input style="width:100px;text-align:right" type="text" id="txt_qtd_fardo" name="txt_qtd_fardo" value="<?=format_number_out($rowSaldoFardo['fldSaldo'])?>" />
                <input type="hidden" id="hid_saldo_fardo" name="hid_saldo_fardo" value="<?=$rowSaldoFardo['fldSaldo']?>" />
            </li>
            <li style="float:right">
                <input type="submit" class="btn_enviar" name="btn_update" id="btn_update" value="Gravar" title="Gravar" />
                <input type="hidden" name="hid_fardo_id" value="<?=$fardo_id?>" />
            </li>
        </ul>
        
    </form>
    
    <script type="text/javascript">
						  
		var quantidade	= br2float($('#txt_qtd_sem_fardo').val()).toFixed(2);
		var saldoFardo	= $('#hid_saldo_fardo').val();
		var saldoTotal	= Number(quantidade) + Number(saldoFardo);
		
		$('#txt_qtd_fardo').keyup(function(){
     		
			novo 	= br2float($(this).val()).toFixed(2);
			saldo 	= Number(saldoTotal) - Number(novo);
			
			$('#txt_qtd_sem_fardo').val(float2br(saldo.toFixed(2)));
			if(saldo < 0){
				document.getElementById('txt_qtd_sem_fardo').style.color = '#C00';
			}else{
				document.getElementById('txt_qtd_sem_fardo').style.color = '#33F';
			}
		});
		
		//#####################################################################
		
		$('#txt_qtd_fardo').blur(function(){
			$(this).val(float2br(br2float($(this).val()).toFixed(2)));
		});	
		
		//#####################################################################
		
		$('#btn_update').click(function(){
			var quantidade = br2float($('#txt_qtd_sem_fardo').val());
			if(quantidade < 0 ){
				alert('N�o h� estoque suficiente para adicionar a este fardo!');
				return false;
			}else{
				return true;
			}
		});
		
    </script>