	<script type="text/javascript">

		$('document').ready(function(){
			$('#buscar').focus();

			//$('#buscar').select();
			/*
			$('#buscar').click(function(){
				$('#buscar').val('');
			});
			*/
			
			$('#btn_buscar').click(function(e){
				e.preventDefault();
				busca_valor();
				$(this).hide();
				$('#loading').fadeIn();
			});
			
			function busca_valor() {
				$('#hid_busca_cursor').val(0);
				//alert($('#chk_busca_qualquer_parte').val());
				var chkCampo = $('.chk_cep_campo:checked').val();
				$.post('cep/busca_cep.php',
				{busca: $('#buscar').val(), campo: chkCampo, tipo: 'busca_cep'},
				function(data){
					if ($('#buscar').val()!=''){
						$('#alvo').show();
						$('#alvo').empty().html(data);
					}else{
						$('#alvo').empty();
					}
					$('#btn_buscar').fadeIn();
					$('#loading').hide();
				});
				$('#buscar').focus();
			}
			
			$('a.selecionar').live('click', function(){
				var id 			= $(this).attr('href');
				var cep			= id;
				var rua			= $('#'+id+'_rua').val();
				var bairro		= $('#'+id+'_bairro').val();
				var cod_myerp	= $('#'+id+'_cod_myerp').val();
				var setor		= $('#'+id+'_setor').val();
				var id_endereco = $('#'+id+'_id_endereco').val();
				var cadastrar	= $('#'+id+'_cadastrar_end').val();
				
				if(cadastrar === "1"){
					var confirma_cadastro = confirm('Deseja adicionar este CEP a sua lista de rotas?');
					if(confirma_cadastro){
						$.ajax({
							type: "GET",
							dataType: "json",
							url: "cep/busca_cep.php?tipo=cadastra_endereco&cep="+cep,
							success: function(data){
								if(data.erro){
									alert("Erro ao efetuar cadastro");
								}else{
									var id_endereco = data.id_endereco;
									$('#txt_endereco_codigo').val(id_endereco);
								}
							}
						});
					}
				} else {
					$('#txt_endereco_codigo').val(id_endereco);
				}

				$('#txt_cep').val(cep);
				$('#txt_endereco').val(rua);
				$('#txt_bairro').val(bairro);
				$('#txt_municipio_codigo').val(cod_myerp);
				$('#txt_setor').val(setor);
				$('#txt_municipio_codigo').focus();
				$('#txt_municipio_codigo').blur(); //remove tudo
				$('.frm_busca_produto').parents('.modal-body').remove();
				$('a.selecionar').die("click");
				$('#txt_cep').focus();
			});		
		});	
		
	</script>

	<form id="frm_busca" name="frm_busca" class="frm_busca_produto" style="width:940px">
        <fieldset>
            <legend>Busca de produto</legend>
			<ul>
				<li style="float:left" >
					<input type="text" style="width: 875px; padding-left:10px" id="buscar" placeholder="Digite algum termo para pesquisa" size="80" >
                    <button type="button" id="btn_buscar">ok</button>
                    <img src="image/layout/loading.gif" id="loading" style="margin:15px 0 0 5px; float:right; display:none" />
				</li>
                <li style="float:left; margin:0 10px 12px 8px">
                	<input type="radio" style="width:0px; height:0; margin:3px 5px 0 0; float:left;" id="chk_cep" name="chk_cep_campo" class="chk_cep_campo" checked="checked" value="fldCEP">
                	<label style="font-size:12px;" for="chk_cep">cep</label>
                </li>
                <li style="float:left; margin:0 10px 12px 0">
                	<input type="radio" style="width:0px; height:0; margin:3px 5px 0 0; float:left;" id="chk_rua" name="chk_cep_campo" class="chk_cep_campo" value="fldRua">
                	<label style="font-size:12px;" for="chk_rua">rua</label>
                </li>
                <li style="float:left; margin:0 10px 12px 0">
                	<input type="radio" style="width:0px; height:0; margin:3px 5px 0 0; float:left;" id="chk_bairro" name="chk_cep_campo" class="chk_cep_campo" value="fldBairro">
                	<label style="font-size:12px;" for="chk_bairro">bairro</label>
                </li>
                <li style="float:left; margin:0 10px 12px 0">
                	<input type="radio" style="width:0px; height:0; margin:3px 5px 0 0; float:left;" id="chk_cidade" name="chk_cep_campo" class="chk_cep_campo" value="fldCidade">
                	<label style="font-size:12px;" for="chk_cidade">cidade</label>
                </li>
			</ul>
		
            <ul id="busca_cabecalho" style="width:940px">
            	<li style="width:17px; margin-left:3px"><a href="busca_cep_faq_icon" style="color:white;" class="modal" rel="400-300">[?]</a></li>
                <li style="width:78px">CEP</li>
                <li style="width:420px">Rua</li>
                <li style="width:190px">Bairro</li>
                <li style="width:180px">Cidade</li>
				<li style="width:20px;">UF</li>
            </ul>
            
            <input type="hidden" id="hid_busca_cursor" value="0" />
            
            <div id="alvo" style="width:940px">
            </div>
            
    	</fieldset>
	</form>
