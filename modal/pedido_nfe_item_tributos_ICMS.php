
        <fieldset style="width:920px;margin:10px">
            <ul style="width:920px;">
                <li>
                    <label for="sel_nfe_item_tributos_icms_situacao_tributaria">Situa&ccedil;&atilde;o Tribut&aacute;ria</label>
                    <select name="sel_nfe_item_tributos_icms_situacao_tributaria" id="sel_nfe_item_tributos_icms_situacao_tributaria" style="width:720px">
<?						require("../produto_fiscal_icms_select_situacao_tributaria.php");
?>					</select>
                </li>
                <li>
                    <label for="sel_nfe_item_tributos_icms_origem">Origem</label>
                    <select name="sel_nfe_item_tributos_icms_origem" id="sel_nfe_item_tributos_icms_origem" style="width:170px">
	                    <option value=""></option>
<?						$sql = "select * from tblnfe_icms_campo_orig order by fldId";
                    	$rsOrigem = mysql_query($sql);
                        	while($rowOrigem = mysql_fetch_array($rsOrigem)){
?>								<option <? $icms_orig ==  $rowOrigem['fldId'] ? print 'selected="selected"' : '' ?> value="<?=$rowOrigem['fldId']?>"><?=substr($rowOrigem['fldDescricao'],0,70)?></option>
<?							}
?>					</select>
                </li>
            </ul>
        </fieldset>
        <fieldset style="width:920px;margin:10px">
            <ul>
                <li>
                    <label for="txt_nfe_item_tributos_icms_pCredSN">Al&iacute;quota aplic&aacute;vel de c&aacute;lculo do cr&eacute;dito</label>
                    <input type="text" style="width:250px;text-align:right" id="txt_nfe_item_tributos_icms_pCredSN" name="txt_nfe_item_tributos_icms_pCredSN" value="<?=$icms_pCredSN?>" />
                </li>
               <li>
                    <label for="txt_nfe_item_tributos_icms_vCredICMSSN">Valor cr&eacute;dito do ICMS que pode ser aproveitado</label>
                    <input type="text" style="width:300px;text-align:right" id="txt_nfe_item_tributos_icms_vCredICMSSN" name="txt_nfe_item_tributos_icms_vCredICMSSN" value="" />
                </li>
                
            </ul>
        </fieldset>
                
        <fieldset style="min-height:280px;width:448px;display:inline;margin:10px">
			<legend>ICMS</legend>
            <ul>
                <li style="width:100%">
                    <label for="sel_nfe_item_tributos_icms_modBC">Modalid. de Determ. da BC ICMS</label>
                    <select id="sel_nfe_item_tributos_icms_modBC" name="sel_nfe_item_tributos_icms_modBC">
                    	<option value=""></option>
<?					   	$rsModalidade = mysql_query("SELECT * FROM tblnfe_icms_campo_modbc");
                        while($rowModalidade = mysql_fetch_array($rsModalidade)){
?>						    <option <? $icms_modBC == $rowModalidade['fldCodigo'] ? print 'selected="selected"' : '' ?> value="<?=$rowModalidade['fldCodigo']?>"><?=$rowModalidade['fldDescricao']?></option>
<? 						}
?>					</select>
                </li>
               
                <li style="width:100%">
                    <label for="txt_nfe_item_tributos_icms_pRedBC">% redu&ccedil;&atilde;o da BC ICMS</label>
                    <input type="text" style="width:200px;text-align:right" id="txt_nfe_item_tributos_icms_pRedBC" name="txt_nfe_item_tributos_icms_pRedBC" value="<?=$icms_pRedBC?>" />
                </li>
                
                <li style="width:100%">
                    <label for="txt_nfe_item_tributos_icms_vBC">BC ICMS</label>
                    <input type="text" style="width:200px;text-align:right" id="txt_nfe_item_tributos_icms_vBC" name="txt_nfe_item_tributos_icms_vBC" value="<?=$icms_vBC?>" />
                </li>
                
                <li style="width:100%">
                    <label for="txt_nfe_item_tributos_icms_pICMS">Al&iacute;quota do ICMS</label>
                    <input type="text" style="width:200px;text-align:right" id="txt_nfe_item_tributos_icms_pICMS" name="txt_nfe_item_tributos_icms_pICMS" value="<?=$icms_pICMS?>" />
                </li>
                
                <li style="width:100%">
                    <label for="txt_nfe_item_tributos_icms_vICMS">ICMS</label>
                    <input type="text" style="width:200px;text-align:right" id="txt_nfe_item_tributos_icms_vICMS" name="txt_nfe_item_tributos_icms_vICMS" value="<?=$icms_vICMS?>" />
                </li>
                
                <li style="width:100%">
                    <label for="txt_nfe_item_tributos_icms_pBCOp">% BC da Opera&ccedil;&atilde;o Pr&oacute;pria</label>
                    <input type="text" style="width:200px;text-align:right" id="txt_nfe_item_tributos_icms_pBCOp" name="txt_nfe_item_tributos_icms_pBCOp" value="<?=$icms_pBCOp?>" />
                </li>
                
                <li style="width:100%">
                    <label for="sel_nfe_item_tributos_icms_motDesICMS">Motivo da Desonera&ccedil;&atilde;o do ICMS</label>
                    <select id="sel_nfe_item_tributos_icms_motDesICMS" name="sel_nfe_item_tributos_icms_motDesICMS">
                    	<option value=""></option>
<?						$rsDesoneracao = mysql_query("SELECT * FROM tblnfe_icms_campo_motdesicms");
						while($rowDesoneracao = mysql_fetch_array($rsDesoneracao)){
?>							<option <? $icms_motDesICMS==$rowDesoneracao['fldId'] ? print 'selected="selected"' : '' ?> value="<?=$rowDesoneracao['fldId']?>"><?=$rowDesoneracao['fldDescricao']?></option>
<? 						}
?>					</select>
            	</li>
			</ul>
		</fieldset>
            
        <fieldset style="min-height:280px;width:470px;display:inline">
			<legend>ICMS ST</legend>
            <ul>
				<li style="width:100%">
                    <label for="sel_nfe_item_tributos_icms_st_modBCST">Modalid. de determ. da BC do ICMS ST</label>
                    <select id="sel_nfe_item_tributos_icms_st_modBCST" name="sel_nfe_item_tributos_icms_st_modBCST">
                    	<option value=""></option>
<?						$rsModalidDeterm 		= mysql_query("SELECT * FROM tblnfe_icms_campo_modbcst");
                        while($rowModalidDeterm = mysql_fetch_array($rsModalidDeterm)){
?>						  	<option <? $icms_modBCST == $rowModalidDeterm['fldId'] ? print 'selected="selected"' : '' ?> value="<?=$rowModalidDeterm['fldId']?>"><?=$rowModalidDeterm['fldDescricao']?></option>
<? 						}
?>					</select>
                </li>
				<li style="width:100%">
                    <label for="txt_nfe_item_tributos_icms_st_pRedBCST">% redu&ccedil;&atilde;o da BC ICMS ST</label>
                    <input type="text" style="width:200px;text-align:right" id="txt_nfe_item_tributos_icms_st_pRedBCST" name="txt_nfe_item_tributos_icms_st_pRedBCST" value="<?=$icms_pRedBCST?>" />
                </li>
				<li style="width:100%">
                    <label for="txt_nfe_item_tributos_icms_st_pMVAST"> % margem de valor adic. do ICMS ST</label>
                    <input type="text" style="width:200px;text-align:right" id="txt_nfe_item_tributos_icms_st_pMVAST" name="txt_nfe_item_tributos_icms_st_pMVAST" value="<?=$icms_pMVAST?>" />
                </li>
				<li style="width:100%">
                    <label for="txt_nfe_item_tributos_icms_st_vBCST"> BC ICMS ST</label>
                    <input type="text" style="width:200px;text-align:right" id="txt_nfe_item_tributos_icms_st_vBCST" name="txt_nfe_item_tributos_icms_st_vBCST" value="<?=$icms_vBCST?>" />
                </li>
				<li style="width:100%">
                    <label for="txt_nfe_item_tributos_icms_st_pICMSST">Al&iacute;quota do ICMS ST</label>
                    <input type="text" style="width:200px;text-align:right" id="txt_nfe_item_tributos_icms_st_pICMSST" name="txt_nfe_item_tributos_icms_st_pICMSST" value="<?=$icms_pICMSST?>" />
                </li>
				<li style="width:100%">
                    <label for="txt_nfe_item_tributos_icms_st_vICMSST">ICMS ST</label><small id="icms_st_202">valor informado j&aacute; com abatimento do ICMS pr&oacute;prio</small>
                    <input type="text" style="width:200px;text-align:right" id="txt_nfe_item_tributos_icms_st_vICMSST" name="txt_nfe_item_tributos_icms_st_vICMSST" value="<?=$icms_vICMSST?>" readonly="readonly" />
                </li>
				<li style="width:100%">
                    <label for="sel_nfe_item_tributos_icms_st_UFST">UF para a qual &eacute; devido o ICMS ST</label>
                    <select id="sel_nfe_item_tributos_icms_st_UFST" name="sel_nfe_item_tributos_icms_st_UFST">
                    	<option value=""></option>
<?						$sql 		 = "select * from tblnfe_icms_campo_ufst";
                       	$rsAuxiliar  = mysql_query($sql);
                       	$rowAuxiliar = mysql_fetch_array($rsAuxiliar);
                       	$rowDados 	 = mysql_fetch_array(mysql_query("SELECT * FROM tblproduto_fiscal WHERE fldProduto_Id = ".$_GET['id']));
                        while($rowAuxiliar = mysql_fetch_array($rsAuxiliar)){
?>							<option <?=($rowAuxiliar['fldDescricao'] == $rowDados['fldicms_UFST'])? print "selected='selected'" : ""; ?> title="<?=$rowAuxiliar['fldId']?>" value="<?=$rowAuxiliar['fldDescricao']?>"><?=substr($rowAuxiliar['fldDescricao'],0,70)?></option>
<?						}
?>					</select>
               	</li>
				<li style="width:100%">
                    <label for="txt_nfe_item_tributos_icms_st_vBCSTRet">BC ICMS ST retido na UF remetente</label>
                    <input type="text" style="width:200px;text-align:right" id="txt_nfe_item_tributos_icms_st_vBCSTRet" name="txt_nfe_item_tributos_icms_st_vBCSTRet" value="<?=$icms_vBCSTRet?>" />
                </li>
				<li style="width:100%">
                    <label for="txt_nfe_item_tributos_icms_st_vICMSSTRet">ICMS ST retido na UF remetente</label>
                    <input type="text" style="width:200px;text-align:right" id="txt_nfe_item_tributos_icms_st_vICMSSTRet" name="txt_nfe_item_tributos_icms_st_vICMSSTRet" value="<?=$icms_vICMSSTRet?>" />
                </li>
				<li style="width:100%">
                    <label for="txt_nfe_item_tributos_icms_st_vBCSTDest">BC ICMS ST da UF destino</label>
                    <input type="text" style="width:200px;text-align:right" id="txt_nfe_item_tributos_icms_st_vBCSTDest" name="txt_nfe_item_tributos_icms_st_vBCSTDest" value="<?=$icms_vBCSTDest?>" />
                </li>
				<li style="width:100%">
                    <label for="txt_nfe_item_tributos_icms_st_vICMSSTDest">ICMS ST da UF destino</label>
                    <input type="text" style="width:200px;text-align:right" id="txt_nfe_item_tributos_icms_st_vICMSSTDest" name="txt_nfe_item_tributos_icms_st_vICMSSTDest" value="<?=$icms_vICMSSTDest?>" />
                </li>
                
			</ul>
		</fieldset>
	