<?
	require_once("../inc/con_db.php");
	$origem 	= $_POST['params'][1];
	$destino 	= $_POST['info']; //restante do caminho enviado pelo arquivo anterior
?>	
    <form id="frm_impressao" action="" target="_blank" method="post" class="frm_detalhe" style="margin-left:5px">
        <ul>
            <li>
                <select name="sel_impressao" id="sel_impressao">
                	<option value="0">Escolha o local de impress&atilde;o</option>
<?
					 $rsImpressao = mysql_query("SELECT tblsistema_estacao.fldLocal, tblsistema_impressao_local.*, tblsistema_impressao.fldSufixo FROM tblsistema_impressao_local
					 								INNER JOIN tblsistema_impressao ON tblsistema_impressao_local.fldModelo_Id = tblsistema_impressao.fldId
													INNER JOIN tblsistema_estacao ON tblsistema_impressao_local.fldEstacao_Id = tblsistema_estacao.fldId");
					 while($rowImpressao = mysql_fetch_array($rsImpressao)){
						 echo "<option id='{$rowImpressao['fldId']}' class='{$rowImpressao['fldModelo_Id']}' value='{$rowImpressao['fldSufixo']}'>{$rowImpressao['fldImpressora']} em {$rowImpressao['fldLocal']}</option>";
					 }
?>                </select>
            </li>
            <li>
            	<div id="impressao_variacao">
                	<select name="sel_impressao_variacao" id="sel_impressao_variacao">
                	</select>
                </div>
            </li>
            <li style="float:right; margin-right:10px">
            	<input id="hid_impressao_local_id" name="hid_impressao_local_id" type="hidden" value="" />
                <input type="submit" class="btn_enviar" name="btn_enviar" id="btn_enviar" value="Imprimir" title="Imprimir" />
            </li>
        </ul>
    </form>


	<script type="text/javascript">
		
		$('#impressao_variacao').hide();
		var origem = '<?= $origem ?>'+'_';
		var destino = '<?= $destino ?>';
		
		$('#sel_impressao').change(function(){
			$('#impressao_variacao').hide();
			$('#frm_impressao').attr({'action' : origem+$('#sel_impressao').val()+'.php'+destino});
			
			var local_id = $('#sel_impressao option:selected').attr('id');
			var modelo_id = $('#sel_impressao option:selected').attr('class');
			$('#hid_impressao_local_id').val(local_id);

			$.get('modal/impressao_destino_variacao.php',{impressao_modelo_id:modelo_id},function(valor){
				if(valor != ''){
					$('#impressao_variacao').show();
					$("select[name=sel_impressao_variacao]").html(valor);
				}
			});
		});
		
		$('#sel_impressao_variacao').change(function(){
			if( $(this).val() != ''){
				variacao = '_'+$(this).val();
			}
			$('#frm_impressao').attr({'action' : origem+$('#sel_impressao').val()+variacao+'.php'+destino});
		});	
		
		$('#btn_enviar').click(function(){
			if($('#sel_impressao').val()== '0'){
				alert('Selecione um local de impressão');
				return false;
			}
			
		});
	</script>