<?php

	require_once('../inc/fnc_ibge.php');
	require_once('../inc/fnc_nfe.php');
	
	$sql = 'SELECT * from tblempresa_info';
	$rsEmpresa 	= mysql_query($sql);
	$rowEmpresa = mysql_fetch_array($rsEmpresa);
		
	$sql = "SELECT * FROM tbl{$documento_tipo} INNER JOIN tblpedido_fiscal
			ON tbl{$documento_tipo}.fldId = tblpedido_fiscal.fldpedido_id WHERE tbl{$documento_tipo}.fldId = $nfe_id GROUP BY fldPedido_Id";
	$rsNFe = mysql_query($sql);
	echo mysql_error();

	while($rowNFe = mysql_fetch_array($rsNFe)){
		
		//criar arquivo em modo append
		$tpAmb 		= fnc_sistema('nfe_ambiente_tipo');
		$ambiente 	= ($tpAmb == '1')? 'producao': 'homologacao' ;
	
		$emitente_uf_codigo = substr($rowEmpresa['fldMunicipio_Codigo'],0,2);
		$mes_ano_emissao 	= explode('-', (($documento_tipo == 'pedido') ? $rowNFe['fldPedidoData'] : $rowNFe['fldCompraData']));
		$mes_ano_emissao 	= substr($mes_ano_emissao[0],2,2).$mes_ano_emissao[1];
		$tpEmis 			= '1';
		$codigo_cnf 		= str_pad($rowNFe['fldpedido_id'], 8, '0', STR_PAD_LEFT);	//C�digo num�rico que comp�e a Chave de Acesso. 
	
		$chave_nfe 			= monta_chave_nfe($emitente_uf_codigo, $mes_ano_emissao, fnc_number_only($rowEmpresa['fldCPF_CNPJ']), 55, $rowNFe['fldserie'], $rowNFe['fldnumero'], $tpEmis, $codigo_cnf); 
		$arquivo_nome 		= '../nfe_myerp/'.$ambiente.'/entradas/'.str_pad($rowNFe['fldpedido_id'],8,'0', STR_PAD_LEFT).'_'.$chave_nfe . '_nfe.txt';
		//$arquivo_nome 	= '../nfe_myerp/'.$ambiente.'/temporarias/txt/'.str_pad($rowNFe['fldpedido_id'],8,'0', STR_PAD_LEFT).'_'.$chave_nfe . '_nfe.txt';
		
		
		$arquivo = fopen($arquivo_nome,"a");
		//iniciar arquivo de sa�da
		fwrite($arquivo,'NOTA FISCAL|1|');
		
		unset($itens);
		unset($cobranca_duplicatas);
		unset($item_numero);
		
		$sql 				= 'SELECT *, CONCAT(fldEndereco," ",fldNumero," ",fldComplemento," ",fldBairro) as fldEndereco_Completo FROM tbltransportador where fldId = ' . $rowNFe['fldtransporte_transportador_id'];
		$rsTransportador 	= mysql_query($sql);
		$rowTransportador 	= mysql_fetch_array($rsTransportador);
		
		
		//$chave_acesso 	= monta_chave_acesso($emitente_uf_codigo, $mes_ano_emissao, fnc_number_only($rowEmpresa['fldCPF_CNPJ']), 55, $rowNFe['fldserie'], $rowNFe['fldnumero'], $rowNFe['fldserie'], $tpEmis, $codigo_cnf); 
		//A-Dados da Nota fiscal eletr�nica
		$versao = array(
			'id'			=>	'A',
			'leiaute'       =>	fnc_sistema('nfe_leiaute'),
			'identificacao' =>	'NFe'.$chave_nfe
		);
		
		$rsNfeOperacao 		= mysql_query('SELECT fldNatureza_Operacao FROM tblnfe_natureza_operacao WHERE fldId = '.$rowNFe['fldnatureza_operacao_id']);
		$rowNfeOperacao 	= mysql_fetch_array($rsNfeOperacao);
		$natureza_operacao 	= $rowNfeOperacao['fldNatureza_Operacao'];
		//*********************************************************
		//B-Identifica��o da Nota Fiscal eletr�nica
		$documento = array(
			'id'									=> 'B',
			'emitente_uf_codigo'					=> $emitente_uf_codigo,
			'codigo_cnf'                          	=> $codigo_cnf,										//C�digo Num�rico que comp�e a Chave de Acesso,numero gerado aleat�rio para cada NF-e
			'operacao_natureza'						=> $natureza_operacao,
			'pagamento_forma'                    	=> $rowNFe['fldpagamento_forma_codigo'],	//0 - a vista, 1 - a prazo, 2 - outros
			'modelo_codigo'                 		=> '55',									//C�digo do Modelo do Doc. Fiscal
			'serie'                               	=> $rowNFe['fldserie'],
			'numero'                              	=> $rowNFe['fldnumero'],
			'emissao'                             	=> ($documento_tipo == 'pedido') ? $rowNFe['fldPedidoData'] : $rowNFe['fldCompraData'],
			'saida_data'                  			=> $rowNFe['fldsaida_data'],
			'saida_hora'                  			=> $rowNFe['fldsaida_hora'],				//Formato da hora "HH:MM:SS"
			'operacao_tipo'                       	=> $rowNFe['fldOperacao_Tipo'],										//0 - entrada, 1 - sa�da
			'municipio_codigo'                    	=> $rowEmpresa['fldMunicipio_Codigo'],		//C�digo do munic�pio de ocorr�ncia do fato gerador
			'danfe_impressao_formato'             	=> '1',										//1-Retrato / 2-Paisagem
			'emissao_tipo'                    		=> $tpEmis,									//1-Normal 2-Contig�ncia FS 3-Contig�ncia SCAN 4-Contig�ncia DPEC 5-Contig�ncia FS-DA
			'chave_acesso_verificacao'        		=> '',//$chave_nfe,							//Informar o DV da chave de acesso da NFe, o DV sera calcualdo com a aplica��o do algoritimo modulo 11 base(2,9) da chave de acesso.
			'ambiente_indentificacao'             	=> $tpAmb,									//1-Produ��o  2- Homologa��o
			'emissao_finalidade'					=> '1',										//B25 - finNFe - Finalidade de emiss�o da NF-e  ---  1 NF-e normal/ 2-NF-e complementar / 3 � NF-e de ajuste
			'emissao_processo_identificador'   		=> '3',										//0-Emiss�o NFe e com aplicativo do contribuinte.1-Avulsa pelo FISCO.2-Avulsa,pelo contribuinte com seu certificado digital,atraves do site do FISCO.3-Pelo contribuinte com aplicativo fornecido pelo FISCO
			'emissao_processo_versao'       		=> fnc_sistema('nfe_leiaute'),
			'contingencia_data_hora_entrada'  		=> '',
			'contingencia_justificativa'      		=> ''
		);
		
		//*********************************************************
		//Grupo de informa��es de NF de produtor rural referenciada, faz parte do bloco B
		/*
		$produtorrural = array(
			
			'codigo_uf'                  => '35',//B20b
			'nfe_data_emissao'           => '2011-05',//B20c formato AAAA-MM
			'emitente_cnpj'              => '98652165485',//B20d
			'emitente_cpf'               => '123.123.654-58',//B20e
			'inscricao_estadual'         => '1354.5487',//B20f
			'documento_codigo_modelo'    => '01',//B20f  informar o codigo 04-NF de Produtor ou 01 para NF avulsa
			'documento_serie'            => '890-899',//B20g informar zero se inexistente
			'documento_numero'           => '28052011',//B20h
			'chave_acesso_ct'            => '',//B20i  Utilizar para referenciar um CT-e emitido anteriormente, vinculada a NFe atual(v2.0)
			'cupom_informacoes'          => '',//B20j Grupo do cupom fiscal vinculado a NFe
			'documento_modelo'           => '',//B20k Preencher com "2B" quando se tratar de cupom fiscal emitido por maquina registradora(nao ECF),com "2C" quando se tratar de cupom fiscal PDV e "2D" quando cupom fiscal(emitido por ECF)
			'ecf_numero'                 => '',//B20l infromar numero sequemncial do ECF que emitiu o cupom fiscal
			'contador_ordem_operacao'    => '',//B20m informar o numero do contador de ordem de operacao vinculado a NFe 
		);
		*/
		//Fim do bloco B
		/**********************************************************/
		
		//C-Indentifica��o do Emitente da nota Fiscal Eletr�nica
		$emitente = array(
			'id'								=> 'C',
			'nome'                             	=> acentoRemover($rowEmpresa['fldRazao_Social']),	//C03 Razao social ou nome do emitente
			'nome_fantasia'                    	=> acentoRemover($rowEmpresa['fldNome_Fantasia']),	//C04
			'inscricao_estadual'               	=> format_cpf_in($rowEmpresa['fldIE']),				//C18 - estou usando a mesma funcao de format do CPF pois eh so pra tirar os pontos
			'substituto_tributario'            	=> '',												//Informar o IE do ST da UF de destino da mercadoria, se houver a reten��o do ICMS ST para UF de destino
			'inscricao_municipal'              	=> $rowEmpresa['fldIM'],							//19
			'cnae'                      		=> $rowEmpresa['fldCNAE'],							//C20 Este campo deve ser informado quando o IM (C19) for informado
			'regime_tributario_codigo'         	=> $rowNFe['fldregime_tributario_codigo']			//C21 Este campo ser� obrigatoriamente preenchido com: 1-Simples Nacional 2-Simples Nacional-excesso de sublimite de receita bruta 3-Regime Normal(v2.0)
		);
		
		//linha C02
		$emitente_documento = array(
			'id'								=> 'C02',
			'cpf_cnpj'                    		=> $rowEmpresa['fldCPF_CNPJ']	//C02 ou C02a - CNPJ ou CPF
		);
		
		//Linha C05 informacoes do emitente
		$emitente_endereco = array(
			'id'								=> 'C05',
			'logradouro'                       	=> acentoRemover($rowEmpresa['fldEndereco']),								//C06
			'numero'                           	=> acentoRemover($rowEmpresa['fldNumero']),									//C07
			'complemento'                      	=> acentoRemover($rowEmpresa['fldComplemento']),							//C08
			'bairro'                           	=> acentoRemover($rowEmpresa['fldBairro']),									//C09
			'municipio_codigo'                 	=> $rowEmpresa['fldMunicipio_Codigo'],										//C10 Utilizar a tabela do IBGE
			'municipio_nome'                   	=> acentoRemover(fnc_ibge_municipio($rowEmpresa['fldMunicipio_Codigo'])),	//C11 
			'uf'                               	=> fnc_ibge_uf_sigla($rowEmpresa['fldMunicipio_Codigo']),					//C12
			'codigo_cep'                       	=> fnc_number_only($rowEmpresa['fldCEP']),									//C13 Informar os Zeros n�o significativos
			'codigo_pais'                      	=> '1058',																	//C14
			'pais_nome'                        	=> 'BRASIL',																//C15 Brasil ou BRASIL
			'telefone'                         	=> fnc_number_only($rowEmpresa['fldTelefone1'],11)							//C16 Ulitizar o DDD+ o numero do telefone, e nas operqa��es com e exterior � permitido informar o c�digo do pa�s + codigo localidade + numero telefone
		);
		//fim do bloco C
		/**********************************************************/
		/*
		D -Indentifica��o do Fisco Emitente da NF-e // n�o esta sendo usado no momento
		$orgao = array(
			'emitente_cnpj'                  => '',//D02 informar os zeros significativos
			'emitente'                       => '',//D03
			'agente_matricula'               => '',//D04
			'agente_nome'                    => '',//D05
			'orgao_telefone'                 => '',//D06
			'uf'                             => '',//D07
			'numero_dar'                     => '',//D08 Numero documento arrecada��o da Recxeita= DAR
			'emissao_data_dar'               => '',//D09 Formato "AAAA-MM-DD"
			'valor_dar'                      => '',//D10
			'reparticao_fiscal_emitente'     => '',//D11
			'pagamento_data_dar'             => '' //D12
		);
		*/
		//Fim do bloco D
		/**********************************************************/
		//E-Indentifica��o do Destinat�rio da Nota Fiscal Eletr�nica
		
		if($documento_tipo == 'pedido'){
			$sql 		= 'SELECT * FROM tblcliente WHERE fldId = ' . $rowNFe['fldCliente_Id'];
			$rsCliente 	= mysql_query($sql);
			$rowDestinatario = mysql_fetch_array($rsCliente);
			$destinatario = array(
				'id'								=> 'E',
				'nome'                         		=> acentoRemover($rowDestinatario['fldNome']),//E04 Raz�o Social ou nome do destinat�rio
				'inscricao_estadual'           		=> ($rowDestinatario['fldTipo'] == 2)?format_cnpj_in($rowDestinatario['fldRG_IE']): 'ISENTO' ,//E17
				'inscricao_suframa'            		=> $rowDestinatario['fldSuframa_Inscricao'],//E18
				'email'                        		=> $rowDestinatario['fldEmail'],//E19
			);
		}else{
			$sql 			= 'SELECT * FROM tblfornecedor WHERE fldId = ' . $rowNFe['fldFornecedor_Id'];
			$rsFornecedor 	= mysql_query($sql);
			$rowDestinatario= mysql_fetch_array($rsFornecedor);
			$destinatario = array(
				'id'								=> 'E',
				'nome'                         		=> acentoRemover($rowDestinatario['fldRazaoSocial']),//E04 Raz�o Social ou nome do destinat�rio
				'inscricao_estadual'           		=> ($rowDestinatario['fldTipo'] == 2)?format_cnpj_in($rowDestinatario['fldIE']): 'ISENTO' ,//E17
				'inscricao_suframa'            		=> $rowDestinatario['fldSuframa_Inscricao'],//E18
				'email'                        		=> $rowDestinatario['fldEmail'],//E19
			);
		}
		
		
		
		/**********************************************************/
		//Linha E02
		if($rowDestinatario['fldTipo'] == '1' ){
			$destinatario_documento = array(
				'id'								=> 'E03',
				'cpf_cnpj'                 			=> fnc_number_only($rowDestinatario['fldCPF_CNPJ'])//E02 ou E03 - CNPJ ou CPF
			);
		}elseif($rowDestinatario['fldTipo'] == '2'){
			$destinatario_documento = array(
				'id'								=> 'E02',
				'cpf_cnpj'                 			=> fnc_number_only($rowDestinatario['fldCPF_CNPJ'])
			);
		}
		/**********************************************************/
		//Linha E05
		$destinatario_endereco = array(
			'id'								=> 'E05',
			'logradouro'                   		=> acentoRemover($rowDestinatario['fldEndereco']),				//E06
			'numero'                       		=> acentoRemover($rowDestinatario['fldNumero']),					//E07
			'complemento'                  		=> acentoRemover($rowDestinatario['fldComplemento']),			//E08
			'bairro'                       		=> acentoRemover($rowDestinatario['fldBairro']),					//E09
			'municipio_codigo'             		=> $rowDestinatario['fldMunicipio_Codigo'],						//E10 Utilizar a tabela do IBGE
			'municipio_nome'               		=> acentoRemover(fnc_ibge_municipio($rowDestinatario['fldMunicipio_Codigo'])),	//E11
			'uf'                           		=> fnc_ibge_uf_sigla($rowDestinatario['fldMunicipio_Codigo']),	//E12
			'cep'                   			=> fnc_number_only($rowDestinatario['fldCEP']),					//E13 Informar os Zeros n�o significativos
			'pais_codigo'                  		=> '1058',													//E14
			'pais_nome'                    		=> 'BRASIL',												//E15 Brasil ou BRASIL
			'telefone'                     		=> fnc_number_only($rowDestinatario['fldTelefone1'])				//E16 Ulitizar o DDD+ o numero do telefone, e nas operqa��es com exterior � permitido informar o c�digo do pa�s + codigo localidade + numero telefone
		);
		
		//FACO UM ARRAY PRA VERIFICACAO DOS ENDERECOS, SE � DIFERENTE DA ENTREGA *****************************************************/
		$cliente_endereco = array(
			'logradouro'                   		=> acentoRemover($rowDestinatario['fldEndereco']),				//E06
			'numero'                       		=> acentoRemover($rowDestinatario['fldNumero']),					//E07
			'complemento'                  		=> acentoRemover($rowDestinatario['fldComplemento']),			//E08
			'bairro'                       		=> acentoRemover($rowDestinatario['fldBairro']),					//E09
			'municipio_codigo'             		=> $rowDestinatario['fldMunicipio_Codigo'],						//E10 Utilizar a tabela do IBGE
			'municipio_nome'               		=> acentoRemover(fnc_ibge_municipio($rowDestinatario['fldMunicipio_Codigo'])),	//E11
			'uf'                           		=> fnc_ibge_uf_sigla($rowDestinatario['fldMunicipio_Codigo']),	//E12
		);	
		
		/**************************************************************************************************************************/
		
		//fim do bloco E
		/**********************************************************/
		/*
		//F-Indentifica��o Local de Retirada // n�o esta sendo usado no momento
		 
		$retirada = array( 
		
			'cnpj'                           => '321654654321',//F02
			'cpf'                            => '3216489732155',//F02a
			'logradouro'                     => 'Av Das Flores',//F03
			'numero'                         => '321',//F04
			'complemento'                    => '-',//F05
			'bairro'                         => 'Tiradentes',//F06
			'municipio_codigo'               => '35',//F07 Utilizar a tabela do IBGE
			'municipio_nome'                 => 'Itapira',//F08
			'uf'                             => 'SP'//F09
		);
		*/
		//fim do bloco F
		/**********************************************************/
		if($rowNFe['fldentrega_diferente_destinatario'] == '1'){
			//G-Indentifica��o do local de Entrega
			$entrega = array(
				'id'								=> 'G',
				'logradouro'                     	=> acentoRemover($rowNFe['fldentrega_endereco']),								//G03
				'numero'                         	=> acentoRemover($rowNFe['fldentrega_numero']),									//G04
				'complemento'                    	=> acentoRemover($rowNFe['fldentrega_complemento']),							//G05
				'bairro'                         	=> acentoRemover($rowNFe['fldentrega_bairro']),									//G06
				'municipio_codigo'               	=> acentoRemover($rowNFe['fldentrega_municipio_codigo']),						//G07 Utilizar a tabela do IBGE
				'municipio_nome'                 	=> acentoRemover(fnc_ibge_municipio($rowNFe['fldentrega_municipio_codigo'])),	//G08
				'uf'                             	=> acentoRemover(fnc_ibge_uf_sigla($rowNFe['fldentrega_municipio_codigo'])),	//G09
			);

			$entrega_cpf_cnpj = fnc_number_only($rowNFe['fldentrega_cpf_cnpj']);
			if(strlen($entrega_cpf_cnpj) <= 13){
				$entrega_documento = array( //CPF
					'id'								=> 'G02a',
					'cpf_cnpj'							=> $entrega_cpf_cnpj //G02
				);
			}else{
				$entrega_documento = array( //CNPJ
					'id'								=> 'G02',	
					'cpf_cnpj'							=> $entrega_cpf_cnpj //G02
				);
			}
		}
		/**********************************************************
		//VAMOS COLOCAR A PRINCIPIO O MESMO DOCUMENTO DO CLIENTE, JA QUE POR ENQUANTO NAO VAI PRECISAR MEXER = >
		//Linha G02
		$entrega_documento = array(
			'id'								=> 'G02',
			'cpf_cnpj'                          => '1321321321321'//G02a
		);
		//fim do bloco G
		if($rowCliente['fldTipo'] == 1 ){
			$entrega_documento = array(
				'id'								=> 'G02a',
				'cpf_cnpj'                 			=> fnc_number_only($rowCliente['fldCPF_CNPJ'])//E02 ou E03 - CNPJ ou CPF
			);
		}elseif($rowCliente['fldTipo'] == 2){
			$entrega_documento = array(
				'id'								=> 'G02',
				'cpf_cnpj'                 			=> fnc_number_only($rowCliente['fldCPF_CNPJ'])
			);
		}
		/**********************************************************/
		//H - Detalhamento de Produtos e Servi�os da NF-e
		
		if($documento_tipo == 'pedido'){
			$filtro_servico = ' AND fldProduto_Tipo_Id = 5';
			$filtro_item 	= ' AND fldProduto_Tipo_Id != 5';
		}
		$sql = "SELECT SUM((fldValor * fldQuantidade) * (fldExcluido * -1 +1 )) AS fldTotalServico
				FROM tbl{$documento_tipo}_item 
				WHERE fld{$documento_tipo}_Id = ".$rowNFe['fldpedido_id'].$filtro_servico;
		$rowServico = mysql_fetch_array(mysql_query($sql));
		$totalServicos = $rowServico['fldTotalServico'];
		
		$sql = "SELECT tblproduto.fldCodigo, tbl{$documento_tipo}_item.*, tblpedido_item_fiscal.* 
				FROM tbl{$documento_tipo}_item 
				INNER JOIN tblpedido_item_fiscal ON tbl{$documento_tipo}_item.fldId = tblpedido_item_fiscal.fldItem_Id
				INNER JOIN tblproduto ON tbl{$documento_tipo}_item.fldProduto_Id = tblproduto.fldId
				WHERE fld{$documento_tipo}_Id = " . $rowNFe['fldpedido_id'].$filtro_item;
		$rsItem = mysql_query($sql);
		echo mysql_error();
		$rows 	= mysql_num_rows($rsItem);
		#VERIFICA SE NOTA TEM DESCONTO TOTAL PARA DISTRIBUIR ENTRE PRODUTOS
		if($rowNFe['fldDesconto'] > 0){
			
			$descontoTotal 	= number_format(($rowNFe['fldDesconto'] / 100 ) * $rowNFe['fldnfe_total'],2,".","");
			/*
			//$desconto		= toFixed($descontoTotal / $rows);
			$desconto 		= toFixed(number_format($descontoTotal / $rows,2,".",""));
			*/
			$desconto_porcento = $rowNFe['fldDesconto'];
		}else{
			$descontoTotal 	= number_format($rowNFe['fldDescontoReais'],2,".","");
			//$desconto 		= toFixed($descontoTotal / $rows);
			$desconto		= toFixed(number_format($descontoTotal / $rows,2,".",""));
		}
		
		while($rowItem = mysql_fetch_array($rsItem)){
			
			//contador de item
			$item_numero += 1;
			
			$item_item 	 = array(
				'id'								=> 'H',
				'numero'               				=> $item_numero,							//H02 - n�mero do item
				'informacoes_adicionais'      		=> $rowItem['fldinformacoes_adicionais'],										//V01 - nfAdProd - Informa��es Adicionais do Produto
			);
			
			if(isset($desconto_porcento)){
				$desconto = number_format(($desconto_porcento / 100 ) * $rowItem['fldValor'],2,".","");
			}
			$descontoAcumulado += $desconto;
			if($rows == $item_numero) { $desconto = $desconto + bcsub($descontoTotal, $descontoAcumulado, 2); } //valores quebrados da divis�o do desconto por itens � jogado no �ltimo item
			
			//I01 - prod - TAG de grupo do detalhamento de Produtos e Servi�os da NF-e
			$item_tag = array(
				'id'								=> 'I',
				'codigo'               				=> $rowItem['fldCodigo'],					//I02 - cProd - C�digo do produto ou servi�o
				'ean'      							=> $rowItem['fldean'],						//I03 - cEAN - GTIN (Global Trade Item Number)
				'descricao'      					=> substr(strtr($rowItem['fldDescricao'], '|', '/'),0,120),				//I04 - xProd - Descri��o do Produto ou Servi�o
				'ncm'      							=> $rowItem['fldncm'],						//I05 - NCM - C�digo NCM (8 posi��es), informar o g�nero (posi��o do cap�tulo do NCM) quando a opera��o n�o for de com�rcio
																								//exterior (importa��o/ exporta��o) ou o produto n�o seja tributado pelo IPI. Em caso de servi�o informar o c�digo 99 (v2.0)
				'ex_tipi'							=> $rowItem['fldex_tipi'],					//I06 - EXTIPI
				'cfop'      						=> $rowItem['fldcfop'],						//I08 - CFOP - C�digo Fiscal de Opera��es e Presta��es
				'unidade_comercial'      			=> $rowItem['fldunidade_comercial'],		//I09 - uCom - Unidade Comercial
				'quantidade_comercial'     			=> $rowItem['fldquantidade_comercial'],		//I10 - qCom - Quantidade Comercial
				'valor_unitario_comercial'  		=> $rowItem['fldvalor_unitario_comercial'],	//I10a - vUnCom - Valor Unit�rio de Comercializa��o - Informar o valor unit�rio de comercializa��o do produto, campo meramente informativo,
																								//o contribuinte pode utilizar a precis�o desejada (0-10 decimais). Para efeitos de c�lculo,
																								//o valor unit�rio ser� obtido pela divis�o do valor do produto pela quantidade comercial. (v2.0)
				'subtotal'      					=> $rowItem['fldvalor_total_bruto'],					//I11 - vProd - Valor total Bruto dos Produtos
				'ean_unidade_tributavel'      		=> $rowItem['fldean_unidade_tributavel'],	//I12 - cEANTrib
				'unidade_tributavel'      			=> $rowItem['fldunidade_tributavel'],		//I13 - uTrib - Unidade Tribut�vel
				'quantidade_tributavel'    			=> $rowItem['fldquantidade_tributavel'],	//I14 - qTrib - Quantidade Tribut�vel
				'valor_unitario_tributavel'  		=> $rowItem['fldvalor_unitario_tributavel'],	//I14a - vUnTrib - Valor Unit�rio de Tributa��o
				'frete'      						=> format_number_zero_retornar_branco($rowItem['fldfrete']),					//I15 - vFrete - Valor Total do Frete
				'seguro'      						=> $rowItem['fldseguro'],					//I16 - vSeg - Valor Total do Seguro
				'desconto'      					=> format_number_zero_retornar_branco(number_format(($rowItem['flddesconto'] + $desconto), 2, '.', '')),					//I17 - vDesc - Valor do Desconto
				'despesas_acessorias'      			=> $rowItem['flddespesas_acessorias'],		//I17a - vOutro - Outras Despesas Acess�rias
				'valor_compoe_total'      			=> $rowItem['fldvalor_bruto_compoe_total']		//I17b - indTot - Indica se valor do Item (vProd entra no valor total da NF-e (vProd)
			);
			
			/**********************************************************/
			
			// J- Detalhamento Especifico de Veiculos Novos
			/*
			$veiculos = array(
				
			  //'produto'                         => '',//I01 Grupo detalhamento de Veiculos novos  Informar apenas quando se tratar de veiculos novos
				'tipo_operacao'                   => '1',//J02 1-Venda Concession�ria, 2-Faturamento Direto para consumidor final, 3-Venda direta para grandes consumidores(frotista, governo, ...)
				'chassi'                          => '102030',//J03 Numero do chassi do veiculo
				'codigo_cor'                      => '202020',//J04 C�digo de cada montadora
				'descricao_cor'                   => 'amrelo',//J05 
				'potencia_motor'                  => '150',//J06 Pot�mcia do motor (CV)
				'cilindradas'                     => '150',//J07 CC
				'peso_liquido'                    => '0159',//J08 Peso em toneladas-4 casas decimais
				'peso_bruto'                      => '0159',//J09 Peso bruto total- Em toneladas 4 casas decimais
				'numero_serie'                    => '00023',//J10
				'tipo_combustivel'                => '02',//J11 Usar tabela RENAVAM -01 �lcool, 02-Gasolina 03-Diesel(...) 16-�lcool/Gasolina 17-Gasolina/�lcool/GNV 18-Gasolina
				'numero_motor'                    => '25252525',//J12
				'capacidade_tracao'               => '100',//J13 Capacidade Maxima de Tra��o
				'distancia_eixos'                 => '2,0',//J14 Dist�ncia entre eixos Em metros 4 casas
				'modelo_ano'                      => '1994',//J16 Ano modelo de fabrica��o
				'fabricacao_ano'                  => '1994',//J17 Ano de Fabrica��o
				'tipo_pintura'                    => 'opaco',//J18
				'tipo'                            => 'moto',//I19 Usar tabela RENAVAM Ex: 06- AUTOMOVEL...
				'especie'                         => '1',//J20 Usar tabela RENAVAM Ex: 1-Passageiro 2-Carga...
				'condicao_chassi'                 => '102030',//J21 Informa-se o veiculo tem VIN-(chassi) R-Remarcado N-Normal
				'condicao'                        => '1',//J22 1-Acabado 2-Inacabado 3-Semi-acabado
				'codigo_marca_modelo'             => '123456',//J23 Utilizar tabela segundo RENAVAM
				'codigo_cor'                      => '102030',//J24 Segundo as regras de pr�-cadastro do DENATRAM Ex: 01-Amarelo 02-Azul...
				'lotacao'                         => '2',//J25 Quantidade M�xima permitida de passadeiros sentados, inclusive motorista.    
				'restricao'                       => '0'//J26 0-N�o h� 1-Alien��o Fiduci�ria 2-Arrendamento Mercantil 3-Reseva de dominio 4- Penhor de Veiculos 9-Outros
			);
			
			//fim do bloco J
			/**********************************************************/
			
			// K-Detalhamento Especifico de Medicamento e de mat�rias-primas farmac�uticas
			/*
			$medicamentos = array(
				
				'numero_fardo'                     => '',//K02  
				'quantidade_fardo'                 => '',//K03
				'fabricacao_data'                 => '',//K04
				'validade_data'                   => '',//K05
				'preco'                           => ''//K06 Pre�o m�ximo consumidor  
			);
			//Fim do bloco K
			/**********************************************************/
			
			//L-Detalhamento Especifico de Armamento
			/*
			$arma =array(
				
				'tipo'                            => '',//L02 Indicador do tipo de arma de fogo  0-Permitido 1-Restrito
				'numero_serie'                    => '',//L03 N�mero de serie da arma
				'numero_serie_cano'               => '',//L04 N�mero de serie do cano
				'descricao'                       => ''//L05 Descri��o completa da arma Ex: calibre, marca, capacidade...
			);
			//Fim do bloco L
			/**********************************************************/
			
			//L01- Detalhamento Especifico de Combustiveis
			if($rowItem['fldTipo_Especifico'] == '1'){ //combustivel
				$sqlANP = mysql_query("SELECT fldCodigo FROM tblproduto_fiscal_anp WHERE fldId = ".$rowItem['fldCombustivel_Codigo_ANP']);
				$rowANP = mysql_fetch_array($sqlANP);
				
				$sqlUF = mysql_query("SELECT fldSigla FROM tblibge_uf WHERE fldCodigo = ".$rowItem['fldCombustivel_UF']);
				$rowUF = mysql_fetch_array($sqlUF);
				$item_combustivel = array(
					'id'							=> 'L01',
					'codigo'                        => $rowANP['fldCodigo'],														//L102 C�digo do produto da ANP
					'codigo_autorizacao'            => format_number_zero_retornar_branco($rowItem['fldCombustivel_Codigo_CodIF']),	//L103 C�digo de Autoriza��o / registro CODIF
					'quantidade_temperatura'        => format_number_zero_retornar_branco($rowItem['fldCombustivel_Qtd_Faturada']),	//L104 informar quando a quantidade faturada informada no campo(I10) tiver sido ajustada para uma temperatura diferente da ambiente
					'uf_consumo'                    => $rowUF['fldSigla']						//L120 Uf do local de consumo 
					//'grupo_cide'                  => '',										//L105 Grupo das informa��es da CIDE(Contribui��o de Interven��o no Dom�nio Econ�mico)
					//'quantidade_base_calculo'     => $rowItem['fldvalor_total_bruto'],		//L106 Informar o BC(Base Calculo) da CIDE(Contribui��o de Interven��o no Dom�nio Econ�mico)
					//'valor_aliquota'              => $rowItem['fldvalor_total_bruto'],		//L107 Informar o valor da Aliquota da CIDE em reais
					//'valor_cide'                  => $rowItem['fldvalor_total_bruto'],		//L108 Informar o valor da CIDE   
				);
				
				//Fim do bloco L1
				/**********************************************************/
				if($rowItem['fldCombustivel_Cide_bCalculo'] > 0){
					
					$item_combustivel_cide = array(
						'id'							=> 'L05',
						'cide_base_calculo'     		=> $rowItem['fldCombustivel_Cide_bCalculo'],		//L106 Informar o BC(Base Calculo) da CIDE(Contribui��o de Interven��o no Dom�nio Econ�mico)
						'cide_valor_aliquota'           => $rowItem['fldCombustivel_Cide_Aliquota'],		//L107 Informar o valor da Aliquota da CIDE em reais
						'cide_valor_cide'               => $rowItem['fldCombustivel_Cide_Valor']		//L108 Informar o valor da CIDE   
					);
				}
			}
			//Fim do bloco L1
			/**********************************************************/
			
			//M - Tributos incidentes no Produto ou Servi�o
			if($export_txt == true){
				$item_tributos = array(
					'id'						=> 'M',
					'total_tributo'				=> format_number_in(format_number_out($rowItem['fldTotal_Tributo']))
				);
			} else {
				$item_tributos = array(
					'id'						=> 'M'
				);
			}
				//M01 - imposto - Grupo de Tributos incidentes no Produto ou Servi�o
				//O grupo ISSQN � mutuamente exclusivo com os grupos ICMS, IPI e II, isto � se ISSQN for informado os grupos ICMS, IPI e II n�o ser�o informados e vice-versa (v2.0).
					
					if($rowItem['fldrad_imposto'] == 'icms'){
						//----------------------------------------------------------------------
						//N - ICMS Normal e ST
						$item_tributos_icms = array(
							'id'				=> 'N'
						);
							//chavear icms
							//****************************************************************************************************************
							
							//C�digo Fiscal a ser carregado
							//die('sitauacao trib' . $rowItem['fldicms_situacao_tributaria']);
							$sql 	 = "SELECT * FROM tblnfe_notafiscal_icms WHERE fldId = '" . $rowItem['fldicms_situacao_tributaria'] . "'";
							$rsICMS  = mysql_query($sql);
							$rowICMS = mysql_fetch_array($rsICMS);
						
							//Listar campos correspondentes
							$sql 	 = "SELECT * FROM tblnfe_icms_campo";
							$rsCampo = mysql_query($sql);
							$campos  = array();
							$arrayGrade = "";
							while($rowCampo = mysql_fetch_array($rsCampo)){
								
								$Campo = $rowCampo["fldCampo"];
								//CASO DE CST OU CSOSN seria ou um ou outro por isso ambos tem descri��o do campo longo de situacao_tributaria
								$show = ($rowICMS["fld$Campo"] ? true : false);
								$arrayGrade[$Campo] = array(
															"Ordem" 		=> $rowICMS["fld$Campo"],
															"Campo"			=> $rowCampo["fldCampo"],
															"CampoLongo"	=> $rowCampo["fldCampo_Longo"],
															'show'			=> $show
														   );
							}
							if(count($arrayGrade)){
								//Colocar em ordem de exibi��o usando o Sort
								sort($arrayGrade);
								//id da linha
								$item_tributos_icms_linha['id'] = $rowICMS['fldnfe_linha_id'];
								//demais dados da linha
								
								foreach($arrayGrade as $Item){
									$campo = $Item['CampoLongo'];
									//limpando para n�o encavalar campos setados no item anterior - corrigido em 28/12/2012
									unset($item_tributos_icms_linha[$campo]);
									
									if($Item['show']){
									
										if($campo == 'situacao_tributaria'){
											$rsCST 	= mysql_query('select * from tblnfe_notafiscal_icms where fldId = ' . $rowItem["fldicms_situacao_tributaria"]);
											$rowCST = mysql_fetch_array($rsCST);
											$item_tributos_icms_linha[$campo] = $rowCST['fldSituacaoTributaria'];
											
										}elseif($campo == 'base_calculo_modalidade'){
											$rsMODBC  = mysql_query('select * from tblnfe_icms_campo_modbc where fldId = ' . $rowItem["fldicms_base_calculo_modalidade"]);
											$rowMODBC = mysql_fetch_array($rsMODBC);
											$item_tributos_icms_linha[$campo] = $rowMODBC['fldCodigo'];
											
										}
										else{
											//101
											if(isset($rowItem["fldicms_$campo"])){
												
												$item_tributos_icms_linha[$campo] = $rowItem["fldicms_$campo"];
												//unset($rowItem["fldicms_$campo"]);
											}
											//202 ou 500
											elseif(isset($rowItem["fldicms$campo"])){
												if($campo == 'st_base_calculo_percentual_reducao'){
													$item_tributos_icms_linha[$campo] = format_number_zero_retornar_branco($rowItem["fldicms$campo"]);
												}elseif($campo == 'st_base_calculo_modalidade'){
													$rsMODBCST 	= mysql_query('select * from tblnfe_icms_campo_modbcst where fldId = ' . $rowItem["fldicmsst_base_calculo_modalidade"]);
													$rowMODBCST = mysql_fetch_array($rsMODBCST);
													$item_tributos_icms_linha[$campo] = $rowMODBCST['fldCodigo'];	
												//}elseif($campo == 'st_base_calculo_retido_uf_remetente' ||$campo == 'st_valor_retido_uf_remetente'){
													//$item_tributos_icms_linha[$campo] = fnc_format_dois_zeros_retornar_zero($rowItem["fldicms$campo"]);
												}else{
													$item_tributos_icms_linha[$campo] = $rowItem["fldicms$campo"];
												}
												//unset($rowItem["fldicms$campo"]);
											}
											
										}
									}
									$Item['show'] = 0;
									
								}
							}
						//fim do ICMS
						//----------------------------------------------------------------------
						
						//O - IPI - Imposto sobre Produto Industrializado
						//Informar apenas quando o item for sujeito ao IPI
						#IF COLOCADO POIS NA HORA DE GERAR O XML ESTAVA APARECENDO IPI MESMO NAO TENDO, E DAVA ERRO NA IMPORTA��O DO XML
						#VOU TESTAR DEPOIS TRANSMITINDO DIRETO DO SISTEMA, CASO NAO DE O MESMO ERRO EU RETIRO O IF
						if($rowItem['fldipi_valor'] > 0){
							$item_tributos_ipi = array(
								'id'								=> 'O',
								'enquadramento_classe'				=> $rowItem['fldipi_enquadramento_classe'],				//Classe de enquadramento do IPI para cigarros e bebidas
								'produtor_cnpj'						=> $rowItem['fldipi_produtor_cnpj'],					//
								'selo_controle_codigo'				=> format_number_zero_retornar_branco($rowItem['fldipi_selo_controle_codigo']),	//
								'selo_controle_quantidade'			=> format_number_zero_retornar_branco($rowItem['fldipi_selo_controle_quantidade']),
								'enquadramento_legal_codigo'		=> $rowItem['fldipi_enquadramento_codigo']				//Tabela a ser criada pela RFB, informar 999 enquanto a tabela n�o for criada
							);
							
							if($rowItem["fldipi_situacao_tributaria"] > 0){
								//chavear IPI
								//Informar apenas um dos grupos O07 ou O08 com base valor atribu�do ao campo O09 � CST do IPI
								$rsIPI_CST = mysql_query('select * from tblnfe_ipi where fldId = ' . $rowItem["fldipi_situacao_tributaria"]);
								$rowIPI_CST = mysql_fetch_array($rsIPI_CST);
								echo mysql_error();
								switch($rowIPI_CST['fldSituacaoTributaria']){
									//Grupo do CST 00, 49, 50 e 99
									case '00':
									case '49':
									case '50':
									case '99':
										$item_tributos_ipi_linha = array(
											'id'								=> 'O07',									//O07 - Grupo do CST 00, 49, 50 e 99
											'situacao_tributaria_codigo'		=> $rowIPI_CST['fldSituacaoTributaria'],		//O09 - 00-Entrada com recupera��o de cr�dito; 49-Outras entradas; 50-Sa�da tributada; 99-Outras sa�das
											'valor'								=> $rowItem['fldipi_valor']					//O14
										);
											//Informar os campos O10 e O13 caso o c�lculo do IPI seja por al�quota
											//ou os campos O11 e O12 caso o c�lculo do IPI seja valor por unidade.
											if($rowItem['fldipi_tipo_calculo'] == 1){ //1 - percentual; 2 - valor
												$item_tributos_ipi_linha_calculo = array(
													'id'								=> 'O10',
													'base_calculo'						=> $rowItem['fldipi_base_calculo'],			//O10
													'aliquota'							=> $rowItem['fldipi_aliquota'],				//O13
												);
											}
											else{
												$item_tributos_ipi_linha_calculo = array(
													'id'								=> 'O11',
													'unidade_padrao_quantidade_total'	=> $rowItem['fldipi_unidade_padrao_quantidade_total'],	//O11
													'unidade_valor'						=> $rowItem['fldipi_unidade_valor'],					//O12
												);
											}
										break;
									//Grupo do CST 01, 02, 03, 04, 51, 52, 53, 54 e 55
									case '01':
									case '02':
									case '03':
									case '04':
									case '51':
									case '52':
									case '53':
									case '54':
									case '55':
										$item_tributos_ipi_linha = array(
											'id'								=> 'O08',									//O08 - Grupo do CST 01, 02, 03, 04, 51, 52, 53, 54 e 55
											'situacao_tributaria'				=> $rowIPI_CST['fldSituacaoTributaria'],	//O09
											'valor'								=> format_number_zero_retornar_branco($rowItem['fldipi_valor']),					//O14
										);
										break;
								}
							}
						}
						//fim do IPI
						//----------------------------------------------------------------------
						
						//P - II - Imposto de Importa��o
						if($rowItem['fldii_valor'] > 0){
							$item_tributos_ii = array(
								'id'								=> 'P',
								'base_calculo'						=> $rowItem['fldii_base_calculo'],						//P02
								'despesas_aduaneiras'				=> $rowItem['fldii_despesas_aduaneiras_valor'],			//P03
								'imposto_importacao'				=> $rowItem['fldii_valor'],								//P04
								'imposto_operacoes_financeiras'		=> $rowItem['fldii_iof_valor'],							//P05
							);
						}
						//fim do II
						//----------------------------------------------------------------------
					}
					else{
						//U - ISSQN --> do P pula para o U mesmo!!! WTF RFB!!!
						//Informar os campos para c�lculo do ISSQN nas NFe conjugadas, onde h� a presta��o de servi�os sujeitos ao ISSQN e fornecimento de pe�as sujeitas ao ICMS.
						//O grupo de ISSQN � mutuamente exclusivo com os grupos ICMS, IPI e II, isto �, se ISSQN for informado os grupos ICMS, IPI e II n�o ser�o informados e vice-versa (v2.0).
						$item_tributos_issqn = array(
							'id'								=> 'U',
							'base_calculo'						=> $rowItem['fldissqn_base_calculo'],					//U02
							'aliquota'							=> $rowItem['fldissqn_aliquota'],						//U03
							'valor'								=> $rowItem['fldissqn_valor'],							//U04
							'municipio_ocorrencia'				=> $rowItem['fldissqn_municipio_ocorrencia'],			//U05 - C�digo do munic�pio de ocorr�ncia do fato gerador do ISSQN
							'servico'							=> $rowItem['fldissqn_servico'],						//U06
							'tributacao'						=> $rowItem['fldissqn_tributacao'],						//U07 - Informar o c�digo da tributa��o do ISSQN: N � NORMAL; R � RETIDA; S �SUBSTITUTA;  I � ISENTA. (v.2.0)
						);
						//fim do ISSQN
						//----------------------------------------------------------------------
					}
					
					//Q - PIS
					//if($rowItem['fldpis_valor'] > 0){
						$item_tributos_pis = array(
							'id'								=> 'Q'
						);
							//chavear PIS
							//Informar apenas um dos grupos Q02, Q03, Q04 ou Q05 com base valor atribu�do ao campo Q06 � CST do PIS
							$rsPIS_CST = mysql_query('select * from tblnfe_pis where fldId = ' . $rowItem["fldpis_situacao_tributaria"]);
							$rowPIS_CST = mysql_fetch_array($rsPIS_CST);
							
							
							switch($rowPIS_CST['fldSituacaoTributaria']){
								case '01':
								case '02':
									//Q02|CST|VBC|PPIS|VPIS|
									$item_tributos_pis_linha = array(
										'id'						=> 'Q02',
										'situacao_tributaria'		=> $rowPIS_CST['fldSituacaoTributaria'],	//Q06 - 01 � Opera��o Tribut�vel (base de c�lculo = valor da opera��o al�quota normal (cumulativo/n�o cumulativo));
																													//02 - Opera��o Tribut�vel (base de c�lculo = valor da opera��o (al�quota diferenciada));
										'base_calculo'				=> $rowItem['fldpis_base_calculo'],			//Q07
										'aliquota'					=> $rowItem['fldpis_aliquota'],				//Q08
										'valor'						=> $rowItem['fldpis_valor']					//Q09
									);
									break;
								case '03':
									//Q03|CST|QBCProd|VAliqProd|VPIS|
									$item_tributos_pis_linha = array(
										'id'						=> 'Q03',
										'situacao_tributaria'		=> $rowPIS_CST['fldSituacaoTributaria'],	//Q06
										'quantidade_vendida'		=> $rowItem['fldpis_quantidade_vendida'],	//Q10
										'aliquota_reais'			=> $rowItem['fldpis_aliquota_reais'],		//Q11
										'valor'						=> $rowItem['fldpis_valor']					//Q09
									);
									break;
								case '04':
								case '06':
								case '07':
								case '08':
								case '09':
									//Q04|CST|
									$item_tributos_pis_linha = array(
										'id'						=> 'Q04',
										'situacao_tributaria'		=> $rowPIS_CST['fldSituacaoTributaria']	//Q06
									);
									break;
								default :
									//Q05|CST|VPIS|
									$item_tributos_pis_linha = array(
										'id'						=> 'Q05',
										'situacao_tributaria'		=> $rowPIS_CST['fldSituacaoTributaria'],	//Q06
										'valor'						=> $rowItem['fldpis_valor']					//Q09
									);
										//Informar campos para c�lculo do PIS em percentual (P07 e P08) ou campos para PIS em valor (P10 e P11).
										if($rowItem['fldpis_tipo_calculo'] == 1){ //1 - percentual; 2 - valor
											//Q07|VBC|PPIS|
											$item_tributos_pis_linha_calculo = array(
												'id'				=> 'Q07',
												'base_calculo'		=> $rowItem['fldpis_base_calculo'],			//Q07
												'aliquota'			=> $rowItem['fldpis_aliquota']				//Q08
											);
										}
										elseif($rowItem['fldpis_tipo_calculo'] == 2){
											//Q10|QBCProd|VAliqProd|
											$item_tributos_pis_linha_calculo = array(
												'id'					=> 'Q10',
												'quantidade_vendida'	=> $rowItem['fldpis_quantidade_vendida'],	//Q07
												'aliquota_reais'		=> $rowItem['fldpis_aliquota_reais']		//Q08
											);
										}
									break;
							}
					//}
					//fim do PIS
					//----------------------------------------------------------------------
					//R - PIS ST
					if($rowItem['fldpisst_valor'] > 0){
						$item_tributos_pisst = array(
							'id'										=> 'R',
							'valor'										=> $rowItem['fldpisst_valor']				//R06
						);
						//Informar campos para c�lculo do PIS em percentual (R02 e R03) ou campos para PIS em valor (R04 e R05).
						if($rowItem['fldpisst_tipo_calculo'] == 1){ //1 - percentual; 2 - valor
							//R02|VBC|PPIS|
							$item_tributos_pisst_calculo = array(
								'id'								=> 'R02',
								'base_calculo'						=> $rowItem['fldpisst_base_calculo'],			//R02
								'aliquota'							=> $rowItem['fldpisst_aliquota_percentual']		//R03
							);
						}
						elseif($rowItem['fldpisst_tipo_calculo'] == 2){
							//R04|QBCProd|VAliqProd|
							$item_tributos_pisst_calculo = array(
								'id'								=> 'R04',
								'quantidade_vendida'				=> $rowItem['fldpisst_quantidade_vendida'],		//R04
								'aliquota_reais'					=> $rowItem['fldpisst_aliquota_reais']			//R05
							);
						}
					}
					//fim do PIS ST
					//----------------------------------------------------------------------
					
					//S - COFINS
					//if($rowItem['fldcofins_valor'] > 0){
						$item_tributos_cofins = array(
							'id'								=> 'S'
						);
							//chavear COFINS
							//Informar apenas um dos grupos S02, S03, S04 ou S04 com base valor atribu�do ao campo S06 � CST do COFINS
							//AQUI ESTAVA ESSA LINHA $rsCOFINS_CST = mysql_query('select * from tblnfe_ipi where fldId = ' . $rowItem["fldipi_situacao_tributaria"]);
							$rsCOFINS_CST = mysql_query('select * from tblnfe_cofins where fldId = ' . $rowItem["fldcofins_situacao_tributaria"]);
							$rowCOFINS_CST = mysql_fetch_array($rsCOFINS_CST);
							switch($rowCOFINS_CST['fldCOFINS']){
								case '01':
								case '02':
									//S02|CST|VBC|PCOFINS|VCOFINS|
									$item_tributos_cofins_linha = array(
										'id'							=> 'S02',
										'situacao_tributaria'			=> $rowCOFINS_CST['fldSituacaoTributaria'],		//S06
										'base_calculo'					=> $rowItem['fldcofins_base_calculo'],			//S07
										'aliquota'						=> $rowItem['fldcofins_aliquota_percentual'],	//S08
										'valor'							=> $rowItem['fldcofins_valor']					//S11
									);
									break;
								case '03':
									//S03|CST|QBCProd|VAliqProd|VCOFINS
									$item_tributos_cofins_linha = array(
										'id'							=> 'S03',
										'situacao_tributaria'			=> $rowCOFINS_CST['fldCOFINS'],	//S06
										'quantidade_vendida'			=> $rowItem['fldcofins_quantidade_vendida'],	//S09
										'aliquota_reais'				=> $rowItem['fldcofins_aliquota_reais'],		//S10
										'valor'							=> $rowItem['fldcofins_valor']					//S11
									);
									break;
								case '04':
								case '06':
								case '07':
								case '08':
								case '09':
									//S04|CST|
									$item_tributos_cofins_linha = array(
										'id'							=> 'S04',
										'situacao_tributaria'			=> $rowCOFINS_CST['fldCOFINS']					//S06
									);
									break;
								default :
									//S05|CST|VCOFINS|
									$item_tributos_cofins_linha = array(
										'id'							=> 'S05',
										'situacao_tributaria'			=> $rowCOFINS_CST['fldCOFINS'],					//S06
										'valor'							=> $rowItem['fldcofins_valor']					//S11
									);
										//Informar  campos  para  c�lculo da COFINS em percentual (S07 e S08) ou campos para COFINS em valor (S09 e S10). 
										if($rowItem['fldcofins_tipo_calculo'] == 1){ //1 - percentual; 2 - valor
											//S07|VBC|PCOFINS|
											$item_tributos_cofins_linha_calculo = array(
												'id'					=> 'S07',
												'base_calculo'			=> $rowItem['fldcofins_base_calculo'],			//S07
												'aliquota'				=> $rowItem['fldcofins_aliquota']				//S08
											);
										}
										elseif($rowItem['fldcofins_tipo_calculo'] == 2){
											//S09|QBCProd|VAliqProd|
											$item_tributos_cofins_linha_calculo = array(
												'id'					=> 'S09',
												'quantidade_vendida'	=> $rowItem['fldcofins_quantidade_vendida'],	//S09
												'aliquota_reais'		=> $rowItem['fldcofins_aliquota_reais']			//S10
											);
										}
									break;
							}
					//}
					//fim do COFINS
					//----------------------------------------------------------------------
					
					//T - COFINS ST
					if($rowItem['fldcofinsst_valor'] > 0){
						$item_tributos_cofinsst = array(
							'id'								=> 'T',
							'valor'								=> $rowItem['fldcofinsst_valor']				//T06
						);
							//Informar campos para c�lculo do COFINS em percentual (R02 e R03) ou campos para COFINS em valor (R04 e R05).
							if($rowItem['fldcofinsst_tipo_calculo'] == 1){ //1 - percentual; 2 - valor
								//T02|VBC|PCOFINS|
								$item_tributos_cofinsst_calculo = array(
									'id'								=> 'T02',
									'base_calculo'						=> $rowItem['fldcofinsst_base_calculo'],			//T02
									'aliquota'							=> $rowItem['fldcofinsst_aliquota']					//T03
								);
							}
							else{
								//T04|QBCProd|VAliqProd|
								$item_tributos_cofinsst_calculo = array(
									'id'								=> 'T04',
									'quantidade_vendida'				=> $rowItem['fldcofinsst_quantidade_vendida'],		//T04
									'aliquota_reais'					=> $rowItem['fldcofinsst_aliquota_reais']			//T05
								);
							}
					}
					//fim do COFINS ST
					//----------------------------------------------------------------------
				
			//agrupar itens em um array
			$itens[$item_numero] = array(
				$item_item,
				$item_tag,
				$item_combustivel,
				$item_combustivel_cide,
				$item_tributos,
				
				$item_tributos_icms,
				$item_tributos_icms_linha,
				
				$item_tributos_ipi,
				$item_tributos_ipi_linha,
				$item_tributos_ipi_linha_calculo,
				
				$item_tributos_ii,
				
				$item_tributos_issqn,
				
				$item_tributos_pis,
				$item_tributos_pis_linha,
				$item_tributos_pis_linha_calculo,
				
				$item_tributos_pisst,
				$item_tributos_pisst_calculo,
				
				$item_tributos_cofins,
				$item_tributos_cofins_linha,
				$item_tributos_cofins_linha_calculo,
				
				$item_tributos_cofinsst,
				$item_tributos_cofinsst_calculo
			);
		}
		
		/**********************************************************/
		//W - Valores Totais da NF-e
		$totais = array(
			'id'										=> 'W'
		);
		
			//Grupo de Valores Totais da NF-e
			//W02|vBC|vICMS|vBCST|vST|vProd|vFrete|vSeg|vDesc|vII|vIPI|vPIS|vCOFINS|vOutro|vNF| 
			$totais_nfe = array(
				'id'									=> 'W02',
				'icms_base_calculo'						=> $rowNFe['fldicms_base_calculo'],																//W03
				'icms_total'							=> $rowNFe['fldicms_total'],																	//W04
				'icmsst_base_calculo'					=> $rowNFe['fldicmsst_base_calculo'],															//W05
				'icmsst_total'							=> $rowNFe['fldicmsst_total'],																	//W06
				'produtos_servicos_total'				=> number_format(($rowNFe['fldprodutos_servicos_total'] - $totalServicos), 2, '.', ''),			//W07
				'frete_total'							=> $rowNFe['fldfrete_total'],																	//W08
				'seguro_total'							=> $rowNFe['fldseguro_total'],																	//W09
				'desconto_total'						=> format_number_zero_retornar_branco(number_format(($rowNFe['flddesconto_total'] + $descontoTotal), 2, '.', '')),					//W10
				'ii_total'								=> $rowNFe['fldii_total'],																		//W11
				'ipi_total'								=> $rowNFe['fldipi_total'],																		//W12
				'pis_total'								=> $rowNFe['fldpis_total'],																		//W13
				'cofins_total'							=> $rowNFe['fldcofins_total'],																	//W14
				'despesas_acessorias_total'				=> $rowNFe['flddespesas_acessorias_total'],														//W15
				'nfe_total'								=> number_format(($rowNFe['fldnfe_total'] - $descontoTotal), 2, '.', ''), //number_format((($rowNFe['fldnfe_total'] - $totalServicos) - $descontoTotal), 2, '.', '')		//W16
			);
			
			if($export_txt == true){
				$totais_nfe['total_trib'] = $rowNFe['fldnfe_total_tributos'];
			}
			
			//Grupo de Valores Totais referentes ao ISSQN
			//W17|VServ|VBC|VISS|VPIS|VCOFINS|
			if($rowNFe['iss_total'] > 0){
				$totais_issqn = array(
					'id'								=> 'W17',
					'iss_nao_incidencia_total'			=> format_number_zero_retornar_branco($rowNFe['fldiss_nao_incidencia_total']),		//W18 - Valor  Total  dos  Servi�os  sob n�o-incid�ncia  ou  n�o tributados pelo ICMS
					'iss_base_calculo'					=> format_number_zero_retornar_branco($rowNFe['fldiss_base_calculo']),				//W19
					'iss_total'							=> format_number_zero_retornar_branco($rowNFe['fldiss_total']),						//W20
					'iss_pis_total'						=> format_number_zero_retornar_branco($rowNFe['fldiss_pis_total']),					//W21
					'iss_cofins_total'					=> format_number_zero_retornar_branco($rowNFe['fldiss_cofins_total'])				//W22
				);
			}
			//Grupo de Reten��es de Tributos
			//W23|VRetPIS|VRetCOFINS|VRetCSLL|VBCIRRF|VIRRF|VBCRetPrev|VRetPrev|
		
			//fiz meio na gambi pq nao achei como verificar se existe algum valor maior que zero direto na array, entao dei um unique e comparei se todos sao iguais, assim, se houver algum >0 vao ter dois valores xD
			$W23 = array(
				$rowNFe['fldretencao_pis_valor'],
				$rowNFe['fldretencao_cofins_valor'],
				$rowNFe['fldretencao_csll_valor'],
				$rowNFe['fldretencao_cirrf_valor'],
				$rowNFe['fldretencao_irrf_valor'],
				$rowNFe['fldretencao_previdencia_base_calculo'],
				$rowNFe['fldretencao_previdencia_valor']	//W30
			);
			
			$W23 = array_unique($W23);
			if( count($W23) > 1){
				$totais_retencoes = array(
					'id'								=> 'W23',
					'retencao_pis_valor'				=> $rowNFe['fldretencao_pis_valor'],						//W24
					'retencao_cofins_valor'				=> $rowNFe['fldretencao_cofins_valor'],						//W25
					'retencao_csll_valor'				=> $rowNFe['fldretencao_csll_valor'],						//W26
					'retencao_cirrf_valor'				=> $rowNFe['fldretencao_cirrf_valor'],						//W27
					'retencao_irrf_valor'				=> $rowNFe['fldretencao_irrf_valor'],						//W28
					'retencao_previdencia_base_calculo'	=> $rowNFe['fldretencao_previdencia_base_calculo'],			//W29
					'retencao_previdencia_valor'		=> $rowNFe['fldretencao_previdencia_valor']					//W30
				);
			}
		/**********************************************************/
		//X - Informa��es do Transporte da NF-e
		$transporte = array(
			'id'										=> 'X',
			'modalidade'								=> $rowNFe['fldtransporte_modalidade_codigo']				//X02
		);
		//Grupo Transportador
		//X03|XNome|IE|XEnder|UF|XMun|
		if($rowNFe['fldtransporte_modalidade_codigo'] != '9'){	 // 9 = sem frete
			$transporte_transportador = array(
				'id'									=> 'X03',
				'nome'              	         		=> acentoRemover($rowTransportador['fldNome']),									//X06
				'ie'                    	       		=> acentoRemover(format_cpf_in($rowTransportador['fldIE'])),					//X07 | USA A FUNCAO DO CPF PARA REMOVER OS PONTOS
				'endereco_completo'						=> acentoRemover($rowTransportador['fldEndereco_Completo']),					//X08 - Endere�o Completo
				'uf'                           			=> fnc_ibge_uf_sigla($rowTransportador['fldMunicipio_Codigo']),					//X09
				'municipio'								=> acentoRemover(fnc_ibge_municipio($rowTransportador['fldMunicipio_Codigo']))	//X10
			);
				//Sele��o entre X04 ou X05
				if(strlen($rowTransportador['fldCPF_CNPJ']) > 11){
					$transporte_transportador_documento = array(
						'id'								=> 'X04',
						'cnpj'                       		=> $rowTransportador['fldCPF_CNPJ']					//X04
					);
				}
				else{
					$transporte_transportador_documento = array(
						'id'								=> 'X05',
						'cpf'								=> $rowTransportador['fldCPF_CNPJ']					//X05
					);
				}
			//Grupo de Reten��o do ICMS do Transporte
			//X11|VServ|VBCRet|PICMSRet|VICMSRet|CFOP|CMunFG|
			if($rowNFe['fldtransporte_retencao_icms_valor'] > 0){
				$transporte_retencao = array(
					'id'								=> 'X11',
					'retencao_icms_servico_valor'		=> $rowNFe['fldtransporte_retencao_icms_servico_valor'],	//X12
					'retencao_icms_base_calculo'		=> $rowNFe['fldtransporte_retencao_icms_base_calculo'],		//X13
					'retencao_icms_retencao_aliquota'	=> $rowNFe['fldtransporte_retencao_icms_aliquota'],			//X14
					'retencao_icms_valor'				=> $rowNFe['fldtransporte_retencao_icms_valor'],			//X15
					'retencao_icms_cfop'				=> $rowNFe['fldtransporte_retencao_icms_cfop'],				//X16
					'retencao_icms_municipio_codigo'	=> $rowNFe['fldtransporte_retencao_icms_municipio_codigo']	//X17
				);
			}
			
			//Grupo Ve�culo
			//X18|Placa|UF|RNTC|
			$transporte_veiculo = array(
				'id'									=> 'X18',
				'placa'									=> $rowNFe['fldTransporte_Veiculo_Placa'],					//X19
				'uf'									=> $rowNFe['fldTransporte_UF'],								//X20
				'rntc'									=> $rowNFe['fldTransporte_rntc']							//X21 - Registro Nacional de Transporte de Carga
			);
				//Grupo Reboque
				//X22|Placa|UF|RNTC|
				$sql = 'select * from tblpedido_fiscal_transporte_reboque
						where fldPedido_Id = ' . $rowNFe['fldpedido_id'];
				$rsReboque = mysql_query($sql);
				while($rowReboque = mysql_fetch_array($rsReboque)){
					$transporte_reboque = array(
						'id'							=> 'X22',
						'placa'							=> $rowReboque['fldPlaca'],									//X23
						'uf'							=> $rowReboque['fldUF'],									//X24
						'rntc'							=> $rowReboque['fldRNTC']									//X25 - Registro Nacional de Transporte de Carga
					);
					
					//agrupar itens em um array
					$transporte_reboques[] = array(
						$transporte_reboque
					);
				}
				//Vag�o
				//X25a|vagao|
				$transporte_vagao = array(
					'id'								=> 'X25a',
					'vagao'								=> $rowNFe['fldTransporte_Vagao']							//X25a
				);
				//Balsa
				//X25b|balsa|
				$transporte_balsa = array(
					'id'								=> 'X25b',
					'balsa'								=> $rowNFe['fldTransporte_Balsa']							//X25a
				);
				//Grupo Volumes
				//X26|QVol|Esp|Marca|NVol|PesoL|PesoB|
				$sql = 'select * from tblpedido_fiscal_transporte_volume
						where fldPedido_Id = ' . $rowNFe['fldpedido_id'];
				$rsVolume = mysql_query($sql);
				while($rowVolume = mysql_fetch_array($rsVolume)){
					$transporte_volume = array(
						'id'							=> 'X26',
						'quantidade'					=> $rowVolume['fldQuantidade'],								//X27
						'especie'						=> $rowVolume['fldEspecie'],								//X28
						'marca'							=> $rowVolume['fldMarca'],									//X29
						'volume'						=> $rowVolume['fldVolume'],									//X30
						'peso_liquido'					=> $rowVolume['fldPeso_Liquido'],							//X31
						'peso_bruto'					=> $rowVolume['fldPeso_Bruto']								//X32
					);
					
					//agrupar itens em um array
					$transporte_volumes[] = array(
						$transporte_volume
					);
					
					//Grupo de Lacres
					//X33|NLacre|
					$sql = 'select * from tblpedido_fiscal_transporte_volume
							where fldId = ' . $rowVolume['fldId'];
					$rsLacre = mysql_query($sql);
					while($rowLacre = mysql_fetch_array($rsLacre)){
						$transporte_volume_lacre = array(
							'id'						=> 'X33',
							'numero'					=> $rowLacre['fldNumero']									//X34
						);
						
						//agrupar itens em um array
						$transporte_volume_lacres[] = array(
							$transporte_volume_lacre
						);
					}
				}
					
		}//se tipo frete != 9
		
		$rsServico = mysql_query("SELECT * FROM tblpedido_funcionario_servico WHERE fldPedido_Id = ".$rowNFe['fldpedido_id']." AND fldFuncao_Tipo = 2");
		if(!mysql_num_rows($rsServico)){
			//Y - Dados da Cobran�a
			$cobranca = array(
				'id'									=> 'Y'
			);
			
			$totalLiquido = $rowNFe['fldnfe_total'] - $descontoTotal;
			$cobranca_fatura = array(
				'id'									=> 'Y02',
				'numero'                    	   		=> '', // devolver depois de testes, estava dando erro - $rowNFe['fldnumero'],										//Y03
				'valor_original'						=> $rowNFe['fldnfe_total'],										//Y04
				'desconto'                      		=> format_number_zero_retornar_branco(number_format($descontoTotal, 2, '.', '')),				//Y05
				'valor_liquido'							=> number_format($totalLiquido,2,".","")						//Y06
			);
			
			//Grupo da Duplicata
			//Y07|NDup|DVenc|VDup|
			//adicionado (provavelmente novamente, parece que foi retirado) o parametro que adiciona ou n�o as duplicatas
			if(fnc_sistema("nfe_exibir_duplicatas") == "1"){
				$sql = "select * from tbl{$documento_tipo}_parcela where fld{$documento_tipo}_Id = " . $rowNFe['fldpedido_id'];
				$rsParcela = mysql_query($sql);
				while($rowParcela = mysql_fetch_array($rsParcela)){
					$cobranca_duplicata = array(
						'id'								=> 'Y07',
						'numero'                       		=> $rowNFe['fldnumero'] . '/' . $rowParcela['fldParcela'],		//Y08
						'vencimento'						=> $rowParcela['fldVencimento'],								//Y09
						'valor'								=> $rowParcela['fldValor']										//Y10
					);	
					
					//agrupar itens em um array
					$cobranca_duplicatas[] = array(
						$cobranca_duplicata
					);
				}
			}
			
		}
		//----------------------------------------------------------------------
		
		$adicionais = array(
			$rowNFe['fldinfo_adicionais_fisco'],
			$rowNFe['fldinfo_adicionais_contribuinte'],
		);
		$adicionais = array_unique($adicionais);
		if(count($adicionais) > 1){
			//Z - Grupo de Informa��es Adicionais
			//Z|InfAdFisco|InfCpl|
			$informacoes_adicionais = array(
				'id'									=> 'Z',
				'informacoes_adicionais_fisco'			=> $rowNFe['fldinfo_adicionais_fisco'],		 //Z02
				'informacoes_adicionais_contribuinte'	=> $rowNFe['fldinfo_adicionais_contribuinte']//Z03
			);
		}
			
		/**********************************************************/
		//juntar arrays do cabe�alho
		$cabecalho = array(
			$versao,
			$documento,
			$emitente,
			$emitente_documento,
			$emitente_endereco,
			$destinatario,
			$destinatario_documento,
			$destinatario_endereco,
			$entrega,
			$entrega_documento
		);
		
		//juntar arrays do rodap�
		$rodape = array(
			$totais,
			$totais_nfe,
			$totais_issqn,
			$totais_retencoes,
			'transporte' => $transporte,
			$transporte_transportador,
			$transporte_transportador_documento,
			$transporte_retencao,
			$transporte_veiculo,
			$transporte_reboques,
			$transporte_vagao,
			$transporte_balsa,
			$transporte_volumes,
			$transporte_volume_lacres,
			$cobranca,
			$cobranca_fatura,
			$cobranca_duplicatas,
			$informacoes_adicionais
		);
		
		//juntar tudo
		$nfe_completa = array(
			$cabecalho,
			$itens,
			$rodape
		);
		
		$n ++;
		//gravar dados varrendo recursivamente os arrays
		array_walk_recursive($nfe_completa, 'test_print');
		//fwrite($GLOBALS['arquivo'],'controle='.$n,$saida);
		
		fclose($arquivo);
		
	}	
	####################################################################################################################################################################
	/* FUNCAO ABAIXO FOI COLOCADA NO ARQUIVO arquivo inc/fnc_nfe
	function test_print($item, $key){
		
		if($key=='id'){
			$saida = "\r\n";
		}
		 if(!intval($key)){ 
			$saida .= $item . '|'; 
			//echo $saida; 
		}
		//gravar no arquivo
		fwrite($GLOBALS['arquivo'],$saida);
	}
	
	function fnc_array_destruir($item, $key){
		$GLOBALS['excluir'][] = $item;
		
		$excluir = array();
		foreach($array as $item ){
			$excluir[] = $item;
		}
		print_r($excluir);
		foreach($excluir as $item){
			unset($item);
		}
	}
	*/

?>
