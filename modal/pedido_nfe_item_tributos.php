		
        <ul>
       		<li style="width: 20px; margin-bottom:0">
                <input type="radio" style="width:20px" id="rad_imposto" name="rad_imposto" value="icms" checked="checked" />
            </li>
            <li style="width:60px;margin-bottom:0">
                <label for="rad_imposto">ICMS</label>
            </li>
            <li style="width: 20px; margin-bottom:0">
                <input type="radio" style="width:20px" id="rad_imposto" name="rad_imposto" value="issqn" />
            </li>
            <li style="width:60px;margin-bottom:0">
                <label for="rad_imposto">ISSQN</label>
            </li>
        </ul>

        <ul id="pedido_nfe_item_tributos_abas" class="menu_modo" style="width:900px; float:none; margin: 0 auto">
            <li class="icms"><a href="ICMS">ICMS</a></li>
<?          if(fnc_nfe_regime() > '1'){
?>				<li class="icms"><a href="IPI">IPI</a></li>
<?          }
?>
			<li class="icms issqn"><a href="PIS">PIS</a></li>
			<li class="icms issqn"><a href="COFINS">COFINS</a></li>
            <li class="icms"><a href="II">Imposto de Importa&ccedil;&atilde;o</a></li>
            <li class="icms"><a href="prod_especifico">Produto Especifico</a></li>
            <li class="issqn"><a href="ISSQN">ISSQN</a></li>
        </ul>
        
        <ul style="margin:10px 0 0 10px">
			<li>
            	<label for="txt_nfe_item_total_tributos">Total dos tributos</label>
		        <input style="width: 120px; text-align:right" type="text" name="txt_nfe_item_total_tributos" id="txt_nfe_item_total_tributos" value="0,00">
                <span style="float:left; margin:5px 0 0 10px">Valor aproximado total de tributos federais, estaduais e municipais conforme disposto na Lei n. 12.741/12</span>
            </li>
        </ul>
        
        <span style="clear:both; display:block"></span>
    
		<div id="pedido_nfe_item_tributos_ICMS">
<?			require("pedido_nfe_item_tributos_ICMS.php");				
?>		</div>
                
        <div id="pedido_nfe_item_tributos_IPI">
<?			require("pedido_nfe_item_tributos_IPI.php");				
?>		</div>
	        
        <div id="pedido_nfe_item_tributos_PIS">
<?			require("pedido_nfe_item_tributos_PIS.php");				
?>		</div>
	        
        <div id="pedido_nfe_item_tributos_COFINS">
<?			require("pedido_nfe_item_tributos_COFINS.php");				
?>		</div>
	        
        <div id="pedido_nfe_item_tributos_II">
<?			require("pedido_nfe_item_tributos_II.php");				
?>		</div>

        <div id="pedido_nfe_item_tributos_ISSQN">
<?			require("pedido_nfe_item_tributos_ISSQN.php");				
?>		</div>

        <div id="pedido_nfe_item_tributos_prod_especifico">
<?			require("pedido_nfe_item_tributos_prod_especifico.php");				
?>		</div>
	
	<script type="text/javascript">
						   
		$('#pedido_nfe_item_tributos_abas a').click(function(event){
			event.preventDefault();
			//marcar aba ativa
			$('ul#pedido_nfe_item_tributos_abas a').removeClass('ativo');
			$(this).addClass('ativo');
			$(this).blur();
			
			//ocultar todas as abas
			$('div#pedido_nfe_item_tributos div').hide();
			
			//exibir a selecionada
			var aba = $(this).attr('href');
			$('div#pedido_nfe_item_tributos_'+aba).show();
			$('div#pedido_nfe_item_tributos_'+aba+' input:first, div#pedido_nfe_item_tributos_'+aba+' textarea:first').focus();
					
		});
		
	</script>
		
        