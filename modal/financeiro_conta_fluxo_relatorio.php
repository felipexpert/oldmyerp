<?	$d1 = $_POST['params'][1];
	$d2 = $_POST['params'][2]; ?>

<div class="form" style="width:510px">
    <form style="width:510px" class="frm_detalhe" id="frm_financeiro_detalhe" action="" method="post">
        <input type="hidden" name="hid_movimento_id" id="hid_movimento_id" value="<?=$registro_id?>" />
        <ul>
            <li>
                <label for="txt_id">Tipo de relat&oacute;rio</label>
                <select id="sel_tipo" style="width:390px">
                	<option value="comum">Relat&oacute;rio comum</option>
                	<option value="fechamento">Relat&oacute;rio de fechamento</option>
                </select>
            </li>
            
            <li style="margin-top:16px;">
				<a id="gerar_relatorio" href="financeiro_conta_fluxo_relatorio.php" target="_blank" style="background:#6789c9; color:white; border:1px solid #CCC; padding:3px 6px; border-radius:3px">
                	Gerar relat&oacute;rio
				</a>
            </li>
            
            <li style="float:left; clear:both;">
                <label><input type="checkbox" style="width:13px" id="chk_resumido" disabled="disabled" />&nbsp;resumido</label>
            </li>
        </ul>
    </form>
</div>

<script>
	$('#sel_tipo').change(function(){
		var selected = $(this).find('option:selected').val();
		if(selected == 'comum') { 
			$('#gerar_relatorio').attr('href', 'financeiro_conta_fluxo_relatorio.php'); 
			$('#chk_resumido').attr('checked', '');
			$('#chk_resumido').attr('disabled', 'disabled');
		}
		
		else { 
			$('#gerar_relatorio').attr('href', 'financeiro_conta_fluxo_relatorio_fechamento.php?d1=<?=$d1?>&d2=<?=$d2?>'); 
			$('#chk_resumido').attr('disabled', '');
		} 
	});
	
	$('#chk_resumido').change(function(){
		if($(this).is(':checked')){
			$('#gerar_relatorio').attr('href', 'financeiro_conta_fluxo_relatorio_fechamento.php?d1=<?=$d1?>&d2=<?=$d2?>&resumo=ok'); 
		} else {
			$('#gerar_relatorio').attr('href', 'financeiro_conta_fluxo_relatorio_fechamento.php?d1=<?=$d1?>&d2=<?=$d2?>'); 
		}
	});
</script>