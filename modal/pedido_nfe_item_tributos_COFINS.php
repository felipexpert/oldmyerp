<fieldset style="width:920px;display:inline;margin:10px">
    <legend>COFINS</legend>
        <ul>
            <li>
                <label for="sel_nfe_item_tributos_cofins_situacao_tributaria">Situa&ccedil;&atilde;o Tribut&aacute;ria</label>
                <select id="sel_nfe_item_tributos_cofins_situacao_tributaria" name="sel_nfe_item_tributos_cofins_situacao_tributaria" style="width:280px">
                	<option value=""></option>
<?					$sql = "SELECT * FROM tblnfe_cofins";
                    $rsCofins_Situacao = mysql_query($sql);
                    while($rowCofins_Situacao = mysql_fetch_array($rsCofins_Situacao)){
?>						<option <?=$cofins_situacao_tributaria==$rowCofins_Situacao["fldId"] ? print 'selected="selected"' : '' ?> value="<?=$rowCofins_Situacao["fldId"]?>" title="<?=$rowCofins_Situacao["fldDescricao"]?>"><?=$rowCofins_Situacao['fldCOFINS'] . $rowCofins_Situacao["fldSituacaoTributaria"] . ' - ' . substr($rowCofins_Situacao["fldDescricao"],0,130)?></option>
<?					}
?>				</select>
            </li>
            <li id="li_calculo_cofins_js">
                <label for="sel_nfe_item_tributos_cofins_calculo_tipo">Tipo de C&aacute;lculo</label>
                <select name="sel_nfe_item_tributos_cofins_calculo_tipo" id="sel_nfe_item_tributos_cofins_calculo_tipo" <?=($COFINSCalculoTipo != 'enable' )? "disabled='disabled'" : ''?> style="width:280px;">
	                <option value=""></option>
                    <option <?=$cofins_calculo_tipo== 1 ? print 'selected="selected"' : '' ?> value="1">Percentual</option>
                    <option <?=$cofins_calculo_tipo== 2 ? print 'selected="selected"' : '' ?> value="2">Valor</option>
                </select>
            </li>
            <li>
                <label for="txt_nfe_item_tributos_cofins_valor_base_calculo">Valor da Base de C&aacute;lculo</label>
                <input type="text" style="width:280px;text-align:right;<?=($disabledCOFINSBC != "enable")? "background:#F1F1F1" : ''?>" id="txt_nfe_item_tributos_cofins_valor_base_calculo" name="txt_nfe_item_tributos_cofins_valor_base_calculo" <?=($disabledCOFINSBC != "enable" )? "disabled='disabled'" : '' ?> value="" maxlength="17" />
            </li>
            <li>
                <label for="txt_nfe_item_tributos_cofins_aliquota_percentual">Al&iacute;quota (percentual)</label>
                <input type="text" style="width:280px;text-align:right;<?=($disabledCOFINSAliqPorcentagem != "enable")? "background:#F1F1F1" : ''?>" id="txt_nfe_item_tributos_cofins_aliquota_percentual" name="txt_nfe_item_tributos_cofins_aliquota_percentual" <?=($disabledCOFINSAliqPorcentagem != "enable" )? "disabled='disabled'" : "value='$cofins_aliquota_porcentagem'" ?> />
            </li>
            <li>
                <label for="txt_nfe_item_tributos_cofins_aliquota_reais">Al&iacute;quota (reais)</label>
                <input type="text" style="width:280px;text-align:right;<?=($disabledCOFINSAliqReais != "enable")? "background:#F1F1F1" : ''?>" id="txt_nfe_item_tributos_cofins_aliquota_reais" name="txt_nfe_item_tributos_cofins_aliquota_reais" <?=($disabledCOFINSAliqReais != "enable" )? "disabled='disabled'" : "value='$cofins_aliquota_reais'" ?> />
            </li>
            <li>	
                <label for="txt_nfe_item_tributos_cofins_quantidade_vendida">Quantidade Vendida</label>
                <input type="text" style="width:280px;text-align:right;<?=($disabledCOFINSQtdVendida != "enable")? "background:#F1F1F1" : ''?>" id="txt_nfe_item_tributos_cofins_quantidade_vendida" name="txt_nfe_item_tributos_cofins_quantidade_vendida" <?=($disabledCOFINSQtdVendida != "enable" )? "disabled='disabled'" : '' ?> value=""/> 
            </li>
            <li>
                <label for="txt_nfe_item_tributos_cofins_valor">Valor do COFINS</label>
                <input type="text" style="width:280px;text-align:right" id="txt_nfe_item_tributos_cofins_valor" name="txt_nfe_item_tributos_cofins_valor" readonly="readonly" value="0,00"/>
            </li>
            
        </ul>
	</fieldset>

	<fieldset style="width:920px;display:inline;margin:10px">
    	<legend>COFINS ST</legend>
        <ul>
            <li id="li_calculo_cofinsST_js">
                <label for="sel_nfe_item_tributos_cofins_st_calculo_tipo">Tipo de C&aacute;lculo</label>
                <select name="sel_nfe_item_tributos_cofins_st_calculo_tipo" id="sel_nfe_item_tributos_cofins_st_calculo_tipo" style="width:280px;">
                	<option value=""></option>
                    <option <?=$cofinsST_calculo_tipo== 1 ? print 'selected="selected"' : '' ?> value="1">Percentual</option>
                    <option <?=$cofinsST_calculo_tipo== 2 ? print 'selected="selected"' : '' ?> value="2">Valor</option>
                </select>
            </li>
            <li>
                <label for="txt_nfe_item_tributos_cofins_st_valor_base_calculo">Valor da Base de C&aacute;lculo</label>
                <input type="text" style="width:280px;text-align:right;<?=($cofinsST_calculo_tipo != 1)? "background:#F1F1F1" : ''?>" id="txt_nfe_item_tributos_cofins_st_valor_base_calculo" name="txt_nfe_item_tributos_cofins_st_valor_base_calculo" <?=($cofinsST_calculo_tipo != 1)? "disabled='disabled'" : ''?> value="" maxlength="17" />
            </li>
            <li>
                <label for="txt_nfe_item_tributos_cofins_st_aliquota_percentual">Al&iacute;quota (percentual)</label>
                <input type="text" style="width:280px;text-align:right;<?=($cofinsST_calculo_tipo != 1)? "background:#F1F1F1" : ''?>" id="txt_nfe_item_tributos_cofins_st_aliquota_percentual" name="txt_nfe_item_tributos_cofins_st_aliquota_percentual" <?=($cofinsST_calculo_tipo != 1)? "disabled='disabled'" : "value='$cofinsST_aliquota_porcentagem'" ?> />
            </li>
            <li>
                <label for="txt_nfe_item_tributos_cofins_st_aliquota_reais">Al&iacute;quota (reais)</label>
                <input type="text" style="width:280px;text-align:right;<?=($cofinsST_calculo_tipo < 2)? "background:#F1F1F1" : ''?>" id="txt_nfe_item_tributos_cofins_st_aliquota_reais" name="txt_nfe_item_tributos_cofins_st_aliquota_reais" <?=($cofinsST_calculo_tipo < 2 )? "disabled='disabled'" : "value='$cofinsST_aliquota_reais'" ?> />
            </li>
            <li>	
                <label for="txt_nfe_item_tributos_cofins_st_quantidade_vendida">Quantidade Vendida</label>
                <input type="text" style="width:280px;text-align:right;<?=($cofinsST_calculo_tipo < 2)? "background:#F1F1F1" : ''?>" id="txt_nfe_item_tributos_cofins_st_quantidade_vendida" name="txt_nfe_item_tributos_cofins_st_quantidade_vendida" <?=($cofinsST_calculo_tipo < 2 )? "disabled='disabled'" : ''?> value=""/> 
            </li>
            <li>
                <label for="txt_nfe_item_tributos_cofins_st_valor">Valor do COFINS ST</label>
                <input type="text" style="width:280px;text-align:right" id="txt_nfe_item_tributos_cofins_st_valor" name="txt_nfe_item_tributos_cofins_st_valor" readonly="readonly" value="0,00"/>
            </li>
            
        </ul>
    </fieldset>
