<?Php
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');
	
	ob_start();
	session_start();
?>		
        <div id="table" style="width:650px; margin: 0 auto">
            <div id="table_cabecalho" style="width:650px">
                <ul class="table_cabecalho" style="width:650px">
                    <li style="width:3px">&nbsp;</li>
                    <li class="order" style="width:80px">C&oacute;d.</li>
                    <li class="order" style="width:80px; text-align:center">Data</li>
                    <li style="width:450px">Usuario</li>
                </ul>
            </div>
            <div id="table_container" style="width:650px">       
                <table id="table_general" class="table_general" summary="Lista de compras realizadas">
                	<tbody>
<?					
						$fornecedorId = $_POST['params'][1];
						if(isset($fornecedorId )){
							$filtro = "WHERE tblcompra_cotacao_fornecedor.fldFornecedor_Id =".$fornecedorId;
						}else{
							$filtro = "WHERE tblcompra_cotacao_fornecedor.fldId > 0 ";
						}
						
						$id_array = array();
						$n = 0;
						
						$linha = "row";
						$rsCotacao = mysql_query("SELECT tblcompra_cotacao.*, tblcompra_cotacao.fldId as cotacaoId FROM 
												 tblcompra_cotacao_item
												 INNER JOIN tblcompra_cotacao_fornecedor ON tblcompra_cotacao_item.fldId = tblcompra_cotacao_fornecedor.fldCotacao_Item_Id
												 INNER JOIN tblcompra_cotacao ON tblcompra_cotacao_item.fldCotacao_Id = tblcompra_cotacao.fldId ".$filtro." GROUP BY tblcompra_cotacao.fldId");
						echo mysql_error();
						while($rowCotacao = mysql_fetch_array($rsCotacao)){
							
							$rsUsuario = mysql_query("SELECT * FROM tblusuario WHERE fldId =".$rowCotacao['fldUsuario_Id']);
							$rowUsuario = mysql_fetch_array($rsUsuario);
							
							$id_array[$n] = $rowCotacao["fldId"];
							$n += 1;	
						
?>							<tr class="<?= $linha; ?>">
                                <td	style="width:3px">&nbsp;</td>
                                <td	style="width:80px; text-align:left" class="cod"><?=str_pad($rowCotacao['cotacaoId'], 6, "0", STR_PAD_LEFT)?></td>
                                <td style="width:80px; text-align:center"><?=format_date_out($rowCotacao['fldData'])?></td>
                                <td style="width:420px"><?=$rowUsuario['fldUsuario']?></td>
                            	<td style="width:25px"><a class="import" href="index.php?p=compra_novo&amp;cotacao=<?=$rowCotacao['fldId']?>&forn=<?=$fornecedorId?>" title="editar"></a></td>
                            </tr>
<?                     		$linha = ($linha == "row" ? "dif-row" : "row");
                  		}
?>		 			</tbody>
				</table>
            </div>
		</div>
