<?php
	
	session_start();
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');
	
	/**************************** PAGINAÇÃO *******************************************/
	$sSQL = "SELECT tblpedido_nfe_numero_inutilizado.*, tblpedido_nfe_numero_inutilizado_motivo.fldDescricao, tblusuario.fldUsuario FROM tblpedido_nfe_numero_inutilizado
			 INNER JOIN tblpedido_nfe_numero_inutilizado_motivo ON tblpedido_nfe_numero_inutilizado_motivo.fldId = tblpedido_nfe_numero_inutilizado.fldMotivo
			 INNER JOIN tblusuario ON tblusuario.fldId = tblpedido_nfe_numero_inutilizado.fldUsuario_Id
			 ORDER BY tblpedido_nfe_numero_inutilizado.fldNFe_Numero DESC";

	$rsNFeNumeros = mysql_query($sSQL);
	$rowsTotal    = mysql_num_rows($rsNFeNumeros);
	$pagina 	  = ($_GET['pagina'] ? $_GET['pagina'] : "1");
	
#####################################################################################
	
	//checando se há algum registro gravado para exibir
	if($rowsTotal > 0) {
?>
    	<div id="table" style="width:770px">
            <div id="table_cabecalho" style="width:770px">
                <ul class="table_cabecalho" style="width:770px">
                    <li style="width:50px; text-align:center">N&ordm; NFe</li>
                    <li style="width:110px">Motivo</li>
					<li style="width:330px">Observa&ccedil;&atilde;o</li>
					<li style="width:130px;text-align:right">Inutilizado em</li>
					<li style="width:80px;text-align:right;padding-right:10px">Usu&aacute;rio</li>
					<li style="width:20px">&nbsp;</li>
					<li style="width:10qwe px">&nbsp;</li>
                </ul>
            </div>
            <div id="table_container" style="width:770px">       
                <table id="table_general" class="table_general" summary="N&uacute;meros inutilizados">
                	<tbody>
<?						$linha = "row";
						while($rowNFeNumero = mysql_fetch_array($rsNFeNumeros)) {
							$perfil_bg 		= ($rowNFeNumero['fldPerfil'] > 0) ? 'bg_usuario_perfil' : '';
							$perfil_title 	= ($rowNFeNumero['fldPerfil'] > 0) ? 'perfil alterado por usu&aacute;rio' : '';
?>							<tr class="<?= $linha; ?>">
								<td style="width:50px; text-align:center; color:#09f;font-weight:bold"><?=$rowNFeNumero['fldNFe_Numero']?></td>
								<td style="width:110px; color:#C00"><?=$rowNFeNumero['fldDescricao']?></td>
								<td style="width:330px"><?=$rowNFeNumero['fldObservacao']?></td>
								<td style="width:130px;text-align:right"><?=format_date_out($rowNFeNumero['fldData'])?> <?=$rowNFeNumero['fldHora']?></td>
								<td style="width:80px; font-weight:bold;text-align:right;padding-right:10px"><?=$rowNFeNumero['fldUsuario']?></td>
								<td style="width:20px"><span class="<?=$perfil_bg?>" title="<?=$perfil_title?>">&nbsp;</span></td>
							</tr>
<?							$linha = ($linha == "row" ) ? "dif-row" : "row";
						}
?>			 		</tbody>
				</table>
            </div>
<?	}
	else {
?>
		<div class="alert" style="width: 574px; margin: 0 auto;">
			<p class="ok">N&atilde;o h&aacute; dados registrados ainda!<p>
		</div>
<?
	}
?>