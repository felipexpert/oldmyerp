<?php
	
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');
	
	$id = $_POST['params'][1];
	
	if(!is_null($id)) {
		$rsEditNomePerfil = mysql_fetch_array(mysql_query("SELECT * FROM tblsistema_pagamento_perfil WHERE fldId = $id"));
		$rsEditPerfil 	  = mysql_query("SELECT * FROM tblsistema_pagamento_perfil_intervalo WHERE fldPerfil_Id = $id");
	}
	
?>
	<form id="frm_perfis" class="frm_detalhe" style="width:480px;" action="index.php?p=configuracao&modo=pagamento" method="post">
		<ul>
			<li style="margin-left:0; margin-right:0; padding:8px; background:#D7D7D7"> 
				<label for="txt_perfil_nome">Nome do Perfil</label>
				<input type="text" style=" width:460px" id="txt_perfil_nome" name="txt_perfil_nome" value="<?=$rsEditNomePerfil['fldPerfil']; ?>" />
<?				if(!is_null($id)) { ?> <input type="hidden" id="hid_perfil_nome_id" name="hid_perfil_nome_id" value="<?=$id; ?>" /> <? } ?>
			</li>
			<li style="margin-left:0; margin-right: 30px; padding:8px; background:#D7D7D7"> 
				<label for="txt_perfil_frequencia">Frequ&ecirc;ncia</label>
				<input type="text" style="text-align:center; width:100px" id="txt_perfil_frequencia" name="txt_perfil_frequencia" value="" />
			</li>
			<li style="margin-left:0; margin-right:0; padding:8px; background:#D7D7D7">
				<label for="sel_perfil_intervalo">Intervalo</label>
				<select id="sel_perfil_intervalo" name="sel_perfil_intervalo" style="width:180px">
<?					$rsCalendario = mysql_query("SELECT * FROM tblsistema_calendario_intervalo");
					while($rowCalendario= mysql_fetch_array($rsCalendario)){
?>						<option value="<?=$rowCalendario['fldId']?>"><?=$rowCalendario['fldNome']?></option>
<?					}
?>				</select>
			</li>
			<li style="float:right;">
				<input type="hidden" name="hid_controle" id="hid_controle" value="0" />
				<input style="margin: 15px 0 0 0" type="submit" class="btn_enviar" id="btn_inserir_pagamento_perfil" name="btn_inserir_pagamento_perfil" value="Inserir" >
			</li>
		</ul>
		
		<div id="intervalos">
			<ul>
				<li style="width: 20px; background: #ccc;"></li>
				<li style="width: 80px; color: #666;">Frequ&ecirc;ncia</li>
				<li style="width: 285px; color: #666;">Intervalo</li>
				<li style="width: 76px; color: #666;">Parcela</li>
			</ul>
			
			<!-- Bloco usado como referência para listar os intervalos -->
			<div id="hidden">
				<ul style="width:480px">
					<li style="height:22px;background:#ccc"><a href="#" title="Remover intervalo" class="excluir_intervalo_perfil"></a></li>
					<li>
						<input type="text" style="width:75px;text-align:center" id="txt_frequencia_0" name="txt_frequencia_0" class="txt_frequencia" value="" />
					</li>
					<li>
						<select id="sel_intervalo_0" name="sel_intervalo_0" class="sel_intervalo" style="width:285px">
<?							$rsCalendario = mysql_query("SELECT * FROM tblsistema_calendario_intervalo");
							while($rowCalendario = mysql_fetch_array($rsCalendario)) {
?>								<option value="<?=$rowCalendario['fldId']?>"><?=$rowCalendario['fldNome']?></option>
<?							}
?>						</select>
						<input type="hidden" id="hid_intervalo_0" name="hid_intervalo_0" class="hid_intervalo" value="" />
					</li>
					<li>
						<input type="text" style="width:71px;text-align:center" id="txt_parcela_0" name="txt_parcela_0" class="txt_parcela" value="" readonly="readonly" />
					</li>
				</ul>
			</div>
			
<?			if(!is_null($id)) {
			
				$n = 1;
				while($rowEditPerfil = mysql_fetch_array($rsEditPerfil)) {
?>					<ul style="width:480px">
						<li style="height:22px;background:#ccc"><a href="#" title="Remover intervalo" class="excluir_intervalo_perfil"></a></li>
						<li>
							<input type="text" style="width:75px;text-align:center" id="txt_frequencia_0" name="txt_frequencia_<?=$n?>" class="txt_frequencia" value="<?=$rowEditPerfil['fldFrequencia']?>" />
						</li>
						<li>
							<select id="sel_intervalo_0" name="sel_intervalo_<?=$n?>" class="sel_intervalo" style="width:285px">
<?								$rsCalendario = mysql_query("SELECT * FROM tblsistema_calendario_intervalo");
								while($rowCalendario = mysql_fetch_array($rsCalendario)) {
?>									<option<?= ($rowCalendario['fldId'] == $rowEditPerfil['fldIntervalo_Id']) ? ' selected="selected"' : ''; ?> value="<?=$rowCalendario['fldId']?>"><?=$rowCalendario['fldNome']?></option>
<?								}
?>							</select>
						</li>
						<li>
							<input type="text" style="width:71px;text-align:center" id="txt_parcela_0" name="txt_parcela_<?=$n?>" class="txt_parcela" value="<?=$n?>" readonly="readonly" />
						</li>
					</ul>
<?				$n++;
				}
				
			}
?>
		</div>
		
		<input type="hidden" id="hid_controle" name="hid_controle" value="<?= (is_null($id)) ? '0' : $n - 1;?>" />
<?		if(!is_null($id)) { ?> <input type="hidden" id="hid_atualizar" name="hid_atualizar" value="true" /> <? } ?>
		<input style="float: right; margin-top: 20px;" type="submit" class="btn_enviar" id="btn_gravar_pagamento_perfil" name="btn_gravar_pagamento_perfil" value="Gravar" />
		
	</form>
	
<? unset($id, $_POST, $rsEditNomePerfil, $rsEditPerfil, $rowEditPerfil, $rsCalendario, $rowCalendario); ?>