<?	
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');

	if(isset($_POST['params'][2])){
		$cliente_id 	= $_POST['params'][2];
		$SQL 			= mysql_query("SELECT fldCodigo, fldNome FROM tblcliente WHERE fldId = $cliente_id");
		$rowCliente 	= mysql_fetch_array($SQL);
		$clienteCodigo 	= $rowCliente['fldCodigo'];
		$clienteNome 	= $rowCliente['fldNome'];
	}else{
		$clienteCodigo 	= 0;
		$clienteNome 	= 'Consumidor';
		$cliente_id 	= 0;
	}

	if(isset($_POST['params'][1])){
		$pedido_id 	= $_POST['params'][1];
		$sSQL 		= mysql_query("SELECT * FROM tblpedido WHERE fldId  = $pedido_id");
		$rowPedido 	= mysql_fetch_assoc($sSQL);
	}

?>
    <script type="text/javascript">
        $('#txt_cliente_codigo').focus();
        
        var form = $('#frm_pedido_rapido_novo').serialize();
        $('#hid_form_venda').val(form);
        $('#frm_pedido_pagamento_parcela #txt_pedido_total').val($('#frm_pedido_rapido_novo #txt_pedido_total').val());
        
		//no campo de cliente, cair prox campo com enter 
        /*$("#txt_cliente_nome").keyup(function(event) {
            switch(event.keyCode){
                case 13:
                    event.preventDefault();
                    $("#txt_entrada").focus();
                break;
            }
        });*/

    </script>
          
	<form class="frm_detalhe " style="width:630px" id="frm_pedido_pagamento_parcela" action="pedido_rapido_gravar.php" method="post">
		<div class="pedido_rapido_pagamento" style="float:right">
            <ul>
                <li>
                    <label for="txt_pedido_total" class="total">Total</label>
                    <input type="text" class="total" style="width:205px" id="txt_pedido_total" name="txt_pedido_total" value="0,00" readonly="readonly" />
                </li>
                <li>
					<input type="hidden" id="hid_submit" name="hid_submit"  value="1" />
                    <input type="hidden" id="hid_print"	 name="hid_print" 	value="0" />
                    <input type="hidden" id="hid_ecf" 	 name="hid_ecf" 	value="0" />
                    
                    <input type="submit" style="width:210px" class="btn_submit" name="btn_gravar_print" id="btn_gravar_print" value="[F11] Finalizar e Imprimir" title="Finalizar e Imprimir" />
                    <input type="submit" style="width:210px" class="btn_submit" name="btn_gravar" id="btn_gravar" value="[F10] Finalizar" title="Finalizar Venda" />
                    <input type="submit" style="width:210px" class="btn_submit" name="btn_gravar_ecf" id="btn_gravar_ecf" value="[F9] Finalizar e Cupom Fiscal" title="Finalizar e Cupom Fiscal" />
                    <a class="modal" style="display:none" id="btn_ecf" href="ecf_documento" rel="345-130" title="Emitir Cupom Fiscal"></a>
               </li>
            </ul>
        </div>

        <ul>
            <li style="margin:10px 0px 0px 0px">
                <label for="txt_cliente_codigo">Cliente [F4]</label>
                <input type="text" style="width:80px;text-align:right" id="txt_cliente_codigo" name="txt_cliente_codigo" value="<?=$clienteCodigo?>" />
                <a href="cliente_busca,txt_cliente_codigo" title="Localizar" class="modal" rel="950-380"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
            </li>
            <li style="margin:10px 0px 0px 5px">
                <label for="txt_cliente_nome">&nbsp;</label>
                <input type="text" style=" width:277px" id="txt_cliente_nome" name="txt_cliente_nome" value="<?=$clienteNome?>" />
                <input type="hidden" id="hid_cliente_id" name="hid_cliente_id" value="<?=$cliente_id?>" />
            </li>
            <li style="margin:0px 0 5px 0px; clear:both">
            	<label for="txt_retirado_por">Endereço</label>
            	<input type="text" style="width:394px" id="txt_pedido_endereco" name="txt_pedido_endereco" value="<?= ($_POST['params'][1]) ? $rowPedido['fldEndereco'] : '';?>">
            </li>
            <? if(fnc_sistema("pedido_retirado_por") == "1") { ?>
            <li style="margin:0 0 5px 0; clear:both">
                <label for="txt_retirado_por">Retirado por</label>
                <input type="text" style="width:394px" id="txt_retirado_por" name="txt_retirado_por" maxlength="62" value="<?= ($_POST['params'][1]) ? $rowPedido['fldRetirado_Por'] : '';?>">
            </li>
            <? } ?>
        </ul>
    
        <div id="pedido_pagamento" style="float:left;">
            <div class="pedido_parcela" style="width:386px;">
                <div class="tabs" id="tabs_modal">
					<ul class="menu_modo" style="background: #fff; width: 390px;">
						<li><a href="parcelamento-avulso" class="atual">Parcelamento</a></li>
						<li><a href="parcelamento-pre-definido">Pr&eacute;-definido</a></li>
					</ul>
					
					<div id="tab_parcelamento-avulso" class="parcelamento tab ativo" style="width:380px;">
						<div>
							<label for="txt_parcela">Pagamento</label>
							<input type="text" id="txt_entrada" name="txt_entrada" value="1" />
							<span>+</span>
							<input type="text" id="txt_parcela" name="txt_parcela" value="0" />
						</div>
						<div>
							<label for="txt_1_vencimento">Vencimento</label>
							<input type="text" id="txt_1_vencimento" name="txt_1_vencimento" class="calendario" value="<?= date('d/m/Y') ?>" />
						</div>
						<input type="submit" name="btn_calcular" id="btn_calcular" title="ok" value="ok" style="position: relative; top: 25px;" />
					</div>
<?php				$rsPerfil = mysql_query("SELECT tblsistema_pagamento_perfil.fldPerfil, tblsistema_pagamento_perfil_intervalo.fldFrequencia, tblsistema_calendario_intervalo.fldValor FROM tblsistema_pagamento_perfil
											INNER JOIN tblsistema_pagamento_perfil_intervalo ON tblsistema_pagamento_perfil.fldId = tblsistema_pagamento_perfil_intervalo.fldPerfil_Id
											INNER JOIN tblsistema_calendario_intervalo ON tblsistema_pagamento_perfil_intervalo.fldIntervalo_Id = tblsistema_calendario_intervalo.fldId
											ORDER BY tblsistema_pagamento_perfil.fldPerfil ASC, tblsistema_pagamento_perfil_intervalo.fldId");
?>
					<div id="tab_parcelamento-pre-definido" class="parcelamento tab" style="width: 380px;">
<?php 						if(mysql_num_rows($rsPerfil) > 0) {
?>
						<div>
							<label for="sel_perfil">Perfil</label>
<?php								$x = 0;
							while($rowPerfil = mysql_fetch_array($rsPerfil)) {
								$perfis['Perfil'][$rowPerfil['fldPerfil']][$x] = $rowPerfil['fldFrequencia'] . ';' . $rowPerfil['fldValor'];
								$x++;
							}
							
							unset($rsPerfil, $rowPerfil, $x);
?>
							<select id="sel_perfil" name="sel_perfil">
<?										foreach($perfis['Perfil'] as $indice => $dados) {
?>											<option value="<?=implode('|', $dados)?>"><?=$indice?></option>
<?										}
								unset($perfis, $indice, $dados);
?>                                  </select>
						</div>
						<div>
							<label for="txt_data_inicial">Data Inicial</label>
							<input type="text" id="txt_data_inicial" name="txt_data_inicial" class="calendario" value="<?= date('d/m/Y') ?>" />
						</div>
						<input type="button" name="btn_calcular_pre_definido" id="btn_calcular_pre_definido" title="ok" value="ok" style="position: relative; top: 25px;" />
						
<?php 						}
					else {
?>
						<p style="text-align: center; font-size: 12px;">
							Voc&ecirc; n&atilde;o cadastrou nenhum perfil de pagamento. <a href="?p=configuracao&modo=pagamento#frm_perfis" title="Cadastrar um perfil de pagamento" class="link">Clique aqui para cadastrar</a>
						</p>
<?php						}
?>
					</div>
				</div>
				
                <ul class="parcelamento_cabecalho">
                    <li style="width:30px">n&deg;</li>
                    <li>Valor</li>
                    <li style="width:104px">Vencimento</li>
                    <li style="width:126px">Pagamento</li>
                    <li style="width:15px">&nbsp;</li>
                </ul>
                <div id="parcelas" style="width:400px;">
					<div id="hidden">
                        <ul class="parcela_detalhe">
                            <li>
                                <input style="text-align:center" class="txt_parcela_numero" type="text" id="txt_parcela_numero_0" name="txt_parcela_numero_0" value="1" readonly="readonly" />
                            </li>
                            <li>
                                <input class="txt_parcela_valor" type="text" id="txt_parcela_valor_0" name="txt_parcela_valor_0" value="" style="width: 100px;" />
                            </li>
                            <li>
                                <input class="txt_parcela_data" type="text" id="txt_parcela_data_0" name="txt_parcela_data_0" value="novo" style="width: 103px;" />
                            </li>
                            <li>
                                <SELECT id="sel_pagamento_tipo_0" name="sel_pagamento_tipo_0" class="sel_pagamento_tipo" title="Forma de Pagamento" style="width: 135px;">
<?									$rsPagamento = mysql_query("SELECT * FROM tblpagamento_tipo WHERE fldExcluido = 0");
                                    while($rowPagamento= mysql_fetch_array($rsPagamento)){
?>										<option value="<?= $rowPagamento['fldId'] ?>"><?= $rowPagamento['fldTipo'] ?></option>
<?									}
?>                    			</SELECT> 
                            </li>
                        </ul>
                    </div>
                    <input type="hidden" name="hid_verificar_limite_credito" id="hid_verificar_limite_credito" value="<?=$verificar_limite_credito;?>"> 
<?					if(isset($_POST['params'][1])){
						$pedido_id = $_POST['params'][1];
						$rsParcela = mysql_query("SELECT * FROM tblpedido_parcela WHERE fldPedido_Id = $pedido_id ORDER BY fldParcela");
						$rows = mysql_num_rows($rsParcela);
						$x = 1;
						while($rowParcela = mysql_fetch_array($rsParcela)){
									
?>							<ul class="parcela_detalhe">
                                <li>
                                    <input style="text-align:center" class="txt_parcela_numero" type="text" id="txt_parcela_numero_<?=$x?>" name="txt_parcela_numero_<?=$x?>" value="<?=$rowParcela['fldParcela']?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input class="txt_parcela_valor" type="text" id="txt_parcela_valor_<?=$x?>" name="txt_parcela_valor_<?=$x?>" value="<?=format_number_out($rowParcela['fldValor'])?>" style="width: 100px;" />
                                </li>
                                <li>
                                    <input class="txt_parcela_data" type="text" id="txt_parcela_data_<?=$x?>" name="txt_parcela_data_<?=$x?>" value="<?=format_date_out($rowParcela['fldVencimento'])?>" style="width: 104px;" />
                                </li>
                                <li>
                                    <select id="sel_pagamento_tipo_<?=$x?>" name="sel_pagamento_tipo_<?=$x?>" class="sel_pagamento_tipo" title="Forma de Pagamento" style="width:133px">
<?										$rsPagamento = mysql_query("SELECT * FROM tblpagamento_tipo WHERE fldExcluido = 0");
                              			while($rowPagamento= mysql_fetch_array($rsPagamento)){
?>											<option <?=($rowParcela['fldPagamento_Id'] == $rowPagamento['fldId']) ? 'selected="selected"' : '' ?> value="<?= $rowPagamento['fldId'] ?>"><?= $rowPagamento['fldTipo'] ?></option>
<?										}
?>									</select> 
                                </li>
                            </ul>
<?							$x += 1;
						}
					}
?>					<input type="hidden" name="hid_controle_parcela" id="hid_controle_parcela"  value="<?=$rows?>" />
					<input type="hidden" name="hid_form_venda"		 id="hid_form_venda" 		value="" />
				</div> 
			</div>
		</div>
	</form>

<script type="text/javascript">
	
	//checando se existe o elemento pai do conteúdo das abas
	if($('#pedido_pagamento').find('div.tabs').length) {
		$('#pedido_pagamento').find('div.tab').hide();
		$('#pedido_pagamento').find('div.tab.ativo').show();
		
		var tabLink = $('#pedido_pagamento').find('ul.menu_modo li a');
		tabLink.live('click', function(e) {
		tabLink.blur();
		
			e.preventDefault();
			
			var tab = $(this).attr('href');
			
			$('#pedido_pagamento').find('div.tab.ativo').hide();
			$('#pedido_pagamento').find('div.tab').removeClass('ativo');
			$('#tab_'+tab).addClass('ativo');
			$('#tab_'+tab).show();
			
			tabLink.removeClass('atual');
			$(this).addClass('atual');
		});
	}
</script>