<img src="image/layout/suporte_mw.jpg" align="mwdesenvolvimento"/>

<p style=" width:850px;margin: 0 auto;font:14px 'Trebuchet MS',Arial, Helvetica,sans-serif;text-align:justify; margin-top: 15px"> 
    
    Devido ao aumento do nosso n&uacute;mero de clientes e com o intuito de melhorar a qualidade de nossos servi&ccedil;os,
    o sistema myERP conta agora com uma valida&ccedil;&atilde;o mensal relativa ao pagamento da mensalidade do mesmo.
    <br /><br />
    Na data de vencimento de sua mensalidade o sistema se conectar&aacute; automaticamente em nosso servidor online e validar&aacute; o sistema para mais um m&ecirc;s, uma vez efetuado o pagamento do mesmo. 
    Caso o pagamento n&atilde;o seja computado o sistema seguir&aacute; o seguinte cronograma:
    <br /><br />
     - Passados 2 dias ap&oacute;s o vencimento o sistema come&ccedil;ar&aacute; a exibir um lembrete sobre o bloqueio.
    <br />
    - Passados 5 dias ap&oacute;s vencimento o sistema se auto bloquear&aacute;.
    <br /><br />
    Caso o sistema n&atilde;o consiga validar online de forma autom&aacute;tica, seja pela aus&ecirc;ncia de conex&atilde;o com a internet ou qualquer outro fator inesperado, 
    o usu&aacute;rio do sistema poder&aacute; entrar em contato com nossa equipe e solicitar uma chave para desbloqueio atrav&eacute;s de e-mail ou telefone, que ser&aacute; gerada mediante confirma&ccedil;&atilde;o do pagamento.
    <br /><br />
    Caso o pagamento n&atilde;o possa ser confirmado por ainda estar processando na rede banc&aacute;ria, e o sistema seja bloqueado, o cliente dever&aacute; encaminhar atrav&eacute;s de e-mail uma c&oacute;pia do comprovante de pagamento.
    <br />
    Deixamos claro que o sistema n&atilde;o ser&aacute; liberado caso o pagamento n&atilde;o seja comprovado.
    <br /><br />
    Desde j&aacute; agradecemos a colabora&ccedil;&atilde;o e compreens&atilde;o de todos.

</p>