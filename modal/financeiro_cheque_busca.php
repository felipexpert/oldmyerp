<?php
	ob_start();
	session_start();
?>
	<script type="text/javascript">
		//evita que o enter faça alguma acao					   
		$("input#buscar").keydown(function(event){
			if (event.keyCode == 13){ return false;	}			   
		});						   
		
		$('document').ready(function(){
				$('#buscar').select();
				//$('#buscar').select();
				//$('#buscar').click(function(){$('#buscar').val('');});

				$("#chk_busca_qualquer_parte").click(function(event){
					busca_keyPress(event);
					$('#buscar').focus();
				});
				
				$('#buscar').keyup(busca_keyPress);
				$('#buscar').keypress(busca_teclas);

				function busca_Scroll(deslocamento){
					//alert(deslocamento);
					var cursorAtual 	= parseInt($('#hid_busca_cursor').val());
					var cursorLimite 	= parseInt($('#hid_busca_controle').text() - 1);
					var mover = false;
					if(deslocamento<0){
						if(cursorAtual > 0){mover = true;}
					}else{
						if(cursorAtual < cursorLimite){mover = true;}
					}
					if(mover){
						$('a[title=busca_lista_cheque]').removeClass('cursor');
						cursorNovo = cursorAtual + deslocamento;
						//corrigir cursor se ultrapassar limites
						if(cursorNovo < 0){cursorNovo = 0;}
						if(cursorNovo > cursorLimite){cursorNovo = cursorLimite;}
						
						$('a[title=busca_lista_cheque]:eq('+cursorNovo+')').addClass('cursor');
						$('#hid_busca_cursor').val(cursorNovo);
						var strNome = $('li[title=nome]:eq('+cursorNovo+')').text();
						$('#buscar').val(strNome);
						
						//19 - altura da linha de exibição de cada registro
						$('#alvo').scrollTop($('#alvo').scrollTop() + 19 * deslocamento);
					}
				}

				function busca_teclas (event) {
					//console.log(this);
					//alert(event.keyCode);
					switch(event.keyCode){
						case 13: //enter
							$('#buscar').focus();
							//alert($('#hid_busca_controle').text());
							//verificar se tem resultado na busca
							if($('#hid_busca_controle').text() == 0){
								alert('Atenção! Não há cheque selecionado!');
								return false;
							}
							else{
								var intCursor = $('#hid_busca_cursor').val();
								var cheque_id =  $('a[title=busca_lista_cheque]:eq('+intCursor+')').attr('href');
								$('#parametro_cheque').load('modal/financeiro_cheque_busca.php', {id_cheque: cheque_id});
							}
							break;
						case 38: //up
						
							busca_Scroll(-1);
							break;
							
						case 40: //down
							busca_Scroll(1);
							break;
							
						case 33: //page up
							busca_Scroll(-10);
							break;
							
						case 34: //page down
							busca_Scroll(10);
							break;
						
					}
				}
				
				function busca_keyPress(event) {
					//console.log(this);
					//alert(event.keyCode);
					switch(event.keyCode){
						case 13: //enter
						case 38: //up
						case 40: //down
						case 33: //page up
						case 34: //page down
							break;

						default: //busca
							//reset da posição do cursor para acompanhar o scroll
							$('#hid_busca_cursor').val(0);

							bolQualquerParte = $("#chk_busca_qualquer_parte").is(":checked");

							if(bolQualquerParte){
								strBusca = '%' + $('#buscar').val() + '%';
							}else{
								strBusca = $('#buscar').val() + '%';
							}

							$.post('modal/financeiro_cheque_busca_consulta.php',
							{busca: strBusca, rad:$('input:radio[name=rad_busca]:checked').val()},
							function(data){
								if ($('#buscar').val()!=''){
									$('#alvo').show();
									$('#alvo').empty().html(data);
								}else{
									$('#alvo').empty();
								}
							});
							break;
					}
				}
			
			//pegar o id no href e retornar para essa mesma página via ajax o valor e gravar na sessão	
			$('a.selecionar').live("click",function(e){
				e.preventDefault();
				$('#parametro_cheque').load('modal/financeiro_cheque_busca.php', {id_cheque: $(this).attr('href')});
				$('a.selecionar').die("click");
			});	
		});
		
		
	</script>
    	
	<div id="parametro_cheque">
<?		if(isset($_POST['id_cheque'])){
?>
			<script type="text/javascript">	
				id_cheque = "<?= $_POST['id_cheque']?>";
				
				$.get('filtro_ajax.php', {busca_cheque:id_cheque}, function(theXML){
					$('dados',theXML).each(function(){
						var correntista = $(this).find("correntista").text();
						var entidade	= $(this).find("entidade").text();
						var numero 		= $(this).find("numero").text();
						var banco 		= $(this).find("banco").text();
						var conta 		= $(this).find("conta").text();
						var agencia 	= $(this).find("agencia").text();
						var valor		= $(this).find("valor").text();
						var cpf 		= $(this).find("cpf").text(); 
						var agencia 	= $(this).find("agencia").text();
						var vencimento 	= $(this).find("vencimento").text();
				
						$('form#frm_cheque_novo input#txt_banco').val(banco);
						$('form#frm_cheque_novo input#txt_numero').val(numero);
						$('form#frm_cheque_novo input#txt_agencia').val(agencia);
						$('form#frm_cheque_novo input#txt_conta').val(conta);
						$('form#frm_cheque_novo input#txt_CPF_CNPJ').val(cpf);
						$('form#frm_cheque_novo input#txt_correntista').val(correntista);
						$('form#frm_cheque_novo input#txt_entidade').val(entidade);
						$('form#frm_cheque_novo input#txt_valor').val(valor);
						$('form#frm_cheque_novo input#txt_vencimento').val(vencimento);
						$('form#frm_cheque_novo input#hid_cheque_id').val(id_cheque);
						
						$('#frm_cheque_novo input').attr('readonly', 'readonly');
						
						$('.frm_busca_cheque').parents('.modal-body').remove();
						return false;
					});
				});
			</script>
<?			die;
		}
?>	</div>
	
<? require("../inc/con_db.php"); ?>

    <form id="frm_busca" name="frm_busca" class="frm_busca_cheque" style="width:620px">
    <fieldset>
    	<legend>Busca de cheque</legend>
           	<ul>
            	<li style="float:left" >
                	<input style="float:left; width:250px" type="text" id="buscar" name="buscar" value="Digite o nome do correntista" size="50" >
                </li>
                <li style="float:left; margin:13px 0 0 5px">
                	<input type="checkbox" style="width:auto; height:auto; margin:2px 5px 0 0; float:left;" id="chk_busca_qualquer_parte" name="chk_busca_qualquer_parte">
                	<label style="font-size:12px;" for="chk_busca_qualquer_parte">qualquer parte do campo</label>
                </li>
			</ul>

			<ul style="clear:both; margin:0 0 0 10px">
                <li style="width: 100px; margin-top:0px; float:left;">
                    <input type="radio" style="width:auto; height:auto; margin:1px 3px 0 0; float:left" id="rad_busca_nome" name="rad_busca" value="nome" checked="checked" />
					<label style="font-size:12px;" for="rad_busca_nome">Correntista</label>
                </li>
                <li style="width: 90px; margin:0; float:left;">
                    <input type="radio" style="width:auto; height:auto; margin:1px 3px 0 0; float:left" id="rad_busca_entidade" name="rad_busca" value="entidade" />
                    <label style="font-size:12px" for="rad_busca_entidade">Entidade</label>
                </li>
                <li style="width: 100px; margin:0; float:left;">
                    <input type="radio" style="width:auto; height:auto; margin:1px 3px 0 0; float:left" id="rad_busca_numero" name="rad_busca" value="numero" />
                    <label style="font-size:12px" for="rad_busca_numero">N&uacute;mero</label>
                </li>
			</ul>
            
			<span style="clear:both; display:block"></span>

            <ul id="busca_cabecalho" style="width:620px; margin-top:10px;">
            	<li style="width:150px" title="correntista">Correntista</li>
                <li style="width:150px" title="correntista">Entidade</li>
                <li style="width:100px;text-align:right" title="correntista">Numero</li>
                <li style="width:50px;text-align:right" title="correntista">Banco</li>
                <li style="width:60px;text-align:right" title="correntista">Conta</li>
                <li style="width:100px;text-align:right" title="correntista">Valor</li>
            </ul>
			<input type="hidden" id="hid_busca_cursor" value="0" />
            <div id="alvo" style="width:620px; height:245px;"></div>
    	</fieldset>
    </form>