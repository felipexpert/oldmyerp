<?
	require('../inc/con_db.php');
	require('../inc/fnc_general.php');
	include('../inc/fnc_status.php');
	
	if(isset($_POST['form'])){
		
		$campo 		= $_POST['campo'];
		$serialize 	= $_POST['form'];	
		parse_str($serialize, $form);
		
		$status 			= $form['sel_status'];
		$codigo	 			= ($form['txt_codigo'] == 'auto') ? '' : $form['txt_codigo'];
		$nome	 			= $form['txt_nome'];
		$nome_fantasia		= $form['txt_nome_fantasia'];
		$sel_genero 		= $form['sel_genero'];
		$sel_tipo			= $form['sel_tipo'];
		$cpf_cnpj 			= $form['txt_cpf_cnpj'];
		$rg_ie				= $form['txt_rg_ie'];
		$telefone1			= $form['txt_telefone1'];
		$telefone2			= $form['txt_telefone2'];	
		$endereco 			= $form['txt_endereco'];
		$endereco_id		= $form['txt_endereco_codigo'];
		$numero				= $form['txt_numero'];
		$nome_fantasia		= $form['txt_nome_fantasia'];
		$complemento 		= $form['txt_complemento'];
		$bairro				= $form['txt_bairro'];
		$cep	 			= $form['txt_cep'];
		$municipio_codigo	= $form['txt_municipio_codigo'];
		$data_atual 		= date("Y-m-d"); 
		

		$SQL = "INSERT INTO tblcliente
		(fldCodigo, fldCadastroData, fldNome, fldNomeFantasia, fldTelefone1, fldTelefone2, 
		fldGenero, fldTipo, fldCPF_CNPJ, fldRG_IE, fldEndereco, fldEndereco_Id, fldNumero, 
		fldComplemento, fldBairro, fldCEP, fldMunicipio_Codigo, fldStatus_Id, fldOrigem_Id)
		VALUES (
		'$codigo',
		'$data_atual',
		'$nome',
		'$nome_fantasia',
		'$telefone1',
		'$telefone2',
		'$sel_genero',
		'$sel_tipo',
		'$cpf_cnpj',
		'$rg_ie',
		'$endereco',
		'$endereco_id',
		'$numero',
		'$complemento',
		'$bairro',
		'$cep',
		'$municipio_codigo',
		'$status',
		'1'
		)";
	
		
		if(mysql_query($SQL)){
			if(!$cliente_id){
				$rsID 	= mysql_query("Select last_insert_id() as lastID");
				$LastId = mysql_fetch_array($rsID);
				$cliente_id = $LastId['lastID'];
				
				if($codigo == ''){
					mysql_query("UPDATE tblcliente SET fldCodigo = '$cliente_id' WHERE fldId = $cliente_id");
					$codigo = $cliente_id;
				}
			} 
			
?>			<div class="alert" style="width:735px">
				<p class="ok">Registro gravado com sucesso!<p>
			</div>
            
			<script language="javascript" type="text/javascript">
			
				var cliente_id		= '<?=$cliente_id?>';
				var codigo_cliente	= '<?=$codigo?>';
				var campo			= '<?=$campo?>';
				
				$('input#txt_'+campo).val(codigo_cliente);
				$('input#txt_'+campo).change();
				$('input#txt_'+campo).focus();
				$('#frm_cliente_rapido').parents('.modal-body').remove();
			</script>
			
<?		}else{
			echo mysql_error();
		}
	}
?>
    <form id="frm_cliente_rapido" name="frm_general" class="frm_detalhe" style="width:95%" action="" method="post">
    	
        <fieldset style="height:280px; background:#EFEFEF">
        	<legend>cliente cadastro r&aacute;pido</legend>
            <ul>
                <li style="margin-right: 350px">
                    <label for="txt_codigo">Codigo</label>
                    <input type="text" style="width:80px; text-align:center" id="txt_codigo" name="txt_codigo" onfocus="limpar (this,'auto')" onblur="mostrar (this, 'auto')" value="auto" />
                </li>
                <li>
                    <label for="txt_cadastro">Cadastrado em</label>
                    <input type="text" style="width:95px;text-align:center" id="txt_cadastro" name="txt_cadastro" value="<?=date("d/m/Y")?>" />
                </li>
                <li>
                    <label for="sel_status">Status</label>
                    <select style="width:150px;float:right;" id="sel_status" class="sel_status_cliente" name="sel_status" >
<?						$rsStatus = mysql_query("SELECT * FROM tblcliente_status");
						while($rowStatus = mysql_fetch_array($rsStatus)){                        
?>                         	<option value="<?=$rowStatus['fldId']?>"><?=$rowStatus['fldStatus']?></option>
<?						}
?>                  </select>
				</li>
                <li>
                    <label for="txt_nome">Cliente</label>
                    <input type="text" style=" width:300px" id="txt_nome" name="txt_nome" value="" />
                </li>
                <li>
                    <label for="txt_nomefantasia">Nome Fantasia</label>
                    <input type="text" style=" width:270px;" id="txt_nome_fantasia" name="txt_nome_fantasia" value="" />
                </li>
                <li>
                    <label for="sel_genero">G&ecirc;nero</label>
                    <select style="width:100px" id="sel_genero" name="sel_genero" >
                        <option value="0">Selecionar</option>
                        <option  value="1">Feminino</option>
                        <option  value="2">Masculino</option>
                    </select>
                </li>
                <li>
                    <label for="sel_tipo">Tipo</label>
                    <select style="width:100px" id="sel_tipo" name="sel_tipo">
                        <option value="1">F&iacute;sica</option>
                        <option value="2">Jur&iacute;dica</option>
                    </select>
                </li>
                <li>
                    <label for="txt_cpf_cnpj">CPF/CNPJ</label>
                    <input type="text" style=" width:145px" id="txt_cpf_cnpj" name="txt_cpf_cnpj" value="" />
                </li>
                <li>
                    <label for="txt_rg_ie">RG/IE</label>
                    <input type="text" style=" width:200px" id="txt_rg_ie" name="txt_rg_ie" value="" />
                </li>
                <li>
                    <label for="txt_telefone1">Telefone</label>
                    <input type="text" style=" width:100px" id="txt_telefone1" name="txt_telefone1" value="" />
                </li>
                <li>
                    <label for="txt_telefone2">Telefone 2</label>
                    <input type="text" style=" width:100px" id="txt_telefone2" name="txt_telefone2" value="" />
                </li>
<?				if($_SESSION["sistema_rota_controle"] == 1){					                   
?>                  <li>
                        <label for="txt_endereco_codigo">C&oacute;d. Endere&ccedil;o</label>
                        <input type="text" style=" width:95px" id="txt_endereco_codigo" name="txt_endereco_codigo" value="" />
                        <a href="endereco_busca" title="Localizar" class="modal" rel="680-380"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                    </li>
<?				}
?>                
                <li>
                    <label for="txt_endereco">Endere&ccedil;o</label>
                    <input type="text" style=" width:250px" id="txt_endereco" name="txt_endereco" value="" />
                </li>
                <li>
                    <label for="txt_numero">N&uacute;mero</label>
                    <input type="text" style=" width:45px" id="txt_numero" name="txt_numero" value="" />
                </li>
                <li>
                    <label for="txt_complemento">Complemento</label>
                    <input type="text" style=" width:140px" id="txt_complemento" name="txt_complemento" value="" />
                </li>
                <li>
                    <label for="txt_bairro">Bairro</label>
                    <input type="text" style=" width:215px" id="txt_bairro" name="txt_bairro" value="" />
                </li>
                <li>
                    <label for="txt_cep">CEP</label>
                    <input type="text" style=" width:115px" id="txt_cep" name="txt_cep" value="" />
                </li>
                <li>
                    <label for="txt_municipio_codigo">C&oacute;d. Munic&iacute;pio</label>
                    <input type="text" style=" width:100px" id="txt_municipio_codigo" name="txt_municipio_codigo" value="" />
                    <a href="municipio_busca" title="Localizar" class="modal" rel="680-380"><img style="margin-left:3px" src="image/layout/search.gif" alt="localizar" /></a>
                </li>
                <li>
                    <label id="disabled" for="txt_municipio">Munic&iacute;pio</label>
                    <input type="text" style=" width:200px; background:#EAEAEA" id="txt_municipio" name="txt_municipio" value="" />
                </li>
                <li>
                    <label id="disabled" for="txt_uf">UF</label>
                    <input type="text" style="width:40px; background:#EAEAEA" id="txt_uf" name="txt_uf" value="" />
                </li>
			</ul>
		</fieldset>
        <input type="submit" style="margin-top:14px; float:right" class="btn_enviar" name="btn_enviar" id="btn_enviar" value="salvar" title="Salvar" />
    </form>

	<script type="text/javascript">
	
        $('#txt_nome').focus();
		$('#frm_cliente_rapido').submit(function(e){
			e.preventDefault();
			var form 	= $(this).serialize();
			var campo 	= '<?= $_POST['params'][1]?>'; 
			
			$('div.modal-conteudo').load('modal/cliente_cadastro_rapido.php', {form : form, campo : campo});
		});
		
	</script>