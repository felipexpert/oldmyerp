<?php

	session_start();
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');

	if(isset($_POST['form'])){
		
		$serialize 	= $_POST['form'];
		parse_str($serialize, $form);
		
		$sql  = "INSERT INTO tblpedido_nfe_numero_inutilizado(fldNFe_Numero, fldMotivo, fldObservacao, fldData, fldHora, fldUsuario_Id, fldPerfil) VALUES";
		$data = date('Y-m-d');
		$hora = date('H:i:s');
		$user = $_SESSION['usuario_id'];
		
		$exec_sql  = false;
		$limite    = $form['hid_controle_qtd_numeros'];
		for($x=1;$x<=$limite;$x++) {
			
			if(isset($form['sel_nnfe_nao_utilizar_'.$x]) && $form['sel_nnfe_nao_utilizar_'.$x] > 0) {
				
				$numero = $form['hid_nfe_numero_'.$x];
				$motivo = $form['sel_nnfe_nao_utilizar_'.$x];
				$obs 	= $form['txt_nnfe_observacao_'.$x];
				$sql .= "('{$numero}', '{$motivo}', '{$obs}', '{$data}', '{$hora}', '{$user}', '1'),";
				$exec_sql = true;
			}
		}
		
		if($exec_sql == true) {
			//retirar a última vírgula
			$sql = substr($sql, 0, -1);
			if(!mysql_query($sql)) { echo mysql_error(); exit(); }else{
?>				<script type="text/javascript">
					$('div.modal-body').remove();
					$("#frm_pedido_perfil_fiscal").submit();
				</script>			
<?			}
		}
		unset($x, $i, $nfeSequencia, $comecoBuscaNFe, $fimBuscaNFe, $numero, $rsMotivosInutilizacao, $rowMotivosInutilizacao, $motivosInutilizacao, $motivo);
	}
	
	
	# JOAO #####################################################################################################################################################
	$comecoBuscaNFe = fnc_sistema('nfe_numero_nota');
	$fimBuscaNFe    = $_POST['params'][1] - 1;
	
	//preencher uma matriz com o intervalo entre o número da nota no sistema e o digitado pelo usuário
	$i = 0;
	for($x=$comecoBuscaNFe; $x<=$fimBuscaNFe; $x++) {
		$nfeSequencia[$i] = $x;
		$i++;
	}
	
	//exibindo ou não a tabela com os números pulados
	if(!empty($nfeSequencia)) {
		
		//pegando os motivos de inutilização pré-cadastrados no banco de dados
		$rsMotivosInutilizacao = mysql_query("SELECT * FROM tblpedido_nfe_numero_inutilizado_motivo");
		$x = 0;
		
		while($rowMotivosInutilizacao = mysql_fetch_array($rsMotivosInutilizacao)) {
			$motivosInutilizacao[$x]['id'] 	 = $rowMotivosInutilizacao['fldId'];
			$motivosInutilizacao[$x]['desc'] = $rowMotivosInutilizacao['fldDescricao'];
			$x++;
		}
?>		<h4 style="width: 95%">N&uacute;mero de pr&oacute;x. nota alterado. Favor selecionar o motivo de inutiliza&ccedil;&atilde;o.</h4>
		<br /><br />
        <form class="frm_detalhe" id="frm_inutilizar_numero" action="" method="post" style="width: 96%;">
            <div id="notas_puladas" style="margin: 0 auto; height: 220px; overflow-x: hidden;">
                <table>
                    <thead>
                        <tr>
                            <th>N&ordm; ausente</th>
                            <th>N&atilde;o utilizar por:</th>
                            <th>Observa&ccedil;&otilde;es</th>
                        </tr>
                    </thead>
                    <tbody>
<?						$x = 1;
						foreach($nfeSequencia as $numero) {
?>                            <tr>
                                <td>
                                    <?=$numero?>
                                    <input type="hidden" id="hid_nfe_numero_<?=$x?>" name="hid_nfe_numero_<?=$x?>" value="<?=$numero?>" />
                                </td>
                                <td>
                                    <select style="width:220px;height:20px" id="sel_nnfe_nao_utilizar_<?=$x?>" name="sel_nnfe_nao_utilizar_<?=$x?>" class="sel_nnfe_nao_utilizar">
                                        <option value="0"></option>
<?										foreach($motivosInutilizacao as $motivo){
?>											<option value="<?=$motivo['id']?>"><?=$motivo['desc']?></option>
<?										}
?>									</select>
                                </td>
                                <td><input type="text" id="txt_nnfe_observacao_<?=$x?>" name="txt_nnfe_observacao_<?=$x?>" value="" /></td>
                            </tr>
<?							$x++;
						}
?>					</tbody>
                </table>
                <input type="hidden" id="hid_controle_nfe_numero_perdido_enviar" name="hid_controle_nfe_numero_perdido_enviar" value="false" />
                <input type="hidden" id="hid_controle_qtd_numeros" name="hid_controle_qtd_numeros" value="<?=$x - 1?>" />
            </div>
            
            <div style="float:right;">
                <input type="submit" class="btn_enviar" name="btn_gravar" id="btn_gravar" value="Gravar" title="Gravar" />
            </div>
		</form>
<?	}
?>	
	<script type="text/javascript">
		
		function functionLoop(){
			erro = true;
			$('.sel_nnfe_nao_utilizar').each(function(){
				if($(this).find('option').filter(':selected').text() == 0 ){
					erro = false; 
				}
			});	
			return erro;
		}
		
		$('#btn_gravar').live('click',function(event){
			event.preventDefault();
			var form 	= $('#frm_inutilizar_numero').serialize();
			if(functionLoop() == true){
				$('div.modal-conteudo').load('modal/nfe_perfil_inutilizar_numero.php', {form : form});
			}
		});
		
	</script>