<?php
	
	session_start();
	
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');
	
	$documento_tipo = $_POST['params'][1];
	$cliente_id 	= $_POST['params'][2];
	
	if($documento_tipo == 'venda'){
		$sSQL = "SELECT 
				tblpedido.fldPedidoData as fldData,
				tblpedido.fldId,
				tblpedido_parcela.fldValor,
				tblpedido_parcela.fldPedido_Id,
				tblpedido_fiscal.fldNumero as fldNFe
				
			FROM tblpedido_parcela
			INNER JOIN tblpedido ON tblpedido.fldId = tblpedido_parcela.fldPedido_Id 
			LEFT JOIN tblpedido_fiscal ON tblpedido_fiscal.fldPedido_Id = tblpedido.fldId AND  tblpedido_fiscal.fldDocumento_Tipo = 1
			WHERE tblpedido_parcela.fldCredito = '1' AND tblpedido.fldCliente_Id = ".$cliente_id;
	}else{
		$sSQL = "SELECT 
				tblcompra.fldCompraData as fldData,
				tblcompra.fldId,
				tblcompra_parcela.fldValor,
				tblcompra_parcela.fldCompra_Id,
				tblpedido_fiscal.fldNumero as fldNFe
				
			FROM tblcompra_parcela
			INNER JOIN tblcompra ON tblcompra.fldId = tblcompra_parcela.fldCompra_Id 
			LEFT JOIN tblpedido_fiscal ON tblpedido_fiscal.fldPedido_Id = tblcompra.fldId AND  tblpedido_fiscal.fldDocumento_Tipo = 2
			WHERE tblcompra_parcela.fldCredito = '1' AND tblcompra.fldFornecedor_Id = ".$cliente_id;
	}
	
	$rsParcelas = mysql_query($sSQL);
	echo mysql_error();
?>
    <form class="table_form" id="frm_parcela_credito" action="" method="post">
    	<div id="table" style="width:540px">
            <div id="table_cabecalho" style="width:540px">
                <ul class="table_cabecalho" style="width:540px">
                    <li style="width:80px;text-align:right"><?=($documento_tipo == 'venda')? 'Venda' : 'Compra'?></li>
                   	<? if($_SESSION["sistema_nfe"] > 0){		?>
                    	<li style="width:70px;text-align:center">NFe</li>
                    <? }										?>
                    <li style="width:160px">Data</li>
                    <li style="width:<?=($_SESSION["sistema_nfe"] > 0)? '200px' : '270px' ?>;text-align:right">Credito</li>
                </ul>
            </div>
            <div id="table_container" style="width:540px">       
                <table id="table_general" class="table_general" summary="Lista de credito">
                	<tbody>
<?						$linha = "row";
						while($rowParcela = mysql_fetch_array($rsParcelas)){
?>							<tr class="<?= $linha; ?>">
								<td	style="width:80px; text-align:right"><?=str_pad($rowParcela['fldId'], 8, 0, STR_PAD_LEFT)?></td>
                                <? if($_SESSION["sistema_nfe"] > 0){		?>
									<td	style="width:70px; text-align:center; color:#C90"><?=str_pad($rowParcela['fldNFe'], 6, 0, STR_PAD_LEFT)?></td>
								<? }										?>
                                <td style="width:160px"><?=format_date_out($rowParcela['fldData'])?></td>
								<td style="width:<?=($_SESSION["sistema_nfe"] > 0)? '200px' : '270px' ?>; text-align:right;"><?=format_number_out($rowParcela['fldValor'])?></td>
							</tr>
<?							$linha = ($linha == "row" ? "dif-row" : "row");
						}
?>			 		</tbody>
				</table>
            </div>
        </div>
<?
			
		#BUSCO O CREDITO SE HOUVER, DESCONTANDO O QUE JA FOI UTILIZADO
		$credito_total 		= fnc_parcela_credito($cliente_id,$documento_tipo);
		$credito_utilizado 	= fnc_parcela_credito_utilizado($cliente_id,$documento_tipo);
		$credito_disponivel = number_format($credito_total - $credito_utilizado, 2, '.', '');
?>    


        <div class="saldo" style="width:532px;margin-left:5px">
            <p style="padding:5px;width:180px;float:left">Total cr&eacute;dito<span style="margin-left:6px width:80px;text-align:right" class="credito"><?=format_number_out($credito_total)?></span></p>
            <p style="padding:5px;width:160px;float:left">Utilizado				 <span style="margin-left:6px width:80px;text-align:right" class="credito"><?=format_number_out($credito_utilizado)?></span></p>
            <p style="padding:5px;width:160px;float:left">Dispon&iacute;vel		 <span style="margin:0;margin-left:6px width:80px;text-align:right" class="credito"><?=format_number_out($credito_disponivel)?></span></p>
        </div>
        
	</form>
	