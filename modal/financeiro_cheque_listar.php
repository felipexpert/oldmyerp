<script type="text/javascript" src="js/cheque.js"></script>
<?
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');

	ob_start();
	session_start();
	$tela_referencia_id = $_POST['params'][1];
	$referencia_id	 	= $_POST['params'][2];
	$timeStamp 			= $_SESSION['ref_timestamp'];
	
	if($timeStamp == '' or $timeStamp == null){
		die('erro ao exibir cheques, carregue esta página novamente');
	}
	
	if(isset($referencia_id)){
		$referencia_id 	= explode('+',$referencia_id);
		$count 			= count($referencia_id);
		//se tiver mais de um numero, mais de um pedido por exemplo
		$n = 0;
		while($n <= $count){
			$ref_id 	.= $referencia_id[$n].',';
			$n++;
		}
		$referencia_id = substr($ref_id, 0 ,strlen($ref_id) - 2);	
		
		if($tela_referencia_id == '1'){
			$filtroCheque	.= " OR (fldOrigem_Id IN ($referencia_id) AND fldOrigem_Movimento_Id = $tela_referencia_id)";
			$filtroCheque 	.= " OR (fldDestino_Id IN ($referencia_id) AND fldDestino_Movimento_Id = $tela_referencia_id)";
		}elseif($tela_referencia_id == '3'){
			$filtroCheque	.= " OR (fldOrigem_Id IN ($referencia_id) AND fldOrigem_Movimento_Id = $tela_referencia_id)";
		}else{
			$filtroCheque 	.= " OR (fldDestino_Id IN ($referencia_id) AND fldDestino_Movimento_Id = $tela_referencia_id)";
		}
	}
	
	if(isset($_POST['excluir'])){
		$cheque_id 	 		= $_POST['excluir'];
		$tela_referencia_id = $_POST['tela_referencia_id'];
		$referencia_id 		= $_POST['referencia_id'];
		
		#VERIFICO SE ESSE REGISTRO QUE ESTOU INSERINDO/EDITANDO É ORIGEM OU DESTINO DO CHEQUE, PARA QUANDO EXCLUIR RETIRAR APENAS A RESPECTIVA REFERENCIA
		if(isset($referencia_id)){
			$rsCheque  = mysql_query("SELECT fldOrigem_Id, fldDestino_Id, fldOrigem_Movimento_Id, fldDestino_Movimento_Id FROM tblcheque WHERE fldId = $cheque_id");
			$rowCheque = mysql_fetch_array($rsCheque);
			if($rowCheque['fldOrigem_Id'] == $referencia_id){
				$filtro_referencia = ",fldOrigem_Id ='', fldOrigem_Movimento_Id =''";
			}elseif($rowCheque['fldDestino_Id'] == $referencia_id){
				$filtro_referencia = ", fldDestino_Id='', fldDestino_Movimento_Id =''";
			}
		}
		mysql_query("UPDATE tblcheque SET fldTimeStamp ='' $filtro_referencia WHERE fldId = $cheque_id ");
		echo mysql_error();
	}
	echo mysql_error();

	if(isset($_POST['form'])){
	
		$serialize 	= $_POST['form'];	
		parse_str($serialize, $form);
		
		$cheque_id	= $form['hid_cheque_id'];
		$banco		= $form['txt_banco'];
		$agencia	= $form['txt_agencia'];
		$conta		= $form['txt_conta'];
		$numero		= $form['txt_numero'];
		$valor		= format_number_in($form['txt_valor']);
		$vencimento = format_date_in($form['txt_vencimento']);
		$correntista= $form['txt_correntista'];
		$CPF_CNPJ	= $form['txt_CPF_CNPJ'];
		$entidade	= $form['txt_entidade'];
		$data		= date("Y-m-d");
		
		$tela_referencia_id = $form['hid_tela_referencia_id'];
		$referencia_id 		= $form['hid_referencia_id'];
		
		if($cheque_id > 0){
			mysql_query("UPDATE tblcheque SET fldTimeStamp = '$timeStamp' WHERE fldId = '$cheque_id'");
		}else{
			mysql_query("INSERT INTO tblcheque
			(fldBanco, fldAgencia, fldConta, fldNumero, fldValor, fldVencimento, fldNome, fldCPF_CNPJ, fldEntidade, fldDataCadastro, fldTimeStamp)
			VALUES(
			'$banco',
			'$agencia',
			'$conta',
			'$numero',
			'$valor',
			'$vencimento',
			'$correntista',
			'$CPF_CNPJ',
			'$entidade',
			'$data',
			'$timeStamp'
			)");
		}

		echo mysql_error();
?>
		<script type="text/javascript">
			$('form#frm_cheque_novo input:not(#btn_inserir)').val('');
            $('#txt_banco').focus();
			$('#btn_inserir').attr('disabled', '');
        </script>
<?		
	}
	
?>
    <div class="form" style="width:750px; margin-bottom:10px; padding:0">
        <form id="frm_cheque_novo" action="" method="post" class="frm_detalhe" style="float:left" >
        	<fieldset>
            	<legend>inserir cheque</legend>
                <ul>
                	<li>
                    	<a style="margin-top:6px" href="financeiro_cheque_busca" class="btn_localizar modal" rel="650-400" title="localizar cheque"></a>
                    </li>
                    <li>
                        <label for="txt_banco">Banco</label>
                        <input type="text" id="txt_banco" name="txt_banco" value="" style="width:60px;text-align:right" />
                    </li>
                    <li>
                        <label for="txt_agencia">Ag&ecirc;ncia</label>
                        <input type="text" id="txt_agencia" name="txt_agencia" value="" style="width:70px;text-align:right" />
                    </li>
                    <li>
                        <label for="txt_conta">N&deg; Conta</label>
                        <input type="text" id="txt_conta" name="txt_conta" value="" style="width:110px;text-align:right" />
                    </li>
                    <li>
                        <label for="txt_numero">N&deg; Cheque</label>
                        <input type="text" id="txt_numero" name="txt_numero" value="" style="width:115px;text-align:right" />
                    </li>
                    <li>
                        <label for="txt_valor">Valor</label>
                        <input type="text" id="txt_valor" name="txt_valor" value="" style="width:90px;text-align:right" />
                    </li>
                    <li>
                        <label for="txt_vencimento">Vencimento</label>
                        <input class="calendario-mask" type="text" id="txt_vencimento" name="txt_vencimento" value="" style="width:70px" />
                    </li>
                    <li>
                        <label for="txt_correntista">Nome do correntista</label>
                        <input type="text" id="txt_correntista" name="txt_correntista" value="" style="width:260px" />
                    </li>
                    <li>
                        <label for="txt_CPF_CNPJ">CPF/CNPJ</label>
                        <input type="text" id="txt_CPF_CNPJ" name="txt_CPF_CNPJ" value="" style="width:115px" />
                    </li>
                    <li>
                        <label for="txt_entidade">Entidade</label>
                        <input type="text" id="txt_entidade" name="txt_entidade" value="" style="width:210px" />
                    </li>
                    <li>
                        <input type="hidden" id="hid_cheque_id" name="hid_cheque_id" value="" />
                        <input type="hidden" id="hid_tela_referencia_id"	name="hid_tela_referencia_id" 	value="<?=$tela_referencia_id?>" />
                        <input type="hidden" id="hid_referencia_id" 		name="hid_referencia_id" 		value="<?=$referencia_id?>" />
                    </li>
                    <input style="margin-top:12px" type="submit" class="btn_enviar" name="btn_inserir" id="btn_inserir" value="inserir" title="Salvar" />
                </ul>
            </fieldset>
        </form>
	</div> 
    
    <div id="table" style="width:750px">
        <div id="table_cabecalho" style="width:750px">
            <ul class="table_cabecalho" style="width:750px">
                <li style="width:60px;margin-right:5px;text-align:right">N&uacute;mero </li>
                <li style="width:150px">Correntista</li>
                <li style="width:150px">Entidade</li>
                <li style="width:80px;text-align:center">Valor</li>
                <li style="width:80px;text-align:center">Ag&ecirc;ncia</li>
                <li style="width:80px;text-align:center">Conta</li>
                <li style="width:80px;text-align:center">Banco</li>
                <li style="width:20px;">&nbsp;</li>
            </ul>
        </div>
        <div id="table_container" style="width:750px; height:200px">       
            <table id="table_general" class="table_general" summary="Lista de cheques inseridos">
               	<tbody>
<?					
					$id_array 	= array();
					$n 			= 0;
					$linha 		= "row";
					$sql		= "SELECT * FROM tblcheque WHERE fldTimeStamp = '$timeStamp' $filtroCheque ";
					$rsCheque 	= mysql_query($sql);
					while($rowCheque  = mysql_fetch_array($rsCheque)){
						$id_array[$n] = $rowCheque["fldId"];
						$n += 1;
?>
                        <tr class="<?= $linha; ?>">
                            <td style="width:60px;padding-right:6px; text-align:right"><?=$rowCheque['fldNumero']?></td>
                            <td style="width:150px"><?=$rowCheque['fldNome']?></td>
                            <td style="width:150px"><?=$rowCheque['fldEntidade']?></td>
                            <td style="width:80px; text-align:right"><?=format_number_out($rowCheque['fldValor'])?></td>
                            <td style="width:80px; text-align:right"><?=$rowCheque['fldAgencia']?></td>
                            <td style="width:80px; text-align:right"><?=$rowCheque['fldConta']?></td>
                            <td style="width:80px; text-align:right"><?=$rowCheque['fldBanco']?></td>
                            <td style="width:20px"><a href="<?=$rowCheque['fldId']?>" title="remover cheque" id="btn_excluir" class="a_excluir"></a></td>
                        </tr>
<?						$linha = ($linha == "row" ?	$linha = "dif-row": $linha = "row");
					}
?>	 			</tbody>
            </table>
        </div>
            
    
	<script type="text/javascript">
		$('#btn_inserir').click(function(event){
            event.preventDefault();
			valor = br2float($('form#frm_cheque_novo #txt_valor').val());

			if(valor > 0){
				$('#btn_inserir').attr('disabled', 'disabled');
				var form = $('#frm_cheque_novo').serialize();
				$('div.modal-conteudo:last').load('modal/financeiro_cheque_listar.php', {form : form});
			}else{
				alert("Valor inválido");
				 $('#txt_valor').focus();
			}
		});	
		
		$('.a_excluir').click(function(event){
            event.preventDefault();
			cheque_id = $(this).attr('href');
			referencia_id 		= $('#hid_referencia_id').val();
			tela_referencia_id 	= $('#hid_tela_referencia_id').val();
			$('div.modal-conteudo:last').load('modal/financeiro_cheque_listar.php', {excluir : cheque_id, referencia_id:referencia_id, tela_referencia_id:tela_referencia_id});
		
		});		
	</script>