<?
	
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');
	
	//classes html para destacar certos dias
	$eventoClass	= 'class="evento"';	
	
	//para mostrar o mês por extenso
	function meses($a) {
		switch($a) {
			case 1:  $mes = "Janeiro";   	break;
			case 2:  $mes = "Fevereiro"; 	break;
			case 3:  $mes = "Mar&ccedil;o"; break;
			case 4:  $mes = "Abril";     	break;
			case 5:  $mes = "Maio";      	break;
			case 6:  $mes = "Junho";     	break;
			case 7:  $mes = "Julho";     	break;
			case 8:  $mes = "Agosto";    	break;
			case 9:  $mes = "Setembro";  	break;
			case 10: $mes = "Outubro";   	break;
			case 11: $mes = "Novembro";  	break;
			case 12: $mes = "Dezembro";  	break;
		}
		
		return $mes;
	}
	
	//tratando as datas vindo via ajax $_POST['params'][x] - caso estejam em branco coloca o dia atual
	if($_POST['params'][1] == ''){
		$_POST['params'][1] = date('Y-m-d');
	}
	
	if($_POST['params'][2] == ''){
		$_POST['params'][2] = date('Y-m-d');
	}
	
?>

<div id="calendarios-container">
	<div id="calendario-inicial">
		<?
			
			//--------------------------------------------------------------------------------------------------------
			//data parâmetro para navegar entre os meses, vindo via ajax deste mesmo arquivo
			//cal-inicial[0] = anterior ou proximo
			//cal-inicial[1] = data parâmetro para incrementar ou decrementar um mês
			//$_POST['params'] = parâmetro via ajax vindo da página de financeiro_contas_pagar_listar
			
			$data = $dataParametro = (!isset($_POST['cal-inicial'])) ? $_POST['params'][1] : $_POST['cal-inicial'][1];
			
			if(isset($_POST['cal-inicial']) and $_POST['cal-inicial'][0] == 'anterior') {
				$data = date('Y-m-d',strtotime("-1 month", strtotime($dataParametro)));
			}
			elseif(isset($_POST['cal-inicial']) and $_POST['cal-inicial'][0] == 'proximo') {
				$data = date('Y-m-d',strtotime("+1 month", strtotime($dataParametro)));
			}
			
			$data = explode('-', $data);
			$dia = $data[2];
			$mes = $data[1];
			$ano = $data[0];
			
			//parametros para passar a data selecionada no calendario, padrão mês atual, para verificar quais os dias tem contas para pagar
			$dataInicialParam 	= $ano . '-' . $mes . '-' . date("d", mktime(0, 0, 0, date('m'), 1, date('Y')) + 1);
			$dataFinalParam		= $ano . '-' . $mes . '-' . date("d", mktime(0, 0, 0, date('m') + 1, 1, date('Y')) - 1);
			
		?>
		
		<table border="0" summary="Calend&aacute;rio" class="calendario_contas">
			<caption><a href="<?='anterior,'.$ano.'-'.$mes.'-'.$dia?>" title="M&ecirc;s anterior" id="cal_inicial-ant">&laquo;</a> <?=meses($mes) . " " . $ano ?> <a href="<?='proximo,'.$ano.'-'.$mes.'-'.$dia?>" title="Pr&oacute;ximo m&ecirc;s" id="cal_inicial-pro">&raquo;</a></caption>
			<thead>
			<tr>
				<th abbr="Domingo" title="Domingo" class="domingo">D</th>
				<th abbr="Segunda" title="Segunda">S</th>
				<th abbr="Ter&ccedil;a" title="Ter&ccedil;a">T</th>
				<th abbr="Quarta" title="Quarta">Q</th>
				<th abbr="Quinta" title="Quinta">Q</th>
				<th abbr="Sexta" title="Sexta">S</th>
				<th abbr="S&aacute;bado" title="S&aacute;bado">S</th>
			</tr>
			</thead>
			<tbody>
			<?php
				
				$data		  = strtotime($mes."/".$dia."/".$ano);
				$diaSemana	  = date('w', strtotime(date('n/\0\1\/Y', $data)));
				$totalDiasMes = date('t', $data);
				
				for($i=1,$d=1;$d<=$totalDiasMes;) {
					
					echo("<tr>");
					
					for($x=1;$x<=7 && $d <= $totalDiasMes;$x++,$i++) {
						
						if ($i > $diaSemana) {
							
							echo('<td><a class="link-dia" href="'. str_pad($d, 2, "0", STR_PAD_LEFT).'/'.$mes.'/'.$ano .'">'.$d++.'</a></td>');
							
						}
						
						else { echo("<td></td>"); }
						
					}
					
					for(;$x<=7;$x++) { echo("<td></td>"); }
					echo("</tr>");
					
				}
				
				unset($data, $diaSemana, $totalDiasMes, $i, $x, $d, $ano, $mes, $dia, $dataParametro);
				
			?>
			</tbody>
		</table>
	</div>
	
	
	
	<div id="calendario-final">
		<?
			//--------------------------------------------------------------------------------------------------------
			//data parâmetro para navegar entre os meses, vindo via ajax deste mesmo arquivo
			//cal-final[0] = anterior ou proximo
			//cal-final[1] = data parâmetro para incrementar ou decrementar um mês
			//$_POST['params'] = parâmetro via ajax vindo da página de financeiro_contas_pagar_listar
			
			$data = $dataParametro = (!isset($_POST['cal-final'])) ? $_POST['params'][2] : $_POST['cal-final'][1];
			
			if(isset($_POST['cal-final']) and $_POST['cal-final'][0] == 'anterior') {
				$data = date('Y-m-d',strtotime( "-1 month", strtotime($dataParametro)));
			}
			elseif(isset($_POST['cal-final']) and $_POST['cal-final'][0] == 'proximo') {
				$data = date('Y-m-d',strtotime( "+1 month", strtotime($dataParametro)));
			}
			
			$data = explode('-', $data);
			$dia = $data[2];
			$mes = $data[1];
			$ano = $data[0];
			
			//parametros para passar a data selecionada no calendario, padrão mês atual, para verificar quais os dias tem contas para pagar
			$dataInicialParam 	= $ano . '-' . $mes . '-' . date("d", mktime(0, 0, 0, date('m'), 1, date('Y')) + 1);
			$dataFinalParam		= $ano . '-' . $mes . '-' . date("d", mktime(0, 0, 0, date('m') + 1, 1, date('Y')) - 1);
			
		?>
		
		<table border="0" summary="Calend&aacute;rio" class="calendario_contas">
			<caption><a href="<?='anterior,'.$ano.'-'.$mes.'-'.$dia?>" title="M&ecirc;s anterior" id="cal_final-ant">&laquo;</a> <?=meses($mes) . " " . $ano ?> <a href="<?='proximo,'.$ano.'-'.$mes.'-'.$dia?>" title="Pr&oacute;ximo m&ecirc;s" id="cal_final-pro">&raquo;</a></caption>
			<thead>
			<tr>
				<th abbr="Domingo" title="Domingo" class="domingo">D</th>
				<th abbr="Segunda" title="Segunda">S</th>
				<th abbr="Ter&ccedil;a" title="Ter&ccedil;a">T</th>
				<th abbr="Quarta" title="Quarta">Q</th>
				<th abbr="Quinta" title="Quinta">Q</th>
				<th abbr="Sexta" title="Sexta">S</th>
				<th abbr="S&aacute;bado" title="S&aacute;bado">S</th>
			</tr>
			</thead>
			<tbody>
			<?php
				
				$data		  = strtotime($mes."/".$dia."/".$ano);
				$diaSemana	  = date('w', strtotime(date('n/\0\1\/Y', $data)));
				$totalDiasMes = date('t', $data);
				
				for($i=1,$d=1;$d<=$totalDiasMes;) {
					
					echo("<tr>");
					
					for($x=1;$x<=7 && $d <= $totalDiasMes;$x++,$i++) {
						
						if ($i > $diaSemana) {
							
							echo('<td><a class="link-dia" href="'. str_pad($d, 2, "0", STR_PAD_LEFT).'/'.$mes.'/'.$ano .'">'.$d++.'</a></td>');
							
						}
						
						else { echo("<td></td>"); }
						
					}
					
					for(;$x<=7;$x++) { echo("<td></td>"); }
					echo("</tr>");
					
				}
				
				unset($data, $diaSemana, $totalDiasMes, $i, $x, $d, $ano, $mes, $dia, $dataParametro);
				
			?>
			</tbody>
		</table>
	</div>
	
	<form action="?p=<?=$_POST['params'][3]?>" method="post" class="frm_detalhe" style=" width: 320px; clear: both; float: none; margin: 0 0 0 -160px; position: absolute; left: 50%; bottom: 0;">
	
		<ul>
			<li>
				<input type="text" id="txt_calendario_data_inicial" name="txt_calendario_data_inicial" value="<?=format_date_out($_POST['params'][1])?>" readonly="readonly" style="width: 80px; text-align: center;" />
			</li>
			
			<li>
				<input type="text" id="txt_calendario_data_final" name="txt_calendario_data_final" value="<?=format_date_out($_POST['params'][2])?>" readonly="readonly" style="width: 80px; text-align: center;" />
			</li>
			
			<li style="margin: 0;"><input style="margin: 4px 0 0 0;" type="submit" class="btn_enviar" name="btn_enviar" id="btn_enviar" value="Salvar" title="Salvar" /></li>
		</ul>
	
	</form>
	
</div>

<script type="text/javascript">
	
	//controlar a navegação do calendário inicial
	$('a#cal_inicial-ant, a#cal_inicial-pro').live('click', function(e){
		e.preventDefault();
		
		var params = $(this).attr('href').split(',');
		$('#calendario-inicial').load('modal/calendario_periodo.php #calendario-inicial', {'cal-inicial' : params});
	});
	
	//controlar a navegação do calendário final
	$('a#cal_final-ant, a#cal_final-pro').live('click', function(e){
		e.preventDefault();
		
		var params = $(this).attr('href').split(',');
		$('#calendario-final').load('modal/calendario_periodo.php #calendario-final', {'cal-final' : params});
	});
	
	//passar a data do calendario para a caixa de texto
	$('#calendario-inicial a.link-dia').live('click', function(e){
		e.preventDefault();
		
		$('#txt_calendario_data_inicial').val($(this).attr('href'));
	});
	
	$('#calendario-final a.link-dia').live('click', function(e){
		e.preventDefault();
		
		$('#txt_calendario_data_final').val($(this).attr('href'));
	});
	
</script>