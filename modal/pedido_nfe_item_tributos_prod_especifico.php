<fieldset style="width:920px;display:inline;margin:10px">
    <legend>Produtos Específicos</legend>
        <ul style="margin-left:8px">
            <li>
                <label for="sel_prod_especifico_tipo">Tipo de produto</label>
                <select name="sel_prod_especifico_tipo" class="sel_prod_especifico_tipo" id="sel_prod_especifico_tipo" style="width:170px;">
                    <option></option>
                    <?php
                        $sqlTipo_Fiscal = mysql_query("SELECT * FROM tblproduto_fiscal_tipo_especifico");
                        while($rowTipo_Fiscal = mysql_fetch_assoc($sqlTipo_Fiscal)){ ?>
                        <option id="<?=$rowTipo_Fiscal['fldId'];?>" <?=($rowProduto_Fiscal['fldTipo_Especifico'] == $rowTipo_Fiscal['fldId']) ? "selected='selected'" : '';?> ><?=$rowTipo_Fiscal['fldTipo'];?></option>
                        <? }
                    ?>
                </select>
            </li>
        </ul>

        <fieldset id="fset_prod_especifico_combustivel" style="width:910px;">
            <legend>Combustivel</legend>

            <ul>
                <li>
                    <label for="sel_produto_especifico_combustivel_codanp">Código ANP</label>
                    <select name="sel_produto_especifico_combustivel_codanp" id="sel_produto_especifico_combustivel_codanp" class="sel_produto_especifico_combustivel_codanp" style="width:170px">
                        <option></option>
                        <?php
                        $sqlANP = mysql_query("SELECT * FROM tblproduto_fiscal_anp");
                        while($rowANP = mysql_fetch_assoc($sqlANP)){ ?>
                        <option id="<?=$rowANP['fldId'];?>" <?=($rowProduto_Fiscal['fldCombustivel_Codigo_ANP'] == $rowANP['fldId']) ? "selected='selected'" : '';?> ><?=$rowANP['fldCodigo'];?></option>
                        <? }
                        ?>
                    </select>
                </li>
                <li>
                    <label for="txt_produto_especifico_combustivel_codif">CODIF</label>
                    <input name="txt_produto_especifico_combustivel_codif" id="txt_produto_especifico_combustivel_codif" class="txt_produto_especifico_combustivel_codif" type="text" style="width:170px" value="<?=$rowProduto_Fiscal['fldCombustivel_Codigo_CodIF']?>" />
                </li>
                <li>
                    <label for="sel_produto_especifico_combustivel_uf">UF</label>
                    <select name="sel_produto_especifico_combustivel_uf" id="sel_produto_especifico_combustivel_uf" style="width:60px">
                        <option></option>
                        <?php
                        $sqlUF = mysql_query("SELECT * FROM tblibge_uf ORDER BY fldSigla");
                        while($rowUF = mysql_fetch_assoc($sqlUF)){ ?> 
                        <option id="<?=$rowUF['fldCodigo'];?>" <?=($rowProduto_Fiscal['fldCombustivel_UF'] == $rowUF['fldCodigo']) ? "selected='selected'" : '';?> ><?=$rowUF['fldSigla'];?></option>
                        <? }
                        ?>
                    </select>
                </li>
                <li>
                    <label>Qtd. faturada em temperatura ambiente</label>
                    <input type="text" style="width:250px" name="txt_produto_especifico_combustivel_qtd_faturada" class="txt_produto_especifico_combustivel_qtd_faturada" id="txt_produto_especifico_combustivel_qtd_faturada" value="<?=$rowProduto_Fiscal['fldCombustivel_Qtd_Faturada']?>"/>
                </li>
            </ul>
        </fieldset>

        <fieldset style="width:910px; margin-bottom:10px">
            <legend>CIDE</legend>

            <ul>
                <li>
                    <label>Base de cálculo</label>
                    <input type="text" style="width:170px" class="txt_produto_especifico_combustivel_cide_bc" name="txt_produto_especifico_combustivel_cide_bc" id="txt_produto_especifico_combustivel_cide_bc" value="<?=$rowProduto_Fiscal['fldCombustivel_Cide_bCalculo'];?>" />
                </li>
                <li>
                    <label>Alíquota</label>
                    <input type="text" style="width:170px" name="txt_produto_especifico_combustivel_cide_aliquota" class="txt_produto_especifico_combustivel_cide_aliquota" id="txt_produto_especifico_combustivel_cide_aliquota" value="<?=$rowProduto_Fiscal['fldCombustivel_Cide_Aliquota'];?>" />
                </li>
                <li>
                    <label>Valor</label>
                    <input type="text" style="width:170px" name="txt_produto_especifico_combustivel_cide_valor" class="txt_produto_especifico_combustivel_cide_valor" id="txt_produto_especifico_combustivel_cide_valor" value="<?=$rowProduto_Fiscal['fldCombustivel_Cide_Valor'];?>" readonly='readonly'/>
                </li>
            </ul>

        </fieldset>

	</fieldset>