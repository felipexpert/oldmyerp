<div id="principal">
<?	
	ob_start();
	session_start();
	
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');
	require_once('../inc/fnc_financeiro.php');
	
	###################################################################################################################################################################################################
	if(isset($_POST["form"])){
		
		$serialize 	= $_POST['form'];	
		parse_str($serialize, $form);
		
		$conta 			= $_SESSION['sel_conta_id'];
		$marcador 		= $form['sel_marcador'];
		$descricao 		= mysql_escape_string($form['txt_descricao']);
		$entidade 		= mysql_escape_string($form['txt_entidade']);
		$pagamento_tipo = $form['sel_pagamento'];
		
		if($form['sel_tipo'] == '1'){
			$credito 	= format_number_in($form['txt_valor']);
			$debito 	= '0,00';
		}elseif($form['sel_tipo'] == '2'){
			$credito 	= '0,00';
			$debito 	= format_number_in($form['txt_valor']);
		}
		
		//1 = Movimento caixa
		$tipo 		= '1';
		$last_id 	= fnc_financeiro_conta_fluxo_lancar($descricao, $credito, $debito, $entidade, $pagamento_tipo, '', $tipo, $marcador, $conta);
		
		if(!empty($_SESSION['ref_timestamp'])){ //CASO A SESSAO ESTIVER EM BRANCO, PRA NAO ATUALIZAR TODOS OS CHEQUES 
			
			if($form['sel_tipo'] ==  '2'){
				$saida = '1';
			}

			$timestamp = $_SESSION['ref_timestamp'];
			$movimento_id = '1';
			$registro_id = $last_id;
			$conta_id = $conta;
			$mov_id = fnc_cheque_movimento_lancar($timestamp, $movimento_id, $registro_id, $conta_id, $saida);

			if($form['sel_tipo'] == '1'){
				//credito
				$origem_movimento_id = "";
				$destino_movimento_id = $mov_id;
			}else{
				//debito
				$origem_movimento_id = $mov_id;
				$destino_movimento_id = "";
			}
			
			#fnc_cheque_update($origem_id, $destino_id, $entidade, $origem_movimento_id, $destino_movimento_id, $timestamp);
		}
		unset($_SESSION['ref_timestamp']);
		/*
?>
        <img src="image/layout/carregando.gif" alt="carregando..." />
        <script type="text/javascript">
			window.location="index.php?p=financeiro&modo=conta_fluxo";
        </script> 
<?		*/ die;
	}
	###################################################################################################################################################################################################	
	
	$remote_ip 	= gethostbyname($REMOTE_ADDR);
	$_SESSION['ref_timestamp'] = $remote_ip.date("YmdHis");
	
?>	
	<div class="form" style="width:860px">
        <form class="frm_detalhe" id="frm_financeiro_novo" action="" method="post">
            <ul>
                <li>
                    <label for="txt_descricao">Descri&ccedil;&atilde;o</label>
                    <input type="text" style="width:200px" id="txt_descricao" name="txt_descricao" value="" />
                </li> 
               	<li>
                    <label for="txt_entidade">Entidade</label>
                    <input type="text" style="width:185px" id="txt_entidade" name="txt_entidade" value="" />
                </li> 
                <li>
					<label for="sel_marcador">Marcador</label>
					<select style="width:150px" id="sel_marcador" name="sel_marcador">
<?						$rsMarcador  = mysql_query("SELECT * FROM tblfinanceiro_conta_fluxo_marcador ORDER BY fldMarcador");
						while($rowMarcador = mysql_fetch_array($rsMarcador)){
?>							<option title="<?= $rowMarcador['fldDescricao'] ?>" value="<?=$rowMarcador['fldId'] ?>"><?=$rowMarcador['fldMarcador'] ?></option>
<? 						}
?>					</select>
				</li>
                <li>
                	<label for="sel_tipo">Tipo</label>
					<select style="width:70px" id="sel_tipo" name="sel_tipo" >
                    	<option value="1">Cr&eacute;dito</option>
                    	<option value="2">D&eacute;bito</option>
					</select>
            	</li>
                <li>
                	<label for="sel_pagamento">Forma pag.</label>
					<select style="width:100px" id="sel_pagamento" name="sel_pagamento" >
<?						$rsPagamento = mysql_query("select * from tblpagamento_tipo");
                        while($rowPagamento= mysql_fetch_array($rsPagamento)){
?>							<option value="<?=$rowPagamento['fldId'] ?>"><?= $rowPagamento['fldTipo']?></option>
<?						}
?> 					</select>
            	</li>
                <li>
                    <label for="txt_valor">Valor</label>
                    <input type="text" style="width:80px; text-align:right" id="txt_valor" name="txt_valor" value="0,00" />
                </li>
                <li>
                	<a style="margin:0" class="modal btn_cheque" name="btn_cheque" id="btn_cheque" title="cheques" href="financeiro_cheque_listar,7" rel="780-410">inserir cheques</a>
                </li>
                <li style="float:right; margin-right:10px">
                    <input type="submit" style="margin:0" class="btn_enviar" name="btn_gravar" id="btn_gravar" value="gravar" title="Gravar" />
                </li>
             </ul>
        </form>
	</div>
    
	<script type="text/javascript">
	
		$('#txt_descricao').focus();
				 
        $('#txt_valor').blur(function(){
			$(this).val(float2br(br2float($(this).val()).toFixed(2)));
		});
        $('#btn_gravar').click(function(event){
            event.preventDefault();
			
			valor = br2float($('#txt_valor').val());
			if(valor > 0){
				$('#btn_gravar').attr('disabled', 'disabled');
				var form 	= $('#frm_financeiro_novo').serialize();
				$('div.modal-conteudo:last').load('modal/financeiro_conta_fluxo_novo.php', {form : form});
			}else{
				alert("Valor inválido");
				$('#txt_valor').focus();
			}
        });	
        
    </script>