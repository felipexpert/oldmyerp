<?php
	ob_start();
	session_start();
	
	require_once('../inc/pdo.php');
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_estoque_enderecamento.php');
	$conn = new Conexao();
/*
	$estoque_id = (!isset($_GET['estoque_id'])) ? "1" : $_GET['estoque_id'];
	$nivel_id 	= (!isset($_GET['nivel_id']))	? "0" : $_GET['nivel_id'];
	/*
	($_SESSION['sel_estoque_id_enderecamento_novo'] == '') ? $_SESSION['sel_estoque_id_enderecamento_novo'] = 1 : ''; //SE NAO FOI SELECIONADO ESTOQUE AINDA
	$_SESSION['sel_estoque_id_enderecamento_novo'] = (isset($_GET['estoque_id'])) ? $_GET['estoque_id'] : $_SESSION['sel_estoque_id_enderecamento_novo'];
	
	//guardo o id do pai em uma sessao, para caso troque o estoque este id nao se perca
	($_SESSION['pai_id'] == '') ? $_SESSION['pai_id'] = 0 : '';
	$_SESSION['pai_id']  = (isset($_GET['pai_id'])) ? $_GET['pai_id'] : $_SESSION['pai_id'];

	/*
	//verifica se o pai_id existe, senao poe 0
	$checar_pai_id = mysql_num_rows(mysql_query("SELECT * FROM tblestoque_enderecamento WHERE fldId = ".$_SESSION['pai_id']." AND fldEstoque_Id = ".$_SESSION['sel_estoque_id_enderecamento_novo']));
	if(!$checar_pai_id){$_SESSION['pai_id'] = 0;}
		
	

	if(isset($_POST['btn_editar_enderecamento'])){ //*editando nivel
		$fld_elemento 	= $_POST['txt_enderecamento_editar_nome'];
		$fld_sigla 		= $_POST['txt_enderecamento_editar_sigla'];
		$fld_id 		= $_POST['txt_enderecamento_editar_id'];

		$query_editar = mysql_query("UPDATE tblestoque_enderecamento SET
									fldElemento = '$fld_elemento',
									fldSigla 	= '$fld_sigla'
									WHERE fldId = '$fld_id'");

		if($query_editar){ 
			header('Location: index.php?p=estoque&modo=enderecamento_novo&estoque_id='.$_SESSION['sel_estoque_id_enderecamento_novo'].'&pai_id='.$fld_id);
		}
		else{ ?>
		<div class="alert">
			<p class="erro">Erro! <?=mysql_error();?><p>
        </div>
<?		}
	}

	if(isset($_POST['btn_excluir'])){ //*excluindo nivel
		$fldelemento_id = $_POST['txt_enderecamento_editar_id'];
		//primeiro pega o pai deste id
		$sql_pai_id = mysql_fetch_assoc(mysql_query("SELECT fldPai_Id FROM tblestoque_enderecamento WHERE fldId = '$fldelemento_id'"));
		$pai_id = $sql_pai_id['fldPai_Id'];
		$ids = $fldelemento_id.getSubNiveis_Id($fldelemento_id, $_SESSION['sel_estoque_id_enderecamento_novo']);
		echo $ids;

		//deleta os produtos
		$update_produtos = mysql_query("DELETE FROM tblproduto_estoque_enderecamento WHERE fldEnderecamento_Id IN ($ids)");

		$delete_niveis = mysql_query("DELETE FROM tblestoque_enderecamento WHERE fldId IN ($ids)") or die(mysql_error());

		if($delete_niveis){
			header("Location: index.php?p=estoque&modo=enderecamento_novo&estoque_id=".$_SESSION['sel_estoque_id_enderecamento_novo']."&pai_id=".$pai_id);
		}
	}

	
	if(isset($_POST['btn_reiniciar'])){
		unset($_SESSION['hierarquia']);
	}
	*/
	
	
	$estoque_id = $_POST['params']['1'];
	$nivel_id 	= 0;
	
	if(isset($_POST['form'])){
		
		$serialize 	= $_POST['form'];	
		parse_str($serialize, $form);
		
		$nome 		= $form['txt_nome'];
		$sigla		= $form['txt_sigla'];
		$quantidade	= $form['txt_quantidade'];
		$estoque_id	= $form['hid_estoque_id'];
		$nivel_id	= $form['nivel'];
			
		if(isset($_POST['btn_next'])){
			
			$hierarquia = isset($_SESSION['hierarquia']) ? $_SESSION['hierarquia'] : array();
			$hierarquia = $hierarquia + array("$nome" => array("$quantidade", "$sigla"));
			$_SESSION['hierarquia'] = $hierarquia;
		}
		
		if(isset($_POST['btn_finalizar'])){
			$hierarquia = isset($_SESSION['hierarquia']) ? $_SESSION['hierarquia'] : array();
			//variaveis controladoras
			$pai_id = 0;
			$estoque = $estoque_id;
			$pai = $nivel_id;
			$ids = array();
			while (list($key, list($val, $sigla_array)) = each($hierarquia)){
			//percorre a $hierarquia, que � onde fica gravado o nome do nivel e sua quantidade.
				if($pai_id > 0){
				//caso $pai_id for diferente de 0, significa que j� n�o � o primeiro nivel, ou seja, haver� algum id referencial
					$id_controle = $ids;
					$ids = array();
					//guardamos os ids dentro de uma outra variavel para que o la�o de repeti��o n�o se perca e ap�s isso resetamos a variavel que recebe os ids
					foreach($id_controle as $pai){
						//controla quantas vezes o nivel ter� de ser gravado, de acordo com o nivel anterior (referencial, o pai)
						for($n=1;$n<=$val;$n++){
							//grava o nivel em si
							$elemento 	= $key.'_'.$n;
							$sigla 		= $sigla_array.'_'.$n;
							//"nome + controle"
							$sql = "insert into tblestoque_enderecamento (fldElemento, fldSigla, fldEstoque_Id, fldPai_Id) values (:elemento, :sigla, :estoque, :pai_id)";
							$consulta = $conn->prepare($sql);
							//carregar par�metros nos placeholders
							$consulta->bindParam(':elemento', $elemento, PDO::PARAM_STR);
							$consulta->bindParam(':sigla', $sigla, PDO::PARAM_STR);
							$consulta->bindParam(':estoque', $estoque_id, PDO::PARAM_INT);
							$consulta->bindParam(':pai_id', $pai, PDO::PARAM_INT);
							//executar a consulta
							$consulta->execute();
							$pai_id = $conn->lastInsertId();
							//ultimo id inserido						
							array_push($ids,$pai_id); 
							//acrescenta no array (que estar� limpo) o id pai para servir de referencia para o proximo nivel
						}
					}
				}
				else{
				//caso o pai_id for 0, significa que � o primeiro nivel, ent�o o procedimento utilizado para $id_controle � descartado
					for($n=1;$n<=$val;$n++){
						$elemento 	= $key.'_'.$n;
						$sigla 		= $sigla_array.'_'.$n;
						$sql = "insert into tblestoque_enderecamento (fldElemento, fldSigla, fldEstoque_Id, fldPai_Id) values (:elemento, :sigla, :estoque, :pai_id)";
						$consulta = $conn->prepare($sql);
						//carregar par�metros nos placeholders
						$consulta->bindParam(':elemento', $elemento, PDO::PARAM_STR);
						$consulta->bindParam(':sigla', $sigla, PDO::PARAM_STR);
						$consulta->bindParam(':estoque', $estoque_id, PDO::PARAM_INT);
						$consulta->bindParam(':pai_id', $pai, PDO::PARAM_INT);
						//executar a consulta
						$consulta->execute();
						$pai_id = $conn->lastInsertId();
						array_push($ids,$pai_id);
						//acrescenta no array (que estar� limpo) o id pai para servir de referencia para o proximo nivel
					}
				}
			}
			
			unset($_SESSION['hierarquia']);
?>			<img src="image/layout/carregando.gif" alt="carregando..." />
			<script type="text/javascript">
				window.location="index.php?p=estoque&modo=enderecamento";
			</script> 
<?			die;
		}
	}
?>

	<script type="text/javascript">
	
		$(document).ready(function(){
			
			$('#txt_nome').focus();
			//var urlDestino = 'filtro_ajax.php';
			var nivel_selecionado = $('#hid_nivel_id').val();
			var estoque_id = "<?=$estoque_id?>";
			var urlDestino = 'estoque_enderecamento_buscar.php';
			//CASO SEJA SELECIONADO UM NIVEL, MANTEM A LISTAGEM
			if(nivel_selecionado > 0){
				$.get(urlDestino,{click_estoque_subnivel:nivel_selecionado, estoque_id : estoque_id},function(valor){
					$(".arvore_modal ul#sub_nivel_"+nivel_selecionado).html(valor);
				});
				$("#tela_ramificacao li").find("span#"+nivel_selecionado).css('background-color', '#DDFFDD'); //adiciona o bg no atual
			}
			
			
			$('.arvore_modal .linkCarregar').live('dblclick', function(event) {
				//alert('teste');
				var nivel_id = $(this).attr('id');
				//alert($(this).parent('li').find("ul#sub_nivel_"+nivel_id).attr('class'));
				//carrega ndo o botao de editar com o id do registro selecionado
				var url = "estoque_enderecamento_editar,"+this.id;
				$('#btn_editar_nivel').attr('href', url);
				
				
				$("#tela_ramificacao li").find('span:first').removeClass('selected'); //limpa tudo, mostra os componentes do produto principal
				$("#tela_ramificacao li").find('span:first').css('background-color', 'transparent'); //limpa tudo, mostra os componentes do produto principal
				$(this).css('background-color', '#DDFFDD'); //adiciona o bg no atual
				$(this).addClass('selected');
				
				//VERIFICO SE ESTA VISIVEL, CASO DB CLICK REMOVE OS SUB
				if($("ul#sub_nivel_"+nivel_id+" li:visible").length > 0){
					$("ul#sub_nivel_"+nivel_id+" li").remove();
				
				}else{
					$.get(urlDestino,{click_estoque_subnivel:nivel_id, estoque_id : estoque_id},function(valor){
						$(".arvore_modal ul#sub_nivel_"+nivel_id).html(valor);
					});
				}
				
			});
						
			$('#selecionar_nivel').click(
				function(event) {
					confirmar = confirm("Adicionar novo em nivel selecionado?");
					if (confirmar){
						nivel = $("#tela_ramificacao li").find('span.selected').attr('id');
						$('#hid_nivel_id').val(nivel);
						$('#txt_nome').focus();
					}else{
						$('#hid_nivel_id').val('0');
						return;
					}
				}
			);
			
		});
		
	</script>
	<form class="frm_detalhe" style="width:230px; margin:0 5px; float:left" name="frm_estoque_enderecamento_novo" id="frm_estoque_enderecamento_novo" action="" method="post">
		<ul>
			<li>
				<label for="txt_nome">Nome do N&iacute;vel</label>
				<input type="text" name="txt_nome" id="txt_nome" value="" style="width:206px" autofocus>
			</li>
			<li>
				<label for="txt_sigla">Sigla</label>
				<input type="text" name="txt_sigla" id="txt_sigla" style="width:206px">
			</li>
			<li>
				<label for="txt_quantidade">Quantidade</label>
				<input type="text" name="txt_quantidade" id="txt_quantidade" style="width:206px">
			</li>
		</ul>
		<input type="hidden" name="nivel" id="hid_nivel_id" value="<?=$nivel_id?>">
		<input type="hidden" name="hid_estoque_id" value="<?=$estoque_id?>">
		<div id="table_action" style="margin: 5px 0 0 3px">
			<button type="submit" class="btn_enviar" name="btn_next" id="btn_next">adicionar n&iacute;vel</button>
			<button type="submit" name="btn_finalizar" id="btn_finalizar">finalizar</button>
		</div>
<?		if($_SESSION['hierarquia'] != ''){ ?>
            <div id="table_action" style="margin: 20px 0 0 3px">
                <ul>
                    <li style="width:100%; color:black; font-size:12px">fila de cria&ccedil;&atilde;o:</li>
<?					$array_dados = $_SESSION['hierarquia'];
					$ids = array();
					while (list($elemento, list($qtd, $sigla_array)) = each($array_dados)){ ?>
						<li style="width:100%; font-size:11px"><?=$elemento?> (<?=$sigla_array?>)</li>
<?					} ?>
				</ul>
			</div>
<?		} ?>
	</form>

    <div style="float:left;">
        <div id="table_cabecalho" style="width:440px; margin-top:10px; ">
            <ul class="table_cabecalho" style="width:440px; float:left;">
                <li style="display:block; padding:5px; width:100%;">
                    <span style="color:#666; float:left; padding: 1px 0 0 0;" id="0" class="linkCarregar titulo_tela">Local de cria&ccedil;&atilde;o:</span>
                    <input type="hidden" id="hid_nivel_selecionado" name="hid_nivel_selecionado" value="<?=$_SESSION['pai_id']?>" />
                </li>
            </ul>
        </div>
        <div id="table_container" style="width:430px; height:300px; border:1px solid #CCC; background:white; clear:both; padding:5px;">
            <ul id="tela_ramificacao" class="arvore_modal">
            	<?=getRamificacao(0, $estoque_id)?>
            </ul>
        </div>
    </div>

    <div id="table_action" style="float:right">
     	<ul id="action_button">
     		<li><a class="btn_novo" name="selecionar_nivel" id="selecionar_nivel">selecionar nivel</a></li>
        </ul>
    </div>
    
	<script type="text/javascript">
		
		$('#btn_next').click(function(event){
			event.preventDefault();
			
			var form = $('#frm_estoque_enderecamento_novo').serialize();
			$('div.modal-conteudo:last').load('modal/estoque_enderecamento_novo.php', {btn_next:true, form : form});
		});
		
		$('#btn_finalizar').click(function(event){
			event.preventDefault();
			
			var form = $('#frm_estoque_enderecamento_novo').serialize();
			$('div.modal-conteudo:last').load('modal/estoque_enderecamento_novo.php', {btn_finalizar:true, form : form});
		});
	</script>
    