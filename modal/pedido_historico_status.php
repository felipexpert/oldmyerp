<?php

	ob_start();
	session_start();
	require("../inc/con_db.php");
	
	$idPedido = $_POST['params'][1];
	
	$queryHistorico = "SELECT * FROM `tblpedido_status_historico` WHERE fldPedido_Id = '$idPedido' ORDER BY fldData ASC, fldHora ASC";
	
	$getHistorico = mysql_query($queryHistorico);
	
	$chkResultado = mysql_num_rows($getHistorico);
	
	
	if ($chkResultado == 0) { echo "<p style='text-align:center; font-size:14px; margin:30px 0;'>Histórico não disponível para esta venda</p>";}
	
	else
	{
	
	//function formata_data
	
	function formata_data($data)
	{
	//recebe o parâmetro e armazena em um array separado por -
	$data = explode('-', $data);
	//armazena na variavel data os valores do vetor data e concatena /
	$data = $data[2].'/'.$data[1].'/'.$data[0];
	
	//retorna a string da ordem correta, formatada
	return $data;
	}
	
?>

<style type="text/css">

	* {font-size:12px;}

	#dados {clear:both; width:530px;}

	#itens li {float: left; margin:0; padding:5px; text-align:center;}
	
	#dados tr td {padding:5px;}

</style>
	
    <div id="table" style="width:590px;">
        <div id="table_container" style="width: 590px; height: 150px;">      
            <table id="table_general" class="table_general" summary="Tipos de acompanhamentos cadastrados!" style="width: 100%;">
				
				<tr>
					<td style="width: 40%; border-right: 1px solid #ccc; background:#666; color:#FFF; text-align:center">Status</td>
					<td style="width: 15%; border-right: 1px solid #ccc; background:#666; color:#FFF; text-align:center">Data</td>
					<td style="width: 15%; border-right: 1px solid #ccc; background:#666; color:#FFF; text-align:center">Horário</td>
					<td style="width: 20%; background:#666; color:#FFF; text-align:center">Usuário</td>
				</tr>
				
				<?php
				
				while($rowHistorico = mysql_fetch_array($getHistorico)){
				
				$id = $rowHistorico['fldId'];
				$pedido_id = $rowHistorico['fldPedido_Id'];
				$status_id = $rowHistorico['fldStatus_Id'];
				$data = $rowHistorico['fldData'];
				$hora = $rowHistorico['fldHora'];
				$usuario = $rowHistorico['fldUsuario_Id'];
				
				$getUsuario = mysql_query("SELECT * FROM tblusuario WHERE fldId = '$usuario'");
				$getStatus  = mysql_query("SELECT * FROM tblpedido_status WHERE fldId = '$status_id'");
				
				$rowUser = mysql_fetch_assoc($getUsuario);
				$rowStatus = mysql_fetch_assoc($getStatus);
				$usuario = $rowUser['fldUsuario'];
				$status = $rowStatus['fldStatus'];
				
				?>
				<tr>
					<td style="width: 40%; border-right: 1px solid #ccc; background:#F1F9FF;"><?php echo $status;?></td>
					<td style="width: 15%; border-right: 1px solid #ccc; background:#F1F9FF; text-align:center"><?php echo formata_data($data);?></td>
					<td style="width: 15%; border-right: 1px solid #ccc; background:#F1F9FF; text-align:center"><?php echo $hora;?></td>
					<td style="width: 20%; background:#F1F9FF; text-align:center"><?php echo $usuario;?></td>
				</tr>
				
				
				<?php
				
				}
				
				?>

			</table>
        </div>
    </div>
	
<?php } ?>