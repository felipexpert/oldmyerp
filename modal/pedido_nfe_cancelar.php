
	<p class="nfe_valido">Cancelar NF-e.</p>
<?	
	ob_start();
	session_start();
	header('Content-type: text/html; charset=UTF-8');
	require('../inc/con_db.php');
	require('../inc/fnc_general.php');
	require_once('../nfephp/libs/ToolsNFePHP.class.php');
		
	if(isset($_POST['form'])){
		
		$campo 		= $_POST['campo'];
		$serialize 	= $_POST['form'];	
		parse_str($serialize, $form);
		
		$nfe_chave 		=  $form['hid_nfe_chave'];
		$justificativa	=  $form['txa_nfe_justificativa'];
		
		$rsProtocolo 	= mysql_query("SELECT fldnfe_protocolo, fldambiente FROM tblpedido_fiscal WHERE fldchave = '$nfe_chave'");
		$rowProtocolo 	= mysql_fetch_array($rsProtocolo);
		$protocolo 		= $rowProtocolo['fldnfe_protocolo'];
		
		$tpAmb 			= $rowProtocolo['fldambiente'];
		$ambiente 		= ($tpAmb == '1')? 'producao': 'homologacao' ;
	
	
		$modSOAP = '2';
		$nfe  	 = new ToolsNFePHP;
		$return  = $nfe->cancelEvent($nfe_chave, $protocolo, $justificativa,'', $modSOAP);
		$msg 	 = $nfe->errMsg;
		if($return == '135'){ #CANCELADA
			mysql_query('UPDATE tblpedido_fiscal SET fldnfe_status_id = "6", fldnfe_cancela_motivo = "'.$justificativa.'" WHERE fldchave = "'.$nfe_chave.'"');
			echo mysql_error();
			$msg  = "\n[cStat]   = 135";
			$msg .= "\n[xMotivo] = NFe cancelada com sucesso.";
			
			$old_file = '../nfe_myerp/'.$ambiente.'/enviadas/aprovadas/'.$nfe_chave.'-nfe.xml';
			$file = '../nfe_myerp/'.$ambiente.'/canceladas/xml/'.$nfe_chave.'-nfe.xml';
			if(copy($old_file, $file)){
				unlink($old_file);
			}
		}
		
		$msg = preg_replace("/\r?\n/", "\\n", addslashes($msg));
?>		<script language="javascript">
			var msg_erro = '<?= $msg?>';
			$('#txa_nfe_retorno').append(msg_erro);
			$('#txa_nfe_retorno').focus();
		</script>

        <form id="frm_nfe_cancelar_retorno" class="frm_detalhe" action="" method="post" style="width:480px">    
			<textarea style="width:480px; height:180px" id="txa_nfe_retorno" name="txa_nfe_retorno" readonly="readonly" class="txa_nfe_erro"></textarea>
            <input style="width:100px"  type="submit" id="btn_nfe_ok" name="btn_nfe_ok" value=" ok " />
        </form>
        
        <script language="javascript">
			
			$('#btn_nfe_ok').click(function(e){
				e.preventDefault();
				window.location="index.php?p=pedido&modo=listar";	
			});
			
			$('a.modal-fechar').remove();
			//n fechar a janela modal ao teclar esc
			$('div.modal-body:last').keydown(function(event){
				if(event.keyCode == 27){
					return false;
				}
			});
		</script>
        
<?		die;	
	}
	
	$chave = $_POST['params'][1];
?>
    <form id="frm_nfe_cancelar" class="frm_detalhe" action="" method="post" style="width:480px">  
    	<input type="hidden" name="hid_nfe_chave" id="hid_nfe_chave" value="<?=$chave?>" />  
    	<ul>
	        <li>
            	<label for="txa_nfe_justificativa">Justificativa</label>
                <textarea style="width:480px; height:100px" id="txa_nfe_justificativa" name="txa_nfe_justificativa" maxlength="255" ></textarea>
                <span style="font-size: 10px; font-style: italic;">(15 a 255 caracteres)</span>
			</li>
            <li style=" width:490px">
            	<button style="float:right;margin:0" class="btn_gravar" id="btn_enviar" name="btn_enviar"> enviar</button>
            </li>
            
            <li class="img_load" style="margin-left:180px;display:none">
            	<img src="image/layout/carregando.gif" alt="carregando..." /><br />
            </li>
		</ul>        
    </form>
    
  
	<script type="text/javascript">
	
        $('#txt_nfe_chave').focus();
		
		$('#frm_nfe_cancelar').submit(function(e){
			e.preventDefault();
			
			$('li.img_load').show();
			var form 	= $(this).serialize();
			
			$('div.modal-conteudo').load('modal/pedido_nfe_cancelar.php', {form : form});
		});
		
	</script>