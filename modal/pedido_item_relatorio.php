<?	require("../inc/con_db.php");
	require("../inc/fnc_general.php");
	ob_start();
	session_start();

	if(isset($_POST["relatorio_tipo"])){
	
		$tipo = $_POST["relatorio_tipo"];
		if($tipo == 'comum'){
			$relatorio = '.php';
		}else if($tipo == 'agrupado'){
			$relatorio = '_vendas.php';
		}else if($tipo == 'peso'){
			$relatorio = '_peso.php';
		}else{
			$relatorio = '_peso_novo.php?d1='.$_POST['d1'].'&d2='.$_POST['d2'];
		}
		
		if(isset($_POST['sel_ordem']) && $_POST['sel_ordem'] != ''){
			$ordem = '?order='.$_POST['sel_ordem'];
		}
?>
        <script type="text/javascript">
			relatorio = '<?= $relatorio.$ordem?>';
			window.open("pedido_item_relatorio"+relatorio	);
			$('.modal-body:last').remove();
        </script> 
<?	}
	
	$dataInicial = format_date_out($_POST['params'][1]);
	$dataFinal 	 = format_date_out($_POST['params'][2]); ?>

    <form id="frm_relatorio" class="frm_detalhe" method="post" action="">
        <ul>
			<li>
                <label for="sel_item_relatorio_tipo">Tipo de Relat&oacute;rio:</label>
                <select class="sel_item_relatorio_tipo" style="width:150px;" id="sel_item_relatorio_tipo" name="sel_item_relatorio_tipo">
                    <option value="comum">Comum</option>
                    <option value="pesos">Pesos</option>
                    <option value="agrupado">Agrupado</option>
                    <option value="pesos_novo">Pesos (agrupado)</option>
                </select>
			</li>
			<li>
                <label for="sel_ordem">Ordenar por:</label>
                <select class="sel_ordem" style="width:180px;" id="sel_ordem" name="sel_ordem">
                    <option value="">Manter ordem de listagem</option>
                    <option value="fornecedor">Fornecedor</option>
                    <option value="categoria">Categoria</option>
                    <option value="marca">Marca</option>
                </select>
			</li>
            <li style="float:right;">
                <input type="hidden" name="d1" id="d1" value="<?=$_POST['params'][1]?>">
                <input type="hidden" name="d2" id="d2" value="<?=$_POST['params'][2]?>">
            	<input type="button" class="btn_enviar" name="btn_enviar" id="btn_enviar" value="Enviar" title="Enviar" />
			</li>
        </ul>
    </form>
	
	<script>
    
        $('#btn_enviar').click(function(event){
            event.preventDefault();
                
            relatorio_tipo 	= $('#sel_item_relatorio_tipo').val();
            sel_ordem	 	= $('#sel_ordem').val();
			d1				= $('#d1').val();
			d2				= $('#d2').val();
            $('div.modal-conteudo:last').load('modal/pedido_item_relatorio.php', {relatorio_tipo : relatorio_tipo, sel_ordem:sel_ordem, d1:d1, d2:d2});
                
        });	
        
    </script>