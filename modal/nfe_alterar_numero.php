<?php

	session_start();
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');
	
	$documento_tipo = $_POST['params'][1];
	$documento_id 	= $_POST['params'][2];

	$sqlNumero = mysql_query("SELECT fldnumero FROM tblpedido_fiscal WHERE fldpedido_id = $documento_id AND fldDocumento_Tipo = $documento_tipo");
	$rowNumero = mysql_fetch_assoc($sqlNumero);
	echo mysql_error();
	$numero = $rowNumero['fldnumero'];
	
?>
	
        <form class="frm_detalhe" id="frmAlterarNumero" name="frmAlterarNumero" method="post" action="">
            <div id="notas_puladas" style="margin: 0 auto;">
                <table>
                    <thead>
                        <tr>
                            <th>Número atual</th>
                            <th style="width:220px;">Motivo da troca:</th>
                            <th>Observa&ccedil;&otilde;es</th>
                        </tr>
                    </thead>
                    <tbody>
                          <tr>
	                          <td>
									<?=$numero?>
                                    <input type="hidden" id="hid_nfe_numero_atual" name="hid_nfe_numero_atual" value="<?=$numero?>" />
									<input type="hidden" id="hid_pedido_id" name="hid_pedido_id" value="<?=$documento_id?>" />
                                </td>
                                <td>
                                    <select style="width:220px;height:20px" id="sel_alterar_motivo" name="sel_alterar_motivo" class="sel_nnfe_nao_utilizar">
                                        <option value="0"></option>
										<?php
										
											$sqlMotivo = mysql_query("SELECT * FROM tblpedido_nfe_numero_inutilizado_motivo");
											
											while($rowMotivo = mysql_fetch_array($sqlMotivo))
											
											{
										
										?>
											<option value="<?=$rowMotivo['fldId']?>"><?=$rowMotivo['fldDescricao']?></option>
										<? } ?>
									</select>
                                </td>
                                <td><input type="text" id="txt_alterar_observacao" name="txt_alterar_observacao" value="" /></td>
                            </tr>
					</tbody>
                </table>
					
				<h3 id="alert" style="margin:25px 0 0 0; text-align:right;">Numeros disponiveis</h5>
				
				<?php //lista os numeros disponiveis para troca
					//Procedimento para procurar por números de NFes ausentes/pulados
					$limiteBuscaNFe = 100;
					
					//busca pelos números de NFes que estão sendo utilizados para poder comparar com números excluídos ou desfeitos. Essa comparação é feita para não aparecer na tela de reutilização números já utilizados
					$rsNumerosNFeUtilizados = mysql_query(" SELECT fldnumero FROM tblpedido_fiscal INNER JOIN tblpedido ON tblpedido_fiscal.fldpedido_id = tblpedido.fldId
															AND tblpedido.fldExcluido =0 AND tblpedido_fiscal.fldDocumento_Tipo = 1
																UNION
															SELECT fldnumero FROM tblpedido_fiscal INNER JOIN tblcompra ON tblpedido_fiscal.fldpedido_id = tblcompra.fldId
															AND tblcompra.fldExcluido =0 AND tblpedido_fiscal.fldDocumento_Tipo = 2
															ORDER BY fldnumero LIMIT {$limiteBuscaNFe}");
					$x = 0;
					while($rowNumerosNFe = mysql_fetch_array($rsNumerosNFeUtilizados)) {
						$nfeNumerosUtilizados[$x] = $rowNumerosNFe['fldnumero'];
						$x++;
					}
					
					########################################################################################################################################################################
					//todos os números de NFes desconsiderando números utilizados
					/*$rsNFe = mysql_query("SELECT tblpedido_fiscal.fldnumero FROM tblpedido_fiscal
										  INNER JOIN tblpedido ON tblpedido.fldId = tblpedido_fiscal.fldpedido_id
										  WHERE tblpedido.fldTipo_Id = 3
										  ORDER BY tblpedido_fiscal.fldnumero DESC LIMIT $limiteBuscaNFe"
										);*/
										
					$rsNFe = mysql_query("SELECT tblpedido_fiscal.fldnumero FROM tblpedido_fiscal
										  ORDER BY tblpedido_fiscal.fldnumero DESC LIMIT $limiteBuscaNFe"
					);
					
					//para evitar erro quando não há nenhuma nota
					
					if(mysql_num_rows($rsNFe) > 0) {
						$x = 0;
						while($rowNFe = mysql_fetch_array($rsNFe)) {
							$nfeSequencia[$x] = $rowNFe['fldnumero'];
							$comecoBuscaNFe   = $rowNFe['fldnumero']; //será o último valor, pois a ordenação está decrescente
							$x++;
						}
						//pega o próximo número do sistema (-1 para o próximo número não aparecer na tela de reutilização)
						$fimBuscaNFe = (fnc_sistema('nfe_numero_nota') - 1);
						//buscar números de NFe que foram puladas/descartados que possuem uma justificativa
						$rsNFeInutilizada = mysql_query("SELECT fldNFe_Numero FROM tblpedido_nfe_numero_inutilizado WHERE fldNFe_Numero BETWEEN {$comecoBuscaNFe} AND {$fimBuscaNFe}");
						
						$x = 0;
						$nfeInutilizadas = array();
						while($rowNFeInutilizada = mysql_fetch_array($rsNFeInutilizada)) {
							$nfeInutilizadas[$x] = $rowNFeInutilizada['fldNFe_Numero'];
							$x++;
						}
						sort($nfeSequencia);
						sort($nfeInutilizadas);
						//preencher uma matriz com o intervalo a ser comparado com os números de NFes do banco de dados, dispensando os números que possuem justificativas
						$i = 0;
						for($x=$comecoBuscaNFe; $x<=$fimBuscaNFe; $x++) {
							if(!in_array($x, $nfeInutilizadas)) $intervaloNFe[$i] = $x;
							$i++;
						}
						//verificar se há algum intervalo, caso não, não há números para serem reaproveitados
						if(!empty($intervaloNFe)) {
							//primeiro: compara as matrizes e retornando apenas os números que constam na primeira e não estão na segunda
							//segundo:  junta duas matrizes, o resultado do primeiro passo com o intervalo de números NFe
							//terceiro: remove os números válidos (utilizados em NFes)
							$numerosFaltando = array_unique(array_diff(array_merge($intervaloNFe, array_diff($intervaloNFe, $nfeSequencia)), $nfeNumerosUtilizados));
						}
					}
					//pegando os motivos de inutilização pré-cadastrados no banco de dados
					$rsMotivosInutilizacao = mysql_query("SELECT * FROM tblpedido_nfe_numero_inutilizado_motivo");
					$x = 0;
					
					while($rowMotivosInutilizacao = mysql_fetch_array($rsMotivosInutilizacao)) {
						$motivosInutilizacao[$x]['id'] 	 = $rowMotivosInutilizacao['fldId'];
						$motivosInutilizacao[$x]['desc'] = $rowMotivosInutilizacao['fldDescricao'];
						$x++;
					}
?>
				<div id="table" style="width:500px; margin:10px auto 0 auto;">
					<div id="table_container" style="width:500px; height: 163px; max-height:200px;">
						<table id="table_general" class="table_general" summary="Numeros inutilizados" style="width: 100%;">
							<tr>
								<td style="width: 80px; border-right: 1px solid #ccc; background:#666; color:#FFF; text-align:center">N&ordm; ausente</td>
								<td style="width: 50px; border-right: 1px solid #ccc; background:#666; color:#FFF; text-align:center">Utilizar</td>
								<td style="width: 130px; border-right: 1px solid #ccc; background:#666; color:#FFF; text-align:center"></td>
							</tr>

<?					$x = 1;
					foreach($numerosFaltando as $numero) { ?>
						<tr>
							<td style="text-align:center;"><?=$numero?></td>
							<td style="text-align:center;">
								<input type="radio" id="rad_nnfe_utilizar_n_perdido_<?=$x?>" name="rad_nnfe_n_perdido" value="<?=$numero?>" />
								<input type="hidden" id="hid_nfe_numero_<?=$x?>" name="hid_nfe_numero_<?=$x?>" value="<?=$numero?>" />
							</td>
							<td></td>
						</tr>
						<?	$x++; } ?>
						<tr>
							<td style="text-align:center;"><?=fnc_sistema('nfe_numero_nota')?></td>
							<td style="text-align:center;">
								<input type="radio" id="rad_nnfe_utilizar_n_perdido" name="rad_nnfe_n_perdido" value="<?=fnc_sistema('nfe_numero_nota')?>" />
								<input type="hidden" id="hid_nfe_numero_proximo" name="hid_nfe_numero_proximo" value="<?=fnc_sistema('nfe_numero_nota')?>" />
							</td>
							<td style="text-align:center;">próxima nota</td>
						</tr>
						</table>
					</div>
				</div>
            </div>
            
                <input type="submit" name="btn_action" id="btn_confirmar" class="btn_disabled" value="confirmar" disabled="disabled"	title="confirmar alteracao de numero" style="clear:both; margin-top:8px; float:right;" />
            
		</form>
		
		
<!-- validacao select e radio -->
<script>

	$("#sel_alterar_motivo").change(function(){
		
		if($("#sel_alterar_motivo").val() == 0){ $("#btn_confirmar").attr("disabled", "disabled"); $("#btn_confirmar").addClass("btn_disabled"); $("#btn_confirmar").removeClass("btn_general"); }
		else{
			if($("input[name='rad_nnfe_n_perdido']").is(':checked')){ $("#btn_confirmar").removeAttr("disabled"); $("#btn_confirmar").removeClass("btn_disabled"); $("#btn_confirmar").addClass("btn_general"); }
		}
		
	})
	
	$("input[name='rad_nnfe_n_perdido']").click(function(){
	
		if($("#sel_alterar_motivo").val() != 0){ $("#btn_confirmar").removeAttr("disabled"); $("#btn_confirmar").removeClass("btn_disabled"); $("#btn_confirmar").addClass("btn_general"); }
		
	})

</script>