<div class="form" style="width:390px">
<?	
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');
	require_once('../nfephp/libs/ToolsNFePHP.class.php');
	
	$tpAmb 		= fnc_sistema('nfe_ambiente_tipo');
	$ambiente 	= ($tpAmb == '1')? 'producao': 'homologacao' ;

	$nfe_ids = explode(';', $_POST['params'][1]);
	$ids	 = implode(',', $nfe_ids);
	
	$rsChave 	= mysql_query("SELECT fldchave FROM tblpedido_fiscal WHERE fldPedido_Id in ($ids)");
	$rowChave 	= mysql_fetch_array($rsChave);
	echo mysql_error();
	$file_name 	= $rowChave['fldchave'].'-nfe.xml';
	$file		= '../nfe_myerp/'.$ambiente.'/assinadas/'.$file_name;
	####################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################
	$nfe = new ToolsNFePHP;
	if(file_exists($file)){
		//se o arquivo existir, carregue o em uma string
		$docXml = file_get_contents($file);
		//coloque o path completo para o schema a ser usado
		$pathXSD = '../nfephp/schemes/PL_006q/nfe_v2.00.xsd';
		//verifique a validade do xml em relação ao seu schema construtivo
		$aErr = array();
		$aRet = $nfe->validXML($docXml, $pathXSD, $aErr);
		//tratando o retorno da validação
		
		if($aRet > 0){
			mysql_query("UPDATE tblpedido_fiscal SET fldnfe_Status_Id = '2' WHERE fldPedido_Id in ($ids)");
		
			#ADICIONEI ESSE ELSE PARA COPIA DO ARQUIVO XML PRA PASTA
			$old_file 	= $file;
			$file 		= '../nfe_myerp/'.$ambiente.'/validadas/'.$file_name;
			if(copy($old_file, $file)){
				unlink($old_file);
			}
		}else{
			$old_file 	= $file;
			$file 		= '../nfe_myerp/'.$ambiente.'/rejeitadas/'.$file_name;
			
			if(copy($old_file, $file)){
				unlink($old_file);
			}
			 // retorno de erro da variavel do tools
			$msg_erro = htmlspecialchars($nfe->errMsg);
			
		}
	}else{
		$msg_erro = "Arquivo xml n&atilde;o encontrado. \nVerifique se NFe foi assinada.";
	}
	
	if(isset($msg_erro)){
?>		<p class="nfe_invalido">Nota n&atilde;o validada.</p>
<?	}else{
?>		<p class="nfe_valido">Nota validada com sucesso.</p>
<?	}
?>
	<form id="frm_nfe_validar" class="frm_detalhe" action="" method="post">        	
		<textarea id="txa_erro" style="width:630px" name="txa_erro" class="txa_nfe_erro" readonly="readonly"><?='<PRE>'. ($msg_erro) ? $msg_erro : 'Nenhum erro.'.'</PRE><BR>'; ?></textarea>
    </form>
</div>