<?

	$estoque_id = $_POST['params']['1'];

	require_once('../inc/con_db.php');
	require_once('../inc/fnc_estoque_enderecamento.php');
?>

<script type="text/javascript">
	$(document).ready(function(){
		var urlDestino = 'filtro_ajax.php';
		var estoque_id = "<?=$estoque_id?>";
		/*---------------------------------------------------------------*/
		//bloqueia a seleção quando der 2 clicks
		if (typeof document.onselectstart!="undefined")
		document.onselectstart=function(){return false}
		else //FF
		document.getElementById('tela_ramificacao').style.MozUserSelect = "none";
		document.getElementById('table_cabecalho').style.MozUserSelect = "none";
		/*---------------------------------------------------------------*/
		
		var timerIn, iconCarregar, ultimoIdCarregado;
				
		$(".submenu").hide().end(); //esconde todos os subcomponentes

		$('.linkCarregar').dblclick(function(event) {
				$('.submenu:first', this).toggle("fast"); //exibe os subcomponentes (componente do componente)
				$('.folder:first', this).toggleClass("openFolder"); //troca a classe de pastinha fechada para pastinha aberta
		event.stopPropagation()});
		
		$('.linkCarregar').click(function(event) { //CARREGA A TABELA
		if (this.id != ultimoIdCarregado) //evita repetição de carregamento
		{
			$("#hid_nivel_id").val(this.id);
			$.post(urlDestino,{estoque_enderecamento_sigla:this.id,estoque_id:estoque_id},function(valor){
				$("#temp_titulo_nivel").val(valor);
			});
		}
		event.stopPropagation()});
		
		$('.titulo_tela').click(function(event){
			$("#tela_ramificacao li").find('span:first').css('background-color', 'transparent'); //limpa tudo, mostra os componentes do produto principal
		})
		
		$('#tela_ramificacao li').click(function(event){
			$("#tela_ramificacao li").find('span:first').css('background-color', 'transparent'); //limpa tudo, mostra os componentes do produto principal
			$(this).find('span:first').css('background-color', '#ffa7a7'); //adiciona o bg no atual
		})
		
		
		$('#mostrar_tudo').click( //mostra todos os subcomponentes
			function(event) {
				$('.submenu').slideDown("fast");
				$('.folder', '#tela_ramificacao').addClass("openFolder");
				event.stopPropagation()
			}
		);
		
		$('#esconder_tudo').click( //esconde todos os subcomponentes
			function(event) {
				$('.submenu').slideUp("fast");
				$('.openFolder', '#tela_ramificacao').attr("class", "folder");
				event.stopPropagation();
			}
		);

		$("#btn_estoque_enderecamento_filtro_enviar").click(function(){
			$('#txt_estoque_enderecamento_sigla').val($('#temp_titulo_nivel').val());
			$('#hid_enderecamento_nivel_id').val($('#hid_nivel_id').val());
			$('.modal-fechar').trigger('click');
		});
	});
</script>

<form action="" method="post" class="frm_detalhe" style="margin-left:3px">
	
			<div style="float:left;">
	            <div id="table_cabecalho" style="width:350px;">
	                <ul class="table_cabecalho" style="width:350px; float:left;">
	                    <li style="display:block; padding:5px; width:100%;">
	                        <span style="color:#666; float:left; padding: 1px 0 0 0;" id="0" class="linkCarregar titulo_tela">Endere&ccedil;amento</span>
	                        <span id="mostrar_tudo" title="Expandir tudo" style="margin-right:8px"></span>
	                        <span id="esconder_tudo" title="Recolher tudo"></span>
	                    </li>
	                </ul>
	            </div>
	            <div id="table_container" style="width:340px; height:270px; border:1px solid #CCC; background:white; clear:both; padding:5px;">
	                <ul id="tela_ramificacao">
	                            <?=getRamificacao(0, $estoque_id)?>
	                </ul>
	            </div>
	        </div>

            <div id="table_action" style="width:350px;">
            		<input type="hidden" id="hid_nivel_id" name="hid_nivel_id">
            		<span style="float:left; font-size:11px; margin:10px 5px 0 5px;">Local selecionado</span>
            		<input style="width:120px; float:left; margin-top:8px; font-size:12px" type="text" id="temp_titulo_nivel" name="temp_titulo_nivel" disabled="disabled" />
             		<input type="button" style="margin:4px 0 0 0; float:right" id="btn_estoque_enderecamento_filtro_enviar" name="btn_estoque_enderecamento_filtro_enviar" class="btn_gravar" value="selecionar" />
            </div>
	
</form>