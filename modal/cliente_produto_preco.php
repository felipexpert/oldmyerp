<?
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');
	if(isset($_POST['form'])){
		
		$serialize 	= $_POST['form'];
		parse_str($serialize, $form);
		
		$valor 		= format_number_in($form['txt_valor']);
		$preco_id	= $form['hid_preco_id'];
		$cliente_id	= $form['hid_cliente_id'];
		$data_atual = date('Y-m-d');
		$user 		= $_SESSION['usuario_id'];
	
		$sql  = "UPDATE tblcliente_produto_preco SET fldValor = '$valor', fldDataAlteracao = '$data_atual', fldUsuario_Id = '$user' WHERE fldId = $preco_id";
		if(mysql_query($sql)){
?>			
			<script type="text/javascript">
				cliente_id = '<?=$cliente_id?>';
               	window.location.href="index.php?p=cliente_detalhe&id="+cliente_id+"&modo=produto_preco";
               	$('div.modal-body:last').remove();
            </script>
<?		}
		die();
	}
	
	$preco_id	= $_POST['params'][1];
	$cliente_id	= $_POST['params'][2];
	$decimal 	= fnc_sistema('venda_casas_decimais');
	$rsProduto 	= mysql_query("SELECT tblcliente_produto_preco.fldValor, tblproduto.fldCodigo, tblproduto.fldNome
							FROM tblcliente_produto_preco INNER JOIN tblproduto ON tblproduto.fldId = tblcliente_produto_preco.fldProduto_Id
							WHERE tblcliente_produto_preco.fldId = $preco_id");
	$rowProduto = mysql_fetch_array($rsProduto);
?>
	<h4 style="width:90%; text-align:left">Produto: <?=$rowProduto["fldCodigo"] . " - " . $rowProduto["fldNome"]?></h4>
    <form id="frm_produto_preco" action="" method="post" class="frm_detalhe" style="float:left;margin:10px">
        <ul style=" width:375px">
            <li>
                <label for="txt_valor">Valor</label>
                <input  type="text"	 id="txt_valor"			name="txt_valor" 		value="<?=format_number_out($rowProduto["fldValor"],$decimal)?>" style="width:150px;text-align:right" />
                <input type="hidden" id="hid_preco_id" 		name="hid_preco_id" 	value="<?=$preco_id?>" />
                <input type="hidden" id="hid_cliente_id" 	name="hid_cliente_id" 	value="<?=$cliente_id?>" />
            </li>
            <li style="float:right">
                <input type="submit"  id="btn_gravar"	name="btn_gravar" 	value="Gravar" class="btn_enviar" title="Gravar" />
            </li>
        </ul>
    </form>
    
    <script type="text/javascript">
        var decimal = <?=$decimal?>;
        $('#txt_valor').select();
		$('#btn_gravar').live('click', function(event){
			event.preventDefault();
										
			var form 	= $('#frm_produto_preco').serialize();
			$('div.modal-conteudo').load('modal/cliente_produto_preco.php', {form : form});
			$('input#btn_gravar').die("click");
		});
		
		$('#txt_valor').blur(function(){
			$(this).val(float2br(br2float($(this).val()).toFixed(decimal)));
	   });
    </script>