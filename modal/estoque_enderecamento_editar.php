<?php
	require_once('../inc/pdo.php');
	$conn = new Conexao();
	$id = $_POST['params']['1'];
	$query = $conn->prepare("SELECT * FROM tblestoque_enderecamento WHERE fldId = :id_nivel");
	$query->bindParam(":id_nivel", $id, PDO::PARAM_STR);
	$query->execute();
	$dados = $query->fetch();
?>

<script type="text/javascript">
	$(document).ready(function(){
		$('#btn_excluir').click( //esconde todos os subcomponentes
			function(event) {
				confirmar = confirm("Deseja mesmo excluir este nivel e seus subniveis?");
				if (confirmar){
					return true;
				}
				else{
					return false;
				}
			}
		);
	});
</script>

<form class="frm_detalhe" style="width:248px; margin:0 5px; float:left" name="estoque_enderecamento_assistente" id="estoque_enderecamento_assistente" action="" method="post">
		<ul>
			<li>
				<label for="txt_enderecamento_editar_nome">Nome do N&iacute;vel</label>
				<input type="text" name="txt_enderecamento_editar_nome" id="txt_enderecamento_editar_nome" value="<?=$dados['fldElemento']?>" style="width:223px" autofocus>
				<input type="hidden" name="txt_enderecamento_editar_id" id="txt_enderecamento_editar_id" value="<?=$dados['fldId']?>">
			</li>
			<li>
				<label for="txt_enderecamento_editar_sigla">Sigla</label>
				<input type="text" name="txt_enderecamento_editar_sigla" id="txt_enderecamento_editar_sigla" style="width:223px" value="<?=$dados['fldSigla']?>">
			</li>
			<li style="margin: -9px 13px 0 0; float:right;"><input type="submit" class="btn_enviar" name="btn_editar_enderecamento" id="btn_editar_enderecamento" value="gravar" title="Enviar" /></li>
			<li style="margin: 1px -9px 0 0; float:right;"><input type="submit" name="btn_excluir" id="btn_excluir" value="excluir" title="Excluir" /></li>
		</ul>
</form>