<?	
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');
?>

    <script type="text/javascript">
        $('#sel_pedido_pagamento_tipo').focus();
        
        var form = $('#frm_pedido_rapido_novo').serialize();
        $('#hid_form_venda').val(form);
        $('#frm_pedido_pagamento #txt_pedido_total').val($('#frm_pedido_rapido_novo #txt_pedido_total').val());
        
        //no campo de cliente, cair prox campo com enter 
        $("#txt_cliente_nome").keydown(function(event) {
            switch(event.keyCode){
                case 13:
                    event.preventDefault();
                    $("#btn_gravar").focus();
                break;
            }
        });
        
    </script>

    <form class="frm_detalhe " style="width:500px" id="frm_pedido_pagamento" action="pedido_rapido_gravar.php" method="post">
		<div class="pedido_rapido_pagamento">
            <ul>
                <li style="margin-top:10px; margin-bottom:5px">
               		<label class="recebido" for="sel_pedido_pagamento_tipo">&nbsp;</label>
                    <select id="sel_pedido_pagamento_tipo" name="sel_pedido_pagamento_tipo" class="recebido" style="width: 210px" title="Forma de Pagamento">
                    	<option value="1">dinheiro</option>
                    	<option value="2">cart&atilde;o cr&eacute;dito</option>
                    	<option value="3">cart&atilde;o d&eacute;bito</option>
                    	<option value="4">cheque</option>
                    </select>
                </li>
                <li>
                	<label class="recebido" for="txt_pedido_recebido">Recebido</label>
               		<input type="text" class="valor" style="width:203px" id="txt_pedido_recebido" name="txt_pedido_recebido" value="0,00" autocomplete="off" />
                </li>
                <li>
                    <div id="parcelas" style="width:210px;height:85px">
                        <div id="hidden">
                            <ul class="parcela_detalhe" title="recebido_lista_item">
                                <li>
                                    <input class="txt_recebido_tipo" style="width:83px" type="text" id="txt_recebido_tipo_0" name="txt_recebido_tipo_0" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input class="txt_recebido_valor" style="width:96px" type="text" id="txt_recebido_valor_0" name="txt_recebido_valor_0" value="" readonly="readonly" />
                                </li>
                            </ul>
                           
							<input type="hidden" name="hid_recebido_cursor" 	id="hid_recebido_cursor" value="0" />
                            <input type="hidden" name="hid_total_recebido" 		id="hid_total_recebido" value="0,00" />
                            <input type="hidden" name="hid_controle_recebido" 	id="hid_controle_recebido" value="0" />
                            <input type="hidden" name="hid_form_venda" 			id="hid_form_venda" value="" />
                       </div> 
                    </div>
                </li>
            </ul>
        </div>
        <div class="pedido_rapido_pagamento">
            <ul>
                <li>
                    <label for="txt_pedido_total" class="total">Total</label>
                    <input type="text" class="total" style="width:205px" id="txt_pedido_total" name="txt_pedido_total" value="0,00" readonly="readonly" />
                </li>
                
                <li>
                	<label class="recebido" for="txt_pedido_troco">Troco</label>
               		<input type="text" class="troco" style="width:203px; margin-left: 0" id="txt_pedido_troco" name="txt_pedido_troco" value="0,00" readonly="readonly"/>
                </li>
                <li>
					<input type="hidden" id="hid_submit" name="hid_submit" 	value="1" />
                    <input type="hidden" id="hid_print"  name="hid_print"  	value="0" />
                    <input type="hidden" id="hid_ecf" 	 name="hid_ecf"	 	value="0" />
                    
                    <input type="submit" style="width:210px" class="btn_submit" name="btn_gravar_print" id="btn_gravar_print" 	value="[F11] Finalizar e Imprimir" title="Finalizar e Imprimir" />
                    <input type="submit" style="width:210px" class="btn_submit" name="btn_gravar" 		id="btn_gravar" 		value="[F10] Finalizar" title="Finalizar Venda" />
                    <input type="submit" style="width:210px" class="btn_submit" name="btn_gravar_ecf" 	id="btn_gravar_ecf" 	value="[F9] Finalizar e Cupom Fiscal" title="Finalizar e Cupom Fiscal" />
                    <a class="modal" style="display:none" id="btn_ecf" href="ecf_documento" rel="345-130" title="Emitir Cupom Fiscal"></a>
               </li>
            </ul>
        </div>
<?
		if(isset($_POST['params'][2])){
			$cliente_id      = $_POST['params'][2];
			$SQL             = mysql_query("SELECT fldCodigo, fldNome FROM tblcliente WHERE fldId = $cliente_id");
			$rowCliente      = mysql_fetch_array($SQL);
			$clienteCodigo   = $rowCliente['fldCodigo'];
			$clienteNome     = $rowCliente['fldNome'];
		}else{
			$clienteCodigo 	= 0;
			$clienteNome 	= 'Consumidor';  
			$cliente_id 	= 0;
		}

        if(isset($_POST['params'][1])){
            $queryPedido    = mysql_query("SELECT * from tblpedido WHERE fldId = ".$_POST['params'][1]);
            $rowPedido      = mysql_fetch_assoc($queryPedido);
        }

?>
        <ul style="margin-left:12px">
            <li>
                <label for="txt_cliente_codigo">Cliente [F4]</label>
                <input type="text" style="width:70px; text-align:right;" id="txt_cliente_codigo" name="txt_cliente_codigo" value="<?=$clienteCodigo?>" />
                <a href="cliente_busca" title="Localizar" class="modal" rel="950-380"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
            </li>
            <li>
                <label for="txt_cliente_nome">&nbsp;</label>
                <input type="text" style=" width:325px" id="txt_cliente_nome" name="txt_cliente_nome" value="<?=$clienteNome?>" />
                <input type="hidden" id="hid_cliente_id" name="hid_cliente_id" value="<?=$cliente_id?>" />
            </li>
            <ul>
                <li>
                    <label for="txt_pedido_endereco">Endereço:</label>
                    <input type="text" style="width:433px" id="txt_pedido_endereco" name="txt_pedido_endereco" value="<?= ($_POST['params'][1]) ? $rowPedido['fldEndereco'] : '';?>">
                </li>
            </ul>
            <? if(fnc_sistema("pedido_retirado_por") == "1") { ?>
            <ul>
                <li>
                    <label for="txt_retirado_por">Retirado por</label>
                    <input type="text" style="width:433px" id="txt_retirado_por" name="txt_retirado_por" maxlength="62" value="<?= ($_POST['params'][1]) ? $rowPedido['fldRetirado_Por'] : '';?>">
                </li>
            </ul>
            <? } ?>
        </ul>
	</form>
