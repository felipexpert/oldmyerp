<?
	ob_start();
	session_start();
	//Fica lento com mais de 5 buscas, mas não dá problema com outras modais.
	//Ao fechar a tela e abrir novamente o peso na memória é aliviado momentaneamente
?>
	<script type="text/javascript">
		//evita que o enter faça alguma acao
		
		//NAO SEI PQ TEM ESSE BLOCO, COMENTADO 26/11/2012 - LUANA
		/*					   
		$("input#buscar").keydown(function(event){
			if (event.keyCode == 13){
				return false;
			}			   
		});	
		*/

		$('document').ready(function(){
			$('#buscar').focus();

			//$('#buscar').select();
			/*
			$('#buscar').click(function(){
				$('#buscar').val('');
			});
			*/

			$('#buscar').keyup(busca_valor);
			$('#buscar').bind('keydown', busca_teclas);
			
			$("#chk_busca_qualquer_parte").click(function(event){
				busca_valor(event);
				$('#buscar').focus();
			});
			
			
			function busca_Scroll(deslocamento){
				var cursorAtual 	= parseInt($('#hid_busca_cursor').val());
				var cursorLimite 	= parseInt($('#hid_busca_controle').text() - 1);
				var mover = false;
				if(deslocamento<0){
					if(cursorAtual > 0){mover = true;}
				}
				else{
					if(cursorAtual < cursorLimite){mover = true;}
				}
				if(mover){
					$('a[title=busca_lista_item]').removeClass('cursor');
					cursorNovo = cursorAtual + deslocamento;
					//corrigir cursor se ultrapassar limites
					if(cursorNovo < 0){cursorNovo = 0;}
					if(cursorNovo > cursorLimite){cursorNovo = cursorLimite;}
					
					$('a[title=busca_lista_item]:eq('+cursorNovo+')').addClass('cursor');
					$('#hid_busca_cursor').val(cursorNovo);
					var strNome = $('li[title=nome]:eq('+cursorNovo+')').text();
					$('#buscar').val(strNome);
					
					//19 - altura da linha de exibição de cada registro
					$('#alvo').scrollTop($('#alvo').scrollTop() + 19 * deslocamento);
				}
			}
			
			function busca_teclas (event) {
				//console.log(this);
				//alert(event.keyCode);
				switch(event.keyCode){
					case 13: //enter

						$('#buscar').focus();
						//alert($('#hid_busca_controle').text());
						//verificar se tem resultado na busca
						if($('#hid_busca_controle').text() == 0){
							alert('Atenção! Não há produto selecionado!');
							return false;
						}
						else{
							var intCursor = $('#hid_busca_cursor').val();
							var codigo_produto =  $('a[title=busca_lista_item]:eq('+intCursor+')').attr('href');
							$('input#txt_produto_codigo').val(codigo_produto);
							$('input#txt_produto_codigo').focus();
							$('input#txt_produto_codigo').blur();
							$('#txt_produto_nome').focus();
							$('.frm_busca_produto').parents('.modal-body').remove();
						}
						break;
					case 38: //up
					
						busca_Scroll(-1);
						break;
						
					case 40: //down
						busca_Scroll(1);
						break;
						
					case 33: //page up
						busca_Scroll(-10);
						break;
						
					case 34: //page down
						busca_Scroll(10);
						break;
					
				}
			}
			function busca_valor (event) {
				//console.log(this);
				//alert(event.keyCode);
				switch(event.keyCode){
					case 13:
					case 38:
					case 40:
					case 33:
					case 34:
						break;
					
					default:
						//reset da posição do cursor para acompanhar o scroll
						$('#hid_busca_cursor').val(0);
						
						//alert($('#chk_busca_qualquer_parte').val());
						
						bolQualquerParte = $("#chk_busca_qualquer_parte").is(":checked");
						
						if(bolQualquerParte){
							strBusca = '%' + $('#buscar').val() + '%';
						}
						else{
							strBusca = $('#buscar').val() + '%';
						}
						
						
						$.post('modal/produto_busca_consulta.php',
						{busca: strBusca},
						function(data){
							if ($('#buscar').val()!=''){
								$('#alvo').show();
								$('#alvo').empty().html(data);
							}else{
								$('#alvo').empty();
							}
						});
						break;
				}

			}
			
			$('a.selecionar').live('click', function(){
				//e.preventDefault();
				var codigo_produto = $(this).attr('href');
				$('input#txt_produto_codigo').val(codigo_produto);
				$('input#txt_produto_codigo').focus();
				$('input#txt_produto_codigo').blur();
				$('#txt_produto_nome').focus();
				$('.frm_busca_produto').parents('.modal-body').remove();
				$('a.selecionar').die("click");
			});		
		});	
		
	</script>
   
	<? require("../inc/con_db.php"); ?>

	<form id="frm_busca" name="frm_busca" class="frm_busca_produto" style="width:940px">
        <fieldset>
            <legend>Busca de produto</legend>
			<ul>
				<li style="float:left" >
					<input type="text" style="width: 920px;" id="buscar" value="Digite o nome do produto" size="80" >
				</li>
                <li style="float:right; margin:0 10px 12px 0">
                	<input type="checkbox" style="width:0px; height:0; margin:5px 5px 0 0; float:left;" id="chk_busca_qualquer_parte" name="chk_busca_qualquer_parte">
                	<label style="font-size:12px;" for="chk_busca_qualquer_parte">qualquer parte do campo</label>
                </li>
			</ul>
		
            <ul id="busca_cabecalho" style="width:940px">
                <li style="width:112px">C&oacute;digo</li>
                <li style="width:562px">Produto</li>
                <li style="width:102px">Marca</li>
                <li style="width:72px">Valor</li>
				<li style="width:72px">Estoque</li>
            </ul>
            <input type="hidden" id="hid_busca_cursor" value="0" />
            <div id="alvo" style="width:940px"></div>
    	</fieldset>
	</form>
