<?php
	
	session_start();
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');
    
    $idRegistro = $_POST['params'][2];
	
	//se $idRegistro for nulo, significa que é um novo agendamento e não atualização
	if(!is_null($idRegistro)) {
	
		$sql = "SELECT tblcrm_acompanhamento_agenda.*, tblcrm_acompanhamento_cliente_tipo.fldTipo_Nome, tblfuncionario.fldNome, tblusuario.fldUsuario FROM tblcrm_acompanhamento_agenda
				INNER JOIN tblcrm_acompanhamento_cliente_tipo ON tblcrm_acompanhamento_cliente_tipo.fldId = tblcrm_acompanhamento_agenda.fldTipo_Id
				INNER JOIN tblfuncionario ON tblfuncionario.fldId = tblcrm_acompanhamento_agenda.fldFuncionario_Id
				INNER JOIN tblusuario ON tblusuario.fldId = tblcrm_acompanhamento_agenda.fldUsuario_Id
				WHERE tblcrm_acompanhamento_agenda.fldId = " . mysql_real_escape_string($idRegistro);
				
		$rowAgenda = mysql_fetch_array(mysql_query($sql));
		
	}

?>

<div class="form" style="width: 710px;">
	<form class="frm_detalhe" id="frm_acompanhamento_agenda" action="?p=crm_acompanhamento_cliente_detalhe" method="post">
		
		<fieldset style="padding: 10px; width: 670px;" id="acompanhamento_agenda">
			<legend>Agendamento:</legend>
			
			<input type="hidden" id="hid_usuario_id" name="hid_usuario_id" value="<?=$_SESSION['usuario_id']?>" />
			<input type="hidden" id="hid_chamado_referencia_id" name="hid_chamado_referencia_id" value="<?=$_POST['params'][1]?>" />
<?			if(!is_null($idRegistro)) {
?>
			<input type="hidden" id="hid_agendamento_id" name="hid_agendamento_id" value="<?=$idRegistro?>" />
<?			}
			else {
?>
			<input type="hidden" id="hid_agendamento_novo" name="hid_agendamento_novo" value="true" />
<?			}
?>		
			<ul>
				<li>
					<label for="sel_chamado_agendamento_tipo">Tipo</label>
					<select id="sel_chamado_agendamento_tipo" name="sel_chamado_agendamento_tipo">
<?					$rsChamadoTipo = mysql_query("SELECT fldId, fldTipo_Nome FROM tblcrm_acompanhamento_cliente_tipo WHERE fldExcluido = 0 ORDER BY fldTipo_Nome");

					if(mysql_num_rows($rsChamadoTipo) > 0) {
						while($rowChamadoTipo = mysql_fetch_array($rsChamadoTipo)) {
?>
						<option value="<?=$rowChamadoTipo['fldId']?>"<?=((isset($rowAgenda) && $rowAgenda['fldTipo_Id'] == $rowChamadoTipo['fldId']) ? ' selected="selected"' : '')?>><?=$rowChamadoTipo['fldTipo_Nome'];?></option>
<?						}
					}
					else {
?>
						<option value="0">Cadastre antes um tipo de agendamento</option>
<?
					}
?>					</select>
					<a href="crm_acompanhamento_cliente_tipo" class="modal btn_add_dados" rel="480-300" title="Adicionar novo Tipo">Adicionar novo Tipo</a>
				</li>

				<li>
					<label for="sel_chamado_agendamento_funcionario">Funcion&aacute;rio encarregado</label>
					<select id="sel_chamado_agendamento_funcionario" name="sel_chamado_agendamento_funcionario">
<?							$rsChamadoFuncionario = mysql_query("SELECT fldId, fldNome FROM tblfuncionario WHERE fldDisabled = '0' ORDER BY fldNome");							
						
					if(mysql_num_rows($rsChamadoFuncionario) > 0) {
						while($rowChamadoFuncionario = mysql_fetch_array($rsChamadoFuncionario)) {
?>
							<option value="<?=$rowChamadoFuncionario['fldId']?>"<?=((isset($rowAgenda) && $rowAgenda['fldFuncionario_Id'] == $rowChamadoFuncionario['fldId']) ? ' selected="selected"' : '')?>><?=$rowChamadoFuncionario['fldNome'];?></option>
<?						}
					}
					else {
?>
						<option value="0">N&atilde;o h&aacute; funcion&aacute;rios cadastrados!</option>
<?
					}
?>					</select>
				</li>
				
				<li>
					<label for="txt_chamado_agendamento_data">Data</label>
					<input type="text" style="width: 80px; text-align: center" class="calendario-mask" id="txt_chamado_agendamento_data" name="txt_chamado_agendamento_data" value="<?=(isset($rowAgenda)) ? format_date_out($rowAgenda['fldData']) : '';?>" />
					<a href="#" title="Exibir calend&aacute;rio" class="exibir-calendario-data-atual"></a>
				</li>
				
				<li>
					<label for="txt_chamado_agendamento_hora">Hora</label>
					<input type="text" style="width: 80px; text-align: center" class="hora-mask" id="txt_chamado_agendamento_hora" name="txt_chamado_agendamento_hora" value="<?=(isset($rowAgenda) and !empty($rowAgenda['fldHora'])) ? date('H:i', strtotime($rowAgenda['fldHora'])) : '';?>" />
				</li>
				
			</ul>
			<ul>
				<li>
					<label for="txt_chamado_agendamento_desc">Descri&ccedil;&atilde;o</label>
					<textarea style="width: 645px; height: 60px; padding: 5px;" id="txt_chamado_agendamento_desc" name="txt_chamado_agendamento_desc"><?=(isset($rowAgenda)) ? strip_tags($rowAgenda['fldDescricao']) : '';?></textarea>
				</li>
			</ul>
		</fieldset>
		
		<div style="float:right">
			<input type="submit" class="btn_enviar" name="btn_gravar" id="btn_gravar" value="Gravar" title="Gravar" />
		</div>
	</form>