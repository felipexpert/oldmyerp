<?Php
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');
	if(isset($_POST['form'])){
	
		$serialize 	= $_POST['form'];	
		parse_str($serialize, $form);
		
		$quantidade	= $form['txt_quantidade'];
		$coluna		= $form['txt_coluna'];
		$linha		= $form['txt_linha'];
		$filtro		= $_POST['filtro_id'];
		$epl 		= $_POST['epl'];
		
		if($epl == "0"){ //se nao for do tipo "zebra"
			$location  = "produto_imprimir_barcode.php?linha=".$linha."&coluna=".$coluna."&qtd=".$quantidade;
			$location .= ($filtro != '')? "&filtro=".$filtro : '';
		}else{
			$tipo_papel = $form['sel_tipo_papel'];
			$location  = "produto_imprimir_barcode_zebra.php?qtd=".$quantidade."&tipo=".$tipo_papel;
			$location .= ($filtro != '')? "&filtro=".$filtro : '';
		}
 ?>       
		<img src="image/layout/carregando.gif" alt="carregando..." />
        <script type="text/javascript">
			window.open("<?=$location?>");
			$('.modal-body:last').remove();
        </script> 
<?		die;        
	}

?>
<form id="frm_produto_barcode" action="" method="post" class="frm_detalhe">
	
	<ul style="width: 350px; position: relative">
    
        <li style="display:block"><input type="checkbox" style="width: 20px" name="chk_selecionado" id="chk_selecionado"/> <span>apenas selecionados</span></li>
        <li style="display:block"><input type="checkbox" style="width: 20px" name="chk_epl" id="chk_epl"/> <span>impressora tipo Zebra</span></li>
        
    	<li>
        	<label for="txt_quantidade">Repetir c&oacute;digo</label>
        	<input type="text" id="txt_quantidade" name="txt_quantidade" style="width: 100px;" value="1" />
		</li>
        <li class="tipo_papel" style="display:none">
        	<label for="sel_tipo_papel">Tipo de papel</label>
        	<select name="sel_tipo_papel" style="width:225px">
            	<option value="grande">grande</option>
                <option value="pequeno">pequeno</option>
                <option value="btpl42">BTPL42</option>
            </select>
		</li>
		<li class="linha_inicial">
        	<label for="txt_linha">Linha inicial</label>
        	<input type="text" id="txt_linha" name="txt_linha" style="width: 100px;" value="1" />
		</li>
		<li class="coluna_inicial">
        	<label for="txt_linha">Coluna inicial</label>
            <input type="text" id="txt_coluna" name="txt_coluna" style="width: 100px;" value="1" />
		</li>
        
        <div style="float:right; margin-top:-10px">
            <input type="hidden" name="hid_filtro" id="hid_filtro" value="" />
            <li><input type="submit" class="btn_enviar" name="btn_enviar_barcode" id="btn_enviar_barcode" value="OK" title="OK" /></li>
        </div>
	</ul>
	
</form>



<script type="text/javascript">

	$('#chk_epl').click(function(){
		if($(this).is(':checked')){
			$('#txt_linha').val('').attr('disabled', 'disabled')
			$('.linha_inicial').css("display", "none");
			$('#txt_coluna').val('').attr('disabled', 'disabled')
			$('.coluna_inicial').css("display", "none");
			$('.tipo_papel').css("display", "block");
		}else{
			$('#txt_linha').val('').removeAttr('disabled').css("display", "block");
			$('#txt_coluna').val('').removeAttr('disabled').css("display", "block");
			$('.linha_inicial').css("display", "block");
			$('.coluna_inicial').css("display", "block");
			$('.tipo_papel').css("display", "none");
		}
	});

    $('#btn_enviar_barcode').click(function(event){
		event.preventDefault();
		var filtro_id = '';
		if($('#chk_selecionado').attr('checked')){
			
			$("#frm_produto input:checkbox[name^='chk_produto_']:checked").each(function(){
				filtro_id = filtro_id+$(this).val()+',';
			});
			len  	  = filtro_id.length-1;
			filtro_id = (filtro_id.substr(0,len));
		}
		
		var epl = '0';
		
		if($('#chk_epl').attr('checked')){
			var epl = '1';
		}
	   
		var form = $('#frm_produto_barcode').serialize();
		$('div.modal-conteudo:last').load('modal/produto_barcode.php', {form : form, filtro_id:filtro_id, epl:epl});
       
    });	
        
</script>