<?php
	
	session_start();
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');
	require_once('../inc/fnc_status.php');
	
	require_once('nfe_importar_vendas_filtro.php');
	
	$natureza_operacao_id = $_POST['params'][2];
/**************************** ORDER BY *******************************************/
	
	$filtroOrder 	= 'tblpedido.fldId ';
	$class 			= 'desc';
	$order_sessao 	= explode(" ", $_SESSION['order_pedido_nfe_importar']);
	if(isset($_POST['order'])){
		switch($_POST['order']){
			case 'codigo'	:  $filtroOrder = "tblpedido.fldId";   			break;
			case 'data'		:  $filtroOrder = "tblpedido.fldPedidoData";	break;
		}
		if($order_sessao[0] == $filtroOrder){
			$class = ($order_sessao[1] == 'asc') ? 'desc' : 'asc';
		}
	}
	
	//definir icone para ordem
	$_SESSION['order_pedido_nfe_importar'] = $filtroOrder.' '.$class;
	
	$order_sessao = explode(" ", $_SESSION['order_pedido_nfe_importar']);
	$filtroOrder  = $order_sessao[0];
	
/**************************** PAGINAÇÃO *******************************************/
	$sSQL = "SELECT 
				tblpedido.fldPedidoData,
				tblpedido.fldId as fldPedidoId,
				tblpedido.fldStatus,
				tblpedido.fldValor_Terceiros,
				tblpedido.fldDesconto,
				tblpedido.fldDescontoReais,
				tblcliente.fldNome AS fldNomeCliente
			FROM tblpedido_parcela
				RIGHT JOIN tblpedido 	ON tblpedido.fldId 				= tblpedido_parcela.fldPedido_Id 
				INNER JOIN tblcliente 	ON tblpedido.fldCliente_Id 		= tblcliente.fldId
			WHERE tblpedido.fldExcluido = 0
				AND tblpedido.fldTipo_Id != 3
				AND (tblpedido.fldPedido_Destino_Nfe_Id IS NULL OR tblpedido.fldPedido_Destino_Nfe_Id <= 0)
				AND tblpedido.fldTipo_Id
				AND tblcliente.fldId = $idCliente ". $_SESSION['filtro_nfe_importar']." ORDER BY " . $_SESSION['order_pedido_nfe_importar'];
	
	$rsTotal = mysql_query($sSQL);
	echo mysql_error();
	$rowsTotal = mysql_num_rows($rsTotal);

	//definição dos limites
	$limite = 200;
	$n_paginas = 7;
	
	$total_paginas = ceil(mysql_num_rows($rsTotal) / $limite);
	
	if(isset($_GET["pagina"]) && $_GET["pagina"] > $total_paginas){
		$inicio = 0;
	}elseif(isset($_GET['pagina'])){
		$inicio = ($_GET['pagina'] - 1) * $limite;
	}else{
		$inicio = 0;
	}
	
	$sSQL .= " LIMIT " . $inicio . "," . $limite;
	$rsPedido = mysql_query($sSQL);
	
	$pagina = ($_GET['pagina'] ? $_GET['pagina'] : "1");
	
#####################################################################################
?>
    <form class="table_form" id="frm_importar" action="?p=pedido_nfe_importar#container" method="post">
    	<input type="hidden" name="hid_operacao" value="<?=$natureza_operacao_id?>" />
    	<div id="table" style="width: 560px;">
            <div id="table_cabecalho" style="width: 560px;">
                <ul class="table_cabecalho"style="width: 560px;">
                    <li class="order" style="width:85px">
                    	<a <?= ($filtroOrder == 'tblpedido.fldId') 			? "class='$class'" : '' ?> style="width:60px" href="codigo">C&oacute;d.</a>
                    </li>
                    <li style="width:200px">Cliente</a>
                    </li>
                    <li class="order" style="width: 100px;">
                    	<a <?= ($filtroOrder == 'tblpedido.fldPedidoData') 	? "class='$class'" : '' ?> style="width:55px" href="data">Emiss&atilde;o</a>
                    </li>
                    <li style="width:60px; text-align:center">Total</li>
                    <li style="width:80px; text-align:center;"><input type="checkbox" name="chk_todos" id="chk_todos" /></li>
                </ul>
            </div>
            <div id="table_container" style="width: 560px;">       
                <table id="table_general" class="table_general" summary="Lista de pedidos">
                	<tbody>
<?					
						$id_array = array();
						$n = 0;
						
						$linha = "row";
						$rows = mysql_num_rows($rsPedido);
						while($rowPedido = mysql_fetch_array($rsPedido)){
						
							$subTotalPedido = 0;
							$pedido_id 		= $rowPedido['fldPedidoId'];
							$id_array[$n] 	= $pedido_id;
							$n += 1;
							
							$sqlItem = "SELECT SUM((fldValor * fldQuantidade) - (((fldValor * fldQuantidade) * fldDesconto) / 100)) AS fldTotalItem, COUNT(fldQuantidade) as fldTotalQuantidade
												  FROM tblpedido_item WHERE fldPedido_Id = $pedido_id AND fldExcluido = '0'";
												  
							$sqlServico = "SELECT SUM(fldValor) as fldValorServico FROM	tblpedido_funcionario_servico WHERE fldFuncao_Tipo = 2 AND fldPedido_Id = $pedido_id ";
							
							$rowItem 			= mysql_fetch_array(mysql_query($sqlItem));
							$rowServico 		= mysql_fetch_array(mysql_query($sqlServico));
							
							$totalItem			= $rowItem['fldTotalItem'];
							$itemQtd			= $rowItem['fldTotalQuantidade'];	
							$subTotalPedido		= (($totalItem 		+ $rowPedido['fldComissao']) + $rowPedido['fldValor_Terceiros']) + $rowServico['fldValorServico'];
						
							$descPedido 		= $rowPedido['fldDesconto'];
							$descPedidoReais 	= $rowPedido['fldDescontoReais'];
							
							$descontoPedido 	= ($subTotalPedido 	* $descPedido) / 100;
							$totalPedido 		= $subTotalPedido 	- $descontoPedido;
							$totalPedido 		= $totalPedido 		- $descPedidoReais;
						
	?>						<tr class="<?= $linha; ?>">
								<td style="width:85px;"><span title="<?= fnc_status_pedido($rowPedido['fldPedidoId'], "tblpedido")?>" class="<?=fnc_status_pedido($rowPedido['fldPedidoId'], "tblpedido")?>"><?=str_pad($rowPedido['fldPedidoId'], 4, "0", STR_PAD_LEFT)?></span></td>
								<td	style="width:200px; text-align:left"><?=$rowPedido['fldNomeCliente']?></td>
								<td style="width:100px; text-align:center"><?=format_date_out($rowPedido['fldPedidoData'])?></td>
								<td style="width:60px; text-align:center;"><?=format_number_out($totalPedido)?></td>
<?								if($rowPedido['fldStatus'] > 3 && $rowPedido['fldStatus'] < 5) {
?>
								<td style="width:80px; text-align: center;"><input type="checkbox" name="chk_pedido_importar_<?=$rowPedido['fldPedidoId']?>" id="chk_pedido_importar_<?=$rowPedido['fldPedidoId']?>" title="selecionar o registro posicionado" /></td>
<?								} else {
?>
								<td style="font-size: 9px; color: #c00; width: 80px; text-align: center;">Não entregue!</td>
							</tr>
<?
							}
							$linha = ($linha == "row" )?"dif-row":"row";
						}
?>			 		</tbody>
				</table>
            </div>
       	  
            <input type="hidden" name="hid_array" id="hid_array" value="<?=urlencode(serialize($id_array))?>" />
			
			<input style="float:none; display:block; margin:0 auto; position: relative; top: 20px;" type="submit" class="btn_enviar" id="btn_importar_vendas" name="btn_importar_vendas" style="margin:0" title="Continuar com a importação" value="Continuar" />
			<!--
            <div id="table_paginacao">
<?				$paginacao_destino = "?p=pedido";
				include("../paginacao.php")
?>		
            </div>   
            <div class="table_registro">
                <span>Exibindo registros <?=($pagina*$limite-$limite+1).' a '.($pagina*$limite-$limite+$rows)?> do total de <?=$rowsTotal?></span>
            </div>     
        </div>
       -->
	</form>
	
	<script type="text/javascript">
		
		$(document).ready(function(){
			$('li.order').find('a').click(function(e){
				e.preventDefault();
				
				$('div.modal-conteudo:last').load('modal/nfe_importar_vendas.php', {'order' : $(this).attr('href'), 'idCliente' : $('#hid_idCliente').val()});
			});
			
			//checar se alguma venda foi selecionada
			$('#btn_importar_vendas').click(function(){
				var check = $('form#frm_importar tr input:checked').length;
				if(check < 1) {
					alert('Você não selecionou nenhuma venda para ser importada!');
					return false;
				}
			});
		});
		
	</script>