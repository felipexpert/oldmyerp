<?	require('../inc/con_db.php');
	require('../inc/fnc_general.php');
	
	$ids = $_POST['params'][1];
	$documento_tipo = $_POST['params'][2];
 	if(isset($_POST['rad_file'])){
		
		$zip = new ZipArchive();
		$ids = $_POST['ids'];

		$ids = explode(';', $ids);
		$ids = implode(',' , $ids);

		$zip_file = '../nfe_myerp/zips/myERP_NFE.zip';

		#APAGO O ZIP ANTIGO PRA NAO ADICIONAR OS NOVOS ARQUIVOS 
		if(is_file($zip_file)){ unlink($zip_file); }
		if( $zip->open($zip_file , ZipArchive::CREATE )  === true){

			$rsNFE = mysql_query('SELECT fldPedido_Id, fldDocumento_Tipo, fldambiente, fldchave, fldnfe_status_id FROM tblpedido_fiscal WHERE fldPedido_Id IN ('.$ids.')');
			while($rowNFE = mysql_fetch_array($rsNFE)){
				$ambiente	 = ($rowNFE['fldambiente'] == '1') ? 'producao' : 'homologacao';
				$rad_file 	 = $_POST['rad_file'];
				if($rad_file == 'txt'){
					$caminho	= 'temporarias/txt/';
					$file_ext	= '_nfe.';
					$file_name 	= $file_ext.'txt' ;
				}elseif($rad_file == 'xml'){
					switch($rowNFE['fldnfe_status_id']){
						case '4':
							$caminho = 'enviadas/aprovadas/';
							$file_ext	= '-nfe.';
							$file_name = $file_ext.'xml';
						break;
						case '6':
							$caminho = 'canceladas/';
							$file_ext	= '-1-procCanc.';
							$file_name = $file_ext.'xml';
						break;
					}
				}
				$caminho = '../nfe_myerp/'.$ambiente.'/'.$caminho;
				$chave_nfe = $rowNFE['fldchave']; 
				unset($arquivo_nome);
				if($rad_file == 'txt'){
					$nfe_id  		= $rowNFE['fldPedido_Id'];
					$documento_tipo = ($rowNFE['fldDocumento_Tipo'] == '2')? 'compra' : 'pedido';
					
					$export_txt = true;
					require('pedido_nfe_exportar.php');
					
					#ao exportar nfe, gera a chave pelo arquivo e busca novamente a variavel
					$file 	 = $chave_nfe.$file_name;
					$origem	 = $arquivo_nome;
					$destino = $caminho.$file;
					
					copy($origem, $destino);
					unlink($origem);
				}else{
					$file = $chave_nfe.$file_name ;
				}
				
				$file_download = $caminho.$file;
				if(is_file($file_download)){
					$zip->addFile($file_download, $chave_nfe.$file_ext.$rad_file );
				}
			}		 
			$zip->close();
		}
		if(is_file('../nfe_myerp/zips/myERP_NFE.zip')){
?>		
			<script type='text/javascript'>
                window.location ='nfe_myerp/zips/myERP_NFE.zip';
                $('.modal-body:last').remove();
            </script>;
<?		}else{
			echo 'Falha ao gerar arquivo!';
		}
	}else{
	
		
		##################################################################################################################################
		$documento_id = explode(';', $ids);
		$documento_id = implode(',' , $documento_id);
		unset($disabled);
		$rsStatus  = mysql_query('SELECT fldnfe_status_id FROM tblpedido_fiscal WHERE fldPedido_Id IN ('.$documento_id.') AND fldDocumento_Tipo = '.$documento_tipo);
		while($rowStatus = mysql_fetch_assoc($rsStatus)){
			 $disabled = ($rowStatus['fldnfe_status_id'] != '4' && $rowStatus['fldnfe_status_id'] != '6')? 'disabled="disabled"' : '';	
		}
				
?>
    
        <form id="frm_nfe_download" class="frm_detalhe" action="" method="post">
            <fieldset style="width:210px; text-align:center">
                <ul style="width:165px;margin:0 auto;float:none;margin-top:5px">
                    <li style="width:50px; margin-right:50px;text-align:center">
                        <div class="nfe_download_txt">
                            <img src="image/layout/icon_file_txt.gif" alt="arquivo txt" title="arquivo txt" />
                        </div>
                        <input style="width:15px" type="radio" name="rad_nfe_tipo" value="txt" checked="checked" />
                        <span>txt</span>
                    </li>
                    <li style="width:50px;text-align:center">
                        <div class="nfe_download_xml">
                            <img src="image/layout/icon_file_xml.gif" alt="arquivo xml" title="arquivo xml" />
                        </div>
                        <input style="width:15px" type="radio" name="rad_nfe_tipo" value="xml" <?= $disabled ?> />
                        <span>xml</span>
                    </li>
                </ul>
            </fieldset>
            <input type="submit" style=" width:100px; height:25px;margin-top:15px" name="btn_enviar" id="btn_enviar" value="download" />
        </form>
    
<?	}    ?>    
<script type="text/javascript">

	$('#btn_enviar').click(function(e){
		e.preventDefault();

		var rad_file 	= $('[name=rad_nfe_tipo]:checked').val();
		var ids 		= '<?= $ids;?>';

		$('div.modal-conteudo:last').load('modal/pedido_nfe_download.php', {rad_file : rad_file, ids:ids});
	});	
	
</script>