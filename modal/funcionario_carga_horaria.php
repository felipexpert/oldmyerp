<style type="text/css">
#table_carga_horaria_novo tr td {
	display:table;
	border:1px solid #999;
}
#table_carga_horaria_novo tr td.hora {
	width:70px;
	text-align:right;
	background: #666;
	color: #FFF;
}
#table_carga_horaria_novo tr td.cabecalho {
	width:50px;
	text-align:center;
	background:#6C85C6;
	border:1px solid #999;
	color: #FFF;
}
#table_carga_horaria_novo tr td input {
	width:46px;
	text-align:center;
	background:#FFC;
	border:0px;
}
</style>

<?php
	
	session_start();
	require_once('../inc/con_db.php');
	require_once('../inc/fnc_general.php');	

	if(isset($_POST['form'])){
		$serialize 	= $_POST['form'];	
		parse_str($serialize, $form);
		
		$carga_horaria_nome	= $form['txt_nome'];
		$carga_horaria_id	= $form['hid_carga_horaria_id'];
		$controle			= $form['hid_controle'];
		
		if($carga_horaria_id > 0){
			mysql_query('DELETE FROM tblfuncionario_carga_horaria_dia WHERE fldCarga_Horaria_Id ='.$carga_horaria_id);
			mysql_query('UPDATE tblfuncionario_carga_horaria SET fldCarga_Horaria = '.$carga_horaria_nome.' WHERE fldId ='.$carga_horaria_id);
		}else{
			if(mysql_query('INSERT INTO tblfuncionario_carga_horaria (fldCarga_Horaria) VALUES ("'.$carga_horaria_nome.'")')){
				echo mysql_error();
				$rsLastId 	= mysql_query("SELECT last_insert_id() as lastId");
				$LastId 	= mysql_fetch_array($rsLastId);
				$carga_horaria_id = $LastId['lastId'];
			}
		}
		echo mysql_error();
		for($n = 1; $n <= $controle; $n++){
			
			$hora_entrada1 	= $form['txt_entrada1_'.$n];
			$hora_saida1 	= $form['txt_saida1_'.$n];
			$hora_entrada2 	= $form['txt_entrada2_'.$n];
			$hora_saida2 	= $form['txt_saida2_'.$n];
			$dia_id 		= $n;
			mysql_query('INSERT INTO tblfuncionario_carga_horaria_dia (fldCarga_Horaria_Id, fldDia_Id, fldHora_Entrada1, fldHora_Saida1, fldHora_Entrada2, fldHora_Saida2) 
						VALUES ("'.$carga_horaria_id.'", "'.$dia_id.'", "'.$hora_entrada1.'", "'.$hora_saida1.'", "'.$hora_entrada2.'", "'.$hora_saida2.'") ');
		}	
		
?>
        <img src="image/layout/carregando.gif" alt="carregando..." />
        <script type="text/javascript">
			window.location="index.php?p=funcionario&modo=carga_horaria";
        </script> 
<?		die;
	}
	
	if(isset($_POST['params'][1])){
		$carga_horaria_id 	= $_POST['params'][1];
		$rsCarga_Horaria 	= mysql_query('SELECT * FROM tblfuncionario_carga_horaria WHERE fldId ='.$carga_horaria_id);
		$rowCarga_Horaria	= mysql_fetch_array($rsCarga_Horaria);
	}
		
?>
	<form style="margin-top:0" class="frm_detalhe" id="frm_funcionario_carga_horaria_novo" action="" method="post">
    	<input type="hidden" name="hid_carga_horaria_id" id="hid_carga_horaria_id" value="<?=$carga_horaria_id?>" />
  		<fieldset>
    		<legend>carga hor&aacute;ria</legend>
    		<ul>
                <li>
                	<input type="text" name="txt_nome" placeholder="Nome" value="<?=$rowCarga_Horaria['fldCarga_Horaria']?>" style="width:270px" />
                </li>
    		</ul>
    		<table style="width:430px;margin:3px" id="table_carga_horaria_novo" class="table_general" summary="dias da semana">
      			<tbody style="width:430px">
        			<tr style="width:70px;float:left">
                        <td class="hora">&nbsp;</td>
                        <td class="hora">Entrada 1</td>
                        <td class="hora">Sa&iacute;da 1</td>
                        <td class="hora">Entrada 2</td>
                        <td class="hora">Sa&iacute;da 2</td>
					</tr>
<?					$rsDias = mysql_query('SELECT * FROM tblsistema_calendario_semana_dia');
					$rowsDias = mysql_num_rows($rsDias);
					while($rowDias = mysql_fetch_array($rsDias)){
						if(isset($carga_horaria_id)){
							$rsHora  = mysql_query('SELECT * FROM tblfuncionario_carga_horaria_dia WHERE fldCarga_Horaria_Id = '.$carga_horaria_id.' AND fldDia_Id = '.$rowDias['fldId']);
							$rowHora = mysql_fetch_array($rsHora);
							$entrada1 	= format_time_short($rowHora['fldHora_Entrada1']);
							$saida1 	= format_time_short($rowHora['fldHora_Saida1']);
							$entrada2 	= format_time_short($rowHora['fldHora_Entrada2']);
							$saida2 	= format_time_short($rowHora['fldHora_Saida2']);
						}
?>
                        <tr style="float:left">
                            <td class="cabecalho"><?=mb_substr($rowDias['fldDia'],0,3,'utf-8')?></td>
                            <td style="width:50px"><input type="text" class="hora-mask" placeholder="__:__" name="txt_entrada1_<?=$rowDias['fldId']?>" 		value="<?=$entrada1?>" 	/></td>
                            <td style="width:50px"><input type="text" class="hora-mask" placeholder="__:__" name="txt_saida1_<?=$rowDias['fldId']?>" 		value="<?=$saida1?>" 	/></td>
                            <td style="width:50px"><input type="text" class="hora-mask" placeholder="__:__" name="txt_entrada2_<?=$rowDias['fldId']?>" 		value="<?=$entrada2?>" 	/></td>
                            <td style="width:50px"><input type="text" class="hora-mask" placeholder="__:__" name="txt_saida2_<?=$rowDias['fldId']?>" 		value="<?=$saida2?>" 	/></td>
                        </tr>
<?					}                    
?>
				</tbody>
    		</table>
    		<input type="hidden" value="<?=$rowsDias?>" name="hid_controle" id="hid_controle" />
    		<button style="width:100px;height:25px;cursor:pointer;float:right;margin:5px" name="btn_gravar" id="btn_gravar" >gravar</button>
  		</fieldset>
	</form>
	<script type="text/javascript">
		$('input[name="txt_nome"]').focus();
		$('input[name^="txt_entrada"], input[name^="txt_saida"]').change(function(){
			var hora 		= $(this).val();
			var dia_id 		= $(this).attr('name').split('_');
			var controle	= $('#hid_controle').val();
			
			inicio			= parseInt(dia_id[2]) +1;
			for(n = inicio; n <= controle; n++){
				$('input[name^="txt_'+dia_id[1]+'_'+n+'"]').val(hora);
			}
		});
		$('#frm_funcionario_carga_horaria_novo').submit(function(e){
			e.preventDefault();
			$('#btn_gravar').attr({'disabled' : 'disabled'});
			var form = $(this).serialize();
			$('div.modal-conteudo').load('modal/funcionario_carga_horaria.php', {form : form});
            
		});		
	</script> 
