<?php
	ob_start();
	session_start();
?>
	<script type="text/javascript">
		//evita que o enter faça alguma acao					   
		$("input#buscar").keydown(function(event){
			if (event.keyCode == 13){ return false;	}			   
		});						   
		
		$('document').ready(function(){
				$('#buscar').focus();
				$('#buscar').select();
				$('#loading').hide();
				$('#buscar').click(function(){
					$('#buscar').val('');
				});
				
				$('#buscar').keypress(busca_keyPress);
				function busca_Scroll(deslocamento){
					//alert(deslocamento);
					var cursorAtual 	= parseInt($('#hid_busca_cursor').val());
					var cursorLimite 	= parseInt($('#hid_busca_controle').text() - 1);
					var mover = false;
					if(deslocamento<0){
						if(cursorAtual > 0){mover = true;}
					}else{
						if(cursorAtual < cursorLimite){mover = true;}
					}
					if(mover){
						$('a[title=busca_lista_cliente]').removeClass('cursor');
						cursorNovo = cursorAtual + deslocamento;
						//corrigir cursor se ultrapassar limites
						if(cursorNovo < 0){cursorNovo = 0;}
						if(cursorNovo > cursorLimite){cursorNovo = cursorLimite;}
						
						$('a[title=busca_lista_cliente]:eq('+cursorNovo+')').addClass('cursor');
						$('#hid_busca_cursor').val(cursorNovo);
						var strNome = $('li[title=nome]:eq('+cursorNovo+')').text();
						$('#buscar').val(strNome);
						
						//19 - altura da linha de exibição de cada registro
						$('#alvo').scrollTop($('#alvo').scrollTop() + 19 * deslocamento);
					}
				}
				
				function busca_keyPress(event) {
					console.log(this);
					//alert(event.keyCode);
					switch(event.keyCode){
						case 13: //enter
							var intCursor = $('#hid_busca_cursor').val();
							placa_id 		= $('a[title=busca_lista_cliente]:eq('+intCursor+')').find('li#placa').attr('class');
							$('#parametro_cliente').load('modal/dependente_busca.php', {codigo_dependente: $('a[title=busca_lista_cliente]:eq('+intCursor+')').attr('href'), placa_id: placa_id});
							
							break;
						case 38: //up
							busca_Scroll(-1);
							break;
						case 40: //down
							busca_Scroll(1);
							break;
						case 33: //page up
							busca_Scroll(-10);
							break;
						case 34: //page down
							busca_Scroll(10);
							break;
						default: //busca
							//reset da posição do cursor para acompanhar o scroll
							$('#hid_busca_cursor').val(0);
							$.post('modal/dependente_busca_consulta.php',
							{busca: $('#buscar').val(), rad:$('input:radio[name=rad_busca]:checked').val()},
							function(data){
								if ($('#buscar').val()!=''){
									$('#alvo').show();
									$('#alvo').empty().html(data);
								}else{
									$('#alvo').empty();
								}
							});
							break;
					}
				}
			
			//pegar o id no href e retornar para essa mesma página via ajax o valor e gravar na sessão	
			$('a.selecionar').live('click',function(e){
				e.preventDefault();
				placa_id = $(this).find('li#placa').attr('class');
				$('#parametro_cliente').load('modal/dependente_busca.php', {codigo_dependente: $(this).attr('href'), placa_id: placa_id});
			});	
		});
		
		$('#btn_cliente_novo').live('click', function(){
			$('.frm_busca_dependente').parents('.modal-body').remove();
		});
	</script>
    	
	<div id="parametro_cliente">
<?		if(isset($_POST['codigo_dependente'])){
			$_SESSION['cliente_placa_id'] = $_POST['placa_id'];
?>
			<script type="text/javascript">	
				codigo_dependente = "<?= $_POST['codigo_dependente']?>";
				$('input#txt_dependente_codigo').val(codigo_dependente);
				$('input#txt_dependente_codigo').focus();
				$('input#txt_dependente_codigo').blur();
				$('.frm_busca_dependente').parents('.modal-body').remove();
			</script>
<?		}
?>	</div>
	
<? require("../inc/con_db.php"); ?>

    <form id="frm_busca" name="frm_busca" class="frm_busca_dependente" style="width:940px">
    <fieldset>
    	<legend>Busca de cliente</legend>
           	<ul>
            	<li style="float:left" >
                	<input style="float:left" type="text" id="buscar" name="buscar" value="Digite o nome do cliente" size="50" >
                </li>
                <li style="width: 20px; margin:0; float:left">
                    <input type="radio" style="width:20px" id="rad_busca" name="rad_busca" value="nome" checked="checked" />
                </li>
                <li style="width:80px;margin-top:10px; float:left">
                    <label style="font-size:12px" for="rad_busca">Nome</label>
                </li>
<?				if($_SESSION["sistema_tipo"] == "automotivo"){                
?>	            	<li style="width: 20px; margin:0; float:left">
                    	<input type="radio" style="width:20px" id="rad_busca" name="rad_busca" value="placa"/>
                    </li>
                    <li style="width:80px;margin-top:10px; float:left">
                        <label style="font-size:12px" for="rad_busca">Placa</label>
                    </li>
<?				}
?>              <li style="width: 20px; margin:0; float:left">
                    <input type="radio" style="width:20px" id="rad_busca" name="rad_busca" value="telefone" />
                </li>
                <li style="width:80px;margin-top:10px; float:left">
                    <label style="font-size:12px" for="rad_busca">Telefone</label>
                </li>
                <li style="width: 20px; margin:0; float:left">
                    <input type="radio" style="width:20px" id="rad_busca" name="rad_busca" value="cpf" />
                </li>
                <li style="width:60px;margin-top:10px; float:left">
                    <label style="font-size:12px" for="rad_busca">CPF</label>
                </li>
                <li style="width: 20px; margin:0; float:left">
                    <input type="radio" style="width:20px" id="rad_busca" name="rad_busca" value="endereco" />
                </li>
                <li style="width:100px;margin-top:10px; float:left">
                    <label style="font-size:12px" for="rad_busca">Endere&ccedil;o</label>
                </li>
                <li style="float:right; margin-right:10px">
                	<a id="btn_cliente_novo" class="modal btn_novo_small" href="cliente_cadastro_rapido,dependente_codigo" title="novo" rel="750-380" ></a>
                </li>
			</ul>            
            
            <ul id="busca_cabecalho" style="width:940px">
            	<li style="width:80px">C&oacute;digo</li>
                <li style="width:200px">Cliente</li>
                <li style="width:190px">Nome Fantasia</li>
                <li style="width:180px">Endere&ccedil;o</li>
                <li style="width:100px">Telefone 1</li>
                <li style="width:90px">CPF/CNPJ</li>
                <li style="width:90px"><?= ($_SESSION["sistema_tipo"] == "automotivo") ? 'Placa' : ''?></li>
            </ul>
			<input type="hidden" id="hid_busca_cursor" value="0" />
            <div id="alvo" style="width:940px"></div>
    	</fieldset>
    </form>