<? die(); ?>
<?
	$usuario_id = $_SESSION['usuario_id'];
	$pedido_id = $_GET['id'];
	$impressao = fnc_sistema('sistema_impressao');
	if(isset($impressao)){
		$rowImpressao = mysql_fetch_array(mysql_query("SELECT * FROM tblsistema_impressao WHERE fldId = $impressao"));
	
		$impressao = $rowImpressao['fldModelo'];
		if($rowImpressao['fldVariacao'] != ''){
			$impressao .= "_".$rowImpressao['fldVariacao'];
		}
	}
	
	$rsPedido = mysql_query("SELECT * FROM tblpedido WHERE fldId = $pedido_id");
	//caso nao exista a venda ou tenha sido excluida
	if(!$rsPedido){
?>	
		<div class="alert">
			<p class="erro">Houve um erro ao encontrar essa venda, ela pode ter sido excluida!<p>
        </div>
<?		die;
	}
	
	$rowPedido = mysql_fetch_array($rsPedido);
	//VERIFICA SE TEM PARCELAS PAGAS
	$rsParcelas = mysql_query("SELECT tblpedido_parcela.fldId, tblpedido_parcela_baixa.fldExcluido,
							  tblpedido_parcela_baixa.fldId FROM tblpedido_parcela INNER JOIN tblpedido_parcela_baixa
							  ON tblpedido_parcela_baixa.fldParcela_Id = tblpedido_parcela.fldId 
							  WHERE tblpedido_parcela_baixa.fldExcluido = 0 AND tblpedido_parcela.fldPedido_Id=$pedido_id");
	if(mysql_num_rows($rsParcelas)){
		$disabled = 1;
	}

	$rowCliente = mysql_fetch_array(mysql_query("SELECT * FROM tblcliente WHERE fldId = ".$rowPedido['fldCliente_Id']));
    $cliente_id = $rowPedido['fldCliente_Id'];
	$rowFuncionario = mysql_fetch_array(mysql_query("SELECT tblfuncionario.fldNome, tblpedido_funcionario_servico.fldComissao, fldFuncionario_Id 
							 FROM tblpedido_funcionario_servico LEFT JOIN tblfuncionario
							 ON tblfuncionario.fldId = tblpedido_funcionario_servico.fldFuncionario_Id
							 WHERE tblpedido_funcionario_servico.fldFuncao_Tipo = 1 
							 AND tblpedido_funcionario_servico.fldPedido_Id = $pedido_id"));
	echo mysql_error();
	$vendaDecimal 		= fnc_sistema('venda_casas_decimais');
	$quantidadeDecimal 	= fnc_sistema('quantidade_casas_decimais');
	if($disabled == 1){
?>			
		<div class="alert" style="margin-top:8px">
           	<p class="alert">J&aacute; existe um pagamento efetuado para esta venda. N&atilde;o ser&aacute; poss&iacute;vel salvar altera&ccedil;&otilde;es.</p>
		</div>		
<?	}

 	$rowUsuario = mysql_fetch_array(mysql_query("SELECT * FROM tblusuario WHERE fldId = ".$rowPedido['fldUsuario_Id']));
 
?>

<div class="form">
	<form class="frm_detalhe" style="width:960px; float:left; margin-left: 10px" name="frm_pedido_rapido_novo" id="frm_pedido_rapido_novo" action="" method="post" autocomplete="off">
		<input type="hidden" id="hid_disabled" name="hid_disabled" value="<?=$disabled?>" />
    	<input type="hidden" id="hid_comanda" name="hid_comanda" value="<?=$rowPedido['fldComanda_Numero']?>" />
        
    	<div class="pedido_rapido">
        	<ul>
                <li>
                    <label for="txt_funcionario_codigo">Funcion&aacute;rio</label>
                    <input type="text" class="enter" style="width:80px; text-align:right" id="txt_funcionario_codigo" name="txt_funcionario_codigo" value="<?=$rowFuncionario['fldFuncionario_Id']?>" />
                    <input type="hidden" id="hid_funcionario_comissao" name="hid_funcionario_comissao" value="<?=$rowFuncionario['fldComissao']?>" />
                    <a href="funcionario_busca,1" title="Localizar" class="modal" rel="680-380"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                </li>
                <li style="width:475px">
                    <label for="txt_funcionario_nome">&nbsp;</label>
                    <input type="text" class="enter" style=" width:603px;" id="txt_funcionario_nome" name="txt_funcionario_nome" value="<?=$rowFuncionario['fldNome']?>" readonly="readonly" />
                </li>
            </ul>
            
            <div id="pedido_produto" style="width:730px">
                <ul>
                    <li>
                        <label for="txt_produto_quantidade">Qtde [F6]</label>
                        <input type="text" style="width:50px; text-align:right" id="txt_produto_quantidade" name="txt_produto_quantidade" value="1" />
                        <input type="hidden" id="txt_produto_desconto" name="txt_produto_desconto" value="" />
                        <input type="hidden" name="hid_quantidade_decimal" id="hid_quantidade_decimal" value="<?=$quantidadeDecimal?>" />
                    </li>
                    <li>
                        <label for="txt_produto_codigo">C&oacute;digo [F7]</label>
                        <input type="text" class="codigo" style="width: 80px" id="txt_produto_codigo" name="txt_produto_codigo" value="" />
                        <a href="produto_busca" title="Localizar" class="modal" rel="950-450"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                       	<input type="hidden" id="hid_produto_id" 		name="hid_produto_id" 		value="" />
                        <input type="hidden" id="hid_estoque_limite" 	name="hid_estoque_limite" 	value="0" />
                        <input type="hidden" id="hid_estoque_controle" 	name="hid_estoque_controle" value="0" />
                        <input type="hidden" id="hid_estoque_negativo_alerta" 	name="hid_estoque_negativo_alerta" 	value="<?= fnc_sistema('alerta_estoque_negativo')?>" />
                        <input type="hidden" id="hid_item_repetido_alerta"	 	name="hid_item_repetido_alerta" 	value="<?=$_SESSION["sistema_pedido_item_repetido"]?>" />
                    </li>
                    <li>
                        <label for="txt_produto_nome">Produto</label>
                        <input type="text" style="width: 400px" id="txt_produto_nome" name="txt_produto_nome" value="" readonly="readonly" />
                        <input type="hidden" name="sel_produto_estoque" id="sel_produto_estoque" 	value="1" /><!-- #s� esta usando o nome pra poder buscar o produto, por causa do JS ser o mesmo da venda comum -->
                        <input type="hidden" name="hid_venda_decimal"	id="hid_venda_decimal" 		value="<?=$vendaDecimal?>" />
                    </li>
                    <li>
                        <label for="txt_produto_valor">Valor Un.</label>
                        <input type="text" style="width:70px; text-align:right" id="txt_produto_valor" name="txt_produto_valor" value="" />
                        <input type="hidden" name="hid_venda_decimal" id="hid_venda_decimal" value="<?=$vendaDecimal?>" />
                    </li>
                    <li>
                       	<button class="btn_sub_small" name="btn_item_inserir" id="btn_item_inserir" title="Inserir" >ok</button>
                    </li>
                </ul>
                <!---aqui uma lista para, conforme excluir algum item que ja estava na venda, ele calcular o estoque devolvido-->
                    <div id="hid_item" style="display:none">
                        <ul class="item_estoque">
                            <li><input type="hidden" class="hid_controle_item_produto_id"	id="hid_controle_item_produto_id"	name="hid_controle_item_produto_id"		value="" /></li>
                            <li><input type="hidden" class="hid_controle_item_estoque" 		id="hid_controle_item_estoque" 		name="hid_controle_item_estoque" 		value="" /></li>
                            <li><input type="hidden" class="hid_controle_item_estoque_id"	id="hid_controle_item_estoque_id"	name="hid_controle_item_estoque_id" 	value="" /></li>
                        </ul>
                        <input type="hidden" id="hid_calculo_estoque_controle" name="hid_calculo_estoque_controle" value="0" />
                    </div>
    
                <div id="table" style="width:728px">
                    <div id="table_cabecalho" style="width:728px">
                        <ul class="table_cabecalho" style="width:728px">
                            <li style="width:3px">&nbsp;</li>
                            <li style="width:80px;">C&oacute;digo</li>
                            <li style="width:380px;">Produto</li>
                            <li style="width:80px; text-align:right">Valor Un.</li>
                            <li style="width:55px; text-align:center">Qtde</li>
                            <li style="width:85px; text-align:right">SubTotal</li>
                        </ul>
                    </div>
                    
                    <div id="table_container" style="width:728px; height:295px"> 
                        <table id="table_pedido_rapido" class="table_general" summary="Lista de produtos">
                            <tbody>
                                    <tr id="pedido_lista_item" title="pedido_lista_item" style="display:none">
                                        <td style="width: 80px; padding-left:8px">
                                            <input class="txt_item_codigo" type="text" style="width: 75px" id="txt_item_codigo_0" name="txt_item_codigo" readonly="readonly" value=""/>
                                            <input class="hid_item_produto_id" type="hidden" id="hid_item_produto_id_0" name="hid_item_produto_id_0" value="" />
                                        </td>
                                        <td style="width: 380px">
                                            <input class="txt_item_nome" type="text" style=" width:375px; text-align:left" id="txt_item_nome_0" readonly="readonly" name="txt_item_nome" value="" />
                                			<input class="hid_item_estoque_id" type="hidden" id="hid_item_estoque_id_0" name="hid_item_estoque_id" value="1" />
                                        </td>
                                        <td style="width: 80px">
                                            <input class="txt_item_valor" type="text" style="width:75px; text-align:right" id="txt_item_valor_0" readonly="readonly" name="txt_item_valor" value="" />
                                        </td>
                                        <td style="width: 55px">
                                            <input class="txt_item_quantidade" type="text" style="width:50px; text-align: right" id="txt_item_quantidade_0" readonly="readonly" name="txt_item_quantidade" value=""  />
                                        </td>
                                        <td style="width: 85px">
                                            <input class="txt_item_subtotal" type="text" style="width:80px; text-align: right" id="txt_item_subtotal_0" readonly="readonly" name="txt_item_subtotal" value=""  />
                                        </td>
                                    </tr>
                                    
<?
									$rsItem = mysql_query("SELECT * FROM tblpedido_item WHERE fldPedido_Id = $pedido_id");
									$rows = mysql_num_rows($rsItem);
									$i = 1;
									while($rowItem = mysql_fetch_array($rsItem)){
											
										$rsProduto = mysql_query("SELECT tblproduto.*, 
														 tblproduto_unidade_medida.fldSigla
														 FROM tblproduto LEFT JOIN tblproduto_unidade_medida ON tblproduto.fldUN_Medida_Id = tblproduto_unidade_medida.fldId
														 WHERE tblproduto.fldId = " . $rowItem['fldProduto_Id']);
										$rowProduto = mysql_fetch_array($rsProduto);
										
										$rowEstoque = mysql_fetch_array(mysql_query("SELECT tblproduto_estoque.fldId, tblproduto_estoque.fldNome
																	FROM tblproduto_estoque INNER JOIN tblpedido_item ON tblproduto_estoque.fldId = tblpedido_item.fldEstoque_Id
																	WHERE tblproduto_estoque.fldId = ".$rowItem['fldEstoque_Id']."
										"));
										
										
										$item_id 	= $rowItem['fldId'];
										$valor_item = $rowItem['fldValor'];
										$qtd 		= $rowItem['fldQuantidade'];
										$total_item = $valor_item * $qtd;
										$desconto 	= $rowItem['fldDesconto'];
										
										$totalDesconto 	= ($total_item * $desconto)/100;
										$totalValor 	= $total_item - $totalDesconto;
						
?>
                                		<!-- ITENS JA EXISTENTES DA VENDA -->
                                    
                                        <tr id="pedido_lista_item" title="pedido_lista_item">
                                            <td style="width: 80px; padding-left:8px">
                                                <input class="txt_item_codigo" type="text" style="width: 75px" id="txt_item_codigo_<?=$i?>" name="txt_item_codigo_<?=$i?>" readonly="readonly" value="<?=$rowProduto['fldCodigo']?>"/>
                                                <input class="hid_item_produto_id" 	type="hidden" id="hid_item_produto_id_<?=$i?>" 	name="hid_item_produto_id_<?=$i?>" 	value="<?=$rowProduto['fldId']?>" />
                                                <input class="hid_item_id" 			type="hidden" id="hid_item_id_<?=$i?>" 			name="hid_item_id_<?=$i?>" 			value="<?=$item_id?>" />
                                                <input class="hid_item_detalhe"		type="hidden" id="hid_item_detalhe_<?=$i?>" 	name="hid_item_detalhe_<?=$i?>" 	value="1" /><!-- parametro pra identificar item que ja estava na venda -->
                                            </td>
                                            <td style="width: 380px">
                                                <input class="txt_item_nome" 		type="text" style=" width:375px; text-align:left" id="txt_item_nome_<?=$i?>" name="txt_item_nome_<?=$i?>" readonly="readonly" value="<?=$rowItem['fldDescricao']?>" />
                                                <input class="hid_item_estoque_id" 	type="hidden" id="hid_item_estoque_id_<?=$i?>" name="hid_item_estoque_id_<?=$i?>" value="<?=$rowItem['fldEstoque_Id']?>" />
                                            </td>
                                            <td style="width: 80px">
                                                <input class="txt_item_valor" type="text" style="width:75px; text-align:right" id="txt_item_valor_0_<?=$i?>" name="txt_item_valor_<?=$i?>" readonly="readonly" value="<?=format_number_out($rowItem['fldValor'],$vendaDecimal)?>" />
                                            </td>
                                            <td style="width: 55px">
                                                <input class="txt_item_quantidade" type="text" style="width:50px; text-align: right" id="txt_item_quantidade_<?=$i?>" name="txt_item_quantidade_<?=$i?>" readonly="readonly" value="<?=format_number_out($rowItem['fldQuantidade'],$quantidadeDecimal)?>"  />
                                            </td>
                                            <td style="width: 85px">
                                                <input class="txt_item_subtotal" type="text" style="width:80px; text-align: right" id="txt_item_subtotal_<?=$i?>" name="txt_item_subtotal_<?=$i?>" readonly="readonly" value="<?=format_number_out($totalValor,$vendaDecimal)?>"  />
                                            </td>
                                        </tr>
                                  		<!---->  
<?								 		$subTotalPedido += $totalValor;
										$i += 1;
									}
									//total pedido
									$descPedido 			= $rowPedido['fldDesconto'];
									$descPedidoReais 		= $rowPedido['fldDescontoReais'];
									
									$totalPedidoComissao	= ($subTotalPedido + $rowPedido['fldComissao']);
									$descontoPedido 		= ($totalPedidoComissao * $descPedido) / 100;
									$totalPedido 			= $totalPedidoComissao - $descontoPedido;
									$totalPedido 			= $totalPedido - $descPedidoReais;
?>	
							</tbody>
                        </table>
					</div>  
                </div>
                <div>
                	<input type="hidden" name="hid_controle" 	id="hid_controle" value="<?=$rows?>" />
					<input type="hidden" name="hid_cursor" 		id="hid_cursor" value="0" />
                </div>
            </div>
		</div>
   
        <div class="pedido_rapido_pagamento">
            <ul>
            	<li class="dados">
                    <label for="txt_codigo">Id</label>
                    <input type="text" style="width:90px;height:15px" id="txt_codigo" name="txt_codigo" readonly="readonly" value="<?=$rowPedido['fldId']?>"/>
                    <input type="hidden" id="txt_check_enviado" name="txt_check_enviado" value="checar" style="width:50px" readonly="readonly" />
                </li>
                
                <li class="dados">
                    <label for="txt_usuario">Usu&aacute;rio</label>
                    <input type="text" style="width:95px;height:15px" id="txt_usuario" name="txt_usuario" value="<?=$rowUsuario['fldUsuario']?>" readonly="readonly"/>
                </li>
                <li class="dados">
                    <label for="txt_pedido_data" style="text-align:left">Data de Venda</label>
                    <input type="text" style="width: 90px;height:15px;text-align: center;" class="calendario-mask" id="txt_pedido_data" name="txt_pedido_data" value="<?=format_date_out($rowPedido['fldPedidoData'])?>" />
                    <a href="#" title="Exibir calend&aacute;rio" class="exibir-calendario-data-atual"></a>
                </li>
                <li class="dados">
                    <label for="txt_pedido_hora">Hora</label>
                    <input type="text" style="width: 72px;height:15px" id="txt_pedido_hora" name="txt_pedido_hora" value="<?=$rowPedido['fldCadastroHora']?>" readonly="readonly" />
                </li>
                <li style="height: 60px;float:left">
<?				if(($_GET['comanda'] && $rowPedido['fldComanda_Fechamento'] == 0) || ($rowPedido['fldComanda_Numero'] != 0 && $rowPedido['fldComanda_Fechamento'] == 0)){                
					echo '<a class="btn_comanda_gravar" name="btn_comanda_gravar" id="btn_comanda_gravar" href="" >voltar</a>';
				}	
?>				</li>

                <li style="height:60px;float:right">
<?				if(($_GET['comanda'] && $rowPedido['fldComanda_Fechamento'] == 0) || ($rowPedido['fldComanda_Numero'] != 0 && $rowPedido['fldComanda_Fechamento'] == 0)){                
					echo '<input type="text" class="total" style="width:98px; height:50px; text-align:center; color:#6FF;font-size:30px" id="txt_comanda" name="txt_comanda" value="'.$rowPedido['fldComanda_Numero'].'" readonly="readonly" />';
				}	
?>				</li>		               
                <li>
                    <label for="txt_pedido_subtotal">Produtos</label>
                    <input type="text" class="valor" style="width:208px" id="txt_pedido_subtotal" name="txt_pedido_subtotal" value="<?=format_number_out($subTotalPedido)?>" readonly="readonly" />
                </li>
<?				if((fnc_sistema('pedido_comissao') == 1 && $_GET['comanda']) || (fnc_sistema('pedido_comissao') == 1 && $rowPedido['fldComanda_Numero'] != 0)){                
?>                
                    <li>
                        <label for="txt_pedido_comissao">Comiss&atilde;o</label>
                        <input type="text" class="comissao" style="width:208px" id="txt_pedido_comissao" name="txt_pedido_comissao" value="<?=format_number_out($rowPedido['fldComissao'])?>" readonly="readonly" />
                    </li>
<?				}

?>
                <li style="float: left">
                    <label for="txt_pedido_desconto_reais">[F8] Desc (R$)</label>
                    <input type="text" class="desconto" style="width: 130px; float:right" id="txt_pedido_desconto_reais" name="txt_pedido_desconto_reais" value="<?=format_number_out($rowPedido['fldDescontoReais'])?>" />
                </li>
                <li>
                    <label for="txt_pedido_desconto">(%)</label>
                    <input type="text" class="desconto" style="width: 60px; float:right" id="txt_pedido_desconto" name="txt_pedido_desconto" value="<?=format_number_out($rowPedido['fldDesconto'])?>" />
                </li>
                <li>
                    <label for="txt_pedido_total" class="total">Total</label>
                    <input type="text" class="total" style="width:208px" id="txt_pedido_total" name="txt_pedido_total" value="<?=format_number_out($totalPedido)?>" readonly="readonly" />
                </li>
                <li>
                    <a class="btn_cancel" style="width:103px" name="btn_finalizar" id="btn_finalizar" href="pedido_pagamento,<?=$pedido_id?><?=(isset($cliente_id)) ? ','.$cliente_id  : ''?>" rel="<?= (fnc_sistema("pedido_retirado_por") == "1") ? '480-390' : '480-340'?>">[F10] Finalizar</a>
                    <a class="btn_cancel" style="width:103px" name="btn_imprimir" id="btn_imprimir" href="<?=$impressao?>" >[F4] Imprimir</a>
                    <a class="btn_cancel" style="width:210px" name="btn_finalizar_prazo" id="btn_finalizar_prazo" href="pedido_pagamento_parcela,<?=$pedido_id?><?=(isset($cliente_id)) ? ','.$cliente_id  : ''?>" rel="650-460">[F11] Finalizar e parcelas</a>
                    <a class="btn_cancel" style="width:210px" name="btn_cancelar" id="btn_cancelar" href="<?=$pedido_id?>"  onclick="return confirm('Deseja cancelar esta venda?')">[F12] Cancelar</a>
		    <a class="btn_cancel modal" rel="596-210" style="width:210px"  id="btn_transferir" href="pedido_rapido_transferir_comanda,<?=$pedido_id?>"  >  Transferir Comanda</a>
		   				
					
					
				</li>
		</li>
            </ul>
        </div>
	</form>
</div>



<script type="text/javascript">
	$("#txt_pedido_total").keydown( function(event) {
		switch(event.keyCode){
			case 13:
				event.preventDefault();
				$("#btn_finalizar").focus();
			break;
		}
	});
	
</script>
