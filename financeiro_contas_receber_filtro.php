<?php
	$filtro = '';
	
	//inicializando com o dia final e inicial do mês
	$_SESSION['txt_contas_receber_data_inicial'] 	= (isset($_SESSION['txt_contas_receber_data_inicial'])) ? $_SESSION['txt_contas_receber_data_inicial'] 	: '';
	$_SESSION['txt_contas_receber_data_final'] 		= (isset($_SESSION['txt_contas_receber_data_final'])) 	? $_SESSION['txt_contas_receber_data_final'] 	: format_date_out(date('Y') . '-' . date('m') . '-' . date("d", mktime(0, 0, 0, date('m') + 1, 1, date('Y')) - 1));
	
	$_SESSION['txt_contas_receber_data_inicial'] 	= (isset($_POST['txt_calendario_data_inicial'])) 	? $_POST['txt_calendario_data_inicial'] : $_SESSION['txt_contas_receber_data_inicial'];
	$_SESSION['txt_contas_receber_data_final'] 		= (isset($_POST['txt_calendario_data_final'])) 		? $_POST['txt_calendario_data_final'] 	: $_SESSION['txt_contas_receber_data_final'];
	
	//memorizar os filtros para exibição nos campos
	if(isset($_POST['btn_limpar'])){
		$_SESSION['txt_contas_receber_cliente_nome'] 	= "";
		$_SESSION['txt_contas_receber_municipio_codigo']= "";
		$_SESSION['sel_contas_receber_funcionario'] 	= "";
		$_SESSION['txt_contas_receber_pedido'] 			= "";
		$_SESSION['sel_contas_receber_tipo_pagamento'] 	= "";
		$_SESSION['sel_carteira']					 	= "";
		$_SESSION['txt_contas_receber_data_inicial'] 	= "";
		$_SESSION['txt_contas_receber_data_final'] 		= format_date_out(date('Y') . '-' . date('m') . '-' . date("d", mktime(0, 0, 0, date('m') + 1, 1, date('Y')) - 1));
		$_POST['chk_contas_receber_cliente'] 		 	= false;
		$_SESSION['sel_contas_receber_pedido']			= "";
	}
	else{
		$_SESSION['txt_contas_receber_cliente_nome']  	= (isset($_POST['txt_contas_receber_cliente_nome'])	 	? $_POST['txt_contas_receber_cliente_nome'] 	: $_SESSION['txt_contas_receber_cliente_nome']);
		$_SESSION['txt_contas_receber_municipio_codigo']= (isset($_POST['txt_municipio_codigo']) 				? $_POST['txt_municipio_codigo'] 				: $_SESSION['txt_contas_receber_municipio_codigo']);
		$_SESSION['sel_contas_receber_funcionario']  	= (isset($_POST['sel_contas_receber_funcionario']) 		? $_POST['sel_contas_receber_funcionario'] 		: $_SESSION['sel_contas_receber_funcionario']);
		$_SESSION['txt_contas_receber_pedido']  		= (isset($_POST['txt_contas_receber_pedido']) 			? $_POST['txt_contas_receber_pedido'] 			: $_SESSION['txt_contas_receber_pedido']);
		$_SESSION['sel_contas_receber_tipo_pagamento']  = (isset($_POST['sel_contas_receber_tipo_pagamento']) 	? $_POST['sel_contas_receber_tipo_pagamento'] 	: $_SESSION['sel_contas_receber_tipo_pagamento']);
		$_SESSION['sel_carteira'] 						= (isset($_POST['sel_carteira']) 						? $_POST['sel_carteira'] 						: $_SESSION['sel_carteira']);
		$_SESSION['txt_contas_receber_data_inicial']  	= (isset($_POST['txt_contas_receber_data_inicial']) 	? $_POST['txt_contas_receber_data_inicial']	 	: $_SESSION['txt_contas_receber_data_inicial']);
		$_SESSION['txt_contas_receber_data_final']  	= (isset($_POST['txt_contas_receber_data_final']) 		? $_POST['txt_contas_receber_data_final'] 		: $_SESSION['txt_contas_receber_data_final']);
		$_SESSION['sel_contas_receber_pedido']			= (isset($_POST['sel_contas_receber_pedido'])			? $_POST['sel_contas_receber_pedido']			: $_SESSION['sel_contas_receber_pedido']);
	}
?>

<form id="frm-filtro" action="?p=financeiro_contas_receber" method="post">
	<fieldset>
  	<legend>Buscar por:</legend>
    <ul>
		<li class="large_height">
			<label for="txt_contas_receber_data_inicial">Per&iacute;odo: </label>
	      	<input title="Data inicial" style="width: 70px; text-align: center;" class="calendario-mask" type="text" name="txt_contas_receber_data_inicial" id="txt_contas_receber_data_inicial" value="<?=$_SESSION['txt_contas_receber_data_inicial']?>"/>
		</li>
		<li class="large_height">
	      	<input title="Data final" style="width: 70px; text-align: center;" class="calendario-mask" type="text" name="txt_contas_receber_data_final" id="txt_contas_receber_data_final" value="<?=$_SESSION['txt_contas_receber_data_final']?>"/>
			<a href="calendario_periodo,<?=format_date_in($_SESSION['txt_contas_receber_data_inicial']) . ',' . format_date_in($_SESSION['txt_contas_receber_data_final'])?>,financeiro_contas_receber" id="exibir-calendario" title="Exibir calend&aacute;rio" class="modal calendario-modal" rel="600-320"></a>
		</li>
    	<li class="large_height">
			<input type="checkbox" name="chk_contas_receber_cliente" id="chk_contas_receber_cliente" <?=($_POST['chk_contas_receber_cliente'] == true ? print 'checked ="checked"' : '')?>/>
<?			$conta = ($_SESSION['txt_contas_receber_cliente_nome']) ? $_SESSION['txt_contas_receber_cliente_nome'] : 'nome do cliente / nome fantasia';
			($_SESSION['txt_contas_receber_cliente_nome'] == "nome do cliente / nome fantasia") ? $_SESSION['txt_contas_receber_cliente_nome'] = '' : '';
?>     		<input style="width:220px" type="text" name="txt_contas_receber_cliente_nome" id="txt_contas_receber_cliente_nome" onfocus="limpar(this,'nome do cliente / nome fantasia');" onblur="mostrar(this, 'nome do cliente / nome fantasia');" value="<?=$conta?>"/>
			<small>marque para qualquer parte do campo</small>
		</li>
        <li class="large_height">
<?			$municipio = ($_SESSION['txt_contas_receber_municipio_codigo'] ? $_SESSION['txt_contas_receber_municipio_codigo'] : "c&oacute;d. munic&iacute;pio");
			($_SESSION['txt_contas_receber_municipio_codigo'] == "cód. município") ? $_SESSION['txt_contas_receber_municipio_codigo'] = '' : '';
?>     		<input style="width:100px" type="text" name="txt_municipio_codigo" id="txt_municipio_codigo" onfocus="limpar (this,'c&oacute;d. munic&iacute;pio');" onblur="mostrar (this,'c&oacute;d. munic&iacute;pio');" value="<?=$municipio?>"/>
			<a href="municipio_busca" title="Localizar" class="modal" rel="680-380"><img src="image/layout/search.gif" alt="localizar" /></a>
		</li>
		<li class="large_height">
			<select style="width: 200px;" id="sel_contas_receber_funcionario" name="sel_contas_receber_funcionario" >
                <option value="">Funcion&aacute;rio</option>
<?				$rsFuncionario = mysql_query("SELECT * FROM tblfuncionario ORDER BY fldNome");
				while($rowFuncionario= mysql_fetch_array($rsFuncionario)){
?>					<option <?=($_SESSION['sel_contas_receber_funcionario'] == $rowFuncionario['fldId']) ? 'selected="selected"' : '' ?> value="<?=$rowFuncionario['fldId']?>"><?=$rowFuncionario['fldNome']?></option>
<?				}
?>			</select>
		</li>
		<li>
			<select style="width: 135px;" id="sel_contas_receber_tipo_pagamento" name="sel_contas_receber_tipo_pagamento" >
                <option value="">Tipo de pagamento</option>
<?				$rsTipoPagamento = mysql_query("SELECT * FROM tblpagamento_tipo ORDER BY fldTipo ASC");
				while($rowTipoPagamento= mysql_fetch_array($rsTipoPagamento)){
?>					<option <?=($_SESSION['sel_contas_receber_tipo_pagamento'] == $rowTipoPagamento['fldId']) ? 'selected="selected"' : '' ?> value="<?=$rowTipoPagamento['fldId']?>"><?=$rowTipoPagamento['fldTipo']?></option>
<?				}
?>			</select>
		</li>
		<li style="float:left">
			<select style="width: 125px;" id="sel_carteira" name="sel_carteira" >
                <option value="">Carteira</option>
				<option value="0" <?=($_SESSION['sel_carteira'] == "0") ? 'selected="selected"' : '' ?>>Padrão</option>
<?				$rsCarteiras = mysql_query("SELECT * FROM tblpedido_parcela_carteira ORDER BY fldDesc ASC");
				while($rowCarteiras = mysql_fetch_array($rsCarteiras)){
?>					<option <?=($_SESSION['sel_carteira'] == $rowCarteiras['fldId']) ? 'selected="selected"' : '' ?> value="<?=$rowCarteiras['fldId']?>"><?=$rowCarteiras['fldDesc']?></option>
<?				}
?>			</select>
		</li>
		<li class="large_height">
<?			$pedido 	= ($_SESSION['txt_contas_receber_pedido']) ? $_SESSION['txt_contas_receber_pedido'] : "";
			$sel_pedido = ($_SESSION['sel_contas_receber_pedido']) ? $_SESSION['sel_contas_receber_pedido'] : "venda";
			($_SESSION['txt_contas_receber_pedido'] == "") ? $_SESSION['txt_contas_receber_pedido'] = '' : '';
?>      	<select id="sel_contas_receber_pedido" name="sel_contas_receber_pedido" style="width:100px">
				<option <?=($sel_pedido == 'venda') ? "selected='selected'" : "";?> value="venda">C&oacute;d. Venda</option>
				<option <?=($sel_pedido == 'nfe') ? "selected='selected'" : "";?> value="nfe">C&oacute;d. NFE</option>
			</select>
			<input style="width:80px" type="text" name="txt_contas_receber_pedido" id="txt_contas_receber_pedido" value="<?=$pedido?>"/>
		</li>		
    <button type="submit" name="btn_exibir" title="Exibir">Exibir</button>
    <button type="submit" name="btn_limpar" title="Limpar filtro">Limpar filtro</button>
		
    </ul>
  </fieldset>
</form>

<?

	if(format_date_in($_SESSION['txt_contas_receber_data_inicial']) != "" || format_date_in($_SESSION['txt_contas_receber_data_final']) != ""){
				if($_SESSION['txt_contas_receber_data_inicial'] != "" && $_SESSION['txt_contas_receber_data_final'] != ""){
					$filtro .= " AND tblpedido_parcela.fldVencimento between '".format_date_in($_SESSION['txt_contas_receber_data_inicial'])."' AND '".format_date_in($_SESSION['txt_contas_receber_data_final'])."'";
				}elseif($_SESSION['txt_contas_receber_data_inicial'] != "" && $_SESSION['txt_contas_receber_data_final'] == ""){
					$filtro .= " AND tblpedido_parcela.fldVencimento >= '".format_date_in($_SESSION['txt_contas_receber_data_inicial'])."'";
				}elseif($_SESSION['txt_contas_receber_data_final'] != "" && $_SESSION['txt_contas_receber_data_inicial'] == ""){
					$filtro .= " AND tblpedido_parcela.fldVencimento <= '".format_date_in($_SESSION['txt_contas_receber_data_final'])."'";
				}
	}

	if(($_SESSION['txt_contas_receber_cliente_nome']) != ""){
		$nome = addslashes($_SESSION['txt_contas_receber_cliente_nome']); // no caso de aspas, pra nao dar erro na consulta
		if($_POST['chk_contas_receber_cliente'] == true) {
			//fldNomeFantasia
			$filtro .= "AND (tblcliente.fldNome like '%".$nome."%' or tblcliente.fldNomeFantasia like '%".$nome."%')";
		}
		else {
			$filtro .= "AND (tblcliente.fldNome like '".$nome."%' or tblcliente.fldNomeFantasia like '".$nome."%')";
		}
	}
	
	if(($_SESSION['txt_contas_receber_municipio_codigo']) != ""){
		
		$filtro .= "AND tblcliente.fldMunicipio_Codigo = '".$_SESSION['txt_contas_receber_municipio_codigo']."'";
	}
	
	if(($_SESSION['txt_contas_receber_pedido']) != ""){
		if($_SESSION['sel_contas_receber_pedido'] == "nfe"){
			$filtro .= "AND tblpedido_fiscal.fldnumero = '".$_SESSION['txt_contas_receber_pedido']."'";
		} else {
			$filtro .= "AND tblpedido_parcela.fldPedido_Id = '".$_SESSION['txt_contas_receber_pedido']."'";
		}
	}
	
	if(($_SESSION['sel_contas_receber_funcionario']) != ""){
		
		$filtro .= "AND tblpedido_funcionario_servico.fldFuncionario_Id = '".$_SESSION['sel_contas_receber_funcionario']."'";
	}
	
	if(($_SESSION['sel_contas_receber_tipo_pagamento']) != ""){
		
		$filtro .= "AND tblpedido_parcela.fldPagamento_Id = '".$_SESSION['sel_contas_receber_tipo_pagamento']."'";
	}
	
	if(($_SESSION['sel_carteira']) != ""){
		
		$filtro .= "AND tblpedido_parcela.fldCarteira_Id = '".$_SESSION['sel_carteira']."'";
	}
		
	//transferir para a sessão
	$_SESSION['filtro_contas_receber'] = $filtro;
?>