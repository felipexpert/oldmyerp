<? 
		#ABAIXO CRIO O CABECALHO #########################################################################################################################################################
		$tabelaCabecalho =' 
			<tr style="border-bottom: 2px solid">
                <td style="width: 600px"><h1>Relat&oacute;rio de Produtos</h1></td>';
		$tabelaCabecalho2 =' 
            </tr>
            <tr style="margin-bottom:0">
                <td>
                    <table style="width: 580px;margin-bottom:0" class="table_relatorio_dados" summary="Relat&oacute;rio">
                        <tr>
                            <td style="width: 320px;">Raz&atilde;o Social: '.$rowDados['fldNome'].'</td>
                            <td style="width: 200px;">Nome Fantasia: '.$rowDados['fldNome_Fantasia'].'</td>
                            <td style="width: 320px;">CPF/CNPJ: '.$CPF_CNPJDados.'</td>
                            <td style="width: 200px;">Telefone: '.$rowDados['fldTelefone1'].'</td>
                        </tr>
                    </table>	
                </td>
                <td>        
                    <table class="dados_impressao">
                        <tr>
                            <td><b>Data: 			</b><span>'.format_date_out(date("Y-m-d")).'</span></td>
                            <td><b>Hora: 			</b><span>'.format_time_short(date("H:i:s")).'</span></td>
                            <td><b>Usu&aacute;rio: 	</b><span>'.$rowUsuario['fldUsuario'].'</span></td>
                        </tr>
                    </table>
                </td>
            </tr>
			<tr>
				<td style="margin:0"><h2 style="background:#E1E1E1;width:790px;padding-left:10px">'.$rowProduto['fldNome'].'</h2></td>
			</tr>
            <tr>
                <td style="margin:0">
                    <table name="table_relatorio" class="table_relatorio" summary="Relat&oacute;rio">
						<tr style="border:none">
							<td class="valor">C&oacute;d</td>
							<td style="width:235px">Produto</td>
							<td class="qtd">Qtd</td>
							<td class="valor">Vl Unit.</td>
							<td class="valor">Total</td>
							<td class="valor">Vl Compra</td>
							<td class="valor">Total</td>
							<td class="valor">Lucro</td>
							<td class="valor">Lucro (%)</td>
						</tr>
					</table>
				</td>
			</tr>';
			$tabelaCabecalho3 ='
			<tr>
				<td>
					<table class="table_relatorio" summary="Relat&oacute;rio">';
	######################################################################################################################################################################################
		//ja esta fazendo a consulta na pagina anterior
		if(isset($data_inicio) && isset($data_final)){
			$filtro = '  AND tblpedido.fldExcluido = 0 AND (tblpedido.fldPedidoData BETWEEN "'.$data_inicio.'" AND "'.$data_final.'") ';
		}
		
		($produtos == 'todos') ? $filtro_join = $filtro : $filtro_where = $filtro;
		
		$filtro_where .= (isset($cliente) && $cliente != 'todos') ? " AND tblpedido.fldCliente_Id = ".$cliente : '';
		$replaceFROM = ',
		tblpedido.fldPedido_Destino_Nfe_Id,
		tblpedido.fldCadastroData,
		tblcliente.fldId as fldClienteId,
		tblcliente.fldNome as fldClienteNome,
		SUM(tblpedido_item.fldQuantidade * (tblpedido_item.fldExcluido * -1 + 1)) AS fldTotalQtd,
		AVG(tblpedido_item.fldValor * (tblpedido_item.fldExcluido * -1 + 1)) as fldValor_Media_Venda, 
		AVG(tblpedido_item.fldValor_Compra * (tblpedido_item.fldExcluido * -1 + 1)) as fldValor_Media_Compra, 
		AVG(tblpedido_item.fldDesconto * (tblpedido_item.fldExcluido * -1 + 1)) as fldProdutoDesconto
		
		FROM tblproduto
		
		LEFT JOIN tblpedido_item ON tblproduto.fldId = tblpedido_item.fldProduto_Id
		LEFT JOIN tblpedido  ON tblpedido.fldId = tblpedido_item.fldPedido_Id '.$filtro_join.' 
		LEFT JOIN tblcliente ON tblpedido.fldCliente_Id = tblcliente.fldId ';
		
		$replaceWHERE	= 'WHERE (tblpedido.fldPedido_Destino_Nfe_Id IS NULL OR tblpedido.fldPedido_Destino_Nfe_Id <= 0) '.$filtro_where.' AND';
		
		//$sSQL 			= str_replace('FROM tblproduto ', $replaceFROM, $_SESSION['produto_relatorio_sql']);
		
		#DEIXAR COM ESSA FORMATACAO PARA PODER PEGAR DA LISTAGEM CORRETAMENTE
		$replace_listar = ',
					 SUM(fldEntrada - fldSaida) as fldEstoqueQuantidade
					 FROM tblproduto 
					 LEFT JOIN tblproduto_estoque_movimento ON tblproduto_estoque_movimento.fldProduto_Id = tblproduto.fldId AND tblproduto_estoque_movimento.fldEstoque_Id > 0
					 ';
		$sSQL 			= str_replace($replace_listar, $replaceFROM, $_SESSION['produto_relatorio_sql']);
					 
		$sSQL 			= str_replace('WHERE', $replaceWHERE, $sSQL);
		$sSQL 			= str_replace('LEFT JOIN tblproduto_compativel ON tblproduto_compativel.fldProduto_Id = tblproduto.fldId', '', $sSQL);
		
		$rsRelatorio 	= mysql_query($sSQL);
		$rowsRelatorio	= mysql_num_rows($rsRelatorio);
		echo mysql_error();
		$n	 			= 1; #DEFINE O NUMERO DO BLOCO
		$countRegistro 	= 1; #DEFINE CONTAGEM DE ITENS NO WHILE PARA QUEBRA DE BLOCO AO ATINGIR LIMITE 
		$x				= 1; #DEFINE CONTAGEM DE ITENS TOTAIS, PRA SABER SE JA TERMINOU WHILE MAS AINDA FALTA ESPACO
		$limite 		= 44;
		
		//$pgTotal 		= ceil($rowsRelatorio / $limite);
		$p = 1;
		
		while($rowRelatorio = mysql_fetch_array($rsRelatorio)){
			echo mysql_error();
			if($detalhes == 'detalhado'){
				$rsCliente = mysql_query("SELECT tblcliente.fldNome,  tblcliente.fldId, 
										AVG(tblpedido_item.fldValor * (tblpedido_item.fldExcluido * -1 + 1)) as fldValorMedia
										FROM tblpedido 
										LEFT JOIN tblpedido_item ON tblpedido_item.fldPedido_Id = tblpedido.fldId
										LEFT JOIN tblcliente ON tblpedido.fldCliente_Id = tblcliente.fldId
										WHERE tblpedido_item.fldProduto_Id = ".$rowRelatorio['ProdutoId'].$filtro_where." GROUP BY tblpedido.fldCliente_Id");
				echo mysql_error();
				//caso precise detalhar por cliente, adiciona  o numero de linhas por cliente
				$rows_cliente 	= mysql_num_rows($rsCliente);
				$rowsRelatorio += $rows_cliente + 1; # +1 por causa do cabecalho do cliente, fora os registros em si
			}
			unset($lucroBruto);
			unset($lucro);
			
			if($produtos == 'todos'){
			
				$vendaValor 	=  $rowRelatorio['fldValorVenda'];
				$compraValor 	=  $rowRelatorio['fldValorCompra'];
				
				if($vendaValor > 0){
					$lucroBruto = $vendaValor - $compraValor;
					$lucro 		= ($lucroBruto / $vendaValor) * 100;
				}
				
				$quantidade = "0";
				
			}else{

				$vendaValor 	= $rowRelatorio['fldValor_Media_Venda'];
				$compraValor 	= $rowRelatorio['fldValor_Media_Compra'];
				
				$CompraDesconto = ($compraValor * $rowCompraValor['fldProdutoDesconto']) / 100;
				$compraValor 	= $compraValor - $CompraDesconto;
				$compraTotal	= number_format($compraValor,2, '.', '') * $rowRelatorio['fldTotalQtd'];
				/////////////////////////////////////////////////////////////////////////////////
			
				$VendaDesconto 	= ($rowRelatorio['fldValor_Media_Venda'] * $rowRelatorio['fldProdutoDesconto']) / 100;
				$vendaValor 	= $vendaValor - $VendaDesconto;
				$vendaTotal 	= number_format($vendaValor,2, '.', '') * $rowRelatorio['fldTotalQtd'];
		
				if($vendaTotal > 0){
					$lucroBruto = $vendaTotal - $compraTotal;
					$lucro 		= ($lucroBruto / $vendaTotal) * 100;
				}
				
				if($rowRelatorio['fldTotalQtd'] > 0){
					$quantidade = $rowRelatorio['fldTotalQtd'];
				}else{
					$quantidade = "0";
				}

			}
			//soma total para rodape ########################//
			$saldoVendaTotal 	+= $vendaTotal;
			$saldoCompraTotal 	+= $compraTotal;
			$saldoLucroBruto 	+= $lucroBruto;
			$totalQuantidade 	+= $quantidade;
			//##############################################//
			
			$style = (isset($detalhes) && $detalhes == 'detalhado') ? "style='background:#DFDFDF'" : '';
			$pagina[$n] .= '
			<tr '.$style.' >
				<td class="valor">'.$rowRelatorio['fldCodigo'].'</td>
				<td style="width:218px">'.substr($rowRelatorio['fldNomeProduto'],0, 28).'</td>
				<td class="valor" style="width:50px">'.format_number_out($quantidade).'</td>
				<td class="valor">'.format_number_out($vendaValor).'</td>
				<td class="valor">'.format_number_out($vendaTotal).'</td>
				<td class="valor">'.format_number_out($compraValor).'</td>
				<td class="valor">'.format_number_out($compraTotal).'</td>
				<td class="valor">'.format_number_out($lucroBruto).'</td>
				<td class="valor">'.format_number_out($lucro).'</td>
			</tr>';
			
			#SE CHEGAR LIMITE DE LINHAS, MUDA DE 'BLOCO' E RECMECA CONTAGEM
			if($countRegistro == $limite){
				$countRegistro = 1;
				$n ++;
			}elseif($rowsRelatorio == $x && $countRegistro < $limite){ #SE JA TERMINOU O WHILE DE REGISTROS MAS AINDA NAO ATINGIU LIMITE DE LINHAS, CONTINUAR CRIANDO LINHAS ATE O LIMITE
				while($countRegistro <= $limite){ $pagina[$n] .='<tr style="border:0; width:800px"></tr>'; $countRegistro++;}
			}else{
				$countRegistro ++;
				
				## CASO DETALHE POR CLIENTE, FAZ O MESMO PROCESSO DE CONTAGEM DAS LINHAS E ADICIONA O CABECALHO ##############################################################################################
				if($rows_cliente > 0){
					$pagina[$n] .= '
						<tr style="background:#F3F3F3">
							<td class="valor">&nbsp;</td>
							<td style="width:620px">Cliente</td>
							<td class="valor">Quantidade</td>
							<td style="width:13px">&nbsp;</td>
						</tr>';
					
					if($countRegistro == $limite){
						$countRegistro = 1;
						$n ++;
						
						## APARECE NOS DOIS IFS (TAMB�M ABAIXO), POIS SE ACABAR A FOLHA COM APENAS O CABECALHO, ELE CONTINUA NA PROXIMA COM O ITEM
						## FAZ O MESMO PROCESSO DE CONTAGEM DAS LINHAS E ADICIONA A QUANTIDADE #################################################################################################################
						while($rowCliente = mysql_fetch_array($rsCliente)){
							$rsProdutos = mysql_query("SELECT SUM(tblpedido_item.fldQuantidade * (tblpedido_item.fldExcluido * -1 + 1)) AS fldQuantidade
												FROM tblpedido 
												LEFT JOIN tblpedido_item ON tblpedido_item.fldPedido_Id = tblpedido.fldId
												$replaceWHERE tblpedido.fldCliente_Id = ".$rowCliente['fldId']." 
												AND tblpedido.fldExcluido = '0' AND tblpedido_item.fldProduto_Id = ".$rowRelatorio['ProdutoId']. $filtro);
							echo mysql_error();
							$rowProdutos = mysql_fetch_array($rsProdutos);
							
							$pagina[$n] .= '
							<tr>
								<td class="valor">&nbsp;</td>
								<td style="width:620px">'.$rowCliente['fldNome'].'</td>
								<td class="valor">'.format_number_out($rowProdutos['fldQuantidade']).'</td>
								<td style="width:13px">&nbsp;</td>
							</tr>';
							
							#SE CHEGAR AO LIMITE DE LINHAS, MUDA DE 'BLOCO' E RECMECA CONTAGEM
							if($countRegistro == $limite){
								$countRegistro = 1;
								$n ++;
							}else{
								$countRegistro ++;
							}
							$x ++;
                        }
					}else{
						$countRegistro ++;
						
						## FAZ O MESMO PROCESSO DE CONTAGEM DAS LINHAS E ADICIONA A QUANTIDADE #################################################################################################################
						while($rowCliente = mysql_fetch_array($rsCliente)){
							$rsProdutos = mysql_query("SELECT SUM(tblpedido_item.fldQuantidade * (tblpedido_item.fldExcluido * -1 + 1)) AS fldQuantidade
												FROM tblpedido 
												LEFT JOIN tblpedido_item ON tblpedido_item.fldPedido_Id = tblpedido.fldId
												$replaceWHERE tblpedido.fldCliente_Id = ".$rowCliente['fldId']." 
												AND tblpedido.fldExcluido = '0' AND tblpedido_item.fldProduto_Id = ".$rowRelatorio['ProdutoId']. $filtro);
							echo mysql_error();
							$rowProdutos = mysql_fetch_array($rsProdutos);
							$pagina[$n] .= '
							<tr>
								<td class="valor">&nbsp;</td>
								<td style="width:620px">'.$rowCliente['fldNome'].'</td>
								<td class="valor">'.format_number_out($rowProdutos['fldQuantidade']).'</td>
								<td style="width:13px">&nbsp;</td>
							</tr>';
							
							#SE CHEGAR AO LIMITE DE LINHAS, MUDA DE 'BLOCO' E RECMECA CONTAGEM
							if($countRegistro == $limite){
								$countRegistro = 1;
								$n ++;
							}else{
								$countRegistro ++;
							}
							$x ++;
                        } #END WHILE
					}#END ELSE
					$x ++;
				}#END IF ROWS CLIENTE
			}#END ELSE LIMITE REGISTRO
			$x ++;			
		}#END WHILE RELATORIO
	
	#AGORA MANDO GERAR NA TELA PARA IMPRESSAO ############################################################################################################################################
		$x = 1;
		while($x <= $n){
			$tabelaCabecalho1 = ($x == 1)? '<table class="relatorio_print" style="page-break-before:avoid">'.$tabelaCabecalho : '<table class="relatorio_print">'.$tabelaCabecalho;
			#PRIMEIRO BLOCO (LANCADOS) ###################################################################################################################################################
				print $tabelaCabecalho1;
                print '<td style="width: 200px"><p class="pag">p&aacute;g. '.$p.'</p></td>';
				
				print $tabelaCabecalho2;
				print $tabelaCabecalho3;
				echo  $pagina[$x];
?>
						</table>
					</td >
				</tr>
			</table>			
<?			$x ++;
			$p ++;
		}
?>                
    <table name="table_relatorio_rodape" class="table_relatorio_rodape" summary="Relat&oacute;rio">
        <tr>
            <td style="width:50px">&nbsp;</td>
            <td style="width:60px">Quantidade</td>
            <td style="width:80px; text-align:right"><?=format_number_out($totalQuantidade)?></td>
            <td style="width:3px; border-left:1px solid">&nbsp;</td>
            <td style="width:80px">Val. Vendas</td>
            <td style="width:80px; text-align:right"><?=format_number_out($saldoVendaTotal)?></td>
            <td style="width:3px; border-left:1px solid">&nbsp;</td>
            <td style="width:80px">Val. Compras</td>
            <td style="width:80px; text-align:right"><?=format_number_out($saldoCompraTotal)?></td>
            <td style="width:3px; border-left:1px solid">&nbsp;</td>
            <td style="width:70px">Lucro</td>
            <td style="width:80px; text-align:right"><?=format_number_out($saldoLucroBruto)?></td>
        </tr>
    </table>