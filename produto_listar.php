<?
	require("inc/con_db.php");
	require("produto_filtro.php");
	
	//a��es em grupo
	if(isset($_POST['hid_action'])){
		require("produto_action.php");
	}
	
	if(isset($_GET['mensagem']) && $_GET['mensagem'] == "ok"){
?>		<div class="alert">
			<p class="ok">Produto "<?= substr($_GET['produto'],0,50)?>" com o c&oacute;digo "<?= $_GET['codigo'] ?>" foi gravado com sucesso!<p>
        </div>
<?
	}

/**************************** ORDER BY *******************************************/
	$filtroOrder = 'tblproduto.fldNome ';
	$class 		 = 'asc';
	$order_sessao = explode(" ", $_SESSION['order_produto']);
	if(isset($_GET['order'])){
		switch($_GET['order']){
			
			case 'codigo'		:  $filtroOrder = "tblproduto.fldCodigo+0";			break;
			case 'produto'		:  $filtroOrder = "tblproduto.fldNome"; 			break;
			case 'categoria'	:  $filtroOrder = "tblcategoria.fldNome"; 			break;
			case 'marca'		:  $filtroOrder = "tblmarca.fldNome"; 				break;
			case 'data'			:  $filtroOrder = "tblproduto.fldCadastroData"; 	break;
			case 'preco'		:  $filtroOrder = "tblproduto.fldValorVenda"; 		break;
		}
		if($order_sessao[0] == $filtroOrder){
			$class = ($order_sessao[1] == 'asc') ? 'desc' : 'asc';
		}
	}
	
	//definir icone para ordem
	$_SESSION['order_produto'] = (!$_SESSION['order_produto'] || $_GET['order']) ? $filtroOrder.' '.$class : $_SESSION['order_produto'];
	$pag	= ($_GET['pagina'])? '&pagina='.$_GET['pagina'] : ''; 
	$raiz 	= "index.php?p=produto$pag&amp;order=";
	
	$order_sessao = explode(" ", $_SESSION['order_produto']);
	$filtroOrder  = $order_sessao[0]; //pra poder comparar na listagem e exibir a class
		
/**************************** PAGINA��O *******************************************/

	#ATENCAO AO MEXER NA CONSULTA POIS ESTA SENDO UTILIZADA COM REPLACE NOS RELATORIOS!!!
	$sSQL =  "SELECT tblproduto.fldId AS ProdutoId,
					 tblproduto.fldDisabled as ProdutoDisabled, 
					 tblproduto.fldCodigo,
                                         tblproduto.fldRefCodigo, 
					 tblproduto.fldCodigoBarras, 
					 tblproduto.fldUN_Medida_Id, 
					 tblproduto.fldCadastroData, 
					 tblproduto.fldValorVenda, 
					 tblproduto.fldValorCompra, 
					 tblproduto.fldNome as fldNomeProduto, 
					 tblproduto.fldFornecedor_Id,
					 tblmarca.fldNome as fldNomeMarca, 
					 tblcategoria.fldNome as fldNomeCategoria,
					 SUM(fldEntrada - fldSaida) as fldEstoqueQuantidade
					 FROM tblproduto 
					 LEFT JOIN tblproduto_estoque_movimento ON tblproduto_estoque_movimento.fldProduto_Id = tblproduto.fldId AND tblproduto_estoque_movimento.fldEstoque_Id > 0
					 LEFT JOIN tblmarca ON tblmarca.fldId = tblproduto.fldMarca_Id
					 LEFT JOIN tblcategoria ON tblcategoria.fldId = tblproduto.fldCategoria_Id
					 LEFT JOIN tblproduto_compativel ON tblproduto_compativel.fldProduto_Id = tblproduto.fldId
					 WHERE tblproduto.fldExcluido = 0 
					 ". $_SESSION['filtro_produto'];
	
	$_SESSION['produto_barcode'] = $sSQL;
	
	 $sSQL .= 		" GROUP BY tblproduto.fldId ".$_SESSION['filtro_produto_estoque_quantidade']."
					 ORDER BY " . $_SESSION['order_produto'].", tblproduto.fldNome";

	$_SESSION['produto_relatorio_sql'] = $sSQL;
	
	$rsTotal = mysql_query($sSQL);
	echo mysql_error();
	$rowsTotal = mysql_num_rows($rsTotal);
	
	//defini��o dos limites
	$limite = 150;
	$n_paginas = 7;
	
	$total_paginas = ceil(mysql_num_rows($rsTotal) / $limite);
	
	if(isset($_GET["pagina"]) && $_GET["pagina"] > $total_paginas){
		$inicio = 0;
	}elseif(isset($_GET['pagina'])){
		$inicio = ($_GET['pagina'] - 1) * $limite;
	}else{
		$inicio = 0;
	}
	
	$sSQL .= " limit $inicio, $limite";
	$rsProduto = mysql_query($sSQL); 
	$pagina = (isset($_GET['pagina'])) ? $_GET['pagina'] : "1";
	
#########################################################################################
	
?>
    <form class="table_form" id="frm_produto" action="" method="post">
    	<div id="table">
        
        	<div id="tabela_corpo">
            <div id="table_cabecalho">
                <ul class="table_cabecalho">
                    <li class="order" style="width:65px;">
                    	<a <?= ($filtroOrder == 'tblproduto.fldCodigo+0') 		? "class='$class'" : '' ?> style="width:50px" href="<?=$raiz?>codigo">C&oacute;digo</a>
                    </li>
                    <li class="order" style="width:280px;">
                    	<a <?= ($filtroOrder == 'tblproduto.fldNome') 			? "class='$class'" : '' ?> style="width:265px" href="<?=$raiz?>produto">Produto</a>
                    </li>
                    <li class="order" style="width:130px;">
                    	<a <?= ($filtroOrder == 'tblcategoria.fldNome') 		? "class='$class'" : '' ?> style="width:130px" href="<?=$raiz?>categoria">Categoria</a>
                    </li>
                    <li class="order" style="width:140px;">
                    	<a <?= ($filtroOrder == 'tblmarca.fldNome') 			? "class='$class'" : '' ?> style="width:125px" href="<?=$raiz?>marca">Marca</a>
                    </li>
                    <li class="order" style="width:70px;">
                    	<a <?= ($filtroOrder == 'tblproduto.fldCadastroData') 	? "class='$class'" : '' ?> style="width:70px" href="<?=$raiz?>data">Cadastro</a>
                    </li>
                    <li class="order" style="width:60px;">
                    	<a <?= ($filtroOrder == 'tblproduto.fldValorVenda') 	? "class='$class'" : '' ?> style="width:45px" href="<?=$raiz?>preco">Pre&ccedil;o</a>
                    </li>
                    <li style="width:50px; text-align:center;">Estoque</li>
                    <li style="width:25px; text-align:right;" title="Unidade de Medida">U.M</li>
                    <li style="width:40px">&nbsp;</li>
                    <li style="width:15px"><input type="checkbox" name="chk_todos" id="chk_todos" /></li>
                </ul>
            </div>
            <div id="table_container">       
                <table id="table_general" class="table_general" summary="Lista de produtos">
                	<tbody>
<?					
						$id_array = array();
						$n = 0;
						$linha 	= "row";
						$rows 	= mysql_num_rows($rsProduto);
						while ($rowProduto 	= mysql_fetch_array($rsProduto)){
							if($rowProduto['fldUN_Medida_Id'] != null){
								$rsUnidade 	= mysql_query("SELECT * FROM tblproduto_unidade_medida WHERE fldId=".$rowProduto['fldUN_Medida_Id']);
								$rowUnidade = mysql_fetch_array($rsUnidade);
							}
							$rsEnderecamento  = mysql_query("SELECT fldEnderecamento_Id FROM tblproduto_estoque_enderecamento WHERE fldProduto_id=".$rowProduto['ProdutoId']);
							if(mysql_num_rows($rsEnderecamento)){
									
								$rowEnderecamento = mysql_fetch_array($rsEnderecamento);
								$enderecamento_sigla = 'Local de estoque: '.getEnderecamento_Sigla($rowEnderecamento['fldEnderecamento_Id'], 1);
							}echo mysql_error();
							$id_array[$n] = $rowProduto["ProdutoId"];
							$n += 1;
								
?>							<tr class="<?= $linha; ?>">
<?					 	 		$icon  = ($rowProduto["ProdutoDisabled"] ? "bg_disable" : "bg_enable");
								$title = ($rowProduto["ProdutoDisabled"] ? "desabilitado" : "habilitado");
?>								<td	style="width:auto;text-align:left;"><img src="image/layout/<?=$icon?>.gif" alt="status" title="<?=$title?>" /></td>
                                <td class="cod"	style="width:40px;"><?=str_pad($rowProduto['fldCodigo'], 6, "0", STR_PAD_LEFT)?></td>
                                <td	style="width:294px; text-align:left; padding-left:10px;" title="<?=$enderecamento_sigla?>"><?=$rowProduto['fldNomeProduto']?></td>
                                <td style="width:130px;"><?=$rowProduto['fldNomeCategoria']?></td>
                                <td style="width:138px;"><?=$rowProduto['fldNomeMarca']?></td>
                                <td style="width:75px; text-align:right;"><?=format_date_out($rowProduto['fldCadastroData'])?></td>
                                <td style="width:60px; text-align:right;"><?=format_number_out($rowProduto['fldValorVenda'], fnc_sistema('venda_casas_decimais'))?></td>
                                <td style="width:60px; text-align:right;"><?=format_number_out($rowProduto['fldEstoqueQuantidade'], fnc_sistema('quantidade_casas_decimais'))?></td>
                                <td style="width:26px; text-align:center;" class="tipo" title="<?=$rowUnidade['fldNome']?>"><?=$rowUnidade['fldSigla']?></td>
								<td style="width:-1px;"></td>
                                <td style="width:0"><a class="edit" href="<?=constant("NEW_PATH")."vwProduct/".$rowProduto['fldRefCodigo']?>" title="editar"></a></td>
                                <td style="width:0"><a class="btn_duplicar" href="#" onclick="return alert('temporariamente desativado!');" title="duplicar produto"></a></td>
                                <td style="width:0"><input type="checkbox" name="chk_produto_<?=$rowProduto['ProdutoId']?>" id="chk_produto_<?=$rowProduto['ProdutoId']?>" value="<?=$rowProduto['ProdutoId']?>" title="selecionar o registro posicionado" /></td>
    	                    </tr>
<?	                      $linha = ($linha == "row") ? "dif-row" : "row";
                  		}
?>		 			</tbody>
				</table>
            </div>
       	  </div>
            <input type="hidden" name="hid_array" id="hid_array" value="<?=urlencode(serialize($id_array))?>" />
            <input type="hidden" name="hid_action" id="hid_action" value="true" />
            
			<div id="table_action">
                <ul id="action_button">
                    <li><a class="btn_novo" href="<?=constant("NEW_PATH")."vwProduct/"?>">novo</a></li>
                    <li><input type="submit" name="btn_action" id="btn_excluir" 	value="excluir" 	title="Excluir registro(s) selecionado(s)" onclick="return confirm('Deseja excluir os registros selecionados?')" /></li>
                    <li><input type="submit" name="btn_action" id="btn_habilitar" 	value="habilitar" 	title="Habilitar registro(s) selecionado(s)" /></li>
                    <li><input type="submit" name="btn_action" id="btn_desabilitar" value="desabilitar" title="Desabilitar registro(s) selecionado(s)" /></li>
                    
                	<li><a class="modal btn_print" 			name="btn_print" id="btn_print" title="imprimir relat&oacute;rio"			href="produto_relatorio"		rel="436-230"></a></li>
                    <li><a class="modal btn_print_barcode"	name="btn_print_barcode" 		title="imprimir c&oacute;digos de barras" 	href="produto_barcode,produto"	rel="430-180"></a></li>
                    <li><a class="btn_balanca" 				name="btn_balanca" 				title="exportar produtos de balan&ccedil;a" href="produto_balanca_exportar.php"></a></li>
                    <li><button type="submit" name="btn_action" id="btn_fardo_relatorio" title="Relat&oacute;rio de fardos" value="relatorio_fardo"></button></li>
                    <!--<li><a class="modal" title="relatorio de comiss&otilde;es" href="produto_relatorio_comissao" rel="420-100"><img src="image/layout/academia_relatorio.png"></a></li>-->
<?					if($_SESSION["sistema_tipo"] == 'farmacia'){    
?>	                	<li><a class="modal btn_import" title="importar arquivo de produtos" href="produto_importar_txt" rel="530-120"></a></li>
<?					}
?>				</ul>
        	</div>
            <div id="table_paginacao" style="width:230px;">
<?				$paginacao_destino = "?p=produto";
				include("paginacao.php")
?>          </div>  
            <div class="table_registro">
            	<span>Exibindo registros <?=($pagina*$limite-$limite+1).' a '.($pagina*$limite-$limite+$rows)?> do total de <?=$rowsTotal?></span>
            </div>      
        </div>
       
	</form>
