<?php
	$filtro = '';
	
	$data2 	= date('Y-m-d');
	$data1 	= date('Y-m-d', strtotime($data2 .' - 1 month'));

	if($_SESSION['funcionario_conta_fluxo_filtro_data1'] == ""){
		$_SESSION['funcionario_conta_fluxo_filtro_data1'] = format_date_out($data1);
	}
	if($_SESSION['funcionario_conta_fluxo_filtro_data2'] == ""){
		$_SESSION['funcionario_conta_fluxo_filtro_data2'] = format_date_out($data2);
	}

	//memorizar os filtros para exibição nos selects
	if(isset($_POST['btn_limpar'])){
		$_SESSION['funcionario_conta_fluxo_filtro_data1'] = format_date_out($data1);
		$_SESSION['funcionario_conta_fluxo_filtro_data2'] = format_date_out($data2);
		$_SESSION['sel_funcionario_conta_fluxo_tipo'] 	= "0";
	}else{
		#NAO PODE FICAR SEM DATA O FILTRO
		$_SESSION['funcionario_conta_fluxo_filtro_data1'] 	= (!empty($_POST['funcionario_conta_fluxo_filtro_data1'])) ? $_POST['funcionario_conta_fluxo_filtro_data1'] : $_SESSION['funcionario_conta_fluxo_filtro_data1'];
		$_SESSION['funcionario_conta_fluxo_filtro_data2'] 	= (!empty($_POST['funcionario_conta_fluxo_filtro_data2'])) ? $_POST['funcionario_conta_fluxo_filtro_data2'] : $_SESSION['funcionario_conta_fluxo_filtro_data2'];
		$_SESSION['sel_funcionario_conta_fluxo_tipo']		= (isset($_POST['sel_funcionario_conta_fluxo_tipo']))			? $_POST['sel_funcionario_conta_fluxo_tipo']	: $_SESSION['sel_funcionario_conta_fluxo_tipo'];
	}
?>

<form id="frm-filtro" action="index.php?p=funcionario_detalhe&modo=conta_fluxo&id=<?=$funcionario_id?>" method="post">
	<fieldset>
        <legend>Buscar por:</legend>
        <ul>
            <li class="large_height">
                <label for="funcionario_conta_fluxo_filtro_data1">Per&iacute;odo</label>
<?				$data1 = $_SESSION['funcionario_conta_fluxo_filtro_data1'];
?>     			<input style="text-align:center;width: 70px" type="text" name="funcionario_conta_fluxo_filtro_data1" id="funcionario_conta_fluxo_filtro_data1" class="calendario-mask" value="<?=$data1?>"/>
            </li>
            <li class="large_height">
                <label for="funcionario_conta_fluxo_filtro_data2"></label>
<?				$data2 = $_SESSION['funcionario_conta_fluxo_filtro_data2'];
?>     			<input style="text-align:center;width: 70px" type="text" name="funcionario_conta_fluxo_filtro_data2" id="funcionario_conta_fluxo_filtro_data2" class="calendario-mask" value="<?=$data2?>"/>
                <a href="calendario_periodo,<?=format_date_in($data1) . ',' . format_date_in($data2)?>,funcionario_detalhe&modo=ponto_listar&id=<?=$funcionario_id?>" id="exibir-calendario" title="Exibir calend&aacute;rio" class="modal calendario-modal" rel="600-320"></a>
            </li>
    		<li>
        		<select id="sel_funcionario_conta_fluxo_tipo" name="sel_funcionario_conta_fluxo_tipo" style="width:200px" title="Tipo de movimenta&ccedil;&atilde;o" >.
                	<option <?=($_SESSION['sel_funcionario_conta_fluxo_tipo'] == "0") ? 'selected="selected"' : '' ?> value="0">todas movimenta&ccedil;&otilde;es</option>
<?					$rsTipo = mysql_query("SELECT * FROM tblfinanceiro_conta_fluxo_tipo WHERE fldReferencia_Id = 1 ORDER BY fldTipo"); //referencia 1 = funcionario
					while($rowTipo = mysql_fetch_array($rsTipo)){
?>              		<option <?=($_SESSION['sel_funcionario_conta_fluxo_tipo'] == $rowTipo['fldId']) ? 'selected="selected"' : '' ?> value="<?=$rowTipo['fldId']?>"><?=$rowTipo['fldTipo']?></option>
<?					}
?>				</select>
			</li>
            <li style="float:right">
                <button type="submit" name="btn_exibir" title="Exibir">Exibir</button>
            	<button type="submit" name="btn_limpar" title="Limpar Filtro">Limpar filtro</button>
        	</li>
    	</ul>
	</fieldset>
</form>

<?
	if(isset($_SESSION['funcionario_conta_fluxo_filtro_data1']) && isset($_SESSION['funcionario_conta_fluxo_filtro_data2'])){
		$filtro = " AND tblfuncionario_conta_fluxo.fldData BETWEEN '".format_date_in($_SESSION['funcionario_conta_fluxo_filtro_data1'])."' AND '".format_date_in($_SESSION['funcionario_conta_fluxo_filtro_data2'])."'";
	}
	
	if(isset($_SESSION['sel_funcionario_conta_fluxo_tipo']) && $_SESSION['sel_funcionario_conta_fluxo_tipo'] > 0){
		$filtro .= " AND tblfuncionario_conta_fluxo.fldMovimento_Tipo = '".$_SESSION['sel_funcionario_conta_fluxo_tipo']."'";
	}
	
	//transferir para a sessão
	$_SESSION['filtro_funcionario_conta_fluxo'] = $filtro;
	
?>
