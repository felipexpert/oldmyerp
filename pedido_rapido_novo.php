<? 	header("Location: ".constant("NEW_PATH")."vwSale");
	die();
	$vendaDecimal 		= fnc_sistema('venda_casas_decimais');
	$quantidadeDecimal 	= fnc_sistema('quantidade_casas_decimais');
	
	//AQUI FAZ A CONSULTA PRA PEGAR O PRIMEIRO GARCOM (VENDEDOR) QUE ESTIVER CADASTRADO E JA JOGAR NA VENDA
	$rsFuncionario = mysql_query("SELECT fldId, fldNome, fldFuncao1_Comissao FROM tblfuncionario WHERE fldFuncao1_Comissao > 0 ORDER BY fldId DESC LIMIT 1");
	$rowFuncionario = mysql_fetch_array($rsFuncionario);
	echo mysql_error();
?>
<div class="form">
	<form class="frm_detalhe" style="width:960px; float:left; margin-left: 10px" name="frm_pedido_rapido_novo" id="frm_pedido_rapido_novo" action="" method="post" autocomplete="off">
    	<input type="hidden" id="hid_disabled" name="hid_disabled" value="0" />
    	<input type="hidden" id="hid_comanda" name="hid_comanda" value="<?=$_GET['comanda']?>" />
        
    	<div class="pedido_rapido">
            <ul>
                <li>
                    <label for="txt_funcionario_codigo">Funcion&aacute;rio</label>
                    <input type="text" class="enter" style="width:80px; text-align:right" id="txt_funcionario_codigo" name="txt_funcionario_codigo" value="<?=$rowFuncionario['fldId']?>" />
                    <a href="funcionario_busca,1" title="Localizar" class="modal" rel="680-380"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                    <input type="hidden" id="hid_funcionario_comissao" name="hid_funcionario_comissao" value="<?=$rowFuncionario['fldFuncao1_Comissao']?>" />
                    
                </li>
                <li style="width:475px">
                    <label for="txt_funcionario_nome">&nbsp;</label>
                    <input type="text" class="enter" style=" width:603px;" id="txt_funcionario_nome" name="txt_funcionario_nome" value="<?=$rowFuncionario['fldNome']?>" readonly="readonly" />
                </li>
            </ul>

            <div id="pedido_produto" style="width:730px">
                <ul>
                    <li>
                        <label for="txt_produto_quantidade">Qtde [F6]</label>
                        <input type="text" style="width:50px; text-align:right" id="txt_produto_quantidade" name="txt_produto_quantidade" value="1" />
                        <input type="hidden" id="txt_produto_desconto" name="txt_produto_desconto" value="" />
                        <input type="hidden" name="hid_quantidade_decimal" id="hid_quantidade_decimal" value="<?=$quantidadeDecimal?>" />
                    </li>
                    <li>
                        <label for="txt_produto_codigo">C&oacute;digo [F7]</label>
                        <input type="text" class="codigo" style="width: 80px" id="txt_produto_codigo" name="txt_produto_codigo" value="" />
                        <a href="produto_busca,txt_produto_codigo" title="Localizar" class="modal" rel="950-450"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                       	<input type="hidden" id="hid_produto_id" 				name="hid_produto_id" 				value="" />
                        <input type="hidden" id="hid_estoque_limite"		 	name="hid_estoque_limite" 			value="0" />
                        <input type="hidden" id="hid_estoque_controle" 			name="hid_estoque_controle" 		value="0" />
                        <input type="hidden" id="hid_estoque_negativo_alerta" 	name="hid_estoque_negativo_alerta" 	value="<?= fnc_sistema('alerta_estoque_negativo')?>" />
                        <input type="hidden" id="hid_item_repetido_alerta"	 	name="hid_item_repetido_alerta" 	value="<?=$_SESSION["sistema_pedido_item_repetido"]?>" />
                    </li>
                    <li>
                        <label for="txt_produto_nome">Produto</label>
                        <input type="text" style="width: 400px" id="txt_produto_nome" name="txt_produto_nome" value="" readonly="readonly" />
                        <input type="hidden" name="sel_produto_estoque" id="sel_produto_estoque" value="1" /><!-- #s� esta usando o nome pra poder buscar o produto, por causa do JS ser o mesmo da venda comum -->
                        <input type="hidden" name="hid_venda_decimal" id="hid_venda_decimal" value="<?=$vendaDecimal?>" />
                    </li>
                    <li>
                        <label for="txt_produto_valor">Valor Un.</label>
                        <input type="text" style="width:70px; text-align:right" id="txt_produto_valor" name="txt_produto_valor" value="" />
                    </li>
                    <li>
                        <button class="btn_sub_small" name="btn_item_inserir" id="btn_item_inserir" >ok</button>
                    </li>
                </ul>
    
                <div id="table" style="width:728px">
                    <div id="table_cabecalho" style="width:728px">
                        <ul class="table_cabecalho" style="width:728px">
                            <li style="width:3px">&nbsp;</li>
                            <li style="width:80px;">C&oacute;digo</li>
                            <li style="width:380px;">Produto</li>
                            <li style="width:80px; text-align:right">Valor Un.</li>
                            <li style="width:55px; text-align:center">Qtde</li>
                            <li style="width:85px; text-align:right">SubTotal</li>
                        </ul>
                    </div>
                    
                    <div id="table_container" style="width:728px; height:295px"> 
                        <table id="table_pedido_rapido" class="table_general" summary="Lista de produtos">
                            <tbody>
								<tr id="pedido_lista_item" title="pedido_lista_item" style="display:none">
									<td style="width: 80px; padding-left:8px">
										<input class="txt_item_codigo" type="text" style="width: 75px" id="txt_item_codigo_0" name="txt_item_codigo" readonly="readonly" value=""/>
										<input class="hid_item_produto_id" type="hidden" id="hid_item_produto_id_0" name="hid_item_produto_id_0" value="" />
									</td>
									<td style="width: 380px">
										<input class="txt_item_nome" type="text" style=" width:375px; text-align:left" id="txt_item_nome_0" readonly="readonly" name="txt_item_nome" value="" />
										<input class="hid_item_estoque_id" type="hidden" id="hid_item_estoque_id_0" name="hid_item_estoque_id" value="1" />
									</td>
									<td style="width: 80px">
										<input class="txt_item_valor" type="text" style="width:75px; text-align:right" id="txt_item_valor_0" readonly="readonly" name="txt_item_valor" value="" />
									</td>
									<td style="width: 55px">
										<input class="txt_item_quantidade" type="text" style="width:50px; text-align: right" id="txt_item_quantidade_0" readonly="readonly" name="txt_item_quantidade" value=""  />
									</td>
									<td style="width: 85px">
										<input class="txt_item_subtotal" type="text" style="width:80px; text-align: right" id="txt_item_subtotal_0" readonly="readonly" name="txt_item_subtotal" value=""  />
									</td>
								</tr>
								
					                
<?								#SE FOR VENDA IMPORTADA
								$controle = 0;
								(isset($_GET['cliente_id'])) ? $cliente_id = $_GET['cliente_id'] : '';
								if(isset($_GET['duplica'])){
									$pedido_id = $_GET['duplica'];
									
									$rsPedido 	= mysql_query("SELECT *	FROM tblpedido WHERE fldId = $pedido_id");
									$rowPedido 	= mysql_fetch_array($rsPedido);
									$cliente_id = $rowPedido['fldCliente_Id'];
									
									$rsItem = mysql_query("SELECT * FROM tblpedido_item WHERE fldPedido_Id = $pedido_id");
									$rows 	= mysql_num_rows($rsItem);
									$i = 1;
									while($rowItem = mysql_fetch_array($rsItem)){
											
										$rsProduto = mysql_query("SELECT tblproduto.*, 
														 tblproduto_unidade_medida.fldSigla
														 FROM tblproduto LEFT JOIN tblproduto_unidade_medida ON tblproduto.fldUN_Medida_Id = tblproduto_unidade_medida.fldId
														 WHERE tblproduto.fldId = " . $rowItem['fldProduto_Id']);
										$rowProduto = mysql_fetch_array($rsProduto);
										
										$rowEstoque = mysql_fetch_array(mysql_query("SELECT tblproduto_estoque.fldId, tblproduto_estoque.fldNome
																	FROM tblproduto_estoque INNER JOIN tblpedido_item ON tblproduto_estoque.fldId = tblpedido_item.fldEstoque_Id
																	WHERE tblproduto_estoque.fldId = ".$rowItem['fldEstoque_Id']."
										"));
										
										$item_id 	= $rowItem['fldId'];
										$valor_item = $rowItem['fldValor'];
										$qtd 		= $rowItem['fldQuantidade'];
										$total_item = $valor_item * $qtd;
										$desconto 	= $rowItem['fldDesconto'];
										$controle 	+= 1;
						
?>

										<tr id="pedido_lista_item" title="pedido_lista_item">
											<td style="width: 80px; padding-left:8px">
												<input class="txt_item_codigo" type="text" style="width: 75px" id="txt_item_codigo_<?=$i?>" name="txt_item_codigo_<?=$i?>" readonly="readonly" value="<?=$rowProduto['fldCodigo']?>"/>
												<input class="hid_item_produto_id" type="hidden" id="hid_item_produto_id_<?=$i?>" name="hid_item_produto_id_<?=$i?>" value="<?=$rowProduto['fldId']?>" />
											</td>
											<td style="width: 380px">
												<input class="txt_item_nome" type="text" style=" width:375px; text-align:left" id="txt_item_nome_<?=$i?>" name="txt_item_nome_<?=$i?>" readonly="readonly" value="<?=$rowItem['fldDescricao']?>" />
												<input class="hid_item_estoque_id" type="hidden" id="hid_item_estoque_id_<?=$i?>" name="hid_item_estoque_id_<?=$i?>" value="<?=$rowItem['fldEstoque_Id']?>" />
											</td>
											<td style="width: 80px">
												<input class="txt_item_valor" type="text" style="width:75px; text-align:right" id="txt_item_valor_0_<?=$i?>" name="txt_item_valor_<?=$i?>" readonly="readonly" value="<?=format_number_out($rowItem['fldValor'],$vendaDecimal)?>" />
											</td>
											<td style="width: 55px">
												<input class="txt_item_quantidade" type="text" style="width:50px; text-align: right" id="txt_item_quantidade_<?=$i?>" name="txt_item_quantidade_<?=$i?>" readonly="readonly" value="<?=format_number_out($rowItem['fldQuantidade'],$quantidadeDecimal)?>"  />
											</td>
											<td style="width: 85px">
												<input class="txt_item_subtotal" type="text" style="width:80px; text-align: right" id="txt_item_subtotal_<?=$i?>" name="txt_item_subtotal_<?=$i?>" readonly="readonly" value="<?=format_number_out($total_item,$vendaDecimal)?>"  />
											</td>
										</tr>

<?								 		$subTotalPedido += $total_item;
										$i += 1;
									}
									//total pedido
									$descPedido 			= $rowPedido['fldDesconto'];
									$descPedidoReais 		= $rowPedido['fldDescontoReais'];
									$desccontoPedido 		= ($subTotalPedido * $descPedido) / 100;
									$totalPedido 			= $subTotalPedido - $desccontoPedido;
									$totalPedido 			= $totalPedido - $descPedidoReais;
								}	
								if(!$descPedido || !$descPedidoReais){
									$descPedido 	 = '0.00';
									$descPedidoReais = '0.00';
								}
								if(!$totalPedido){
									$totalPedido 	= '0.00';
									$subTotalPedido = '0.00';
								}
?>
							</tbody>
                        </table>
					</div>  
                </div>
                <div>
                	<input type="hidden" name="hid_controle" id="hid_controle" value="<?=$controle?>" />
					<input type="hidden" name="hid_cursor" id="hid_cursor" value="0" />
                </div>
            </div>
		</div>
   
        <div class="pedido_rapido_pagamento">
            <ul>
            	<li class="dados">
                    <label for="txt_codigo">Id</label>
                    <input type="text" style="width:90px; height:15px" id="txt_codigo" name="txt_codigo" readonly="readonly" value="novo"/>
                    <input type="hidden" id="txt_check_enviado" name="txt_check_enviado" value="checar" style="width:50px" readonly="readonly" />
                </li>
                
                <li class="dados">
                    <label for="txt_usuario">Usu&aacute;rio</label>
                    <input type="text" style="width:95px; height:15px" id="txt_usuario" name="txt_usuario" value="<?=$rowUsuario['fldUsuario']?>" readonly="readonly"/>
                </li>
                <li class="dados">
                    <label for="txt_pedido_data" style="text-align:left">Data de Venda</label>
                    <input type="text" style="width:90px; height:15px;text-align:center; font-size:12px" class="calendario-mask" id="txt_pedido_data" name="txt_pedido_data" value="<?=date('d/m/Y')?>" />
                    <a href="#" title="Exibir calend&aacute;rio" class="exibir-calendario-data-atual"></a>
                </li>
                <li class="dados">
                    <label for="txt_pedido_hora">Hora</label>
                    <input type="text" style="width: 72px;height:15px" id="txt_pedido_hora" name="txt_pedido_hora" value="<?=date('H:i')?>" readonly="readonly" />
                </li>
                <li style="height: 60px;float:left">
<?				if($_GET['comanda']){                
					echo '<a class="btn_comanda_gravar" name="btn_comanda_gravar" id="btn_comanda_gravar" href="" >voltar</a>';
				}	
?>				</li>

                <li style="height:60px;float:left">
<?				if($_GET['comanda']){                
					echo '<input type="text" class="total" style="width:98px; height:50px; text-align:center; color:#6FF;font-size:30px" id="txt_comanda" name="txt_comanda" value="'.$_GET['comanda'].'" readonly="readonly" />';
				}	
?>				</li>		
                <li>
                    <label for="txt_pedido_subtotal">Produtos</label>
                    <input type="text" class="valor" style="width:208px" id="txt_pedido_subtotal" name="txt_pedido_subtotal" value="<?=format_number_out($subTotalPedido)?>" readonly="readonly" />
                </li>
<?				if(fnc_sistema('pedido_comissao') == 1 && $_GET['comanda']){
?>					<li>
                        <label for="txt_pedido_comissao">Comiss&atilde;o</label>
                        <input type="text" class="comissao" style="width:208px" id="txt_pedido_comissao" name="txt_pedido_comissao" value="0,00" readonly="readonly" />
                    </li>
<?				}
?>				<li style="float:left">
                    <label for="txt_pedido_desconto_reais">[F8] Desc (R$)</label>
                    <input type="text" class="desconto" style="width:130px;float:right" id="txt_pedido_desconto_reais" name="txt_pedido_desconto_reais" value="<?=format_number_out($descPedidoReais)?>" />
                </li>
                <li>
                    <label for="txt_pedido_desconto">(%)</label>
                    <input type="text" class="desconto" style="width:60px;float:right" id="txt_pedido_desconto" name="txt_pedido_desconto" value="<?=format_number_out($desccontoPedido)?>" />
                </li>
                <li>
                    <label for="txt_pedido_total" class="total">Total</label>
                    <input type="text" class="total" style="width:208px" id="txt_pedido_total" name="txt_pedido_total" value="<?=format_number_out($totalPedido)?>" readonly="readonly" />
                </li>
                <li>
                    <a class="btn_cancel" style="width:210px" name="btn_finalizar" id="btn_finalizar" href="pedido_pagamento<?=(isset($cliente_id)) ? ','.$cliente_id  : ''?>" rel="<?= (fnc_sistema("pedido_retirado_por") == "1") ? '480-390' : '480-340'?>">[F10] Finalizar</a>
                    <a class="btn_cancel" style="width:210px" name="btn_finalizar_prazo" id="btn_finalizar_prazo" href="pedido_pagamento_parcela" rel="650-460">[F11] Finalizar e parcelas</a>
                    <a class="btn_cancel" style="width:210px" name="btn_cancelar" id="btn_cancelar" href=""  onclick="return confirm('Deseja cancelar esta venda?')">[F12] Cancelar</a>
               </li>
            </ul>
        </div>
	</form>
</div>

<script type="text/javascript">
	$("#txt_pedido_total").keydown( function(event) {
		switch(event.keyCode){
			case 13:
				event.preventDefault();
				$("#btn_finalizar").focus();
			break;
		}
	});
</script>
