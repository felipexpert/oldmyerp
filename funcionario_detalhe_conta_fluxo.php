<?	
	if(isset($_POST['btn_action'])){
		require("funcionario_detalhe_conta_fluxo_action.php");
	}
	require("funcionario_detalhe_conta_fluxo_filtro.php");
	$sSQL = "SELECT tblfuncionario_conta_fluxo.*, tblfinanceiro_conta_fluxo_tipo.fldTipo, tblusuario.fldUsuario
	
			 FROM tblfuncionario_conta_fluxo 
			 INNER JOIN tblfinanceiro_conta_fluxo_tipo	ON tblfuncionario_conta_fluxo.fldMovimento_Tipo = tblfinanceiro_conta_fluxo_tipo.fldId
			 LEFT  JOIN tblusuario 						ON tblfuncionario_conta_fluxo.fldUsuario_Id		= tblusuario.fldId
			 WHERE tblfuncionario_conta_fluxo.fldFuncionario_Id = '$funcionario_id' AND tblfinanceiro_conta_fluxo_tipo.fldReferencia_Id = '1' 
			 ".$_SESSION['filtro_funcionario_conta_fluxo']." ORDER BY fldData DESC, fldHora DESC";
	$rsMovimento = mysql_query($sSQL);
	$_SESSION['funcionario_conta_fluxo_relatorio'] = $sSQL;
?>

<form class="table_form" id="frm_funcionario_conta_fluxo" action="" method="post">
    <div id="table">
        <div id="table_cabecalho">
            <ul class="table_cabecalho">
                <li style="width:80px;text-align:center">Data</li>
                <li style="width:60px;text-align:center">Hora</li>
                <li style="width:430px">Descri&ccedil;&atilde;o</li>
                <li style="width:80px;text-align:right">Refer&ecirc;ncia</li>
                <li style="width:100px;text-align:right">Valor</li>
                <li style="width:150px;text-align:right">Usu&aacute;rio</li>
                <li style="width:15px"></li>
            </ul>
        </div>
        <div id="table_container">       
            <table id="table_general" class="table_general" summary="Hist&oacute;rico de servi&ccedil;os">
                <tbody>
<?					$id_array 	= array();
                    $linha 		= "row";
					$n = 0;
                   	while($rowMovimento = mysql_fetch_array($rsMovimento)){	
						$rsEstorno = mysql_query('SELECT fldEstorno FROM tblfuncionario_conta_fluxo WHERE fldEstorno = '.$rowMovimento['fldId']);
						$id_array[$n] 	= $rowMovimento["fldId"];
						$n += 1;
						if(mysql_num_rows($rsEstorno)){
							$class 			= 'credito';
							$descricao 		= $rowMovimento['fldTipo'].' [ESTORNO]';
							$total_saldo	-= $rowMovimento['fldValor'];
						}else{
							$class 			= 'debito';
							$descricao 		= $rowMovimento['fldTipo'];
							$total_saldo	+= $rowMovimento['fldValor'];
						}					
?>						<tr class="<?= $linha?>" style="color:">
                            <td style="width:80px;text-align:center"><?=format_date_out($rowMovimento['fldData'])?></td>
                            <td style="width:60px;text-align:center"><?=format_time_short($rowMovimento['fldHora'])?></td>
                            <td style="width:430px" class="<?=$class?>"><?=$descricao?></td>
                            <td style="width:80px;text-align:right"><?= str_pad($rowMovimento['fldReferencia_Id'], 5,'0', STR_PAD_LEFT)?></td>
                            <td style="width:100px;text-align:right" class="<?=$class?>"><?=format_number_out($rowMovimento['fldValor'])?></td>
                            <td style="width:150px;text-align:right"><?=$rowMovimento['fldUsuario']?></td>
							<td style="width:20px; text-align:right">
<?							if($rowMovimento['fldEstorno'] == '0'){                            
?>                            	<input type="checkbox" name="chk_funcionario_fluxo_<?=$rowMovimento['fldId']?>" id="chk_funcionario_fluxo_<?=$rowMovimento['fldId']?>" title="selecionar o registro posicionado" /></td>
<?							}
?>                        </tr>
<?                 		$linha 		 = ($linha == "row") ? "dif-row" : "row";
                    }
                    echo mysql_error();
?>				</tbody>
            </table>
        </div>
        
		<input type="hidden" name="hid_array" 			id="hid_array" 			value="<?=urlencode(serialize($id_array))?>" />
        <div class="saldo" style="width:952px; float:right;">
            <p style="padding:4px; margin-left:750px">Total Pago <span style="margin-left:4px;width:100px;text-align:right" class="debito"><?=format_number_out($total_saldo)?></span></p>
        </div>
        
        <div id="table_action">
            <ul id="action_button">
            
                <li><a class="btn_novo modal" href="funcionario_conta_fluxo_novo,<?=$funcionario_id?>" rel="650-150" title="novo">novo</a></li>
                <li><input type="submit" name="btn_action" id="btn_recibo" 	value="recibo" 		title="Imprimir recibo de registro(s) selecionado(s))" /></li>
                <li><a id="btn_print" name="btn_print" class="btn_print" href="funcionario_detalhe_conta_fluxo_relatorio.php?id=<?=$funcionario_id?>" title="Imprimir Relat&oacute;rio" target="_blank">&nbsp;</a></li>
            </ul>
        </div> 
    </div>
</form>