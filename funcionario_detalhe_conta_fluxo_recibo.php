<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
        <title>Impress&atilde;o de recibo</title>
		<link rel="stylesheet" type="text/css" href="style/impressao/style_pedido_imprimir_recibo_A4.css" />
    	<link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />
    
	</head>
	<body>
    
<? 
	ob_start();
	session_start();
	
	require("inc/con_db.php");
	require("inc/fnc_general.php");
	require("inc/fnc_imprimir.php");
	require("inc/fnc_identificacao.php");
	
	$rsDados 	= mysql_query("SELECT * FROM tblempresa_info");
	$rowDados 	= mysql_fetch_array($rsDados);
	
	$funcionario_id	= $_GET["id"];
	$filtro	= $_GET["filtro"];/*
	$filtro_id	= explode(',', $_GET["id"]);
	$filtro_id 	= array_filter($filtro_id); //RETIRANDO ARRAY NULL
	echo mysql_error();
	*/
	$data = date("Y-m-d");
	$hora = date("H:i:s");
	
	
	$sSQL = "SELECT tblfuncionario.fldNome, tblfuncionario.fldId as fldFuncionario_Id, fldValor, fldData, tblfinanceiro_conta_fluxo_tipo.fldTipo
			FROM tblfuncionario_conta_fluxo 
			INNER JOIN tblfuncionario ON tblfuncionario_conta_fluxo.fldFuncionario_Id = tblfuncionario.fldId
			INNER JOIN tblfinanceiro_conta_fluxo_tipo ON tblfuncionario_conta_fluxo.fldMovimento_Tipo = tblfinanceiro_conta_fluxo_tipo.fldId
			WHERE tblfuncionario_conta_fluxo.fldId IN ($filtro)";

	$rsBaixa		= mysql_query($sSQL);
	
	$impressao		= fnc_sistema('sistema_impressao');
	$rowImpressao 	= mysql_fetch_array(mysql_query("SELECT * FROM tblsistema_impressao WHERE fldId = $impressao"));
	$impressao 		= $rowImpressao['fldModelo'];
	
	require('funcionario_detalhe_conta_fluxo_recibo_'.$impressao.'.php');
?>

    </body>
</html>