<?	
	if (!isset($_SESSION['logado'])){
		session_start();
	}

	ob_start();
	session_start();
	
	require_once("inc/con_db.php");
	require_once("inc/fnc_general.php");
	require_once("inc/fnc_imprimir.php");
	require_once("inc/fnc_identificacao.php");
	require_once("inc/fnc_ibge.php");

	$impressao_local = fnc_estacao_impressora($_SESSION['remote_name']);
	if(!$impressao_local){
		$impressao_local = fnc_estacao_impressora('todos');
	}
	
	$texto 				= $impressao_local."\r\n";
	$usuario_sessao 	= $_SESSION['usuario_id'];
	$pedido_id  		= $_GET['id'];
	//$data 				= date("Y-m-d");
	
	$rsPedido  			= mysql_query("SELECT SUM((tblpedido_item.fldValor * (tblpedido_item.fldExcluido * -1 + 1)) * tblpedido_item.fldQuantidade) as fldTotalItem,
							(SELECT SUM(fldValor) FROM tblpedido_funcionario_servico WHERE fldFuncao_Tipo = 2 AND fldPedido_Id = tblpedido.fldId) as fldTotalServico,
							tblpedido.fldCadastroData as fldPedidoData, tblpedido.fldServico, tblpedido.fldDesconto, tblpedido.fldValor_Terceiros, tblpedido.fldId as fldPedidoId, tblpedido.fldDescontoReais, tblcliente.*
							FROM tblpedido 
							LEFT JOIN tblpedido_item ON tblpedido.fldId = tblpedido_item.fldPedido_Id
							INNER JOIN tblcliente ON tblpedido.fldCliente_Id = tblcliente.fldId
							WHERE tblpedido.fldId = $pedido_id GROUP BY tblpedido_item.fldPedido_Id");
	$rowPedido 			= mysql_fetch_array($rsPedido);
	
	echo mysql_error();
	$rsEmpresa  	= mysql_query("SELECT * FROM tblempresa_info");
	$rowEmpresa 	= mysql_fetch_array($rsEmpresa);
	$CPF_CNPJ		= formatCPFCNPJTipo_out($rowEmpresa['fldCPF_CNPJ'], $rowEmpresa['fldTipo']);
	
	$total_pedido 			= $rowPedido['fldTotalItem'] + $rowPedido['fldTotalServico'] + $rowPedido['fldValor_Terceiros'];
	$desconto_pedido 		= $rowPedido['fldDesconto'];
	$desconto_reais_pedido 	= $rowPedido['fldDescontoReais'];
	$desconto 				= ($total_pedido * $desconto_pedido) / 100;
	$total_descontos 		= $desconto + $desconto_reais_pedido;
	$total_venda 			= $total_pedido - $total_descontos;
	
	
	$CPF_CNPJCliente = formatCPFCNPJTipo_out($rowPedido['fldCPF_CNPJ'], $rowPedido['fldTipo']);
	
	$rsParcela = mysql_query("SELECT fldVencimento FROM tblpedido_parcela WHERE fldPedido_Id = ".$rowPedido['fldPedidoId']." AND fldParcela = '1' AND fldStatus = 1 AND fldExcluido = 0 ");
	$rowParcela = mysql_fetch_array($rsParcela);
	
	//acentoRemover(strtoupper($rowEmpresa['fldNome_Fantasia']));
	
	$limite = 32; #LIMITE DE LINHAS POR FOLHA
	
	$texto 	.= str_pad("", 80, "=");
	$texto 	.= "\r\n\r\n\r\n";
	
	$municipio 	= fnc_ibge_municipio($rowEmpresa['fldMunicipio_Codigo']);
	$siglaUF	= fnc_ibge_uf_sigla($rowEmpresa['fldMunicipio_Codigo']);
	
	//$data_extenso 		  = dataSemiExtenso(date("m"));
	$textoRodape 		 .= format_margem_print($textoNP, 80, 'esquerda');
	$textoRodape 		 .="\r\n\r\n";
	$limiteRodape 		 +=2;
	
	$textoRodape 		 .= format_margem_print((" ".$municipio." - ".$siglaUF.",    ".format_date_out($rowPedido['fldPedidoData'])), 80, 'esquerda');
	$textoRodape 		 .="\r\n\r\n\r\n";
	$limiteRodape 		 +=3;
	
	$textoRodape 		 .= format_margem_print(" ".acentoRemover($rowPedido['fldNome']), 80, 'esquerda');
	$textoRodape 		 .="\r\n";
	$limiteRodape 		 +=1;
	
	$textoRodape 		 .= format_margem_print(" CPF/CNPJ: ". $CPF_CNPJCliente, 80, 'esquerda');
	$textoRodape 		 .="\r\n\r\n\r\n";
	$limiteRodape 		 +=3;
	
	$textoRodape 		 .= format_margem_print(" -----------------------------------------  ", 50, 'esquerda');
	$textoRodape 		 .= format_margem_print("  --------------------------- ", 30, 'direita');
	$textoRodape 		 .="\r\n";
	$limiteRodape 		 +=1;
	
	$textoRodape 		 .= format_margem_print(" ".substr(acentoRemover($rowPedido['fldNome']),0,45), 50, 'esquerda');
	$textoRodape 		 .= format_margem_print(" ".substr(acentoRemover($rowEmpresa['fldRazao_Social']),0, 28), 30, 'centro');
	$limiteRodape 		 +=1;
		
	$texto_np	= " Ao <b>".formata_data_extenso($rowParcela['fldVencimento'])."</b> pagarei por esta unica via de NOTA PROMISSORIA a <b>".acentoRemover($rowEmpresa['fldRazao_Social'])."</b> CNPJ - $CPF_CNPJ ou a sua ordem, a quantia de R$ " . format_number_out($total_venda) . " (".utf8_encode(valorExtenso($total_venda,1,"baixa")).") em moeda corrente deste pais, pagavel em $municipio - $siglaUF.";
	$x = 0;
	$limite_caracter = 80;
	while(strlen(substr($texto_np,$x * $limite_caracter, $limite_caracter)) > 0){
		
		$textoNP .= substr($texto_np,$x * $limite_caracter, $limite_caracter)." \r\n ";
		$x ++;
		$limiteRodape += 1;
	}
	
	$texto .= $textoNP;
	$texto .= $textoRodape;
	
	$linhasRestantes 	= $limite - $limiteRodape;
	while($linhasRestantes  > 0){
		$texto .= "\r\n";
		$linhasRestantes  -=1;
	}
	
	$timestamp  = date("Ymd_His");
	$local_file = "impressao///inbox///imprimir_np_$timestamp.txt"; // Definimos o local para salvar o arquivo de texto
	
	$fp 		= fopen($local_file, "w+"); //utilizamos o operador w+ para criar o arquivo imprimir.txt, e APAGAR tudo que j� existe nele, caso ele j� exista.
	$salva		= fwrite($fp, $texto);
	$texto 		= fread($fp, filesize($local_file));
	
	//transformamos as quebras de linha em etiquetas <br>
	print $texto;

?>
