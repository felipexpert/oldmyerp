<?
	require("inc/con_db.php");
	require("funcionario_funcao_filtro.php");
	
	//a��es em grupo
	if(isset($_POST['hid_action'])){
		require("funcionario_funcao_action.php");
	}
	
	if(isset($_GET['mensagem']) && $_GET['mensagem'] == "ok"){
?>		<div class="alert">
			<p class="ok">Registro gravado com sucesso!<p>
        </div>
<?	}
	
/**************************** ORDER BY *******************************************/
	$filtroOrder 	= 'fldFuncao';
	$class 		 = 'asc';
	$order_sessao = explode(" ", $_SESSION['order_funcionario_funcao']);
	if(isset($_GET['order'])){
		switch($_GET['order']){
			
			case 'codigo'	:  $filtroOrder = "fldId";   		break;
			case 'funcao'	:  $filtroOrder = "fldFuncao"; 		break;
			case 'comissao'	:  $filtroOrder = "fldComissao"; 	break;
			case 'tipo'		:  $filtroOrder = "fldTipo"; 		break;
		}
		if($order_sessao[0] == $filtroOrder){
			$class = ($order_sessao[1] == 'asc') ? 'desc' : 'asc';
		}
	}
	
	//definir icone para ordem
	$_SESSION['order_funcionario_funcao'] = (!$_SESSION['order_funcionario_funcao'] || $_GET['order']) ? $filtroOrder.' '.$class : $_SESSION['order_funcionario_funcao'];
	$pag	= ($_GET['pagina'])? '&pagina='.$_GET['pagina'] : ''; 
	$raiz 	= "index.php?p=funcionario&amp;modo=funcao$pag&amp;order=";
	
	$order_sessao = explode(" ", $_SESSION['order_funcionario_funcao']);
	$filtroOrder  = $order_sessao[0]; //pra poder comparar na listagem e exibir a class
	
/**************************** PAGINA��O *******************************************/
	$sSQL = "SELECT * FROM tblfuncionario_funcao ". $_SESSION['filtro_funcionario_funcao']." ORDER BY " . $_SESSION['order_funcionario_funcao'];
	$_SESSION['funcionario_relatorio_funcao'] = $sSQL;
	
	$rsTotal 	= mysql_query($sSQL);
	$rowsTotal 	= mysql_num_rows($rsTotal);
	echo mysql_error();
	//defini��o dos limites
	$limite		= 50;
	$n_paginas 	= 7;
	$total_paginas = ceil(mysql_num_rows($rsTotal) / $limite);
	
	if(isset($_GET["pagina"]) && $_GET["pagina"] > $total_paginas){
		$inicio = 0;
	}elseif(isset($_GET['pagina'])){
		$inicio = ($_GET['pagina'] - 1) * $limite;
	}else{
		$inicio = 0;
	}
	
	$sSQL .= " LIMIT $inicio , $limite";
	$rsFuncao = mysql_query($sSQL);
	
	$pagina = ($_GET['pagina'] ? $_GET['pagina'] : "1");
	
#########################################################################################
?>
    <form class="table_form" id="frm_funcionario" action="" method="post">
    	<div id="table">
            <div id="table_cabecalho">
                <ul class="table_cabecalho">
                    <li class="order" style="width:70px">
                    	<a <?= ($filtroOrder == 'fldId') 		? "class='$class'" : '' ?> style="width:55px" href="<?=$raiz?>codigo">C&oacute;digo</a>
                    </li>
                    <li class="order" style="width:490px">
                    	<a <?= ($filtroOrder == 'fldFuncao') 	? "class='$class'" : '' ?> style="width:475px" href="<?=$raiz?>funcao">Fun&ccedil;&atilde;o</a>
                    </li>
                    <li class="order" style="width:115px;text-align:center">
                    	<a <?= ($filtroOrder == 'fldComissao') 	? "class='$class'" : '' ?> style="width:85px" href="<?=$raiz?>comissao">Comiss&atilde;o (%)</a>
                    </li>
                    <li class="order" style="width:100px;text-align:center">
                    	<a <?= ($filtroOrder == 'fldTipo') 		? "class='$class'" : '' ?> style="width:65px" href="<?=$raiz?>tipo">Tipo</a>
                    </li>
                    <li style="width:100px;text-align:center">Cadastro</li>
                    <li style="width:23px"></li>
                    <li style="width:20px"><input type="checkbox" name="chk_todos" id="chk_todos" /></li>
                </ul>
            </div>
            <div id="table_container">       
                <table id="table_general" class="table_general">
               		<tbody>
<?					
						$id_array = array();
						$n 		= 0;
						$linha 	= "row";
						$rows 	= mysql_num_rows($rsFuncao);
						while($rowFuncao = mysql_fetch_array($rsFuncao)){
							$id_array[$n] = $rowFuncao["fldId"];
							$n += 1;
							$tipo = ($rowFuncao['fldTipo'] == 1 )? 'venda' : 'servi&ccedil;o' ;
?>							<tr class="<?= $linha; ?>">
                                <td class="cod"	style="width:70px;text-align:center"><?= str_pad($rowFuncao['fldId'], 4, "0", STR_PAD_LEFT)?></td>
                                <td style="width:480px; padding-left:10px; text-align:left"><?=$rowFuncao['fldFuncao']?></td>
                                <td style="width:115px; text-align:center"><?=format_number_out($rowFuncao['fldComissao'])?></td>
                                <td style="width:102px; text-align:center"><?=$tipo?></td>
                                <td style="width:100px; text-align:center"><?=format_date_out($rowFuncao['fldDataCadastro'])?></td>
								<td style="width:4px;"></td>
                                <td style="width:auto"><a class="edit modal" href="funcionario_funcao,<?=$rowFuncao['fldId']?>" rel="520-150" title="editar"></a></td>
                                <td style="width:auto"><input type="checkbox" name="chk_funcao_<?=$rowFuncao['fldId']?>" id="chk_funcao_<?=$rowFuncao['fldId']?>" title="selecionar o registro posicionado" /></td>
                            </tr>
<?                      	$linha = ($linha == "row") ? "dif-row" : "row";
                   		}
?>		 			</tbody>
				</table>
            </div>
       	  
            <input type="hidden" name="hid_array" id="hid_array" value="<?=urlencode(serialize($id_array))?>" />
            <input type="hidden" name="hid_action" id="hid_action" value="true" />
            
			<div id="table_action">
                <ul id="action_button">
                    <li><a class="btn_novo modal" href="funcionario_funcao" rel="520-150">novo</a></li>
                    <li><input type="submit" name="btn_action" id="btn_excluir" value="excluir" title="Excluir registro(s) selecionado(s)" onclick="return confirm('Deseja excluir os registros selecionados?')" /></li>
                </ul>
        	</div>
            <div id="table_paginacao">
<?				$paginacao_destino = "?p=funcionario&modo=funcao";
				include("paginacao.php")
?>		
            </div>      
            <div class="table_registro">
            	<span>Exibindo registros <?=($pagina*$limite-$limite+1).' a '.($pagina*$limite-$limite+$rows)?> do total de <?=$rowsTotal?></span>
            </div>       
        </div>
	</form>