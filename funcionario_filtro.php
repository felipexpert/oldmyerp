<?php

	//memorizar os filtros para exibição nos selects
	if(isset($_POST['btn_limpar'])){
		$_SESSION['txt_funcionario_cod'] = "";
		$_SESSION['txt_funcionario'] = "";
		$_SESSION['txt_funcionario_cpf'] = "";
		$_POST['chk_funcionario'] = false;
	}else{
		$_SESSION['txt_funcionario_cod'] = (isset($_POST['txt_funcionario_cod']) ? $_POST['txt_funcionario_cod'] : $_SESSION['txt_funcionario_cod']);
		$_SESSION['txt_funcionario'] = (isset($_POST['txt_funcionario']) ? $_POST['txt_funcionario'] : $_SESSION['txt_funcionario']);
		$_SESSION['txt_funcionario_cpf'] = (isset($_POST['txt_cpf_funcionario']) ? $_POST['txt_cpf_funcionario'] : $_SESSION['txt_cpf_funcionario']);
	}

?>
<form id="frm-filtro" action="index.php?p=funcionario&modo=listar" method="post">
	<fieldset>
  	<legend>Buscar por:</legend>
    <ul>
    	<li>
<?			$codigo = ($_SESSION['txt_funcionario_cod'] ? $_SESSION['txt_funcionario_cod'] : "c&oacute;digo");
			($_SESSION['txt_funcionario_cod'] == "código") ? $_SESSION['txt_funcionario_cod'] = '' : '';
?>      	<input style="width: 124px" type="text" name="txt_funcionario_cod" id="txt_funcionario_cod" onfocus="limpar (this,'c&oacute;digo');" onblur="mostrar (this, 'c&oacute;digo');" value="<?=$codigo?>"/>
		</li>
    	<li>
        	<input type="checkbox" name="chk_funcionario" id="chk_funcionario" <?=($_POST['chk_funcionario'] == true ? print 'checked ="checked"' : '')?>/>
<?			$funcionario = ($_SESSION['txt_funcionario'] ? $_SESSION['txt_funcionario'] : "funcionario");
			($_SESSION['txt_funcionario'] == "funcionario") ? $_SESSION['txt_funcionario'] = '' : '';
?>     		<input style="width: 165px" type="text" name="txt_funcionario" id="txt_funcionario" onfocus="limpar (this,'funcionario');" onblur="mostrar (this, 'funcionario');" value="<?=$funcionario?>"/>
			<small>marque para qualquer parte do campo</small>
        </li>
        <li>
<?			$cpf = ($_SESSION['txt_funcionario_cpf'] ? $_SESSION['txt_funcionario_cpf'] : "CPF");
			($_SESSION['txt_funcionario_cpf'] == "CPF") ? $_SESSION['txt_funcionario_cpf'] = '' : '';
?>     		<input style="width: 124px" type="text" name="txt_cpf_funcionario" id="txt_cpf_funcionario" onfocus="limpar (this,'CPF');" onblur="mostrar (this, 'CPF');" value="<?=$cpf?>"/>
			<small style="margin-left: 40px"> *apenas n&uacute;meros</small>
		</li>
        <li style="margin-left:200px">
        	<button type="submit" name="btn_limpar" title="Limpar Filtro">Limpar filtro</button>
        </li>
        <li>
	        <button type="submit" name="btn_exibir" title="Exibir">Exibir</button>
        </li>
    </ul>
  </fieldset>
</form>

<?
	
	/** inicializando a string sql para realizar a consulta ($filtro)
	 * assim sempre exitirá a clásula where, mesmo sendo irrelevante neste ponto
	 */
	
	$filtro = 'where fldId > 0 ';
	
	if(($_SESSION['txt_funcionario_cod']) != ""){
		
		$filtro .= "and fldId = '".$_SESSION['txt_funcionario_cod']."'";
	}

	if(($_SESSION['txt_funcionario']) != ""){
		$funcionario = addslashes($_SESSION['txt_funcionario']); // no caso de aspas, pra nao dar erro na consulta
		if($_POST['chk_funcionario'] == true){
			$filtro .= "and fldNome like '%".$funcionario."%'";
		}else{
			$filtro .= "and fldNome like '".$funcionario."%'";
		}
	}
	
	if(($_SESSION['txt_funcionario_cpf']) != ""){
		
		$filtro .= "and fldCPF_CNPJ = '".$_SESSION['txt_funcionario_cpf']."'";
	}
			

	//transferir para a sessão
	if(isset($_POST['btn_exibir'])){
		$_SESSION['filtro_funcionario'] = $filtro;
	}
	elseif(isset($_POST['btn_limpar'])){
		$_SESSION['filtro_funcionario'] = "";
	}

?>
