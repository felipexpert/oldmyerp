 <script type="text/javascript" language="javascript">
	
	$(document).ready(function () {
		// Ao mover a barra de rolagem da tabela, mover seus cabecalhos e o 'versus'
		$("div#tabela_corpo").scroll(function () {
			$('div#tabela_corpo #table_cabecalho').css('top', $(this).scrollTop());
		});
	});

</script>

<style type="text/css">
	
	div#tabela_corpo{
		width: 960px;      /* Largura da minha tabela na tela */
		height: 280px;     /* Altura da minha tabela na tela */
		overflow: auto;    /* Barras de rolagem autom�ticas nos eixos X e Y */
		margin: 0 auto;    /* O 'auto' � para ficar no centro da tela */
		position:relative; /* Necess�rio para os cabecalhos fixos */
		top:0;             /* Necess�rio para os cabecalhos fixos */
		left:0;            /* Necess�rio para os cabecalhos fixos */
		border: 1px solid #CCC;
		border-top: 0;

	}
	
	div#tabela_corpo #table_cabecalho{
		position:absolute; /* Posi��o vari�vel em rela��o ao topo da div#tabela */
		top:0;             /* Posi��o inicial em rela��o ao topo da div#tabela */
		z-index:5;         /* Para ficar por cima da tabela de dados */
	}


	div#tabela_corpo #table_compra_listar{

		margin-top:25px;  /* 30px de altura do cabecalho horizontal + 2 pixels das bordas do cabecalho + 1 px*/
		z-index:2;		  /* Menor que dos cabecalhos, para que fique por detr�s deles */
	}

</style>

<?
	require("compra_filtro.php");

	//a��es em grupo
	if(isset($_POST['btn_action'])){require("compra_action.php");}
	
	if(isset($_GET['mensagem']) && $_GET['mensagem'] == "ok"){
?>		<div class="alert">
			<p class="ok">Registro gravado com sucesso!<p>
        </div>
<?	}
	
	/**************************** ORDER BY *******************************************/
	$filtroOrder  = 'tblcompra.fldId ';
	$class 		  = 'desc';
	$order_sessao = explode(" ", $_SESSION['order_compra']);
	if(isset($_GET['order'])){
		switch($_GET['order']){
			
			case 'codigo'		:  $filtroOrder = "tblcompra.fldId";			break;
			case 'fornecedor'	:  $filtroOrder = "fldNomeFantasia"; 			break;
			case 'data'			:  $filtroOrder = "tblcompra.fldCompraData"; 	break;
		}
		if($order_sessao[0] == $filtroOrder){
			$class = ($order_sessao[1] == 'asc') ? 'desc' : 'asc';
		}
	}
	
	//definir icone para ordem
	$_SESSION['order_compra'] = (!$_SESSION['order_compra'] || $_GET['order']) ? $filtroOrder.' '.$class : $_SESSION['order_compra'];
	$pag	= ($_GET['pagina'])? '&pagina='.$_GET['pagina'] : ''; 
	$raiz 	= "index.php?p=compra$pag&amp;order=";
	
	$order_sessao = explode(" ", $_SESSION['order_compra']);
	$filtroOrder  = $order_sessao[0]; //pra poder comparar na listagem e exibir a class
	
/**************************** PAGINA��O *******************************************/
	$sSQL = "SELECT 
				tblpedido_fiscal.fldnumero,
				tblpedido_fiscal.fldnfe_status_id,
				tblpedido_fiscal.fldnatureza_operacao_id,
				tblpedido_fiscal.fldDanfe,
				tblcompra.fldId as fldCompra_Id,
				tblcompra.fldTransporte,  
				tblcompra.fldTipo_Id,  
				tblcompra.fldDesconto,  
				tblcompra.fldDescontoReais,  
				tblcompra.fldIPI_Valor,    
				tblcompra.fldST_Valor,   
				tblcompra.fldUsuario_Id,  
				tblcompra.fldNota, 
				tblcompra.fldObservacao,
				tblcompra.fldCompraData,
				tblcompra.fldFaturado,
				(SELECT IFNULL(REPLACE(FORMAT(SUM((tblcompra_item.fldValor - ((tblcompra_item.fldDesconto / 100) * tblcompra_item.fldValor)) * tblcompra_item.fldQuantidade),2), ',' ,'') ,0) 
						FROM tblcompra_item WHERE fldCompra_Id = tblcompra.fldId) as fldTotalItem,
			
				(SELECT SUM(tblcompra_parcela_baixa.fldValor * (tblcompra_parcela_baixa.fldExcluido * -1 + 1)) FROM 
																tblcompra_parcela INNER JOIN tblcompra_parcela_baixa ON tblcompra_parcela.fldId = tblcompra_parcela_baixa.fldParcela_Id 
																WHERE tblcompra_parcela.fldcompra_Id = tblcompra.fldId) as fldValorBaixa,
				tblcompra.fldStatus AS fldStatus, 
				tblcompra_parcela.fldValor,
				tblcompra_parcela.fldVencimento, 
				tblfornecedor.fldNomeFantasia,
				tblfinanceiro_conta_fluxo_marcador.fldMarcador
			FROM (tblcompra_parcela LEFT JOIN tblcompra_parcela_baixa ON tblcompra_parcela.fldId = tblcompra_parcela_baixa.fldParcela_Id) 
				RIGHT JOIN tblcompra ON tblcompra.fldId = tblcompra_parcela.fldCompra_Id 
				LEFT JOIN tblpedido_fiscal ON tblcompra.fldId = tblpedido_fiscal.fldpedido_id AND fldDocumento_Tipo = 2
				INNER JOIN tblfornecedor ON tblcompra.fldFornecedor_Id = tblfornecedor.fldId
				LEFT JOIN tblfinanceiro_conta_fluxo_marcador ON tblfinanceiro_conta_fluxo_marcador.fldId = tblcompra.fldMarcador
			WHERE tblcompra.fldExcluido = '0' ". $_SESSION['filtro_compra']." ORDER BY " . $_SESSION['order_compra'];
			
	$_SESSION['compra_relatorio'] = $sSQL;
	$rsTotal = mysql_query($sSQL) or die(mysql_error());
	
	$rowsTotal = mysql_num_rows($rsTotal);
	echo mysql_error();
	//defini��o dos limites
	$limite = 80;
	$n_paginas = 7;
	
	$total_paginas = ceil(mysql_num_rows($rsTotal) / $limite);
	
	if(isset($_GET["pagina"]) && $_GET["pagina"] > $total_paginas){
		$inicio = 0;
	}elseif(isset($_GET['pagina'])){
		$inicio = ($_GET['pagina'] - 1) * $limite;
	}else{
		$inicio = 0;
	}
	
	
	#####################################################################################
	while($rowTotal = mysql_fetch_array($rsTotal)){
	
		$descontoCompra 	= ($rowTotal['fldTotalItem'] * $rowTotal['fldDesconto']) / 100;
		$totalCompra 		= $rowTotal['fldTotalItem'] 	- $descontoCompra;
		$totalCompra 		= $totalCompra 	- $rowTotal['fldDescontoReais'];
		$totalCompra		= (($totalCompra + $rowTotal['fldTransporte']) + $rowTotal['fldIPI_Valor']) + $rowTotal['fldST_Valor'];

		if($rowTotal['fldnatureza_operacao_id'] == '2'){
			$rodapeCompra 		-= $totalCompra;
		}elseif($rowTotal['fldParcelas '] == '0'){
			$rodapeCompra 		= $rodapeCompra;
		}else{
			$rodapeCompra 		+= $totalCompra;
		}	
		
	}	
	#####################################################################################
	
	$sSQL .= " LIMIT " . $inicio . "," . $limite;
	$rsCompra = mysql_query($sSQL);
	
	$pagina = ($_GET['pagina'] ? $_GET['pagina'] : "1");
#########################################################################################
	
?>

    <form class="table_form" id="frm_compra" action="" method="post">
        <div id="table">
        
            <div id="tabela_corpo">
            
                 <div id="table_cabecalho" style="width:1170px">
					<ul class="table_cabecalho" style="width:1170px">
                        <li style="width:15px;"><input type="checkbox" style="width:15px" name="chk_todos" id="chk_todos" /></li>
                        <li class="order" style="width:60px; text-align:right;">
                            <a <?= ($filtroOrder == 'tblcompra.fldId') 			? "class='$class'" : '' ?> style="width:35px" href="<?=$raiz?>codigo">C&oacute;d</a>
                        </li>
                        <li class="order" style="width:240px;">
                            <a <?= ($filtroOrder == 'fldNomeFantasia') 			? "class='$class'" : '' ?> style="width:165px" href="<?=$raiz?>fornecedor">Fornecedor</a>
                        </li>
                        <li class="order" style="width:70px">
                            <a <?= ($filtroOrder == 'tblcompra.fldCompraData') 	? "class='$class'" : '' ?> style="width:55px;text-align:center" href="<?=$raiz?>data">Emiss&atilde;o</a>
                        </li>
                        <li style="width:60px; text-align:center;">Ref. nota</li>
                        <li style="width:165px;">Marcador</li>
                        <li style="width:50px; text-align:right;">Itens</li>
                        <li style="width:60px; text-align:right;">Total</li>
                        <li style="width:35px; text-align:center;">Parc.</li>
                        <li style="width:65px;border">&nbsp;</li>
<?						if($_SESSION["sistema_nfe"] > 0){                        
?>							<li style="width:85px">&nbsp;</li> 
							<li class="order" style="width:50px">NFe</li> 
							<li style="width:160px; padding-left:5px">Observa&ccedil;&atilde;o</li>
<?						}else{
?>							<li style="width:290px; padding-left:5px">Observa&ccedil;&atilde;o</li>
<?						}
?>					</ul>
                </div>
                        
               
                <table id="table_compra_listar" class="table_general table_nfe_listar" summary="Lista de compras" style=" width:1170px">
                    <tbody>
<?					
						$id_array = array();
						$n = 0;
						
						$linha = "row";
						$rows = mysql_num_rows($rsCompra);
						while ($rowCompra = mysql_fetch_array($rsCompra)){
							
							$item = 0;
							$subTotalCompra = 0;
							
							$id_array[$n] 	= $rowCompra["fldCompra_Id"];
							$n += 1;
							
							$rsParcela 		= mysql_query("select * from tblcompra_parcela where fldCompra_Id = " . $rowCompra['fldCompra_Id']);
							$rowParcela 	= mysql_num_rows($rsParcela);
							
							$parcelaTotal 	= str_pad($rowParcela, 2, "0", STR_PAD_LEFT);
							$paginaLink 	= redirecionarPagina($rowCompra['fldTipo_Id'], 'compra');

							$rsItem			= mysql_query("select * from tblcompra_item where fldCompra_Id = " . $rowCompra['fldCompra_Id']);
							while($rowItem 	= mysql_fetch_array($rsItem)){
								
								$valor_item 	= $rowItem['fldValor'];
								$qtd 			= $rowItem['fldQuantidade'];
								$total_item 	= $valor_item * $qtd;
								
								$desconto 		= $rowItem['fldDesconto'];
								$totalDesconto 	= ($total_item * $desconto)/100;
								$totalValor		= $total_item - $totalDesconto;
								
								//total compra
								$subTotalCompra += $totalValor;
								$item 			+= $rowItem['fldQuantidade'];	
							}
							
							$total_itens 		= $item;
							$transporte 		= $rowCompra['fldTransporte'];
							$valorIPI 			= $rowCompra['fldIPI_Valor'];
							$valorST 			= $rowCompra['fldST_Valor'];
							
							$descCompra 		= $rowCompra['fldDesconto'];
							$descCompraReais 	= $rowCompra['fldDescontoReais'];
							$descontoCompra 	= ($subTotalCompra * $descCompra) / 100;
							$totalCompra 		= $subTotalCompra - $descontoCompra;
							$totalCompra 		= (($totalCompra - $descCompraReais) + $transporte) + $valorIPI + $valorST;
							
							$faturado_class		= ($rowCompra['fldFaturado'] == 0) ? "nao_faturado" : "faturado";
							$faturado_titulo	= ($rowCompra['fldFaturado'] == 0) ? "n&atilde;o faturado" : "faturado";
							
							
?>							<tr class="<?= $linha; ?>" id="<?=$rowCompra['fldCompra_Id']?>">
								<td style="width:20px;"><input type="checkbox" name="chk_compra_<?=$rowCompra['fldCompra_Id']?>" id="chk_compra_<?=$rowCompra['fldCompra_Id']?>" title="selecionar o registro posicionado" style="margin-top: 2px;" /></td>
								<td style="width:60px; 	text-align:center;"><span title="<?=fnc_status_pedido($rowCompra['fldCompra_Id'], "tblcompra")?>" class="<?=fnc_status_pedido($rowCompra['fldCompra_Id'], "tblcompra")?>"><?=str_pad($rowCompra['fldCompra_Id'], 4, "0", STR_PAD_LEFT)?></span></td>
								<td	style="width:240px; text-align:left;"><?=substr($rowCompra['fldNomeFantasia'],0,35)?></td>
								<td style="width:70px;	text-align:center;"><?=format_date_out3($rowCompra['fldCompraData'])?></td>
								<td style="width:60px; 	text-align:center;"><?=$rowCompra['fldNota']?></td>
								<td style="width:160px;"><?=$rowCompra['fldMarcador']?></td>
								<td style="width:50px; 	text-align:right;"><?=format_number_out($total_itens)?></td>
								<td style="width:60px; text-align:right;"><?=format_number_out($totalCompra)?></td>
								<td style="width:35px; text-align:center;"><?=$parcelaTotal?></td>
								<td style="width:25px;"><span class="<?=$faturado_class?>" title="<?=$faturado_titulo?>">&nbsp;</span></td>
								<td style="width:20px;"><a class="edit" href="index.php?p=<?=$paginaLink?>&amp;id=<?=$rowCompra['fldCompra_Id']?>" title="editar" style="margin-top:5px"></a></td>
								<td style="width:20px;"><a class="print" href="compra_imprimir.php?&amp;id=<?=$rowCompra['fldCompra_Id']?>" title="imprimir" rel="externo"></a></td>
<?								if($_SESSION["sistema_nfe"] > 0){	
									if($rowCompra['fldTipo_Id'] != 3){
										$transmitir_class 	= 'nfe_transmitir_disabled';
										$danfe_class 		= 'nfe_danfe_disabled';
										$nfe_status 		= 'disabled';
									}elseif($rowCompra['fldnfe_status_id'] > 0){
										$rsNfeStatus 		= mysql_query('SELECT fldStatus FROM tblnfe_pedido_status WHERE fldId ='.$rowCompra['fldnfe_status_id']);
										$rowNfeStatus 		= mysql_fetch_array($rsNfeStatus);
										$nfe_status			= $rowNfeStatus['fldStatus'];
										$transmitir_class 	= 'nfe_transmitir_disabled';
										$danfe_class 		= 'nfe_danfe_disabled';
										#CASO ESTEJA VALIDADO E NAO TRANSMITIDO
										switch($rowCompra['fldnfe_status_id']){
											case 2 :
												$nfe_status 	= 'processo';
											break;
											case 4:
												if($rowCompra['fldDanfe'] == '1'){
													$danfe_class 		= 'nfe_danfe_print';
													$danfe_title		= 'Danfe impresso';
												}else{
													$danfe_class 		= 'nfe_danfe';
													$danfe_title		= 'Imprimir Danfe';
												}
											break;
											case 5:
												$transmitir_class 	= 'nfe_transmitir';
											break;
										}
										
									}else{
										$nfe_status 		= 'digitacao';
										$transmitir_class 	= 'nfe_transmitir';
										$danfe_class 		= 'nfe_danfe_disabled';
									}
									
?>									<td style="width:15px"><a href="pedido_nfe_consulta_status,<?=$rowCompra['fldCompra_Id']?>" name="nfe_status" class="nfe_status_<?=$nfe_status?> modal" title="<?=$nfe_status?>" rel="780-300"></a></td>
									<td style="width:20px"><a id="modal_transmitir" href="pedido_nfe_transmitir,<?=$rowCompra['fldCompra_Id']?>,2" class="modal <?=$transmitir_class?>" title="Transmitir NFe" rel="780-300"></a></td>
									<td style="width:15px"><a href="pedido_nfe_danfe.php?id=<?=$rowCompra['fldCompra_Id']?>" target="_blank" class="<?=$danfe_class?>" title="<?=$danfe_title?>"></a></td>
                                    <td style="width:50px; text-align:right; color:#C90"><?=str_pad($rowCompra['fldnumero'],6, '0', STR_PAD_LEFT)?></td>
                                    <td style="width:170px;padding-left:5px"><?=substr($rowCompra['fldObservacao'],0,28)?></td>
<?								}else{
?>                            		<td style="width:290px;padding-left:5px"><?=substr($rowCompra['fldObservacao'],0,55)?></td>
<?								}
?>
							</tr>
<?							$linha 		= ($linha == "row" ?	$linha = "dif-row": $linha = "row");
					   }
?>			 		</tbody>
				</table>
			</div>
            
            
            <div class="saldo" style="width:952px;float:right;">
                <p style="padding:5px;width:150px;float:right">Total<span style="margin-left:6px width:100px;text-align:right" class="credito"><?=format_number_out($rodapeCompra)?></span></p>
			</div>
            
            <input type="hidden" name="hid_array" id="hid_array" value="<?=urlencode(serialize($id_array))?>" />
            <input type="hidden" name="hid_action" id="hid_action" value="true" />
            
            <div id="table_paginacao" style="width: 962px">
                <div class="table_registro" style="float:left;margin:0;text-align:left; padding-left:10px">
            		<span>Exibindo registros <?=($pagina*$limite-$limite+1).' a '.($pagina*$limite-$limite+$rows)?> do total de <?=$rowsTotal?></span>
           		</div>
                <div style="width:400px; height:25px;float:right;display:table;text-align:right;padding-left:20px">
<?					$paginacao_destino = "?p=compra";
					include("paginacao.php");
?>          	</div>
			</div>
            
            
			<div id="table_action">
                <ul id="action_button">
                    <li><a class="btn_novo" href="index.php?p=compra_novo" title="novo">novo</a></li>
                	
<?					if($_SESSION["sistema_nfe"] > 0){					
?>                      <li><a class="btn_nfe_novo modal" style="width:150px;padding-left:0" href="nfe_documento_novo,compra" rel="400-120" title="outro documento fiscal">nova NFe</a></li>
                        <li><input type="submit" name="btn_action" id="btn_alterarnumero"	value="alterar numero"	title="Alterar n&uacute;mero" /></li>
                        <li style="display:none"><a id="alterar_numero_verificado" class="modal btn_importar" href="nfe_alterar_numero,2,<?=$filtro_id?>" rel="520-375" title="alterar numero">alterar</a></li>
                    	<li><input type="submit" name="btn_action" id="btn_download" 	value="download"	title="Fazer download de registro(s) selecionado(s)"/></li>
<?					}
?>                	<li><input type="submit" name="btn_action" id="btn_excluir" value="excluir" title="Excluir registro(s) selecionado(s)" onclick="return confirm('Deseja excluir os registros selecionados?')" /></li>
                	<li><input type="submit" name="btn_action" id="btn_entregue" value="entregue" title="Efetivar entrega em registro(s) selecionado(s)" /></li>
                    <li><button id="btn_print" name="btn_action" type="submit" value="imprimir" title="Imprimir relat&oacute;rio de registro(s) selecionado(s)" ></button></li>
                </ul>
        	</div>
            
                
                 
        </div>
       
	</form>
    <ul class="status_legenda">
        <li><span class="andamento">em andamento</span></li>
        <li><span class="entregue">entregue</span></li>
    </ul>
    
    
	<script type="text/javascript">
		$('a#modal_transmitir').click(function(){
			setTimeout(fnc_nfe_consulta, 20000);
			nfe_consulta();
		});
		
		
	</script>	
    
    
    
    