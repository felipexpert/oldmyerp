<?php

	$campoId = $_GET['id'];

	##VERIFICA O ID
	if(!isset($campoId)){header("Location: index.php?p=produto&modo=camposespecificos");}
	else {
		$chkId = mysql_num_rows(mysql_query("SELECT fldId FROM tblcampos_especificos WHERE fldId = '$campoId' LIMIT 1"));
		if($chkId == 0){
?>			<div class="alert" style="margin-top:20px">
				<p class="erro">Houve um erro ao encontrar este campo, ele pode ter sido exclu&iacute;do!<p>
			</div>
<?			die;
		}
	}

	$sqlDados = mysql_query("SELECT * FROM tblcampos_especificos WHERE fldId = '$campoId' LIMIT 1");
	$rowDados = mysql_fetch_assoc($sqlDados);
	$tela_id = $rowDados['fldTela_Id'];

?>

<div id="voltar">
    <p><a href="index.php?p=produto">n&iacute;vel acima</a></p>
</div>	

<h4>Campo Espec&iacute;fico: <?=$rowDados['fldNomeCampo'];?> (em Produtos)</h4><br><br>

<div id="produto_codigo">
<?	if($_SERVER['REQUEST_METHOD'] == 'POST'){

		$nomeCampo 			= $_POST['txt_nome_campo'];
		$tipoCampo	 		= $_POST['sel_tipo_campo'];
		$exibirPara			= $_POST['sel_exibir_para'];
		$tamanhoCampo		= $_POST['txt_tamanho_campo'];
		$campoNumerico		= $_POST['sel_campo_numerico'];
		$campoObrigatorio	= $_POST['sel_campo_obrigatorio'];
		$campoRelacionado	= $_POST['sel_campo_relacionado'];
		if($campoRelacionado == 1){
			$campoObrigatorio = '0';
			//manipula a array para criar um string e salvar no bd
			$campoRelacionamento = $_POST['select_campo_relacionamento'];
			for ($i=0; $i<count($campoRelacionamento); $i++) {
				$strCR .= $campoRelacionamento[$i];
			}
		}else{$strCR = '';}
		$campoDesc 			= $_POST['txt_campo_descricao'];
		$excluido			= ($_POST['chk_excluido'] == 'excluido') ? 1 : 0;
	
		$sqlInsert = "UPDATE tblcampos_especificos SET
		fldNomeCampo =	'$nomeCampo',
		fldTipoCampo = '$tipoCampo',
		fldRelacionado = '$campoRelacionado',
		fldExclusivoTipo = '$exibirPara',
		fldObrigatorio = '$campoObrigatorio',
		fldNumerico = '$campoNumerico',
		fldDescricao = '$campoDesc',
		fldRelacionamento = '$strCR',
		fldExcluido = '$excluido' WHERE fldId = '$campoId' LIMIT 1";
		
		####### TRATAMENTOS PARA CRIAR CAMPO NO BD ############
		//manipular tipo do campo
		if($tipoCampo == 'text'){
			if($campoNumerico == '1'){
				$tipoCampoMySQL = "DECIMAL";
				$tamanhoCampo = "16,6";
			}
			else {
				$tipoCampoMySQL = "VARCHAR";
				$tamanhoCampo = "255";
				}
		}
		else{ //checkbox
			$tipoCampoMySQL = "INT";
			$tamanhoCampo = "1";
		}
		
		//obrigatorio ou nao
		if($campoObrigatorio == '1'){$obrigatorioMySQL = 'NOT NULL';}
		else {$obrigatorioMySQL = 'NULL';}
		
		#######################################################
		$aliasCampo = $_POST['txt_alias_ce'];
		$sqlInsertTabela = "ALTER TABLE `tblproduto_auxiliar` CHANGE `$aliasCampo` `$aliasCampo` $tipoCampoMySQL($tamanhoCampo) $obrigatorioMySQL";
		
		if(mysql_query($sqlInsert) && mysql_query($sqlInsertTabela)){
			//redirecionando
			switch($_SESSION['acao_cadastro_produto']){
				case '1'	:  $filtro ='';   																break;
				case '2'	:  $filtro = "_detalhe&id=".$produto_id;									 	break;
				case '3'	:  $filtro = '_novo';															break;
			}
			header("Location: index.php?p=produto&modo=camposespecificos");
		}
		else {
?>			<div class="alert">
				<p class="erro">N&atilde;o foi poss&iacute;vel gravar os dados. Por favor, entre em contato com o suporte imediatamente!</p>
				<a class="voltar" href="index.php?p=produto_camposespecificos_novo">voltar</a>
			</div>
<?			echo mysql_error();
		}
	}
?>
		<form class="frm_detalhe" name="frm_general" style="width:890px" id="frm_produto" action="" method="post" >
			<div class="form">
				
				<div style="margin:20px auto; width:870px;">
					
					<ul>
						<li class="form">
							<label for="txt_codigo">Codigo</label>
							<input type="text" readonly="readonly" style="width: 80px" value="<?=$rowDados['fldId'];?>" />
							<input type="hidden" name="txt_alias_ce" id="txt_alias_ce" readonly="readonly" style="width: 80px" value="<?=$rowDados['fldAlias'];?>" />
						</li>
						<li class="form">
							<label for="txt_nome_campo">Nome do campo</label>
							<input type="text" style=" width:240px" id="txt_nome_campo" name="txt_nome_campo" value="<?=$rowDados['fldNomeCampo'];?>" />
						</li>
						<li class="form">
							<label for="sel_tipo_campo">Tipo do campo</label>
							<select id="sel_tipo_campo" name="sel_tipo_campo" style="width:180px">
								<option <?=($rowDados['fldTipoCampo'] == '')? "selected='selected'" : ''?>></option>
								<option <?=($rowDados['fldTipoCampo'] == 'text')? "selected='selected'" : ''?> value="text">texto</option>
								<option <?=($rowDados['fldTipoCampo'] == 'check')? "selected='selected'" : ''?> value="check">checkbox</option>
								<option <?=($rowDados['fldTipoCampo'] == 'select')? "selected='selected'" : ''?> value="check">select</option>
							</select>
						</li>
						<?	if($tela_id == '3'){ ?>
						<li class="form">
							<label for="sel_exibir_para">Exibir para</label>
							<select style="width:135px" id="sel_exibir_para" name="sel_exibir_para" >
								<option value="0">todos os produtos</option>
								<?	$sqlMostrarTipos = mysql_query("SELECT * FROM tblproduto_tipo WHERE fldInativo = 0 ORDER BY fldTipo ASC");
									while($rowMostrarTipos = mysql_fetch_assoc($sqlMostrarTipos)){	?>
								<option value="<?=$rowMostrarTipos['fldId']?>" <?=($rowDados['fldExclusivoTipo'] == $rowMostrarTipos['fldId']) ? 'selected="selected"' : '';?>><?=$rowMostrarTipos['fldTipo']?></option>								
								<? 	} ?>
							</select>
						</li>
						<? } ?>
						<li class="form">
							<label for="sel_grupo">Grupo</label>
							<?	($tela_id == '3') ? $filtroGrupo = ' AND fldIdTipo = '.$rowDados['fldExclusivoTipo'].'' : $filtroGrupo = ''; ?>
							<select id="sel_grupo" name="sel_grupo" style="width:160px">
								<option value="0"></option>
								<?php
									$sqlGrupo = mysql_query("SELECT * FROM tblcampos_especificos_grupo WHERE fldTela_Id = $tela_id".$filtroGrupo);
									while($rowGrupo = mysql_fetch_assoc($sqlGrupo)){ ?>
										<option value="<?=$rowGrupo['fldIdGrupo'];?>" <?=($rowDados['fldGrupo'] == $rowGrupo['fldIdGrupo']) ? 'selected="selected"' : '';?>><?=$rowGrupo['fldTitulo']?></option>
								<? }
								?>
								<option style="background:#FFCC00; font-weight:bold; padding:3px" onclick="return gerenciarGrupo()">gerenciar grupos</option>
							</select>
							<a class="modal" href="campos_especificos_grupos,<?=$tela_id?>,0" id="gerenciar-grupos" rel="600-311" style="display:hidden;"></a>
						</li>
						<li class="form">
							<label for="sel_campo_numerico">Somente numeros</label>
							<? ($rowDados['fldTipoCampo'] == 'check') ? $desabilitado = 'disabled="disabled"' : $desabilitado = '';	?>
							<select id="sel_campo_numerico" name="sel_campo_numerico" style="width:140px" <?=$desabilitado;?>>
								<option <?=($rowDados['fldNumerico'] == '0')? "selected='selected'" : ''?> value="0">nao</option>
								<option <?=($rowDados['fldNumerico'] == '1')? "selected='selected'" : ''?> value="1">sim</option>
							</select>
						</li>
						<li class="form">
							<label for="sel_campo_obrigatorio">Obrigatorio</label>
							<select id="sel_campo_obrigatorio" name="sel_campo_obrigatorio" style="width:100px">
								<option <?=($rowDados['fldObrigatorio'] == '0')? "selected='selected'" : ''?> value="0">nao</option>
								<option <?=($rowDados['fldObrigatorio'] == '1')? "selected='selected'" : ''?> value="1">sim</option>
							</select>
						</li>
						<li class="form">
							<label for="sel_campo_relacionado">Relacionamento</label>
							<select id="sel_campo_relacionado" name="sel_campo_relacionado" <?=($rowDados['fldRelacionado'] == '1')? '' : "disabled='disabled'"?> style="width:128px">
								<option <?=($rowDados['fldRelacionado'] == '0')? "selected='selected'" : ''?> value="0">nao</option>
								<option <?=($rowDados['fldRelacionado'] == '1')? "selected='selected'" : ''?> value="1">sim</option>
							</select>
						</li>
											
						<fieldset style="clear:both; padding:3px; margin-bottom:15px;" id="field_relacionamento">
							<legend>Relacionamento</legend>
														
							<div style="overflow-x:auto; overflow-y:hidden; width:850px; height:100px;">
								<div id="relacionamento_scroll" style="width: auto; margin-top:20px;">
									
									<div id="hidden">
										<div class="teste" style="float:left; position:relative; padding:10px 5px">
											<span class="remover_relacionamento" id="a" style="font-size:10px; float:left; position:absolute; right:0; top:0; margin-top:-5px; margin-right:-3px; display:none; cursor:pointer"><img src="image/layout/excluir_relacionamento.png"></span>
										</div>
									</div>
									
<?									if($rowDados['fldRelacionado'] == 0) { #comeca exibicao dos relacionamentos ?>

									<div id="select_campo_relacionamento" class="select_campo_relacionamento" style="float:left; padding:10px 5px">
										<select name="select_campo_relacionamento[]" style="width:100px;">
											<option selected="selected"></option>
											<!-- campos fixos primeiro -->
											<?php
											switch($tela_id){
												case "1": //clientes?>
												<option value="">Limite de cr&eacute;dito</option>
											<?php	break;
												case "3": //produtos?>
												<option value="fldEstoque_Minimo">Estoque m&iacute;nimo</option>
												<option value="fldValorCompra">Valor de compra</option>
												<option value="fldValorVenda">Valor de venda</option>
											<?php	break;
												case "8": //pedido?>
												<option value="">Total de produtos</option>
												<option value="">Total de servi&ccedil;os</option>
												<option value="">Total de terceiros</option>
												<option value="">Subtotal</option>
												<option value="">Total</option>
											<?php	break;
												case "9": //compra?>
												<option value="">IPI</option>
												<option value="">Transporte</option>
												<option value="">Subtotal</option>
												<option value="">Total</option>
											<?php	break;
												case "11": //pedido_NFE?>
												<option value="">Total de produtos</option>
												<option value="">Total de servi&ccedil;os</option>
												<option value="">Total de terceiros</option>
												<option value="">Subtotal</option>
												<option value="">Total</option>
												<option value="">Total dos produtos (ICMS)</option>
												<option value="">Total ICMS</option>
												<option value="">Total ICMS Substitui&ccedil;&atilde;o</option>
												<option value="">Total Seguro</option>
												<option value="">Total II</option>
												<option value="">Total PIS</option>
												<option value="">Outras despesas Acess&oacute;rias</option>
												<option value="">BC do ICMS</option>
												<option value="">BC ICMS Substitui&ccedil;&atilde;o</option>
												<option value="">Total Frete</option>
												<option value="">Total Desconto</option>
												<option value="">Total IPI</option>
												<option value="">Total COFINS</option>
												<option value="">Total da Nota</option>
												<option value="">Base de C&aacute;lculo do ISS</option>
												<option value="">Total do ISS</option>
												<option value="">PIS sobre servi&ccedil;os</option>
												<option value="">COFINS sobre servi&ccedil;os</option>
												<option value="">Total servi&ccedil;os n&atilde;o tributados ICMS</option>
												<option value="">Valor retido de PIS</option>
												<option value="">Valor retido de CSLL</option>
												<option value="">Valor retido de IRRF</option>
												<option value="">Reten&ccedil;&atilde;o prev. social</option>
												<option value="">Valor retido COFINS</option>
												<option value="">Base C&aacute;lculo IRRF</option>
												<option value="">BC Reten&ccedil;&atilde;o prev. Social</option>
											<?php	break;
											}
											?>
											<!-- campos especificos -->
											<?php
												$sqlCamposRelacionamento = mysql_query("SELECT * FROM tblcampos_especificos WHERE fldTela_Id = $tela_id AND fldTipoCampo = 'text' AND fldNumerico = 1");
												if(mysql_num_rows($sqlCamposRelacionamento) > 0){
													while($rowCR = mysql_fetch_assoc($sqlCamposRelacionamento)){ ?>
														<option value="<?=$rowCR['fldAlias'];?>"><?=$rowCR['fldNomeCampo'];?></option>
											<?php	}
												}
											?>
										</select>
									</div>
									
<?									} else {
										$arrRelacionamento = $rowDados['fldRelacionamento'];
										$arrRelacionamento = explode("|s|", $arrRelacionamento);
										
										function campoAtual($campo, $atual){
											($campo == $atual) ? $selected = 'selected="selected"' : $selected = '';
											return $selected;
										}
										
										for($i = 0; $i < count($arrRelacionamento); $i++){
											
											if($i == 0){ //select fixo ?>
												<div id="select_campo_relacionamento" class="select_campo_relacionamento" style="float:left; padding:10px 5px">
													<select name="select_campo_relacionamento[]" style="width:100px;">
														<option selected="selected"></option>
														<!-- campos fixos primeiro -->
														<?php
														switch($tela_id){
															case "1": //clientes?>
															<option value="">Limite de cr&eacute;dito</option>
														<?php	break;
															case "3": //produtos?>
															<option value="fldEstoque_Minimo" <?=campoAtual('fldEstoque_Minimo', $arrRelacionamento[$i])?>>Estoque m&iacute;nimo</option>
															<option value="fldValorCompra" <?=campoAtual('fldValorCompra', $arrRelacionamento[$i])?>>Valor de compra</option>
															<option value="fldValorVenda" <?=campoAtual('fldValorVenda', $arrRelacionamento[$i])?>>Valor de venda</option>
														<?php	break;
															case "8": //pedido?>
															<option value="">Total de produtos</option>
															<option value="">Total de servi&ccedil;os</option>
															<option value="">Total de terceiros</option>
															<option value="">Subtotal</option>
															<option value="">Total</option>
														<?php	break;
															case "9": //compra?>
															<option value="">IPI</option>
															<option value="">Transporte</option>
															<option value="">Subtotal</option>
															<option value="">Total</option>
														<?php	break;
															case "11": //pedido_NFE?>
															<option value="">Total de produtos</option>
															<option value="">Total de servi&ccedil;os</option>
															<option value="">Total de terceiros</option>
															<option value="">Subtotal</option>
															<option value="">Total</option>
															<option value="">Total dos produtos (ICMS)</option>
															<option value="">Total ICMS</option>
															<option value="">Total ICMS Substitui&ccedil;&atilde;o</option>
															<option value="">Total Seguro</option>
															<option value="">Total II</option>
															<option value="">Total PIS</option>
															<option value="">Outras despesas Acess&oacute;rias</option>
															<option value="">BC do ICMS</option>
															<option value="">BC ICMS Substitui&ccedil;&atilde;o</option>
															<option value="">Total Frete</option>
															<option value="">Total Desconto</option>
															<option value="">Total IPI</option>
															<option value="">Total COFINS</option>
															<option value="">Total da Nota</option>
															<option value="">Base de C&aacute;lculo do ISS</option>
															<option value="">Total do ISS</option>
															<option value="">PIS sobre servi&ccedil;os</option>
															<option value="">COFINS sobre servi&ccedil;os</option>
															<option value="">Total servi&ccedil;os n&atilde;o tributados ICMS</option>
															<option value="">Valor retido de PIS</option>
															<option value="">Valor retido de CSLL</option>
															<option value="">Valor retido de IRRF</option>
															<option value="">Reten&ccedil;&atilde;o prev. social</option>
															<option value="">Valor retido COFINS</option>
															<option value="">Base C&aacute;lculo IRRF</option>
															<option value="">BC Reten&ccedil;&atilde;o prev. Social</option>
														<?php	break;
														}
														?>
														<!-- campos especificos -->
														<?php
															$sqlCamposRelacionamento = mysql_query("SELECT * FROM tblcampos_especificos WHERE fldTela_Id = $tela_id AND fldTipoCampo = 'text' AND fldNumerico = 1");
															if(mysql_num_rows($sqlCamposRelacionamento) > 0){
																while($rowCR = mysql_fetch_assoc($sqlCamposRelacionamento)){ ?>
																	<option value="<?=$rowCR['fldAlias'];?>" <?=campoAtual($rowCR['fldAlias'], $arrRelacionamento[$i])?>><?=$rowCR['fldNomeCampo'];?></option>
														<?php	}
															}
														?>
													</select>
												</div>
<?											}

											else{
												
												if($i%2 != 0){ ?>
													<div class="teste" style="float:left; position:relative; padding:10px 5px">
														<span class="remover_relacionamento" id="a" style="font-size:10px; float:left; position:absolute; right:0; top:0; margin-top:-5px; margin-right:-3px; display:none; cursor:pointer"><img src="image/layout/excluir_relacionamento.png"></span>
														<select name="select_campo_relacionamento[]" style="float:left; margin:1px 5px 0px 0px">
															<option selected="selected"></option>
															<option value="|s|+|s|" <?=campoAtual('+', $arrRelacionamento[$i])?>>+</option>
															<option value="|s|-|s|" <?=campoAtual('-', $arrRelacionamento[$i])?>>-</option>
															<option value="|s|*|s|" <?=campoAtual('*', $arrRelacionamento[$i])?>>*</option>
															<option value="|s|/|s|" <?=campoAtual('/', $arrRelacionamento[$i])?>>/</option>
														</select>
													
<?												}
												
												else{ ?>
													<div id="select_campo_relacionamento" class="select_campo_relacionamento" style="float:left;">
														<select name="select_campo_relacionamento[]" style="width:100px;">
															<option selected="selected"></option>
															<!-- campos fixos primeiro -->
															<?php
															switch($tela_id){
																case "1": //clientes?>
																<option value="">Limite de cr&eacute;dito</option>
															<?php	break;
																case "3": //produtos?>
																<option value="fldEstoque_Minimo" <?=campoAtual('fldEstoque_Minimo', $arrRelacionamento[$i])?>>Estoque m&iacute;nimo</option>
																<option value="fldValorCompra" <?=campoAtual('fldValorCompra', $arrRelacionamento[$i])?>>Valor de compra</option>
																<option value="fldValorVenda" <?=campoAtual('fldValorVenda', $arrRelacionamento[$i])?>>Valor de venda</option>
															<?php	break;
																case "8": //pedido?>
																<option value="">Total de produtos</option>
																<option value="">Total de servi&ccedil;os</option>
																<option value="">Total de terceiros</option>
																<option value="">Subtotal</option>
																<option value="">Total</option>
															<?php	break;
																case "9": //compra?>
																<option value="">IPI</option>
																<option value="">Transporte</option>
																<option value="">Subtotal</option>
																<option value="">Total</option>
															<?php	break;
																case "11": //pedido_NFE?>
																<option value="">Total de produtos</option>
																<option value="">Total de servi&ccedil;os</option>
																<option value="">Total de terceiros</option>
																<option value="">Subtotal</option>
																<option value="">Total</option>
																<option value="">Total dos produtos (ICMS)</option>
																<option value="">Total ICMS</option>
																<option value="">Total ICMS Substitui&ccedil;&atilde;o</option>
																<option value="">Total Seguro</option>
																<option value="">Total II</option>
																<option value="">Total PIS</option>
																<option value="">Outras despesas Acess&oacute;rias</option>
																<option value="">BC do ICMS</option>
																<option value="">BC ICMS Substitui&ccedil;&atilde;o</option>
																<option value="">Total Frete</option>
																<option value="">Total Desconto</option>
																<option value="">Total IPI</option>
																<option value="">Total COFINS</option>
																<option value="">Total da Nota</option>
																<option value="">Base de C&aacute;lculo do ISS</option>
																<option value="">Total do ISS</option>
																<option value="">PIS sobre servi&ccedil;os</option>
																<option value="">COFINS sobre servi&ccedil;os</option>
																<option value="">Total servi&ccedil;os n&atilde;o tributados ICMS</option>
																<option value="">Valor retido de PIS</option>
																<option value="">Valor retido de CSLL</option>
																<option value="">Valor retido de IRRF</option>
																<option value="">Reten&ccedil;&atilde;o prev. social</option>
																<option value="">Valor retido COFINS</option>
																<option value="">Base C&aacute;lculo IRRF</option>
																<option value="">BC Reten&ccedil;&atilde;o prev. Social</option>
															<?php	break;
															}
															?>
															<!-- campos especificos -->
															<?php
																$sqlCamposRelacionamento = mysql_query("SELECT * FROM tblcampos_especificos WHERE fldTela_Id = $tela_id AND fldTipoCampo = 'text' AND fldNumerico = 1");
																if(mysql_num_rows($sqlCamposRelacionamento) > 0){
																	while($rowCR = mysql_fetch_assoc($sqlCamposRelacionamento)){ ?>
																		<option value="<?=$rowCR['fldAlias'];?>" <?=campoAtual($rowCR['fldAlias'], $arrRelacionamento[$i])?>><?=$rowCR['fldNomeCampo'];?></option>
															<?php	}
																}
															?>
														</select>
													</div>
												</div>
<?												}
											}
										}
									} ?>
									
									<div id="append_campo_relacionamento" style="float:left;"></div>
									
									<input type="button" id="adicionar_campos_relacionamento" style="float:left; margin-top:12px; background:url('image/layout/adicionar_relacionamento.png'); background-repeat:no-repeat; width:16px; height:16px; border:0; cursor:pointer;" />
									
									<span style="clear:both; display:block"></span>
								</div>
							</div>
						</fieldset>

						<li class="form">
							<label for="txt_campo_descricao">Descri&ccedil;&atilde;o</label>
							<textarea style=" width:850px; height:80px;" id="txt_campo_descricao" name="txt_campo_descricao"><?=$rowDados['fldDescricao'];?></textarea>
						</li>
						
						<div style="float:right; margin-right:4px; margin-top:-10px;">
							
							<li style="margin-top:5px; width:140px;">
								<input type="checkbox" <?=($rowDados['fldExcluido'] == '1')? "checked='checked'" : ''?> id="chk_excluido" value="excluido" name="chk_excluido" style="width:0; height:0; margin:2px;"> <span style="margin:0 0 0 5px">Desabilitar Campo</span>
							</li>
						
							<li style="margin:-14px 5px 0 0;">
								<label for="sel_acao">&nbsp;</label>
								<select style=" background:#FDDC9D" id="sel_acao" name="sel_acao" >
									<option <?=($_SESSION['acao_cadastro_produto'] == 1)? "selected='selected'" : ''?> value="1">salvar e voltar</option>
									<option <?=($_SESSION['acao_cadastro_produto'] == 2)? "selected='selected'" : ''?> value="2">salvar e continuar editando</option>
									<option <?=($_SESSION['acao_cadastro_produto'] == 3)? "selected='selected'" : ''?> value="3">salvar e novo</option>
								</select>
							</li>
							<li style="margin-top:0px">
								<input type="submit" style="margin: 0" class="btn_enviar" name="btn_enviar" id="btn_enviar" value="salvar" title="Salvar" />
							</li>
							<li style="margin-top:0px">
								<a href="index.php?p=produto&modo=camposespecificos" class="btn_cancel">cancelar</a>
							</li>
							
						</div>
						
						
					</ul>
					
				</div>
					
        	</div>
		</form>      
</div>

<script>

<?	if($rowDados['fldRelacionado'] == '0'){ ?>
	$('#field_relacionamento').hide(); //esconde o relacionamento
<?	} ?>

	/*** RELACIONAMENTO -> CHANGE OPERADOR ***/
	var width = 170 * <?=count($arrRelacionamento)?>;
	
	$("#adicionar_campos_relacionamento").click(function(){
		width = width + 140;
		if(width > 880){
			$("#relacionamento_scroll").css('width', width+'px');
		}

		campoOperador = '<select name="select_campo_relacionamento[]">';
		campoOperador = campoOperador+'<option selected="selected"></option>';
		campoOperador = campoOperador+'<option value="|s|+|s|">+</option>';
		campoOperador = campoOperador+'<option value="|s|-|s|">-</option>';
		campoOperador = campoOperador+'<option value="|s|*|s|">*</option>';
		campoOperador = campoOperador+'<option value="|s|/|s|">/</option></select>';

		divRelacionamento = $(".teste:first").clone(true).appendTo('#append_campo_relacionamento');

		novoCampo = $(".select_campo_relacionamento:first").clone();
		$('.teste:last').append(campoOperador+novoCampo.html());
		$(this).blur();
		$('.teste:last').find("select").val([]); //coloca o selected no primeiro (qe e em branco)
	})
	
	$(".teste").mouseenter(function(){
		$(this).find('.remover_relacionamento').css('display', 'block');
		$(".teste").css('background', 'transparent');
		$(this).css('background', '#EFDCDC');
	})
	
	$(".teste").mouseleave(function(){
		$(".teste").css('background', 'transparent');
		$(this).find('.remover_relacionamento').css('display', 'none');
	})
	
	$('.remover_relacionamento').click(function(){
		$(this).parent().remove();
		width = width - 140;
		if(width > 840){$("#relacionamento_scroll").css('width', width+'px');}
		else{$("#relacionamento_scroll").css('width','auto');}
	})
	
	//PRECISA SER SOMENTE NUMEROS E TIPO TEXTO PARA PODER RELACIONAR VALOR
	$("#sel_tipo_campo").change(function(){
		if($("#sel_tipo_campo").val() == 'check'){
			$("#sel_campo_relacionado").attr('disabled', 'disabled');
			$("#txt_tamanho_campo").val('');
			$("#txt_tamanho_campo").attr('disabled', 'disabled');
			$("#sel_campo_numerico").attr('disabled', 'disabled');
			$("#sel_campo_relacionado").find('option:first').attr('selected','selected');
			$("#sel_campo_numerico").find('option:first').attr('selected','selected');
			$('#field_relacionamento').hide(); //esconde o relacionamento
		}
		else{
			$("#sel_campo_numerico").attr('disabled', '');
			$("#txt_tamanho_campo").attr('disabled', '');
			if($("#sel_campo_numerico").val() == '1'){
				$("#sel_campo_relacionado").attr('disabled', '');
			}
			else
			{
				$("#sel_campo_relacionado").attr('disabled', 'disabled');
			}
		}
	})

	$("#sel_campo_numerico").change(function(){
		if($("#sel_campo_numerico").val() == '0'){
			$("#sel_campo_relacionado").find('option:first').attr('selected', 'selected');
			$("#sel_campo_relacionado").attr('disabled', 'disabled');
			$('#field_relacionamento').hide();
			$("#txt_tamanho_campo").attr('disabled', '');
		}
		else{
			if($("#sel_tipo_campo").val() != 'check'){
				$("#sel_campo_relacionado").attr('disabled', '');
			}
			else
			{
				$("#sel_campo_relacionado").find('option:first').attr('selected', 'selected');
				$("#sel_campo_relacionado").attr('disabled', 'disabled');
			}
		}
	})

	$("#sel_campo_relacionado").change(function(){
		if($('#sel_campo_relacionado').val() == '1'){
			$("#txt_tamanho_campo").val('');
			$("#txt_tamanho_campo").attr('disabled', 'disabled');
			$('#field_relacionamento').show();
		}else{
			$("#txt_tamanho_campo").attr('disabled', '');
			$('#field_relacionamento').hide();
		}
	})

	<?php
	if($tela_id == '3'){
	?>	
	/**************************
	 * RELACAO TIPOS E GRUPOS *
	 **************************/
	$("#sel_exibir_para").change(function(){
		if($(this).val()){ //se tiver selecionado algo
			$('#sel_grupo').load('camposespecificos_action.php?busca_grupos=ok&tela_id=<?=$tela_id;?>&tipo='+$(this).val());
			$("#gerenciar-grupos").attr('href', 'campos_especificos_grupos,<?=$tela_id?>,'+$(this).val());
		}
	})
	/**************************/
	<?php } ?>
	
	function SomenteNumero(e){
		var tecla=(window.event)?event.keyCode:e.which;
		if((tecla>47 && tecla<58)) return true;
		else{
			if(tecla==8 || tecla==0) return true;
			else  return false;
		}
	}
	
	function gerenciarGrupo(){
		$('#sel_grupo').find('option:first').attr('selected', 'selected');
		$('#gerenciar-grupos').trigger('click');
	}
	
	function validar(){
		var nome = $('#txt_nome_campo').val();
		if(nome == ''){
			alert('preencha o nome do campo!');
			return false;
		}
	}

</script>