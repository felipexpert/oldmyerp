<?	$documento_tipo =($_GET['doc_tipo'] == '1') ? 'pedido' : 'compra';	?>
<div id="voltar">
    <p><a href="index.php?p=<?=$documento_tipo?>">n&iacute;vel acima</a></p>
</div>	

<h2>Editar Nota Fiscal Eletr&ocirc;nica</h2>

<div id="principal">
<?
	$usuario_id 	= $_SESSION['usuario_id'];
	$documento_id 	= $_GET['id'];
	$impressao 		= fnc_sistema('sistema_impressao');
	$vendaDecimal 	= fnc_sistema('venda_casas_decimais');
	$qtdeDecimal 	= fnc_sistema('quantidade_casas_decimais');
	$servico_valor 	= fnc_sistema('venda_servico_valor');
	
	#BUSCA IMPRESSAO E CASO HAJA ALGUMA VARIACAO (EX A4 COM SERVICO)
	if(isset($impressao)){
		$rowImpressao 	= mysql_fetch_array(mysql_query("SELECT * FROM tblsistema_impressao WHERE fldId = $impressao"));
		$impressao 		= $rowImpressao['fldModelo'];
		if($rowImpressao['fldVariacao'] != ''){
			$impressao .= "_".$rowImpressao['fldVariacao'];
		}
	}
	
	if($documento_tipo == 'pedido'){
		$select_sql		= ', tblcliente_veiculo.fldAno, tblcliente_veiculo.fldChassi';
		$filtro_sql		= 'LEFT JOIN tblcliente_veiculo ON tblpedido.fldVeiculo_Id = tblcliente_veiculo.fldId';
	}
	
	$rsSQL = mysql_query("SELECT tbl{$documento_tipo}.*,tbl{$documento_tipo}.fldId as fldDocumento_Id, tblpedido_fiscal.*, tblnfe_natureza_operacao.fldEstoque, tblnfe_natureza_operacao.fldNatureza_Operacao,
							tblnfe_natureza_operacao.fldId as fldNatureza_Operacao_Id, tblnfe_natureza_operacao.fldParcelas {$select_sql}
							FROM tbl{$documento_tipo} 
							INNER JOIN tblpedido_fiscal ON tbl{$documento_tipo}.fldId = tblpedido_fiscal.fldpedido_id
							LEFT JOIN tblnfe_natureza_operacao ON tblnfe_natureza_operacao.fldId = tblpedido_fiscal.fldnatureza_operacao_id
							$filtro_sql	
							WHERE tbl{$documento_tipo}.fldId = '$documento_id'") or die(mysql_error());
							
	//CASO NAO EXISTA VENDA OU ESTEJA EXCLUIDA
	echo mysql_error();
	if(!$rsSQL){
?>		<div class="alert">
			<p class="erro">Houve um erro ao encontrar essa venda, ela pode ter sido excluida!<p>
        </div>
<?		die;
	}
	
	$rowNFe = mysql_fetch_array($rsSQL);
	#VERIFICAR SE NOTA FOI IMPORTADA DE VENDAS COMUNS ###############################################
	if($documento_tipo == 'pedido'){
		$sSQL 						= mysql_query("SELECT fldId FROM tblpedido WHERE fldPedido_Destino_Nfe_Id = $documento_id");
		$venda_origem 				= mysql_num_rows($sSQL);
	}
	#VERIFICAR SE EH UMA NFe DEVOLUCAO ##############################################################
	$nfe_devolucao				= ($rowNFe['fldDocumento_Devolucao_Id'] > 0)? 'true' : '';
	$nfe_parcelamento_exibir	= $rowNFe['fldParcelas'];
	$nfe_estoque_controlar 		= $rowNFe['fldEstoque'];
	$nfe_natureza_operacao_id 	= $rowNFe['fldNatureza_Operacao_Id'];
	$nfe_natureza_operacao	 	= $rowNFe['fldNatureza_Operacao'];
	$nfe_operacao_tipo			= $rowNFe['fldOperacao_Tipo'];
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		
		$nfe_estoque_controlar 	= $_POST['hid_estoque_controlar'];
		$referencia 			= $_POST['txt_referencia'];
		$destinatario_id		= $_POST['hid_cliente_id'];
		$dependente_id 			= ($_POST['hid_dependente_id'] > 0 ) ? $_POST['hid_dependente_id'] : "NULL" ;
		$conta_bancaria_id 		= $_POST['sel_conta_bancaria'];
		$veiculo_id				= $_POST['sel_veiculo']; //automotivo
		$veiculo_km				= $_POST['txt_veiculo_km']; //automotivo
		$desconto				= format_number_in($_POST['txt_pedido_desconto']);
		$deconto_reais 			= format_number_in($_POST['txt_pedido_desconto_reais']);
		$observacao 			= mysql_escape_string($_POST['txt_pedido_obs']);
		$data_emissao 			= format_date_in($_POST['txt_pedido_data']);
		//$entrega_data 			= format_date_in($_POST['txt_pedido_entrega']);
		$marcador				= $_POST['sel_marcador'];
		$cliente_pedido			= $_POST['txt_cliente_pedido'];
		$outros_servicos		= mysql_escape_string($_POST['txa_outros_servicos']);
		$servicos_terceiros		= $_POST['txt_pedido_terceiros'];
		$faturado				= ($_POST['chk_faturado'] == 'faturado') ? 1 : 0;
		
		$sql_update = fnc_nfe_update_registro($documento_tipo, $documento_id, $destinatario_id, $desconto, $desconto_reais, $observacao, $data_emissao, $marcador, $valor_terceiros, $faturado);
											
		//Aqui eu fiz uma array bidimensional com todos os itens da �tica para facilitar a organiza��o ---------------------------------------------------------------------------------------------------------
		$otica_valores = array(
			'perto' => array(
				'armacao' 			=>	$_POST['hid_pedido_otica_perto_armacao'],
				'material'			=>	$_POST['hid_pedido_otica_perto_material'],	
				'OD' => array(
					'esferico' 		 => $_POST['txt_pedido_otica_perto_od_esferico'],
					'cilindrico'	 => $_POST['txt_pedido_otica_perto_od_cilindrico'],
					'eixo' 			 => $_POST['txt_pedido_otica_perto_od_eixo'],
					'DP'			 => $_POST['txt_pedido_otica_perto_od_dp']
				),
				'OE' => array(
					'esferico' 		 => $_POST['txt_pedido_otica_perto_oe_esferico'],
					'cilindrico'	 => $_POST['txt_pedido_otica_perto_oe_cilindrico'],
					'eixo' 			 => $_POST['txt_pedido_otica_perto_oe_eixo'],
					'DP'			 => $_POST['txt_pedido_otica_perto_oe_dp']
				)
			),
			'longe' => array(
				'armacao' 			=>	$_POST['hid_pedido_otica_longe_armacao'],
				'material'			=>	$_POST['hid_pedido_otica_longe_material'],	
				'OD' => array(
					'esferico' 		 => $_POST['txt_pedido_otica_longe_od_esferico'],
					'cilindrico'	 => $_POST['txt_pedido_otica_longe_od_cilindrico'],
					'eixo' 			 => $_POST['txt_pedido_otica_longe_od_eixo'],
					'DP'			 => $_POST['txt_pedido_otica_longe_od_dp']
				),
				'OE' => array(
					'esferico' 		 => $_POST['txt_pedido_otica_longe_oe_esferico'],
					'cilindrico'	 => $_POST['txt_pedido_otica_longe_oe_cilindrico'],
					'eixo' 			 => $_POST['txt_pedido_otica_longe_oe_eixo'],
					'DP'			 => $_POST['txt_pedido_otica_longe_oe_dp']
				)
			),
			'bifocal' => array(
				'marca' 			=>	$_POST['sel_otica_marca'],
				'lente'				=>	$_POST['sel_otica_lente'],	
				'OD' => array(
					'altura'		=>	$_POST['txt_pedido_otica_bifocal_od_altura'],
					'DNP'			=>	$_POST['txt_pedido_otica_bifocal_od_dnp'],
					'adicao'		=>	$_POST['txt_pedido_otica_bifocal_od_adicao']
				),
				'OE' => array(
					'altura'		=>	$_POST['txt_pedido_otica_bifocal_oe_altura'],
					'DNP'			=>	$_POST['txt_pedido_otica_bifocal_oe_dnp'],
					'adicao'		=>	$_POST['txt_pedido_otica_bifocal_oe_adicao']
				)
			)
		);
		
		//UPDATE FISCAL DO PEDIDO *************************************************************************************************************************************************************************************************************************************************************************************************
		if($sql_update){
			$documento_tipo_id 					= ($documento_tipo == 'pedido' ? '1' : '2');
			$nfe_serie							= $_POST['txt_serie'];
			$nfe_numero							= $_POST['txt_pedido_numero'];
			$nfe_saida_data						= format_date_in($_POST['txt_pedido_saida_data']);
			$nfe_saida_hora						= $_POST['txt_pedido_saida_hora'];
			$nfe_operacao_tipo					= $_POST['sel_pedido_operacao_tipo'];
			//$nfe_operacao_natureza_id			= $_POST['sel_pedido_operacao_natureza'];
			$nfe_pagamento_forma_codigo			= $_POST['sel_pedido_nfe_pagamento_forma'];
				
			$nfe_total_produtos					= format_number_in($_POST['txt_pedido_nfe_total_produtos']);
			$nfe_total_bc_icms					= format_number_in($_POST['txt_pedido_nfe_total_bc_icms']);
			$nfe_total_icms						= format_number_in($_POST['txt_pedido_nfe_total_icms']);
			$nfe_total_bc_icms_substituicao		= format_number_in($_POST['txt_pedido_nfe_total_bc_icms_substituicao']);
			$nfe_total_icms_substituicao		= format_number_in($_POST['txt_pedido_nfe_total_icms_substituicao']);
			$nfe_total_frete					= format_number_in($_POST['txt_pedido_nfe_total_frete']);
			$nfe_total_seguro					= format_number_in($_POST['txt_pedido_nfe_total_seguro']);
			$nfe_total_desconto					= format_number_in($_POST['txt_pedido_nfe_total_desconto']);
			$nfe_total_valor_ipi				= format_number_in($_POST['txt_pedido_nfe_total_ipi']);
				
			$nfe_total_valor_ii					= format_number_in($_POST['txt_pedido_nfe_total_ii']);
			$nfe_total_valor_pis				= format_number_in($_POST['txt_pedido_nfe_total_pis']);
			$nfe_total_valor_cofins				= format_number_in($_POST['txt_pedido_nfe_total_cofins']);
			$nfe_out_despesas_acessorias		= format_number_in($_POST['txt_pedido_nfe_outras_despesas_acessorias']);
			$nfe_total_valor_nota				= format_number_in($_POST['txt_pedido_nfe_total_nfe']);
			$nfe_total_tributo					= format_number_in($_POST['txt_pedido_nfe_total_tributos']);
			
			$nfe_total_bc_iss					= format_number_in($_POST['txt_pedido_nfe_total_bc_iss']);
			$nfe_total_iss						= format_number_in($_POST['txt_pedido_nfe_total_iss']);
			$nfe_total_pis_servicos				= format_number_in($_POST['txt_pedido_nfe_total_pis_servicos']);
			$nfe_total_cofins_servicos			= format_number_in($_POST['txt_pedido_nfe_total_cofins_servicos']);
			$nfe_total_icms_nao_incidencia		= format_number_in($_POST['txt_pedido_nfe_total_servicos_nao_incidencia_icms']);
			
			$nfe_total_retido_pis				= format_number_in($_POST['txt_pedido_nfe_total_retido_pis']);
			$nfe_total_retido_cofins			= format_number_in($_POST['txt_pedido_nfe_total_retido_cofins']);
			$nfe_total_retido_csll				= format_number_in($_POST['txt_pedido_nfe_total_retido_csll']);
			$nfe_total_bc_irrf					= format_number_in($_POST['txt_pedido_nfe_total_bc_irrf']);
			$nfe_total_retido_irrf				= format_number_in($_POST['txt_pedido_nfe_total_retido_irrf']);
			$nfe_total_bc_retencao_prev_social	= format_number_in($_POST['txt_pedido_nfe_bc_retencao_prev_social']);
			$nfe_total_retencao_prev_social		= format_number_in($_POST['txt_pedido_nfe_total_retencao_prev_social']);
			
			$nfe_entrega_endereco				= $_POST['txt_pedido_nfe_entrega_endereco'];
			$nfe_entrega_numero					= $_POST['txt_pedido_nfe_entrega_numero'];
			$nfe_entrega_complemento			= $_POST['txt_pedido_nfe_entrega_complemento'];
			$nfe_entrega_bairro					= $_POST['txt_pedido_nfe_entrega_bairro'];
			$nfe_entrega_cep					= $_POST['txt_pedido_nfe_entrega_cep'];
			$nfe_entrega_municipio_codigo		= $_POST['txt_municipio_codigo_pedido_nfe'];
			$nfe_entrega_cpf_cnpj 				= $_POST['txt_pedido_nfe_entrega_cpf_cnpj'];
			$nfe_entrega_diferente_destinatario = $_POST['chk_local_entrega_diferente'];
			
			$nfe_transporte_modalidade			= $_POST['sel_pedido_nfe_transporte_modalidade'];
			$nfe_transportador					= $_POST['sel_pedido_nfe_transportador'];
			$nfe_transporte_icms_isento			= $_POST['chk_nfe_transporte_icms_isento'];
			
			$nfe_transporte_veiculo_placa		= $_POST['txt_pedido_nfe_transporte_veiculo_placa'];
			$nfe_transporte_veiculo_uf			= $_POST['sel_pedido_nfe_transporte_veiculo_uf'];
			$nfe_transporte_veiculo_rntc		= $_POST['txt_pedido_nfe_transporte_veiculo_rntc'];
			$nfe_transporte_balsa				= $_POST['txt_pedido_nfe_transporte_balsa_identificacao'];
			$nfe_transporte_vagao				= $_POST['txt_pedido_nfe_transporte_vagao_identificacao'];
			
			$nfe_retencao_bc					= format_number_in($_POST['txt_pedido_nfe_transporte_retencao_bc']);
			$nfe_retencao_aliquota				= format_number_in($_POST['txt_pedido_nfe_transporte_retencao_aliquota']);
			$nfe_retencao_valor_servico			= format_number_in($_POST['txt_pedido_nfe_transporte_retencao_valor_servico']);
			$nfe_retencao_municipio_codigo		= $_POST['sel_pedido_nfe_transporte_retencao_uf'].$_POST['sel_pedido_nfe_transporte_retencao_municipio'];
			$nfe_retencao_cfop					= $_POST['sel_pedido_nfe_transporte_retencao_cfop'];
			$nfe_retencao_icms_retido			= format_number_in($_POST['txt_pedido_nfe_transporte_retencao_icms_retido']);
			
			$nfe_info_fisco						= $_POST['txt_pedido_nfe_fisco'];
			$nfe_info_contribuinte				= $_POST['txt_pedido_nfe_contribuite'];
			
			$update_pedido_fiscal			 				= "UPDATE tblpedido_fiscal set
			fldsaida_data 									= '$nfe_saida_data',
			fldsaida_hora 									= '$nfe_saida_hora',
			fldOperacao_Tipo		 						= '$nfe_operacao_tipo',
			fldpagamento_forma_codigo 						= '$nfe_pagamento_forma_codigo',
			
			fldprodutos_servicos_total 						= '$nfe_total_produtos',
			fldicms_base_calculo 							= '$nfe_total_bc_icms',
			fldicms_total									= '$nfe_total_icms',
			fldicmsst_base_calculo							= '$nfe_total_bc_icms_substituicao',
			fldicmsst_total 								= '$nfe_total_icms_substituicao',
			fldfrete_total 									= '$nfe_total_frete',
			fldseguro_total 								= '$nfe_total_seguro',
			
			fldipi_total 									= '$nfe_total_valor_ipi',
			fldii_total 									= '$nfe_total_valor_ii',
			fldpis_total 									= '$nfe_total_valor_pis',
			fldcofins_total 								= '$nfe_total_valor_cofins',
			flddespesas_acessorias_total					= '$nfe_out_despesas_acessorias',
			flddesconto_total 								= '$nfe_total_desconto',
			fldnfe_total 									= '$nfe_total_valor_nota',
			fldnfe_total_tributos							= '$nfe_total_tributo',
			
			fldiss_base_calculo 							= '$nfe_total_bc_iss',
			fldiss_total 									= '$nfe_total_iss',
			fldpis_servicos_total 							= '$nfe_total_pis_servicos',
			fldcofins_servicos_total 						= '$nfe_total_cofins_servicos',
			fldicms_nao_incidencia_total				 	= '$nfe_total_icms_nao_incidencia',	
			
			fldpis_retido_total 							= '$nfe_total_retido_pis',
			fldcofins_retido_total 							= '$nfe_total_retido_cofins',
			fldcsll_retido_total 							= '$nfe_total_retido_csll',
			fldirrf_base_calculo 							= '$nfe_total_bc_irrf',
			fldirrf_retido_total 							= '$nfe_total_retido_irrf',
			fldretencao_prev_social_base_calculo 			= '$nfe_total_bc_retencao_prev_social',
			fldretencao_prev_social_total					= '$nfe_total_retencao_prev_social',
			
			fldentrega_endereco 							= '$nfe_entrega_endereco',
			fldentrega_numero								= '$nfe_entrega_numero',
			fldentrega_complemento 							= '$nfe_entrega_complemento',
			fldentrega_bairro 								= '$nfe_entrega_bairro',
			fldentrega_cep 									= '$nfe_entrega_cep',
			fldentrega_municipio_codigo 					= '$nfe_entrega_municipio_codigo',
			fldentrega_cpf_cnpj								= '$nfe_entrega_cpf_cnpj',
			fldentrega_diferente_destinatario				= '$nfe_entrega_diferente_destinatario',
			
			fldtransporte_modalidade_codigo 				= '$nfe_transporte_modalidade',
			fldtransporte_transportador_id			 		= '$nfe_transportador',
			fldtransporte_icms_isento						= '$nfe_transporte_icms_isento',
			
			fldtransporte_retencao_icms_base_calculo 		= '$nfe_retencao_bc',
			fldtransporte_retencao_icms_aliquota 			= '$nfe_retencao_aliquota',
			fldtransporte_retencao_icms_servico_valor 		= '$nfe_retencao_valor_servico',
			fldtransporte_retencao_icms_municipio_codigo	= '$nfe_retencao_municipio_codigo',
			fldtransporte_retencao_icms_cfop 				= '$nfe_retencao_cfop',
			fldtransporte_retencao_icms_retencao_valor		= '$nfe_retencao_icms_retido',
			
			fldtransporte_veiculo_placa 					= '$nfe_transporte_veiculo_placa',
			fldtransporte_veiculo_uf 						= '$nfe_transporte_veiculo_uf',
			fldtransporte_veiculo_rntc 						= '$nfe_transporte_veiculo_rntc',
			
			fldtransporte_balsa_identificacao 				= '$nfe_transporte_balsa',
			fldtransporte_vagao_identificacao 				= '$nfe_transporte_vagao',
			
			fldinfo_adicionais_fisco 						= '$nfe_info_fisco',
			fldinfo_adicionais_contribuinte				 	= '$nfe_info_contribuinte',
			fldnfe_status_id								= '0'
			WHERE fldPedido_Id = $documento_id AND fldDocumento_Tipo = $documento_tipo_id";
			
			mysql_query($update_pedido_fiscal);
			
			//INSERINDO REBOQUES ***********************************************************************************************************************************************************************************************************************************************************************************
			mysql_query("DELETE FROM tblpedido_fiscal_transporte_reboque 
								INNER JOIN tblpedido_fiscal ON tblpedido_fiscal.fldPedido_Id = tblpedido_fiscal_transporte_reboque.fldPedido_Id
								WHERE tblpedido_fiscal_transporte_reboque.fldPedido_Id = $documento_id  AND tblpedido_fical.fldDocumento_Tipo = $documento_tipo_id");
			$n= 1;
			$limite 	= $_POST["hid_reboque_controle"];
			while($n 	<= $limite){
				if($_POST['hid_reboque_controle'] > 0){
					
					$reboquePlaca 	= $_POST['txt_pedido_nfe_transporte_reboque_placa_'.$n];
					$reboqueUF	 	= $_POST['hid_pedido_nfe_transporte_reboque_uf_'.$n];
					$reboqueRNTC 	= $_POST['txt_pedido_nfe_transporte_reboque_rntc_'.$n];
					
					mysql_query("INSERT INTO tblpedido_fiscal_transporte_reboque
					(fldPedido_Id, fldPlaca, fldUF, fldRNTC)
					VALUES(
					'$documento_id',
					'$reboquePlaca',
					'$reboqueUF',
					'$reboqueRNTC'
					)");
				}
				$n++;
			}#end while
		
			//INSERINDO VOLUMES *************************************************************************************************************************************************************************************************************************************************************************************************
			mysql_query("DELETE FROM tblpedido_fiscal_transporte_volume WHERE fldPedido_Id = $documento_id");								
			$n= 1;
			$limite = $_POST["hid_volume_controle"];
			while($n <= $limite){
				if($_POST['hid_volume_controle'] > 0){
					
					$volumeQtd 			= format_number_in($_POST['txt_pedido_nfe_transporte_volume_qtd_'.$n]);
					$volumeEspecie 		= $_POST['txt_pedido_nfe_transporte_volume_especie_'.$n];
					$volumeMarca 		= $_POST['txt_pedido_nfe_transporte_volume_marca_'.$n];
					$volumeNummero 		= $_POST['txt_pedido_nfe_transporte_volume_numero_'.$n];
					$volumePesoLiq 		= format_number_in($_POST['txt_pedido_nfe_transporte_volume_peso_liquido_'.$n]);
					$volumePesoBruto 	= format_number_in($_POST['txt_pedido_nfe_transporte_volume_peso_bruto_'.$n]);
					$volumeLacres 		= $_POST['hid_pedido_nfe_transporte_volume_lacre_'.$n];
					
					mysql_query("INSERT INTO tblpedido_fiscal_transporte_volume
					(fldPedido_Id, fldQuantidade, fldEspecie, fldMarca, fldNumero, fldPeso_Liquido, fldPeso_Bruto)
					VALUES(
					'$documento_id',		'$volumeQtd',
					'$volumeEspecie',	'$volumeMarca',
					'$volumeNummero',	'$volumePesoLiq',
					'$volumePesoBruto'
					)") or mysql_error(die);
					echo mysql_error();
					
					$rsLastId 		= mysql_query("SELECT last_insert_id() as lastId");
					$LastId 		= mysql_fetch_array($rsLastId);
					$volumeId 		= $LastId['lastId'];
					
					$volumeLacres 	= substr($volumeLacres, 0 ,strlen($volumeLacres) - 1);
					$volumeLacres	= explode(',',$volumeLacres);
					foreach($volumeLacres as $lacre){
						
						mysql_query("INSERT INTO tblpedido_fiscal_transporte_volume_lacre
						(fldVolume_Id, fldLacre)
						VALUES('$volumeId', '$lacre')") 
						or die (mysql_error());
					}
				}
				$n++;
			}#wnd while
				
			//ATUALIZA COMISSAO DE FUNCIONARIO *************************************************************************************************************************************************************************************************************************************************************************************************
			if($venda_origem == 0 && $documento_tipo =='pedido'){
				#DELETA O QUE TIVER CADASTRADO E RECADASTRA
				mysql_query("DELETE FROM tblpedido_funcionario_servico WHERE fldPedido_Id = $documento_id") or die(mysql_error());
				for($x=1; $x <= 3; $x++){
					$funcionario_id = $_POST['sel_funcionario'.$x];
					if($funcionario_id> 0){
						$funcionario_servico	= mysql_escape_string($_POST['txa_funcionario'.$x.'_servico']);
						$funcionario_tempo		= format_number_in($_POST['txt_funcionario'.$x.'_tempo']);
						$funcionario_valor		= format_number_in($_POST['txt_funcionario'.$x.'_valor']);
						
						$rsComissao  			= mysql_query("SELECT fldFuncao2_Comissao FROM tblfuncionario WHERE fldId = $Funcionario_Id");
						$rowComissao 			= mysql_fetch_array($rsComissao);				
						$comissao				= $rowComissao['fldFuncao2_Comissao'];
						
						$insertServico			= "INSERT INTO tblpedido_funcionario_servico (fldPedido_Id, fldFuncionario_Id, fldServico, fldTempo, fldValor, fldComissao, fldFuncao_Tipo, fldOrdem)
													VALUES ($documento_id, $Funcionario_Id, '$funcionario_servico', '$funcionario_tempo', '$funcionario_valor', '$comissao', '2', $x)";
						mysql_query($insertServico) or mysql_error(die);
					}
				}
			}#endif $venda_origem
			
			#ATUALIZANDO ITENS *************************************************************************************************************************************************************************************************************************************************************************************************
			#EXCLUI TODOS OS DADOS FISCAIS DOS ITENS E INSERE DE NOVO
			mysql_query("DELETE tblpedido_item_fiscal
					FROM tbl{$documento_tipo}_item	
					INNER JOIN tbl{$documento_tipo} ON tbl{$documento_tipo}_item.fld{$documento_tipo}_Id = tbl{$documento_tipo}.fldId
					LEFT JOIN tblpedido_item_fiscal ON tblpedido_item_fiscal.fldItem_Id = tbl{$documento_tipo}_item.fldId
					WHERE tbl{$documento_tipo}_item.fld{$documento_tipo}_Id = $documento_id");
			$limite = $_POST["hid_controle"];
			$itens_id = '0,';
			
			for($n=1; $n <= $limite; $n++){
					
				$item_id								= $_POST['hid_pedido_item_id_'.$n];
				$produto_id 							= $_POST['hid_item_produto_id_'.$n];
				$item_quantidade	 					= format_number_in($_POST['txt_item_quantidade_'.$n]);
				$item_valor								= format_number_in($_POST['txt_item_valor_'.$n]);
				$item_desconto 							= format_number_in($_POST['txt_item_desconto_'.$n]);
				$item_nome 								= mysql_escape_string($_POST['txt_item_nome_'.$n]);
				$item_tabela_preco	 					= $_POST['hid_item_tabela_preco_'.$n];
				$item_fardo	 							= $_POST['hid_item_fardo_'.$n];
				$item_estoque_id						= $_POST['hid_item_estoque_id_'.$n];
				$item_referencia						= $_POST['txt_item_referencia_'.$n];
				$item_entregue							= ($_POST['chk_entregue_'.$n] == true)? '1' : '0';
				$item_entregue_data 					= "'" . date('Y-m-d') . "'";
				
				$NCM 		 							= $_POST['txt_nfe_pedido_item_principal_ncm_'.$n];
				$EXTIPI 	 							= $_POST['txt_nfe_pedido_item_principal_ex_tipi_'.$n];
				$CFOP 		 							= $_POST['txt_nfe_pedido_item_principal_cfop_'.$n];
				$UnidadeComercial 						= $_POST['txt_nfe_pedido_item_principal_unidade_comercial_'.$n];
				
				$QtdComercial 							= format_number_in($_POST['txt_item_quantidade_'.$n]); //ser� mesma que quantidade do item
				$ValorUnitComercial 					= format_number_in($_POST['txt_item_valor_'.$n]);//ser� mesma que valor do item
				$desconto			 					= format_number_in($_POST['txt_nfe_pedido_item_principal_desconto_'.$n]);//ser� mesma que valor do item
				$UnidadeTributavel 						= format_number_in($_POST['txt_nfe_pedido_item_principal_unidade_tributavel_'.$n]);
				$QtdTributavel 							= format_number_in($_POST['txt_nfe_pedido_item_principal_quantidade_tributavel_'.$n]);
				$ValorUnitTributavel 					= format_number_in($_POST['txt_nfe_pedido_item_principal_valor_unidade_tributavel_'.$n]);
				$TotalSeguro 							= format_number_in($_POST['txt_nfe_pedido_item_principal_total_seguro_'.$n]);
				
				$Frete 									= format_number_in($_POST['txt_nfe_pedido_item_principal_frete_'.$n]);
				$EAN 									= format_number_in($_POST['txt_nfe_pedido_item_principal_ean_'.$n]);
				$EANTributavel 							= format_number_in($_POST['txt_nfe_pedido_item_principal_ean_tributavel_'.$n]);
				$OutrasDespesasAcessorias		 		= format_number_in($_POST['txt_nfe_pedido_item_principal_outras_despesas_acessorias_'.$n]);
				$ValorTotalBruto 						= format_number_in($_POST['txt_nfe_pedido_item_principal_valor_total_bruto_'.$n]);
				$PedidoCompra 							= format_number_in($_POST['txt_nfe_pedido_item_principal_pedido_compra_'.$n]);
				$NumeroItemPedidoCompra	 				= format_number_in($_POST['txt_nfe_pedido_item_principal_numero_item_pedido_compra_'.$n]);
				$ValorTotalBrutoChk 					= ($_POST['chk_nfe_pedido_item_principal_valor_total_bruto_'.$n] == true)? '1' : '0';
				
				$InformacoesAdicionais					= $_POST['txt_nfe_item_pedido_informacoes_adicionais_'.$n];
				$radImposto								= $_POST['hid_nfe_tributo_rad_'.$n];
				
				$ICMSSituacaoTributaria					= $_POST['hid_nfe_pedido_item_tributos_icms_situacao_tributaria_'.$n];
				$ICMSOrigem 							= $_POST['hid_nfe_pedido_item_tributos_icms_origem_'.$n];
				$ICMSAliquotaAplicavelCalculoCredito	= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_pCredSN_'.$n]);
				$ICMSValorCreditoAproveitado			= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_vCredICMSSN_'.$n]);
			
				$ICMSModalidadeDeterminacaoBC 			= format_number_in($_POST['hid_nfe_pedido_item_tributos_icms_modBC_'.$n]);
				$ICMSValorBC				 			= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_vBC_'.$n]);
				$ICMSPorcentReducaoBC	 				= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_pRedBC_'.$n]);
				$ICMSAliquota 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_pICMS_'.$n]);
				$ICMSValor	 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_vICMS_'.$n]);
				$ICMSPorcentBCOperacaoPropria 			= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_pBCOp_'.$n]);
				$ICMSMotivoDesoneracao 					= $_POST['hid_nfe_pedido_item_tributos_icms_motDesICMS_'.$n];
				$ICMSSTModalidadeDetermicaoBC	 		= $_POST['hid_nfe_pedido_item_tributos_icms_st_modBCST_'.$n];
				$ICMSSTPorcentReducaoBC 				= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_st_pRedBCST_'.$n]);
				$ICMSSTPorcentMargemValorAdicional 		= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_st_pMVAST_'.$n]);
				$ICMSSTValorBC 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_st_vBCST_'.$n]);
				$ICMSSTAliquota 						= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_st_pICMSST_'.$n]);
				$ICMSSTValor 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_st_vICMSST_'.$n]);
					
				$ICMSSTUFDevido 						= $_POST['hid_nfe_pedido_item_tributos_icms_st_UFST_'.$n];
				
				$ICMSSTValorBCRetidoAnterior			= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_st_vBCSTRet_'.$n]);
				$ICMSSTValorRetidoAnterior 				= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_st_vICMSSTRet_'.$n]);
				$ICMSSTValorBCUFDestino					= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_st_vBCSTDest_'.$n]);
				$ICMSSTValorUFDestino					= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_st_vICMSSTDest_'.$n]);
				
				$COFINSSituacaoTributaria	 			= $_POST['hid_nfe_pedido_item_tributos_cofins_situacao_tributaria_'.$n];
				$COFINSTipoCalculo 						= $_POST['hid_nfe_pedido_item_tributos_cofins_calculo_tipo_'.$n];
				$COFINSValorBC 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_cofins_valor_base_calculo_'.$n]);
				$COFINSAliquotaPercentual 				= format_number_in($_POST['txt_nfe_pedido_item_tributos_cofins_aliquota_percentual_'.$n]);
				$COFINSAliquotaReais 					= format_number_in($_POST['txt_nfe_pedido_item_tributos_cofins_aliquota_reais_'.$n]);
				$COFINSQtdVendida 						= format_number_in($_POST['txt_nfe_pedido_item_tributos_cofins_quantidade_vendida_'.$n]);
				$COFINSValor 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_cofins_valor_'.$n]);
				
				$COFINSSTTipoCalculo 					= $_POST['hid_nfe_pedido_item_tributos_cofins_st_calculo_tipo_'.$n];
				$COFINSSTValorBC 						= format_number_in($_POST['txt_nfe_pedido_item_tributos_cofins_st_valor_base_calculo_'.$n]);
				$COFINSSTAliquotaPercentual 			= format_number_in($_POST['txt_nfe_pedido_item_tributos_cofins_st_aliquota_percentual_'.$n]);
				$COFINSSTAliquotaReais 					= format_number_in($_POST['txt_nfe_pedido_item_tributos_cofins_st_aliquota_reais_'.$n]);
				$COFINSSTQtdVendida 					= format_number_in($_POST['txt_nfe_pedido_item_tributos_cofins_st_quantidade_vendida_'.$n]);
				$COFINSSTValor 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_cofins_st_valor_'.$n]);
				
				$IIValorBC 								= format_number_in($_POST['txt_nfe_pedido_item_tributos_ii_valor_base_calculo_'.$n]);
				$IIValorDespesasAduaneiras 				= format_number_in($_POST['txt_nfe_pedido_item_tributos_ii_valor_aduaneiras_'.$n]);
				$IIValorIOF 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_ii_valor_iof_'.$n]);
				$IIValor 								= format_number_in($_POST['txt_nfe_pedido_item_tributos_ii_valor_'.$n]);
				
				$IPISituacaoTributaria					= $_POST['hid_nfe_pedido_item_tributos_ipi_situacao_tributaria_'.$n];
				$IPIClasseEnquadramento 				= $_POST['txt_nfe_pedido_item_tributos_ipi_enquadramento_classe_'.$n];
				$IPICodigoEnquadramentoLegal			= $_POST['txt_nfe_pedido_item_tributos_ipi_enquadramento_codigo_'.$n];
				$IPICNPJProdutor						= $_POST['txt_nfe_pedido_item_tributos_ipi_produtor_cnpj_'.$n];
				$IPICodigoSeloControle 					= $_POST['txt_nfe_pedido_item_tributos_ipi_selo_controle_codigo_'.$n];
				$IPIQtdSeloControle 					= $_POST['txt_nfe_pedido_item_tributos_ipi_selo_controle_quantidade_'.$n];
				$IPITipoCalculo 						= $_POST['hid_nfe_pedido_item_tributos_ipi_calculo_tipo_'.$n];
				$IPIValorBC 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_ipi_valor_base_calculo_'.$n]);
				$IPIAliquota 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_ipi_aliquota_'.$n]);
				$IPIQtdTotalUnidadePadrao				= format_number_in($_POST['txt_nfe_pedido_item_tributos_ipi_unidade_padrao_quantidade_total_'.$n]);
				$IPIValorUnidade 						= format_number_in($_POST['txt_nfe_pedido_item_tributos_ipi_unidade_valor_'.$n]);
				$IPIValor 								= format_number_in($_POST['txt_nfe_pedido_item_tributos_ipi_valor_'.$n]);
				
				$ISSQNTributacao 						= $_POST['hid_nfe_pedido_item_tributos_issqn_tributacao_'.$n];
				$ISSQNValorBC 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_issqn_valor_base_calculo_'.$n]);
				$ISSQNAliquota 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_issqn_aliquota_'.$n]);
				$ISSQNListaServico 						= $_POST['hid_nfe_pedido_item_tributos_issqn_servico_lista_'.$n];
				$ISSQNUF 								= $_POST['hid_nfe_pedido_item_tributos_issqn_uf_'.$n];
				$ISSQNMunicipioOcorrencia 				= $_POST['hid_nfe_pedido_item_tributos_issqn_municipio_'.$n];
				$ISSQNValor 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_issqn_valor_issqn_'.$n]);
						
				$PISSituacaoTributaria	 				= $_POST['hid_nfe_pedido_item_tributos_pis_situacao_tributaria_'.$n];
				$PISTipoCalculo 						= $_POST['hid_nfe_pedido_item_tributos_pis_calculo_tipo_'.$n];
				$PISValorBC 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_pis_valor_base_calculo_'.$n]);
				$PISAliquotaPercentual 					= format_number_in($_POST['txt_nfe_pedido_item_tributos_pis_aliquota_percentual_'.$n]);
				$PISAliquotaReais 						= format_number_in($_POST['txt_nfe_pedido_item_tributos_pis_aliquota_reais_'.$n]);
				$PISQtdVendida 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_pis_quantidade_vendida_'.$n]);
				$PISValor 								= format_number_in($_POST['txt_nfe_pedido_item_tributos_pis_valor_'.$n]);
				$PISSTTipoCalculo 						= $_POST['hid_nfe_pedido_item_tributos_pis_st_calculo_tipo_'.$n];
				$PISSTValorBC 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_pis_st_valor_base_calculo_'.$n]);
				$PISSTAliquotaPercentual 				= format_number_in($_POST['txt_nfe_pedido_item_tributos_pis_st_aliquota_percentual_'.$n]);
				$PISSTAliquotaReais 					= format_number_in($_POST['txt_nfe_pedido_item_tributos_pis_st_aliquota_reais_'.$n]);
				$PISSTQtdVendida 						= format_number_in($_POST['txt_nfe_pedido_item_tributos_pis_st_quantidade_vendida_'.$n]);
				$PISSTValor 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_pis_st_valor_'.$n]);

				$Tipo_Especifico 						= $_POST['txt_nfe_item_produto_especifico_id_'.$n];
				$Combustivel_Cod_ANP 					= $_POST['txt_nfe_item_produto_especifico_combustivel_anp_'.$n];
				$Combustivel_Cod_IF 					= $_POST['txt_nfe_item_produto_especifico_combustivel_if_'.$n];
				$Combustivel_UF 						= $_POST['txt_nfe_item_produto_especifico_combustivel_uf_'.$n];
				$Combustivel_Qtd_Faturada 				= $_POST['txt_nfe_item_produto_especifico_combustivel_qtd_faturada_'.$n];
				$Combustivel_Cide_bCalculo 				= $_POST['txt_nfe_item_produto_especifico_combustivel_cide_bc_'.$n];
				$Combustivel_Cide_Aliquota 				= $_POST['txt_nfe_item_produto_especifico_combustivel_cide_aliquota_'.$n];
				$Combustivel_Cide_Valor 				= $_POST['txt_nfe_item_produto_especifico_combustivel_cide_valor_'.$n];
				
				$Total_Tributo							= format_number_in($_POST['txt_nfe_item_produto_especifico_total_tributos_'.$n]);
				
				if(isset($produto_id)){
					#ATUALIZANDO DADOS DO ITEM
					if(isset($item_id)){
						$rowItem 			= mysql_fetch_array(mysql_query("SELECT fldQuantidade FROM tbl{$documento_tipo}_item WHERE fldId = $item_id"));
						$quantidade_bd 		= $rowItem['fldQuantidade'];
						
						##SE NFE FOR CONTROLAR ESTOQUE (EXCETO DEVOLUCAO, GARANTIA...)
						if($nfe_estoque_controlar == '1'){
							## SE VENDA OU COMPRA
							if($documento_tipo == 'pedido'){
								if($quantidade_bd > $item_quantidade){
									$entrada = $quantidade_bd - $item_quantidade;
									fnc_estoque_movimento_lancar($produto_id, '', $entrada, '', 11, $documento_id, $item_estoque_id, $item_estoque_id, '', $item_id);
								}elseif($quantidade_bd < $item_quantidade){
									$saida = $item_quantidade - $quantidade_bd;
									fnc_estoque_movimento_lancar($produto_id, '', '', $saida, 1, $documento_id, $item_estoque_id, $item_estoque_id, '', $item_id);
								}
							}elseif($documento_tipo == 'compra'){
								if($quantidade_bd > $item_quantidade){
									$saida = $item_quantidade - $quantidade_bd;
									fnc_estoque_movimento_lancar($produto_id, '', '', $saida, 1, $documento_id, $item_estoque_id, $item_estoque_id, '', $item_id);
								}elseif($quantidade_bd < $item_quantidade){
									$entrada = $quantidade_bd - $item_quantidade;
									fnc_estoque_movimento_lancar($produto_id, '', $entrada, '', 11, $documento_id, $item_estoque_id, $item_estoque_id, '', $item_id);
								}
							}
						}
						
						$sql = "UPDATE tbl{$documento_tipo}_item SET
							fldQuantidade	= '$item_quantidade',
							fldValor		= '$item_valor',
							fldDesconto		= '$item_desconto',
							fldDescricao	= '$item_nome'
							WHERE fldId 	= $item_id";
							
						if(!mysql_query($sql)){ echo mysql_error(); die;}
					}else{
					
						$rsDadosProduto		= mysql_query("SELECT fldEstoque_Controle, fldTipo_Id, fldValorCompra FROM tblproduto WHERE fldId = $produto_id ");
						$rowDadosProduto 	= mysql_fetch_array($rsDadosProduto);
						$valor_compra 		= $rowDadosProduto['fldValorCompra'];
						$estoque_controle	= $rowDadosProduto['fldEstoque_Controle'];
						$tipo_id 			= $rowDadosProduto['fldTipo_Id'];
						
						
						$item_id = fnc_nfe_inserir_item($documento_tipo, $documento_id, $produto_id, $item_quantidade, $item_nome, $item_valor, $item_desconto, $item_estoque_id,
									$valor_compra, $item_fardo, $item_tabela_preco,  $tipo_id, $item_entregue, $item_entregue_data);
			
						if($nfe_estoque_controlar == '1'){
							fnc_estoque_movimento_lancar($produto_id, '', '', $item_quantidade, 1, $documento_id, $item_estoque_id, $item_estoque_id, '', $item_id);
						}
					
						if($documento_tipo == 'pedido'){
							//insiro os componentes.
							############################################################################################################################################################################################					
							//agora tem 2 tipos de relacionamento, pesquiso por um, dps por outro!
							//primeiro o comum
							$sqlComponente = mysql_query("SELECT tblproduto_componente.* FROM tblproduto_componente WHERE fldProduto_Id = $produto_id");
							if(mysql_num_rows($sqlComponente) > 0){ //normal
								while($rowComponente = mysql_fetch_assoc($sqlComponente)){
									//insere todos os componentes, al�m de mexer no estoque!
									$componente_id		= $rowComponente['fldProduto_Componente_Id'];
									$componente_qtd		= $rowComponente['fldComponente_Qtd'] * $item_quantidade;
		
									//insert na tblpedido_item_componente
									mysql_query("INSERT INTO tblpedido_item_componente
												(fldProduto_Id, fldItem_Id, fldPedido_Id, fldComponente_Id, fldQtd, fldExcluido)
												VALUES ('$produto_id', '$item_id', '$documento_id', '$componente_id', '$componente_qtd', '0')") or die(mysql_error());
										
									if($nfe_estoque_controlar == '1'){ fnc_estoque_movimento_lancar($componente_id, '', '', $componente_qtd, 12, $documento_id, $item_estoque_id, $item_estoque_id, '', $item_id); }
								}
							}
							
							##############################################################################################################################################################################################
							//agora inverso
							$sqlComponente = mysql_query("SELECT tblproduto_componente.* FROM tblproduto_componente WHERE fldProduto_Componente_Id = $produto_id AND fldRelacionamento = 2");
							if(mysql_num_rows($sqlComponente) > 0){ //inverso
								while($rowComponente = mysql_fetch_assoc($sqlComponente)){
									//insere todos os componentes, al�m de mexer no estoque!
									$componente_id		= $rowComponente['fldProduto_Id'];
									$componente_qtd		= $rowComponente['fldQtd_Proporcional'] * $item_quantidade;
		
									//insert na tblpedido_item_componente
									mysql_query("INSERT INTO tblpedido_item_componente
												(fldProduto_Id, fldItem_Id, fldPedido_Id, fldComponente_Id, fldQtd, fldExcluido)
												VALUES ('$produto_id', '$item_id', '$documento_id', '$componente_id', '$componente_qtd', '0')") or die(mysql_error());
										
									if($nfe_estoque_controlar == '1'){ fnc_estoque_movimento_lancar($componente_id, '', '', $componente_qtd, 12, $documento_id, $item_estoque_id, $item_estoque_id, '', $item_id); }
								}
							}
						}#end if pedido
					}#end $item_id
					
					if($otica_valores['perto']['armacao'] == $n){
						$Otica_Perto_Armacao = $item_id;
					}
					if($otica_valores['perto']['material'] == $n){
						$Otica_Perto_Material = $item_id;
					}
					if($otica_valores['longe']['armacao'] == $n){
						$Otica_Longe_Armacao = $item_id;
					}
					if($otica_valores['longe']['material'] == $n){
						$Otica_Longe_Material = $item_id;
					}
				
					$sqlInsert_Item_Fiscal =
					"INSERT INTO tblpedido_item_fiscal (
						fldItem_Id,
						fldncm,
						fldex_tipi,
						fldcfop,
						fldunidade_comercial,
						fldquantidade_comercial,
						fldvalor_unitario_comercial,
						fldunidade_tributavel,
						fldquantidade_tributavel,
						fldvalor_unitario_tributavel,
						fldtotal_seguro,
						fldoutras_despesas_acessorias,
						fldfrete,
						flddesconto,
						fldean,
						fldean_unidade_tributavel,
						fldvalor_total_bruto,
						fldpedido_compra,
						fldnumero_item_pedido_compra,
						fldvalor_bruto_compoe_total,
						fldinformacoes_adicionais,
						fldrad_imposto,
						
						fldgenero,
						fldicms_origem,
						fldicms_situacao_tributaria,
						fldicms_base_calculo_modalidade,
						fldicms_calculo_credito_aliquota_aplicavel,
						fldicms_credito_aproveitado,
						fldicms_base_calculo_percentual_reducao,
						fldicms_aliquota,
						fldicms_base_calculo,
						fldicms_valor,
						fldicms_base_calculo_operacao_propria,
						fldicms_desoneracao_motivo,
						
						fldicmsst_base_calculo_modalidade,
						fldicmsst_base_calculo_percentual_reducao,
						fldicmsst_mva,
						fldicmsst_aliquota,
						fldicmsst_valor,
						fldicmsst_base_calculo,
						fldicmsst_base_calculo_retido_uf_remetente,
						fldicmsst_valor_retido_uf_remetente,
						fldicmsst_base_calculo_uf_destino,
						fldicmsst_valor_uf_destino,
						fldicmsst_uf,
						
						fldpis_situacao_tributaria,
						fldpis_tipo_calculo,
						fldpis_base_calculo,
						fldpis_aliquota_percentual,
						fldpis_aliquota_reais,
						fldpis_quantidade_vendida,
						fldpis_valor,
						fldpisst_tipo_calculo,
						fldpisst_base_calculo,
						fldpisst_aliquota_percentual,
						fldpisst_aliquota_reais,
						fldpisst_quantidade_vendida,
						fldpisst_valor,
						
						fldipi_situacao_tributaria,
						fldipi_enquadramento_classe,
						fldipi_enquadramento_codigo,
						fldipi_produtor_cnpj,
						fldipi_calculo_tipo,
						fldipi_aliquota,
						fldipi_unidade_quantidade_total,
						fldipi_unidade_valor,
						fldipi_selo_controle_codigo,
						fldipi_selo_controle_quantidade,
						fldipi_base_calculo,
						fldipi_valor,
						
						fldcofins_situacao_tributaria,
						fldcofins_tipo_calculo,
						fldcofins_base_calculo,
						fldcofins_aliquota_percentual,
						fldcofins_aliquota_reais,
						fldcofins_quantidade_vendida,
						fldcofins_valor,
						fldcofinsst_tipo_calculo,
						fldcofinsst_base_calculo,
						fldcofinsst_aliquota_percentual,
						fldcofinsst_aliquota_reais,
						fldcofinsst_quantidade_vendida,
						fldcofinsst_valor,
						
						fldii_base_calculo,
						fldii_despesas_aduaneiras_valor,
						fldii_iof_valor,
						fldii_valor,
						
						fldissqn_tributacao,
						fldissqn_base_calculo,
						fldissqn_aliquota,
						fldissqn_lista_servico,
						fldissqn_uf,
						fldissqn_municipio_ocorrencia,
						fldissqn_valor,

						fldTipo_Especifico,
						fldCombustivel_Codigo_ANP,
						fldCombustivel_Codigo_CodIF,
						fldCombustivel_UF,
						fldCombustivel_Qtd_Faturada,
						fldCombustivel_Cide_bCalculo,
						fldCombustivel_Cide_Aliquota,
						fldCombustivel_Cide_Valor,
						
						fldTotal_Tributo

					)
					VALUES(
						'$item_id',
						'$NCM',
						'$EXTIPI',
						'$CFOP',
						'$UnidadeComercial',
						'$QtdComercial',
						'$ValorUnitComercial',
						'$UnidadeTributavel',
						'$QtdTributavel',
						'$ValorUnitTributavel',
						'$TotalSeguro',
						'$OutrasDespesasAcessorias',
						'$Frete',
						'$desconto',
						'$EAN',
						'$EANTributavel',
						'$ValorTotalBruto',
						'$PedidoCompra',
						'$NumeroItemPedidoCompra',
						'$ValorTotalBrutoChk',
						'$InformacoesAdicionais',
						'$radImposto',
						
						'$genero',
						'$ICMSOrigem',
						'$ICMSSituacaoTributaria',
						'$ICMSModalidadeDeterminacaoBC',
						'$ICMSAliquotaAplicavelCalculoCredito',
						'$ICMSValorCreditoAproveitado',							
						'$ICMSPorcentReducaoBC',
						'$ICMSAliquota',
						'$ICMSValorBC',
						'$ICMSValor',
						'$ICMSPorcentBCOperacaoPropria',
						'$ICMSMotivoDesoneracao',
						
						'$ICMSSTModalidadeDetermicaoBC',
						'$ICMSSTPorcentReducaoBC',
						'$ICMSSTPorcentMargemValorAdicional',
						'$ICMSSTAliquota',
						'$ICMSSTValor',
						
						'$ICMSSTValorBC',
						'$ICMSSTValorBCRetidoAnterior',
						'$ICMSSTValorRetidoAnterior',
						'$ICMSSTValorBCUFDestino',
						'$ICMSSTValorUFDestino',
						'$ICMSSTUFDevido',
						'$PISSituacaoTributaria',
						'$PISTipoCalculo',
						'$PISValorBC',
						'$PISAliquotaPercentual',
						'$PISAliquotaReais',
						'$PISQtdVendida',
						'$PISValor',
						'$PISSTTipoCalculo',
						'$PISSTValorBC',
						'$PISSTAliquotaPercentual',
						'$PISSTAliquotaReais',
						'$PISSTQtdVendida',
						'$PISSTValor',
						'$IPISituacaoTributaria',
						'$IPIClasseEnquadramento',
						'$IPICodigoEnquadramentoLegal',
						'$IPICNPJProdutor',
						'$IPITipoCalculo',
						'$IPIAliquota',
						'$IPIQtdTotalUnidadePadrao',
						'$IPIValorUnidade',
						'$IPICodigoSeloControle',
						'$IPIQtdSeloControle',
						'$IPIValorBC',
						'$IPIValor',
						'$COFINSSituacaoTributaria',
						'$COFINSTipoCalculo',
						'$COFINSValorBC',
						'$COFINSAliquotaPercentual',
						'$COFINSAliquotaReais',
						'$COFINSQtdVendida',
						'$COFINSValor',
						'$COFINSSTTipoCalculo',
						'$COFINSSTValorBC',
						'$COFINSSTAliquotaPercentual',
						'$COFINSSTAliquotaReais',
						'$COFINSSTQtdVendida',
						'$COFINSSTValor',
						'$IIValorBC',
						'$IIValorDespesasAduaneiras',
						'$IIValorIOF',
						'$IIValor',
						'$ISSQNTributacao',
						'$ISSQNValorBC',
						'$ISSQNAliquota',
						'$ISSQNListaServico',
						'$ISSQNUF',
						'$ISSQNMunicipioOcorrencia',
						'$ISSQNValor',
						'$Tipo_Especifico',
						'$Combustivel_Cod_ANP',
						'$Combustivel_Cod_IF',
						'$Combustivel_UF',
						'$Combustivel_Qtd_Faturada',
						'$Combustivel_Cide_bCalculo',
						'$Combustivel_Cide_Aliquota',
						'$Combustivel_Cide_Valor',
						'$Total_Tributo'
					)";
					
					if($venda_origem == 0 && $documento_tipo =='pedido'){
						if(fnc_sistema('pedido_comissao') == '2'){
							$item_valor_comissao 	= $item_valor * $item_quantidade;
							$func_id				= $_POST['txt_funcionario_codigo'];
							$rsDadosComissao 		= mysql_query("SELECT * FROM tblproduto_comissao WHERE fldProduto_Id = $produto_id AND fldFuncionario_Id = $func_id");
							$count_comissao			= mysql_num_rows($rsDadosComissao);
						
							if($count_comissao > 0){
								$rowComissao_Produto 	= mysql_fetch_assoc($rsDadosComissao);
								$comissao_porc 			= ($rowComissao_Produto['fldComissao_Tipo'] == '2') ? $rowComissao_Produto['fldComissao_Valor'] : '';
								$comissao_reais			= ($rowComissao_Produto['fldComissao_Tipo'] == '1') ? $rowComissao_Produto['fldComissao_Valor'] : '';
						
								mysql_query("INSERT INTO tblpedido_funcionario_servico 
														  (fldPedido_Id, fldFuncionario_Id, fldServico, fldTempo, fldValor, fldComissao, fldComissao_Reais, fldFuncao_Tipo)
														  VALUES ('$documento_id', '$func_id', '', '', '$item_valor_comissao', '$comissao_porc', '$comissao_reais', '1')") or die(mysql_error());	
							}
						}
					}
	
					mysql_query($sqlInsert_Item_Fiscal);
					#AGRUPA OS IDS DE ITENS PRA CONFERIR SE HOUVE EXCLUS�O LOGO ABAIXO
					$itens_id 		.= $item_id.','; 
					$sub_total		 = $item_valor * $item_quantidade;
					$total_desconto	 = ($item_desconto / 100) * $sub_total;
					$total_item		+= $sub_total - $total_desconto;
				}
			}
			echo mysql_error();
			 
			//INSERIR FUNCIONARIO DE VENDA - TOTAL DE ITENS PARA ARMAZENAR JUNTO
			if($venda_origem == 0 && $documento_tipo =='pedido'){
				if(fnc_sistema('pedido_comissao') != '2'){
					$funcionario_id	= $_POST['txt_funcionario_codigo'];
					if($funcionario_id > 0 ){
						$rsComissao  			= mysql_query("SELECT fldFuncao1_Comissao FROM tblfuncionario WHERE fldId = '$funcionario_id'");
						$rowComissao 			= mysql_fetch_array($rsComissao);				
						$comissao				= $rowComissao['fldFuncao1_Comissao'];
						$funcionario_valor		= $total_item;
						
						$insertServico			= "INSERT INTO tblpedido_funcionario_servico (fldPedido_Id, fldFuncionario_Id, fldValor, fldComissao, fldFuncao_Tipo)
													VALUES ('$documento_id', '$funcionario_id', '$funcionario_valor', '$comissao', '1')";
						mysql_query($insertServico);
					}
				}
			}
			
			#CONFIRO SE AINDA EXISTE NO BANCO ALGUM ITEM DESSA VENDA, MAS QUE NAO EXISTE NA LISTAGEM => ITEM EXCLUIDO NA EDI��O
			$itens_id 	 =  substr($itens_id, 0, strlen($itens_id) -1);
			#CASO TENHA EXCLUIDO ALGUM ITEM NA LISTAGEM, DELETA NA tblitem E REPOE O ESTOQUE SE ESTIVER CONTROLANDO
			$rsExcluidos = mysql_query("SELECT * FROM tbl{$documento_tipo}_item WHERE fldId NOT IN ($itens_id) AND fld{$documento_tipo}_Id = $documento_id");
			while($rowExcluidos  = mysql_fetch_array($rsExcluidos)){
				$item_id		 = $rowExcluidos['fldId'];
				$item_quantidade = $rowExcluidos['fldQuantidade'];
				$item_estoque_id = $rowExcluidos['fldEstoque_Id'];
				$produto_id 	 = $rowExcluidos['fldProduto_Id'];
				
				mysql_query("DELETE FROM tbl{$documento_tipo}_item WHERE fldId = ".$rowExcluidos['fldId']);
				#SE CONTROLA ESTOQUE DE ACORDO COM NATUREZA OP
				if($nfe_estoque_controlar == '1'){
					#REPOE ESTOQUE DO ITEM
					fnc_estoque_movimento_lancar($produto_id, '', $item_quantidade,'', 11, $documento_id, $item_estoque_id, $item_estoque_id, '', $item_id);	
				}
				
				if($documento_tipo == 'pedido'){
					#DELETA OS COMPONENTES DOS PRODUTOS EXCLUIDOS 
					$rowComponente_Excluido = mysql_query("SELECT * FROM tblpedido_item_componente WHERE fldItem_Id = $item_id");
					if(mysql_num_rows($rowComponente_Excluido)){
						$rowComponente_Excluido = mysql_fetch_assoc($rowComponente_Excluido);
						$componente_id 			= $rowComponente_Excluido['fldComponente_Id'];
						$componente_qtd 		= $rowComponente_Excluido['fldQtd'];
						if($nfe_estoque_controlar == '1'){ fnc_estoque_movimento_lancar($componente_id, '', $componente_qtd, '', 13, $documento_id, $item_estoque_id, $item_estoque_id, '', $item_id); }
					}
				}
			}#end while
		
			#INSERCAO DOS DADOS RECEBIDOS DA VENDA (OTICA)
			if($_SESSION["sistema_tipo"] == "otica" ){
				//excluir todos os parametros de otica antigos
				mysql_query("DELETE FROM tblpedido_otica WHERE fldPedido_Id = $documento_id");
			
				$inserir_dados_otica = mysql_query("
				INSERT INTO tblpedido_otica (
				fldPedido_Id,				fldResponsavel,
				fldPerto_Armacao_Item_Id,	fldPerto_Material_Item_Id,
				fldPerto_OD_Esferico,		fldPerto_OD_Cilindro,
				fldPerto_OD_Eixo,			fldPerto_OD_DP,
				fldPerto_OE_Esferico,		fldPerto_OE_Cilindro,
				fldPerto_OE_Eixo,			fldPerto_OE_DP,
				fldLonge_Armacao_Item_Id,	fldLonge_Material_Item_Id,
				fldLonge_OD_Esferico,		fldLonge_OD_Cilindro,
				fldLonge_OD_Eixo,			fldLonge_OD_DP,
				fldLonge_OE_Esferico,		fldLonge_OE_Cilindro,
				fldLonge_OE_Eixo,			fldLonge_OE_DP,
				fldBifocal_Marca_Id,		fldBifocal_Lente_Id,
				fldBifocal_OD_Altura,		fldBifocal_OD_DNP,
				fldBifocal_OD_Adicao,		fldBifocal_OE_Altura,
				fldBifocal_OE_DNP,			fldBifocal_OE_Adicao,
				fldMedico,					fldMedico_crm
				)VALUES (
				'$documento_id',
				'".$_POST['sel_cliente_responsavel']."',
				'".$Otica_Perto_Armacao."',
				'".$Otica_Perto_Material."',
				'".$otica_valores['perto']['OD']['esferico']."',
				'".$otica_valores['perto']['OD']['cilindrico']."',
				'".$otica_valores['perto']['OD']['eixo']."',
				'".$otica_valores['perto']['OD']['DP']."',
				'".$otica_valores['perto']['OE']['esferico']."',
				'".$otica_valores['perto']['OE']['cilindrico']."',
				'".$otica_valores['perto']['OE']['eixo']."',
				'".$otica_valores['perto']['OE']['DP']."',
				'".$Otica_Longe_Armacao."',
				'".$Otica_Longe_Material."',
				'".$otica_valores['longe']['OD']['esferico']."',
				'".$otica_valores['longe']['OD']['cilindrico']."',
				'".$otica_valores['longe']['OD']['eixo']."',
				'".$otica_valores['longe']['OD']['DP']."',
				'".$otica_valores['longe']['OE']['esferico']."',
				'".$otica_valores['longe']['OE']['cilindrico']."',
				'".$otica_valores['longe']['OE']['eixo']."',
				'".$otica_valores['longe']['OE']['DP']."',
				'".$otica_valores['bifocal']['marca']."',
				'".$otica_valores['bifocal']['lente']."',
				'".$otica_valores['bifocal']['OD']['altura']."',
				'".$otica_valores['bifocal']['OD']['DNP']."',
				'".$otica_valores['bifocal']['OD']['adicao']."',
				'".$otica_valores['bifocal']['OE']['altura']."',
				'".$otica_valores['bifocal']['OE']['DNP']."',
				'".$otica_valores['bifocal']['OE']['adicao']."',
				'".$_POST['txt_pedido_otica_medico']."',
				'".$_POST['txt_pedido_otica_medico_crm']."'
				)") or die (mysql_error());
			}#end if otica
			
			echo mysql_error();
			#ATUALIZANDO PARCELAS CASO CONTROLANDO DE ACORDO COM NATUREZA OP
			if($nfe_parcelamento_exibir == '1'){
				
				#CHEGANDO SE TEM PARCELAS PAGAS
				if(isset($_POST['hid_parcelas_pagas_ids'])) $deleteSQL = "DELETE FROM tbl{$documento_tipo}_parcela WHERE fld{$documento_tipo}_Id = $documento_id AND fldId NOT IN(". $_POST['hid_parcelas_pagas_ids'] .")";
				else $deleteSQL = "DELETE FROM tbl{$documento_tipo}_parcela WHERE fld{$documento_tipo}_Id = $documento_id";
				
				mysql_query($deleteSQL);
					
				$y = 1;
				$limite = $_POST["hid_controle_parcela"];
				while($y <= $limite){
					if(isset($_POST['txt_parcela_numero_'.$y])){
						$parcela_numero 	= $_POST['txt_parcela_numero_'.$y];
						$parcela_vencimento = format_date_in($_POST['txt_parcela_data_'.$y]);
						$parcela_valor 		= format_number_in($_POST['txt_parcela_valor_'.$y]);
						$pagamento_tipo 	= $_POST['sel_pagamento_tipo_'.$y];
						$parcela_obs		= $_POST['txt_pedido_obs'];
						
						mysql_query ("INSERT INTO tbl{$documento_tipo}_parcela
						(fld{$documento_tipo}_Id, fldParcela, fldVencimento, fldValor, fldPagamento_Id, fldObservacao, fldStatus)
						VALUES(
						'$documento_id', '$parcela_numero', '$parcela_vencimento',	'$parcela_valor', '$pagamento_tipo', '$parcela_obs', '1'
						)");
						
						$LastId = mysql_fetch_array(mysql_query("SELECT last_insert_id() as lastID "));
						$Data 	= ("Y-m-d");
						//ACOES DE PAGAMENTO CONFORME TIPO 
						$rowPagamentoTipo = mysql_fetch_array(mysql_query("SELECT * FROM tblpagamento_tipo WHERE fldId = $pagamento_tipo"));
						$sigla = $rowPagamentoTipo['fldSigla'];
						echo mysql_error();
							
						$acao = fnc_sistema("{$documento_tipo}_parcela_acao_$sigla");
						//verifica se eh a vista, se for substitui o $acao
						if($Data == $parcela_vencimento){
							$acao = fnc_sistema("{$documento_tipo}_parcela_acao_AV");
						}
							
						if($acao == 2){ 
							$insertBaixa ="INSERT INTO tbl{$documento_tipo}_parcela_baixa	(fldParcela_Id, fldDataCadastro, fldValor, fldDataRecebido)
								VALUES( '".$LastId['lastID']."', '$Data', '$parcela_valor',	'$Data')";
							
							if( mysql_query($insertBaixa)){
								//lan�ar no caixa
								$conta = fnc_sistema("{$documento_tipo}_parcela_conta_$sigla");
								$LastIdBaixa = mysql_fetch_array(mysql_query("SELECT last_insert_id() as lastID"));
								//$descricao, $credito, $debito, $entidade, $pagamento_tipo_id, $referencia_id, $movimento_tipo, $marcador, $conta
								if($documento_tipo == 'pedido'){
									$credito = $parcela_valor;
									$movimento_tipo = '3';
								}elseif($documento_tipo == 'compra'){
									$debito = $parcela_valor;
									$movimento_tipo = '4';
								}
								fnc_financeiro_conta_fluxo_lancar('', $credito, $debito, '', $pagamento_tipo, $LastIdBaixa['lastID'], $movimento_tipo, '', $conta);
							}
						}
					}
					$y++;
				}
			}//end while
			echo mysql_error();
			//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
			//cheques
			if(!empty($_SESSION['ref_timestamp'])){ //CASO A SESSAO ESTIVER EM BRANCO, PRA NAO ATUALIZAR TODOS OS CHEQUES 
				/*
				$origem_movimento_id = '3'; //recebimento de parcela
				$timestamp			= $_SESSION['ref_timestamp'];
				$origem_id 			= $documento_id;
				fnc_cheque_update($origem_id, '', '', $origem_movimento_id, '', $timestamp);
				*/
				###############################################################################################################################################
				
				$timestamp	 = $_SESSION['ref_timestamp'];
				$movimento_id= $movimento_tipo;
				$registro_id = $documento_id;
				$conta_id	 = $conta; //JA DEFINIDO NAS PARCELAS ACIMA
				fnc_cheque_movimento_lancar($timestamp, $movimento_id, $registro_id, $conta_id);
			}
			unset($_SESSION['ref_timestamp']);
		}//end $nfe_parcelamento_exibir
		
		
		if(!mysql_error()){
			header("location:index.php?p={$documento_tipo}&mensagem=ok");
		}else {
?>			<div class="alert">
				<p class="erro">N&atilde;o foi poss&iacute;vel gravar os dados</p>
				<a class="voltar" href="index.php?p=<?=$documento_tipo?>">voltar</a>
			</div>
<?			echo mysql_error();
		}
	}//if post
	else{
		
		$remote_ip = gethostbyname($REMOTE_ADDR);
		$_SESSION['ref_timestamp'] = $remote_ip.date("YmdHis");

		$venda_usuario_id 	= $rowNFe['fldUsuario_Id'];
		$rowUsuario 		= mysql_fetch_array(mysql_query("SELECT * FROM tblusuario WHERE fldId = $venda_usuario_id"));

		//se financeira
		if($_SESSION['sistema_tipo'] == 'financeira' && $rowNFe['fldContaBancaria_Id'] != NULL){
			$rsBanco = mysql_query("SELECT tblcliente_contabancaria.*, tblcliente_contabancaria.fldId as fldContaId, tblbanco_codigo.fldBanco 
								   FROM tblcliente_contabancaria INNER JOIN tblbanco_codigo ON tblcliente_contabancaria.fldBanco_Numero = tblbanco_codigo.fldNumero
								   WHERE tblcliente_contabancaria.fldId =".$rowNFe['fldContaBancaria_Id']);
			$rowBanco = mysql_fetch_array($rsBanco);
			
		}elseif($_SESSION['sistema_tipo'] == 'automotivo' && isset($rowNFe['fldVeiculo_Id'])){
			$rowVeiculo = mysql_fetch_array(mysql_query("SELECT * FROM tblcliente_veiculo WHERE fldId=". $rowNFe['fldVeiculo_Id']));
			echo mysql_error();
		}

		//FAZ OUTRA ARRAY COM OS VALORES QUE PEGOU DO BANCO
		if($_SESSION["sistema_tipo"] == "otica"){
	
			$rowOtica 		= mysql_fetch_array(mysql_query("SELECT * FROM tblpedido_otica WHERE fldPedido_Id = $documento_id"));
			$otica_retorno 	= array(
				'perto' => array(
					'armacao' 			=>	$rowOtica['fldPerto_Armacao_Item_Id'],
					'material'			=>	$rowOtica['fldPerto_Material_Item_Id'],	
					'OD' => array(
						'esferico' 		 => $rowOtica['fldPerto_OD_Esferico'],
						'cilindrico'	 => $rowOtica['fldPerto_OD_Cilindro'],
						'eixo' 			 => $rowOtica['fldPerto_OD_Eixo'],
						'DP'			 => $rowOtica['fldPerto_OD_DP']
					),
					'OE' => array(
						'esferico' 		 => $rowOtica['fldPerto_OE_Esferico'],
						'cilindrico'	 => $rowOtica['fldPerto_OE_Cilindro'],
						'eixo' 			 => $rowOtica['fldPerto_OE_Eixo'],
						'DP'			 => $rowOtica['fldPerto_OE_DP']
					)
				),
				'longe' => array(
					'armacao' 			=>	$rowOtica['fldLonge_Armacao_Item_Id'],
					'material'			=>	$rowOtica['fldLonge_Material_Item_Id'],	
					'OD' => array(
						'esferico' 		 => $rowOtica['fldLonge_OD_Esferico'],
						'cilindrico'	 => $rowOtica['fldLonge_OD_Cilindro'],
						'eixo' 			 => $rowOtica['fldLonge_OD_Eixo'],
						'DP'			 => $rowOtica['fldLonge_OD_DP']
					),
					'OE' => array(
						'esferico' 		 => $rowOtica['fldLonge_OE_Esferico'],
						'cilindrico'	 => $rowOtica['fldLonge_OE_Cilindro'],
						'eixo' 			 => $rowOtica['fldLonge_OE_Eixo'],
						'DP'			 => $rowOtica['fldLonge_OE_DP']
					)
				),
				'bifocal' => array(
					'marca' 			=>	$rowOtica['fldBifocal_Marca_Id'],
					'lente'				=>	$rowOtica['fldBifocal_Lente_Id'],	
					'OD' => array(
						'altura'		=>	$rowOtica['fldBifocal_OD_Altura'],
						'DNP'			=>	$rowOtica['fldBifocal_OD_DNP'],
						'adicao'		=>	$rowOtica['fldBifocal_OD_Adicao']
					),
					'OE' => array(
						'altura'		=>	$rowOtica['fldBifocal_OE_Altura'],
						'DNP'			=>	$rowOtica['fldBifocal_OE_DNP'],
						'adicao'		=>	$rowOtica['fldBifocal_OE_Adicao']
					)
				)
			);
		}
		
		$serie 				= $rowNFe['fldserie'];
		$numero				= $rowNFe['fldnumero'];
		$dataSaida 			= $rowNFe['fldsaida_data'];
		$saidaHora 			= format_time_short($rowNFe['fldsaida_hora']);
		$documento_data		= ($documento_tipo == 'compra')? $rowNFe['fldCompraData'] : $rowNFe['fldPedidoData'];
		$modFrete			= $rowNFe['fldtransporte_modalidade_codigo'];
		$transportador	 	= $rowNFe['fldtransporte_transportador_id'];
		$infoContribuinte 	= $rowNFe['fldinfo_adicionais_contribuinte'];
		$infoFisco			= $rowNFe['fldinfo_adicionais_fisco'];
		$nfePagamento		= $rowNFe['fldpagamento_forma_codigo'];
		
		
		$cancel_class 		= ($rowNFe["fldnfe_status_id"] =='4' )? 'btn_nfe_cancel' : 'btn_nfe_cancel_disabled' ;
		$ccorrecao_class 	= ($rowNFe["fldnfe_status_id"] =='4' )? 'btn_nfe_carta_correcao' : 'btn_nfe_carta_correcao_disabled' ;
		
?>
		<ul class="header_bar">
	        <li><a class="<?=$ccorrecao_class?>	modal" 	href="pedido_nfe_carta_correcao,<?=$rowNFe['fldchave']?>" 					rel="620-430" title="Emitir Carta de Corre&ccedil;&atilde;o"></a></li>
	        <li><a class="<?=$cancel_class?> 	modal" 	href="pedido_nfe_cancelar,<?=$rowNFe['fldchave']?>" 						rel="520-300" title="Cancelar NFe"></a></li>
	        <li><a class="print_carne" 					href="pedido_imprimir_carne_A4.php?id=<?=$documento_id?>"			 		rel="externo" title="imprimir carn&ecirc;" <?=($documento_tipo == 'compra') ? 'style="pointer-events:none"': ''?> ></a></li>
			<li><a class="print" 						href="pedido_imprimir_<?=$impressao?>.php?&amp;id=<?=$documento_id?>" 		rel="externo" title="imprimir venda" <?=($documento_tipo == 'compra') ? 'style="pointer-events:none"': ''?>></a></li>
            <li><a class="print_np"						href="pedido_imprimir_np_<?=$impressao?>.php?&amp;id=<?=$documento_id?>" 	rel="externo" title="imprimir nota promiss&oacute;ria" <?=($documento_tipo == 'compra') ? 'style="pointer-events:none"': ''?>></a></li>
            <li><a class="btn_print_cupom_fiscal modal" href="ecf_documento,<?=$documento_id?>" 									rel="345-130" title="Emitir Cupom Fiscal" <?=($documento_tipo == 'compra') ? 'style="pointer-events:none"': ''?>></a></li>
            
<?			if($documento_tipo == 'pedido'){
				$sql = "SELECT * FROM tblpedido_parcela INNER JOIN tblpagamento_tipo ON tblpedido_parcela.fldPagamento_Id = tblpagamento_tipo.fldId
						WHERE fldPedido_Id = $documento_id and fldSigla = 'BCR'";
				$rsBoletoCR = mysql_query($sql);
				if(mysql_num_rows($rsBoletoCR)){
?>					<li><a class="btn_print_barcode modal"  rel="400-380" href="pedido_imprimir_boleto_cr,<?=$documento_id?>" title="boleto CR"></a></li>
<?				}
			}
			if($venda_origem > 0){
?>				<li><a class="btn_vendas_origem modal" href="pedido_nfe_vendas_origem,<?=$documento_id?>" rel="350-330" title="Exibir vendas de origem"></a></li>
<?			}
?>		</ul>
        
<?		if($rowNFe["fldnfe_status_id"] =='4' && $rowNFe["fldnfe_status_id"] =='6'){
			echo "<div class='alert'><p class='alert'>NFe j&aacute; transmitida. N&atilde;o dispon&iacute;vel para edi&ccedil;&atilde;o.</p></div>";
		}
?>        
        
        <div class="form">
            <form class="frm_detalhe" style="width:890px" id="frm_pedido_nfe_novo" action="" method="post">
            	<input type="hidden" id="hid_venda_origem" name="hid_venda_origem" value="<?=$venda_origem?>" />
                <ul>
                    <li>
                        <label for="txt_codigo">C&oacute;d.</label>
                        <input type="text" style="width:70px; text-align:right" id="txt_codigo" name="txt_codigo" readonly="readonly" value="<?=$rowNFe['fldDocumento_Id']?>"/>
                    </li>
                    <li>
                        <label for="txt_usuario">Usu&aacute;rio</label>
                        <input type="text" style="width:150px" id="txt_usuario" name="txt_usuario" value="<?=$rowUsuario['fldUsuario']?>" readonly="readonly"/>
                    </li>
                    <? 	if($documento_tipo == 'pedido'){ #MARCADOR E REFERENCIA � DA VENDA, NAO NFe E NEM COMPRA ?>
                            <li>
                                <label for="sel_marcador">Marcador</label>
                                <select style="width:180px;" id="sel_marcador" name="sel_marcador">
                                    <option value="0">Selecionar</option>
<?									$rsMarcador = mysql_query("SELECT * FROM tblpedido_marcador WHERE fldExcluido = 0 ORDER BY fldMarcador");
									while($rowMarcador = mysql_fetch_array($rsMarcador)){                            
?>                      		    	<option <?=($rowNFe['fldMarcador_Id'] == $rowMarcador["fldId"]) ? 'selected="selected"' : '' ?> value="<?=$rowMarcador['fldId']?>"><?=$rowMarcador['fldMarcador']?></option>
<?									}
?>          	    	   		</select>
            				</li>
                            <li>
                                <label for="txt_referencia">Referencia</label>
                                <input type="text" style="width:95px" id="txt_referencia" name="txt_referencia" value="<?=$rowNFe['fldReferencia']?>" onkeyup="numOnly(this)"/>
                            </li>
					<? 	}																			?>
                    <li>
                        <label for="txt_pedido_data">Data Emiss&atilde;o</label>
                        <input type="text" style="width:80px; text-align: center;" class="calendario-mask" id="txt_pedido_data" name="txt_pedido_data" value="<?=format_date_out($documento_data)?>" />
                        <a href="#" title="Exibir calend&aacute;rio" class="exibir-calendario-data-atual"></a>
                    </li>
                    <li>
                        <label for="txt_pedido_hora">Hora Emiss&atilde;o</label>
                        <input type="text" style="width:80px; text-align: right" id="txt_pedido_hora" name="txt_pedido_hora" value="<?=format_time_short($rowNFe['fldCadastroHora'])?>"/>
                    </li>
                    <li>
                        <label for="txt_pedido_saida_data">Data Sa&iacute;da</label>
                        <input type="text" style="width:70px; text-align: center;" class="calendario-mask" id="txt_pedido_saida_data" name="txt_pedido_saida_data" value="<?=format_date_out($dataSaida)?>" />
                        <a href="#" title="Exibir calend&aacute;rio" class="exibir-calendario-data-atual"></a>
                    </li>
                    <li>
                        <label for="txt_pedido_saida_hora">Hora Sa&iacute;da</label>
                        <input type="text" style="width:70px;text-align:right" id="txt_pedido_saida_hora" name="txt_pedido_saida_hora" value="<?=$saidaHora?>"/>
                    </li>
                    <li>
                    <?	if($rowNFE['fldnfe_status_id'] > 0){
							$rowStatus = mysql_fetch_assoc(mysql_query('SELECT fldStatus FROM tblnfe_pedido_status WHERE fldId='.$rowNFE['fldnfe_status_id'])); 
							$nfe_status = $rowStatus['fldStatus'];
						}else{
							$nfe_status = 'em digita&ccedil;&atilde;o';
						}
					?>
                        <label for="txt_pedido_status">Status NF-e</label>
                        <input type="text" style="width:170px" id="txt_pedido_status" name="txt_pedido_status" value="<?=$nfe_status?>"/>
                    </li>
                    <ul>
                        <li>
                            <label for="txt_serie">S&eacute;rie</label>
                            <input type="text" style="width:40px;text-align:right;background:#FFC" id="txt_serie" name="txt_serie" value="<?=$serie?>"/>
                        </li>
                        <li>
                            <label for="txt_pedido_numero">N&uacute;mero</label>
                            <input type="text" style="width:75px;text-align:right" id="txt_pedido_numero" name="txt_pedido_numero" readonly="readonly" value="<?=$numero?>"/>
                        </li>
                        <li>
                            <label for="txt_pedido_chave">Chave</label>
                            <input type="text" style="width:335px;text-align:right" id="txt_pedido_chave" name="txt_pedido_chave" readonly="readonly" value="<?=$rowNFe['fldchave']?>"/>
                        </li>
                        <li>
                            <label for="txt_pedido_natureza_operacao">Natureza da Opera&ccedil;&atilde;o</label>
                            <input type="text" 	 name="txt_pedido_natureza_operacao" 	id="txt_pedido_natureza_operacao" 		value="<?=$nfe_natureza_operacao?>" readonly="readonly" style="width:210px" class="disabled" />
                            <input type="hidden" name="hid_pedido_natureza_operacao_id"	id="hid_pedido_natureza_operacao_id" 	value="<?=$nfe_natureza_operacao_id?>" />
                            <input type="hidden" name="hid_parcela_exibir" 		id="hid_parcela_exibir" 	value="<?=$nfe_parcelamento_exibir?>" />
                            <input type="hidden" name="hid_estoque_controlar" 	id="hid_estoque_controlar" 	value="<?=$nfe_estoque_controlar?>" />
                        </li>
                        <li>
                            <label for="sel_pedido_operacao_tipo">Tipo de documento</label>
                            <select class="sel_pedido_operacao_tipo" style="width:120px;" id="sel_pedido_operacao_tipo" name="sel_pedido_operacao_tipo" >
                                <option <?=($nfe_operacao_tipo == 0 )? "selected ='selected'" : '' ?> value="0">Entrada</option>
                                <option <?=($nfe_operacao_tipo == 1 )? "selected ='selected'" : '' ?> value="1">Sa&iacute;da</option>
                            </select>
                        </li>
                    </ul>
                </ul>
                <ul>
<?				
				if($documento_tipo == 'pedido'){
						$rowDestinatario = mysql_fetch_array(mysql_query("SELECT * FROM tblcliente WHERE fldId = ".$rowNFe['fldCliente_Id']));
						$destinatario_nome 	 = $rowDestinatario['fldNome'];
						$destinatario_codigo = $rowDestinatario['fldCodigo'];
					}else{
						$rowDestinatario = mysql_fetch_array(mysql_query("SELECT * FROM tblfornecedor WHERE fldId = ".$rowNFe['fldFornecedor_Id']));
						$destinatario_nome = $rowDestinatario['fldRazaoSocial'];
						$destinatario_codigo = $rowDestinatario['fldId'];
					}
					echo mysql_error();
?>             		<li>
                        <label for="txt_cliente_codigo">Destinat&aacute;rio</label>
                        <input type="text" style="width:70px; text-align:right;" id="txt_cliente_codigo" name="txt_cliente_codigo" value="<?=$destinatario_codigo?>" <?=($documento_tipo == 'compra') ? 'readonly="readonly"' : '' ?> />
                       	<? 	if($documento_tipo == 'pedido'){ ?>
                        		<a href="cliente_busca" title="Localizar" class="modal" rel="950-380"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                    	<?	}								?>
                    </li>
                    <li>
                        <label for="txt_cliente_nome">&nbsp;</label>
                        <input type="text" style=" width:328px" id="txt_cliente_nome" name="txt_cliente_nome" readonly="readonly" value="<?=$destinatario_nome?>"  <?=($documento_tipo == 'compra') ? 'disabled="disabled"' : '' ?> />
                        <input type="hidden" id="hid_cliente_id" name="hid_cliente_id" value="<?=$rowDestinatario['fldId']?>" />
                    </li>
<?   				if($nfe_devolucao != 'true'){                 
							$rsFuncionario= mysql_query("SELECT tblfuncionario.fldNome, fldFuncionario_Id 
								 FROM tblpedido_funcionario_servico LEFT JOIN tblfuncionario
								 ON tblfuncionario.fldId = tblpedido_funcionario_servico.fldFuncionario_Id
								 WHERE tblpedido_funcionario_servico.fldFuncao_Tipo = 1 
								 AND tblpedido_funcionario_servico.fldPedido_Id = $documento_id");
							$rows_func = mysql_num_rows($rsFuncionario);
							
							if($rows_func > 0){	$rowFuncionario = mysql_fetch_array($rsFuncionario); }
							else {
								$sql_origem		= mysql_query("SELECT fldId FROM tblpedido WHERE fldPedido_Destino_Nfe_Id = $documento_id");
								$origem 		= mysql_num_rows($sql_origem);
								if($origem > 0){
									$origem = mysql_fetch_array($sql_origem);
									$origem = $origem['fldId'];
								
									$rsFuncionario  = mysql_query("SELECT tblfuncionario.fldNome, tblfuncionario.fldId as fldFuncionario_Id FROM tblfuncionario 
																	INNER JOIN tblpedido_funcionario_servico ON tblfuncionario.fldId = tblpedido_funcionario_servico.fldFuncionario_Id
																	AND tblpedido_funcionario_servico.fldFuncao_Tipo = 1 
																	WHERE tblpedido_funcionario_servico.fldPedido_Id = $origem 
																	GROUP BY tblpedido_funcionario_servico.fldFuncionario_Id, tblpedido_funcionario_servico.fldPedido_Id LIMIT 1");									
									$rowFuncionario 	= mysql_fetch_array($rsFuncionario);
								}
							}
							
?>      	        	<li>
							<label for="txt_funcionario_codigo">Funcion&aacute;rio</label>
							<input type="text" style="width:75px; text-align:right;" id="txt_funcionario_codigo" name="txt_funcionario_codigo" value="<?=$rowFuncionario['fldFuncionario_Id']?>" <?=($venda_origem > 0) ? 'readonly="readonly"' : '';?> />
                            <? if ($venda_origem == 0){ ?>
							<a href="funcionario_busca,1" title="Localizar" class="modal" rel="680-380"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                            <? } ?>
						</li>
						<li style="width:350px">
							<label for="txt_funcionario_nome">&nbsp;</label>
							<input type="text" style=" width:400px;" id="txt_funcionario_nome" name="txt_funcionario_nome" value="<?=$rowFuncionario['fldNome']?>" readonly="readonly" />
						</li>
<?						if($_SESSION["exibir_dependente"] > 0){
							if($rowNFe['fldDependente_Id'] > 0){
								$rowDependente 		= mysql_fetch_array(mysql_query("SELECT fldId, fldCodigo, fldNome FROM tblcliente WHERE fldId = ".$rowNFe['fldDependente_Id']));
								$dependente_id	 	= $rowDependente['fldId'];
								$dependente_codigo 	= $rowDependente['fldCodigo'];
								$dependente_nome 	= $rowDependente['fldNome'];
							}
?>                    
                            <li>
                                <label for="txt_dependente_codigo">Dependente</label>
                                <input type="text" style="width:70px; text-align:right" id="txt_dependente_codigo" name="txt_dependente_codigo" value="<?=$dependente_codigo?>" />
                                <a href="dependente_busca" title="Localizar" class="modal" rel="950-380"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                            </li>
                            <li>
                                <label for="txt_dependente_nome">&nbsp;</label>
                                <input type="text"	 id="txt_dependente_nome" 	name="txt_dependente_nome" 	value="<?=$dependente_nome?>" style=" width:315px" readonly="readonly" />
                                <input type="hidden" id="hid_dependente_id" 	name="hid_dependente_id" 	value="<?=$dependente_id?>" />
                            </li>
<?                  	}  
					}
					
					#VENDA OU REVENDA DE MERCADORIAS
					if($nfe_natureza_operacao_id =='1' || $nfe_natureza_operacao_id == '3'){
?>
                        <li>
                           <label for="txt_cliente_pedido">N&ordm; Servi&ccedil;o</label>
                           <input type="text" style=" width:70px; text-align:right" name="txt_cliente_pedido" id="txt_cliente_pedido" value="<?=$rowNFe['fldCliente_Pedido']?>" readonly="readonly"  />
                        </li>
<?						if($_SESSION["sistema_tipo"]=="automotivo" && $venda_origem  < 2){
							$exibir = fnc_sistema('venda_exibir_veiculo');
?>							<li>
								<label for="sel_veiculo">Ve&iacute;culo</label>
								<select class="sel_veiculo" style="width:200px;" id="sel_veiculo" name="sel_veiculo" >
									<option selected="selected" value="<?=$rowVeiculo['fldId']?>"><?= ($exibir == 'placa') ? $rowVeiculo['fldPlaca'] : $rowVeiculo['fldVeiculo']." [".$rowVeiculo['fldPlaca']."]"?></option>
								</select>
							</li>
							<a class="modal" style="display:none" id="modal_veiculo" href="cliente_veiculo_cadastro_venda" rel="680-200" title=""></a>
							<li>
								<label for="txt_veiculo_km">KM</label>
								<input type="text" style="width:135px" id="txt_veiculo_km" name="txt_veiculo_km" value="<?=$rowNFe['fldVeiculo_Km']?>"/>
							</li>
							<li>
								<label for="txt_veiculo_ano">Ano</label>
								<input type="text" style="width:70px" id="txt_veiculo_ano" name="txt_veiculo_ano" value="<?=$rowNFe['fldAno']?>" readonly="readonly"/>
							</li>
							<li>
								<label for="txt_veiculo_chassi">Chassi</label>
								<input type="text" style="width:150px" id="txt_veiculo_chassi" name="txt_veiculo_chassi" value="<?=$rowNFe['fldChassi']?>" readonly="readonly"/>
							</li>
<?						}
						if($_SESSION["sistema_tipo"]=="financeira"){
?>							<li>
                                <label for="sel_conta_bancaria">Conta Banc&aacute;ria</label>
                                <select style="width:260px" id="sel_conta_bancaria" name="sel_conta_bancaria" >
                                    <option selected="selected" value="<?=$rowBanco['fldContaId']?>"><?=$rowBanco['fldBanco']." - ".$rowBanco['fldConta']?></option>
                                </select>
                            </li>
                            <li>
                                <label for="txt_agencia">Ag&ecirc;ncia</label>
                                <input style="width: 60px" type="text" id="txt_agencia" name="txt_agencia" readonly="readonly" value="<?=$rowBanco['fldAgencia']?>"/>
                            </li>
                            <li>
                                <label for="txt_titular">Titular</label>
                                <input type="text" name="txt_titular" id="txt_titular" value="<?=$rowBanco['fldTitular']?>" readonly="readonly" />
                            </li>
<?						}
						if($_SESSION["sistema_tipo"]=="otica" && $venda_origem == 0){   
							$rsResponsavel  = mysql_query("SELECT fldResponsavel FROM tblcliente_dados_adicionais WHERE fldCliente_Id = ". $rowNFe['fldCliente_Id']);
							$rowResponsavel = mysql_fetch_array($rsResponsavel);
?>						
                            <li>
                                <label for="sel_cliente_responsavel">Respons&aacute;vel</label>
                                <select style="width:200px" id="sel_cliente_responsavel" name="sel_cliente_responsavel" >
                                    <option value="1" <?= ($rowOtica['fldResponsavel'] == '1')? "selected = 'selected'" : '' ?>><?=$rowDestinatario['fldNome']?></option>
                                    <option value="2" <?= ($rowOtica['fldResponsavel'] == '2')? "selected = 'selected'" : '' ?>><?=$rowResponsavel['fldResponsavel']?></option>
                                </select>
                            </li>
                            <li>
                                <label for="txt_pedido_entrega">Entrega</label>
                                <input type="text" style=" width:70px" name="txt_pedido_entrega" id="txt_pedido_entrega" class="calendario-mask" value="<?=date('d/m/Y')?>" />
                            </li>
<?						}
					}#endif Devolucao
?>
	              	<li>
                 	   <label for="txt_pedido_obs">Observa&ccedil;&atilde;o</label>
                       <textarea style=" <?=($_SESSION["sistema_tipo"]=="otica")? 'width:480px;' : 'width:780px;' ?> height:50px" id="txt_pedido_obs" name="txt_pedido_obs"><?=$rowNFe['fldObservacao']?></textarea>
                    </li>
                </ul>
                <!---aqui uma lista para, conforme excluir algum item que ja estava na venda, ele calcular o estoque devolvido-------------------------------------------------------------------->
                <div id="hid_item" style="display:none">
                    <ul class="item_estoque">
                        <li><input type="hidden" class="hid_controle_item_produto_id" 	id="hid_controle_item_produto_id" 	name="hid_controle_item_produto_id" value="0" /></li>
                        <li><input type="hidden" class="hid_controle_item_estoque" 		id="hid_controle_item_estoque" 		name="hid_controle_item_produto_id" value="" /></li>
                        <li><input type="hidden" class="hid_controle_item_estoque_id" 	id="hid_controle_item_estoque_id" 	name="hid_controle_item_produto_id" value="" /></li>
                    </ul>
                    <input type="hidden" id="hid_calculo_estoque_controle" name="hid_calculo_estoque_controle" value="0" />
                </div>
                <!-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
<?				if($venda_origem == 0 && $documento_tipo == 'pedido'){                 
?>					<ul id="pedido_modo_aba" class="menu_modo" style="width:952px;float:left; background:#FFF">
						<li><a href="produto">produtos</a></li>
						<li><a href="servico">servi&ccedil;os</a></li>
					</ul>
<?				}                    
?>         		<div id="modo_aba_produto" style="width:960px; display:table">
					
                    <input type="hidden" id="hid_quantidade_decimal" name="hid_quantidade_decimal" 	value="<?=$qtdeDecimal?>" /> 
					<input type="hidden" id="hid_venda_decimal"		 name="hid_venda_decimal" 		value="<?=$vendaDecimal?>" />       
<?					if($venda_origem == 0){                     
?>                  	<div id="pedido_produto">
                            <ul style="width:962px">
                                <li>
                                    <label for="txt_produto_codigo">C&oacute;digo</label>
                                    <input type="text" class="codigo" style="width: 80px" id="txt_produto_codigo" name="txt_produto_codigo" value="" />
                                    <a href="produto_busca" title="Localizar" class="modal" rel="950-450"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                                    <input type="hidden" id="hid_produto_id" 				name="hid_produto_id" 				value="" />
                                    <input type="hidden" id="hid_estoque_limite" 			name="hid_estoque_limite" 			value="0" />
                                    <input type="hidden" id="hid_estoque_controle" 			name="hid_estoque_controle"			value="0" />
                                    <input type="hidden" id="hid_estoque_negativo_alerta" 	name="hid_estoque_negativo_alerta" 	value="<?= fnc_sistema('alerta_estoque_negativo')?>" />
                                    <input type="hidden" id="hid_item_repetido_alerta"	 	name="hid_item_repetido_alerta" 	value="<?=$_SESSION["sistema_pedido_item_repetido"]?>" />
                                </li>
                                <li>
                                    <label for="txt_produto_nome">Produto</label>
                                    <textarea class="enter next" style="width: 270px; height:40px" id="txt_produto_nome" name="txt_produto_nome" readonly="readonly"></textarea>
                                </li>
                                <li>
                                    <label for="sel_produto_fardo">Fardo</label>
                                    <SELECT name="sel_produto_fardo" id="sel_produto_fardo" style="width:60px">
                                    </SELECT>
                                </li>
                                <li>
                                    <label for="sel_produto_tabela">Tabela</label>
                                    <select name="sel_produto_tabela" id="sel_produto_tabela" style="width:75px">
                                        <option value="null">padr&atilde;o</option>
<?		                        		$filtro_tabela 	 = (fnc_sistema('cliente_produto_preco') == 0 ) ? 'AND fldId > 0' : '';
										$rsTabela 		 = mysql_query("SELECT * FROM tblproduto_tabela WHERE fldExcluido = '0' $filtro_tabela ");
                                   		while($rowTabela = mysql_fetch_array($rsTabela)){
?>	                            			<option value="<?=$rowTabela['fldId']?>"><?=$rowTabela['fldSigla']?></option>
<?										}
?>									</select>
                                </li>
                                <li>
                                    <label for="sel_produto_estoque">Estoque</label>
                                    <select name="sel_produto_estoque" id="sel_produto_estoque" style="width:80px">
<?	                          			$rsEstoque = mysql_query("SELECT * FROM tblproduto_estoque WHERE fldExcluido = '0'");
                        				while($rowEstoque = mysql_fetch_array($rsEstoque)){
?>	                           				<option value="<?=$rowEstoque['fldId']?>"><?=$rowEstoque['fldNome']?></option>
<?										}
?>									</select>
                            	</li>
                                <li>
                                    <label for="txt_produto_valor">Valor Un.</label>
                                    <input type="text" id="txt_produto_valor" 	 	name="txt_produto_valor" 		value="" style="width:60px;text-align:right" />
                                </li>
                                <li>
                                    <label for="txt_produto_quantidade">Qtde</label>
                                    <input type="text" id="txt_produto_quantidade" 	name="txt_produto_quantidade" 	value="" style="width:50px;text-align:right"/>
                                </li>
                                <li>
                                    <label for="txt_produto_desconto">Desc (%)</label>
                                    <input type="text" id="txt_produto_desconto" 	name="txt_produto_desconto" 	value="" style="width:50px; text-align:right"/>
                                </li>
                                <li>
                                    <label for="txt_produto_referencia">Referencia</label>
                                    <input type="text" id="txt_produto_referencia" 	name="txt_produto_referencia" 	value="" style="width:60px; text-align:right"/>
                                </li>
                                <li>
                                    <input type="hidden" id="hid_produto_UM" 		name="hid_produto_UM" 	 value="" />
                            		<button class="btn_sub_small" name="btn_item_inserir" id="btn_item_inserir" title="Inserir" >ok</button>
                                    <a class="modal" style="display:none" id="btn_nfe" href="pedido_nfe_item" rel='980-500' title=""></a>
                                </li>
                            </ul>
                        </div>
<?					}                        
?>                  <div id="pedido_lista" style="width:960px">
                        <ul id="pedido_lista_cabecalho" style="width: 11080px">
<? 							if($venda_origem == 0){ 	
?>    	                       	<li style="width:20px">&nbsp;</li>
<?							}									?>	
	                        <li style="width:20px">&nbsp;</li>
<?							if($documento_tipo == 'pedido'){ 	?>
                            	<li style="width:20px" class="entregue" title="entregue"></li>
<?							}									?>
                            <li style="width:80px">C&oacute;digo</li>
                            <li style="width:300px">Produto</li>
                            <li style="width:45px">Fardo</li>
                            <li style="width:50px">Tabela</li>
                            <li style="width:80px">Valor Un.</li>
                            <li style="width:55px">Qtde</li>
                            <li style="width:60px">Estoque</li>
                            <li style="width:30px">U.M.</li>
                            <li style="width:45px">Desc(%)</li>
                            <li style="width:73px">Subtotal</li>
                            <li style="width:70px">Referencia</li>
                            
                            <li style="width:70px">NCM</li>
                            <li style="width:70px">EX TIPI</li>
                            <li style="width:70px">CFOP</li>
                            <li style="width:90px">Uni. Comercial</li>
                            <li style="width:70px">Uni. Tribut.</li>
                            <li style="width:70px">Qtd. Tribut.</li>
                            <li style="width:110px">Valor Unit. Tribut.</li>
                            <li style="width:70px">Tot. Seguro</li>
                            <li style="width:85px">Desconto</li>
                            <li style="width:70px">Frete</li>
                            <li style="width:70px">EAN</li>
                            <li style="width:70px">EAN Tribut.</li>
                            <li style="width:120px">Out. Desp. Acess&oacute;rias</li>
                            <li style="width:90px">Val. Tot. Bruto</li>
                            <li style="width:70px">Ped. Compra</li>
                            <li style="width:110px">N&ordm; Item Ped. Compra</li>
                            <li style="width:110px">Val. Tot. Bruto</li>
                            
                            <li style="width:120px">ICMS Sit. Tribut.</li>
                            <li style="width:70px">Origem</li>
                            <li style="width:150px">Aliq. Aplic&aacute;vel Calc. Cr&eacute;d.</li>
                            <li style="width:150px">Cr&eacute;d. pode ser aproveitado</li>
                            <li style="width:150px">Modalid. Determ. BC ICMS</li>
                            <li style="width:80px">BC ICMS</li>
                            <li style="width:120px">% Red. BC ICMS</li>
                            
                            <li style="width:70px">Al&iacute;q. ICMS</li>
                            <li style="width:80px">ICMS</li>
                            <li style="width:150px">% BC Opera&ccedil;. Pr&oacute;pria</li>
                            <li style="width:150px">Motivo desonera&ccedil;&atilde;o ICMS</li>
                            <li style="width:180px">Modalid. Determ. BC ICMS ST</li>
                            
                            <li style="width:120px">% Redu&ccedil;. BC ICMS ST</li>
                            <li style="width:150px">% Marg. Val. Adic. ICMS ST</li>
                            <li style="width:100px">BC do ICMS ST</li>
                            <li style="width:100px">Al&iacute;q. ICMS ST</li>
                            <li style="width:80px">ICMS ST</li>
                            <li style="width:120px">UF Devido ICMS ST</li>
                            <li style="width:150px">BC ICMS retido ant.</li>
                            <li style="width:150px">ICMS retido ant.</li>
                            <li style="width:150px">BC BC UF dest.</li>
                            <li style="width:150px">BC ICMS UF dest.</li>
                            
                            <li style="width:120px">COFINS - Sit. Tribut.</li>
                            <li style="width:110px">COFINS - Tipo C&aacute;lc.</li>
                            <li style="width:100px">COFINS - Val. BC</li>
                            <li style="width:130px">COFINS - Al&iacute;q. Percent.</li>
                            <li style="width:130px">COFINS - Al&iacute;q. Reais</li>
                            <li style="width:120px">COFINS - Qtd. Vendida</li>
                            <li style="width:100px">COFINS Val.</li>
                            
                            <li style="width:120px">COFINS ST - Tipo C&aacute;lc.</li>
                            <li style="width:120px">COFINS ST - Val. BC</li>
                            <li style="width:140px">COFINS ST - Al&iacute;q. Percent.</li>
                            <li style="width:120px">COFINS ST - Al&iacute;q. Reais</li>
                            <li style="width:140px">COFINS ST - Qtd. Vendida</li>
                            <li style="width:120px">COFINS ST Val.</li>
                            
                            <li style="width:120px">II - Val. BC</li>
                            <li style="width:150px">II - Val. Desp. Aduaneiras</li>
                            <li style="width:70px">II - Val. IOF</li>
                            <li style="width:70px">II Val.</li>
                            
                            <li style="width:100px">IPI - Sit. Tribut.</li>
                            <li style="width:140px">IPI - Class. Enquadramento</li>
                            <li style="width:170px">IPI - C&oacute;d. Enquadramento Leg.</li>
                            <li style="width:120px">IPI - CNPJ Produtor</li>
                            <li style="width:140px">IPI - C&oacute;d. Selo Controle</li>
                            <li style="width:140px">IPI - Qtd. Selo Controle</li>
                            <li style="width:120px">IPI - Tipo C&aacute;lc.</li>
                            <li style="width:70px">IPI - Val. BC.</li>
                            <li style="width:120px">IPI - Al&iacute;quota</li>
                            <li style="width:150px">IPI - Qtd. Tot. Unid. Padr&atilde;o</li>
                            <li style="width:120px">IPI - Val. Unid.</li>
                            <li style="width:70px">IPI Val.</li>
                            
                            <li style="width:120px">ISSQN - Tributa&ccedil;&atilde;o</li>
                            <li style="width:120px">ISSQN - Val. BC</li>
                            <li style="width:120px">ISSQN - Al&iacute;quota</li>
                            <li style="width:140px">ISSQN - List. Servi&ccedil;o</li>
                            <li style="width:70px">ISSQN - UF</li>
                            <li style="width:140px">ISSQN - Mun. Ocorr&ecirc;ncia</li>
                            <li style="width:80px">ISSQN Val.</li>
                            
                            <li style="width:100px">PIS - Sit. Tribut.</li>
                            <li style="width:100px">PIS - Tipo Calc.</li>
                            <li style="width:100px">PIS - Val. BS Calc.</li>
                            <li style="width:110px">PIS - Al&iacute;q. Percent.</li>
                            <li style="width:100px">PIS - Al&iacute;q. Reais.</li>
                            <li style="width:100px">PIS - Qtd. Vendida</li>
                            <li style="width:60px">PIS Val.</li>
                            
                            <li style="width:120px">PIS ST - Tipo Calc.</li>
                            <li style="width:100px">PIS ST - Val. BC</li>
                            <li style="width:120px">PIS ST - Al&iacute;q. Percent.</li>
                            <li style="width:120px">PIS ST - Al&iacute;q. Reais.</li>
                            <li style="width:120px">PIS ST - Qtd. Vendida</li>
                            <li style="width:70px">PIS ST Val.</li>
                            <li style="width:200px">Info. Adicionais</li>
                            <li style="width:122px">Prod. Especifico</li>
                        </ul>
                        
                        <div id="hidden">
                            <ul id="pedido_lista_item" style="width: 11080px">
                                <li style="width:20px;">
                                    <a class="a_excluir" id="excluir_0" href="" title="Excluir item"></a>
                                </li>
                                <li style="width:20px;">
                                    <a class="edit edit_js modal" id="edit_0" href="pedido_nfe_item" name='' rel='980-500' title="Editar item"></a>
                                </li>
                                <?	if($documento_tipo == 'pedido'){ 	?>
                                        <li style="width:20px; background:#FFF">
                                            <input class="chk_entregue" style="width:20px" type="checkbox" name="chk_entregue" id="chk_entregue" title="item entregue" onclick="return false" />
                                        </li>
                                <?	}									?>
                                <li>
	                                <input class="hid_pedido_item_id" 	type="hidden" 	id="hid_pedido_item_id" 	name="hid_pedido_item_id" 	value="0" />
                                    <input class="txt_item_codigo" 		type="text"		id="txt_item_codigo" 		name="txt_item_codigo"		value=""  style="width: 80px" readonly="readonly"/>
                                    <input class="hid_item_produto_id" 	type="hidden" 	id="hid_item_produto_id" 	name="hid_item_produto_id" 	value="" />
                                    <input class="hid_item_detalhe" 	type="hidden" 	id="hid_item_detalhe" 		name="hid_item_detalhe" 	value="0" />
                                </li>
                                <li>
                                    <input class="txt_item_nome" type="text" style=" width:300px; text-align:left" id="txt_item_nome" name="txt_item_nome" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input class="txt_item_fardo" type="text" style="width:45px" id="txt_item_fardo" name="txt_item_fardo" value="" readonly="readonly" />
                                    <input class="hid_item_fardo" type="hidden" id="hid_item_fardo" name="hid_item_fardo" value="" />
                                </li>
                                <li>
                                    <input class="txt_item_tabela_sigla" type="text" style="width:50px" id="txt_item_tabela_sigla" name="txt_item_tabela_sigla" value="" readonly="readonly" />
                                    <input class="hid_item_tabela_preco" type="hidden" id="hid_item_tabela_preco" name="hid_item_tabela_preco" value="" />
                                </li>
                                <li>
                                    <input class="txt_item_valor" type="text" style="width:80px; text-align:right" id="txt_item_valor" name="txt_item_valor" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input class="txt_item_quantidade" type="text" style="width:55px; text-align: right" id="txt_item_quantidade" name="txt_item_quantidade" value="0,00" readonly="readonly" />
                                </li>
                                <li>
                                    <input class="txt_item_estoque_nome" type="text" style="width:60px" id="txt_item_estoque_nome" name="txt_item_estoque_nome" value="" readonly="readonly" />
                                    <input class="hid_item_estoque_id" type="hidden" id="hid_item_estoque_id" name="hid_item_estoque_id" value="1" />
                                </li>
                                <li>
                                    <input class="txt_item_UM" type="text" style="width:30px; text-align:center" id="txt_item_UM" name="txt_item_UM" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input class="txt_item_desconto" type="text" style="width:45px; text-align: right" id="txt_item_desconto" name="txt_item_desconto" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input class="txt_item_subtotal" type="text" style="width:73px; text-align: right" id="txt_item_subtotal" name="txt_item_subtotal" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input class="txt_item_referencia" type="text"		id="txt_item_referencia" 	name="txt_item_referencia" 		value="" style="width:70px; text-align:right" readonly="readonly" />
                                </li>
                                
                                <!-- nfe -->
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_ncm" id="txt_nfe_pedido_item_principal_ncm_0" name="txt_nfe_pedido_item_principal_ncm" style="width: 70px" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_ex_tipi" id="txt_nfe_pedido_item_principal_ex_tipi_0" name="txt_nfe_pedido_item_principal_ex_tipi" style="width: 70px" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_cfop" id="txt_nfe_pedido_item_principal_cfop_0" name="txt_nfe_pedido_item_principal_cfop" style="width: 70px" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_unidade_comercial" id="txt_nfe_pedido_item_principal_unidade_comercial_0" name="txt_nfe_pedido_item_principal_unidade_comercial" style="width: 90px; text-align:center" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_unidade_tributavel" id="txt_nfe_pedido_item_principal_unidade_tributavel_0" name="txt_nfe_pedido_item_principal_unidade_tributavel" style="width: 70px;text-align:center" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_quantidade_tributavel" id="" name="txt_nfe_pedido_item_principal_quantidade_tributavel" style="width: 70px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_valor_unidade_tributavel" id="txt_nfe_pedido_item_principal_valor_unidade_tributavel_0" name="txt_nfe_pedido_item_principal_valor_unidade_tributavel" style="width: 110px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_total_seguro" id="txt_nfe_pedido_item_principal_total_seguro_0" name="txt_nfe_pedido_item_principal_total_seguro" style="width: 70px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_desconto" style="width:85px; text-align: right" id="txt_nfe_pedido_item_principal_desconto_0" name="txt_nfe_pedido_item_principal_desconto" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_frete" id="txt_nfe_pedido_item_principal_frete_0" name="txt_nfe_pedido_item_principal_frete" style="width: 70px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_ean" id="txt_nfe_pedido_item_principal_ean_0" name="txt_nfe_pedido_item_principal_ean" style="width: 70px" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_ean_tributavel" id="txt_nfe_pedido_item_principal_ean_tributavel_0" name="txt_nfe_pedido_item_principal_ean_tributavel" style="width: 70px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_outras_despesas_acessorias" id="txt_nfe_pedido_item_principal_outras_despesas_acessorias_0" name="txt_nfe_pedido_item_principal_outras_despesas_acessorias" style="width: 120px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_valor_total_bruto" id="txt_nfe_pedido_item_principal_valor_total_bruto_0" name="txt_nfe_pedido_item_principal_valor_total_bruto" style="width: 90px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_pedido_compra" id="txt_nfe_pedido_item_principal_pedido_compra_0" name="txt_nfe_pedido_item_principal_pedido_compra" style="width: 70px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_numero_item_pedido_compra" id="txt_nfe_pedido_item_principal_numero_item_pedido_compra_0" name="txt_nfe_pedido_item_principal_numero_item_pedido_compra" style="width: 110px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <!--
                                <li>
                                    <input type="text" style="width:70px;text-align:left" class="txt_nfe_pedido_item_principal_produto_especifico" id="txt_nfe_pedido_item_principal_produto_especifico_0" name="txt_nfe_pedido_item_principal_produto_especifico"  value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_principal_produto_especifico"  id="hid_nfe_pedido_item_principal_produto_especifico" name="hid_nfe_pedido_item_principal_produto_especifico" value="" />  
                                </li>
                                -->
                                <li>
                                    <input type="checkbox" class="chk_nfe_pedido_item_principal_valor_total_bruto" id="chk_nfe_pedido_item_principal_valor_total_bruto_0" name="chk_nfe_pedido_item_principal_valor_total_bruto" style="width: 110px;text-align:left" checked="checked" readonly="readonly" />
                                </li>
                                <!--tributos-->
                                <li>
                                    <input type="text" style="width:120px;text-align:left" class="txt_nfe_pedido_item_tributos_icms_situacao_tributaria" id="txt_nfe_pedido_item_tributos_icms_situacao_tributaria_0" name="txt_nfe_pedido_item_tributos_icms_situacao_tributaria" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_icms_situacao_tributaria" id="hid_nfe_pedido_item_tributos_icms_situacao_tributaria" name="hid_nfe_pedido_item_tributos_icms_situacao_tributaria" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_tributo_rad" id="hid_nfe_tributo_rad" name="hid_nfe_tributo_rad" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:70px;text-align:center" class="txt_nfe_pedido_item_tributos_icms_origem" id="txt_nfe_pedido_item_tributos_icms_origem_0" name="txt_nfe_pedido_item_tributos_icms_origem" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_icms_origem" id="hid_nfe_pedido_item_tributos_icms_origem" name="hid_nfe_pedido_item_tributos_icms_origem" value="" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_pCredSN" id="txt_nfe_pedido_item_tributos_icms_pCredSN_0" name="txt_nfe_pedido_item_tributos_icms_pCredSN" style="width:150px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_vCredICMSSN" id="txt_nfe_pedido_item_tributos_icms_vCredICMSSN_0" name="txt_nfe_pedido_item_tributos_icms_vCredICMSSN" style="width:150px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text"  style="width:150px;text-align:left" class="txt_nfe_pedido_item_tributos_icms_modBC" id="txt_nfe_pedido_item_tributos_icms_modBC_0" name="txt_nfe_pedido_item_tributos_icms_modBC" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_icms_modBC" id="hid_nfe_pedido_item_tributos_icms_modBC" name="hid_nfe_pedido_item_tributos_icms_modBC" value="" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_vBC" id="txt_nfe_pedido_item_tributos_icms_vBC_0" name="txt_nfe_pedido_item_tributos_icms_vBC" style="width:80px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_pRedBC" id="txt_nfe_pedido_item_tributos_icms_pRedBC_0" name="txt_nfe_pedido_item_tributos_icms_pRedBC" style="width:120px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_pICMS" id="txt_nfe_pedido_item_tributos_icms_pICMS_0" name="txt_nfe_pedido_item_tributos_icms_pICMS" style="width:70px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_vICMS" id="txt_nfe_pedido_item_tributos_icms_vICMS_0" name="txt_nfe_pedido_item_tributos_icms_vICMS" style="width:80px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_pBCOp" id="txt_nfe_pedido_item_tributos_icms_pBCOp_0" name="txt_nfe_pedido_item_tributos_icms_pBCOp" style="width:150px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:150px;text-align:left" class="txt_nfe_pedido_item_tributos_icms_motDesICMS" id="txt_nfe_pedido_item_tributos_icms_motDesICMS_0" name="txt_nfe_pedido_item_tributos_icms_motDesICMS" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_icms_motDesICMS"  id="hid_nfe_pedido_item_tributos_icms_motDesICMS_0" name="hid_nfe_pedido_item_tributos_icms_motDesICMS" value="" />
                                </li>
                                <li>
                                    <input type="text" style="width:180px;text-align:left" class="txt_nfe_pedido_item_tributos_icms_st_modBCST" id="txt_nfe_pedido_item_tributos_icms_st_modBCST_0" name="txt_nfe_pedido_item_tributos_icms_st_modBCST" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_icms_st_modBCST" id="hid_nfe_pedido_item_tributos_icms_st_modBCST_0" name="hid_nfe_pedido_item_tributos_icms_st_modBCST" value="" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_pRedBCST" id="txt_nfe_pedido_item_tributos_icms_st_pRedBCST_0" name="txt_nfe_pedido_item_tributos_icms_st_pRedBCST" style="width:120px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_pMVAST" id="txt_nfe_pedido_item_tributos_icms_st_pMVAST_0" name="txt_nfe_pedido_item_tributos_icms_st_pMVAST" style="width:150px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_vBCST" id="txt_nfe_pedido_item_tributos_icms_st_vBCST_0" name="txt_nfe_pedido_item_tributos_icms_st_vBCST" style="width:100px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_pICMSST" id="txt_nfe_pedido_item_tributos_icms_st_pICMSST_0" name="txt_nfe_pedido_item_tributos_icms_st_pICMSST" style="width:100px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_vICMSST" id="txt_nfe_pedido_item_tributos_icms_st_vICMSST_0" name="txt_nfe_pedido_item_tributos_icms_st_vICMSST" style="width:80px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:120px;text-align:left" class="txt_nfe_pedido_item_tributos_icms_st_UFST" id="txt_nfe_pedido_item_tributos_icms_st_UFST_0" name="txt_nfe_pedido_item_tributos_icms_st_UFST" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_icms_st_UFST" id="hid_nfe_pedido_item_tributos_icms_st_UFST_0" name="hid_nfe_pedido_item_tributos_icms_st_UFST" value="" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_vBCSTRet" id="txt_nfe_pedido_item_tributos_icms_st_vBCSTRet_0" name="txt_nfe_pedido_item_tributos_icms_st_vBCSTRet" style="width:150px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_vICMSSTRet" id="txt_nfe_pedido_item_tributos_icms_st_vICMSSTRet_0" name="txt_nfe_pedido_item_tributos_icms_st_vICMSSTRet" style="width:150px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_vBCSTDest" id="txt_nfe_pedido_item_tributos_icms_st_vBCSTDest_0" name="txt_nfe_pedido_item_tributos_icms_st_vBCSTDest" style="width:150px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_vICMSSTDest" id="txt_nfe_pedido_item_tributos_icms_st_vICMSSTDest_0" name="txt_nfe_pedido_item_tributos_icms_st_vICMSSTDest" style="width:150px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <!--tributos cofins-->
                                <li>
                                    <input type="text" style="width:120px;text-align:left"  class="txt_nfe_pedido_item_tributos_cofins_situacao_tributaria" id="txt_nfe_pedido_item_tributos_cofins_situacao_tributaria_0" name="txt_nfe_pedido_item_tributos_cofins_situacao_tributaria" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_cofins_situacao_tributaria" id="hid_nfe_pedido_item_tributos_cofins_situacao_tributaria" name="hid_nfe_pedido_item_tributos_cofins_situacao_tributaria" value="" />
                                </li>
                                <li>
                                    <input type="text" style="width:110px;text-align:center" class="txt_nfe_pedido_item_tributos_cofins_calculo_tipo" id="txt_nfe_pedido_item_tributos_cofins_calculo_tipo_0" name="txt_nfe_pedido_item_tributos_cofins_calculo_tipo" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_cofins_calculo_tipo" id="hid_nfe_pedido_item_tributos_cofins_calculo_tipo" name="hid_nfe_pedido_item_tributos_cofins_calculo_tipo" value="" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_cofins_valor_base_calculo" id="txt_nfe_pedido_item_tributos_cofins_valor_base_calculo_0" name="txt_nfe_pedido_item_tributos_cofins_valor_base_calculo" style="width:100px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_cofins_aliquota_percentual" id="txt_nfe_pedido_item_tributos_cofins_aliquota_percentual_0" name="txt_nfe_pedido_item_tributos_cofins_aliquota_percentual" style="width:130px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_cofins_aliquota_reais" id="txt_nfe_pedido_item_tributos_cofins_aliquota_reais_0" name="txt_nfe_pedido_item_tributos_cofins_aliquota_reais" style="width:130px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>	
                                    <input type="text" class="txt_nfe_pedido_item_tributos_cofins_quantidade_vendida" id="txt_nfe_pedido_item_tributos_cofins_quantidade_vendida_0" name="txt_nfe_pedido_item_tributos_cofins_quantidade_vendida" style="width:120px;text-align:right" value="" readonly="readonly" /> 
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_cofins_valor" id="txt_nfe_pedido_item_tributos_cofins_valor_0" name="txt_nfe_pedido_item_tributos_cofins_valor" style="width:100px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:120px;text-align:center" class="txt_nfe_pedido_item_tributos_cofins_st_calculo_tipo" id="txt_nfe_pedido_item_tributos_cofins_st_calculo_tipo_0" name="txt_nfe_pedido_item_tributos_cofins_st_calculo_tipo" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_cofins_st_calculo_tipo" id="hid_nfe_pedido_item_tributos_cofins_st_calculo_tipo" name="hid_nfe_pedido_item_tributos_cofins_st_calculo_tipo" value="" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_cofins_st_valor_base_calculo" id="txt_nfe_pedido_item_tributos_cofins_st_valor_base_calculo_0" name="txt_nfe_pedido_item_tributos_cofins_st_valor_base_calculo" style="width:120px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_cofins_st_aliquota_percentual" id="txt_nfe_pedido_item_tributos_cofins_st_aliquota_percentual_0" name="txt_nfe_pedido_item_tributos_cofins_st_aliquota_percentual" style="width:140px;" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_cofins_st_aliquota_reais" id="txt_nfe_pedido_item_tributos_cofins_st_aliquota_reais_0" name="txt_nfe_pedido_item_tributos_cofins_st_aliquota_reais" style="width:120px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>	
                                    <input type="text" class="txt_nfe_pedido_item_tributos_cofins_st_quantidade_vendida" id="txt_nfe_pedido_item_tributos_cofins_st_quantidade_vendida_0" name="txt_nfe_pedido_item_tributos_cofins_st_quantidade_vendida" style="width:140px;text-align:right" value="" readonly="readonly" /> 
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_cofins_st_valor" id="txt_nfe_pedido_item_tributos_cofins_st_valor_0" name="txt_nfe_pedido_item_tributos_cofins_st_valor" style="width:120px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <!--tributos ii-->
                                <li>
                                    <input type="text" style="width:120px;text-align:right" class="txt_nfe_pedido_item_tributos_ii_valor_base_calculo" id="txt_nfe_pedido_item_tributos_ii_valor_base_calculo_0" name="txt_nfe_pedido_item_tributos_ii_valor_base_calculo" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:150px;text-align:right" class="txt_nfe_pedido_item_tributos_ii_valor_aduaneiras" id="txt_nfe_pedido_item_tributos_ii_valor_aduaneiras_0" name="txt_nfe_pedido_item_tributos_ii_valor_aduaneiras" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:70px;text-align:right" class="txt_nfe_pedido_item_tributos_ii_valor_iof" id="txt_nfe_pedido_item_tributos_ii_valor_iof_0" name="txt_nfe_pedido_item_tributos_ii_valor_iof" value="" readonly="readonly" />
                                </li>
                                <li>	
                                    <input type="text" class="txt_nfe_pedido_item_tributos_ii_valor" id="txt_nfe_pedido_item_tributos_ii_valor_0" name="txt_nfe_pedido_item_tributos_ii_valor" style="width:70px;text-align:right" value="" readonly="readonly" /> 
                                </li>
                                <!--tributos ipi-->
                                <li>
                                    <input type="text" style="width:100px;text-align:left" class="txt_nfe_pedido_item_tributos_ipi_situacao_tributaria" id="txt_nfe_pedido_item_tributos_ipi_situacao_tributaria_0" name="txt_nfe_pedido_item_tributos_ipi_situacao_tributaria" value="" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_ipi_situacao_tributaria" id="hid_nfe_pedido_item_tributos_ipi_situacao_tributaria" name="hid_nfe_pedido_item_tributos_ipi_situacao_tributaria" value="" />
                                </li>
                                <li>	
                                    <input type="text" class="txt_nfe_pedido_item_tributos_ipi_enquadramento_classe" id="txt_nfe_pedido_item_tributos_ipi_enquadramento_classe_0" name="txt_nfe_pedido_item_tributos_ipi_enquadramento_classe" style="width:140px" value="" readonly="readonly" /> 
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_ipi_enquadramento_codigo" id="txt_nfe_pedido_item_tributos_ipi_enquadramento_codigo_0" name="txt_nfe_pedido_item_tributos_ipi_enquadramento_codigo" style="width:170px" value="" readonly="readonly"/>
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_ipi_produtor_cnpj" id="txt_nfe_pedido_item_tributos_ipi_produtor_cnpj_0" name="txt_nfe_pedido_item_tributos_ipi_produtor_cnpj" style="width:120px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_ipi_selo_controle_codigo" id="txt_nfe_pedido_item_tributos_ipi_selo_controle_codigo_0" name="txt_nfe_pedido_item_tributos_ipi_selo_controle_codigo" style="width:140px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_ipi_selo_controle_quantidade" id="txt_nfe_pedido_item_tributos_ipi_selo_controle_quantidade_0" name="txt_nfe_pedido_item_tributos_ipi_selo_controle_quantidade" style="width:140px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:120px;text-align:center" class="txt_nfe_pedido_item_tributos_ipi_calculo_tipo" id="txt_nfe_pedido_item_tributos_ipi_calculo_tipo_0" name="txt_nfe_pedido_item_tributos_ipi_calculo_tipo" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_ipi_calculo_tipo" id="hid_nfe_pedido_item_tributos_ipi_calculo_tipo" name="hid_nfe_pedido_item_tributos_ipi_calculo_tipo" value="" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_ipi_valor_base_calculo" id="txt_nfe_pedido_item_tributos_ipi_valor_base_calculo_0" name="txt_nfe_pedido_item_tributos_ipi_valor_base_calculo" style="width:70px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_ipi_aliquota" id="txt_nfe_pedido_item_tributos_ipi_aliquota_0" name="txt_nfe_pedido_item_tributos_ipi_aliquota" style="width:120px;text-align:right" value="" readonly="readonly"/>
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_ipi_unidade_padrao_quantidade_total" id="txt_nfe_pedido_item_tributos_ipi_unidade_padrao_quantidade_total_0" name="txt_nfe_pedido_item_tributos_ipi_unidade_padrao_quantidade_total" style="width:150px;text-align:right"  readonly="readonly"/>
                                </li>
                                <li>
                                    <input type="text" style="width:120px;text-align:right" class="txt_nfe_pedido_item_tributos_ipi_unidade_valor" id="txt_nfe_pedido_item_tributos_ipi_unidade_valor_0" name="txt_nfe_pedido_item_tributos_ipi_unidade_valor" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:70px;text-align:right" class="txt_nfe_pedido_item_tributos_ipi_valor" id="txt_nfe_pedido_item_tributos_ipi_valor_0" name="txt_nfe_pedido_item_tributos_ipi_valor" value="" readonly="readonly" />
                                </li>
                                <!--tributos issqn-->
                                <li>
                                    <input type="text" style="width:120px;text-align:left" class="txt_nfe_pedido_item_tributos_issqn_tributacao" id="txt_nfe_pedido_item_tributos_issqn_tributacao_0" name="txt_nfe_pedido_item_tributos_issqn_tributacao" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_issqn_tributacao"  id="hid_nfe_pedido_item_tributos_issqn_tributacao" name="hid_nfe_pedido_item_tributos_issqn_tributacao" value="" />
                                </li>
                                <li>
                                    <input type="text" style="width:120px;text-align:right" class="txt_nfe_pedido_item_tributos_issqn_valor_base_calculo" id="txt_nfe_pedido_item_tributos_issqn_valor_base_calculo_0" name="txt_nfe_pedido_item_tributos_issqn_valor_base_calculo" value="" readonly="readonly" />
                                </li>
                                
                                <li>
                                    <input type="text" style="width:120px;text-align:right" class="txt_nfe_pedido_item_tributos_issqn_aliquota" id="txt_nfe_pedido_item_tributos_issqn_aliquota_0" name="txt_nfe_pedido_item_tributos_issqn_aliquota" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:140px;text-align:left" class="txt_nfe_pedido_item_tributos_issqn_servico_lista" id="txt_nfe_pedido_item_tributos_issqn_servico_lista_0" name="txt_nfe_pedido_item_tributos_issqn_servico_lista" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_issqn_servico_lista" id="hid_nfe_pedido_item_tributos_issqn_servico_lista" name="hid_nfe_pedido_item_tributos_issqn_servico_lista" value="" />
                                </li>
                                <li>
                                    <input type="text"  style="width:70px;text-align:left" class="txt_nfe_pedido_item_tributos_issqn_uf" id="txt_nfe_pedido_item_tributos_issqn_uf_0" name="txt_nfe_pedido_item_tributos_issqn_uf" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_issqn_uf" id="hid_nfe_pedido_item_tributos_issqn_uf" name="hid_nfe_pedido_item_tributos_issqn_uf" value="" />
                                </li>
                                <li>
                                    <input type="text" style="width:140px;text-align:left" class="txt_nfe_pedido_item_tributos_issqn_municipio" id="txt_nfe_pedido_item_tributos_issqn_municipio_0" name="txt_nfe_pedido_item_tributos_issqn_municipio" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_issqn_municipio" id="hid_nfe_pedido_item_tributos_issqn_municipio" name="hid_nfe_pedido_item_tributos_issqn_municipio" value="" />
                                </li>
                                <li>
                                    <input type="text" style="width:80px;text-align:right" class="txt_nfe_pedido_item_tributos_issqn_valor" id="txt_nfe_pedido_item_tributos_issqn_valor_0" name="txt_nfe_pedido_item_tributos_issqn_valor" value="" readonly="readonly" />
                                </li>
                                <!--tributos pis-->
                                <li>
                                    <input type="text" style="width:100px;text-align:left" class="txt_nfe_pedido_item_tributos_pis_situacao_tributaria" id="txt_nfe_pedido_item_tributos_pis_situacao_tributaria_0" name="txt_nfe_pedido_item_tributos_pis_situacao_tributaria" value="" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_pis_situacao_tributaria" id="hid_nfe_pedido_item_tributos_pis_situacao_tributaria" name="hid_nfe_pedido_item_tributos_pis_situacao_tributaria" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:100px;text-align:left" class="txt_nfe_pedido_item_tributos_pis_calculo_tipo" id="txt_nfe_pedido_item_tributos_pis_calculo_tipo_0" name="txt_nfe_pedido_item_tributos_pis_calculo_tipo" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_pis_calculo_tipo" id="hid_nfe_pedido_item_tributos_pis_calculo_tipo" name="hid_nfe_pedido_item_tributos_pis_calculo_tipo" value="" />         
                                </li>
                                <li>
                                    <input type="text" style="width:100px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_valor_base_calculo" id="txt_nfe_pedido_item_tributos_pis_valor_base_calculo_0" name="txt_nfe_pedido_item_tributos_pis_valor_base_calculo" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:110px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_aliquota_percentual" id="txt_nfe_pedido_item_tributos_pis_aliquota_percentual_0" name="txt_nfe_pedido_item_tributos_pis_aliquota_percentual" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:100px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_aliquota_reais" id="txt_nfe_pedido_item_tributos_pis_aliquota_reais_0" name="txt_nfe_pedido_item_tributos_pis_aliquota_reais" value="" readonly="readonly" />
                                </li>
                                <li>	
                                    <input type="text" style="width:100px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_quantidade_vendida" id="txt_nfe_pedido_item_tributos_pis_quantidade_vendida_0" name="txt_nfe_pedido_item_tributos_pis_quantidade_vendida" value="" readonly="readonly" /> 
                                </li>
                                <li>
                                    <input type="text" style="width:60px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_valor" id="txt_nfe_pedido_item_tributos_pis_valor_0" name="txt_nfe_pedido_item_tributos_pis_valor" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:120px;text-align:center" class="txt_nfe_pedido_item_tributos_pis_st_calculo_tipo" id="txt_nfe_pedido_item_tributos_pis_st_calculo_tipo_0" name="txt_nfe_pedido_item_tributos_pis_st_calculo_tipo" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_pis_st_calculo_tipo" id="hid_nfe_pedido_item_tributos_pis_st_calculo_tipo" name="hid_nfe_pedido_item_tributos_pis_st_calculo_tipo" value="" />  
                                </li>
                                <li>
                                    <input type="text" style="width:100px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_st_valor_base_calculo" id="txt_nfe_pedido_item_tributos_pis_st_valor_base_calculo_0" name="txt_nfe_pedido_item_tributos_pis_st_valor_base_calculo" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:120px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_st_aliquota_percentual" id="txt_nfe_pedido_item_tributos_pis_st_aliquota_percentual_0" name="txt_nfe_pedido_item_tributos_pis_st_aliquota_percentual" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:120px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_st_aliquota_reais" id="txt_nfe_pedido_item_tributos_pis_st_aliquota_reais_0" name="txt_nfe_pedido_item_tributos_pis_st_aliquota_reais" value="" readonly="readonly" />
                                </li>
                                <li>	
                                    <input type="text" style="width:120px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_st_quantidade_vendida" id="txt_nfe_pedido_item_tributos_pis_st_quantidade_vendida_0" name="txt_nfe_pedido_item_tributos_pis_st_quantidade_vendida" value="" readonly="readonly" /> 
                                </li>
                                <li>
                                    <input type="text" style="width:70px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_st_valor" id="txt_nfe_pedido_item_tributos_pis_st_valor_0" name="txt_nfe_pedido_item_tributos_pis_st_valor" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:200px;text-align:left" class="txt_nfe_item_pedido_informacoes_adicionais" id="txt_nfe_item_pedido_informacoes_adicionais_0" name="txt_nfe_item_pedido_informacoes_adicionais" value="" readonly="readonly" />
                                </li>
                                <!--- PRODUTO ESPECIFICO -->
                                <li>
                                    <input type="text" 	 style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico" 							id="txt_nfe_item_produto_especifico_0" 								name="txt_nfe_item_produto_especifico" 							value="" readonly="readonly" />
                                    <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_id"							id="txt_nfe_item_produto_especifico_id_0" 							name="txt_nfe_item_produto_especifico_id" 						value="" readonly="readonly" />
                                    <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_combustivel_anp" 			id="txt_nfe_item_produto_especifico_combustivel_anp_0" 				name="txt_nfe_item_produto_especifico_combustivel_anp" 			value="" readonly="readonly" />
                                    <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_combustivel_if" 			id="txt_nfe_item_produto_especifico_combustivel_if_0" 				name="txt_nfe_item_produto_especifico_combustivel_if" 			value="" readonly="readonly" />
                                    <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_combustivel_uf" 			id="txt_nfe_item_produto_especifico_combustivel_uf_0" 				name="txt_nfe_item_produto_especifico_combustivel_uf" 			value="" readonly="readonly" />
                                    <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_combustivel_qtd_faturada" 	id="txt_nfe_item_produto_especifico_combustivel_qtd_faturada_0" 	name="txt_nfe_item_produto_especifico_combustivel_qtd_faturada" value="" readonly="readonly" />
                                    <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_combustivel_cide_bc" 		id="txt_nfe_item_produto_especifico_combustivel_cide_bc_0" 			name="txt_nfe_item_produto_especifico_combustivel_cide_bc" 		value="" readonly="readonly" />
                                    <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_combustivel_cide_aliquota" 	id="txt_nfe_item_produto_especifico_combustivel_cide_aliquota_0"	name="txt_nfe_item_produto_especifico_combustivel_cide_aliquota"value="" readonly="readonly" />
                                    <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_combustivel_cide_valor" 	id="txt_nfe_item_produto_especifico_combustivel_cide_valor_0" 		name="txt_nfe_item_produto_especifico_combustivel_cide_valor" 	value="" readonly="readonly" />
                                    <!-- TRIBUTO -->
                                    <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_total_tributos" id="txt_nfe_item_produto_especifico_total_tributos_0" name="txt_nfe_item_produto_especifico_total_tributos" value="" readonly="readonly" />
                                </li>
                                <!--- PRODUTO ESPECIFICO -->
                            </ul>  
                        </div>
<?
						$rsItem = mysql_query("SELECT tbl{$documento_tipo}_item.*, tbl{$documento_tipo}_item.fldProduto_Id as fldProdutoId, tblpedido_item_fiscal.*
											  FROM tbl{$documento_tipo}_item LEFT JOIN tblpedido_item_fiscal ON tbl{$documento_tipo}_item.fldId = tblpedido_item_fiscal.fldItem_Id
											  WHERE tbl{$documento_tipo}_item.fld{$documento_tipo}_Id = $documento_id");
											
						echo mysql_error();
						$rows = mysql_num_rows($rsItem);
						$i = 1;
						while($rowItem = mysql_fetch_array($rsItem)){
							echo mysql_error();	
							$rsProduto = mysql_query("SELECT tblproduto.*, 
													 tblproduto_unidade_medida.fldSigla
													 FROM tblproduto LEFT JOIN tblproduto_unidade_medida ON tblproduto.fldUN_Medida_Id = tblproduto_unidade_medida.fldId
													 WHERE tblproduto.fldId = " . $rowItem['fldProdutoId']);
							$rowProduto = mysql_fetch_array($rsProduto);
							
							echo mysql_error();
							$rowEstoque = mysql_fetch_array(mysql_query("SELECT tblproduto_estoque.fldId, tblproduto_estoque.fldNome
														FROM tblproduto_estoque INNER JOIN tblpedido_item ON tblproduto_estoque.fldId = tblpedido_item.fldEstoque_Id
														WHERE tblproduto_estoque.fldId = ".$rowItem['fldEstoque_Id']."
							"));
							
							echo mysql_error();
							$item_id 		= $rowItem['fldId'];
							
							$valor_item 	= $rowItem['fldValor'];
							$qtd 			= $rowItem['fldQuantidade'];
							$total_item 	= $valor_item * $qtd;
							$desconto 		= $rowItem['fldDesconto'];
							$totalDesconto 	= ($total_item * $desconto) / 100;
							$totalValor 	= $total_item - $totalDesconto;
							$produto_id 	= $rowItem['fldProdutoId'];
							
							########################################################
							if($_SESSION["sistema_tipo"] == "otica"){
						
								if($otica_retorno['perto']['armacao'] == $item_id){
									$Otica_Perto_Armacao_Descricao = $rowItem['fldDescricao'];
									$Otica_Perto_Armacao = $i;
								}
								if($otica_retorno['perto']['material'] == $item_id){
									$Otica_Perto_Material_Descricao = $rowItem['fldDescricao'];
									$Otica_Perto_Material = $i;
								}
								if($otica_retorno['longe']['armacao'] == $item_id){
									$Otica_Longe_Armacao_Descricao = $rowItem['fldDescricao'];
									$Otica_Longe_Armacao = $i;
								}
								if($otica_retorno['longe']['material'] == $item_id){
									$Otica_Longe_Material_Descricao = $rowItem['fldDescricao'];
									$Otica_Longe_Material = $i;
								}
								if($otica_retorno['bifocal']['armacao'] == $item_id){
									$Otica_Bifocal_Armacao_Descricao = $rowItem['fldDescricao'];
									$Otica_Bifocal_Armacao = $i;
								}
								if($otica_retorno['bifocal']['material'] == $item_id){
									$Otica_Bifocal_Material_Descricao = $rowItem['fldDescricao'];
									$Otica_Bifocal_Material = $i;
								}
							}
							
							//NFE ####################################################################################
						
							$itemTotalBruto 			= $rowItem['fldvalor_total_bruto'];
							$itemCompoeBruto 			= $rowItem['fldvalor_bruto_compoe_total'];
							$itemChkBruto 				= ($itemCompoeBruto == 0 )? $itemCompoeBruto : '1';
							$ncm 						= $rowItem['fldncm'];
							$ex_tipi 					= $rowItem['fldex_tipi'];
							$cfop 						= $rowItem['fldcfop'];
							$unid_comercial 			= $rowItem['fldunidade_comercial'];
							$unid_tributavel 			= $rowItem['fldunidade_tributavel'];
							$qtde_tributavel 			= $rowItem['fldquantidade_tributavel'];
							$valor_unit_tributavel 		= $rowItem['fldvalor_unitario_tributavel'];
							$total_seguro 				= $rowItem['fldtotal_seguro'];
							$desconto					= $rowItem['flddesconto'];
							$frete 						= $rowItem['fldfrete'];
							$ean 						= $rowItem['fldean'];
							$ean_unid_tributavel 		= $rowItem['fldean_unidade_tributavel'];
							$outras_despesas			= $rowItem['fldoutras_despesas_acessorias'];
							$pedido_compra 				= $rowItem['fldpedido_compra'];
							$numero_pedido_compra 		= $rowItem['fldnumero_item_pedido_compra'];
							$produto_especifico 		= $rowItem['fldproduto_especifico'];
							$icms_sit_tributaria 		= $rowItem['fldicms_situacao_tributaria'];
							$rad_imposto				= $rowItem['fldrad_imposto'];
							$icms_origem		 		= $rowItem['fldicms_origem'];
							$icms_aliq_aplicavel		= $rowItem['fldicms_calculo_credito_aliquota_aplicavel'];
							$icms_cred_aproveitado		= $rowItem['fldicms_credito_aproveitado'];
							$icms_bc_modalidade			= $rowItem['fldicms_base_calculo_modalidade'];
							$icms_base_calculo			= $rowItem['fldicms_base_calculo'];
							$icms_bc_perc_reducao		= $rowItem['fldicms_base_calculo_percentual_reducao'];
							$icms_aliquota				= $rowItem['fldicms_aliquota'];
							$icms_valor					= $rowItem['fldicms_valor'];
							$icms_bc_op_propria			= $rowItem['fldicms_base_calculo_operacao_propria'];
							$icms_desoneracao_motiv 	= $rowItem['fldicms_desoneracao_motivo'];
							$icmsst_bc_modalidade		= $rowItem['fldicmsst_base_calculo_modalidade'];
							$icmsst_bc_perc_reducao		= $rowItem['fldicmsst_base_calculo_percentual_reducao'];
							$icmsst_mva					= $rowItem['fldicmsst_mva'];
							$icmsst_base_calculo		= $rowItem['fldicmsst_base_calculo'];
							$icmsst_aliquota			= $rowItem['fldicmsst_aliquota'];
							$icmsst_valor				= $rowItem['fldicmsst_valor'];
							$icmsst_uf					= $rowItem['fldicmsst_uf'];
							$icmsst_bc_ret_uf_rem		= $rowItem['fldicmsst_base_calculo_retido_uf_remetente'];
							$icmsst_val_ret_uf_rem		= $rowItem['fldicmsst_valor_retido_uf_remetente'];
							$icmsst_bc_uf_destino		= $rowItem['fldicmsst_base_calculo_uf_destino'];
							$icmsst_valor_uf_destino	= $rowItem['fldicmsst_valor_uf_destino'];
							$cofins_sit_tributaria		= $rowItem['fldcofins_situacao_tributaria'];
							$cofins_tipo_calculo		= $rowItem['fldcofins_tipo_calculo'];
							$cofins_base_calculo		= $rowItem['fldcofins_base_calculo'];
							$cofins_aliq_percent		= $rowItem['fldcofins_aliquota_percentual'];
							$cofins_aliq_reais			= $rowItem['fldcofins_aliquota_reais'];
							$cofins_qtde_vendida		= $rowItem['fldcofins_quantidade_vendida'];
							$cofins_valor				= $rowItem['fldcofins_valor'];
							$cofinsst_tipo_calculo		= $rowItem['fldcofinsst_tipo_calculo'];
							$cofinsst_base_calculo		= $rowItem['fldcofinsst_base_calculo'];
							$cofinsst_aliq_percent		= $rowItem['fldcofinsst_aliquota_percentual'];
							$cofinsst_aliq_reais		= $rowItem['fldcofinsst_aliquota_reais'];
							$cofinsst_qtde_vendida		= $rowItem['fldcofinsst_quantidade_vendida'];
							$cofinsst_valor				= $rowItem['fldcofinsst_valor'];
							$ii_base_calculo			= $rowItem['fldii_base_calculo'];
							$ii_desp_aduaneiras_val		= $rowItem['fldii_despesas_aduaneiras_valor'];
							$ii_iof_valor				= $rowItem['fldii_iof_valor'];
							$ii_valor					= $rowItem['fldii_valor'];
							$ipi_sit_tributaria			= $rowItem['fldipi_situacao_tributaria'];
							$ipi_enquadra_classe		= $rowItem['fldipi_enquadramento_classe'];
							$ipi_enquadra_codigo		= $rowItem['fldipi_enquadramento_codigo'];
							$ipi_produtor_cnpj			= $rowItem['fldipi_produtor_cnpj'];
							$ipi_selo_controle_cod		= $rowItem['fldipi_selo_controle_codigo'];
							$ipi_selo_controle_qtde		= $rowItem['fldipi_selo_controle_quantidade'];
							$ipi_calculo_tipo			= $rowItem['fldipi_calculo_tipo'];
							$ipi_base_calculo			= $rowItem['fldipi_base_calculo'];
							$ipi_aliquota				= $rowItem['fldipi_aliquota'];
							$ipi_unid_qtde_total		= $rowItem['fldipi_unidade_quantidade_total'];
							$ipi_unidade_valor			= $rowItem['fldipi_unidade_valor'];
							$ipi_valor					= $rowItem['fldipi_valor'];
							$issqn_sit_tributaria		= $rowItem['fldissqn_tributacao'];
							$issqn_base_calculo			= $rowItem['fldissqn_base_calculo'];
							$issqn_aliquota				= $rowItem['fldissqn_aliquota'];
							$issqn_lista_servico		= $rowItem['fldissqn_lista_servico'];
							$issqn_uf					= $rowItem['fldissqn_uf'];
							$issqn_mun_ocorrendia		= $rowItem['fldissqn_municipio_ocorrencia'];
							$issqn_valor				= $rowItem['fldissqn_valor'];
							$pis_sit_tributaria			= $rowItem['fldpis_situacao_tributaria'];
							$pis_calculo_tipo			= $rowItem['fldpis_tipo_calculo'];
							$pis_base_calculo			= $rowItem['fldpis_base_calculo'];
							$pis_aliq_percentual		= $rowItem['fldpis_aliquota_percentual'];
							$pis_aliq_reais				= $rowItem['fldpis_aliquota_reais'];
							$pis_qtde_vendida			= $rowItem['fldpis_quantidade_vendida'];
							$pis_valor					= $rowItem['fldpis_valor'];
							$pisst_tipo_calculo			= $rowItem['fldpisst_tipo_calculo'];
							$pisst_base_calculo			= $rowItem['fldpisst_base_calculo'];
							$pisst_aliq_percent			= $rowItem['fldpisst_aliquota_percentual'];
							$pisst_aliq_reais			= $rowItem['fldpisst_aliquota_reais'];
							$pisst_qtde_vendida			= $rowItem['fldpisst_quantidade_vendida'];
							$pisst_valor				= $rowItem['fldpisst_valor'];
							$info_adicionais			= $rowItem['fldinformacoes_adicionais'];
							$prod_especifico_id 		= $rowItem['fldTipo_Especifico'];
                            $combustivel_anp 			= $rowItem['fldCombustivel_Codigo_ANP'];
                            $combustivel_if 			= $rowItem['fldCombustivel_Codigo_CodIF'];
							$combustivel_uf 			= $rowItem['fldCombustivel_UF'];
                            $combustivel_qtd_faturada 	= $rowItem['fldCombustivel_Qtd_Faturada'];
							$combustivel_cide_bc 		= $rowItem['fldCombustivel_Cide_bCalculo'];
                            $combustivel_cide_aliquota 	= $rowItem['fldCombustivel_Cide_Aliquota'];
							$combustivel_cide_valor 	= $rowItem['fldCombustivel_Cide_Valor'];
							$total_tributos				= $rowItem['fldTotal_Tributo'];

							
							if($prod_especifico_id){
								$sqlProd_Especifico_Nome = mysql_query("SELECT fldTipo FROM tblproduto_fiscal_tipo_especifico WHERE fldId = ".$prod_especifico_id." LIMIT 1");
								$rowProd_Especifico_Nome = mysql_fetch_assoc($sqlProd_Especifico_Nome);
								$prod_especifico 		 = $rowProd_Especifico_Nome['fldTipo'];
							}
?>					
                            <ul id="pedido_lista_item" style="width: 11080px">
								<? if($venda_origem == 0){ ?>                           
                                        <li style="width:20px;">
                                            <a class="a_excluir" id="excluir_<?=$i?>" href="" title="Excluir item" ></a>
                                        </li>
                                <?	}						?>
                                <li style="width:20px">
                                    <a class="edit edit_js modal" id="edit_<?=$i?>" href="pedido_nfe_item,<?=$produto_id.",".$i.",".$venda_origem?>" name='edit_<?=$i?>' rel='980-500' title="Editar item"></a>
                                </li>
                                <?	if($documento_tipo == 'pedido'){ 	?>
                                        <li style="width:20px;background:#FFF">
                                            <input class="chk_entregue"			type="checkbox" id="chk_pedido_<?=$i?>" 			name="chk_entregue_<?=$i?>" <?= ($rowItem['fldEntregue'] == 1) ? "checked='checked'" : ''?>  style="width:20px" title="item entregue" onclick="return false" />
                                        </li>
                                <?	}									?>
                                <li>
                                    <input type="text" class="txt_item_codigo"  style="width:80px" id="txt_item_codigo_<?=$i?>" name="txt_item_codigo_<?=$i?>" value="<?=$rowProduto['fldCodigo']?>" readonly="readonly"/>
                                    <input class="hid_pedido_item_id" 	type="hidden" 	id="hid_pedido_item_id_<?=$i?>" 	name="hid_pedido_item_id_<?=$i?>"	value="<?=$rowItem['fldId']?>" />
                                    <input class="hid_item_produto_id" 	type="hidden" 	id="hid_item_produto_id_<?=$i?>"	name="hid_item_produto_id_<?=$i?>" 	value="<?=$rowProduto['fldId']?>" />
                                    <input class="hid_item_detalhe" 	type="hidden" 	id="hid_item_detalhe_<?=$i?>" 		name="hid_item_detalhe_<?=$i?>" 	value="1" /><? //parametro pra identificar item que ja estava na venda ?>
                                </li>
                                <li>
                                    <input type="text" class="txt_item_nome" id="txt_item_nome_<?=$i?>" name="txt_item_nome_<?=$i?>" style="width:300px;text-align:left" value="<?=$rowItem['fldDescricao']?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input class="txt_item_fardo" 		type="text" 	id="txt_item_fardo_<?=$i?>" 		name="txt_item_fardo_<?=$i?>" 		value="<?=$rowFardo['fldCodigo']?>" style="width:45px" readonly="readonly" />
                                    <input class="hid_item_fardo" 		type="hidden" 	id="hid_item_fardo_<?=$i?>" 		name="hid_item_fardo_<?=$i?>" 		value="<?=$rowFardo['fldId']?>" />
                                </li>
                                <li>
									<?	if($rowItem['fldTabela_Preco_Id'] != NULL){
												$rowTabela 	= mysql_fetch_array(mysql_query("SELECT * FROM tblproduto_tabela WHERE fldId = ".$rowItem['fldTabela_Preco_Id'])); 
                                        		$sigla 		= $rowTabela['fldSigla'];
										}else{	$sigla 		= 'padr&atilde;o';			}
                                    ?>
                                    <input class="txt_tabela_sigla" 	type="text" 	id="txt_tabela_sigla_<?=$i?>" 		name="txt_tabela_sigla_<?=$i?>" 	value="<?=$sigla?>" style="width:50px" readonly="readonly" />
                                    <input class="hid_item_tabela_preco"type="hidden" 	id="hid_item_tabela_preco_<?=$i?>" 	name="hid_item_tabela_preco_<?=$i?>"value="<?=$rowItem['fldTabela_Preco_Id']?>" />
                                </li>
                                <li>
                                    <input type="text" class="txt_item_valor"  id="txt_item_valor_<?=$i?>" name="txt_item_valor_<?=$i?>" style="width:80px; text-align:right" value="<?=format_number_out($rowItem['fldValor'],$vendaDecimal)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_item_quantidade" style="width:55px; text-align: right" id="txt_item_quantidade_<?=$i?>" name="txt_item_quantidade_<?=$i?>" value="<?=format_number_out($rowItem['fldQuantidade'],$qtdeDecimal)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:60px" class="txt_item_estoque_nome" id="txt_item_estoque_nome_<?=$i?>" name="txt_item_estoque_nome_<?=$i?>" value="<?=$rowEstoque['fldNome']?>" readonly="readonly" />
                                    <input type="hidden" class="hid_item_estoque_id" id="hid_item_estoque_id_<?=$i?>" name="hid_item_estoque_id_<?=$i?>" value="<?=$rowEstoque['fldId']?>" />
                                </li>
                                <li>
                                    <input type="text" class="txt_item_UM" id="txt_item_UM_<?=$i?>" name="txt_item_UM_<?=$i?>" style="width:30px;text-align:center" value="<?=$rowProduto['fldSigla']?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_item_desconto" style="width:45px; text-align: right" id="txt_item_desconto_<?=$i?>" name="txt_item_desconto_<?=$i?>" value="<?=format_number_out($rowItem['fldDesconto'])?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_item_subtotal" style="width:73px; text-align: right" id="txt_item_subtotal_<?=$i?>" name="txt_item_subtotal_<?=$i?>" value="<?=format_number_out($totalValor)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_item_referencia"style="width:70px; text-align:right" id="txt_item_referencia_<?=$i?>"name="txt_item_referencia_<?=$i?>"value="<?=$rowItem['fldReferencia']?>" 	readonly="readonly" />
                                </li>
                                
                                <!-- nfe -->
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_ncm"	id="txt_nfe_pedido_item_principal_ncm_<?=$i?>" 		name="txt_nfe_pedido_item_principal_ncm_<?=$i?>" 	style="width: 70px" value="<?=$ncm?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_ex_tipi"id="txt_nfe_pedido_item_principal_ex_tipi_<?=$i?>"	name="txt_nfe_pedido_item_principal_ex_tipi_<?=$i?>"style="width: 70px" value="<?=$ex_tipi?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_cfop" 	id="txt_nfe_pedido_item_principal_cfop_<?=$i?>" 	name="txt_nfe_pedido_item_principal_cfop_<?=$i?>" 	style="width: 70px" value="<?=$cfop?>" readonly="readonly" />
    
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_unidade_comercial" id="txt_nfe_pedido_item_principal_unidade_comercial_<?=$i?>" name="txt_nfe_pedido_item_principal_unidade_comercial_<?=$i?>" style="width: 90px; text-align:center" value="<?=$unid_comercial?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_unidade_tributavel" id="txt_nfe_pedido_item_principal_unidade_tributavel_<?=$i?>" name="txt_nfe_pedido_item_principal_unidade_tributavel_<?=$i?>" style="width: 70px;text-align:center" value="<?=$unid_tributavel?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_quantidade_tributavel" id="" name="txt_nfe_pedido_item_principal_quantidade_tributavel_<?=$i?>" style="width: 70px;text-align:right" value="<?=format_number_out($qtde_tributavel)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_valor_unidade_tributavel" id="txt_nfe_pedido_item_principal_valor_unidade_tributavel_<?=$i?>" name="txt_nfe_pedido_item_principal_valor_unidade_tributavel_<?=$i?>" style="width: 110px;text-align:right" value="<?=format_number_out($valor_unit_tributavel,$vendaDecimal)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_total_seguro" id="txt_nfe_pedido_item_principal_total_seguro_<?=$i?>" name="txt_nfe_pedido_item_principal_total_seguro_<?=$i?>" style="width: 70px;text-align:right" value="<?=format_number_out($total_seguro)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_desconto" style="width:85px; text-align: right" id="txt_nfe_pedido_item_principal_desconto_<?=$i?>" name="txt_nfe_pedido_item_principal_desconto_<?=$i?>" value="<?=format_number_out($desconto)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_frete" id="txt_nfe_pedido_item_principal_frete_<?=$i?>" name="txt_nfe_pedido_item_principal_frete_<?=$i?>" style="width: 70px;text-align:right" value="<?=format_number_out($frete)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_ean" id="txt_nfe_pedido_item_principal_ean_<?=$i?>" name="txt_nfe_pedido_item_principal_ean_<?=$i?>" style="width: 70px" value="<?=$ean?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_ean_tributavel" id="txt_nfe_pedido_item_principal_ean_tributavel_<?=$i?>" name="txt_nfe_pedido_item_principal_ean_tributavel_<?=$i?>" style="width: 70px;text-align:right" value="<?=$ean_unid_tributavel?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_outras_despesas_acessorias" id="txt_nfe_pedido_item_principal_outras_despesas_acessorias_<?=$i?>" name="txt_nfe_pedido_item_principal_outras_despesas_acessorias_<?=$i?>" style="width: 120px;text-align:right" value="<?=format_number_out($outras_despesas)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_valor_total_bruto" id="txt_nfe_pedido_item_principal_valor_total_bruto_<?=$i?>" name="txt_nfe_pedido_item_principal_valor_total_bruto_<?=$i?>" style="width: 90px;text-align:right" value="<?=format_number_out($itemTotalBruto)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_pedido_compra" id="txt_nfe_pedido_item_principal_pedido_compra_<?=$i?>" name="txt_nfe_pedido_item_principal_pedido_compra_<?=$i?>" style="width: 70px;text-align:right" value="<?=$pedido_compra?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_numero_item_pedido_compra" id="txt_nfe_pedido_item_principal_numero_item_pedido_compra_<?=$i?>" name="txt_nfe_pedido_item_principal_numero_item_pedido_compra_<?=$i?>" style="width: 110px;text-align:right" value="<?=$numero_pedido_compra?>" readonly="readonly" />
                                </li>
                                <!--
                                <li>
                                    <input type="text" style="width:70px;text-align:left" class="txt_nfe_pedido_item_principal_produto_especifico" id="txt_nfe_pedido_item_principal_produto_especifico_<?=$i?>" name="txt_nfe_pedido_item_principal_produto_especifico_<?=$i?>"  value="<?=$produto_especifico?>" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_principal_produto_especifico"  id="hid_nfe_pedido_item_principal_produto_especifico" name="hid_nfe_pedido_item_principal_produto_especifico_<?=$i?>" value="<?=$rowItem['fldproduto_especifico']?>" />  
                                </li>
                                -->
                                <li>
                                    <input type="checkbox" class="chk_nfe_pedido_item_principal_valor_total_bruto" id="chk_nfe_pedido_item_principal_valor_total_bruto_<?=$i?>" name="chk_nfe_pedido_item_principal_valor_total_bruto_<?=$i?>" style="width: 110px;text-align:left" <?= ($itemChkBruto == '1') ? "checked='checked'" : ''?> onclick="return false" />
                                </li>
                                <!--tributos-->
                                <li>
                                    <?	$rowSitTributaria = mysql_fetch_array(mysql_query("SELECT * FROM tblnfe_notafiscal_icms WHERE fldId = ".fncNullId($icms_sit_tributaria)))	?>
                                    <input type="text" style="width:120px;text-align:left" class="txt_nfe_pedido_item_tributos_icms_situacao_tributaria" id="txt_nfe_pedido_item_tributos_icms_situacao_tributaria_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_situacao_tributaria_<?=$i?>" value="<?=$rowSitTributaria['fldSituacaoTributaria'].' - '.$rowSitTributaria['fldDescricao']?>" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_icms_situacao_tributaria" id="hid_nfe_pedido_item_tributos_icms_situacao_tributaria" name="hid_nfe_pedido_item_tributos_icms_situacao_tributaria_<?=$i?>" value="<?=$icms_sit_tributaria?>" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_tributo_rad" id="hid_nfe_tributo_rad_<?=$i?>" name="hid_nfe_tributo_rad_<?=$i?>" value="<?=$rad_imposto?>"  />
                                </li>
                                <li>
                                    <?	$rowOrigem = mysql_fetch_array(mysql_query("SELECT * FROM tblnfe_icms_campo_orig WHERE fldId = ".fncNullId($icms_origem)))	?>
                                    <input type="text" style="width:70px;text-align:center" class="txt_nfe_pedido_item_tributos_icms_origem" id="txt_nfe_pedido_item_tributos_icms_origem_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_origem_<?=$i?>" value="<?=$rowOrigem['fldDescricao']?>" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_icms_origem" id="hid_nfe_pedido_item_tributos_icms_origem_<?=$i?>" name="hid_nfe_pedido_item_tributos_icms_origem_<?=$i?>" value="<?=$icms_origem?>" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_pCredSN" id="txt_nfe_pedido_item_tributos_icms_pCredSN_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_pCredSN_<?=$i?>" style="width:150px;text-align:right" value="<?=format_number_out($icms_aliq_aplicavel)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_vCredICMSSN" id="txt_nfe_pedido_item_tributos_icms_vCredICMSSN_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_vCredICMSSN_<?=$i?>" style="width:150px;text-align:right" value="<?=format_number_out($icms_cred_aproveitado)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <?	$rowModBC = mysql_fetch_array(mysql_query("SELECT * FROM tblnfe_icms_campo_modbc WHERE fldId = ".fncNullId($icms_bc_modalidade)))	?>
                                    <input type="text"  style="width:150px;text-align:left" class="txt_nfe_pedido_item_tributos_icms_modBC" id="txt_nfe_pedido_item_tributos_icms_modBC_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_modBC_<?=$i?>" value="<?=$rowModBC['fldDescricao']?>" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_icms_modBC" id="hid_nfe_pedido_item_tributos_icms_modBC" name="hid_nfe_pedido_item_tributos_icms_modBC_<?=$i?>" value="<?=$icms_bc_modalidade?>" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_vBC" id="txt_nfe_pedido_item_tributos_icms_vBC_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_vBC_<?=$i?>" style="width:80px;text-align:right" value="<?=format_number_out($icms_base_calculo)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_pRedBC" id="txt_nfe_pedido_item_tributos_icms_pRedBC_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_pRedBC_<?=$i?>" style="width:120px;text-align:right" value="<?=format_number_out($icms_bc_perc_reducao)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_pICMS" id="txt_nfe_pedido_item_tributos_icms_pICMS_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_pICMS_<?=$i?>" style="width:70px;text-align:right" value="<?=format_number_out($icms_aliquota)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_vICMS" id="txt_nfe_pedido_item_tributos_icms_vICMS_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_vICMS_<?=$i?>" style="width:80px;text-align:right" value="<?=format_number_out($icms_valor)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_pBCOp" id="txt_nfe_pedido_item_tributos_icms_pBCOp_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_pBCOp_<?=$i?>" style="width:150px;text-align:right" value="<?=format_number_out($icms_bc_op_propria)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <?	$rowDesMotivo = mysql_fetch_array(mysql_query("SELECT * FROM tblnfe_icms_campo_motdesicms WHERE fldId = ".fncNullId($icms_desoneracao_motiv)))	?>
                                    <input type="text" style="width:150px;text-align:left" class="txt_nfe_pedido_item_tributos_icms_motDesICMS" id="txt_nfe_pedido_item_tributos_icms_motDesICMS_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_motDesICMS_<?=$i?>" value="<?=$rowDesMotivo['fldDescricao']?>" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_icms_motDesICMS"  id="hid_nfe_pedido_item_tributos_icms_motDesICMS_<?=$i?>" name="hid_nfe_pedido_item_tributos_icms_motDesICMS_<?=$i?>" value="<?=$icms_desoneracao_motiv?>" />
                                </li>
                                <li>
                                    <?	$rowModBCST = mysql_fetch_array(mysql_query("SELECT * FROM tblnfe_icms_campo_modbcst WHERE fldId = ".fncNullId($icmsst_bc_modalidade))); echo mysql_error()?>
                                    <input type="text" style="width:180px;text-align:left" class="txt_nfe_pedido_item_tributos_icms_st_modBCST" id="txt_nfe_pedido_item_tributos_icms_st_modBCST_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_st_modBCST_<?=$i?>" value="<?=$rowModBCST['fldDescricao']?>" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_icms_st_modBCST" id="hid_nfe_pedido_item_tributos_icms_st_modBCST_<?=$i?>" name="hid_nfe_pedido_item_tributos_icms_st_modBCST_<?=$i?>" value="<?=$icmsst_bc_modalidade?>" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_pRedBCST" id="txt_nfe_pedido_item_tributos_icms_st_pRedBCST_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_st_pRedBCST_<?=$i?>" style="width:120px;text-align:right" value="<?=format_number_out($icmsst_bc_perc_reducao)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_pMVAST" id="txt_nfe_pedido_item_tributos_icms_st_pMVAST_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_st_pMVAST_<?=$i?>" style="width:150px;text-align:right" value="<?=format_number_out($icmsst_mva)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_vBCST" id="txt_nfe_pedido_item_tributos_icms_st_vBCST_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_st_vBCST_<?=$i?>" style="width:100px;text-align:right" value="<?=format_number_out($icmsst_base_calculo)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_pICMSST" id="txt_nfe_pedido_item_tributos_icms_st_pICMSST_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_st_pICMSST_<?=$i?>" style="width:100px;text-align:right" value="<?=format_number_out($icmsst_aliquota)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_vICMSST" id="txt_nfe_pedido_item_tributos_icms_st_vICMSST_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_st_vICMSST_<?=$i?>" style="width:80px;text-align:right" value="<?=format_number_out($icmsst_valor)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:120px;text-align:left" class="txt_nfe_pedido_item_tributos_icms_st_UFST" id="txt_nfe_pedido_item_tributos_icms_st_UFST_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_st_UFST_<?=$i?>" value="<?=$icmsst_uf?>" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_icms_st_UFST" id="hid_nfe_pedido_item_tributos_icms_st_UFST_<?=$i?>" name="hid_nfe_pedido_item_tributos_icms_st_UFST_<?=$i?>" value="<?=$rowItem['fldicmsst_uf']?>" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_vBCSTRet" id="txt_nfe_pedido_item_tributos_icms_st_vBCSTRet_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_st_vBCSTRet_<?=$i?>" style="width:150px;text-align:right" value="<?=format_number_out($icmsst_bc_ret_uf_rem)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_vICMSSTRet" id="txt_nfe_pedido_item_tributos_icms_st_vICMSSTRet_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_st_vICMSSTRet_<?=$i?>" style="width:150px;text-align:right" value="<?=format_number_out($icmsst_val_ret_uf_rem)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_vBCSTDest" id="txt_nfe_pedido_item_tributos_icms_st_vBCSTDest_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_st_vBCSTDest_<?=$i?>" style="width:150px;text-align:right" value="<?=format_number_out($icmsst_bc_uf_destino)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_vICMSSTDest" id="txt_nfe_pedido_item_tributos_icms_st_vICMSSTDest_<?=$i?>" name="txt_nfe_pedido_item_tributos_icms_st_vICMSSTDest_<?=$i?>" style="width:150px;text-align:right" value="<?=format_number_out($icmsst_valor_uf_destino)?>" readonly="readonly" />
                                </li>
                                <!--tributos cofins-->
                                <li>
                                    <?	$rowSitTribCofins = mysql_fetch_array(mysql_query("SELECT * FROM tblnfe_cofins WHERE fldId = ".fncNullId($cofins_sit_tributaria)))	?>
                                    <input type="text" style="width:120px;text-align:left"  class="txt_nfe_pedido_item_tributos_cofins_situacao_tributaria" id="txt_nfe_pedido_item_tributos_cofins_situacao_tributaria_<?=$i?>" name="txt_nfe_pedido_item_tributos_cofins_situacao_tributaria_<?=$i?>" value="<?=$rowSitTribCofins['fldCOFINS'].' - '.$rowSitTribCofins['fldDescricao']?>" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_cofins_situacao_tributaria" id="hid_nfe_pedido_item_tributos_cofins_situacao_tributaria" name="hid_nfe_pedido_item_tributos_cofins_situacao_tributaria_<?=$i?>" value="<?=$cofins_sit_tributaria?>" />
                                </li>
                                <li>
                                    <?	$calculoTipo = ($cofins_tipo_calculo == '1') ? 'percentual' : ''; $calculoTipo = ($cofins_tipo_calculo == '2') ? 'percentual' : ''; //FIZ AS DUAS VERIFICACOES PQ PODE TER 0 OU NULL TB ?>
                                    <input type="text" style="width:110px;text-align:center" class="txt_nfe_pedido_item_tributos_cofins_calculo_tipo" id="txt_nfe_pedido_item_tributos_cofins_calculo_tipo_<?=$i?>" name="txt_nfe_pedido_item_tributos_cofins_calculo_tipo_<?=$i?>" value="<?=$calculoTipo?>" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_cofins_calculo_tipo" id="hid_nfe_pedido_item_tributos_cofins_calculo_tipo" name="hid_nfe_pedido_item_tributos_cofins_calculo_tipo_<?=$i?>" value="<?=$cofins_tipo_calculo?>" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_cofins_valor_base_calculo" id="txt_nfe_pedido_item_tributos_cofins_valor_base_calculo_<?=$i?>" name="txt_nfe_pedido_item_tributos_cofins_valor_base_calculo_<?=$i?>" style="width:100px;text-align:right" value="<?=format_number_out($cofins_base_calculo)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_cofins_aliquota_percentual" id="txt_nfe_pedido_item_tributos_cofins_aliquota_percentual_<?=$i?>" name="txt_nfe_pedido_item_tributos_cofins_aliquota_percentual_<?=$i?>" style="width:130px;text-align:right" value="<?=format_number_out($cofins_aliq_percent)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_cofins_aliquota_reais" id="txt_nfe_pedido_item_tributos_cofins_aliquota_reais_<?=$i?>" name="txt_nfe_pedido_item_tributos_cofins_aliquota_reais_<?=$i?>" style="width:130px;text-align:right" value="<?=format_number_out($cofins_aliq_reais)?>" readonly="readonly" />
                                </li>
                                <li>	
                                    <input type="text" class="txt_nfe_pedido_item_tributos_cofins_quantidade_vendida" id="txt_nfe_pedido_item_tributos_cofins_quantidade_vendida_<?=$i?>" name="txt_nfe_pedido_item_tributos_cofins_quantidade_vendida_<?=$i?>" style="width:120px;text-align:right" value="<?=format_number_out($cofins_qtde_vendida)?>" readonly="readonly" /> 
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_cofins_valor" id="txt_nfe_pedido_item_tributos_cofins_valor_<?=$i?>" name="txt_nfe_pedido_item_tributos_cofins_valor_<?=$i?>" style="width:100px;text-align:right" value="<?=format_number_out($cofins_valor)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <?	$calculoTipo = ($cofinsst_tipo_calculo == '1') ? 'percentual' : ''; $calculoTipo = ($cofinsst_tipo_calculo == '2') ? 'percentual' : ''; //FIZ AS DUAS VERIFICACOES PQ PODE TER 0 OU NULL TB ?>
                                    <input type="text" style="width:120px;text-align:center" class="txt_nfe_pedido_item_tributos_cofins_st_calculo_tipo" id="txt_nfe_pedido_item_tributos_cofins_st_calculo_tipo_<?=$i?>" name="txt_nfe_pedido_item_tributos_cofins_st_calculo_tipo_<?=$i?>" value="<?=$calculoTipo?>" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_cofins_st_calculo_tipo" id="hid_nfe_pedido_item_tributos_cofins_st_calculo_tipo" name="hid_nfe_pedido_item_tributos_cofins_st_calculo_tipo_<?=$i?>" value="<?=$cofinsst_tipo_calculo?>" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_cofins_st_valor_base_calculo" id="txt_nfe_pedido_item_tributos_cofins_st_valor_base_calculo_<?=$i?>" name="txt_nfe_pedido_item_tributos_cofins_st_valor_base_calculo_<?=$i?>" style="width:120px;text-align:right" value="<?=format_number_out($cofinsst_base_calculo)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_cofins_st_aliquota_percentual" id="txt_nfe_pedido_item_tributos_cofins_st_aliquota_percentual_<?=$i?>" name="txt_nfe_pedido_item_tributos_cofins_st_aliquota_percentual_<?=$i?>" style="width:140px;text-align:" value="<?=format_number_out($cofinsst_aliq_percent)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_cofins_st_aliquota_reais" id="txt_nfe_pedido_item_tributos_cofins_st_aliquota_reais_<?=$i?>" name="txt_nfe_pedido_item_tributos_cofins_st_aliquota_reais_<?=$i?>" style="width:120px;text-align:right" value="<?=format_number_out($cofinsst_aliq_reais)?>" readonly="readonly" />
                                </li>
                                <li>	
                                    <input type="text" class="txt_nfe_pedido_item_tributos_cofins_st_quantidade_vendida" id="txt_nfe_pedido_item_tributos_cofins_st_quantidade_vendida_<?=$i?>" name="txt_nfe_pedido_item_tributos_cofins_st_quantidade_vendida_<?=$i?>" style="width:140px;text-align:right" value="<?=format_number_out($cofinsst_qtde_vendida)?>" readonly="readonly" /> 
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_cofins_st_valor" id="txt_nfe_pedido_item_tributos_cofins_st_valor_<?=$i?>" name="txt_nfe_pedido_item_tributos_cofins_st_valor_<?=$i?>" style="width:120px;text-align:right" value="<?=format_number_out($cofinsst_valor)?>" readonly="readonly" />
                                </li>
                                <!--tributos ii-->
                                <li>
                                    <input type="text" style="width:120px;text-align:right" class="txt_nfe_pedido_item_tributos_ii_valor_base_calculo" id="txt_nfe_pedido_item_tributos_ii_valor_base_calculo_<?=$i?>" name="txt_nfe_pedido_item_tributos_ii_valor_base_calculo_<?=$i?>" value="<?=format_number_out($ii_base_calculo)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:150px;text-align:right" class="txt_nfe_pedido_item_tributos_ii_valor_aduaneiras" id="txt_nfe_pedido_item_tributos_ii_valor_aduaneiras_<?=$i?>" name="txt_nfe_pedido_item_tributos_ii_valor_aduaneiras_<?=$i?>" value="<?=format_number_out($ii_desp_aduaneiras_val)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:70px;text-align:right" class="txt_nfe_pedido_item_tributos_ii_valor_iof" id="txt_nfe_pedido_item_tributos_ii_valor_iof_<?=$i?>" name="txt_nfe_pedido_item_tributos_ii_valor_iof_<?=$i?>" value="<?=format_number_out($ii_iof_valor)?>" readonly="readonly" />
                                </li>
                                <li>	
                                    <input type="text" class="txt_nfe_pedido_item_tributos_ii_valor" id="txt_nfe_pedido_item_tributos_ii_valor_<?=$i?>" name="txt_nfe_pedido_item_tributos_ii_valor_<?=$i?>" style="width:70px;text-align:right" value="<?=format_number_out($ii_valor)?>" readonly="readonly" /> 
                                </li>
                                <!--tributos ipi-->
                                <li>
                                    <?	$rowSitTribIPI = mysql_fetch_array(mysql_query("SELECT * FROM tblnfe_ipi WHERE fldId = ".fncNullId($ipi_sit_tributaria)))	?>
                                    <input type="text" style="width:100px;text-align:left" class="txt_nfe_pedido_item_tributos_ipi_situacao_tributaria" id="txt_nfe_pedido_item_tributos_ipi_situacao_tributaria_<?=$i?>" name="txt_nfe_pedido_item_tributos_ipi_situacao_tributaria_<?=$i?>" value="<?=$rowSitTribIPI['fldSituacaoTributaria'].' - '.$rowSitTribIPI['fldDescricao']?>" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_ipi_situacao_tributaria" id="hid_nfe_pedido_item_tributos_ipi_situacao_tributaria" name="hid_nfe_pedido_item_tributos_ipi_situacao_tributaria_<?=$i?>" value="<?=$ipi_sit_tributaria?>" />
                                </li>
                                <li>	
                                    <input type="text" class="txt_nfe_pedido_item_tributos_ipi_enquadramento_classe" id="txt_nfe_pedido_item_tributos_ipi_enquadramento_classe_<?=$i?>" name="txt_nfe_pedido_item_tributos_ipi_enquadramento_classe_<?=$i?>" style="width:140px" value="<?=$ipi_enquadra_classe?>" readonly="readonly" /> 
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_ipi_enquadramento_codigo" id="txt_nfe_pedido_item_tributos_ipi_enquadramento_codigo_<?=$i?>" name="txt_nfe_pedido_item_tributos_ipi_enquadramento_codigo_<?=$i?>" style="width:170px" value="<?=$ipi_enquadra_codigo?>" readonly="readonly"/>
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_ipi_produtor_cnpj" id="txt_nfe_pedido_item_tributos_ipi_produtor_cnpj_<?=$i?>" name="txt_nfe_pedido_item_tributos_ipi_produtor_cnpj_<?=$i?>" style="width:120px;text-align:right" value="<?=$ipi_produtor_cnpj?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_ipi_selo_controle_codigo" id="txt_nfe_pedido_item_tributos_ipi_selo_controle_codigo_<?=$i?>" name="txt_nfe_pedido_item_tributos_ipi_selo_controle_codigo_<?=$i?>" style="width:140px;text-align:right" value="<?=$ipi_selo_controle_cod?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_ipi_selo_controle_quantidade" id="txt_nfe_pedido_item_tributos_ipi_selo_controle_quantidade_<?=$i?>" name="txt_nfe_pedido_item_tributos_ipi_selo_controle_quantidade_<?=$i?>" style="width:140px;text-align:right" value="<?=format_number_out($ipi_selo_controle_qtde)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <?	$calculoTipo = ($ipi_calculo_tipo == '1') ? 'percentual' : ''; $calculoTipo = ($ipi_calculo_tipo == '2') ? 'percentual' : ''; //FIZ AS DUAS VERIFICACOES PQ PODE TER 0 OU NULL TB ?>
                                    <input type="text" style="width:120px;text-align:center" class="txt_nfe_pedido_item_tributos_ipi_calculo_tipo" id="txt_nfe_pedido_item_tributos_ipi_calculo_tipo_<?=$i?>" name="txt_nfe_pedido_item_tributos_ipi_calculo_tipo_<?=$i?>" value="<?=$calculoTipo?>" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_ipi_calculo_tipo" id="hid_nfe_pedido_item_tributos_ipi_calculo_tipo" name="hid_nfe_pedido_item_tributos_ipi_calculo_tipo_<?=$i?>" value="<?=$ipi_calculo_tipo?>" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_ipi_valor_base_calculo" id="txt_nfe_pedido_item_tributos_ipi_valor_base_calculo_<?=$i?>" name="txt_nfe_pedido_item_tributos_ipi_valor_base_calculo_<?=$i?>" style="width:70px;text-align:right" value="<?=format_number_out($ipi_base_calculo)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_ipi_aliquota" id="txt_nfe_pedido_item_tributos_ipi_aliquota_<?=$i?>" name="txt_nfe_pedido_item_tributos_ipi_aliquota_<?=$i?>" style="width:120px;text-align:right" value="<?=format_number_out($ipi_aliquota)?>" readonly="readonly"/>
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_ipi_unidade_padrao_quantidade_total" id="txt_nfe_pedido_item_tributos_ipi_unidade_padrao_quantidade_total_<?=$i?>" name="txt_nfe_pedido_item_tributos_ipi_unidade_padrao_quantidade_total_<?=$i?>" value="<?=format_number_out($ipi_unid_qtde_total)?>" style="width:150px;text-align:right"  readonly="readonly"/>
                                </li>
                                <li>
                                    <input type="text" style="width:120px;text-align:right" class="txt_nfe_pedido_item_tributos_ipi_unidade_valor" id="txt_nfe_pedido_item_tributos_ipi_unidade_valor_<?=$i?>" name="txt_nfe_pedido_item_tributos_ipi_unidade_valor_<?=$i?>" value="<?=format_number_out($ipi_unidade_valor)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:70px;text-align:right" class="txt_nfe_pedido_item_tributos_ipi_valor" id="txt_nfe_pedido_item_tributos_ipi_valor_<?=$i?>" name="txt_nfe_pedido_item_tributos_ipi_valor_<?=$i?>" value="<?=format_number_out($ipi_valor)?>" readonly="readonly" />
                                </li>
                                <!--tributos issqn-->
                                <li>
                                    <?	$rowSitTribISSQN = mysql_fetch_array(mysql_query("SELECT * FROM tblnfe_tributacao_issqn WHERE fldId = ".fncNullId($issqn_sit_tributaria))) ?>
                                    <input type="text" style="width:120px;text-align:left" class="txt_nfe_pedido_item_tributos_issqn_tributacao" id="txt_nfe_pedido_item_tributos_issqn_tributacao_<?=$i?>" name="txt_nfe_pedido_item_tributos_issqn_tributacao_<?=$i?>" value="<?=$rowSitTribISSQN['fldSituacaoTributaria'].' - '.$rowSitTribISSQN['fldDescricao']?>" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_issqn_tributacao"  id="hid_nfe_pedido_item_tributos_issqn_tributacao" name="hid_nfe_pedido_item_tributos_issqn_tributacao_<?=$i?>" value="<?=$issqn_sit_tributaria?>" />
                                </li>
                                <li>
                                    <input type="text" style="width:120px;text-align:right" class="txt_nfe_pedido_item_tributos_issqn_valor_base_calculo" id="txt_nfe_pedido_item_tributos_issqn_valor_base_calculo_<?=$i?>" name="txt_nfe_pedido_item_tributos_issqn_valor_base_calculo_<?=$i?>" value="<?=format_number_out($issqn_base_calculo)?>" readonly="readonly" />
                                </li>
                                
                                <li>
                                    <input type="text" style="width:120px;text-align:right" class="txt_nfe_pedido_item_tributos_issqn_aliquota" id="txt_nfe_pedido_item_tributos_issqn_aliquota_<?=$i?>" name="txt_nfe_pedido_item_tributos_issqn_aliquota_<?=$i?>" value="<?=format_number_out($issqn_aliquota)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <?	$rowListaServSSQN = mysql_fetch_array(mysql_query("SELECT * FROM tblnfe_issqn_servico WHERE fldId = ".fncNullId($issqn_lista_servico))) ?>
                                    <input type="text" style="width:140px;text-align:left" class="txt_nfe_pedido_item_tributos_issqn_servico_lista" id="txt_nfe_pedido_item_tributos_issqn_servico_lista_<?=$i?>" name="txt_nfe_pedido_item_tributos_issqn_servico_lista_<?=$i?>" value="<?=$rowListaServSSQN['fldDescricao']?>" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_issqn_servico_lista" id="hid_nfe_pedido_item_tributos_issqn_servico_lista" name="hid_nfe_pedido_item_tributos_issqn_servico_lista_<?=$i?>" value="<?=$issqn_lista_servico?>" />
                                </li>
                                <li>
                                    <?	$rowUF = mysql_fetch_array(mysql_query("SELECT * FROM tblibge_uf WHERE fldCodigo = ".fncNullId($issqn_uf))) ?>
                                    <input type="text"  style="width:70px;text-align:left" class="txt_nfe_pedido_item_tributos_issqn_uf" id="txt_nfe_pedido_item_tributos_issqn_uf_<?=$i?>" name="txt_nfe_pedido_item_tributos_issqn_uf_<?=$i?>" value="<?=$rowUF['fldSigla']?>" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_issqn_uf" id="hid_nfe_pedido_item_tributos_issqn_uf_<?=$i?>" name="hid_nfe_pedido_item_tributos_issqn_uf_<?=$i?>" value="<?=$issqn_uf?>" />
                                </li>
                                <li>
                                    <?	$rowMunicipio = mysql_fetch_array(mysql_query("SELECT * FROM tblibge_municipio WHERE fldCodigo = ".fncNullId($issqn_mun_ocorrendia))) ?>
                                    <input type="text" style="width:140px;text-align:left" class="txt_nfe_pedido_item_tributos_issqn_municipio" id="txt_nfe_pedido_item_tributos_issqn_municipio_<?=$i?>" name="txt_nfe_pedido_item_tributos_issqn_municipio_<?=$i?>" value="<?=$rowMunicipio['fldNome']?>" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_issqn_municipio" id="hid_nfe_pedido_item_tributos_issqn_municipio" name="hid_nfe_pedido_item_tributos_issqn_municipio_<?=$i?>" value="<?=$issqn_mun_ocorrendia?>" />
                                </li>
                                <li>
                                    <input type="text" style="width:80px;text-align:right" class="txt_nfe_pedido_item_tributos_issqn_valor" id="txt_nfe_pedido_item_tributos_issqn_valor_<?=$i?>" name="txt_nfe_pedido_item_tributos_issqn_valor_<?=$i?>" value="<?=$issqn_valor?>" readonly="readonly" />
                                </li>
                                <!--tributos pis-->
                                <li>
                                    <?	$rowSitTribPIS = mysql_fetch_array(mysql_query("SELECT * FROM tblnfe_pis WHERE fldId = ".fncNullId($pis_sit_tributaria))) ?>
                                    <input type="text" style="width:100px;text-align:left" class="txt_nfe_pedido_item_tributos_pis_situacao_tributaria" id="txt_nfe_pedido_item_tributos_pis_situacao_tributaria_<?=$i?>" name="txt_nfe_pedido_item_tributos_pis_situacao_tributaria_<?=$i?>" value="<?=$rowSitTribPIS['fldSituacaoTributaria'].' - '.$rowSitTribPIS['fldDescricao']?>"  readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_pis_situacao_tributaria" id="hid_nfe_pedido_item_tributos_pis_situacao_tributaria" name="hid_nfe_pedido_item_tributos_pis_situacao_tributaria_<?=$i?>" value="<?=$pis_sit_tributaria?>" readonly="readonly" />
                                </li>
                                <li>
                                    <?	$calculoTipo = ($pis_calculo_tipo == '1') ? 'percentual' : ''; $calculoTipo = ($pis_calculo_tipo == '2') ? 'percentual' : ''; //FIZ AS DUAS VERIFICACOES PQ PODE TER 0 OU NULL TB ?>
                                    <input type="text" style="width:100px;text-align:left" class="txt_nfe_pedido_item_tributos_pis_calculo_tipo" id="txt_nfe_pedido_item_tributos_pis_calculo_tipo_<?=$i?>" name="txt_nfe_pedido_item_tributos_pis_calculo_tipo_<?=$i?>" value="<?=$calculoTipo?>" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_pis_calculo_tipo" id="hid_nfe_pedido_item_tributos_pis_calculo_tipo" name="hid_nfe_pedido_item_tributos_pis_calculo_tipo_<?=$i?>" value="<?=$pis_calculo_tipo?>" />         
                                </li>
                                <li>
                                    <input type="text" style="width:100px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_valor_base_calculo" id="txt_nfe_pedido_item_tributos_pis_valor_base_calculo_<?=$i?>" name="txt_nfe_pedido_item_tributos_pis_valor_base_calculo_<?=$i?>" value="<?=format_number_out($pis_base_calculo)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:110px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_aliquota_percentual" id="txt_nfe_pedido_item_tributos_pis_aliquota_percentual_<?=$i?>" name="txt_nfe_pedido_item_tributos_pis_aliquota_percentual_<?=$i?>" value="<?=format_number_out($pis_aliq_percentual)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:100px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_aliquota_reais" id="txt_nfe_pedido_item_tributos_pis_aliquota_reais_<?=$i?>" name="txt_nfe_pedido_item_tributos_pis_aliquota_reais_<?=$i?>" value="<?=format_number_out($pis_aliq_reais)?>" readonly="readonly" />
                                </li>
                                <li>	
                                    <input type="text" style="width:100px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_quantidade_vendida" id="txt_nfe_pedido_item_tributos_pis_quantidade_vendida_<?=$i?>" name="txt_nfe_pedido_item_tributos_pis_quantidade_vendida_<?=$i?>" value="<?=format_number_out($pis_qtde_vendida)?>" readonly="readonly" /> 
                                </li>
                                <li>
                                    <input type="text" style="width:60px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_valor" id="txt_nfe_pedido_item_tributos_pis_valor_<?=$i?>" name="txt_nfe_pedido_item_tributos_pis_valor_<?=$i?>" value="<?=format_number_out($pis_valor)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <?	$calculoTipo = ($pisst_tipo_calculo == '1') ? 'percentual' : ''; $calculoTipo = ($pisst_tipo_calculo == '2') ? 'percentual' : ''; //FIZ AS DUAS VERIFICACOES PQ PODE TER 0 OU NULL TB ?>
                                    <input type="text" style="width:120px;text-align:center" class="txt_nfe_pedido_item_tributos_pis_st_calculo_tipo" id="txt_nfe_pedido_item_tributos_pis_st_calculo_tipo_<?=$i?>" name="txt_nfe_pedido_item_tributos_pis_st_calculo_tipo_<?=$i?>" value="<?=$calculoTipo?>" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_pis_st_calculo_tipo" id="hid_nfe_pedido_item_tributos_pis_st_calculo_tipo" name="hid_nfe_pedido_item_tributos_pis_st_calculo_tipo_<?=$i?>" value="<?=$pisst_tipo_calculo?>" />  
                                </li>
                                <li>
                                    <input type="text" style="width:100px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_st_valor_base_calculo" id="txt_nfe_pedido_item_tributos_pis_st_valor_base_calculo_<?=$i?>" name="txt_nfe_pedido_item_tributos_pis_st_valor_base_calculo_<?=$i?>" value="<?=format_number_out($pisst_base_calculo)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:120px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_st_aliquota_percentual" id="txt_nfe_pedido_item_tributos_pis_st_aliquota_percentual_<?=$i?>" name="txt_nfe_pedido_item_tributos_pis_st_aliquota_percentual_<?=$i?>" value="<?=format_number_out($pisst_aliq_percent)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:120px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_st_aliquota_reais" id="txt_nfe_pedido_item_tributos_pis_st_aliquota_reais_<?=$i?>" name="txt_nfe_pedido_item_tributos_pis_st_aliquota_reais_<?=$i?>" value="<?=format_number_out($pisst_aliq_reais)?>" readonly="readonly" />
                                </li>
                                <li>	
                                    <input type="text" style="width:120px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_st_quantidade_vendida" id="txt_nfe_pedido_item_tributos_pis_st_quantidade_vendida_<?=$i?>" name="txt_nfe_pedido_item_tributos_pis_st_quantidade_vendida_<?=$i?>" value="<?=format_number_out($pisst_qtde_vendida)?>" readonly="readonly" /> 
                                </li>
                                <li>
                                    <input type="text" style="width:70px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_st_valor" id="txt_nfe_pedido_item_tributos_pis_st_valor_<?=$i?>" name="txt_nfe_pedido_item_tributos_pis_st_valor_<?=$i?>" value="<?=format_number_out($pisst_valor)?>" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:200px;text-align:left" class="txt_nfe_item_pedido_informacoes_adicionais" id="txt_nfe_item_pedido_informacoes_adicionais_<?=$i?>" name="txt_nfe_item_pedido_informacoes_adicionais_<?=$i?>" value="<?=$info_adicionais?>" readonly="readonly" />
                                </li>
                                <!--- PRODUTO ESPECIFICO -->
                                <li>
                                    <input type="text" 	 style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico" 							id="txt_nfe_item_produto_especifico_<?=$i?>" 							name="txt_nfe_item_produto_especifico_<?=$i?>" 							value="<?=$prod_especifico;?>" 			readonly="readonly" />
									<input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_id" 						id="txt_nfe_item_produto_especifico_id_<?=$i?>" 						name="txt_nfe_item_produto_especifico_id_<?=$i?>" 						value="<?=$prod_especifico_id?>"		readonly="readonly" />
                                    <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_combustivel_anp"			id="txt_nfe_item_produto_especifico_combustivel_anp_<?=$i?>" 			name="txt_nfe_item_produto_especifico_combustivel_anp_<?=$i?>" 			value="<?=$combustivel_anp?>" 			readonly="readonly" />
                                    <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_combustivel_if" 			id="txt_nfe_item_produto_especifico_combustivel_if_<?=$i?>" 			name="txt_nfe_item_produto_especifico_combustivel_if_<?=$i?>" 			value="<?=$combustivel_if?>" 			readonly="readonly" />
                                    <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_combustivel_uf" 			id="txt_nfe_item_produto_especifico_combustivel_uf_<?=$i?>" 			name="txt_nfe_item_produto_especifico_combustivel_uf_<?=$i?>" 			value="<?=$combustivel_uf?>" 			readonly="readonly" />
                                    <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_combustivel_qtd_faturada" 	id="txt_nfe_item_produto_especifico_combustivel_qtd_faturada_<?=$i?>" 	name="txt_nfe_item_produto_especifico_combustivel_qtd_faturada_<?=$i?>" value="<?=$combustivel_qtd_faturada?>" 	readonly="readonly" />
                                    <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_combustivel_cide_bc" 		id="txt_nfe_item_produto_especifico_combustivel_cide_bc_<?=$i?>" 		name="txt_nfe_item_produto_especifico_combustivel_cide_bc_<?=$i?>" 		value="<?=$combustivel_cide_bc?>" 		readonly="readonly" />
                                    <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_combustivel_cide_aliquota" 	id="txt_nfe_item_produto_especifico_combustivel_cide_aliquota_<?=$i?>" 	name="txt_nfe_item_produto_especifico_combustivel_cide_aliquota_<?=$i?>" value="<?=$combustivel_cide_aliquota?>"readonly="readonly" />
                                    <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_combustivel_cide_valor" 	id="txt_nfe_item_produto_especifico_combustivel_cide_valor_<?=$i?>" 	name="txt_nfe_item_produto_especifico_combustivel_cide_valor_<?=$i?>" 	value="<?=$combustivel_cide_valor?>" 	readonly="readonly" />
                                    <!-- TRIBUTO -->
                                    <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_total_tributos" 			id="txt_nfe_item_produto_especifico_total_tributos_<?=$i?>" 			name="txt_nfe_item_produto_especifico_total_tributos_<?=$i?>" 			value="<?=format_number_out($total_tributos)?>" readonly="readonly" />
                                </li>
                                <!--- PRODUTO ESPECIFICO -->
                            </ul> 
<?                     		$subTotalPedido 	+= $totalValor;
							$totalNotaItem		+= $itemTotalBruto;
							$totalNotaDesconto	+= number_format($desconto,2,'.','');
							$i += 1;
						}
?>					
           			</div>
                    <div>
                        <input type="hidden" name="hid_controle" id="hid_controle" value="<?=$rows?>" />
                    </div>
<?					if($_SESSION["sistema_tipo"]=="otica" && $venda_origem == 0){		
?>                   
                        <div id="pedido_otica_opcoes">
                            <!-- �culos longe -->
                            <div class="pedido_otica_opcoes_detalhe">
                                <span>LONGE</span>
                                <ul class="pedido_otica_opcoes_dados" style="margin-left: 25px">
                                    <li><input style="width:235px;background:#F5F4D8" type="text" 	name="txt_pedido_otica_longe_armacao" value="<?=($Otica_Longe_Armacao_Descricao != '') ? $Otica_Longe_Armacao_Descricao : "Arma&ccedil;&atilde;o";?>" readonly="true"></li>
                                    <li><input type="hidden" name="hid_pedido_otica_longe_armacao" 	value="<?=substr($Otica_Longe_Armacao,0,30)?>"></li>
                                    <li><input style="width:235px;background:#D6F2F8" type="text"	name="txt_pedido_otica_longe_material" value="<?=($Otica_Longe_Material_Descricao != '') ? $Otica_Longe_Material_Descricao : "Material";?>" readonly="true"></li>
                                    <li><input type="hidden" name="hid_pedido_otica_longe_material" value="<?=substr($Otica_Longe_Material,0,30)?>"></li>
                                </ul>
                                <ul class="pedido_otica_opcoes_cabecalho">
                                    <li style="width: 57px; text-align: right;">Esf&eacute;rico</li>
                                    <li style="width: 57px; text-align: left;">Cil&iacute;ndrico</li>
                                    <li style="width: 57px; text-align: left;">Eixo</li>
                                    <li style="width: 57px; text-align: left;">DP</li>
                                </ul>
                                <ul class="pedido_otica_opcoes_dados">
                                    <li style="width: 26px;">O.D</li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_longe_od_esferico" 	style="width:54px; height:19px; text-align:right" 	value="<?=$otica_retorno['longe']['OD']['esferico']?>"></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_longe_od_cilindrico" style="width:54px; height:19px; text-align:right"	value="<?=$otica_retorno['longe']['OD']['cilindrico']?>"></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_longe_od_eixo" 		style="width:54px; height:19px; text-align:right" 	value="<?=$otica_retorno['longe']['OD']['eixo']?>"></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_longe_od_dp" 		style="width:54px; height:19px; text-align:right" 	value="<?=$otica_retorno['longe']['OD']['DP']?>"></li>
                                </ul>
                                <ul class="pedido_otica_opcoes_dados">
                                    <li style="width: 26px;">O.E</li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_longe_oe_esferico" 	style="width:54px; height:19px; text-align:right" 	value="<?=$otica_retorno['longe']['OE']['esferico']?>"></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_longe_oe_cilindrico" style="width:54px; height:19px; text-align:right" 	value="<?=$otica_retorno['longe']['OE']['cilindrico']?>"></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_longe_oe_eixo" 		style="width:54px; height:19px; text-align:right"	value="<?=$otica_retorno['longe']['OE']['eixo']?>"></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_longe_oe_dp"			style="width:54px; height:19px; text-align:right" 	value="<?=$otica_retorno['longe']['OE']['DP']?>"></li>
                                </ul>
                            </div>
                            <!-- �culos perto -->
                            <a class="modal" style="display:none" id="modal_otica" href="pedido_otica_select" rel="345-130" title=""></a>
                            <div class="pedido_otica_opcoes_detalhe">
                                <span>PERTO</span>
                                <ul class="pedido_otica_opcoes_dados" style="margin-left: 25px">
                                    <li><input style="width:235px;background:#F5F4D8" type="text" 	name="txt_pedido_otica_perto_armacao" value="<?=($Otica_Perto_Armacao_Descricao != '') ? $Otica_Perto_Armacao_Descricao : "Arma&ccedil;&atilde;o";?>" readonly="true"></li>
                                    <li><input type="hidden" name="hid_pedido_otica_perto_armacao" 	value="<?=substr($Otica_Perto_Armacao,0,30)?>"></li>
                                    <li><input style="width:235px;background:#D6F2F8" type="text" 	name="txt_pedido_otica_perto_material" value="<?=($Otica_Perto_Material_Descricao != '') ? $Otica_Perto_Material_Descricao : "Material";?>" readonly="true"></li>
                                    <li><input type="hidden" name="hid_pedido_otica_perto_material" value="<?=substr($Otica_Perto_Material,0,30)?>"></li>
                                </ul>
                                <ul class="pedido_otica_opcoes_cabecalho">
                                    <li style="width: 57px; text-align: right;">Esf&eacute;rico</li>
                                    <li style="width: 57px; text-align: left;">Cil&iacute;ndrico</li>
                                    <li style="width: 57px; text-align: left;">Eixo</li>
                                    <li style="width: 57px; text-align: left;">DP</li>
                                </ul>
                                <ul class="pedido_otica_opcoes_dados">
                                    <li style="width: 26px;">O.D</li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_perto_od_esferico" 	style="width:54px; height:19px; text-align:right" 	value="<?=$otica_retorno['perto']['OD']['esferico']?>"></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_perto_od_cilindrico" style="width:54px; height:19px; text-align:right"	value="<?=$otica_retorno['perto']['OD']['cilindrico']?>"></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_perto_od_eixo" 		style="width:54px; height:19px; text-align:right" 	value="<?=$otica_retorno['perto']['OD']['eixo']?>"></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_perto_od_dp" 		style="width:54px; height:19px; text-align:right" 	value="<?=$otica_retorno['perto']['OD']['DP']?>"></li>
                                </ul>
                                <ul class="pedido_otica_opcoes_dados">
                                    <li style="width: 26px;">O.E</li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_perto_oe_esferico" 	style="width:54px; height:19px; text-align:right"	value="<?=$otica_retorno['perto']['OE']['esferico']?>"></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_perto_oe_cilindrico" style="width:54px; height:19px; text-align:right"	value="<?=$otica_retorno['perto']['OE']['cilindrico']?>"></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_perto_oe_eixo" 		style="width:54px; height:19px; text-align:right" 	value="<?=$otica_retorno['perto']['OE']['eixo']?>"></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_perto_oe_dp" 		style="width:54px; height:19px; text-align:right" 	value="<?=$otica_retorno['perto']['OE']['DP']?>"></li>
                                </ul>
                            </div>
                            <!-- �culos Bifocal -->
                            <div class="pedido_otica_opcoes_detalhe">
                                <span>BIFOCAL / MULTIFOCAL</span>
                                <ul class="pedido_otica_opcoes_dados" style="margin-left: 25px">
                                    <li>
                                        <select style="width:180px;width:235px;background:#F5F4D8"" id="sel_otica_marca" name="sel_otica_marca" >
                                            <option value="0">Selecionar</option>
<?											$rsMarca = mysql_query("SELECT * FROM tblmarca WHERE fldDisabled = '0' ORDER BY fldNome");
											while($rowMarca = mysql_fetch_array($rsMarca)){                            
?>                   	       					<option value="<?=$rowMarca['fldId']?>" <?= ($otica_retorno['bifocal']['marca'] == $rowMarca['fldId'])?"selected = 'selected'" : '' ?> ><?=$rowMarca['fldNome']?></option>
<?											}
?>                 						</select>
                                    </li>
                                    <li>
                                        <select style="width:180px;width:235px;background:#D6F2F8"" id="sel_otica_lente" name="sel_otica_lente" >
                                            <option value="0">Selecionar</option>
<?											$rsLente = mysql_query("SELECT * FROM tblproduto_otica_lente ORDER BY fldLente");
											while($rowLente = mysql_fetch_array($rsLente)){                            
?>                   	       					<option value="<?=$rowLente['fldId']?>" <?= ($otica_retorno['bifocal']['lente'] == $rowLente['fldId'])?"selected = 'selected'" : '' ?>><?=$rowLente['fldLente']?></option>
<?											}
?>                 						</select>
									</li>
                                </ul>
                                <ul class="pedido_otica_opcoes_cabecalho" style="margin-left:10px">
                                    <li style="width: 66px; text-align: right;">Altura</li>
                                    <li style="width: 60px; text-align: center;">DNP</li>
                                    <li style="width: 60px; text-align: center;">Adi&ccedil;&atilde;o</li>
                                </ul>
                                <ul class="pedido_otica_opcoes_dados">
                                    <li style="width: 26px;">O.D</li>
                                    <li style="width: 60px;"><input type="text" name="txt_pedido_otica_bifocal_od_altura" 	style="width:56px; height:19px; text-align:right" 	value="<?=$otica_retorno['bifocal']['OD']['altura']?>"></li>
                                    <li style="width: 60px;"><input type="text" name="txt_pedido_otica_bifocal_od_dnp" 		style="width:56px; height:19px; text-align:right" 	value="<?=$otica_retorno['bifocal']['OD']['DNP']?>"></li>
                                    <li style="width: 60px;"><input type="text" name="txt_pedido_otica_bifocal_od_adicao" 	style="width:56px; height:19px; text-align:right" 	value="<?=$otica_retorno['bifocal']['OD']['adicao']?>"></li>
                                </ul>
                                <ul class="pedido_otica_opcoes_dados">
                                    <li style="width: 26px;">O.E</li>
                                    <li style="width: 60px;"><input type="text" name="txt_pedido_otica_bifocal_oe_altura" 	style="width:56px; height:19px; text-align:right" 	value="<?=$otica_retorno['bifocal']['OE']['altura']?>"></li>
                                    <li style="width: 60px;"><input type="text" name="txt_pedido_otica_bifocal_oe_dnp" 		style="width:56px; height:19px; text-align:right"	value="<?=$otica_retorno['bifocal']['OE']['DNP']?>"></li>
                                    <li style="width: 60px;"><input type="text" name="txt_pedido_otica_bifocal_oe_adicao" 	style="width:56px; height:19px; text-align:right" 	value="<?=$otica_retorno['bifocal']['OE']['adicao']?>"></li>
                                </ul>
                            </div>
                            <ul class="pedido_otica_opcoes_dados" style="margin:0">
                                <li>
                                    <label for="txt_pedido_otica_medico">M&eacute;dico</label>
                                    <input type="text" name="txt_pedido_otica_medico" style="width: 625px" value="<?=$rowOtica['fldMedico']?>" />
                                </li>
                                <li>
                                    <label for="txt_pedido_otica_medico_crm">CRM</label>
                                    <input type="text" name="txt_pedido_otica_medico_crm" style="width: 300px" value="<?=$rowOtica['fldMedico_crm']?>" />
                                </li>
                            </ul>
                        </div>
<?					}
?>				</div> <!-- div de produtos-->
                
<?				
				##BUSCA VALOR DE SERVICO, CASO NFE TENHA SIDO IMPORTADA DE VENDAS COMUNS, ASSIM S� VAI TRAZER O VALOR
				$sSQL = mysql_query("SELECT fldValor FROM tblpedido_funcionario_servico WHERE fldPedido_Id = $documento_id AND fldFuncionario_Id = 0");
				$rowValorServico = mysql_fetch_array($sSQL);
				$totalServicos = $rowValorServico['fldValor'];
			
				#####################################################################################################
?>
                <div id="modo_aba_servico" style="width:962px;height:274px;display:table;background:#D3D3D3">
                	<input type="hidden" name="hid_servico_valor" id="hid_servico_valor" value="<?=$servico_valor?>" />
                	
<?					$sqlFuncaionario_Venda = "SELECT * FROM tblpedido_funcionario_servico WHERE fldPedido_Id = $documento_id";
					$sqlFuncionario = "SELECT tblfuncionario.fldNome, tblfuncionario.fldId
															 FROM tblfuncionario INNER JOIN tblfuncionario_funcao 
															 ON tblfuncionario.fldFuncao2_Id = tblfuncionario_funcao.fldId
															 WHERE tblfuncionario_funcao.fldTipo = '2'";
?>					<fieldset style="width:310px;float:left;margin:7px 7px 0px 0px;border:1px solid #FFF">
                        <ul style="width:310px">
                            <li style="margin-bottom:0">
                                <label for="sel_funcionario1">Funcionario 1</label>
                                <select style="width:300px" id="sel_funcionario1" name="sel_funcionario1" class="sel_funcionario">
                                	<option value="0">selecionar</option>
<?									$rowFuncionarioVenda 	= mysql_fetch_array(mysql_query($sqlFuncaionario_Venda." AND fldOrdem = 1"));
									$rsFuncionario 			= mysql_query($sqlFuncionario);
									while($rowFuncionario 	= mysql_fetch_array($rsFuncionario)){
?>										<option value="<?=$rowFuncionario['fldId']?>" <?= ($rowFuncionarioVenda['fldFuncionario_Id'] == $rowFuncionario['fldId'])? "selected='selected'" : '' ?>><?=$rowFuncionario['fldNome']?></option>
<?									}
?>              	        	</select>	
                            </li>
                            <li style="margin-bottom:0">
                                <label for="txa_funcionario1_servicos">Servi&ccedil;o</label>
                                <textarea style="width:295px; height:50px" 				id="txa_funcionario1_servico" name="txa_funcionario1_servico" <?= (isset($rowFuncionarioVenda['fldFuncionario_Id']) ? '' : "readonly='readonly'") ?>><?=$rowFuncionarioVenda['fldServico']?></textarea>
                            </li>
                            <li style="margin-bottom:0">
                                <label for="txt_funcionario1_tempo">Tempo</label>
                                <input type="text" style="width:142px;text-align:right" id="txt_funcionario1_tempo" name="txt_funcionario1_tempo" value="<?=format_number_out($rowFuncionarioVenda['fldTempo'])?>" class="txt_funcionario_tempo" <?= (isset($rowFuncionarioVenda['fldFuncionario_Id']) ? '' : "readonly='readonly'") ?>>
                            </li>
                            <li>
                                <label for="txt_funcionario1_valor">Valor</label>
                                <input type="text" style="width:142px;text-align:right" id="txt_funcionario1_valor" name="txt_funcionario1_valor" value="<?=format_number_out($rowFuncionarioVenda['fldValor'])?>" <?= (isset($rowFuncionarioVenda['fldFuncionario_Id']) ? '' : "readonly='readonly'") ?>>
                            </li>
                        </ul>
                    </fieldset>
<?					$totalServicos += $rowFuncionarioVenda['fldValor'];
?>
                    <fieldset style="width:310px;float:left;margin:7px 7px 0px 0px;border:1px solid #FFF">
                    	<ul style="width:310px">
                    		<li style="margin-bottom:0">
                            	<label for="sel_funcionario2">Funcionario 2</label>
                            	<select style="width:300px" id="sel_funcionario2" name="sel_funcionario2" class="sel_funcionario">
                                	<option value="0">selecionar</option>
<?									$rowFuncionarioVenda 	= mysql_fetch_array(mysql_query($sqlFuncaionario_Venda." AND fldOrdem = 2"));
									$rsFuncionario 			= mysql_query($sqlFuncionario);                        	
									while($rowFuncionario 	= mysql_fetch_array($rsFuncionario)){
?>										<option value="<?=$rowFuncionario['fldId']?>" <?= ($rowFuncionarioVenda['fldFuncionario_Id'] == $rowFuncionario['fldId'])? "selected='selected'" : '' ?>><?=$rowFuncionario['fldNome']?></option>
<?									}
?>              	    	    </select>	
							</li>
                            <li style="margin-bottom:0">
                                <label for="txa_funcionario2_servico">Servi&ccedil;o</label>
                                <textarea style="width:295px; height:50px" id="txa_funcionario2_servico" name="txa_funcionario2_servico" <?= (isset($rowFuncionarioVenda['fldFuncionario_Id']) ? '' : "readonly='readonly'") ?>><?=$rowFuncionarioVenda['fldServico']?></textarea>
                            </li>
                            <li>
                                <label for="txt_funcionario2_tempo">Tempo</label>
                                <input type="text" style="width:142px;text-align:right" id="txt_funcionario2_tempo" name="txt_funcionario2_tempo" value="<?=format_number_out($rowFuncionarioVenda['fldTempo'])?>" class="txt_funcionario_tempo" <?= (isset($rowFuncionarioVenda['fldFuncionario_Id']) ? '' : "readonly='readonly'") ?>>
                            </li>
                            <li style="margin-bottom:0">
                                <label for="txt_funcionario2_valor">Valor</label>
                                <input type="text" style="width:142px;text-align:right" id="txt_funcionario2_valor" name="txt_funcionario2_valor" value="<?=format_number_out($rowFuncionarioVenda['fldValor'])?>" <?= (isset($rowFuncionarioVenda['fldFuncionario_Id']) ? '' : "readonly='readonly'") ?>>
                            </li>
                        </ul>
                    </fieldset>
<?					$totalServicos += $rowFuncionarioVenda['fldValor'];
?>
                    <fieldset style="width:310px;float:left; margin-top:7px;border:1px solid #FFF">
                    	<ul style="width:310px">
                            <li style="margin-bottom:0">
                                <label for="sel_funcionario3">Funcionario 3</label>
                                <select style="width:300px" id="sel_funcionario3" name="sel_funcionario3" class="sel_funcionario">
                                	<option value="0">selecionar</option>
<?									$rowFuncionarioVenda 	= mysql_fetch_array(mysql_query($sqlFuncaionario_Venda." AND fldOrdem = 3"));
									$rsFuncionario 			= mysql_query($sqlFuncionario);                        	
									while($rowFuncionario 	= mysql_fetch_array($rsFuncionario)){
?>										<option value="<?=$rowFuncionario['fldId']?>"><?=$rowFuncionario['fldNome']?></option>
<?									}
?>              	       		</select>	
							</li>
                            <li style="margin-bottom:0">
                                <label for="txa_funcionario3_servico">Servi&ccedil;o</label>
                                <textarea style="width:295px; height:50px" id="txa_funcionario3_servico" name="txa_funcionario3_servico" <?= (isset($rowFuncionarioVenda['fldFuncionario_Id']) ? '' : "readonly='readonly'") ?>><?=$rowFuncionarioVenda['fldServico']?></textarea>
                            </li>
                            <li style="margin-bottom:0">
                                <label for="txt_funcionario3_tempo">Tempo</label>
                                <input type="text" style="width:142px;text-align:right" id="txt_funcionario3_tempo" name="txt_funcionario3_tempo" value="<?=format_number_out($rowFuncionarioVenda['fldTempo'])?>" class="txt_funcionario_tempo" <?= (isset($rowFuncionarioVenda['fldFuncionario_Id']) ? '' : "readonly='readonly'") ?>>
                            </li>
                            <li>
                                <label for="txt_funcionario3_valor">Valor</label>
                                <input type="text" style="width:142px;text-align:right" id="txt_funcionario3_valor" name="txt_funcionario3_valor" value="<?=format_number_out($rowFuncionarioVenda['fldValor'])?>" <?= (isset($rowFuncionarioVenda['fldFuncionario_Id']) ? '' : "readonly='readonly'") ?>>
                            </li>
                        </ul>
                    </fieldset>
<?					$totalServicos += $rowFuncionarioVenda['fldValor'];
?>
                    <ul>
                    	<li>
                            <label for="txa_outros_servicos">Outros servi&ccedil;os</label>
                            <textarea style="width:935px; height:65px" id="txa_outros_servicos" name="txa_outros_servicos"><?= $rowNFe['fldServico']?></textarea>
						</li>
					</ul>
                </div>
                
<?				//total pedido

				$totalNotaDesconto 	= ($rowNFe['flddesconto_total']) ? $rowNFe['flddesconto_total'] : $totalNotaDesconto;
				$totalNota 			= $totalNotaItem - $totalNotaDesconto;
				$totalNota 			= ($rowNFe['fldnfe_total']) ? $rowNFe['fldnfe_total'] : $totalNota;
				
				$totalTerceiros		= $rowNFe['fldValor_Terceiros'];
				
				$pedidoDesc 		= $rowNFe['fldDesconto'];
				$pedidoDescReais 	= $rowNFe['fldDescontoReais'];
				$pedidoSubTotal		= $totalNota + $totalServicos + $totalTerceiros;
				
				$pedidoDescTotal 	= ($pedidoSubTotal * $pedidoDesc) / 100;
				$pedidoTotal		= $pedidoSubTotal - $pedidoDescTotal;
				$pedidoTotal 		= $pedidoTotal - $pedidoDescReais;
?>				

				<div id="dados_nfe">
                    <ul id="pedido_nfe_abas" class="menu_modo" style="width:470px; float:left">
                        <li><a href="totais">Totais</a></li>
                        <li><a href="entrega" onclick="blur();">Entrega</a></li>
                        <li><a href="transporte">Transporte</a></li>
                        <li><a href="informacoes">Inf. Adic.</a></li>
                    </ul>
					
                    <div id="pedido_nfe_totais">
                        <ul id="pedido_nfe_totais_abas" class="menu_modo" style="width:470px; float:left">
                            <li><a href="icms">ICMS</a></li>
                            <li><a href="issqn">ISSQN</a></li>
                            <li><a href="retencao">Reten&ccedil;&atilde;o de Tributos</a></li>
                        </ul>
                        
                        <div id="totais_icms">
                            <ul class="dados_nfe">
                                <li class="dados" style="margin-bottom:20px; margin-top:10px; width: 470px">
                                    <label for="sel_pedido_nfe_pagamento_forma">Forma Pagamento</label>
                                    <select style="width:305px; float: right" id="sel_pedido_nfe_pagamento_forma" name="sel_pedido_nfe_pagamento_forma" class="sel_pedido_nfe_pagamento_forma" title="Forma Pagamento">
										<option <?= ($nfePagamento == 0)? "selected='selected'":'' ?> value="0">Pagamento &agrave; vista</option>
										<option <?= ($nfePagamento == 1)? "selected='selected'":'' ?> value="1">Pagamento &agrave; prazo</option>
										<option <?= ($nfePagamento == 2)? "selected='selected'":'' ?> value="2">Outros</option>
                    				</select> 
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_produtos">Total dos Produtos</label>
                                    <input type="text" id="txt_pedido_nfe_total_produtos" name="txt_pedido_nfe_total_produtos" value="<?=format_number_out($rowNFe['fldprodutos_servicos_total'])?>" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_bc_icms">BC do ICMS</label>
                                    <input type="text" id="txt_pedido_nfe_total_bc_icms" name="txt_pedido_nfe_total_bc_icms" value="<?=format_number_out($rowNFe['fldicms_base_calculo'])?>" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_icms">Total ICMS</label>
                                    <input type="text" id="txt_pedido_nfe_total_icms" name="txt_pedido_nfe_total_icms" value="<?=format_number_out($rowNFe['fldicms_total'])?>" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_bc_icms_substituicao">BC ICMS Substitui&ccedil;&atilde;o</label>
                                    <input type="text" id="txt_pedido_nfe_total_bc_icms_substituicao" name="txt_pedido_nfe_total_bc_icms_substituicao" value="<?=format_number_out($rowNFe['fldicmsst_base_calculo'])?>" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_icms_substituicao">Total ICMS Substitui&ccedil;&atilde;o</label>
                                    <input type="text" id="txt_pedido_nfe_total_icms_substituicao" name="txt_pedido_nfe_total_icms_substituicao" value="<?=format_number_out($rowNFe['fldicmsst_total'])?>" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_frete">Total Frete</label>
                                    <input type="text" id="txt_pedido_nfe_total_frete" name="txt_pedido_nfe_total_frete" value="<?=format_number_out($rowNFe['fldfrete_total'])?>" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_seguro">Total Seguro</label>
                                    <input type="text" id="txt_pedido_nfe_total_seguro" name="txt_pedido_nfe_total_seguro" value="<?=format_number_out($rowNFe['fldseguro_total'])?>" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_desconto">Total Desconto</label>
                                    <input type="text" id="txt_pedido_nfe_total_desconto" name="txt_pedido_nfe_total_desconto" value="<?=format_number_out($totalNotaDesconto)?>" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_ii">Total II</label>
                                    <input type="text" id="txt_pedido_nfe_total_ii" name="txt_pedido_nfe_total_ii" value="<?=format_number_out($rowNFe['fldii_total'])?>" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_ipi">Total IPI</label>
                                    <input type="text" id="txt_pedido_nfe_total_ipi" name="txt_pedido_nfe_total_ipi" value="<?=format_number_out($rowNFe['fldipi_total'])?>" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_pis">Total PIS</label>
                                    <input type="text" id="txt_pedido_nfe_total_pis" name="txt_pedido_nfe_total_pis" value="<?=format_number_out($rowNFe['fldpis_total'])?>" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_cofins">Total Cofins</label>
                                    <input type="text" id="txt_pedido_nfe_total_cofins" name="txt_pedido_nfe_total_cofins" value="<?=format_number_out($rowNFe['fldcofins_total'])?>" readonly="readonly" />
                                </li>
                                
                                <li class="dados">
                                    <label for="txt_pedido_nfe_outras_despesas_acessorias">Out. Despesas Acess&oacute;rias</label>
                                    <input type="text" id="txt_pedido_nfe_outras_despesas_acessorias" name="txt_pedido_nfe_outras_despesas_acessorias" value="<?=format_number_out($rowNFe['flddespesas_acessorias_total'])?>" readonly="readonly" />
                                </li>
                                <li class="dados" >
                                    <label for="txt_pedido_nfe_total_nfe">Total da Nota</label>
                                    <input type="text" id="txt_pedido_nfe_total_nfe" name="txt_pedido_nfe_total_nfe" value="<?=format_number_out($totalNota)?>" readonly="readonly" />
                                </li>
                            </ul>
                            
                            <hr />
                            
                            <ul style="margin-top:20px">
                            	<li class="dados">
                                    <label for="txt_pedido_nfe_total_tributos">Total dos Tributos</label>
                                    <input type="text" id="txt_pedido_nfe_total_tributos" name="txt_pedido_nfe_total_tributos" value="<?=format_number_out($rowNFe['fldnfe_total_tributos'])?>" readonly="readonly" />
                                    <img src="image/layout/loading.gif" style="float:left; margin:2px 0 0 10px; display:none" id="total_tributos_loading" />
                                </li>
                            </ul>
						</div>
                        
                        <div id="totais_issqn">
                            <ul class="dados_nfe">
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_bc_iss">Base de C&aacute;lculo do ISS</label>
                                    <input type="text" id="txt_pedido_nfe_total_bc_iss" name="txt_pedido_nfe_total_bc_iss" value="<?=format_number_out($rowNFe['fldiss_base_calculo'])?>" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_iss">Total do ISS</label>
                                    <input type="text" id="txt_pedido_nfe_total_iss" name="txt_pedido_nfe_total_iss" value="<?=format_number_out($rowNFe['fldiss_total'])?>" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_pis_servicos">PIS sobre servi&ccedil;os</label>
                                    <input type="text" id="txt_pedido_nfe_total_pis_servicos" name="txt_pedido_nfe_total_pis_servicos" value="<?=format_number_out($rowNFe['fldpis_servicos_total'])?>" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_cofins_servicos">COFINS sobre servi&ccedil;os</label>
                                    <input type="text" id="txt_pedido_nfe_total_cofins_servicos" name="txt_pedido_nfe_total_cofins_servicos" value="<?=format_number_out($rowNFe['fldcofins_servicos_total'])?>" readonly="readonly" />
                                </li>
                                <li style="width: 470px" class="dados">
                                    <label for="txt_pedido_nfe_total_servicos_nao_incidencia_icms">Total dos servi&ccedil;os sob n&atilde;o incid&ecirc;ncia ou n&atilde;o tributados pelo ICMS</label>
                                    <input type="text" id="txt_pedido_nfe_total_servicos_nao_incidencia_icms" name="txt_pedido_nfe_total_servicos_nao_incidencia_icms" value="<?=format_number_out($rowNFe['fldicms_nao_incidencia_total'])?>" readonly="readonly" />
                                </li>
                            </ul>
						</div>     
                        <div id="totais_retencao">
                            <ul class="dados_nfe">
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_retido_pis">Valor retido de PIS</label>
                                    <input type="text" id="txt_pedido_nfe_total_retido_pis" name="txt_pedido_nfe_total_retido_pis" value="<?=format_number_out($rowNFe['fldpis_retido_total'])?>" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_retido_cofins">Valor retido de COFINS</label>
                                    <input type="text" id="txt_pedido_nfe_total_retido_cofins" name="txt_pedido_nfe_total_retido_cofins" value="<?=format_number_out($rowNFe['fldcofins_retido_total'])?>" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_retido_csll">Valor retido de CSLL</label>
                                    <input type="text" id="txt_pedido_nfe_total_retido_csll" name="txt_pedido_nfe_total_retido_csll" value="<?=format_number_out($rowNFe['fldcsll_retido_total'])?>" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_bc_irrf">Base de C&aacute;lculo do IRRF</label>
                                    <input type="text" id="txt_pedido_nfe_total_bc_irrf" name="txt_pedido_nfe_total_bc_irrf" value="<?=format_number_out($rowNFe['fldirrf_base_calculo'])?>" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_retido_irrf">Valor Retido do IRRF</label>
                                    <input type="text" id="txt_pedido_nfe_total_retido_irrf" name="txt_pedido_nfe_total_retido_irrf" value="<?=format_number_out($rowNFe['fldirrf_retido_total'])?>" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_bc_retencao_prev_social">BC Reten&ccedil;&atilde;o da Prev. Social</label>
                                    <input type="text" id="txt_pedido_nfe_bc_retencao_prev_social" name="txt_pedido_nfe_bc_retencao_prev_social" value="<?=format_number_out($rowNFe['fldretencao_prev_social_base_calculo'])?>" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_retencao_prev_social">Reten&ccedil;&atilde;o da Prev. Social</label>
                                    <input type="text" id="txt_pedido_nfe_total_retencao_prev_social" name="txt_pedido_nfe_total_retencao_prev_social" value="<?=format_number_out($rowNFe['fldretencao_prev_social_total'])?>" readonly="readonly" />
                                </li>
                            </ul>
						</div>                   
                    </div>
<?
					if($rowNFe['fldentrega_diferente_destinatario'] == '0'){
						$municipio_codigo = $rowDestinatario['fldMunicipio_Codigo'];
					}else{
						$municipio_codigo = $rowNFe['fldentrega_municipio_codigo'];
					}
?>                    
					<div id="pedido_nfe_entrega">
                    	<ul class="dados_nfe" style="margin:10px 0 0 3px">
							<li>
                    			<label for="txt_pedido_nfe_entrega_cpf_cnpj">CPF/CNPJ</label>
                    			<input type="text" style="width: 220px; text-align:left" id="txt_pedido_nfe_entrega_cpf_cnpj" <?=($rowNFe['fldentrega_diferente_destinatario'] == '0') ? 'readonly="readonly"' : ''; ?> name="txt_pedido_nfe_entrega_cpf_cnpj" value="<?=$rowNFe['fldentrega_cpf_cnpj'];?>" />
                    		</li>
                        	<li>
                                <label for="txt_pedido_nfe_entrega_endereco">Endereco</label>
                                <input type="text" style="width: 220px; text-align:left" id="txt_pedido_nfe_entrega_endereco" <?=($rowNFe['fldentrega_diferente_destinatario'] == '0') ? 'readonly="readonly"' : ''; ?> name="txt_pedido_nfe_entrega_endereco" value="<?=$rowNFe['fldentrega_endereco'];?>" />
							</li>
                        	<li>
                                <label for="txt_pedido_nfe_entrega_numero">N&uacute;mero</label>
                                <input type="text" style="width: 220px; text-align:left" id="txt_pedido_nfe_entrega_numero" <?=($rowNFe['fldentrega_diferente_destinatario'] == '0') ? 'readonly="readonly"' : ''; ?> name="txt_pedido_nfe_entrega_numero" value="<?=$rowNFe['fldentrega_numero'];?>" />
							</li>
                        	<li>
                                <label for="txt_pedido_nfe_entrega_complemento">Complemento</label>
                                <input type="text" style="width: 220px; text-align:left" id="txt_pedido_nfe_entrega_complemento" <?=($rowNFe['fldentrega_diferente_destinatario'] == '0') ? 'readonly="readonly"' : ''; ?> name="txt_pedido_nfe_entrega_complemento" value="<?=$rowNFe['fldentrega_complemento'];?>" />
							</li>
                        	<li>
                                <label for="txt_pedido_nfe_entrega_bairro">Bairro</label>
                                <input type="text" style="width: 220px; text-align:left" id="txt_pedido_nfe_entrega_bairro" <?=($rowNFe['fldentrega_diferente_destinatario'] == '0') ? 'readonly="readonly"' : ''; ?> name="txt_pedido_nfe_entrega_bairro" value="<?=$rowNFe['fldentrega_bairro'];?>" />
							</li>
                        	<li>
                                <label for="txt_pedido_nfe_entrega_cep">Cep</label>
                                <input type="text" style="width: 220px; text-align:left" id="txt_pedido_nfe_entrega_cep" <?=($rowNFe['fldentrega_diferente_destinatario'] == '0') ? ' readonly="readonly"' : ''; ?> name="txt_pedido_nfe_entrega_cep" value="<?=$rowNFe['fldentrega_cep'];?>" />
							</li>
                            <!-- aqui teve que ficar com esses ids no codigo do municipio por causa da busca, qe ja esta sendo parametrizada com esse id -->
                            <li>
                                <label for="txt_municipio_codigo_pedido_nfe">C&oacute;d. Munic&iacute;pio</label>
                                <input type="hidden" id="hid_pedido_nfe_municipio_entrega" name="hid_pedido_nfe_municipio_entrega" value="<?=$municipio_codigo?>" />
                                <input type="text" style=" width:185px; text-align:left"  id="txt_municipio_codigo_pedido_nfe" <?=($rowNFe['fldentrega_diferente_destinatario'] == '0') ? 'readonly="readonly"' : ''; ?> name="txt_municipio_codigo_pedido_nfe" value="<?=$rowNFe['fldentrega_municipio_codigo'];?>" />
                                <a style="float:left;" href="municipio_busca,pedido_nfe" title="Localizar" class="modal" rel="680-380"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                            </li>
                            <li>

                            	<?php

                            		$codigo_municipio 	= substr($rowNFe['fldentrega_municipio_codigo'], 2);
                            		$codigo_uf			= substr($rowNFe['fldentrega_municipio_codigo'], 0, 2);
                            		$qrMunicipio 		= mysql_query("SELECT fldNome FROM tblibge_municipio WHERE fldCodigo = '".$codigo_municipio."' AND fldUF_Codigo = '".$codigo_uf."'");
                            		$rowMunicipio 		= mysql_fetch_assoc($qrMunicipio);

                            	?>

                                <label for="txt_pedido_nfe_municipio">&nbsp;</label>
                                <input type="text" style=" width:220px; text-align:left" id="txt_pedido_nfe_municipio" name="txt_pedido_nfe_municipio" disabled="disabled" value="<?=$rowMunicipio['fldNome'];?>" />
                            </li>
                            <li style="width:463px;">
                            	<a style="margin:10px 0px 10px 4px; float:right" id="pedido_nfe_entrega_busca" class="btn_novo modal" href="pedido_nfe_entrega_busca,<?=$rowNFe['fldCliente_Id']?>" rel="900-350">buscar</a>
                            	<a style="margin:10px 0; float:right" class="btn_general" id="btn_entrega_limpar" href="">limpar</a>
                            </li>
							<li style="float:right; width:300px;">
								<span style="float:right; width:275px">
									<input type="checkbox" <?=($rowNFe['fldentrega_diferente_destinatario'] == '1') ? 'checked="checked"' : ''; ?> style="margin:0px 3px 0 0; width:22px;" value="1" id="chk_local_entrega_diferente" name="chk_local_entrega_diferente">
									<label for="chk_local_entrega_diferente" style="margin-top:3px">Local de entrega diferente do destinat&aacute;rio</label>
								</span>
                    		</li>
						</ul>
                    </div>
                    
                    <div id="pedido_nfe_transporte">
                    	<ul>
                 			<li>
                                <label for="sel_pedido_nfe_transporte_modalidade">Modalidade do frete</label>
                                <select id="sel_pedido_nfe_transporte_modalidade" name="sel_pedido_nfe_transporte_modalidade" class="sel_pedido_nfe_transporte_modalidade" title="Modalidade do frete">
<?									$rsTransporte = mysql_query("SELECT * FROM tblnfe_pedido_transporte");
                                    while($rowTransporte = mysql_fetch_array($rsTransporte)){
?>										<option <?= ($modFrete == $rowTransporte['fldCodigo']) ? "selected='selected'":'' ?> value="<?= $rowTransporte['fldCodigo'] ?>"><?= $rowTransporte['fldCodigo'] ?> - <?= $rowTransporte['fldDescricao'] ?></option>
<?									}
?>                    			</select> 
                            </li>
                            <li>
                                <label for="sel_pedido_nfe_transportador">Transportador</label>
                                <select style="width: 250px" id="sel_pedido_nfe_transportador" name="sel_pedido_nfe_transportador" class="sel_pedido_nfe_transportador" title="Transportador">
                                	<option value="0">Selecionar</option>
<?									$rsTransportador = mysql_query("SELECT * FROM tbltransportador");
                                    while($rowTransportador = mysql_fetch_array($rsTransportador)){
?>										<option <?= ($transportador == $rowTransportador['fldId']) ? "selected='selected'":'' ?> value="<?= $rowTransportador['fldId'] ?>"><?= $rowTransportador['fldNome'] ?></option>
<?									}
?>                    			</select> 
                            </li>  
                            <li style="width:20px; margin-bottom:0">
                            	<input style="width:20px" type="checkbox" name="chk_nfe_transporte_icms_isento" id="chk_nfe_transporte_icms_isento" value="true" <?= ($rowNFe['fldtransporte_icms_isento'] == 'true') ? "checked='checked'":'' ?> />
                            </li>
                            <li style="margin-bottom:0">
                            	<label for="chk_nfe_transporte_icms_isento">Isento do ICMS</label>
                            </li>
						</ul>
                                
                        <ul id="pedido_nfe_transporte_abas" class="menu_modo" style="width:470px;float:left">
                            <li style="margin-left:0;margin-right:0"><a href="volumes">Volumes</a></li>
                            <li style="margin-left:0;margin-right:0"><a href="retencao">Reten&ccedil;&atilde;o ICMS</a></li>
                            <li style="margin-left:0;margin-right:0"><a href="veiculo">Ve&iacute;culo/Reboque/Balsa/Vag&atilde;o</a></li>
                        </ul>
                        
                        <div id="pedido_nfe_transporte_volumes" style="float:left">
<?							$rsVolume = mysql_query("SELECT * FROM tblpedido_fiscal_transporte_volume WHERE fldPedido_Id = $documento_id");
							echo mysql_error();
							$rows = mysql_num_rows($rsVolume);		
							
?>                        	<input class="hid_volume_controle" type="hidden" name="hid_volume_controle" id="hid_volume_controle" value="<?=$rows?>" />
                            <div class="dados_listar">
                                <ul class="dados_cabecalho">
                                    <li style="width:40px">qtd</li>
                                    <li style="width:80px">esp&eacute;cie</li>
                                    <li style="width:90px">marca</li>
                                    <li style="width:40px">num.</li>
                                    <li style="width:80px">peso liq.</li>
                                    <li style="width:80px; border:0">peso bruto</li>
                                    <li style="width:75px; border:0">&nbsp;</li>
                                </ul>
                                
                                <div class="dados_listar_dados" id="dados_listar_volume"> 
                                	<div id="hidden"> 
                                        <ul class="volume_listar">
                                            <li style="width:40px"><input type="text" style="width:40px;border:0;background:none" class="txt_pedido_nfe_transporte_volume_qtd" 			name="txt_pedido_nfe_transporte_volume_qtd" 		value="" readonly="readonly" /></li>
                                            <li style="width:80px"><input type="text" style="width:80px;border:0;background:none" class="txt_pedido_nfe_transporte_volume_especie" 		name="txt_pedido_nfe_transporte_volume_especie" 	value="" readonly="readonly" /></li>
                                            <li style="width:90px"><input type="text" style="width:90px;border:0;background:none" class="txt_pedido_nfe_transporte_volume_marca" 		name="txt_pedido_nfe_transporte_volume_marca" 		value="" readonly="readonly" /></li>
                                            <li style="width:40px"><input type="text" style="width:40px;border:0;background:none" class="txt_pedido_nfe_transporte_volume_numero" 		name="txt_pedido_nfe_transporte_volume_numero" 		value="" readonly="readonly" /></li>
                                            <li style="width:80px"><input type="text" style="width:80px;border:0;background:none" class="txt_pedido_nfe_transporte_volume_peso_liquido" 	name="txt_pedido_nfe_transporte_volume_peso_liquido"value="" readonly="readonly" /></li>
                                            <li style="width:80px"><input type="text" style="width:80px;border:0;background:none" class="txt_pedido_nfe_transporte_volume_peso_bruto" 	name="txt_pedido_nfe_transporte_volume_peso_bruto" 	value="" readonly="readonly" /></li>
                                            <li style="width:22px"><a style="width:22px;height:20px" name="" class="edit edit_volume modal" href="" title="editar" rel="620-450"></a></li>
                                            <li style="width:20px"><a style="height:20px" class="a_excluir excluir_volume" id="excluir_0" href="" title="Excluir item"></a></li>
                                        </ul>
                                    </div>
<?										$x = 1;
										while($rowVolume = mysql_fetch_array($rsVolume)){    
											$rsLacre = mysql_query("SELECT * FROM tblpedido_fiscal_transporte_volume_lacre WHERE fldVolume_Id = ".$rowVolume['fldId']." ORDER BY fldId");
											$n = 1;
											while($rowLacre = mysql_fetch_array($rsLacre)){
												$lacre .= $n."=".$rowLacre['fldLacre'].";";
												$n++;
											}
?>                                      	<ul class="volume_listar">
                                                <li style="width:40px"><input type="text" style="width:40px;border:0;background:none" class="txt_pedido_nfe_transporte_volume_qtd_<?=$x?>" 			name="txt_pedido_nfe_transporte_volume_qtd_<?=$x?>" 		value="<?=$rowVolume['fldQuantidade']?>" readonly="readonly" /></li>
                                                <li style="width:80px"><input type="text" style="width:80px;border:0;background:none" class="txt_pedido_nfe_transporte_volume_especie_<?=$x?>" 		name="txt_pedido_nfe_transporte_volume_especie_<?=$x?>" 	value="<?=$rowVolume['fldEspecie']?>" readonly="readonly" /></li>
                                                <li style="width:90px"><input type="text" style="width:90px;border:0;background:none" class="txt_pedido_nfe_transporte_volume_marca_<?=$x?>" 		name="txt_pedido_nfe_transporte_volume_marca_<?=$x?>" 		value="<?=$rowVolume['fldMarca']?>" readonly="readonly" /></li>
                                                <li style="width:40px"><input type="text" style="width:40px;border:0;background:none" class="txt_pedido_nfe_transporte_volume_numero_<?=$x?>" 		name="txt_pedido_nfe_transporte_volume_numero_<?=$x?>" 		value="<?=$rowVolume['fldNumero']?>" readonly="readonly" /></li>
                                                <li style="width:80px"><input type="text" style="width:80px;border:0;background:none" class="txt_pedido_nfe_transporte_volume_peso_liquido_<?=$x?>" 	name="txt_pedido_nfe_transporte_volume_peso_liquido_<?=$x?>"value="<?=str_replace('.', ',', $rowVolume['fldPeso_Liquido'])?>" readonly="readonly" /></li>
                                                <li style="width:80px"><input type="text" style="width:80px;border:0;background:none" class="txt_pedido_nfe_transporte_volume_peso_bruto_<?=$x?>" 	name="txt_pedido_nfe_transporte_volume_peso_bruto_<?=$x?>" 	value="<?=str_replace('.', ',', $rowVolume['fldPeso_Bruto'])?>" readonly="readonly" /></li>
                                                <li style="width:22px"><a style="width:22px;height:20px" name="" class="edit edit_volume modal" href="pedido_nfe_transporte_volume,<?=$x.",".$lacre?>" title="editar" rel="620-450"></a></li>
                                                <li style="width:20px"><a style="height:20px;border:0" class="a_excluir" id="excluir_<?=$x?>" href="excluir_volume" title="Excluir item"></a></li>
                                            </ul>
<?											$x ++;
										}                                            
?>                                  
								</div>
                            </div>
                            <a style="float:right; margin: 10px" class="btn_novo modal" href="pedido_nfe_transporte_volume" rel="620-450">incluir</a>
                        </div>
                        
                        <div style="float:left" id="pedido_nfe_transporte_retencao">
                        	<ul class="dados_nfe">
                                <li style="width: 146px">
                                    <label for="txt_pedido_nfe_transporte_retencao_bc">Base de C&aacute;lculo</label>
                                    <input type="text" style="width:146px" id="txt_pedido_nfe_transporte_retencao_bc" name="txt_pedido_nfe_transporte_retencao_bc" value="<?=format_number_out($rowNFe['fldtransporte_retencao_icms_base_calculo'])?>" />
                                </li>
                                <li style="width: 146px">
                                    <label for="txt_pedido_nfe_transporte_retencao_aliquota">Al&iacute;quota</label>
                                    <input type="text" style="width:146px" id="txt_pedido_nfe_transporte_retencao_aliquota" name="txt_pedido_nfe_transporte_retencao_aliquota" value="<?=format_number_out($rowNFe['fldtransporte_retencao_icms_aliquota'])?>" />
                                </li>
                                <li style="width: 146px">
                                    <label for="txt_pedido_nfe_transporte_retencao_valor_servico">Valor do Servi&ccedil;o</label>
                                    <input type="text" style="width:146px" id="txt_pedido_nfe_transporte_retencao_valor_servico" name="txt_pedido_nfe_transporte_retencao_valor_servico" value="<?=format_number_out($rowNFe['fldtransporte_retencao_icms_servico_valor'])?>" />
                                </li>
                               	<li>
                                    <label for="sel_pedido_nfe_transporte_retencao_uf">UF</label>
                                    <select class="sel_uf" name="sel_pedido_nfe_transporte_retencao_uf" id="sel_pedido_nfe_transporte_retencao_uf" style="width: 225px">
                                    	<option value="0">Selecionar</option>
<?										$rsUF = mysql_query("SELECT * FROM tblibge_uf ORDER BY fldSigla");
                                    	while($rowUF = mysql_fetch_array($rsUF)){
?>                                 	  		<option <?=(fncNullId(substr($rowNFe['fldentrega_municipio_codigo'],0,2)) == $rowUF['fldCodigo']) ?  "selected='selected'" : '' ?> value="<?=$rowUF['fldCodigo']?>"><?=$rowUF['fldSigla']?></option>;
<?                                    	}
?>									</select>
                                </li>
                                <li>
                                    <label for="sel_pedido_nfe_transporte_retencao_municipio">Munic&iacute;pio</label>
                                    <select class="sel_municipio" name="sel_pedido_nfe_transporte_retencao_municipio" style="width: 225px">
<?										if($rowNFe['fldentrega_municipio_codigo'] != 0){
											$rsMunicipio = mysql_query("SELECT * FROM tblibge_municipio ORDER BY fldNome");
                                    		while($rowMunicipio = mysql_fetch_array($rsMunicipio)){
?>                                 	  			<option <?=(substr($rowNFe['fldentrega_municipio_codigo'],2,5) == $rowMunicipio['fldCodigo']) ?  "selected='selected'" : '' ?> value="<?=$rowMunicipio['fldCodigo']?>"><?=$rowMunicipio['fldNome']?></option>;
<?                                    		}
										}
?>									</select>
                                </li>
                                <li style="width: 320px">
                                    <label for="sel_pedido_nfe_transporte_retencao_cfop">CFOP</label>
                                    <select name="sel_pedido_nfe_transporte_retencao_cfop" style="width: 320px">
<?										$rsCFOP = mysql_query('SELECT * FROM tblnfe_cfop WHERE fldModo = 2');
										while($rowCFOP = mysql_fetch_array($rsCFOP)){
?>											<option <?=($rowNFe['fldtransporte_retencao_icms_cfop'] == $rowCFOP['fldId'])? "selected='selected'" :'' ?> value="<?=$rowCFOP['fldId']?>"><?=$rowCFOP['fldCFOP']?> - <?=$rowCFOP['fldDescricao']?></option>
<?                              		}
?>									</select>
                                </li>
                                <li style="width: 130px">
                                    <label for="txt_pedido_nfe_transporte_retencao_icms_retido">ICMS Retido</label>
                                    <input type="text" style="width:130px" id="txt_pedido_nfe_transporte_retencao_icms_retido" name="txt_pedido_nfe_transporte_retencao_icms_retido" value="<?=format_number_out($rowNFe['fldtransporte_retencao_icms_retencao_valor'])?>" />
                                </li>
                            </ul>
                        </div>
                        
                    	<div id="pedido_nfe_transporte_veiculo" style="float:left">
                        	<ul style="border-bottom: 1px solid;">
                            	<li style="width: 20px; margin-bottom:0">
	                                <input type="radio" style="width:20px" name="rad_transporte" value="veiculo" checked="checked" />
                                </li>
                                <li style="width:120px;margin-bottom:0">
                                	<label for="rad_veiculo">Veiculo/Reboque</label>
                                </li>
                            	<li style="width: 20px; margin-bottom:0">
	                                <input type="radio" style="width:20px" name="rad_transporte" value="balsa" />
                                </li>
                                <li style="width:120px;margin-bottom:0">
                                	<label for="rad_veiculo">Balsa</label>
                                </li>
                            	<li style="width: 20px; margin-bottom:0">
	                                <input type="radio" style="width:20px" value="vagao" name="rad_transporte" />
                                </li>
                                <li style="width:120px;margin-bottom:0">
                                	<label for="rad_veiculo">Vag&atilde;o</label>
                                </li>
                            </ul>
                            
                            <div id="rad_veiculo" style="float:left">
                                <ul class="dados_nfe" style="margin-bottom: 5px">
                                    <li style="width: 146px">
                                        <label for="txt_pedido_nfe_transporte_veiculo_placa">Placa</label>
                                        <input type="text" style="width:146px; text-align:left" id="txt_pedido_nfe_transporte_veiculo_placa" name="txt_pedido_nfe_transporte_veiculo_placa" value="<?=$rowNFe['fldtransporte_veiculo_placa']?>" />
                                    </li>
                                    <li style="width: 100px">
                                        <label for="sel_pedido_nfe_transporte_veiculo_uf">UF</label>
                                        <select name="sel_pedido_nfe_transporte_veiculo_uf" id="sel_pedido_nfe_transporte_veiculo_uf" style="width: 100px">
                                            <option value="0">Selecionar</option>
<?											$rsUF = mysql_query("SELECT * FROM tblibge_uf ORDER BY fldSigla");
                                    		while($rowUF = mysql_fetch_array($rsUF)){
?>                                 	  			<option <?=(substr($rowNFe['fldtransporte_veiculo_uf'],0,2) == $rowUF['fldCodigo'])? "selected='selected'" : '' ?> value="<?=$rowUF['fldCodigo']?>"><?=$rowUF['fldSigla']?></option>;
<?                                    		}
?>                                  	</select>
                                    </li>
                                    <li style="width: 146px">
                                        <label for="txt_pedido_nfe_transporte_veiculo_rntc">RNTC</label>
                                        <input type="text" style="width:146px; text-align:left" id="txt_pedido_nfe_transporte_veiculo_rntc" name="txt_pedido_nfe_transporte_veiculo_rntc" value="<?=$rowNFe['fldtransporte_veiculo_rntc']?>" />
                                    </li>
                                </ul>
<?								$rsReboque = mysql_query("SELECT * FROM tblpedido_fiscal_transporte_reboque WHERE fldPedido_Id = $documento_id");
								echo mysql_error();
								$rows = mysql_num_rows($rsReboque);                                
?>                              <input class="hid_reboque_controle" type="hidden" name="hid_reboque_controle" id="hid_reboque_controle" value="0" />
                                <div class="dados_listar" style="height:80px">
                                    <ul class="dados_cabecalho">
                                        <li style="width:150px">Placa</li>
                                        <li style="width:80px">UF</li>
                                        <li style="width:247px">RNTC</li>
                                    </ul>
                                   
                                    <div class="dados_listar_dados" style="height:80px" id="dados_listar_reboque"> 
                                    	<div id="hidden">     
                                            <ul class="reboque_listar">
                                                <li style="width:150px"><input type="text" style="width:150px" class="txt_pedido_nfe_transporte_reboque_placa"					name="txt_pedido_nfe_transporte_reboque_placa" 	value="" readonly="readonly" /></li>
                                                <li style="width:80px"><input  type="text" style="width:80px; text-align:center" class="txt_pedido_nfe_transporte_reboque_uf" 	name="txt_pedido_nfe_transporte_reboque_uf" 	value="" readonly="readonly" /></li>
                                                <li><input  type="hidden" class="hid_pedido_nfe_transporte_reboque_uf" 	name="hid_pedido_nfe_transporte_reboque_uf" value="" 	readonly="readonly" /></li>
                                                <li style="width:180px"><input type="text" style="width:180px" class="txt_pedido_nfe_transporte_reboque_rntc" 					name="txt_pedido_nfe_transporte_reboque_rntc" 	value="" readonly="readonly" /></li>
                                                <li style="width:22px"><a style="width:22px;height:22px;background-position:5px" name="" class="edit edit_reboque modal" href="" title="editar" rel="470-150"></a></li>
                                                <li style="width:22px"><a style="height:22px" class="a_excluir excluir_reboque" id="excluir_0" href="" title="Excluir item"></a></li>
                                    		</ul>
                                    	</div>
<?										$x = 1;
										while($rowReboque = mysql_fetch_array($rsReboque)){    
?>											<ul class="reboque_listar">
                                                <li style="width:150px"><input type="text" style="width:150px" class="txt_pedido_nfe_transporte_reboque_placa"					name="txt_pedido_nfe_transporte_reboque_placa_<?=$x?>" 	value="" readonly="readonly" /></li>
                                                <li style="width:80px"><input  type="text" style="width:80px; text-align:center" class="txt_pedido_nfe_transporte_reboque_uf" 	name="txt_pedido_nfe_transporte_reboque_uf_<?=$x?>" 	value="" readonly="readonly" /></li>
                                                <li><input  type="hidden" class="hid_pedido_nfe_transporte_reboque_uf" 	name="hid_pedido_nfe_transporte_reboque_uf_<?=$x?>" 	value="" 	readonly="readonly" /></li>
                                                <li style="width:180px"><input type="text" style="width:180px" class="txt_pedido_nfe_transporte_reboque_rntc" 					name="txt_pedido_nfe_transporte_reboque_rntc_<?=$x?>" 	value="" readonly="readonly" /></li>
                                                <li style="width:22px"><a style="width:22px;height:22px;background-position:5px" name="edit_<?=$x?>" class="edit edit_reboque modal" href="pedido_nfe_transporte_reboque,<?=$x?>" title="editar" rel="470-150"></a></li>
                                                <li style="width:22px"><a style="height:22px" class="a_excluir excluir_reboque" id="excluir_<?=$x?>" href="" title="Excluir item"></a></li>
                                    		</ul>                                        
<?                                        	$x ++;
										}
?>                                	</div>
                            	</div>
                            	<a style="float:right; margin: 10px" class="btn_novo" id="btn_reboque_novo" href="pedido_nfe_transporte_reboque" rel="470-150">incluir</a>
							</div>
                            <div id="rad_balsa" style="float:left">
                            	<ul class="dados_nfe">
                                    <li style="width: 420px">
                                        <label for="txt_pedido_nfe_transporte_balsa_identificacao">Identifica&ccedil;&atilde;o da Balsa</label>
                                        <input type="text" style="width:420px" id="txt_pedido_nfe_transporte_balsa_identificacao" name="txt_pedido_nfe_transporte_balsa_identificacao" value="<?=$rowNFe['fldtransporte_balsa_identificacao']?>" />
                                    </li>
                                </ul>
                            </div>   
                            <div id="rad_vagao" style="float:left">
                            	<ul class="dados_nfe">
                                    <li style="width: 420px">
                                        <label for="txt_pedido_nfe_transporte_vagao_identificacao">Identifica&ccedil;&atilde;o do Vag&atilde;o</label>
                                        <input type="text" style="width:420px" id="txt_pedido_nfe_transporte_vagao_identificacao" name="txt_pedido_nfe_transporte_vagao_identificacao" value="<?=$rowNFe['fldtransporte_balsa_identificacao']?>" />
                                    </li>
                                </ul>
                            </div>                                
                        </div>
                        
                    </div>
                    
                    <div id="pedido_nfe_informacoes">
                    	<ul class="dados_nfe" style="margin-left:20px">
                        	<li style="width: 420px">
                                <label for="txt_pedido_nfe_fisco">Fisco</label>
                                <textarea style=" width:420px; height:100px" id="txt_pedido_nfe_fisco" name="txt_pedido_nfe_fisco"><?=$infoFisco?></textarea>
							</li>
                        	<li style="width: 420px">
                                <label for="txt_pedido_nfe_contribuite">Contribuinte</label>
                                <textarea style=" width:420px; height:100px" id="txt_pedido_nfe_contribuite" name="txt_pedido_nfe_contribuite"><?=$infoContribuinte?></textarea>
							</li>
						</ul>
                    </div>
                </div>  

               	<div id="pedido_pagamento" style="margin-left:35px; width:445px">
                    <div class="pagamento_bottom">
                        <ul style="float:right; margin-right: 10px">
                            <li style="visibility: hidden;">
                                <label class="pedido" for="txt_pedido_produtos">Total Nota</label>
                            	<input type="text" id="txt_pedido_produtos" name="txt_pedido_produtos" value="<?=format_number_out($totalNota)?>" readonly="readonly" />
                            </li>
                            <li>
                                <label class="pedido" for="txt_pedido_servicos">Servi&ccedil;os</label>
                            	<input type="text" id="txt_pedido_servicos" name="txt_pedido_servicos" value="<?=format_number_out($totalServicos)?>" readonly="readonly" />
                            </li>
                            <li>
                                <label class="pedido" for="txt_pedido_terceiros">Terceiros</label>
                            	<input style="background:#FEF9BA; height:25px; font-size:18px" type="text" id="txt_pedido_terceiros" name="txt_pedido_terceiros" value="<?=format_number_out($totalTerceiros)?>" <?=($venda_origem > 0)? 'readonly="readonly"' : '' ?> />
                            </li>
                            <li>
                                <label class="pedido" for="txt_pedido_subtotal">Subtotal</label>
                            	<input style="background:#D7F9C6; height:25px; font-size:18px" type="text" id="txt_pedido_subtotal" name="txt_pedido_subtotal" value="<?=format_number_out($pedidoSubTotal)?>" readonly="readonly" />
                            </li>
                            <li>
                                <label for="txt_pedido_desconto">Desconto (%)</label>
                                <input style="background:#C0E9F8; height:22px; font-size:16px" type="text" id="txt_pedido_desconto" name="txt_pedido_desconto" value="<?=format_number_out($pedidoDesc)?>" <?=($venda_origem > 0)? 'readonly="readonly"' : '' ?> />
                            </li>
                            <li>
                                <label for="txt_pedido_desconto_reais">Desconto (R$)</label>
                                <input style="background:#C0E9F8; height:22px; font-size:16px" type="text" id="txt_pedido_desconto_reais" name="txt_pedido_desconto_reais" value="<?=format_number_out($pedidoDescReais)?>" <?=($venda_origem > 0)? 'readonly="readonly"' : '' ?> />
                            </li>
							<li>
								<label for="txt_pedido_nfe_total_desconto">Desconto em itens</label>
								<input style="background:#C0E9F8; height:22px; font-size:16px" type="text" id="txt_pedido_nfe_total_desconto" name="txt_pedido_nfe_total_desconto" value="<?=format_number_out($totalNotaDesconto)?>" readonly="readonly" />
							</li>
                            <li>
                                <label class="total" for="txt_pedido_total">Total</label>
                                <input type="text" class="total" id="txt_pedido_total" name="txt_pedido_total" value="<?=format_number_out($pedidoTotal)?>" readonly="readonly" />
                            </li>
<?							if($nfe_parcelamento_exibir == '1'){
?>								<li>
                                    <a style="margin:0; float:right" class="modal btn_cheque" name="btn_cheque" id="btn_cheque" title="exibir cheques" href="financeiro_cheque_listar,3,<?=$documento_id?>" rel="780-410">cheques</a>
                                </li>
<?							}
?>                          
						</ul>
                    </div>
<?					if($nfe_parcelamento_exibir == '1'){
						require_once('componentes/parcelamento/parcelas_editar.php');
					}
?>		
                </div>
                <div style="float:right">
                    <input type="submit" class="btn_enviar" name="btn_gravar" id="btn_gravar" value="Gravar" title="Gravar" />
                </div>
            </form>
        </div>
<?	}
?>  </div>
	
<?		if($rowNFe["fldnfe_status_id"] =='4' || $rowNFe["fldnfe_status_id"] =='6'){
?>			<script type="text/javascript">
				$('#frm_pedido_nfe_novo ').find('input:not(#txt_pedido_chave), select, textarea, button').attr("disabled", true);	
			</script>
<?		}
?>

    <script type="text/javascript">
	
		$('#txt_cliente_codigo').focus();
		
        $('div#dados_nfe div').hide();
        $('div#pedido_nfe_totais').show();
        $('div#pedido_nfe_totais div#totais_icms').show();
        
        $('ul#pedido_nfe_abas a[href=totais]').addClass('ativo');
        $('ul#pedido_nfe_totais_abas a[href=icms]').addClass('ativo');
		//$('div#pedido_nfe_totais input:first').focus();
		
        $('ul#pedido_nfe_abas a').click(function(event){
            event.preventDefault();
            //marcar aba ativa
            $('ul#pedido_nfe_abas a').removeClass('ativo');
            $(this).addClass('ativo');
            
            //ocultar todas as abas
            $('div#dados_nfe div').hide();
			
            //exibir a selecionada
            var aba = $(this).attr('href');
			
			if(aba == 'transporte'){
				$('div#pedido_nfe_transporte div#pedido_nfe_transporte_volumes').show();
				$('div#pedido_nfe_transporte div#pedido_nfe_transporte_volumes div').show();
				$('div.dados_listar_dados div#hidden').hide(); // pra esconder div hidden
			}else if(aba == 'totais'){
				$('div#pedido_nfe_totais div#totais_icms').show();
           		$('ul#pedido_nfe_totais_abas a').removeClass('ativo');
            	$('ul#pedido_nfe_totais_abas  a[href=icms]').addClass('ativo');
			}
			
            $('div#pedido_nfe_'+aba).show();
			$('div#pedido_nfe_'+aba+' input:first, div#pedido_nfe_'+aba+' textarea:first').focus();
        });
		
		//#############################################################################################################################################
		$('ul#pedido_nfe_totais_abas a').click(function(event){
            event.preventDefault();
            //marcar aba ativa
            $('ul#pedido_nfe_totais_abas a').removeClass('ativo');
            $(this).addClass('ativo');
            
            //ocultar todas as abas
            $('div#pedido_nfe_totais div').hide();
			
            //exibir a selecionada
            var aba = $(this).attr('href');
			
            $('div#totais_'+aba).show();
			$('div#totais_'+aba+' input:first, div#totais_'+aba+' textarea:first').focus();
        });
		
		//#############################################################################################################################################
		$('div#pedido_nfe_transporte div').hide();
        $('ul#pedido_nfe_transporte_abas a[href=volumes]').addClass('ativo');
		
		$('ul#pedido_nfe_transporte_abas a').click(function(event){
            event.preventDefault();
			
            //marcar aba ativa
            $('ul#pedido_nfe_transporte_abas a').removeClass('ativo');
            $(this).addClass('ativo');
           
            //ocultar todas as abas
            $('div#pedido_nfe_transporte div').hide();
			
            //exibir a selecionada
            var aba = $(this).attr('href');
            $('div#pedido_nfe_transporte_'+aba).show();
            $('div#pedido_nfe_transporte_'+aba+' div').show();
			$('div.dados_listar_dados div#hidden').hide(); // pra esconder div hidden
			$('div#pedido_nfe_transporte_'+aba+' input:first, div#pedido_nfe_transporte_'+aba+' textarea:first').focus();
       
	   });
    </script>
    