

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="My ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
        <title>Impress&atilde;o de carn&ecirc;</title>
		<link rel="stylesheet" type="text/css" href="style/impressao/style_pedido_imprimir_recibo_A4.css" />
    	<link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />
    
	</head>
	<body>
    
<? 
	ob_start();
	session_start();
	
	require("inc/con_db.php");
	require("inc/fnc_general.php");
	
	$pedidoId =  $_GET['id'];
	$rowDados = mysql_fetch_array(mysql_query("select * from tblempresa_info"));
		
	$rsParcelas = mysql_query("SELECT tblpedido.*,
								tblpedido_parcela.fldValor as fldValorParcela,
								tblpedido_parcela.fldId as fldParcela_Id,
								tblpedido_parcela.*,
								tblcliente.fldNome as fldClienteNome,
								tblcliente.fldCodigo as fldClienteCodigo,
								tblcliente.fldCPF_CNPJ as fldClienteCPF,
								tblpedido_marcador.fldMarcador
								FROM tblpedido 
								INNER JOIN tblpedido_parcela ON tblpedido_parcela.fldPedido_Id = tblpedido.fldId
								INNER JOIN tblcliente ON tblpedido.fldCliente_Id = tblcliente.fldId
								LEFT JOIN tblpedido_marcador ON tblpedido_marcador.fldId = tblpedido.fldMarcador_Id
								WHERE tblpedido_parcela.fldPedido_Id = $pedidoId AND tblpedido_parcela.fldExcluido = 0");
	
	
	echo mysql_error();
	$data = date("Y-m-d");
	$hora = date("H:i:s");
	
	$rows = mysql_num_rows(mysql_query("SELECT fldId FROM tblpedido_parcela WHERE fldPedido_Id = $pedidoId AND fldExcluido = 0"));
?>
    <div id="no-print">
        <a class="print" href="#" onClick="window.print()">imprimir</a>
    </div>
<?	$limite = 4;
	$x 		= 1;
	while($rowParcela = mysql_fetch_array($rsParcelas)){
?>	
        <div class="recibo_print" style="width:19cm;height:6cm;margin-left:1cm;<?= ($x == $limite) ?'page-break-after: always;' : ''?>margin-top:1cm">
        	<div style=" width:11cm;display:table; float:left">
                <h1>Carn&ecirc; de pagamento</h1>
                <p style="width:10cm">
                    <span><?=format_date_out($data)?></span>&nbsp;<span><?=format_time_short($hora)?></span>
                </p>
                <p style="width:10cm">
                    <span style="font-size:15px"><?=$rowDados['fldNome_Fantasia']?></span>
                    <br />
                    <span style="width:1.2cm;display:inline-table">Curso:</span>
					<span><strong><?=$rowParcela['fldMarcador']?></strong></span>
                </p>
                <p style="width:9cm;">
                	<span style="width:1.2cm;display:inline-table">Cliente:</span>
                    <span><strong><?=$rowParcela['fldClienteNome']?></strong></span>
                	<br />
                    <span style="width:1.2cm;display:inline-table">CPF:</span>
                    <span><strong><?=format_cpf_out($rowParcela['fldClienteCPF'])?></strong></span>
				</p>
                <br /><br />
                <div class="dados" style=" width:10.5cm;float:left;margin-left:0.5cm">
                    <ul class="cabecalho">
                        <li>Venda</li>
                        <li style="width:40px">Parc.</li>
                        <li>Vencimento</li>
                        <li style="text-align:right">Valor</li>
                    </ul>
                    <ul class="dados">
                        <li><?=$rowParcela['fldPedido_Id']?></li>
                        <li style="width:40px"><?=$rowParcela['fldParcela']?>/<?=$rows?></li>
                        <li><?=format_date_out($rowParcela['fldVencimento'])?></li>
                        <li style="text-align:right"><?=format_number_out($rowParcela['fldValorParcela'])?></li>
                    </ul>
                </div>
				<p style="font-size: 11px; margin-top: 1.3cm; display: block; text-align: center;">Multa por atraso de pagamento 2,00% + 0,16% ao dia</p>
			</div>
            
            <div style=" width:7.5cm; height:6cm;border-left:1px dashed;float:left; margin-left:0.2cm">
            	<h1>Carn&ecirc; de pagamento</h1>
                <p style="width:7.5cm">
                    <span><?=format_date_out($data)?></span>&nbsp;<span><?=format_time_short($hora)?></span>
                </p>
                <p style="width:7.5cm">
                    <span style="font-size:15px"><?=$rowDados['fldNome_Fantasia']?></span>
                    <br />
                    <span style="width:1.2cm;display:inline-table">Marcador:</span>
					<span><strong><?=$rowParcela['fldMarcador']?></strong></span>
                </p>
                <p style="width:8cm;">
                	<span style="width:1.2cm;display:inline-table">Cliente:</span>
                    <span><strong><?=$rowParcela['fldClienteNome']?></strong></span>
                	<br />
                    <span style="width:1.2cm;display:inline-table">CPF:</span>
                    <span><strong><?=format_cpf_out($rowParcela['fldClienteCPF'])?></strong></span>
				</p>
                <br /><br />
                <div class="dados" style=" width:7cm;float:left;margin-left:0.5cm">
                    <ul class="cabecalho">
                        <li style="width:1.2cm">Venda</li>
                        <li style="width:1cm">Parc.</li>
                        <li style="width:2cm">Vencimento</li>
                        <li style="text-align:right">Valor</li>
                    </ul>
                    <ul class="dados">
                        <li style="width:1.2cm"><?=$rowParcela['fldPedido_Id']?></li>
                        <li style="width:1cm"><?=$rowParcela['fldParcela']?>/<?=$rows?></li>
                        <li style="width:2cm"><?=format_date_out($rowParcela['fldVencimento'])?></li>
                        <li style="text-align:right"><?=format_number_out($rowParcela['fldValorParcela'])?></li>
                    </ul>
                </div>
				<p style="font-size: 11px; margin-top: 1.3cm; display: block; width: 7.5cm;">Multa por atraso de pagamento: 2,00% + 0,16% ao dia</p>
			</div>
        </div>

<?		($x > $limite) ? $x = 0: $x++;
	}
?>
    </body>
</html>
