<?php
		
	$_SESSION['sel_cheque'] = (isset($_SESSION['sel_cheque']) ? $_SESSION['sel_cheque'] : "caixa");

	//memorizar os filtros para exibição nos selects
	if(isset($_POST['btn_limpar'])){
		$_SESSION['txt_cheque_conta'] 		= "";
		$_SESSION['txt_cheque_numero']		= "";
		$_SESSION['txt_nome_correntista']	= "";
		$_SESSION['txt_data1'] 				= "";
		$_SESSION['txt_data2'] 				= "";
		$_SESSION['sel_cheque'] 			= "caixa";
	}
	else{
		$_SESSION['txt_cheque_conta'] 		= (isset($_POST['txt_cheque_conta']) 		? $_POST['txt_cheque_conta'] 		: $_SESSION['txt_cheque_conta']);
		$_SESSION['txt_cheque_numero']		= (isset($_POST['txt_cheque_numero']) 		? $_POST['txt_cheque_numero'] 		: $_SESSION['txt_cheque_numero']);
		$_SESSION['txt_nome_correntista'] 	= (isset($_POST['txt_nome_correntista']) 	? $_POST['txt_nome_correntista'] 	: $_SESSION['txt_nome_correntista']);
		$_SESSION['txt_data1']				= (isset($_POST['txt_data1']) 				? $_POST['txt_data1'] 				: $_SESSION['txt_data1']);
		$_SESSION['txt_data2'] 				= (isset($_POST['txt_data2']) 				? $_POST['txt_data2'] 				: $_SESSION['txt_data2']);
		$_SESSION['sel_cheque'] 			= (isset($_POST['sel_cheque']) 				? $_POST['sel_cheque'] 				: $_SESSION['sel_cheque']);
	}
?>

<form id="frm-filtro" action="" method="post" >
	<fieldset>
  		<legend>Buscar por:</legend>
  		<ul>
    		<li>
<?				$cheque_conta = ($_SESSION['txt_cheque_conta'] 	? $_SESSION['txt_cheque_conta'] : "n&ordm; conta");
				($_SESSION['txt_cheque_conta'] == "nº conta") 	? $_SESSION['txt_cheque_conta'] = '' : '';
?>      		<input style="width: 80px" type="text" name="txt_cheque_conta" id="txt_cheque_conta" onfocus="limpar (this,'n&ordm; conta');" onblur="mostrar (this, 'n&ordm; conta');" value="<?=$cheque_conta?>"/>
            </li>
    		<li>
<?				$cheque_numero = ($_SESSION['txt_cheque_numero'] ? $_SESSION['txt_cheque_numero'] : "n&ordm; cheque");
				($_SESSION['txt_cheque_numero'] == "nº cheque") ? $_SESSION['txt_cheque_numero'] = '' : '';
?>      		<input style="width: 80px" type="text" name="txt_cheque_numero" id="txt_cheque_numero" onfocus="limpar (this,'n&ordm; cheque');" onblur="mostrar (this, 'n&ordm; cheque');" value="<?=$cheque_numero?>"/>
            </li>
            <li>
<?				$nome_correntista = ($_SESSION['txt_nome_correntista'] ? $_SESSION['txt_nome_correntista'] : "nome do correntista");
				($_SESSION['txt_nome_correntista'] == "nome do correntista") ? $_SESSION['txt_nome_correntista'] = '' : '';
?>      		<input style="width: 200px" type="text" name="txt_nome_correntista" id="txt_nome_correntista" onfocus="limpar (this,'nome do correntista');" onblur="mostrar (this, 'nome do correntista');" value="<?=$nome_correntista?>"/>
            </li>
            <li>
      			<label for="txt_data1">Vencimento: </label>
<?				$data1 = ($_SESSION['txt_data1'] ? $_SESSION['txt_data1'] : "");
?>     			<input title="Data inicial" style="text-align:center;width: 80px" type="text" name="txt_data1" id="txt_data1" class="calendario-mask" value="<?=$data1?>"/>
            </li>
            <li>
<?				$data2 = ($_SESSION['txt_data2'] ? $_SESSION['txt_data2'] : "");
?>     			<input title="Data final" style="text-align:center;width: 80px" type="text" name="txt_data2" id="txt_data2" class="calendario-mask" value="<?=$data2?>"/>
            </li>
            <li>
                <select style=" width:145px" id="sel_cheque" name="sel_cheque">
                    <option <?=($_SESSION['sel_cheque'] == 'todos' ? print "selected='selected'" : "");?> value="todos">TODOS</option>
                    <option <?=($_SESSION['sel_cheque'] == 'caixa' ? print "selected='selected'" : "");?> value="caixa">EM CAIXA</option>
                    <option <?=($_SESSION['sel_cheque'] == 'saida' ? print "selected='selected'" : "");?> value="saida">SA&Iacute;DAS</option>
                </select>
            </li>
        </ul>
        <button type="submit" id="btn_exibir" name="btn_exibir" title="Exibir">Exibir</button>
        <button type="submit" id="btn_limpar" name="btn_limpar" title="Limpar Filtro">Limpar filtro</button>
	</fieldset>
</form>

<?
	$filtro = "WHERE tblcheque.fldId > 0 ";
	
	if(($_SESSION['txt_cheque_conta']) != ""){
		$filtro .= "AND fldConta = '".$_SESSION['txt_cheque_conta']."'";
	}
	
	if(($_SESSION['txt_cheque_numero']) != ""){
		$filtro .= "AND fldNumero = '".$_SESSION['txt_cheque_numero']."'";
	}
	
	if(($_SESSION['txt_nome_correntista']) != ""){
		$correntista = addslashes($_SESSION['txt_nome_correntista']); // no caso de aspas, pra nao dar erro na consulta
		$filtro 	.= "AND fldNome LIKE '".$correntista."%'";
	}
	
	if(format_date_in($_SESSION['txt_data1']) != ""){

		if(format_date_in($_SESSION['txt_data2']) != ""){
			$filtro .= " AND fldVencimento BETWEEN '".format_date_in($_SESSION['txt_data1'])."' AND '".format_date_in($_SESSION['txt_data2'])."'";
		}else{
			$filtro .= " AND fldVencimento = '".format_date_in($_SESSION['txt_data1'])."'";
		}
	}
				
	if(($_SESSION['sel_cheque']) != ""){
		
		switch($_SESSION["sel_cheque"]){
			case "todos":
				$filtro .= "";
			break;
			case "caixa":
				$filtro .= " AND fldDestino_Id = 0";
			break;
			case "saida":
				$filtro .= " AND fldDestino_Id > 0";
			break;
		}
	}

	//transferir para a sessão
	$_SESSION['filtro_cheque'] = $filtro;

?>
 