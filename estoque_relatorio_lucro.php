<?Php
		#ABAIXO CRIO O CABECALHO #########################################################################################################################################################
		$tabelaCabecalho =' 
			<tr style="border-bottom: 2px solid">
                <td style="width: 600px"><h1>Relat&oacute;rio de Estoque</h1></td>';
		$tabelaCabecalho2 =' 
            </tr>
            <tr>
                <td>
                    <table style="width: 580px" class="table_relatorio_dados" summary="Relat&oacute;rio">
                        <tr>
                            <td style="width: 320px;">Raz&atilde;o Social: '.$rowDados['fldNome'].'</td>
                            <td style="width: 200px;">Nome Fantasia: '.$rowDados['fldNome_Fantasia'].'</td>
                            <td style="width: 320px;">CPF/CNPJ: '.$CPF_CNPJDados.'</td>
                            <td style="width: 200px;">Telefone: '.$rowDados['fldTelefone1'].'</td>
                        </tr>
                    </table>	
                </td>
                <td>        
                    <table class="dados_impressao">
                        <tr>
                            <td><b>Data: 			</b><span>'.format_date_out(date("Y-m-d")).'</span></td>
                            <td><b>Hora: 			</b><span>'.format_time_short(date("H:i:s")).'</span></td>
                            <td><b>Usu&aacute;rio: 	</b><span>'.$rowUsuario['fldUsuario'].'</span></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="border-top: 1px solid">
				<td style="width:75px;margin-right:10px; text-align:right">C&oacute;d</td>
				<td style="width:270px">Produto</td>
				<td style="width:70px; text-align:right">Estoque</td>
				<td style="width:73px; text-align:right">Vl Unit.</td>
				<td style="width:73px; text-align:right">Total</td>
				<td style="width:73px; text-align:right">Vl Compra</td>
				<td style="width:73px; text-align:right">Total</td>
				<td style="width:73px; text-align:right">Lucro</td>
				<td style="width:8px"></td>
			</tr>';
			$tabelaCabecalho3 ='
			<tr>
				<td>
					<table class="table_relatorio" summary="Relat&oacute;rio">';
		##########################################################################################################################################################################################
		
			$sSQL 			= $_SESSION['relatorio_estoque']. $_SESSION['txt_estoque_quantidade']." ORDER BY ". $_SESSION['order_estoque'].", tblproduto_estoque_movimento.fldData, tblproduto_estoque_movimento.fldHora, tblproduto_estoque_movimento.fldId ";
			$rsEstoque  	= mysql_query($sSQL);
			$rowsEstoque 	= mysql_num_rows($rsEstoque);
		
			$n	 			= 1; #DEFINE O NUMERO DA DO BLOCO
			$countRegistro  = 1; #DEFINE CONTAGEM DE ITENS NO WHILE PARA QUEBRA DE BLOCO AO ATINGIR LIMITE DE 20
			$x				= 1; #DEFINE CONTAGEM DE ITENS TOTAIS, PRA SABER SE JA TERMINOU WHILE MAS AINDA FALTA ESPACO
			$limite 		= 45;
			
			$pgTotal 		= ceil($rowsEstoque / $limite);
			$p = 1;

			while($rowEstoque = mysql_fetch_array($rsEstoque)){
				echo mysql_error();
                                $estoque_original = $rowEstoque['fldEstoqueQuantidade'];
				$estoque 	= ($estoque_original < 0) ? 0 : $estoque_original;
				$vCompra 	= $rowEstoque['fldValorCompra'];
				$vVenda 	= $rowEstoque['fldValorVenda'];
				
				$totalCompra 	= $vCompra * $estoque;
				$totalVenda 	= $vVenda  * $estoque;
				$totaLucro 		= $totalVenda - $totalCompra;
				$saldoLucro 	+= $totaLucro;
				$saldoCompra 	+= $totalCompra;
				$saldoVenda 	+= $totalVenda;
				
				$pagina[$n] .='
				<tr>
                	<td style="width:75px; text-align:right">'.str_pad($rowEstoque['fldCodigo'], 6, "0", STR_PAD_LEFT).'</td>
					<td style="width:260px">'.substr($rowEstoque['fldNome'],0, 38).'</td>
					<td style="width:70px; text-align:right">'.format_number_out($estoque_original).'</td>
					<td class="valor">'.format_number_out($vVenda).'</td>
					<td class="valor">'.format_number_out($totalVenda).'</td>
					<td class="valor">'.format_number_out($vCompra).'</td>
					<td class="valor">'.format_number_out($totalCompra).'</td>
					<td class="valor">'.format_number_out($totaLucro).'</td>
				</tr>';
				
				
				#SE CHEGAR A 20 LINHAS, MUDA DE 'BLOCO' E RECMECA CONTAGEM
				if($countRegistro == $limite){
					$countRegistro = 1;
					$n ++;
				}elseif($rowsEstoque == $x && $countRegistro < $limite){ #SE JA TERMINOU O WHILE DE REGISTROS MAS AINDA NAO ATINGIU 20 LINHAS, CONTINUAR CRIANDO LINHAS ATE O LIMITE
					while($countRegistro <= $limite){ $pagina[$n] .='<tr style="border:0; width:800px"></tr>'; $countRegistro++;}
				}else{
					$countRegistro ++;
				}
				$x ++;
			}
			
		#AGORA MANDO GERAR NA TELA PARA IMPRESSAO ############################################################################################################################################
		$x = 1;
		while($x <= $n){
			$tabelaCabecalho1 = ($x == 1)? '<table class="relatorio_print" style="page-break-before:avoid">'.$tabelaCabecalho : '<table class="relatorio_print">'.$tabelaCabecalho;
			#PRIMEIRO BLOCO (LANCADOS) ###################################################################################################################################################
				print $tabelaCabecalho1;
				
                print '<td style="width: 200px"><p class="pag">'.$p.' de '.$pgTotal.'</p></td>';
				print $tabelaCabecalho2;
				print $tabelaCabecalho3;
				echo  $pagina[$x];
?>
						</table>
					</td >
				</tr>
			</table>			
<?			$x ++;
			$p ++;
		}
?>        
		<table class="table_relatorio_rodape" summary="Relat&oacute;rio">
            <tr>
                <td style="width:220px">&nbsp;</td>
                <td style="width:100px">Total Venda</td>
                <td style="width:60px; text-align:right"><?=format_number_out($saldoVenda)?></td>
                <td style="width:10px; border-left:1px solid">&nbsp;</td>
                <td style="width:100px">Total Compra</td>
                <td style="width:60px; text-align:right"><?=format_number_out($saldoCompra)?></td>
                <td style="width:10px; border-left:1px solid">&nbsp;</td>
                <td style="width:100px">Total Lucro</td>
                <td style="width:60px; text-align:right"><?=format_number_out($saldoLucro)?></td>
            </tr>
        </table> 
