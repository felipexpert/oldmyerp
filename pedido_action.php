<?Php
	
	function varificaStatusArray($id_query_array, $statusRegistro){
		
		$id_selecionados = array();
		$id_selecionados = array_values($id_query_array); //'reseta' o indice da array
		$counterFinalizado = 0;
		$id_verificado = array();
		
		while($id_selecionados[$counterFinalizado]){
			
			$sqlStatusAtual = mysql_query("SELECT fldStatus FROM tblpedido WHERE fldId = $id_selecionados[$counterFinalizado] AND fldTipo_Id != 3");
			$rowStatusAtual = mysql_fetch_assoc($sqlStatusAtual);
			$statusAtual = $rowStatusAtual['fldStatus'];
			
			if($statusAtual != $statusRegistro){
				$id_verificado[$counterFinalizado] = $id_selecionados[$counterFinalizado];
			}
			$counterFinalizado+=1;
		}
		
		$id_verificado = array_values($id_verificado);
		return $id_verificado;
	}
	
	
	function verificaNFeParcela($ids) {
		
		$rsParcelasPagas = mysql_query("SELECT tblpedido.fldId, tblpedido_fiscal.fldnumero FROM tblpedido
										INNER JOIN tblpedido_fiscal ON tblpedido_fiscal.fldpedido_id = tblpedido.fldId 
										LEFT JOIN tblpedido_parcela ON tblpedido_parcela.fldPedido_Id = tblpedido.fldId 
										INNER JOIN tblpedido_parcela_baixa ON tblpedido_parcela_baixa.fldParcela_Id = tblpedido_parcela.fldId 
										WHERE tblpedido.fldId IN (" . join(",", $ids) . ") AND tblpedido_parcela_baixa.fldExcluido = 0");
		
		$x = 0;
		while($rowParcelasPagas = mysql_fetch_array($rsParcelasPagas)) {
			$pedido_id[$x]   = $rowParcelasPagas['fldId'];
			$pedidoNota[$x] = $rowParcelasPagas['fldnumero'];
			
			//remover da matriz com os ids de exclusão, as notas que já possuem algum pagamento
			$key = array_search($pedido_id[$x], $ids);
			unset($ids[$key]);
			
			$x++;
		}
		
		$msgSingular 	= 'NFe {nNota}, n&atilde;o foi exclu&iacute;da, pois já possui parcelas pagas!';
		$msgPlural   	= 'NFes {nNota}, n&atilde;o foram exclu&iacute;das, pois já possuem parcelas pagas!';
		//gerando mensagem de erro caso haja alguma NFe com parcela paga
		if(isset($pedido_id) and isset($pedidoNota)) {
			$nNota = implode(',', $pedidoNota);
			
			if(count($pedidoNota) > 1) $msg = $msgPlural;
			else $msg = $msgSingular;
			
			$_SESSION['msg_alert'] = array('open_markup'  => '<div class="alert">',
										   'close_markup' => '</div>',
										   'content'	  => '<p class="alert">'. str_replace('{nNota}', $nNota, $msg) .'</p>'
										  );
		}
		
		unset($rsParcelasPagas, $rowParcelasPagas, $parcelas, $key, $x, $msg, $pedido_id, $pedidoNota, $nNota);
		return (!empty($ids)) ? $ids : false;
		
	}
	
	function verifica_estoque_item($pedido_id){
	
		$rsPedido = mysql_query("SELECT * FROM tblpedido WHERE fldId in (" . join(",", $pedido_id) . ")");
		$erro	 = 'erro_false';
		while($rowPedido = mysql_fetch_array($rsPedido)){
			$x	 	= 0;
			$status_venda = $rowPedido['fldStatus'];
			
			$rsItem = mysql_query("SELECT tblpedido_item.fldQuantidade, tblpedido_item.fldEntregue, tblpedido_item.fldId as fldItem_Id, tblpedido_item.fldPedido_Id, tblpedido_item.fldEstoque_Id, 
			tblproduto.fldId, tblproduto.fldCodigo, tblproduto.fldEstoque_Controle
								  FROM tblpedido_item 
								  INNER JOIN tblproduto ON tblpedido_item.fldProduto_Id = tblproduto.fldId
								  WHERE tblpedido_item.fldPedido_Id =".$rowPedido['fldId']);
			while($rowItem = mysql_fetch_array($rsItem)){
				$produto_id			= $rowItem['fldProduto_Id'];
				$produto_codigo 	= $rowItem['fldCodigo'];
				$estoque_id 		= $rowItem['fldProduto_Id'];
				$pedido_id  		= $rowItem['fldPedido_Id'];
				$produto_id 		= $rowItem['fldId'];
				$item_quantidade	= $rowItem['fldQuantidade'];
				$estoque_id 		= $rowItem['fldEstoque_Id'];
				$estoque_controle	= $rowItem['fldEstoque_Controle'];
				$item_entregue		= $rowItem['fldEntregue'];
				$item_id			= $rowItem['fldItem_Id'];
				
				$rs = "SELECT 
					tblproduto.fldEstoque_Controle,
					SUM((tblproduto_estoque_movimento.fldEntrada) - (tblproduto_estoque_movimento.fldSaida)) as fldSaldo
					FROM tblproduto_estoque_movimento 
					LEFT JOIN tblproduto ON tblproduto.fldId = tblproduto_estoque_movimento.fldProduto_Id
					WHERE tblproduto_estoque_movimento.fldEstoque_Id = $estoque_id 
					AND tblproduto_estoque_movimento.fldProduto_Id = $produto_id
					GROUP BY tblproduto_estoque_movimento.fldProduto_Id";
				$rsSql 			 = mysql_query($rs);
				$rowEstoque		 = mysql_fetch_array($rsSql);
				$estoqueControle = $rowEstoque['fldEstoque_Controle'];
				$estoqueSaldo	 = $rowEstoque['fldSaldo'];
				echo mysql_error();
				
				#VERIFICA SE ESSE ITEM JA DESCONTOU DO ESTOQUE ANTES PRA NAO DESCONTAR DE NOVO ####################################################################
				$rs =  mysql_query("SELECT SUM(fldEntrada - fldSaida) as fldQuantidade FROM tblproduto_estoque_movimento WHERE fldItem_Id = $item_id AND fldEstoque_Id = $estoque_id GROUP BY fldEstoque_Id");
				echo mysql_error();
				if(mysql_num_rows($rs) == 0 ){
				
					if($item_quantidade > $estoqueSaldo && $estoqueControle == 1){
						$erro		= 'erro_true';
						$mensagem 	= "Venda $pedido_id : produto $produto_codigo não possui estoque suficiente!";
					}
					
					$estoque_lancar[$x] = array(
							'produto_id'		=> $produto_id,
							'saida'				=> $item_quantidade,
							'referencia_id'		=> $pedido_id,
							'estoque_id'		=> $estoque_id,
							'estoque_controle'	=> $estoque_controle,
							'item_entregue'		=> $item_entregue,
							'item_id' 			=> $item_id
					);
					$x++;
				}
			}
			if($erro == 'erro_true'){
?>
				<script language="javascript" charset="ISO-8859-1">	
                    alert('<?= $mensagem?>!');
                </script>
<?				return 'erro';
				break;
			}else{
				if($estoque_lancar){
					foreach($estoque_lancar as $lancar){
						if(($lancar['estoque_controle'] == 1 && $lancar['item_entregue'] == 0) || ($status_venda == 1 && $lancar['estoque_controle'] == 3)){
							fnc_estoque_movimento_lancar($lancar['produto_id'], '', '', $lancar['saida'], 1, $lancar['referencia_id'], $lancar['estoque_id'], $lancar['estoque_id'], '', $lancar['item_id']);					
						}
					}
				}
			}
			
		}
	}

 #############################################################################################################################################
	$id_array = unserialize(urldecode($_POST["hid_array"]));
	$n = 0;
	$id_query_array = array();
	while($id_array[$n]){
		if($_POST["chk_pedido_".$id_array[$n]]){
			$id_query_array[$n] = $id_array[$n];
			$validate = true;
			$filtro_id .= $id_array[$n] . ", ";
		}
		$n += 1;
	}
	$filtro_id = substr($filtro_id, 0 ,strlen($filtro_id) - 2);
	if(!$validate && $_POST["btn_action"] != 'imprimir' && $_POST['btn_action'] != 'confirmar'  && !isset($_POST['sel_action_status'])){
?>		<div class="alert">
			<p class="alert">voc&ecirc; deve selecionar algum registro antes de solicitar uma a&ccedil;&atilde;o</p>
		</div>
<?	}elseif (count($id_query_array) > 1 && $_POST["btn_action"] && $_POST["btn_action"] == 'alterar numero'){
?>		<div class="alert">
			<p class="alert">selecione apenas um registro</p>
		</div>
<?	}else{
		if(isset($_POST['sel_action_status'])){

			$data_atual = date("Y-m-d");
			$hora_atual = date("H:i:s");
			$usuario_id = $_SESSION['usuario_id'];
			
			switch($_POST["sel_action_status"]){
				
				case "aprovado":
?>					<script language="javascript" charset="ISO-8859-1">
						alert("É necessario marcar os produtos como 'Entregue' para que seja descontado estoque!");
					</script>
<?
					if(verifica_estoque_item($id_query_array) != 'erro'){
						$id_verificado = varificaStatusArray($id_query_array, 2);
						
						/*
						$id_selecionados 	= array();
						$id_selecionados 	= array_values($id_query_array); //'reseta' o indice da array
						$counterFinalizado	= 0;
						$id_verificado		= array();
						
						while($id_selecionados[$counterFinalizado]){
							
							$sqlStatusAtual = mysql_query("SELECT fldStatus FROM tblpedido WHERE fldId = $id_selecionados[$counterFinalizado]");
							$rowStatusAtual = mysql_fetch_assoc($sqlStatusAtual);
							$statusAtual = $rowStatusAtual['fldStatus'];
							
							if($statusAtual != 2){
							
								$id_verificado[$counterFinalizado] = $id_selecionados[$counterFinalizado];
							}
							
							$counterFinalizado+=1;
						}
						
						$id_verificado = array_values($id_verificado);
						*/
						$counterInserir = 0;
						while($id_verificado[$counterInserir]){
							#insere no historico de status
							mysql_query("INSERT INTO tblpedido_status_historico (fldPedido_Id, fldStatus_Id, fldData, fldHora, fldUsuario_Id) VALUES ('$id_verificado[$counterInserir]','2','$data_atual','$hora_atual','$usuario_id')");
							
							#faz update de acordo com status
							//atualiza as parcelas
							mysql_query("UPDATE tblpedido_parcela SET fldStatus = 1 WHERE fldPedido_Id = '$id_verificado[$counterInserir]' ");
							mysql_query("UPDATE tblpedido SET fldStatus = 2 WHERE fldId = '$id_verificado[$counterInserir]' ");
							
							$counterInserir+=1;
						}
					}
	
					
				break;	
	/*****************************************************************/
				case "finalizado":
					 if(verifica_estoque_item($id_query_array) != 'erro'){
						/*			
						$id_selecionados 	= array();
						$id_selecionados 	= array_values($id_query_array); //'reseta' o indice da array
						$counterFinalizado 	= 0;
						$id_verificado 		= array();
						
						while($id_selecionados[$counterFinalizado]){
							
							$sqlStatusAtual = mysql_query("SELECT fldStatus FROM tblpedido WHERE fldId = $id_selecionados[$counterFinalizado]");
							$rowStatusAtual = mysql_fetch_assoc($sqlStatusAtual);
							$statusAtual = $rowStatusAtual['fldStatus'];
							
							if($statusAtual != 3){
								$id_verificado[$counterFinalizado] = $id_selecionados[$counterFinalizado];
							}
							
							$counterFinalizado+=1;
						}
						
						$id_verificado 	= array_values($id_verificado);
						$data_atual 	= date("Y-m-d");
						$hora_atual 	= date("H:i:s");
						$usuario_id 	= $_SESSION['usuario_id'];
						*/
						$id_verificado = varificaStatusArray($id_query_array, 3);
						$counterInserir = 0;
						
						while($id_verificado[$counterInserir]){
							mysql_query("UPDATE tblpedido_parcela SET fldStatus = 1 WHERE fldPedido_Id = '$id_verificado[$counterInserir]'");
							mysql_query("UPDATE tblpedido SET fldStatus = 3 WHERE fldId = '$id_verificado[$counterInserir]'");
							
							mysql_query("INSERT INTO tblpedido_status_historico (fldPedido_Id, fldStatus_Id, fldData, fldHora, fldUsuario_Id) VALUES ('$id_verificado[$counterInserir]','3','$data_atual','$hora_atual','$usuario_id')");
							$counterInserir+=1;
						}
					 }
				break;
	/****************************************************************/
				case "entregue":
				//VERIFICA SE ESTAVA EM ORCAMENTO PARA PODER DAR BAIXA NO ESTOQUE
					if(verifica_estoque_item($id_query_array) != 'erro'){
						/*
						$id_selecionados = array();
						$id_selecionados = array_values($id_query_array); //'reseta' o indice da array
						$counterFinalizado = 0;
						$id_verificado = array();
						
						while($id_selecionados[$counterFinalizado]){
							
							$sqlStatusAtual = mysql_query("SELECT fldStatus FROM tblpedido WHERE fldId = $id_selecionados[$counterFinalizado]");
							$rowStatusAtual = mysql_fetch_assoc($sqlStatusAtual);
							$statusAtual = $rowStatusAtual['fldStatus'];
							
							if($statusAtual != 4){
							
								$id_verificado[$counterFinalizado] = $id_selecionados[$counterFinalizado];
							}
							
							$counterFinalizado+=1;
						}
						
						$id_verificado = array_values($id_verificado);
						
						$data_atual = date("Y-m-d");
						$hora_atual = date("H:i:s");
						$usuario_id = $_SESSION['usuario_id'];
						*/
						$id_verificado = varificaStatusArray($id_query_array, 4);
						$counterInserir = 0;

						while($id_verificado[$counterInserir]){
							
							mysql_query("UPDATE tblpedido_parcela 	SET fldStatus = 1 WHERE fldPedido_Id = '$id_verificado[$counterInserir]'");
							mysql_query("UPDATE tblpedido 			SET fldStatus = 4 WHERE fldId = '$id_verificado[$counterInserir]'");
							mysql_query("UPDATE tblpedido_item 		SET fldEntregue = 1, fldEntregueData = CURRENT_DATE() WHERE fldPedido_Id = '$id_verificado[$counterInserir]'");
							
							mysql_query("INSERT INTO tblpedido_status_historico (fldPedido_Id, fldStatus_Id, fldData, fldHora, fldUsuario_Id) VALUES ('$id_verificado[$counterInserir]','4','$data_atual','$hora_atual','$usuario_id')");
							$counterInserir+=1;
						}
					}
				break;
	/*****************************************************************/
			}
		}else{//se nao for alteracao de status
				
			switch($_POST["btn_action"]){
	/*****************************************************************/
				case "excluir":
					//VERIFICA SE O PEDIDO NAO ESTAVA EM ORCAMENTO PRA DEVOLVER OS ITENS NO ESTOQUE
					$rsPedido = mysql_query("SELECT * FROM tblpedido WHERE fldId in (" . join(",", $id_query_array) . ")");
					while($rowPedido = mysql_fetch_array($rsPedido)){
						if($rowPedido['fldStatus'] > 1){
							//devolver o produto para estoque para pedidos que forem excluidos
							$rsItem = mysql_query("SELECT tblpedido_item.fldQuantidade, tblpedido_item.fldPedido_Id, 
												  tblpedido_item.fldId as fldItem_Id, tblpedido_item.fldEstoque_Id, tblproduto.fldId
												  FROM tblpedido_item 
												  INNER JOIN tblproduto ON tblpedido_item.fldProduto_Id = tblproduto.fldId
												  WHERE tblpedido_item.fldPedido_Id =".$rowPedido['fldId']);
							
							//número da nota para inserir junto com a movimentação do estoque
							$nNota 	= mysql_fetch_array(mysql_query("SELECT fldnumero, fldEstoque FROM tblpedido_fiscal INNER JOIN tblnfe_natureza_operacao 
																	 ON tblpedido_fiscal.fldnatureza_operacao_id = tblnfe_natureza_operacao.fldId
																	 WHERE tblpedido_fiscal.fldDocumento_Tipo = 1 AND fldpedido_id = " . $rowPedido['fldId']));
							$nNFe 	= $nNota['fldnumero'];
							echo mysql_error();
							while($rowItem = mysql_fetch_array($rsItem)){
									
								$item_id 	= $rowItem['fldItem_Id'];
								$pedido_id 	= $rowItem['fldPedido_Id'];
								$quantidade	= $rowItem['fldQuantidade'];
								$produto_id = $rowItem['fldId'];
								$estoque_id = $rowItem['fldEstoque_Id'];
	
								if($nNFe > 0 && $nNota['fldEstoque'] == '1'){
									//$tipo_id = 9 //'nfe excluída'
									fnc_estoque_movimento_lancar($produto_id, '', $quantidade, '', 9, $nNFe, $estoque_id, $estoque_id, '', $item_id);
								}else{
									fnc_estoque_movimento_lancar($produto_id, '', $quantidade, '', 2, $pedido_id, $estoque_id, $estoque_id, '', $item_id);
								}
								
							}
						}
					}
					
					#CASO NFe IMPORTADA ##############################################################################################################
					//rotina para pegar todos os ids, da nova NFe e das vendas de origens para poder "excluir"
					$rsPedidoOrigem = mysql_query("SELECT fldId FROM tblpedido WHERE fldPedido_Destino_Nfe_Id IN (" . join(",", $id_query_array) . ")");
					$x = 0;
					while($rowPedidoOrigem = mysql_fetch_array($rsPedidoOrigem)) {
						$pedidoOrigem[$x] = $rowPedidoOrigem['fldId'];
						$x++;
					}
					
					//verificação para evitar erros com NFes 'diretas' (não possuem origens)
					if(count($pedidoOrigem) > 0) $id_pedidos = implode(',', array_merge($pedidoOrigem, $id_query_array));
					else $id_pedidos = implode(',', $id_query_array);
					##################################################################################################################################
					
					mysql_query("UPDATE tblpedido 			SET fldExcluido = '1' WHERE fldId 		 IN (" . join(",", $id_query_array) . ")");
					mysql_query("UPDATE tblpedido_item 	  	SET fldExcluido = '1' WHERE fldPedido_Id IN (" . join(",", $id_query_array) . ")");
					mysql_query("UPDATE tblpedido_parcela 	SET fldExcluido = '1' WHERE fldPedido_Id IN (" . join(",", $id_query_array) . ")");
					
				break;
					
				
	/***************************************************************/
				case "imprimir":
					
					$desc = $_POST['sel_pedido_relatorio'];
					
	?>				<script language="javascript">
						window.open("pedido_relatorio.php?desc=<?=$desc?>");
					</script>
	<?			break;
	
	/***************************************************************/
				case "desfazer importação":
					$id_query_array = verificaNFeParcela($id_query_array);
					if($id_query_array != false) {
						
						//rotina para pegar todos os ids, da nova nota e das vendas de origens para poder "excluir"
						$rsPedidoOrigem = mysql_query("SELECT fldId, fldPedido_Destino_Nfe_Id FROM tblpedido WHERE fldPedido_Destino_Nfe_Id IN (" . join(",", $id_query_array) . ")");
						
						#ARMAZENA A ORIGEM E O DESTINO(NFE)
						$x = 0;
						while($rowPedidoOrigem = mysql_fetch_array($rsPedidoOrigem)) {
							$pedidoOrigem[$x]  = $rowPedidoOrigem['fldId'];
							$pedidoDestino[$x] = $rowPedidoOrigem['fldPedido_Destino_Nfe_Id'];
							$x++;
						}
						
						if(is_array($pedidoDestino)) {
							$NFeSemOrigem = array_diff($id_query_array, $pedidoDestino);
						}
						
						//exibir erro caso o registro selecionado não possua origem para ser desfeito
						if(!isset($pedidoDestino) || count($NFeSemOrigem) > 0) {
							$rsNFeSemOrigemNumero = mysql_query("SELECT fldnumero FROM tblpedido_fiscal WHERE fldpedido_id IN (" . join(",", $NFeSemOrigem) . ")");
							$x = 0;
							while($rowNFeSemOrigemNumero = mysql_fetch_array($rsNFeSemOrigemNumero)) {
								$NFeNumero[$x]  = $rowNFeSemOrigemNumero['fldnumero'];
								$x++;
							}
							
							//exibir mensagem no singular ou plural
							if(count($NFeNumero) > 1) $msg = 'NFes ' . implode(',', $NFeNumero) . ' não foram desfeitas, pois não foram geradas a partir de vendas comuns!';
							else $msg = 'NFe ' . implode(',', $NFeNumero) . ' não foi desfeita, pois não foi gerada a partir de vendas comuns!';
							
							$_SESSION['msg_alert'] = array('open_markup'  => '<div class="alert">',
														   'close_markup' => '</div>',
														   'content'	  => '<p class="alert">'. $msg .'</p>'
														  );
						}
						
						#APENAS SETA COMO NULO, PARA NAO HAVER MAIS DESTINO DE NOTA
						if(mysql_query("UPDATE tblpedido SET fldPedido_Destino_Nfe_Id = NULL WHERE fldId IN (". implode(',', $pedidoOrigem) .")")) {
							
							//para deletar apenas as NFes, não as origens das mesmas
							$ids_nota = implode(',', $pedidoDestino);
							
							mysql_query("UPDATE tblpedido 			SET fldExcluido = '1' WHERE fldId in ($ids_nota)");
							mysql_query("UPDATE tblpedido_item 		SET fldExcluido = '1' WHERE fldPedido_Id in ($ids_nota)");
							mysql_query("UPDATE tblpedido_parcela 	SET fldExcluido = '1' WHERE fldPedido_Id in ($ids_nota)");
						}
						unset($rsPedidoOrigem, $x, $i, $ids_nota, $indice, $rowPedidoOrigem, $pedidoOrigem, $produto_id, $entrada, $saida, $pedido_id, $estoque_id, $ItemOrigem, $ItemAtual, $arrayControle, $sqlItensAtuais, $sqlItensOrigens, $msgSingular, $msgPlural, $msg);
					}
					
					header("location:index.php?p=pedido");
					
				break;
	/***************************************************************/
				case "download": 
	?>				<a id="modal_download" href="pedido_nfe_download,<?=join(";", $id_query_array)?>,1" class="modal" rel="250-180"></a>
					<script language="javascript">
						winModal($('#modal_download'));
					</script>
	<?			break;
				
	/***************************************************************/
				case "alterar numero": 
	?>
					<script type="text/javascript">
						$(document).ready(function() {
							$('#alterar_numero_verificado').trigger('click'); 
						});
					</script>
	<?						
				break;
	/***************************************************************/
				#CONFIRMAR ALTERAR NUMERO
				case "confirmar":
						
					$numero_atual 			= $_POST['hid_nfe_numero_atual'];
					$motivo_alteracao		= $_POST['sel_alterar_motivo'];
					$pedido_id				= $_POST['hid_pedido_id'];
					$observacao_alteracao	= $_POST['txt_alterar_observacao'];
					$data_atual				= date("Y-m-d");
					$hora_atual 			= date("H:i:s");
					$usuario_id 			= $_SESSION['usuario_id'];
					
					if(!is_null($_POST['rad_nnfe_n_perdido'])) { $numeroAusenteNFe = $_POST['rad_nnfe_n_perdido']; }
					$nfe_numero				= (isset($numeroAusenteNFe)) ? $numeroAusenteNFe : fnc_sistema('nfe_numero_nota');
					if($numeroAusenteNFe == fnc_sistema('nfe_numero_nota')){$numeroAusenteNFe = null;}
					
					
					$queryInutiliza = mysql_query("INSERT INTO tblpedido_nfe_numero_inutilizado (fldNFE_Numero, fldMotivo, fldObservacao, fldData, fldHora, fldUsuario_Id) VALUES ('$numero_atual', '$motivo_alteracao', '$observacao_alteracao', '$data_atual', '$hora_atual', '$usuario_id')");
					$queryUpdate 	= mysql_query("UPDATE tblpedido_fiscal SET fldnumero = '$nfe_numero' WHERE fldpedido_id = '$pedido_id' AND fldDocumento_Tipo = 1");
					
					if (!$queryInutiliza){ 
	?>
						<div class="alert">
							<p class="alert">erro ao inutilizar o numero atual</p>
						</div>
	<?
					}elseif (!$queryUpdate){ 
	?>
						<div class="alert">
							<p class="alert">erro ao salvar registros no banco</p>
						</div>
								
	<?				}else{
						if(!isset($numeroAusenteNFe)){
							$nfe_numero += 1;
							fnc_sistema_update('nfe_numero_nota', $nfe_numero);
						}
	?>
						<div class="alert">
							<p class="ok">registro salvo com sucesso</p>
						</div>
	<?				}
				break;
				
			}
		}
	}
		

?>
