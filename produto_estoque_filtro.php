<?php

	$_SESSION['txt_produto_estoque_filtro_data1'] 	= (isset($_SESSION['txt_produto_estoque_filtro_data1']))	? $_SESSION['txt_produto_estoque_filtro_data1'] :'';
	$_SESSION['txt_produto_estoque_filtro_data2'] 	= (isset($_SESSION['txt_produto_estoque_filtro_data2']))	? $_SESSION['txt_produto_estoque_filtro_data2'] : date('d/m/Y');	
	
	//recebendo a data do calendário de período
	if(isset($_POST['txt_calendario_data_inicial']) && isset($_POST['txt_calendario_data_final'])){
	    $_SESSION['txt_produto_estoque_filtro_data1'] = (isset($_POST['txt_calendario_data_inicial'])) 	? $_POST['txt_calendario_data_inicial'] : '';
	    $_SESSION['txt_produto_estoque_filtro_data2'] = (isset($_POST['txt_calendario_data_final']))	? $_POST['txt_calendario_data_final'] 	: '';
	}
	
	//memorizar os filtros para exibição nos selects
	if(isset($_POST['btn_limpar'])){
		$_SESSION['sel_produto_estoque_filtro_tipo_data'] 		= "movimento";
		$_SESSION['txt_produto_estoque_filtro_data1'] 			=  '';
		$_SESSION['txt_produto_estoque_filtro_data2'] 			=  date('d/m/Y');
		$_SESSION['sel_produto_estoque_filtro_tipo_movimento'] 	= "";
	}else{
		$_SESSION['sel_produto_estoque_filtro_tipo_data'] 		= (isset($_POST['sel_produto_estoque_filtro_tipo_data'])	 	? $_POST['sel_produto_estoque_filtro_tipo_data'] 		: $_SESSION['sel_produto_estoque_filtro_tipo_data']);
		$_SESSION['txt_produto_estoque_filtro_data1'] 			= (isset($_POST['txt_produto_estoque_filtro_data1'])			? $_POST['txt_produto_estoque_filtro_data1']			: $_SESSION['txt_produto_estoque_filtro_data1']);
		$_SESSION['txt_produto_estoque_filtro_data2'] 			= (isset($_POST['txt_produto_estoque_filtro_data2'])			? $_POST['txt_produto_estoque_filtro_data2']			: $_SESSION['txt_produto_estoque_filtro_data2']);
		$_SESSION['sel_produto_estoque_filtro_tipo_movimento']	= (isset($_POST['sel_produto_estoque_filtro_tipo_movimento'])	? $_POST['sel_produto_estoque_filtro_tipo_movimento']	: $_SESSION['sel_produto_estoque_filtro_tipo_movimento']);
	}

?>
<form id="frm-filtro" method="post">
	<fieldset>
  	<legend>Buscar por:</legend>
    <ul>
        <li>
            <select id="sel_produto_estoque_filtro_tipo_data" name="sel_produto_estoque_filtro_tipo_data" style="width: 120px" >
                <option <?=($_SESSION['sel_produto_estoque_filtro_tipo_data'] == "movimento") ? 'selected="selected"' : '' ?> value="movimento">movimenta&ccedil;&atilde;o</option>
                <option <?=($_SESSION['sel_produto_estoque_filtro_tipo_data'] == "validade") 	? 'selected="selected"' : '' ?> value="validade" >validade</option>
			</select>
		</li>
        <li>
<?			$data1 = ($_SESSION['txt_produto_estoque_filtro_data1'] ? $_SESSION['txt_produto_estoque_filtro_data1'] : "");
?>     		<input title="Data inicial" style="text-align:center;width: 70px" type="text" name="txt_produto_estoque_filtro_data1" id="txt_produto_estoque_filtro_data1" class="calendario-mask" value="<?=$data1?>"/>
      	</li>
        <li>
<?			$data2 = ($_SESSION['txt_produto_estoque_filtro_data2'] ? $_SESSION['txt_produto_estoque_filtro_data2'] : "");
?>     		<input title="Data final" style="text-align:center;width: 70px" type="text" name="txt_produto_estoque_filtro_data2" id="txt_produto_estoque_filtro_data2" class="calendario-mask" value="<?=$data2?>"/>
			<a href="calendario_periodo,<?=format_date_in($data1) . ',' . format_date_in($data2)?>,produto_detalhe&id=<?=$produto_id?>&modo=estoque" id="exibir-calendario" title="Exibir calend&aacute;rio" class="modal calendario-modal" rel="600-320"></a>
      	</li>
        <li>
            <select id="sel_produto_estoque_filtro_tipo_movimento" name="sel_produto_estoque_filtro_tipo_movimento" style="width: 120px" >
                <option <?=($_SESSION['sel_produto_estoque_filtro_tipo_movimento'] == "todos") 	? 'selected="selected"' : '' ?> value="todos"	>todos registros</option>
                <option <?=($_SESSION['sel_produto_estoque_filtro_tipo_movimento'] == "entrada")? 'selected="selected"' : '' ?> value="entrada"	>entradas</option>
                <option <?=($_SESSION['sel_produto_estoque_filtro_tipo_movimento'] == "saida") 	? 'selected="selected"' : '' ?> value="saida"	>sa&iacute;das</option>
			</select>
		</li>
        
        <li style="float:right">
	        <button type="submit" name="btn_exibir" style="margin:0" title="Exibir">Exibir</button>
        </li>
        <li style="float:right">
        	<button type="submit" name="btn_limpar" style="margin:0" title="Limpar Filtro">Limpar filtro</button>
        </li>
    </ul>
  </fieldset>
</form>

<? 

	if(format_date_in($_SESSION['txt_produto_estoque_filtro_data1']) != "" || format_date_in($_SESSION['txt_produto_estoque_filtro_data2']) != ""){
		if(($_SESSION['sel_produto_estoque_filtro_tipo_data']) == "validade"){
			if($_SESSION['txt_produto_estoque_filtro_data1'] != "" && $_SESSION['txt_produto_estoque_filtro_data2'] != ""){
				$filtro .= " AND tblproduto_estoque_movimento.fldValidade_Data BETWEEN '".format_date_in($_SESSION['txt_produto_estoque_filtro_data1'])."' AND '".format_date_in($_SESSION['txt_produto_estoque_filtro_data2'])."'" ;
			}elseif($_SESSION['txt_produto_estoque_filtro_data1'] != "" && $_SESSION['txt_produto_estoque_filtro_data2'] == ""){
				$filtro .= " AND tblproduto_estoque_movimento.fldValidade_Data >= '".format_date_in($_SESSION['txt_produto_estoque_filtro_data1'])."'";
			}elseif($_SESSION['txt_produto_estoque_filtro_data2'] != "" && $_SESSION['txt_produto_estoque_filtro_data1'] == ""){
				$filtro .= " AND tblproduto_estoque_movimento.fldValidade_Data <= '".format_date_in($_SESSION['txt_produto_estoque_filtro_data2'])."'";
			}
		}else{
			if($_SESSION['txt_produto_estoque_filtro_data1'] != "" && $_SESSION['txt_produto_estoque_filtro_data2'] != ""){
				$filtro .= " AND tblproduto_estoque_movimento.fldData BETWEEN '".format_date_in($_SESSION['txt_produto_estoque_filtro_data1'])."' AND '".format_date_in($_SESSION['txt_produto_estoque_filtro_data2'])."'" ;
			}elseif($_SESSION['txt_produto_estoque_filtro_data1'] != "" && $_SESSION['txt_produto_estoque_filtro_data2'] == ""){
				$filtro .= " AND tblproduto_estoque_movimento.fldData >= '".format_date_in($_SESSION['txt_produto_estoque_filtro_data1'])."'";
			}elseif($_SESSION['txt_produto_estoque_filtro_data2'] != "" && $_SESSION['txt_produto_estoque_filtro_data1'] == ""){
				$filtro .= " AND tblproduto_estoque_movimento.fldData <= '".format_date_in($_SESSION['txt_produto_estoque_filtro_data2'])."'";
			}
		}		
	}
	if(($_SESSION['sel_produto_estoque_filtro_tipo_movimento']) == "entrada"){
		$filtro .= " AND tblproduto_estoque_movimento.fldEntrada > 0 AND tblproduto_estoque_movimento.fldSaida = 0";
	}elseif(($_SESSION['sel_produto_estoque_filtro_tipo_movimento']) == "saida"){
		$filtro .= " AND tblproduto_estoque_movimento.fldSaida > 0 AND tblproduto_estoque_movimento.fldEntrada = 0";
	}
	
	//transferir para a sessão
	$_SESSION['filtro_produto_estoque'] = $filtro;

?>