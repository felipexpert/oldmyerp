<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
      	<title>myERP - Relat&oacute;rio de Clientes</title>
        <link rel="stylesheet" type="text/css" href="style/style_relatorio.css" />
        <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />

	</head>
	<body>
      
        <div id="no-print">
            <a class="print" href="#" onClick="window.print()">imprimir</a>
        </div>

<? 
		ob_start();
		session_start();
		
		require("inc/con_db.php");
		require("inc/fnc_general.php");
		require("inc/fnc_ibge.php");
		
		$rsDados 		= mysql_query("select * from tblempresa_info");
		$rowDados 		= mysql_fetch_array($rsDados);
		$CPF_CNPJDados 	= formatCPFCNPJTipo_out($rowDados['fldCPF_CNPJ'], $rowDados['fldTipo']);
		$rsUsuario  	= mysql_query("select * from tblusuario where fldId=".$_SESSION['usuario_id']);
		$rowUsuario 	= mysql_fetch_array($rsUsuario);

        #ABAIXO CRIO O CABECALHO #########################################################################################################################################################
        $tabelaCabecalho =' 
            <tr style="border-bottom: 2px solid">
                <td style="width: 600px"><h1>Relat&oacute;rio de Clientes</h1></td>';
        $tabelaCabecalho2 =' 
            </tr>
            <tr>
                <td>
                    <table style="width: 580px" class="table_relatorio_dados" summary="Relat&oacute;rio">
                        <tr>
                            <td style="width: 320px;">Raz&atilde;o Social: '.$rowDados['fldNome'].'</td>
                            <td style="width: 200px;">Nome Fantasia: '.$rowDados['fldNome_Fantasia'].'</td>
                            <td style="width: 320px;">CPF/CNPJ: '.$CPF_CNPJDados.'</td>
                            <td style="width: 200px;">Telefone: '.$rowDados['fldTelefone1'].'</td>
                        </tr>
                    </table>    
                </td>
                <td>        
                    <table class="dados_impressao">
                        <tr>
                            <td><b>Data:            </b><span>'.format_date_out(date("Y-m-d")).'</span></td>
                            <td><b>Hora:            </b><span>'.format_time_short(date("H:i:s")).'</span></td>
                            <td><b>Usu&aacute;rio:  </b><span>'.$rowUsuario['fldUsuario'].'</span></td>
                        </tr>
                    </table>
                </td>
            </tr>';
            $tabelaCabecalho3 ='
            <tr>
                <td>
                    <table class="table_relatorio" summary="Relat&oacute;rio">';
        #######################################################################################################################################################################################
		
		$rsCliente 		 = mysql_query($_SESSION['cliente_relatorio']);
		$rowsCliente     = mysql_num_rows($rsCliente);
		echo mysql_error();

        $n              = 1; #DEFINE O NUMERO DA DO BLOCO
        $countRegistro  = 1; #DEFINE CONTAGEM DE ITENS NO WHILE PARA QUEBRA DE BLOCO AO ATINGIR LIMITE DE 20
        $x              = 1; #DEFINE CONTAGEM DE ITENS TOTAIS, PRA SABER SE JA TERMINOU WHILE MAS AINDA FALTA ESPACO
        $limite         = 6;
        
        $pgTotal        = ceil($rowsCliente / $limite);
        $p = 1;


        while($rowCliente = mysql_fetch_array($rsCliente)){
            echo mysql_error();

            $Municipio  = fnc_ibge_municipio($rowCliente['fldMunicipio_Codigo']);
            $Sigla      = fnc_ibge_uf_sigla($rowCliente['fldMunicipio_Codigo']);
            $CPF_CNPJ   = formatCPFCNPJTipo_out($rowCliente['fldCPF_CNPJ'], $rowCliente['fldTipo']);
            
            if($rowCliente['fldFuncionario_Id']){
                $rsFuncionario  = mysql_query("SELECT * FROM tblfuncionario WHERE fldId =".$rowCliente['fldFuncionario_Id']);
                $rowFuncionario = mysql_fetch_array($rsFuncionario);}
            
            $rsVenda = mysql_query("SELECT tblpedido.fldPedidoData, SUM(tblpedido_parcela.fldValor * (tblpedido_parcela.fldExcluido * -1 + 1)) as fldValorPedido FROM tblpedido_parcela INNER JOIN tblpedido ON tblpedido.fldId = tblpedido_parcela.fldPedido_Id
                                    WHERE tblpedido.fldCliente_Id = ". $rowCliente['fldId'] . " ORDER BY tblpedido.fldPedidoData desc limit 1");
            $rowVenda = mysql_fetch_array($rsVenda);

            $pagina[$n] .='
            <tr class="relatorio_dados">
                <td><b>Nome:            </b>'.$rowCliente["fldNome"].'</td>
                <td><b>C&oacute;digo:   </b>'.$rowCliente["fldCodigo"].'</td>
                <td><b>Nome Fantasia:   </b>'.$rowCliente["fldNomeFantasia"].'</td>
                <td><b>Cadastrado em:   </b>'.format_date_out($rowCliente["fldCadastroData"]).'</td>
                <td><b>Endere&ccedil;o: </b>'.$rowCliente["fldEndereco"].', '.$rowCliente["fldNumero"].'</td>
                <td><b>Bairro:          </b>'.$rowCliente["fldBairro"].'</td>
                <td><b>CEP:             </b>'.$rowCliente["fldCEP"].'</td>
                <td><b>Cidade:          </b>'.$Municipio.'</td>
                <td><b>UF:              </b>'.$Sigla.'</td>
                <td><b>Telefone 1:      </b>'.$rowCliente["fldTelefone1"].'</td>
                <td><b>Telefone 2:      </b>'.$rowCliente["fldTelefone2"].'</td>
                <td><b>Fax:             </b>'.$rowCliente["fldFax"].'</td>
                <td><b>E-mail:          </b>'.$rowCliente["fldEmail"].'</td>
                <td><b>Site:            </b>'.$rowCliente["fldWebsite"].'</td>
                <td><b>CPF/CNPJ:        </b>'.$CPF_CNPJ.'</td>
                <td><b>RG/IE:           </b>'.$rowCliente["fldRG_IE"].'</td>
                <td><b>Funcion&aacute;rio:   </b>'.$rowFuncionario["fldNome"].'</td>
                <td><b>&Uacute;ltima compra: </b>'.format_date_out($rowVenda["fldPedidoData"]).'</td>
                <td><b>Valor:           </b>'.format_number_out($rowVenda["fldValorPedido"]).'</td>
            </tr>';
            
            #SE CHEGAR A 20 LINHAS, MUDA DE 'BLOCO' E RECMECA CONTAGEM
            if($countRegistro == $limite){
                $countRegistro = 1;
                $n ++;
            }elseif($rowsCliente == $x && $countRegistro < $limite){ #SE JA TERMINOU O WHILE DE REGISTROS MAS AINDA NAO ATINGIU 20 LINHAS, CONTINUAR CRIANDO LINHAS ATE O LIMITE
                while($countRegistro <= $limite){ $pagina[$n] .='<tr style="border:0; width:800px"></tr>'; $countRegistro++;}
            }else{
                $countRegistro ++;
            }
            $x ++;
        }

        #AGORA MANDO GERAR NA TELA PARA IMPRESSAO ############################################################################################################################################
        $x = 1;
        while($x <= $n){
            $tabelaCabecalho1 = ($x == 1)? '<table class="relatorio_print" style="page-break-before:avoid">'.$tabelaCabecalho : '<table class="relatorio_print">'.$tabelaCabecalho;
            #PRIMEIRO BLOCO (LANCADOS) ###################################################################################################################################################
                print $tabelaCabecalho1;
                
                print '<td style="width: 200px"><p class="pag">'.$p.' de '.$pgTotal.'</p></td>';
                print $tabelaCabecalho2;
                print $tabelaCabecalho3;
                echo  $pagina[$x];
?>
                        </table>
                    </td >
                </tr>
            </table>            
<?          $x ++;
            $p ++;
        }
?>
	</body>
</html>