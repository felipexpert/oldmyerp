<div id="principal">
<?
	$cliente_id				   		= mysql_real_escape_string($_GET['id']);
	$selecionarDadosAdicionais 		= mysql_query("SELECT * FROM tblcliente_dados_adicionais WHERE fldCliente_Id = '".$cliente_id."'");
	$contarRegistrosEncontrados 	= mysql_num_rows($selecionarDadosAdicionais);
	$selecionarDadosAdicionais	 	= mysql_fetch_array($selecionarDadosAdicionais);
	
	if(!$contarRegistrosEncontrados){
		$inserirDadosAdicionais 	 = mysql_query("INSERT INTO tblcliente_dados_adicionais (fldCliente_Id) VALUES ('".$cliente_id."')");
	}
	
	if(isset($_POST["btn_enviar"])){
		
		function substituir($inicio, $final, $variavel){
			return str_replace($inicio, $final, $variavel);
		}
		
		//Dados enviados pelo formulário:
		$DadosResponsavel = array(
			'fldResponsavel'       				=> $_POST['txt_responsavel'],
			'fldResponsavel_CPF'				=> $_POST['txt_responsavel_cpf'],
			'fldResponsavel_Telefone1'  		=> $_POST['txt_responsavel_telefone1'],
			'fldResponsavel_Telefone2'     		=> $_POST['txt_responsavel_telefone2'],
			'fldResponsavel_Endereco'			=> $_POST['txt_responsavel_endereco'],
			'fldResponsavel_Numero'				=> $_POST['txt_responsavel_numero'],
			'fldResponsavel_Municipio_Codigo'	=> $_POST['txt_responsavel_municipio_codigo']
		);
		
		$DadosProfissionais = array(
			'fldTrabalho_Empresa'       => $_POST['txt_local'],
			'fldTrabalho_Endereco'		=> $_POST['txt_endereco'],
			'fldTrabalho_Telefone1'     => $_POST['txt_telefone1'],
			'fldTrabalho_Telefone2'     => $_POST['txt_telefone2'],
			'fldTrabalho_Admissao'		=> format_date_in($_POST['txt_data_admissao']),
			'fldTrabalho_Cargo'			=> $_POST['txt_cargo'],
			'fldTrabalho_Salario'		=> format_number_in($_POST['txt_salario']),
			'fldTrabalho_Tempo'			=> $_POST['txt_tempo_trabalho']
		);
		
		$DadosConjuge = array(
			'fldConjuge_Nome'			=> $_POST['txt_conjuge_nome'],
			'fldConjuge_Trabalho'		=> $_POST['txt_conjuge_local_trabalho'],
			'fldConjuge_Salario'		=> format_number_in($_POST['txt_conjuge_salario']),
			'fldConjuge_Profissao'		=> $_POST['txt_conjuge_profissao'],
			'fldConjuge_Trabalho_Tempo'	=> $_POST['txt_conjuge_tempo_trabalho'],
		);
		
		$DadosExtras = array(
			'fldRenda_Familiar'			=> format_number_in($_POST['txt_renda_familiar']),
			'fldPrestacao_Valor'		=> format_number_in($_POST['txt_valor_prestacao'])
		);
		
		$DadosReferenciasComerciais = array(
			'fldReferencia_Empresa1'			=> $_POST['txt_empresa1'],
			'fldReferencia_Empresa1_Telefone1' 	=> $_POST['txt_empresa1_telefone1'],
			'fldReferencia_Empresa1_Telefone2' 	=> $_POST['txt_empresa1_telefone2'],
			'fldReferencia_Empresa2'			=> $_POST['txt_empresa2'],
			'fldReferencia_Empresa2_Telefone1' 	=> $_POST['txt_empresa2_telefone1'],
			'fldReferencia_Empresa2_Telefone2' 	=> $_POST['txt_empresa2_telefone2'],
			'fldReferencia_Empresa3'			=> $_POST['txt_empresa3'],
			'fldReferencia_Empresa3_Telefone1' 	=> $_POST['txt_empresa3_telefone1'],
			'fldReferencia_Empresa3_Telefone2' 	=> $_POST['txt_empresa3_telefone2'],
		);
		
		$DadosReferenciasPessoais = array(
			'fldReferencia_Pessoal1'			=>	$_POST['txt_referencia_pessoal1'],
			'fldReferencia_Pessoal1_Telefone1' 	=>	$_POST['txt_referencia_pessoal1_telefone1'],
			'fldReferencia_Pessoal1_Telefone2'	=>	$_POST['txt_referencia_pessoal1_telefone2'],
			'fldReferencia_Pessoal1_Parentesco'	=>	$_POST['txt_referencia_pessoal1_parentesco'],
			'fldReferencia_Pessoal2'			=>	$_POST['txt_referencia_pessoal2'],
			'fldReferencia_Pessoal2_Telefone1' 	=>	$_POST['txt_referencia_pessoal2_telefone1'],
			'fldReferencia_Pessoal2_Telefone2'	=>	$_POST['txt_referencia_pessoal2_telefone2'],
			'fldReferencia_Pessoal2_Parentesco'	=>	$_POST['txt_referencia_pessoal2_Parentesco'],
			'fldReferencia_Pessoal3'			=>	$_POST['txt_referencia_pessoal3'],
			'fldReferencia_Pessoal3_Telefone1' 	=>	$_POST['txt_referencia_pessoal3_telefone1'],
			'fldReferencia_Pessoal3_Telefone2'	=>	$_POST['txt_referencia_pessoal3_telefone2'],
			'fldReferencia_Pessoal3_Parentesco'	=>	$_POST['txt_referencia_pessoal3_Parentesco'],
		);
		
		//Final dos dados enviados pelo Formulário.
		
		//Início das atualizações na tabela do banco de dados.
		
		$atualizarDadosResponsavel			=		mysql_query("
		UPDATE tblcliente_dados_adicionais SET
		fldResponsavel					 	= '".$DadosResponsavel['fldResponsavel']."',
		fldResponsavel_CPF					= '".$DadosResponsavel['fldResponsavel_CPF']."',
		fldResponsavel_Telefone1 			= '".$DadosResponsavel['fldResponsavel_Telefone1']."',
		fldResponsavel_Telefone2 			= '".$DadosResponsavel['fldResponsavel_Telefone2']."',
		fldResponsavel_Endereco 			= '".$DadosResponsavel['fldResponsavel_Endereco']."',
		fldResponsavel_Numero 				= '".$DadosResponsavel['fldResponsavel_Numero']."',
		fldResponsavel_Municipio_Codigo 	= '".$DadosResponsavel['fldResponsavel_Municipio_Codigo']."'
		WHERE fldCliente_Id 	= '".$cliente_id."'
		") or die (mysql_error());
		
		
		$atualizarDadosProfissionais		=		mysql_query("
		UPDATE tblcliente_dados_adicionais SET
		fldTrabalho_Empresa 	= '".$DadosProfissionais['fldTrabalho_Empresa']."',
		fldTrabalho_Endereco 	= '".$DadosProfissionais['fldTrabalho_Endereco']."',
		fldTrabalho_Telefone1 	= '".$DadosProfissionais['fldTrabalho_Telefone1']."',
		fldTrabalho_Telefone2 	= '".$DadosProfissionais['fldTrabalho_Telefone2']."',
		fldTrabalho_Admissao 	= '".$DadosProfissionais['fldTrabalho_Admissao']."',
		fldTrabalho_Cargo 		= '".$DadosProfissionais['fldTrabalho_Cargo']."',
		fldTrabalho_Salario 	= '".$DadosProfissionais['fldTrabalho_Salario']."',
		fldTrabalho_Tempo 		= '".$DadosProfissionais['fldTrabalho_Tempo']."'
		WHERE fldCliente_Id 	= '".$cliente_id."'
		") or die (mysql_error());
		
		$atualizarDadosConjuge				=		mysql_query("
		UPDATE tblcliente_dados_adicionais SET
		fldConjuge_Nome				= '".$DadosConjuge['fldConjuge_Nome']."',
		fldConjuge_Trabalho 		= '".$DadosConjuge['fldConjuge_Trabalho']."',
		fldConjuge_Salario 			= '".$DadosConjuge['fldConjuge_Salario']."',
		fldConjuge_Profissao 		= '".$DadosConjuge['fldConjuge_Profissao']."',
		fldConjuge_Trabalho_Tempo 	= '".$DadosConjuge['fldConjuge_Trabalho_Tempo']."'
		WHERE fldCliente_Id = '".$cliente_id."'
		") or die (mysql_error());
		
		$atualizarDadosExtras				=		mysql_query("
		UPDATE tblcliente_dados_adicionais SET
		fldRenda_Familiar 	= '".$DadosExtras['fldRenda_Familiar']."',
		fldPrestacao_Valor 	= '".$DadosExtras['fldPrestacao_Valor']."'
		WHERE fldCliente_Id = '".$cliente_id."'
		") or die (mysql_error());
		
		$atualizarReferenciasComerciais		=		mysql_query("
		UPDATE tblcliente_dados_adicionais SET
		fldReferencia_Empresa1			 = '".$DadosReferenciasComerciais['fldReferencia_Empresa1']."',
		fldReferencia_Empresa1_Telefone1 = '".$DadosReferenciasComerciais['fldReferencia_Empresa1_Telefone1']."',
		fldReferencia_Empresa1_Telefone2 = '".$DadosReferenciasComerciais['fldReferencia_Empresa1_Telefone2']."',
		fldReferencia_Empresa2			 = '".$DadosReferenciasComerciais['fldReferencia_Empresa2']."',
		fldReferencia_Empresa2_Telefone1 = '".$DadosReferenciasComerciais['fldReferencia_Empresa2_Telefone1']."',
		fldReferencia_Empresa2_Telefone2 = '".$DadosReferenciasComerciais['fldReferencia_Empresa2_Telefone2']."',
		fldReferencia_Empresa3			 = '".$DadosReferenciasComerciais['fldReferencia_Empresa3']."',
		fldReferencia_Empresa3_Telefone1 = '".$DadosReferenciasComerciais['fldReferencia_Empresa3_Telefone1']."',
		fldReferencia_Empresa3_Telefone2 = '".$DadosReferenciasComerciais['fldReferencia_Empresa3_Telefone2']."'
		WHERE fldCliente_Id = '".$cliente_id."'
		") or die (mysql_error());
	
		$atualizarReferenciasPessoais		=		mysql_query("
		UPDATE tblcliente_dados_adicionais SET
		fldReferencia_Pessoal1				= '".$DadosReferenciasPessoais['fldReferencia_Pessoal1']."',
		fldReferencia_Pessoal1_Telefone1 	= '".$DadosReferenciasPessoais['fldReferencia_Pessoal1_Telefone1']."',
		fldReferencia_Pessoal1_Telefone2	= '".$DadosReferenciasPessoais['fldReferencia_Pessoal1_Telefone2']."',
		fldReferencia_Pessoal1_Parentesco	= '".$DadosReferenciasPessoais['fldReferencia_Pessoal1_Parentesco']."',
		fldReferencia_Pessoal2				= '".$DadosReferenciasPessoais['fldReferencia_Pessoal2']."',
		fldReferencia_Pessoal2_Telefone1 	= '".$DadosReferenciasPessoais['fldReferencia_Pessoal2_Telefone1']."',
		fldReferencia_Pessoal2_Telefone2	= '".$DadosReferenciasPessoais['fldReferencia_Pessoal2_Telefone2']."',
		fldReferencia_Pessoal2_Parentesco	= '".$DadosReferenciasPessoais['fldReferencia_Pessoal2_Parentesco']."',
		fldReferencia_Pessoal3				= '".$DadosReferenciasPessoais['fldReferencia_Pessoal3']."',
		fldReferencia_Pessoal3_Telefone1 	= '".$DadosReferenciasPessoais['fldReferencia_Pessoal3_Telefone1']."',
		fldReferencia_Pessoal3_Telefone2	= '".$DadosReferenciasPessoais['fldReferencia_Pessoal3_Telefone2']."',
		fldReferencia_Pessoal3_Parentesco	= '".$DadosReferenciasPessoais['fldReferencia_Pessoal3_Parentesco']."'
		WHERE fldCliente_Id = '".$cliente_id."'
		") or die (mysql_error());
		
		if(mysql_error()){
			echo mysql_error();
?>			<div class="alert">
				<p class="erro">N&atilde;o foi poss&iacute;vel gravar os dados</p>
			</div>
<?			
		}else{
?>			<div class="alert" style="margin-top: 5px">
            	<p class="ok">Cadastro atualizado</p>
            </div>
<?			
		}
		
	}
	
	$selecionarDadosAdicionais 		= mysql_query("SELECT * FROM tblcliente_dados_adicionais WHERE fldCliente_Id = '".$cliente_id."'");
	$selecionarDadosAdicionais	 	= mysql_fetch_array($selecionarDadosAdicionais);
	
	
	$responsavelMunicipio = fnc_ibge_municipio($selecionarDadosAdicionais['fldResponsavel_Municipio_Codigo']);
	$responsavelUF = fnc_ibge_uf_sigla($selecionarDadosAdicionais['fldResponsavel_Municipio_Codigo']);

	
?>
	
	<div >
		<form method="post" action="" style="width: 890px;" class="frm_detalhe">
			<ul>
            	<fieldset style="border: 1px solid #cccccc;">
					<legend>Dados Respons&aacute;vel</legend>
                    <li>
                        <label for="txt_responsavel">Respons&aacute;vel</label>
                        <input type="text" style="width: 200px;" id="txt_responsavel" name="txt_responsavel" value="<?=$selecionarDadosAdicionais['fldResponsavel']?>" />
                    </li>
                    <li>
                        <label for="txt_responsavel_cpf">CPF</label>
                        <input type="text" style="width: 200px;" id="txt_responsavel_cpf" name="txt_responsavel_cpf" value="<?=$selecionarDadosAdicionais['fldResponsavel_CPF']?>" />
                    </li>
                    <li>
                        <label for="txt_responsavel_Telefone1">Telefone 1</label>
                        <input type="text" style=" width:100px" class="fone-mask" id="txt_responsavel_Telefone1" name="txt_responsavel_Telefone1" value="<?=$selecionarDadosAdicionais['fldResponsavel_Telefone1']?>" />
                    </li>
                    <li>
                        <label for="txt_responsavel_Telefone2">Telefone 2</label>
                        <input type="text" style=" width:100px" class="fone-mask" id="txt_responsavel_Telefone2" name="txt_responsavel_Telefone2" value="<?=$selecionarDadosAdicionais['fldResponsavel_Telefone2']?>" />
                    </li>
                    <li>
                        <label for="txt_responsavel_endereco">Endere&ccedil;o</label>
                        <input type="text" style=" width:100px"  id="txt_responsavel_endereco" name="txt_responsavel_endereco" value="<?=$selecionarDadosAdicionais['fldResponsavel_Endereco']?>" />
                    </li>
                    <li>
                        <label for="txt_responsavel_numero">N&uacute;mero</label>
                        <input type="text" style=" width:80px;" id="txt_responsavel_numero" name="txt_responsavel_numero" value="<?=$selecionarDadosAdicionais['fldResponsavel_Numero']?>" />
                    </li>
                    <li>
                        <label for="txt_responsavel_municipio_codigo">C&oacute;d. Munic&iacute;pio</label>
                        <input type="text" style=" width:100px;" id="txt_municipio_codigo" name="txt_responsavel_municipio_codigo" value="<?=$selecionarDadosAdicionais['fldResponsavel_Municipio_Codigo']?>" />
                        <a href="municipio_busca" title="Localizar" class="modal" rel="680-380"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                    </li>
                    <li>
                        <label for="txt_responsavel_municipio">Munic&iacute;pio</label>
                        <input type="text" style=" width:275px" id="txt_municipio" name="txt_responsavel_municipio" value="<?=$responsavelMunicipio?>" />
                    </li>
                    <li>
                        <label for="txt_responsavel_uf">UF</label>
                        <input type="text" style=" width:100px" id="txt_uf" name="txt_responsavel_uf" value="<?=$responsavelUF?>" />
                    </li>
                </fieldset>
                    
                <fieldset style="border: 1px solid #cccccc;">
                    <legend>Dados Conjuge</legend>
                    <li>
                        <label for="txt_nome">Nome</label>
                        <input type="text" style=" width:200px;" id="txt_conjuge_nome" name="txt_conjuge_nome" value="<?=$selecionarDadosAdicionais['fldConjuge_Nome'];?>" />
                    </li>
                                    
                    <li>
                        <label for="txt_local_trabalho">Local de Trabalho</label>
                        <input type="text" style=" width:150px;" id="txt_conjuge_local_trabalho" name="txt_conjuge_local_trabalho" value="<?=$selecionarDadosAdicionais['fldConjuge_Trabalho'];?>" />
                    </li>
                    <li>
                        <label for="txt_conjuge_salario">Salário</label>
                        <input type="text" style=" width:100px;" name="txt_conjuge_salario" value="<?=format_number_out($selecionarDadosAdicionais['fldConjuge_Salario']);?>" />
                    </li>
                    <li>
                        <label for="txt_conjuge_profissao">Profissao</label>
                        <input type="text" style=" width:200px;" id="txt_conjuge_profissao" name="txt_conjuge_profissao" value="<?=$selecionarDadosAdicionais['fldConjuge_Profissao'];?>" />
                    </li>
                    <li>
                        <label for="txt_tempo_trabalho">Tempo de Trabalho</label>
                        <input type="text" style=" width:120px;" id="txt_conjuge_tempo_trabalho" name="txt_conjuge_tempo_trabalho" value="<?=$selecionarDadosAdicionais['fldConjuge_Trabalho_Tempo'];?>" />
                    </li>
                </fieldset>
                
                
                
				<fieldset style="border: 1px solid #cccccc;">
					<legend>Dados Profissionais</legend>
                    <li>
                        <label for="txt_local">Empresa</label>
                        <input type="text" style="width: 200px;" id="txt_local" name="txt_local" value="<?=$selecionarDadosAdicionais['fldTrabalho_Empresa'];?>" />
                    </li>
                    <li>
                        <label for="txt_local">Endereço da Empresa</label>
                        <input type="text" style="width: 200px;" id="txt_endereco" name="txt_endereco" value="<?=$selecionarDadosAdicionais['fldTrabalho_Endereco'];?>" />
                    </li>
                    <li>
                        <label for="txt_telefone">Telefone 1</label>
                        <input type="text" style=" width:100px" id="txt_telefone1" name="txt_telefone1" value="<?=$selecionarDadosAdicionais['fldTrabalho_Telefone1'];?>" />
                    </li>
                    <li>
                        <label for="txt_telefone">Telefone 2</label>
                        <input type="text" style=" width:100px" id="txt_telefone2" name="txt_telefone2" value="<?=$selecionarDadosAdicionais['fldTrabalho_Telefone2'];?>" />
                    </li>
                    <li>
                        <label for="txt_telefone">Data de admissão</label>
                        <input type="text" style=" width:100px" class="calendario-mask" id="txt_data" name="txt_data_admissao" value="<?=$selecionarDadosAdicionais['fldTrabalho_Admissao'];?>" />
                    </li>
                    <li>
                        <label for="txt_profissao">Cargo</label>
                        <input type="text" style=" width:200px;" id="txt_cargo" name="txt_cargo" value="<?=$selecionarDadosAdicionais['fldTrabalho_Cargo'];?>" />
                    </li>
                    <li>
                        <label for="txt_salario">Salário</label>
                        <input type="text" style=" width:100px;"  name="txt_salario" value="<?=format_number_out($selecionarDadosAdicionais['fldTrabalho_Salario']);?>" />
                    </li>
                    <li>
                        <label for="txt_tempo_trabalho">Tempo de Trabalho</label>
                        <input type="text" style=" width:120px;" id="txt_tempo_trabalho" name="txt_tempo_trabalho" value="<?=$selecionarDadosAdicionais['fldTrabalho_Tempo'];?>" />
                    </li>
                </fieldset>
                    
                <fieldset style="border: 1px solid #cccccc;">
                    <legend>Dados Conjuge</legend>
                    <li>
                        <label for="txt_nome">Nome</label>
                        <input type="text" style=" width:200px;" id="txt_conjuge_nome" name="txt_conjuge_nome" value="<?=$selecionarDadosAdicionais['fldConjuge_Nome'];?>" />
                    </li>
                                    
                    <li>
                        <label for="txt_local_trabalho">Local de Trabalho</label>
                        <input type="text" style=" width:150px;" id="txt_conjuge_local_trabalho" name="txt_conjuge_local_trabalho" value="<?=$selecionarDadosAdicionais['fldConjuge_Trabalho'];?>" />
                    </li>
                    <li>
                        <label for="txt_conjuge_salario">Salário</label>
                        <input type="text" style=" width:100px;" name="txt_conjuge_salario" value="<?=format_number_out($selecionarDadosAdicionais['fldConjuge_Salario']);?>" />
                    </li>
                    <li>
                        <label for="txt_conjuge_profissao">Profissao</label>
                        <input type="text" style=" width:200px;" id="txt_conjuge_profissao" name="txt_conjuge_profissao" value="<?=$selecionarDadosAdicionais['fldConjuge_Profissao'];?>" />
                    </li>
                    <li>
                        <label for="txt_tempo_trabalho">Tempo de Trabalho</label>
                        <input type="text" style=" width:120px;" id="txt_conjuge_tempo_trabalho" name="txt_conjuge_tempo_trabalho" value="<?=$selecionarDadosAdicionais['fldConjuge_Trabalho_Tempo'];?>" />
                    </li>
                </fieldset>
                
                <fieldset style="border: 1px solid #cccccc;">
                    <legend>Dados Extras</legend>
                   <li>
                        <label for="txt_renda_familiar">Renda Familiar</label>
                        <input type="text" style=" width:100px;"  name="txt_renda_familiar"  value="<?=format_number_out($selecionarDadosAdicionais['fldRenda_Familiar']);?>" />
                    </li>
                    <li>
                        <label for="txt_valor_prestacao">Valor da Prestacao</label>
                        <input type="text" style=" width:100px" name="txt_valor_prestacao" value="<?=format_number_out($selecionarDadosAdicionais['fldPrestacao_Valor']);?>" />
                    </li>                 
                </fieldset>
                    
                <fieldset style="border: 1px solid #cccccc;">
                    <legend>Referências Comerciais</legend>
                    <li>
                        <label for="txt_empresa1">Referência 1</label>
                        <input type="text" style=" width:400px;"  name="txt_empresa1"  value="<?=$selecionarDadosAdicionais['fldReferencia_Empresa1'];?>" />
                    </li>
                    <li>
                        <label for="txt_empresa1_telefone1">Telefone 1</label>
                        <input type="text" style=" width:200px;" class="fone-mask" id="txt_empresa1_telefone1" name="txt_empresa1_telefone1"  value="<?=$selecionarDadosAdicionais['fldReferencia_Empresa1_Telefone1'];?>" />
                    </li>
                    <li>
                        <label for="txt_empresa1_telefone2">Telefone 2</label>
                        <input type="text" style=" width:200px;" class="fone-mask" id="txt_empresa1_telefone2" name="txt_empresa1_telefone2"  value="<?=$selecionarDadosAdicionais['fldReferencia_Empresa1_Telefone2'];?>" />
                    </li>
                    <li>
                        <label for="txt_empresa2">Referência 2</label>
                        <input type="text" style=" width:400px" id="txt_empresa2" name="txt_empresa2" value="<?=$selecionarDadosAdicionais['fldReferencia_Empresa2'];?>" />
                    </li>
                    <li>
                        <label for="txt_empresa2_telefone1">Telefone 1</label>
                        <input type="text" style=" width:200px;" class="fone-mask" id="txt_empresa2_telefone1" name="txt_empresa2_telefone1"  value="<?=$selecionarDadosAdicionais['fldReferencia_Empresa2_Telefone1'];?>" />
                    </li>
                    <li>
                        <label for="txt_empresa2_telefone2">Telefone 2</label>
                        <input type="text" style=" width:200px;" class="fone-mask" id="txt_empresa2_telefone2" name="txt_empresa2_telefone2"  value="<?=$selecionarDadosAdicionais['fldReferencia_Empresa2_Telefone2'];?>" />
                    </li>
                    <li>
                        <label for="txt_empresa3">Referência 3</label>
                        <input type="text" style=" width:400px" id="txt_empresa3" name="txt_empresa3" value="<?=$selecionarDadosAdicionais['fldReferencia_Empresa3'];?>" />
                    </li>
                    <li>
                        <label for="txt_empresa3_telefone1">Telefone 1</label>
                        <input type="text" style=" width:200px;" class="fone-mask" id="txt_empresa3_telefone1" name="txt_empresa3_telefone1"  value="<?=$selecionarDadosAdicionais['fldReferencia_Empresa3_Telefone1'];?>" />
                    </li>
                    <li>
                        <label for="txt_empresa3_telefone2">Telefone 2</label>
                        <input type="text" style=" width:200px;" class="fone-mask" id="txt_empresa3_telefone2" name="txt_empresa3_telefone2"  value="<?=$selecionarDadosAdicionais['fldReferencia_Empresa3_Telefone2'];?>" />
                    </li>
                </fieldset>
            
                <fieldset style="border: 1px solid #cccccc;">
                    <legend>Referências Pessoais</legend>
                    <!-- Primeira referência -->
                     <li>
                        <label for="txt_referencia_pessoal1">Referência 1</label>
                        <input type="text" style=" width:290px;" id="txt_referencia_pessoal1"  name="txt_referencia_pessoal1"  value="<?=$selecionarDadosAdicionais['fldReferencia_Pessoal1'];?>" />
                    </li>
                    <li>
                        <label for="txt_referencia_pessoal1_telefone1">Telefone 1</label>
                        <input type="text" style=" width:150px;" class="fone-mask" id="txt_referencia_pessoal1_telefone1"  name="txt_referencia_pessoal1_telefone1"  value="<?=$selecionarDadosAdicionais['fldReferencia_Pessoal1_Telefone1'];?>" />
                    </li>
                    <li>
                        <label for="txt_referencia_pessoal1_telefone2">Telefone 2 </label>
                        <input type="text" style=" width:150px;" class="fone-mask" id="txt_referencia_pessoal1_telefone2"  name="txt_referencia_pessoal1_telefone2"  value="<?=$selecionarDadosAdicionais['fldReferencia_Pessoal1_Telefone2'];?>" />
                    </li>
                    <li>
                      <label for="txt_referencia_pessoal1_parentesco">Grau de parentesco</label>
                        <input type="text" style="width: 200px;" id="txt_referencia_pessoal1_parentesco" name="txt_referencia_pessoal1_parentesco" value="<?=$selecionarDadosAdicionais['fldReferencia_Pessoal1_Parentesco'];?>">
                    </li>
                     
                     <!-- Segunda referência -->
                     
                    <li>
                        <label for="txt_referencia_pessoal2">Referência 2</label>
                        <input type="text" style=" width:290px;" id="txt_referencia_pessoal2" name="txt_referencia_pessoal2" value="<?=$selecionarDadosAdicionais['fldReferencia_Pessoal2'];?>" />
                    </li>
                    <li>
                        <label for="txt_referencia_pessoal2_telefone1">Telefone 1</label>
                        <input type="text" style=" width:150px;" class="fone-mask" id="txt_referencia_pessoal2_telefone1" name="txt_referencia_pessoal2_telefone1"  value="<?=$selecionarDadosAdicionais['fldReferencia_Pessoal2_Telefone1'];?>" />
                    </li>
                    <li>
                        <label for="txt_referencia_pessoal2_telefone2">Telefone 2</label>
                        <input type="text" style=" width:150px;" class="fone-mask" id="txt_referencia_pessoal2_telefone2" name="txt_referencia_pessoal2_telefone2"  value="<?=$selecionarDadosAdicionais['fldReferencia_Pessoal2_Telefone2'];?>" />
                    </li>
                    <li>
                      <label for="txt_referencia_pessoal2_Parentesco">Grau de parentesco</label>
                        <input type="text" style="width: 200px;" id="txt_referencia_pessoal2_Parentesco" name="txt_referencia_pessoal2_Parentesco" value="<?=$selecionarDadosAdicionais['fldReferencia_Pessoal2_Parentesco'];?>">
                    </li>
                        
                        <!-- Terceira referência -->
                        
                    <li>
                        <label for="txt_referencia_pessoal3">Referência 3</label>
                        <input type="text" style=" width:290px;" id="txt_referencia_pessoal3" name="txt_referencia_pessoal3" value="<?=$selecionarDadosAdicionais['fldReferencia_Pessoal3'];?>" />
                    </li>
                    <li>
                        <label for="txt_referencia_pessoal3_telefone1">Telefone 1</label>
                        <input type="text" style=" width:150px;" class="fone-mask" id="txt_referencia_pessoal3_telefone1" name="txt_referencia_pessoal3_telefone1"  value="<?=$selecionarDadosAdicionais['fldReferencia_Pessoal3_Telefone1'];?>" />
                    </li>
                    <li>
                        <label for="txt_referencia_pessoal3_telefone2">Telefone 2</label>
                        <input type="text" style=" width:150px;" class="fone-mask" id="txt_referencia_pessoal3_telefone2" name="txt_referencia_pessoal3_telefone2"  value="<?=$selecionarDadosAdicionais['fldReferencia_Pessoal3_Telefone2'];?>" />
                    </li>
                    <li>
                        <label for="txt_referencia_pessoal3_Parentesco">Grau de parentesco</label>
                        <input type="text" style="width: 200px;" id="txt_referencia_pessoal3_Parentesco" name="txt_referencia_pessoal3_Parentesco" value="<?=$selecionarDadosAdicionais['fldReferencia_Pessoal3_Parentesco'];?>">
                    </li>
                </fieldset>
                <!-- Final das referências -->
                <li style="float: right">
                    <input type="submit" style="margin-top: 14px" class="btn_enviar" name="btn_enviar" id="btn_enviar" value="salvar" title="Salvar" />
                </li>
            </ul>
		</form>
	</div>       
</div>