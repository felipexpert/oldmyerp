<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html language="pt-br">
<head>
<title>myERP - Imprimir NP</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Robots" content="none" />

    <link rel="stylesheet" type="text/css" media="all" href="style/impressao/style_pedido_imprimir_np_recibo1.css" />
    <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />
    <script type="text/javascript" src="js/general.js"></script>
    
</head>
<body>

	
<?
	if (!isset($_SESSION['logado'])){
		session_start();
	}

	ob_start();
	session_start();
	
	//conectar ao db
	require_once("inc/con_db.php");
	require_once("inc/fnc_general.php");

	$pedido_id = $_GET['id'];
	
	$data = date("Y-m-d");
	
	$rsPedido  	= mysql_query("SELECT SUM((tblpedido_item.fldValor * (tblpedido_item.fldExcluido * -1 + 1)) * tblpedido_item.fldQuantidade) as fldTotalItem,
						(SELECT SUM(fldValor) FROM tblpedido_funcionario_servico WHERE fldFuncao_Tipo = 2 AND fldPedido_Id = tblpedido.fldId) as fldTotalServico,
						tblpedido.fldCadastroData as fldPedidoData, tblpedido.fldServico, tblpedido.fldDesconto, tblpedido.fldValor_Terceiros, tblpedido.fldId as fldPedidoId, tblpedido.fldDescontoReais, tblcliente.*
						FROM tblpedido 
						LEFT JOIN tblpedido_item ON tblpedido.fldId = tblpedido_item.fldPedido_Id
						INNER JOIN tblcliente ON tblpedido.fldCliente_Id = tblcliente.fldId
						WHERE tblpedido.fldId = $pedido_id GROUP BY tblpedido_item.fldPedido_Id");
	$rowPedido 	= mysql_fetch_array($rsPedido);
	
	$rsParcela = mysql_query("SELECT fldVencimento FROM tblpedido_parcela WHERE fldPedido_Id = ".$rowPedido['fldPedidoId']." AND fldParcela = '1' AND fldStatus = 1 AND fldExcluido = 0 ");
	$rowParcela = mysql_fetch_array($rsParcela);
	
	if($rowPedido['fldMunicipio_Codigo']){
		
		$queryString = $rowPedido['fldMunicipio_Codigo'];
		$municipio =  substr($queryString,2,5);	
		$uf =  substr($queryString,0,2);
			
		$rsMunicipio = mysql_query("select * from tblibge_municipio where fldCodigo = ".$municipio." and fldUF_codigo =".$uf);
		$rowMunicipio = mysql_fetch_array($rsMunicipio);
	
		$rsUF = mysql_query("select * from tblibge_uf where fldCodigo = ".$uf);
		$rowUF = mysql_fetch_array($rsUF);
	}
	
	if($rowPedido['fldCPF_CNPJ'] != null){
		if($rowPedido['fldTipo'] == 1){
			$CPF_CNPJ_Cliente = format_cpf_out($rowPedido['fldCPF_CNPJ']);
		}elseif($rowPedido['fldTipo'] == 2){
			$CPF_CNPJ_Cliente = format_cnpj_out($rowPedido['fldCPF_CNPJ']);
		}
	}
	
	if($rowPedido['fldMunicipio_Codigo'] > 0 ){
		
		$queryString = $rowPedido['fldMunicipio_Codigo'];
		$municipio =  substr($queryString,2,5);	
		$uf =  substr($queryString,0,2);
			
		$rsMunicipio = mysql_query("select * from tblibge_municipio where fldCodigo = ".$municipio." and fldUF_Codigo =".$uf);
		$rowMunicipio = mysql_fetch_array($rsMunicipio);

		$rsUF = mysql_query("select * from tblibge_uf where fldCodigo = ".$uf);
		$rowUF = mysql_fetch_array($rsUF);
		
		$municipio = $rowMunicipio['fldNome']." - ". $rowUF['fldSigla'];        
        
	}
	
	/*----------------------------------------------------------------------------------------------*/
	$rsEmpresa = mysql_query("SELECT * FROM tblempresa_info");
	$rowEmpresa = mysql_fetch_array($rsEmpresa);
	
	if($rowEmpresa['fldCPF_CNPJ'] != null){
		if($rowEmpresa['fldTipo'] == 1){
			$CPF_CNPJ = format_cpf_out($rowEmpresa['fldCPF_CNPJ']);
		}elseif($rowEmpresa['fldTipo'] == 2){
			$CPF_CNPJ = format_cnpj_out($rowEmpresa['fldCPF_CNPJ']);
		}
	}
	
	
?>
 	
    <div id="no-print">
        <div id="impressao_cabecalho">
            <ul id="bts">
                <li><a href="#" onclick ="window.print()"><span>Imprimir</span></a></li>
                <li><a href="index.php?p=pedido&amp;mensagem=ok"><span>Finalizar</span></a></li>
            </ul>
        </div>
	</div>
    
	<table style="margin-top: 30px" name="table_imprimir_np" class="table_imprimir" summary="Imprimir Pedido">
        <tr>
			<td class="cabecalho"><strong><?=$rowEmpresa['fldNome_Fantasia']?></strong></td>
			<td class="cabecalho"><strong><?=$rowEmpresa['fldTelefone1']?></strong></td>
        </tr>
        <tr>
			<td>Cliente: <?=$rowPedido['fldNome']?></td>
        </tr>
        <tr>
			<td>CPF/CNPJ: <?=$CPF_CNPJ_Cliente?></td>
        </tr>
        <tr>
			<td>Data da Fatura: <?=format_date_out($rowPedido['fldPedidoData'])?></td>
			<td>N&ordm;: <?=$rowPedido['fldPedidoId']?></td>
        </tr>
        <tr>
			<td>Vencimento: <?=format_date_out($rowParcela['fldVencimento'])?></td>
        </tr>
	</table>
    
    <table name="table_imprimir_np" class="table_imprimir" summary="Imprimir Pedido">
       
<?		
		$total_pedido = $rowPedido['fldTotalItem'] + $rowPedido['fldTotalServico'] + $rowPedido['fldValor_Terceiros'];
				
		$desconto_pedido = $rowPedido['fldDesconto'];
		$desconto_reais_pedido = $rowPedido['fldDescontoReais'];
		
		$desconto = ($total_pedido * $desconto_pedido) / 100;
		$total_descontos = $desconto + $desconto_reais_pedido;
		
		$total_pedido_apagar = ($total_pedido - $total_descontos);

?>		
        <tr style="margin-top:20px">
			<td colspan="4"> Ao <?=formata_data_extenso($rowParcela['fldVencimento'])?> pagarei por esta unica via de NOTA PROMISSORIA a <?=$rowEmpresa['fldRazao_Social']?> CNPJ - <?=$CPF_CNPJ?> ou a sua ordem, a quantia de R$ <?= format_number_out($total_pedido_apagar); ?> (<span class="uppercase"><?= utf8_encode(valorExtenso($total_pedido_apagar,1,"baixa"))?></span>) em moeda corrente deste pais, pagavel em ITAPIRA-SP.</td>
        </tr>        
        <tr >
        	<td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 7.5cm"><?=$rowPedido['fldNome']?></td>
        </tr>
<?		if($rowPedido['fldEndereco'] != ''){        
?>
            <tr>
                <td style="width: 7.5cm"><?=$rowPedido['fldEndereco']?>, <?=$rowPedido['fldNumero']?></td>
            </tr>
            <tr>
                <td style="width: 7.5cm" style="text-align:left"><?=$rowPedido['fldBairro']?> / <?=$municipio?></td>
            </tr>
<?		}
?>
        <tr >
        	<td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 7.5cm; text-align:center">ITAPIRA, <?=format_date_out($rowPedido['fldPedidoData'])?></td>
            <td style="width: 0.5cm" >&nbsp;</td>
           	<td style="text-align:center;border-top:1px solid" colspan="2" ><?=$rowPedido['fldNome']?></td>
        </tr>
	
    </table>


</body>
</html>