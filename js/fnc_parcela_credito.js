

function fnc_credito_restante(id_parcela) {
	var credito_disponivel 	= $('#hid_credito_disponivel').val();
	var credito_baixa 		= 0;
	$('.sel_pagamento_tipo:not(#sel_pagamento_tipo_'+id_parcela+')').each(function() {
		var tipo_pagamento 	= $(this).val();
		var valor_baixa 	= br2float($(this).parents('tr').find('td input.valor_baixa').val()).toFixed(2);
		if(tipo_pagamento == 15){ //15= credito
			credito_baixa += Number(valor_baixa);
		}
	});
	retorno_credito = credito_disponivel - credito_baixa;
	
	return retorno_credito;
}

$(document).ready(function(){
	
	
	$(".sel_pagamento_tipo").change(function(){
		var nome_input			= $(this).attr('name').split('_');
		var id_parcela			= nome_input[3];
		var valor_baixa_atual 	= br2float($(this).parents('tr').find('td input.valor_baixa').val()).toFixed(2);
		retorno_credito 		= fnc_credito_restante(id_parcela);
		
		if(valor_baixa_atual > retorno_credito){
			 $(this).parents('tr').find('td input.valor_baixa').val(float2br(retorno_credito.toFixed(2)));
		}
		
		$('input.valor_baixa').blur();
	});
	
	$('input.valor_baixa').change(function(){	
		var valor_baixa_atual	= $(this).val();
		var nome_input			= $(this).attr('name').split('_');
		var id_parcela			= nome_input[3];
		retorno_credito 		= fnc_credito_restante(id_parcela);
		
		if(valor_baixa_atual > retorno_credito){
			 $(this).val(float2br(retorno_credito.toFixed(2)));
		}
	});
	
});