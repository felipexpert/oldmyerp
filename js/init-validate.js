//As funções para validar CPF e CPNJ serão usadas para validar fora do plugin validate
//Serão utilizadas no evento submit do formulário

//Função para Validar CNPJ
function validarCNPJ(cnpj) {
   
   if(cnpj == ""){ return true };
   
   cnpj = jQuery.trim(cnpj);
   cnpj = cnpj.replace('/','');
   cnpj = cnpj.replace('.','');
   cnpj = cnpj.replace('.','');
   cnpj = cnpj.replace('-','');
 
   var numeros, digitos, soma, i, resultado, pos, tamanho, digitos_iguais;
   digitos_iguais = 1;
 
   if (cnpj.length < 14 && cnpj.length < 15){
      return false;
   }
   for (i = 0; i < cnpj.length - 1; i++){
      if (cnpj.charAt(i) != cnpj.charAt(i + 1)){
         digitos_iguais = 0;
         break;
      }
   }
 
   if (!digitos_iguais){
      tamanho = cnpj.length - 2
      numeros = cnpj.substring(0,tamanho);
      digitos = cnpj.substring(tamanho);
      soma = 0;
      pos = tamanho - 7;
 
      for (i = tamanho; i >= 1; i--){
         soma += numeros.charAt(tamanho - i) * pos--;
         if (pos < 2){
            pos = 9;
         }
      }
      resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
      if (resultado != digitos.charAt(0)){
         return false;
      }
      tamanho = tamanho + 1;
      numeros = cnpj.substring(0,tamanho);
      soma = 0;
      pos = tamanho - 7;
      for (i = tamanho; i >= 1; i--){
         soma += numeros.charAt(tamanho - i) * pos--;
         if (pos < 2){
            pos = 9;
         }
      }
      resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
      if (resultado != digitos.charAt(1)){
         return false;
      }
      return true;
   }
   else
   {
      return false;
   }
}

/***************************************************************************************************************/


//Função para validar CPF
function validarCPF(cpf) {
   
   if(cpf == ""){ return true };
      
   exp = /\.|-/g;
   cpf = cpf.toString().replace(exp, "");
   
   if (cpf == "00000000000" || cpf == "11111111111" || cpf == "22222222222" ||
       cpf == "33333333333" || cpf == "44444444444" || cpf == "55555555555" ||
       cpf == "66666666666" || cpf == "77777777777" || cpf == "88888888888" || cpf == "99999999999"
       ) return false;
   
   add = 0;
   
   for (i=0; i < 9; i ++) {
      add += parseInt(cpf.charAt(i)) * (10 - i);
      rev = 11 - (add % 11);
   }
   
   if (rev == 10 || rev == 11) rev = 0;
   if (rev != parseInt(cpf.charAt(9))) return false;
   
   add = 0;
   
   for (i = 0; i < 10; i ++) {
      add += parseInt(cpf.charAt(i)) * (11 - i);
      rev = 11 - (add % 11);
   }
   
   if (rev == 10 || rev == 11) rev = 0;
   if (rev != parseInt(cpf.charAt(10))) return false;
   
   return true;
   
}




function validaData(value){
 
    // Define uma expressão regular para validar se a data informada está
    // no formato nn/nn/nnnn, onde n é um número entre 0 e 9
    var regex 	= new RegExp("^([0-9]{2})/([0-9]{2})/([0-9]{4})$");
    var matches = regex.exec(value);
    if (matches != null){
		
        var day 	= parseInt(matches[1], 10);
        var month 	= parseInt(matches[2], 10) - 1;
        var year 	= parseInt(matches[3], 10);
 
        var date 	= new Date(year, month, day, 1, 0, 0, 0); //1 ali p/ evitar problemas com o horário de verão
        valid 		= date.getFullYear() == year && date.getMonth() == month &&
        date.getDate() == day;
    }
 
    return valid;
}

/***************************************************************************************************************/
 
$(document).ready(function(){
	
	//máscaras de entrada
	$('#txt_telefone1, #txt_telefone2, #txt_fax, .fone-mask').live('focusin',function(){
		$(this).unmask();
		$(this).mask('(99) 9999-9999?9');
	});
	
	//máscaras de entrada
	$('#txt_hora, .hora-mask').live('focusin',function(){
		$(this).unmask();
		$(this).mask('99:99');
	});
	
    //foi usado o live com mask/function para adicionar máscara mesmo nos modais!
	$('#txt_nascimento_abertura, #txt_nascimento, #txt_rg_emissao, .calendario-mask').live('focusin',function(){ //adicionado o evento focusin no lugar de focus por causa do problema de pular caracter dentro de um modal
		$(this).unmask();
		$(this).mask('99/99/9999');
		
		//POR CAUSA DA VALIDACAO DE DATA
	    $(this).next('small.error').remove();
	});
	
	$('#txt_nascimento_abertura, #txt_nascimento, #txt_rg_emissao, .calendario-mask').live('focusout',function(){
		var msgData  = '<small class="error">Data inválida!</small>';
		if(validaData($(this).val()) == false){
			$(msgData).insertAfter(this);
			$(this).css({'background-color' : '#FFD9D9'})
		}
	});
	
	$('#txt_data_nascimento, .aniversario-mask').live('focus',function(){
		 $(this).unmask();
		 $(this).mask('99/99');
	});
	
	$('#txt_cep, .cep-mask').live('focus',function(){
		 $(this).unmask();
		 $(this).mask('99.999-999');
	});
    
    //máscara para horário
   	$('input.hora-mask').live('focus', function(){
      $(this).unmask();
      $(this).mask('99:99');
    });
		
   //plugin validate
   $('form').validate({
      rules: {
         'txt_nome' 				: {required : $('#txt_nome').attr('class')=='obrigatorio'},
         'txt_nome_fantasia' 		: {required : $('#txt_nome_fantasia').attr('class')=='obrigatorio'},
         'txt_razao_social' 		: {required : $('#txt_razao_social').attr('class')=='obrigatorio'},
         'txt_telefone1' 			: {required : $('#txt_telefone1').attr('class')=='obrigatorio'},
         'txt_email'		 		: {required : $('#txt_email').attr('class')=='obrigatorio'},
         'txt_cpf_cnpj'				: {required : $('#txt_cpf_cnpj').attr('class')=='obrigatorio'},
         'txt_rg_ie' 				: {required : $('#txt_rg_ie').attr('class')=='obrigatorio'},
         'txt_endereco' 			: {required : $('#txt_endereco').attr('class')=='obrigatorio'},
         'txt_nascimento_abertura' 	: {required : $('#txt_nascimento_abertura').attr('class')=='obrigatorio'},
         'txt_bairro' 				: {required	: $('#txt_bairro').attr('class')=='obrigatorio'},
         'txt_numero' 				: {required	: $('#txt_numero').attr('class')=='obrigatorio'},
         'txt_municipio_codigo' 	: {required : $('#txt_municipio_codigo').attr('class')=='obrigatorio'},
         'txt_cep' 					: {required : $('#txt_cep').attr('class')=='obrigatorio'},
         'txt_usuario' 				: {required : $('#txt_usuario').attr('class')=='obrigatorio'},
         'txt_senha' 				: {required : $('#txt_senha').attr('class')=='obrigatorio'},
         'txt_credito_limite' 		: {required : $('#txt_credito_limite').attr('class')=='obrigatorio'},
         'txt_comissao' 			: {required : $('#txt_comissao').attr('class')=='obrigatorio'},
         'txt_descricao' 			: {required : $('#txt_descricao').attr('class')=='obrigatorio'},
         'txt_estoque' 				: {required : $('#txt_estoque').attr('class')=='obrigatorio'},
         'sel_marca' 				: {required : $('#sel_marca').attr('class')=='obrigatorio' && $('#sel_marca').attr('value')==0},
         'sel_categoria'			: {required : $('#sel_categoria').attr('class')=='obrigatorio' && $('#sel_categoria').attr('value')==0},
         'sel_sub_categoria' 		: {required : $('#sel_sub_categoria').attr('class')=='obrigatorio' && $('#sel_sub_categoria').attr('value')==0},
         'sel_fornecedor' 			: {required : $('#sel_fornecedor').attr('class')=='obrigatorio' && $('#sel_fornecedor').attr('value')==0},
         'txt_valor_compra' 		: {required : $('#txt_valor_compra').attr('class')=='obrigatorio'},
         'txt_lucro_margem' 		: {required : $('#txt_lucro_margem').attr('class')=='obrigatorio'},
         'txt_valor_venda' 			: {required : $('#txt_valor_venda').attr('class')=='obrigatorio'},
         'txt_desconto_avista' 		: {required : $('#txt_desconto_avista').attr('class')=='obrigatorio'},
         'txt_desconto_aprazo' 		: {required : $('#txt_desconto_aprazo').attr('class')=='obrigatorio'}
      },
           
      messages: {
         'txt_nome' : 'O nome deve ser informado!',
         'txt_nome_fantasia' : 'O nome fantasia deve ser informado!',
         'txt_razao_social' : 'A razão social deve ser informada!',
         'txt_telefone1' : 'O telefone deve ser informado!',
         'txt_email' : 'O e-mail deve ser informado!',
         'txt_cpf_cnpj' : 'Este campo deve ser informado!',
         'txt_rg_ie' : 'Este campo deve ser informado!',
         'txt_endereco' : 'O endereço deve ser informado!',
         'txt_nascimento_abertura' : 'Este campo deve ser informado!',
         'txt_bairro' : 'O bairro deve ser informado!',
         'txt_numero' : 'Nº deve ser informado!',
         'txt_cep' : 'O cep do município deve ser informado!',
         'txt_municipio_codigo' : 'O código do município deve ser informado!',
         'txt_usuario' : 'O nome de usuário deve ser informado!',
         'txt_senha' : 'A senha deve ser informada!',
         'txt_credito_limite' : 'O limite de crédito deve ser informado!',
         'txt_comissao' : 'A comissão deve ser informada!',
         'txt_descricao' : 'A descrição deve ser informada!',
         'sel_marca' : 'A marca deve ser informada!',
         'sel_categoria' : 'A categoria deve ser informada!',
         'sel_sub_categoria' : 'A sub categoria deve ser informada!',
         'sel_fornecedor' : 'O fornecedor deve ser informado!',
         'txt_estoque' : 'O estoque deve ser informado!',
         'txt_valor_compra' : 'O valor de compra deve ser informado!',
         'txt_lucro_margem' : 'A margem de lucro deve ser informado!',
         'txt_valor_venda' : 'O valor de venda deve ser informado!',
         'txt_desconto_avista' : 'O desconto deve ser informado!',
         'txt_desconto_aprazo' : 'O desconto deve ser informado!',
         'txt_codigo_barras' : 'O código de barras deve ser informado!',
      }
   });
   
   //VALIDAÇÃO DE CPF/CNPJ NO EVENTO SUBMIT DO FORMULÁRIO
   //para retirar os estilos de erro do campo de cpf e cnpj quando ganhar foco
   $('input#txt_cpf_cnpj').focus(function(){
      $(this).removeClass('error');
      $('label.error.cpf-cnpj').remove();
   });
   
   //mensagens de erro pra CPF e CNPJ
   //elas estão em uma label com a classe 'error cpf-cnpj'
   var msgCPF   	= '<label class="error cpf-cnpj">Insira um CPF válido!</label>';
   var msgCNPJ  	= '<label class="error cpf-cnpj">Insira um CNPJ válido!</label>';
   
   var retorno;
   
   //validadando cpf e cnpj -> Formulário Novo Cliente
   $('form#frm_cliente').submit(function(){
		var retorno = valid();
		if(!retorno){ return false; }
		return true;
   });
   
	$('form#frm_fornecedor').submit(function(){
		var retorno = valid();
		if(!retorno){ return false; }
		return true;
	});

  	function valid(){
		if($('select#sel_tipo').val() == 1) {
        	retorno = validarCPF($('input#txt_cpf_cnpj').val());
       		if(!retorno){
        		$('input#txt_cpf_cnpj').attr('class', 'error');
            	$(msgCPF).insertAfter('input#txt_cpf_cnpj');
			}
		 }		  
		 else if($('select#sel_tipo').val() == 2){
			 retorno = validarCNPJ($('input#txt_cpf_cnpj').val());
			 if(!retorno){
				$('input#txt_cpf_cnpj').attr('class', 'error');
				$(msgCNPJ).insertAfter('input#txt_cpf_cnpj');
			 }
		  }
		
		if(retorno){return true;}
		else{return false;}
	};
   
   //validadando cpf -> Formulário Funcionarios
   $('form#frm_funcionario').submit(function(){ 
      
      retorno = validarCPF($('input#txt_cpf_cnpj').val());
      if(!retorno){
         $('input#txt_cpf_cnpj').attr('class', 'error');
         $(msgCPF).insertAfter('input#txt_cpf_cnpj');
      }
      
      if(retorno) { return true; }
      return false;
      
   });
    
});