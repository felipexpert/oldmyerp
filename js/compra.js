
//PEDIDO
$(document).ready(function(){
						   
	//BUSCAR DADOS DO PRODUTO
	var urlDestino = 'filtro_ajax.php';
	$('#txt_produto_codigo').blur(function(){
							   
		var id = $(this).attr('value');
		$.get(urlDestino, {busca_produto:id}, function(theXML){
			$('dados',theXML).each(function(){
				var produto_id 		= $(this).find("produto_id").text();
				var produto 		= $(this).find("produto").text();
				var valor 			= $(this).find("valor").text();
				var desconto 		= $(this).find("desconto").text();
				var valor_compra 	= $(this).find("valor_compra").text();
				var UnidadeMedida 	= $(this).find("unidade_medida").text();
				
				//exibe o valor
				$('#hid_produto_id').val(produto_id);
				$('#txt_produto_nome').val(unescape(produto));
				$('#txt_produto_valor').val(float2br(valor));
				$('#txt_produto_valor_compra').val(valor_compra);
				$('#txt_produto_quantidade').val(1);
				$('#hid_produto_UM').val(UnidadeMedida);
				$('#txt_produto_desconto_compra').val(float2br(desconto));
				
				if(produto !=''){
					$("#txt_produto_nome").attr({
						"readonly":""
					});
				}
			});
		});

	});
	
	/*----------------------------------------------------------------------------------------------*/
	//INSERIR PRODUTO NA COMPRA 
	$('#btn_item_inserir_compra').click(function(event){
		event.preventDefault(); 
		
		var compraDecimal = $("#hid_compra_decimal").val();
		if($("#txt_produto_nome").val()!=""){ 
			//o parametro true em clone faz com que seja copiado tamb�m os eventos do elemento
			$('#pedido_lista_item:first').clone(true).appendTo('#pedido_lista');
			
			$('.hid_item_produto_id:last').val($('#hid_produto_id').val());
			$('.txt_item_codigo:last').val($('#txt_produto_codigo').val());
			$('.txt_item_nome:last').val($('#txt_produto_nome').val());
			$('.txt_item_valor_compra:last').val(float2br(br2float($('#txt_produto_valor_compra').val()).toFixed(compraDecimal)));
			$('.txt_item_quantidade:last').val($('#txt_produto_quantidade').val());
			$('.txt_item_desconto:last').val(float2br(br2float($('#txt_produto_desconto_compra').val()).toFixed(2)));
			$('.txt_item_UM:last').val($('#hid_produto_UM').val());
			$('.txt_item_validade:last').val($('#txt_produto_validade').val());
			$('.txt_item_referencia:last').val($('#txt_produto_referencia').val());
			$('.txt_item_subtotal:last').val($('#txt_item_subtotal').val());
			$('.hid_estoque_id:last').val($('#sel_produto_estoque').val());
			$('.txt_estoque_nome:last').val($('#hid_estoque_nome').val());
			
			//CALCULAR SUBTOTAL DO ITEM
			var fltDesconto = br2float($('#txt_produto_valor_compra').val()) * br2float($('#txt_produto_desconto_compra').val()) / 100;
			var fltSubtotal = (br2float($('#txt_produto_valor_compra').val()) - fltDesconto) * br2float(float2br($('#txt_produto_quantidade').val()));
			$('.txt_item_subtotal:last').val(float2br(fltSubtotal.toFixed(compraDecimal)));
			
			//SOMAR AO SUBTOTAL GERAL
			var fltSubtotalAtual = br2float($('#txt_pedido_subtotal').val());
			$('#txt_pedido_subtotal').val(float2br((fltSubtotalAtual + fltSubtotal).toFixed(2)));
			
			/*----------------------------------------------------------------*/
			
			$('#hid_controle').val(parseInt($('#hid_controle').val()) + 1);
			
			$('.hid_item_produto_id:last').attr('name', $('.hid_item_produto_id:last').attr('class') + "_" + $('#hid_controle').val());
			$('.txt_item_codigo:last').attr('name', $('.txt_item_codigo:last').attr('class') + "_" + $('#hid_controle').val());
			$('.txt_item_nome:last').attr('name', $('.txt_item_nome:last').attr('class') + "_" + $('#hid_controle').val());
			$('.txt_item_valor_compra:last').attr('name', $('.txt_item_valor_compra:last').attr('class') + "_" + $('#hid_controle').val());
			$('.txt_item_quantidade:last').attr('name', $('.txt_item_quantidade:last').attr('class') + "_" + $('#hid_controle').val());
			$('.txt_item_UM:last').attr('name', $('.txt_item_UM:last').attr('class') + "_" + $('#hid_controle').val());
			$('.txt_item_validade:last').attr('name', $('.txt_item_validade:last').attr('class') + "_" + $('#hid_controle').val());
			$('.txt_item_referencia:last').attr('name', $('.txt_item_referencia:last').attr('class') + "_" + $('#hid_controle').val());
			$('.txt_item_desconto:last').attr('name', $('.txt_item_desconto:last').attr('class') + "_" + $('#hid_controle').val());
			$('.hid_estoque_id:last').attr('name', $('.hid_estoque_id:last').attr('class') + "_" + $('#hid_controle').val());
			$('.a_excluir:last').attr('id', $('.a_excluir:last').attr('class') + "_" + $('#hid_controle').val());
	
			$('#txt_produto_codigo').val("");
			$('#txt_produto_nome').val("");
			$('#txt_produto_valor_compra').val("");
			$('#txt_produto_quantidade').val("");
			$('#txt_produto_desconto_compra').val("");
			$('#hid_produto_UM').val("");
			$('#txt_produto_referencia').val("");
			$('#txt_produto_validade').val("");
			$('#txt_item_subtotal').val("");
			
			$('#txt_produto_codigo').focus();
			
			totalCalcular();
		}
	});
	
	
	/*----------------------------------------------------------------------------------------------*/
	//EXCLUIR ITEM
	$('.a_excluir_compra').click(function(event){
		event.preventDefault();

		//subtrai do total o valor do item a excluir
		var fltTotal = br2float($('#txt_pedido_subtotal').val());
		var fltSubTotal = br2float($(this).parents("#pedido_lista_item").find(".txt_item_subtotal").val());

		$('#txt_pedido_subtotal').val(float2br((fltTotal - fltSubTotal).toFixed(2)));

		//remove o parente identificado pela class
		$(this).parents("#pedido_lista_item").remove();
		totalCalcular();
	});	
	
	/*----------------------------------------------------------------------------------------------*/
	//AO TROCAR ESTOQUE
	 $("select[name=sel_produto_estoque]").change(function(){
		var id = $(this).attr('value');		
		var produtoId = $('#hid_produto_id').attr('value');		

		$.get(urlDestino, {produto_estoque_id:id, produtoId:produtoId}, function(theXML){
			$('dados',theXML).each(function(){

				var estoqueNome = $(this).find("estoqueNome").text();
				
				$('#hid_estoque_nome').val(estoqueNome);
				
			});
		});
	})
	
	/*----------------------------------------------------------------------------------------------*/	
	//ZERANDO CAMPOS NULL
	$('#txt_produto_desconto_compra, #txt_produto_valor_compra, #txt_produto_quantidade').change(function(){
		if(br2float($(this).val()) == '' || isNaN(br2float($(this).val()))){
			$(this).attr('value', '0');
		}
	});
	
	/*----------------------------------------------------------------------------------------------*/	
	//V�O TER DOIS CAMPOS DE DESCONTO, EM (%) E EM (R$)... ENT�O S�O ESSES DOIS CAMPOS QUE TEM QUE FUNCIONAR
	$('#txt_pedido_desconto_reais').change(function(){
		//LIMPAR O OUTRO CAMPO DE DESCONTO
		$('#txt_pedido_desconto').val('0');
				
		if(br2float($(this).val()) == '' || isNaN(br2float($(this).val()))){
			$(this).attr('value', '0,00');
		}
		totalCalcular();
	});
	
	$('#txt_pedido_desconto').change(function(){
		//LIMPAR O OUTRO CAMPO DE DESCONTO
		$('#txt_pedido_desconto_reais').val('0');
				
		if(br2float($(this).val()) == '' || isNaN(br2float($(this).val()))){
			$(this).attr('value', '0,00');
		}
		totalCalcular();
	});
	
	/*----------------------------------------------------------------------------------------------*/
	//TRANSPORTE
	$('#txt_pedido_transporte').change(function(){
				
		if(br2float($(this).val()) == '' || isNaN(br2float($(this).val()))){
			$(this).attr('value', '0,00');
		}
		totalCalcular();
	});
	
	/*----------------------------------------------------------------------------------------------*/
	//IPI
	$('#txt_compra_ipi_valor').change(function(){
				
		if(br2float($(this).val()) == '' || isNaN(br2float($(this).val()))){
			$(this).attr('value', '0,00');
		}
		totalCalcular();
	});
	
	$('#txt_compra_st_valor').change(function(){
				
		if(br2float($(this).val()) == '' || isNaN(br2float($(this).val()))){
			$(this).attr('value', '0,00');
		}
		totalCalcular();
	});
	/*----------------------------------------------------------------------------------------------*/
	
	//CALCULAR O TOTAL
	function totalCalcular(){
		valorTotalCompra 			= br2float($('#txt_pedido_subtotal').val());
		valorTransporte 			= br2float($('#txt_pedido_transporte').val());
		valorIPI 					= br2float($('#txt_compra_ipi_valor').val());
		valorST 					= br2float($('#txt_compra_st_valor').val());
		valorDesconto_moeda	 		= br2float($('#txt_pedido_desconto_reais').val());
		valorDesconto_percentual	= br2float($('#txt_pedido_desconto').val());

		valorDesconto_percentual	= valorTotalCompra * valorDesconto_percentual /100;
		valorDesconto 				= valorDesconto_moeda + valorDesconto_percentual;

		$('#txt_pedido_total').val((float2br((((valorTotalCompra - valorDesconto) + valorTransporte) + valorIPI + valorST).toFixed(2))));

		//FORMATAR COM CASAS DECIMAIS
		//FORMATAR COM CASAS DECIMAIS
		$('#txt_pedido_desconto').val(float2br(br2float($('#txt_pedido_desconto').val()).toFixed(2)));
		$('#txt_pedido_desconto_reais').val(float2br(br2float($('#txt_pedido_desconto_reais').val()).toFixed(2)));
		$('#txt_pedido_transporte').val(float2br(br2float($('#txt_pedido_transporte').val()).toFixed(2)));
		$('#txt_compra_ipi_valor').val(float2br(br2float($('#txt_compra_ipi_valor').val()).toFixed(2)));
		$('#txt_compra_st_valor').val(float2br(br2float($('#txt_compra_st_valor').val()).toFixed(2)));
	};
	
	/*----------------------------------------------------------------------------------------------*/
	//VERIFICAR SE HA forneceodr na compra
	$('form#frm_compra_novo input#btn_gravar').click(function(){
		
		if($('#txt_fornecedor_codigo').val() == ''){
			alert("O fornecedor deve ser selecionado!");
			return false;
		}else{
			return true;
		}
	});

	
	/*----------------------------------------------------------------------------------------------*/
	//compra cotacao
	
	//HABILITA O CAMPO DE FORNECEDOR QUANDO RECARREGA A PAGINA COM O PRODUTO SELECIONADO
	if($('table#table_cotacao_item tr.cotacao').attr('class') != undefined){
		var classTr = $('table#table_cotacao_item tr.cotacao').attr('class').split(' ');
	
		if(classTr[2] == 'selected'){
			$("input#txt_fornecedor_codigo").attr({
				"disabled":""
			});
		}
	}
	$('table#table_cotacao_item tr.cotacao').click(function(){
			
		$('table#table_cotacao_item  tr.cotacao').removeClass('selected');
		//HABILITA O FORNECEDOR
		$("input#txt_fornecedor_codigo").attr({
			"disabled":""
		});	
		
		var cotacaoItemId = $(this).attr('id');
		var hidId = $('#hid_cotacao_item_id').attr('value');
		
		//trecho pra verificar qual pagina deve recarregar, novo ou detalhe *************
		var cotacaoId =  $('#hid_cotacao_id').attr('value');
		var urlP = location.search.split("&");//pra saber se esta no novo ou detalhe
		url = urlP[0];
		if(cotacaoId > 0){
			url = urlP[0]+'&id='+cotacaoId;
		}
		//*******************************************************************************
		
		if(cotacaoItemId != hidId){
			$('table#table_cotacao_item tr#' + hidId).removeClass('selected');
		}
		
		$('table#table_cotacao_item tr#' + cotacaoItemId).addClass('selected');
		
		$.post(urlDestino,{compra_cotacao:cotacaoItemId, url:url, cotacaoItemId:cotacaoItemId},function(valor){
			$("table#table_cotacao_fornecedor").html(valor);
		})
		
		$('#hid_cotacao_item_id').val(cotacaoItemId); // aqui armazena o id da cotacao pra cadastro de fornecedor
	});
	
	//GRAVA O VALOR DO FORNECEDOR
	$('input.cotacao_fornecedor_valor').live('blur', function(){
													
		var cotacaoItemId = $(this).attr('name').split('_');
		var valor = $(this).attr('value');
		//$(this).val(float2br(valor)).toFixed(2);
		
		$.post(urlDestino,{cotacao_fornecedor_valor:cotacaoItemId[2], valor:valor},function(valor){})
	});
	
	
	
	
});