
function verificaSiglaPagto(sigla,pagtoId,form) {
	$.get('filtro_ajax.php', {verifica_pagamento_sigla:'true', sigla:sigla, pagtoId:pagtoId}, function(theXML){
		$('dados',theXML).each(function(){
		
			var siglaErro = $(this).find("siglaErro").text();
			if(siglaErro == 1){
				alert('Sigla j� cadastrada');
				return false;
			}else{
				$('#'+form).submit();
			}
		});
	});
}

//PEDIDO
$(document).ready(function(){

	var valorId = $("#sel_sistema_impressao").val();
	switch(valorId){
		case '4':
		case '5':
		case '10':
		case '11':
		case '13':
		break;
		default:
			$("div.hid_impressao_txt").hide();
		break;
	}
	
	$("#sel_sistema_impressao").change(function(){
		valorId = $(this).val();
		
		switch(valorId){
			case '4':
			case '5':
			case '10':
			case '11':
			case '13':
				$("div.hid_impressao_txt").show();
			break;
			default:
				$("div.hid_impressao_txt").hide();
			break;
		}
	});
	
	// ############################################################################################# //
	
	$("#btn_gravar_pagamento_tipo").live('click',function(){
		
		pagtoId	= $('#hid_id').val();
		sigla	= $('#txt_pagamento_sigla').val();
		form 	= $(this).parents('form:first').attr('id');
		verificaSiglaPagto(sigla,pagtoId,form);
		return false;
	});
	
	/***************************************************************************************************************/

	/**
	 * Configura��es do sistema - Perfis de parcelamento
	 */

	$('input#btn_inserir_pagamento_perfil').live('click', function(e){
		e.preventDefault();
		
		//confirmando se o usu�rio digitou alguma frequencia
		if(!isNaN($('#frm_perfis #txt_perfil_frequencia').val()) && $('#frm_perfis #txt_perfil_frequencia').val() != '') {
			//'-1' por causa da lista usada como refer�ncia para clonar
			var contador = parseInt($('#intervalos ul').length, 10) - 1;
			
			$('div#intervalos div#hidden ul:first').clone(true).appendTo('div#intervalos');
			
			//inserindo os valores do formul�rio
			$('div#intervalos input#txt_frequencia_0:last').val($('#frm_perfis input#txt_perfil_frequencia').val());
			$('div#intervalos select#sel_intervalo_0:last').val($('#frm_perfis select#sel_perfil_intervalo').find('option').filter(':selected').val());
			$('div#intervalos input#txt_parcela_0:last').val(contador);
			
			//inserindo um name diferente para os novos campos
			$('div#intervalos input#txt_frequencia_0:last').attr('name', $('div#intervalos input#txt_frequencia_0:last').attr('class') + '_' + contador);
			$('div#intervalos select#sel_intervalo_0:last').attr('name', $('div#intervalos select#sel_intervalo_0:last').attr('class') + '_' + contador);
			$('div#intervalos input#txt_parcela_0:last').attr('name', $('div#intervalos input#txt_parcela_0:last').attr('class') + '_' + contador);
			
			//inserindo o novo valor para o campo de refer�ncia do contador
			$('#frm_perfis input#hid_controle').val(contador);
			
			//resetando os campos do formul�rio
			$('#frm_perfis input#txt_perfil_frequencia').val('');
			$('#frm_perfis select#sel_perfil_intervalo').val($('#frm_perfis select#sel_perfil_intervalo').find('option:first'));
			
			$('#frm_perfis input#txt_perfil_frequencia').focus();
		}
		else {
			$('#frm_perfis input#txt_perfil_frequencia').focus();
		}
	});
	
	//removendo um intervalo da lista
	$('a.excluir_intervalo_perfil').live('click', function(e) {
		e.preventDefault();
		
		//'-2' por causa da lista oculta usada como refer�ncia para montar o datagrid e do cabe�alho
		var totalIntervalos = parseInt($('div#intervalos').find('ul').length, 10) - 2;
		
		//removendo o intervalo desejado
		$(this).parents('div#intervalos ul').remove();
		
		//recalculando o n�mero de parcelas
		for(x=1; x<=totalIntervalos; x++) {
			$('#intervalos input#txt_parcela_0:eq(' + (x) + ')').val(x);
			
			//renomeando names dos inputs
			$('div#intervalos input#txt_frequencia_0:eq(' + (x) + ')').attr('name', $('div#intervalos input#txt_frequencia_0:eq(' + (x) + ')').attr('class') + '_' + x);
			$('div#intervalos select#sel_intervalo_0:eq(' + (x) + ')').attr('name', $('div#intervalos select#sel_intervalo_0:eq(' + (x) + ')').attr('class') + '_' + x);
			$('div#intervalos input#txt_parcela_0:eq(' + (x) + ')').attr('name', $('div#intervalos input#txt_parcela_0:eq(' + (x) + ')').attr('class') + '_' + x);
		}
		
		//passando o total de intervalos para o controle, assim novos intervalos n�o ficam com as parcelas erradas
		$('#frm_perfis input#hid_controle').val(parseInt($('div#intervalos').find('ul').length, 10) - 2);
	});

});