/*
 *	Funções
 */
function checarCampo(seletor) {
	elemento = $(seletor);
	
	if(isNaN(br2float(elemento.val())) || elemento.val() == '' || elemento.val() < 0) {
		elemento.val('0,00');
		return false;
	}
	
	return true;
}

function juros(id_parcela) {
	var valorParcela = br2float($('td.valor_total_'+id_parcela).html());
	var valorPago	 = br2float($('td.valor_pago_'+id_parcela).html());
	var valorJuros 	 = br2float($('input[name=txt_juros_'+id_parcela+']').val());
	
	if(checarCampo('input[name=txt_juros_'+id_parcela+']') === false) { valorJuros = 0; }
	
	valorJuros = ((valorJuros * valorParcela) / 100).toFixed(2);
	
	return (parseFloat(valorParcela) - parseFloat(valorPago)) + parseFloat(valorJuros);
}

function multa(id_parcela) {
	var valorMulta = br2float($('input[name=txt_multa_'+id_parcela+']').val());
	
	if(checarCampo('input[name=txt_multa_'+id_parcela+']') === false) { valorMulta = 0; }
	return parseFloat(valorMulta);
}

function acrescimo(id_parcela) {
	var valorAcrescimo = br2float($('input[name=txt_acrescimo_'+id_parcela+']').val());
	
	if(checarCampo('input[name=txt_acrescimo_'+id_parcela+']') === false) { valorAcrescimo = 0; }
	return parseFloat(valorAcrescimo);
}

function desconto(id_parcela) {
	var valorDesconto = br2float($('input[name=txt_desconto_'+id_parcela+']').val());
	
	if(checarCampo('input[name=txt_desconto_'+id_parcela+']') === false) { valorDesconto = 0; }
	return parseFloat(valorDesconto);
}

$(document).ready(function(){
	//PARCELAS ######################################################################################################
	
	//modificando o juros
	$("input[name^='txt_juros_']").change(function() {
		
		var id = ($(this).attr("name")).substring(10);
		
		if(juros(id) === false) { return false }
		
		valorBaixaParcela = juros(id) + multa(id) + acrescimo(id) - desconto(id);
		$('input[name=txt_valor_baixa_'+id+']').val(float2br(valorBaixaParcela.toFixed(2)));
		
		$(this).val(float2br(br2float($(this).val()).toFixed(2)));
		return true;
		
	});
	
	//modificando a multa
	$("input[name^='txt_multa_']").change(function() {
		
		var id = ($(this).attr("name")).substring(10);
		
		if(multa(id) === false) { return false }
		
		valorBaixaParcela = juros(id) + multa(id) + acrescimo(id) - desconto(id);
		$('input[name=txt_valor_baixa_'+id+']').val(float2br(valorBaixaParcela.toFixed(2)));
		
		$(this).val(float2br(br2float($(this).val()).toFixed(2)));
		
		return true;
		
	});
	
	//modificando o acréscimo
	$("input[name^='txt_acrescimo_']").change(function() {
		
		var id = ($(this).attr("name")).substring(14);
		
		if(acrescimo(id) === false) { return false }
		
		valorBaixaParcela = juros(id) + multa(id) + acrescimo(id) - desconto(id);
		$('input[name=txt_valor_baixa_'+id+']').val(float2br(valorBaixaParcela.toFixed(2)));
		
		$(this).val(float2br(br2float($(this).val()).toFixed(2)));
		
		return true;
		
	});
	
	//modificando o desconto
	$("input[name^='txt_desconto_']").change(function(){
		
		var id 					  = ($(this).attr("name")).substring(13);
		var valorTotal    		  = juros(id) + multa(id) + acrescimo(id);
		var valorDesconto 		  = desconto(id);
		var valorTotalComDesconto = valorTotal - valorDesconto;
		
		if(valorDesconto > valorTotal) {
			$(this).val('0,00');
			$("input[name^='txt_valor_baixa_"+id+"']").val(float2br(valorTotal.toFixed(2)));
		}
		else {
			$("input[name^='txt_valor_baixa_"+id+"']").val(float2br(valorTotalComDesconto.toFixed(2)));
		}
		
		$(this).val(float2br(br2float($(this).val()).toFixed(2)));
		
	});
	
	//modificando o valor da baixa
	$("input[name^='txt_valor_baixa_']").change(function(){
		
		var id = ($(this).attr("name")).substring(16);
		var valorBaixaParcela = juros(id) + multa(id) + acrescimo(id) - desconto(id);
		
		if(parseFloat($(this).val()) <= 0 || checarCampo('input[name=txt_valor_baixa_'+id+']') === false) {
			$('input[name=txt_valor_baixa_'+id+']').val(float2br(valorBaixaParcela.toFixed(2)));
		}
		
		if(br2float($(this).val()) > valorBaixaParcela) {
			$('input[name=txt_valor_baixa_'+id+']').val(float2br(valorBaixaParcela.toFixed(2)));
		}
		
		$(this).val(float2br(br2float($(this).val()).toFixed(2)));
		return true;
	
	});
	
	//######################################################################################################
	//VERIFICA SE NAO ESTA TENTANDO DAR BAIXA EM PARCELAS TOTALMENTE PAGAS
	/*$("#frm_fornecedor_parcela #btn_baixa").click(function(){
		var checkboxs = document.getElementsByTagName("INPUT");// varre todas as inputs conferindo se é um checkbox e se está checked
		for (loop = 0; loop < checkboxs.length; loop++){
			var item = checkboxs[loop];
			if (item.type == "checkbox" && item.checked && $(item).attr('class') == 'pago'){            
				alert('Uma ou mais parcelas selecionadas já está paga!');
				return false;
			}
		}
	});*/
	
	/*
	 *	Melhorado a verificação de tentativas de baixas em parcelas totalmente pagas
	 *	Agora, mesmo selecionando uma parcela paga o processo segue em frente, mas apenas levando as parcelas não pagas
	 *	Se por acaso apenas parcelas pagas forem selecionadas, o usuário recebe uma mensagem alertando e o processo para
	 */
	if($('#frm_fornecedor_parcela').length) {
		$('#frm_fornecedor_parcela #btn_baixa').click(function() {
		    
			//checar antes de tudo se o usuário selecionou alguma parcela para dar a baixa
			if($('input:checked:not(#chk_todos)').length == 0) {
				alert('Nenhuma parcela foi selecionada para dar baixa!');
				return false;
			}
			
			var totalRegSelecionado = 0;
			var totalRegPago 		= 0;
			
			$('input:checked:not(#chk_todos)').each(function() {
				if($(this).attr('class') == 'pago') {
					totalRegPago++;
					$(this).attr('checked', false);
				}
				
				totalRegSelecionado++;
			});
			
			if(totalRegPago == totalRegSelecionado) {
				if(totalRegPago > 1) alertMsg = 'As parcelas selecionadas já estão pagas!';
				else alertMsg = 'A parcela selecionada já está paga!';
				
				alert(alertMsg);
				return false;
			}
			else{
				return true;	
			}
		   
		});
	}
	
});