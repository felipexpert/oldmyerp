
function clienteBusca(){
	telefone	= $('#txt_telefone').val();
	if(telefone != ''){
		$.get("modal/bina_chamada_busca.php", {table_action:'cliente', telefone:telefone}, function(valor){
			$("table#table_bina_listar tbody").html(valor);
			
			cliente_id = $("table#table_bina_listar tbody tr:first").attr('id');
			
			$($('#hid_cliente_id').val(cliente_id)).load(function(){
			 },telefoneBusca());
			$("table#table_bina_listar tbody tr:first").addClass('selected');
		});
	}
};

function formDisable(){
	$('#frm_chamada_bina input:not(:#txt_telefone)').attr({"readonly":""});
	$('#frm_chamada_bina select').attr({"disabled":""});
	$('#txt_nome').focus();
}

function formClear(){
	$('#frm_chamada_bina input:not(:[name^=btn_], :[name=txt_telefone])').val('');
}

function tableClear(){
	$('#table_bina_listar tr').remove();
}

function telefoneBusca(){

	$('#frm_chamada_bina input:not(:#txt_telefone)').attr({"readonly":"readonly"});
	$('#frm_chamada_bina select').attr({"disabled":"disabled"});
	
	cliente_id	= $('#hid_cliente_id').val();
	if(cliente_id > 0){
		$.post("modal/bina_chamada_busca.php", {table_action:'telefone', cliente_id:cliente_id}, function(theXML){
			$('dados',theXML).each(function(){
			
				var cliente_id		= $(this).find("cliente_id").text();
				var codigo 			= $(this).find("Codigo").text();
				var cadastro	 	= $(this).find("Cadastro").text();
				var nome 			= $(this).find("Nome").text();
				var nomefantasia	= $(this).find("NomeFantasia").text();
				var genero		 	= $(this).find("Genero").text();
				var pessoaTipo 		= $(this).find("Tipo").text();
				var cpfCnpj		 	= $(this).find("CPF_CNPJ").text();
				var RG		 		= $(this).find("RG").text();
				var telefone1 		= $(this).find("Fone1").text();
				var telefone2	 	= $(this).find("Fone2").text();
				var codEndereco		= $(this).find("Endereco_Cod").text();
				var endereco	 	= $(this).find("Endereco").text();
				var numero		 	= $(this).find("Numero").text();
				var complemento 	= $(this).find("Complemento").text();
				var bairro	 		= $(this).find("Bairro").text();
				var cep				= $(this).find("Cep").text();
				var codMunicipio	= $(this).find("Municipio_Cod").text();
				var municipio	 	= $(this).find("Municipio").text();
				var uf			 	= $(this).find("UF").text();
			
				$('#txt_codigo').val(codigo);	
				$('#hid_ciente_id').val(cliente_id);	
				$('#txt_cadastro').val(cadastro);	
				$('#txt_nome').val(nome);	
				$('#txt_nome_fantasia').val(nomefantasia);	
				$('#sel_genero').val(genero);	
				$('#sel_tipo').val(pessoaTipo);	
				$('#txt_cpf_cnpj').val(cpfCnpj);	
				$('#txt_rg_ie').val(RG);	
				$('#txt_telefone1').val(telefone1);	
				$('#txt_telefone2').val(telefone2);	
				$('#txt_endereco_codigo').val(codEndereco);	
				$('#txt_endereco').val(endereco);	
				$('#txt_numero').val(numero);	
				$('#txt_complemento').val(complemento);	
				$('#txt_bairro').val(bairro);	
				$('#txt_cep').val(cep);	
				$('#txt_municipio_codigo').val(codMunicipio);		
				$('#txt_municipio').val(municipio);	
				$('#txt_uf').val(uf);	
				
			});
		});	
	}else{
		confirma = confirm('Telefone não encontrado. Deseja fazer novo cadastro?');
		if(confirma){
			formClear();
			formDisable();
			$('#txt_telefone1').val($('#txt_telefone').val());
		}
	}
};

/*
$(document).ready(function() {
12.$("input[type=button]").click(function(event) {
13.$("#box").load('links.html a');
14.});
15.});
*/

$(document).ready(function() {
	$('#frm_chamada_bina #btn_localizar').live('click', function(event){
		event.preventDefault();
		if($('#txt_telefone').val() != ''){																 
			formClear();
			tableClear();
			clienteBusca();
		}
	});
	
	$('#table_bina_listar tr').live('click', function(event){
		cliente_id = $(this).attr('id');
		$('#hid_cliente_id').val(cliente_id);
		telefoneBusca();
		
		$('#table_bina_listar tr').removeClass('selected');
		$(this).addClass('selected');
	});
	
	$('#frm_chamada_bina #btn_editar').live('click', function(event){
		formDisable();
		return false;
	});
	
	$('#frm_chamada_bina #btn_novo').live('click', function(event){
		formClear();
		formDisable();
		$('#txt_codigo').val('auto');
		var currentDate = dateFormat(new Date(), "dd/mm/yyyy");
		$('#txt_cadastro').val(currentDate);
		
		return false;
	});
});	