
$(document).ready(function(){
	
	//FUN��ES DE ENVIO A VISTA ----------------------------------------------------------------------------------------------/
	function checkFinalizarDisabled(event){
		var disabled = $('#hid_disabled').val();
		if(disabled == 1){
		  alert('J� existe um pagamento efetuado para esta venda. N�o ser� poss�vel salvar altera��es');
		  return false;
		}
	}
	
	function checkFinalizarItemAvista(event){
		if(checkFinalizarDisabled() != false){
			if(verificaEstoque() != false ){
				if($('#hid_controle').val() == '0'){
					alert("Insira os itens para esta venda!");
					return false;
				}else{
					anchor = $('#btn_finalizar');
					winModal(anchor);
				}
			}
		}
	}
	
	function checkFinalizarVendaAvista(event){
		
		totalPedido = br2float($('#txt_pedido_total').val());
		hidRecebido	= br2float($('#hid_total_recebido').val());
	
		if(hidRecebido < totalPedido){
			if($('#hid_cliente_id').val() == 0){
				alert('Voc� deve selecionar um cliente para marcar o valor pendente!');
				$('#txt_cliente_codigo').focus();
			}else{
				$('#frm_pedido_pagamento').submit();
			}
		}else{
			$('#frm_pedido_pagamento').submit();
		}
	}
	
	///FUNCOES A PRAZO ----------------------------------------------------------------------------------------------/
	function checkFinalizarItemAprazo(event){
		if(checkFinalizarDisabled() != false){
			if(verificaEstoque() != false ){
				if($('#hid_controle').val() == '0'){
					alert("Insira os itens para esta venda!");
					return false;
				}else{
					anchor = $('#btn_finalizar_prazo');
					winModal(anchor);
				}
			}
		}
	}
	
	function checkFinalizarVendaAprazo(event){
		hidControle	= $('#hid_controle_parcela').val();
		
		if(checkfinalizarParcelas() == false){
			return false;
		}
			
		else if($('#hid_cliente_id').val() == 0){
			alert('Voc� deve selecionar um cliente para marcar o valor pendente!');
			$('#txt_cliente_codigo').focus();
		}else{
			$('#frm_pedido_pagamento_parcela').submit();
		}
	}
	
	//----------------------------------------------------------------------------------------------//
	function cancelarVenda(event){
		var pedido_id = $('#btn_cancelar').attr('href');
		$.post("pedido_rapido_cancela.php", {action:'cancel', pedido_id:pedido_id}, function(theXML){
			$('dados',theXML).each(function(){
				var comanda = $(this).find("comanda").text();
				if(comanda > 0 ){
					window.location="index.php?p=pedido&modo=comanda"; 
				}else{
					window.location="index.php?p=pedido&modo=rapido_novo"; 
				}
				
			});
		});
	}
			
			
	function fncImprimir(event){
		var impressao	= $('#btn_imprimir').attr('href');
		var pedido_id	= $('#txt_codigo').val();
		var raiz		= "rapido_novo"; 
		
		if($('#txt_comanda').length > 0){
			var raiz	= "comanda";
		}
		

			
		var URL = 'pedido_imprimir_'+impressao+'.php?id='+pedido_id+'&raiz='+raiz;
		//SE FOR A4, ABRE EM OUTRA ABA
		if(impressao.substr(0,2) == 'A4'){
			window.open(URL,'_blank');
		}else{
			window.open(URL, 'janela', 'width=200, height=200, scrollbars=no, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no');
		}
		window.location="index.php?p=pedido&modo="+raiz;
		
	}
	
	//----------------------------------------------------------------------------------------------//
	//colocar quantidade 	
	$('#txt_produto_codigo').focus(function(event) {
		if(($('#txt_produto_quantidade').attr('value')) == ''){
			$('#txt_produto_quantidade').val(1);
		}
   	});
	
	//----------------------------------------------------------------------------------------------//
	$("#btn_finalizar").focus(function() {
		$("#btn_finalizar").css('border', '2px solid #C00');
	});
	
	$("#btn_finalizar").blur(function() {
		$("#btn_finalizar").css('border', '2px solid #D7D7D7');
	});
	/*----------------------------------------------------------------------------------------------*/
	//INTERA��O POR TECLADO NOS ITENS DO PEDIDO
	$("#txt_produto_codigo, #txt_produto_quantidade").live('keypress',function(event){

		//console.log(this);
		//alert(event.keyCode);
		switch(event.keyCode){
			case 13: //enter
				if($(this).attr('value') == ''){
					$("#btn_finalizar").focus();
				}
				break;
			case 38: //up
				//MOVER CURSOR
				var cursorAtual = parseInt($('#hid_cursor').val());
				var cursorLimite = 1;
				if(cursorAtual > cursorLimite){
					$('tr[title=pedido_lista_item]').removeClass('cursor');
					cursorNovo = cursorAtual - 1;
					$('tr[title=pedido_lista_item]:eq('+cursorNovo+')').addClass('cursor');
					$('#hid_cursor').val(cursorNovo);
				}
				break;
			case 40: //down
				//MOVER CURSOR
				var cursorAtual = parseInt($('#hid_cursor').val());
				var cursorLimite = parseInt($('#hid_controle').val());
				if(cursorAtual < cursorLimite){
					$('tr[title=pedido_lista_item]').removeClass('cursor');
					cursorNovo = cursorAtual + 1;
					$('tr[title=pedido_lista_item]:eq('+cursorNovo+')').addClass('cursor');
					$('#hid_cursor').val(cursorNovo);
				}
				break;
			case 46: //delete
				//subtrai do total o valor do item a excluir
				
				var fltTotal 		= br2float($('#txt_pedido_subtotal').val());
				var fltSubTotal		= br2float($('tr[title=pedido_lista_item].cursor').find(".txt_item_subtotal").val());
				
				var detalheControle = $('tr[title=pedido_lista_item].cursor').find(".hid_item_detalhe").val(); // pra verificar se item ja estava na venda
				var estoqueId 		= $('tr[title=pedido_lista_item].cursor').find(".hid_item_estoque_id").val(); 
				var fltProdutoId	= $('tr[title=pedido_lista_item].cursor').find(".hid_item_produto_id").val(); 
				var fltItemQtd		= $('tr[title=pedido_lista_item].cursor').find(".txt_item_quantidade").val(); 
				var controle 		= $('#hid_calculo_estoque_controle').val();
				
				if(detalheControle == 1){
					n = 0;
					var x = 0;
					
					while(n <= controle){
						var hidProdutoId 	= $('input[name=hid_controle_item_produto_id_'+n+']').attr('value'); //qual id do item que esta listado no momento
						var hidItemQtd 		= parseInt($('input[name=hid_controle_item_estoque_'+n+']').attr('value'));//quantidade
						var hidEstoqueId 	= parseInt($('input[name=hid_controle_item_estoque_id_'+n+']').attr('value'));//quantidade
						
						if(hidProdutoId == fltProdutoId && hidEstoqueId == estoqueId){
							calculoEstoque = hidItemQtd + fltItemQtd;
							$('input[name=hid_controle_item_estoque_'+n+']').val(calculoEstoque);					
							var x = 1;
						}
						n++;
					}
					if(x == 0){
						$('.item_estoque:first').clone(true).appendTo('#hid_item');
						
						$('.hid_controle_item_produto_id:last').val(fltProdutoId);
						$('.hid_controle_item_estoque:last').val(fltItemQtd);
						$('.hid_controle_item_estoque_id:last').val(estoqueId);
						
						var n = parseInt(controle) + 1;
						$('.hid_controle_item_estoque:last').attr('name', $('#hid_controle_item_estoque:last').attr('class') + "_" + n);
						$('.hid_controle_item_estoque_id:last').attr('name', $('#hid_controle_item_estoque_id:last').attr('class') + "_" + n);
						$('#hid_calculo_estoque_controle').val(n);
					}
				}
				
				$('#txt_pedido_subtotal').val(float2br((fltTotal - fltSubTotal).toFixed(2)));
				$('#txt_pedido_total').val(float2br((fltTotal - fltSubTotal).toFixed(2))); //venda rapida
				
				var pedido_comissao 	 = br2float(ifNumberNull($('#txt_pedido_comissao').val()));
				var funcionario_comissao = $('#hid_funcionario_comissao').val();
				
				comissao = (funcionario_comissao / 100) * fltSubTotal;
				$('#txt_pedido_comissao').val(float2br((pedido_comissao - comissao).toFixed(2))); //venda rapida
				
				//REMOVER ITEM POSICIONADO
				$('tr[title=pedido_lista_item].cursor').remove();
				$('#hid_cursor').val(1);
				$('tr[title=pedido_lista_item]:eq(1)').addClass('cursor');
				
				totalCalcularVendaRapida();
				
				break;
		}
	});

	/*----------------------------------------------------------------------------------------------*/
	$('#frm_pedido_rapido_novo #txt_pedido_desconto_reais').change(function(){
		//LIMPAR O OUTRO CAMPO DE DESCONTO
		$('#txt_pedido_desconto').val('0');
				
		if(br2float($(this).val()) == '' || isNaN(br2float($(this).val()))){
			$(this).attr('value', '0,00');
		}
		totalCalcularVendaRapida();
	});
	
	$('#frm_pedido_rapido_novo #txt_pedido_desconto').change(function(){
		//LIMPAR O OUTRO CAMPO DE DESCONTO
		$('#txt_pedido_desconto_reais').val('0');
				
		if(br2float($(this).val()) == '' || isNaN(br2float($(this).val()))){
			$(this).attr('value', '0,00');
		}
		totalCalcularVendaRapida();
	});

	/*AO TROCAR VENDEDOR (GARCOM) ----------------------------------------------------------------------------------------*/
	$('#txt_funcionario_codigo').change(function(){
		//aqui dou um atraso de 8 milisegundos pra poder carregar o hid pra calcular a comissao
		window.setTimeout(totalCalcularVendaRapida, 80);
	});
	
	
	/*----------------------------------------------------------------------------------------------*/
	//MODAL
	//INTERA��O POR TECLADO NOS PAGAMENTOS
	$("#txt_pedido_recebido").live('keypress keydown',function(event){
		console.log(this);
		
		totalPedido 		= br2float($('#txt_pedido_total').val());
		valorRecebido		= br2float($('#txt_pedido_recebido').val());
		hidRecebido			= br2float($('#hid_total_recebido').val());
		somaRecebido		= valorRecebido + hidRecebido;
		
		switch(event.keyCode){
			
			case 13: //enter
				if($(this).val() != '0,00'){
					
					$('#txt_pedido_recebido').val(float2br(br2float($('#txt_pedido_recebido').val()).toFixed(2)));
					pagamentoTipo 	= $('#sel_pedido_pagamento_tipo').val();
					if(somaRecebido > totalPedido){ // verifica se nao esta dando mais do que o total do pedido, pra printar na tela o valor restante s�
						Recebido	= totalPedido - hidRecebido;
					}else{
						Recebido	= valorRecebido;
					}
										
					if(Recebido > '0'){
						//NUMERAR CONTROLE ATUAL
						$('#hid_controle_recebido').val(parseInt($('#hid_controle_recebido').val()) + 1);
						$('#hid_recebido_cursor').val(parseInt($('#hid_recebido_cursor').val()) + 1);
						
						totalRecebido	= hidRecebido + valorRecebido;
						valorTroco		= (float2br((totalRecebido - totalPedido).toFixed(2)));
						
						$.post("filtro_ajax.php", {sel_pagamento_id:pagamentoTipo}, function(theXML){
							$('dados',theXML).each(function(){
							
								var pagamentoTipo = $(this).find("pagamentoTipo").text();
								$('input.txt_recebido_tipo:last').attr('value', pagamentoTipo);
							});
						});
						
						//copiando o item da lista com inputs e colocando valores
						$('ul.parcela_detalhe:last').clone(true).appendTo('div#parcelas');
						
						//LIMPAR CURSOR DE TODOS OS ITENS
						$('ul.parcela_detalhe').removeClass('cursor');
						//MARCAR CURSOR DO ITEM ATUAL
						$('ul.parcela_detalhe:last').addClass('cursor');
						$('input.txt_recebido_valor:last').attr('value', (float2br((Recebido).toFixed(2))));
						$('input.txt_recebido_valor:last').attr('name', 'txt_recebido_valor_' + $('#hid_controle_recebido').val());
						$('input.txt_recebido_valor:last').attr('id', 	'txt_recebido_valor_' + $('#hid_controle_recebido').val());
						$('input.txt_recebido_tipo:last').attr('name', 	'txt_recebido_tipo_' + $('#hid_controle_recebido').val());
						$('input.txt_recebido_tipo:last').attr('id', 	'txt_recebido_tipo_' + $('#hid_controle_recebido').val());
			
						$('input#hid_total_recebido').attr('value', (float2br((hidRecebido + Recebido).toFixed(2))));
						$('input#txt_pedido_troco').attr('value', valorTroco);
					}
					
					$(this).val('0,00');
					if(br2float(valorTroco) >= 0){
						$('#btn_gravar').focus(); 
						return false; //pra nao sair gravando o submit
					}else{
						$('#sel_pedido_pagamento_tipo').focus();
					}
				}else{
					$('#btn_gravar').focus(); 
					return false; //pra nao sair gravando o submit
				}
				break;
				
			case 38: //up
				//MOVER CURSOR
				var cursorAtual = parseInt($('#hid_recebido_cursor').val());
				var cursorLimite = 1;
				if(cursorAtual > cursorLimite){
					$('ul[title=recebido_lista_item]').removeClass('cursor');
					cursorNovo = cursorAtual - 1;
					$('ul[title=recebido_lista_item]:eq('+cursorNovo+')').addClass('cursor');
					$('#hid_recebido_cursor').val(cursorNovo);
				}
				break;
			case 40: //down
				//MOVER CURSOR
				var cursorAtual = parseInt($('#hid_recebido_cursor').val());
				var cursorLimite = parseInt($('#hid_controle_recebido').val());
				if(cursorAtual < cursorLimite){
					$('ul[title=recebido_lista_item]').removeClass('cursor');
					cursorNovo = cursorAtual + 1;
					$('ul[title=recebido_lista_item]:eq('+cursorNovo+')').addClass('cursor');
					$('#hid_recebido_cursor').val(cursorNovo);
				}
				break;
			case 46: //delete
				
				if($('#hid_recebido_cursor').val() > 0){
					//altera o troco com o valor do item a excluir
					var fltTotal 			= br2float($('#txt_pedido_total').val());
					var fltRecebidoTotal	= br2float($('#hid_total_recebido').val());
					//var fltTroco 			= br2float($('#txt_pedido_troco').val());
					var fltPagamento 		= br2float($('ul[title=recebido_lista_item].cursor').find(".txt_recebido_valor").val());
					
					$('#hid_total_recebido').val(fltRecebidoTotal - fltPagamento);
					$('#txt_pedido_troco').val(float2br((fltRecebidoTotal - fltTotal - fltPagamento).toFixed(2)));
					
					//REMOVER ITEM POSICIONADO
					$('ul[title=recebido_lista_item].cursor').remove();
					$('#hid_recebido_cursor').val(1);
					$('ul[title=recebido_lista_item]:eq(1)').addClass('cursor');
					//subtrair controle
					$('#hid_controle_recebido').val($('#hid_controle_recebido').val() - 1);
					
					//totalCalcular();
				}
				break;
		}
	});
/*----------------------------------------------------------------------------------------------*/	
	//atalhos gerais
	//qualquer local do arquivo/modal
	$(document).keydown(function(event){
								 
		switch(event.keyCode){
			case 115:
				if($('.modal-body').length > 0){
					//pular para campo de cliente [F4] => MODAL
					$('#txt_cliente_codigo').focus();	
				}else{
					fncImprimir();
				}
				break;
			case 117: //pular para quantidade [F6]
					event.preventDefault();
					$('#txt_produto_quantidade').focus();	
				break;
			case 118: //pular para campo de produto [F7]
					event.preventDefault();
					$('#txt_produto_codigo').focus();	
				break;
			case 119: //pular para campo de desconto [F8]
					$('#txt_pedido_desconto_reais').focus();	
				break;
				
			case 120: // [F9]
				if($('.janela-modal').length > 0){
					event.preventDefault();
					$('#hid_print').val(0);
					$('#hid_ecf').val(1);
					if($('#frm_pedido_pagamento').length > 0){ //verifica se esta mandando pelo modal de pagamento a vista
						if($('#txt_cliente_codigo').is(':focus') == false){ //verifica se nao esta no campo de selecionar o cliente
							checkFinalizarVendaAvista();
						}
					}else if($('#frm_pedido_pagamento_parcela').length > 0){ //verifica se esta mandando pelo modal de pagamento a prazo
						if($('#txt_cliente_codigo').is(':focus') == false){ //verifica se nao esta no campo de selecionar o cliente
							checkFinalizarVendaAprazo();
						}
					}
				}
				break;
			
			case 121: //finaliza pedido [F10]
			
					$('#hid_ecf').val(0);
					$('#hid_print').val(0);
					if($('#frm_pedido_pagamento').length > 0){ //verifica se esta mandando o formulario da venda ou do modal de pagamento
						checkFinalizarVendaAvista();
					}else if($('#frm_pedido_pagamento_parcela').length > 0){ //verifica se esta mandando pelo modal de pagamento a prazo
						checkFinalizarVendaAprazo();
					}else{
						checkFinalizarItemAvista();
					}
					event.preventDefault();// o F10 serve para jogar o foco no menu do navegador, entao deixa esse prevent para mesmo depois da a��o no sistema nao deixar fazer essa acao
				break;
			case 122: //finalizar e imprimir pedido [F11] 
					event.preventDefault();
					$('#hid_ecf').val(0);
					$('#hid_print').val(1);
					if($('#frm_pedido_pagamento').length > 0){ //verifica se esta mandando o formulario da venda ou do modal de pagamento
						checkFinalizarVendaAvista();
					}else if($('#frm_pedido_pagamento_parcela').length > 0){ //verifica se esta mandando pelo modal de pagamento a prazo
						checkFinalizarVendaAprazo();
					}else{
						checkFinalizarItemAprazo();
					}
				break;
			case 123: //cancela [F12]
					event.preventDefault();
					if(confirm('Deseja cancelar esta venda?')){
						if($('#frm_pedido_pagamento').length > 0){ //verifica se esta mandando o formulario da venda ou do modal de pagamento
							$("div.modal-body:last").remove();
							$("#btn_finalizar").focus();
						}else{
							cancelarVenda();
						}
					}
				break;
		}
	});

	 
	/*----------------------------------------------------------------------------------------------*/
	//AQUI VAO AS ACOES PRO CASO DO USUARIO CLICAR NOS BOTOES AO INVES DE Fx
	//ABRIR PAGAMENTO
	$("#frm_pedido_rapido_novo #btn_finalizar").click(function(event) {
		event.preventDefault();
		$('#hid_ecf').val(0);
		$('#hid_print').val(0);
		checkFinalizarItemAvista();
	});
	
	$("#frm_pedido_rapido_novo #btn_finalizar_prazo").click(function(event) {
		event.preventDefault();
		$('#hid_ecf').val(0);
		$('#hid_print').val(0);
		checkFinalizarItemAprazo();
	});
	
	
	//FINALIZAR PAGAMENTO
	$("#btn_gravar").live('click', function(event) {
		event.preventDefault();
		$('#hid_ecf').val(0);
		$('#hid_print').val(0);
		if($('#frm_pedido_pagamento').length > 0){ //verifica se esta mandando o formulario da venda ou do modal de pagamento
			checkFinalizarVendaAvista();
		}else if($('#frm_pedido_pagamento_parcela').length > 0){ //verifica se esta mandando pelo modal de pagamento a prazo
			checkFinalizarVendaAprazo();
		}else{
			checkFinalizarItemAvista();
		}
		$('#btn_gravar').die("click");
	});
	
	/*----------------------------------------------------------------------------------------------*/
	//FINALIZAR E IMPRIMIR
	$("#btn_gravar_print").live('click', function(event){
		event.preventDefault();
		$('#hid_ecf').val(0);
		$('#hid_print').val(1);
		if($('#frm_pedido_pagamento').length > 0){ //verifica se esta mandando o formulario da venda ou do modal de pagamento
			checkFinalizarVendaAvista();
		}else if($('#frm_pedido_pagamento_parcela').length > 0){ //verifica se esta mandando pelo modal de pagamento a prazo
			checkFinalizarVendaAprazo();
		}else{
			checkFinalizarItemAprazo();
		}
		$('#btn_gravar_print').die("click");
	});
	
	//FINALIZAR E ECF
	$("#btn_gravar_ecf").live('click', function(event){
		event.preventDefault();
		$('#hid_print').val(0);
		$('#hid_ecf').val(1);
		if($('#frm_pedido_pagamento').length > 0){ //verifica se esta mandando o formulario da venda ou do modal de pagamento
			checkFinalizarVendaAvista();
		}else if($('#frm_pedido_pagamento_parcela').length > 0){ //verifica se esta mandando pelo modal de pagamento a prazo
			checkFinalizarVendaAprazo();
		}else{
			checkFinalizarItemAprazo();
		}
		$('#btn_gravar_ecf').die("click");
	});
	
	/*----------------------------------------------------------------------------------------------*/
	//GRAVAR COMANDA
	$("#btn_comanda_gravar").live('click', function(event){
		event.preventDefault();
		$('#frm_pedido_rapido_novo').submit();
	});
	
	/*----------------------------------------------------------------------------------------------*/
	//GRAVAR COMANDA
	$("#btn_cancelar").live('click', function(event){
		event.preventDefault();
		
		if(confirm('Deseja cancelar esta venda?')){
			cancelarVenda();
		}
	});
	
	/*----------------------------------------------------------------------------------------------*/
	//IMPRIMIR
	$("#btn_imprimir").live('click', function(event){
		event.preventDefault();
		fncImprimir();
	});
	
	/*----------------------------------------------------------------------------------------------*/
	//enviando dados
	$('#frm_pedido_pagamento, #frm_pedido_pagamento_parcela, #frm_pedido_rapido_novo').live('submit', function(event){
		var form 	= $(this).serialize();
		var formId 	= $(this).attr('id');
		var checker = $("#txt_check_enviado").val();

		//AGORA FAZ UMA VALIDACAO COM O TXT_CHECK_ENVIADO, AO ENVIAR PELA PRIMEIRA VEZ, ELE LIMPA O INPUT, EVITANDO COM QUE
		//SEJA DUPLICADO OS REGISTROS CASO O USUARIO SEGURAR AS TECLAS DE FINALIZACAO

		if(checker != '') {
			$('#txt_check_enviado').val('');
			$('#frm_pedido_pagamento input[id^=btn_gravar]').attr({'disabled' : 'disabled'});
			console.log(this);
			$.post("pedido_rapido_gravar.php", {action: "gravar_form", form:form, formId:formId}, function(theXML){
				$('dados',theXML).each(function(){
					console.log(this);
					var imprimir 		= $(this).find("imprimir").text();
					var imprimir_ecf 	= $(this).find("imprimir_ecf").text();
					var pedido_id 		= $(this).find("pedido_id").text();
					var impressao 		= $(this).find("impressao").text();
					var erro 			= $(this).find("erro").text();
					var comanda 		= $(this).find("comanda").text();

					if(erro == '0'){
						alert('N�o foi poss�vel efetuar esta venda. Entre em contato com o suporte!');
						
					}else{

						if(comanda > 0){
							var raiz = "comanda"
						}else{
							var raiz = "rapido_novo"
						}
						
						$("#btn_ecf").attr({
							"href": 'ecf_documento,'+pedido_id+','+raiz
						});
						//finalizar e imprimir cupom			
						if(imprimir_ecf == 1){
							anchor = $('#btn_ecf');
							winModal(anchor);
							return false;
						}
						//finalizar e imprimir
						else {
							if(imprimir == 1){

								var URL = 'pedido_imprimir_'+impressao+'.php?id='+pedido_id+'&raiz='+raiz;
								//SE FOR A4, ABRE EM OUTRA ABA
								if(impressao.substr(0,2) == 'A4'){
									window.open(URL,'_blank');
								}else{
									window.open(URL, 'janela', 'width=200, height=200, scrollbars=no, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no');
								}
							}
							window.location="index.php?p=pedido&modo="+raiz;
						}
					}
				});
			})
			return false;
		}
		else{return false;}
	});
	
	//####################################################################################################################################################################################################################################################################################
	
	
});