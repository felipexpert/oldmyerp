var urlDestino = 'filtro_ajax.php';

function calcularTotalizador(){
  var entradas = 0;
  $('.produto_entrada').each(function(){ entradas += parseInt($(this).val()); });
  
  var saidas = 0;
  $('.produto_saida').each(function(){ saidas += parseInt($(this).val()); });
  
  $('#totEntrada').html('Total entradas: '+entradas);
  $('#totSaida').html('Total saidas: '+saidas);
}

$(document).ready(function(){
  $('#txt_produto_codigo').focus();
  
	/*----------------------------------------------------------------------------------------------*/
	//BUSCAR DADOS DO PRODUTO
	//QUANDO RECEBE O FOCO, LIMPA OS CAMPOS EVITANDO PROBLEMAS PORQUE EXISTEM PRODUTOS QUE PODEM NAO TER COMPONENTES
	
	$('form[id^=frm_estoque_lancamento_multiplo] input#txt_produto_codigo').blur(function(){
		codigo 	= $(this).attr('value');

		if(codigo != ''){
			$.get(urlDestino, {busca_produto_simples:codigo}, function(theXML){
				$('dados',theXML).each(function(){
					var produto_id 		= $(this).find("produto_id").text();
					var produto 		= $(this).find("produto").text();
					
					$('#txt_produto_nome').val(produto);
					$('#hid_produto_id').val(produto_id);
					$('form[id^=frm_estoque_lancamento_multiplo] button#btn_item_inserir').trigger('click');
				});
			});
		}
	});
	
	$('form[id^=frm_estoque_lancamento_multiplo] button#btn_item_inserir').click(function(e){
		e.preventDefault();
		
    var metodo = $('#sel_metodo').val();
		var produto_codigo = $('#txt_produto_codigo').val();
		var produto_id = $('#hid_produto_id').val();
		var produto_desc = $('#txt_produto_nome').val();
		var entrada = (metodo == "entrada") ? $('#txt_quantidade').val() : 0;
		var saida = (metodo == "saida") ? $('#txt_quantidade').val() : 0;

    var controle = parseInt($('#txt_controle').val()) + 1;
		
		//#################################################
		if(entrada == "")	{ entrada = 0; }
		if(saida == "") 	{ saida = 0; }
		//#################################################
		
		if(produto_codigo == "" || produto_codigo == 0 || produto_id == "" || produto_id == 0){
			$('#txt_produto_codigo').focus();
			alert('nenhum produto selecionado');
			return false;			
		}
		
		if(entrada == 0 && saida == 0){
			$('#txt_estoque_entrada').focus();
			alert('insira uma entrada ou saida');
			return false;
		}
    
    if($('.produto_id[value='+produto_id+']').length > 0){
      var novaEntrada = parseInt($('.produto_entrada[produto-id='+produto_id+']').val()) + parseInt(entrada);
      var novaSaida = parseInt($('.produto_saida[produto-id='+produto_id+']').val()) + parseInt(saida);
      $('.produto_entrada[produto-id='+produto_id+']').val(novaEntrada);
      $('.produto_saida[produto-id='+produto_id+']').val(novaSaida);
      $('#'+produto_id).find('.listagem_entrada').html(novaEntrada);
      $('#'+produto_id).find('.listagem_saida').html(novaSaida);
      $('#listagem').prepend($('#'+produto_id));
    } else {
      //copia para a listagem
      $('#listagem_hidden').clone().attr('id', produto_id).prependTo('#listagem').css('display', 'block');
      $('.listagem_codigo:first').text(produto_codigo);
      $('.listagem_desc:first').text(produto_desc);
      $('.listagem_entrada:first').text(entrada);
      $('.listagem_saida:first').text(saida);
      $('.deletar_listagem:first').attr('href', controle);

      //copia para o formulario
      $('#form_hidden').clone().appendTo('#dados');

      $('.produto_id:last').val(produto_id);
      $('.produto_entrada:last').val(entrada);
      $('.produto_saida:last').val(saida);

      $('.produto_id:last').attr({'name': 'produtos['+controle+'][id]', 'produto-id' : produto_id});
      $('.produto_entrada:last').attr({'name': 'produtos['+controle+'][entrada]', 'produto-id' : produto_id});
      $('.produto_saida:last').attr({'name': 'produtos['+controle+'][saida]', 'produto-id' : produto_id});

      $('#txt_controle').val(controle);
    }

    //limpa o form
    $('#txt_produto_codigo').val("");
    $('#hid_produto_id').val("");
    $('#txt_produto_nome').val("");
    
    //calcula os totais
    calcularTotalizador();
		
		$('.deletar_listagem').click(function(e){
			e.preventDefault();
			var id = $(this).attr('href');
			$(this).parent().parent().remove();
			$('.produto_id[name="produtos['+id+'][id]"]').remove();
			$('.produto_entrada[name="produtos['+id+'][entrada]"]').remove();
			$('.produto_saida[name="produtos['+id+'][saida]"]').remove();
		});
    
    $('#txt_quantidade').val('1');
    $('#txt_produto_codigo').focus();		
	});
	
});