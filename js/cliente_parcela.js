
$(document).ready(function(){
	
	//somando os valores da baixa
	$('input.valor_baixa').blur(function(){
		
		var baixaTotal = 0.0;
		var id = ($(this).attr("name")).substring(16);
		
		tipoMulta			= $('#hid_multa_tipo').val();
		hidValorBaixa		= $("input[name^='hid_baixa_valor_"+id+"']").val();//valor da parcela - baixas, que sera somado
		txtCampoBaixa 		= br2float($("input[name^='txt_valor_baixa_"+id+"']").val()).toFixed(2);//campo da baixa na tela de parcela
		txtCampoDesconto	= br2float($("input[name^='txt_desconto_"+id+"']").val()).toFixed(2);
		valorMulta 			= ifNumberNull(br2float($("input[name^='txt_multa_"+id+"']").val()).toFixed(2));
		
		valorRecebido 		= $("input[name^='hid_valor_recebido_"+id+"']").val();//valor ja recebido
		valorParcela 		= $("input[name^='hid_valor_parcela_"+id+"']").val();//valor da parcela
		
		var hidValorBaixa 	= Number(hidValorBaixa);
		var txtCampoBaixa 	= Number(txtCampoBaixa);
		var txtCampoDesconto= Number(txtCampoDesconto);
		
		//calcula o juros de acordo com o campo digitado
		txtCampoJuros = br2float($("input[name^='txt_juros_"+id+"']").val()).toFixed(2);
		if(isNaN(txtCampoJuros)){txtCampoJuros=0};
		var txtCampoJuros = Number(txtCampoJuros);
		
		if(tipoMulta == '2'){ //1=reais, 2=porcentagem 
			valorMulta		= ((hidValorBaixa * valorMulta ) / 100).toFixed(2);
		}
			
		valorJuros		  = ((txtCampoJuros * hidValorBaixa) / 100).toFixed(2);
		valorBaixaParcela = parseFloat(hidValorBaixa) + parseFloat(valorJuros) + parseFloat(valorMulta);
		
		//calcula o juros inicial, para voltar o padrao caso o valor digitado pelo usuario seja maior que o permitido
		hidJuros = $("input[name^='hid_juros_"+id+"']").val(); //campo que fica armazenado o juros do php
		var hidJuros = Number(hidJuros);
		
		hidJurosValor	 = ((hidValorBaixa * hidJuros) / 100).toFixed(2);
		valorBaixaPadrao = parseFloat(hidValorBaixa) + parseFloat(hidJurosValor) + parseFloat(valorMulta);
	
		if($("input#hid_juros_tipo").val() == 2){ //se for juros composto
			
			valor_juros = (txtCampoJuros * valorBaixaParcela) / 100;
			valorBaixaParcela = parseFloat(valorBaixaParcela) +  parseFloat(valor_juros);
			valorBaixaParcela = valorBaixaParcela.toFixed(2);
			
			valor_juros = ((hidJuros * valorBaixaPadrao) / 100).toFixed(2);
			valorBaixaPadrao = parseFloat(valorBaixaPadrao) + parseFloat(valor_juros);
			valorBaixaPadrao =  valorBaixaPadrao.toFixed(2) + parseFloat(valorMulta);
			
		}
		
		devedor = (Number(valorBaixaParcela) - Number(txtCampoBaixa)) - Number(txtCampoDesconto);
		//devedor = valorBaixaParcela - txtCampoBaixa - txtCampoDesconto;
		
		//caso o ususario limpe a caixa 
		if($(this).val() == ''){ 
			$(this).val('0,00');
			txtCampoBaixa = '0.00';
			devedor = valorBaixaPadrao;
			$("input[name^='txt_juros_"+id+"']").val(float2br(hidJuros.toFixed(2)));
		}
			
		//verifica se o valor da baixa é menor que o valor total a receber da parcela + juros
		Baixa 		 = txtCampoBaixa + txtCampoDesconto;
		BaixaParcela = parseFloat(valorBaixaParcela).toFixed(2);
		
		if(Baixa > BaixaParcela || Baixa < 0 || isNaN(Baixa) || typeof(Baixa) == 'undefined') {
			alert("O valor digitado não é válido ou é maior que o valor da parcela!");
			valorBaixaPadrao = parseFloat(valorBaixaPadrao);

			$("input[name^='txt_valor_baixa_"+id+"']").val(float2br(valorBaixaPadrao.toFixed(2)));
			
			$("input[name^='txt_juros_"+id+"']").val(float2br(hidJuros.toFixed(2)));
			$("input[name^='txt_multa_"+id+"']").val(float2br($("input[name^='hid_multa_"+id+"']").val().toFixed(2)));
			
			$("input[name^='txt_valor_devedor_"+id+"']").val('0,00');
			$("input[name^='txt_desconto_"+id+"']").val('0,00');
		}
		else if(Baixa == BaixaParcela){
			$(this).val(float2br(br2float($(this).val()).toFixed(2)));
			$("input[name^='txt_valor_devedor_"+id+"']").val('0,00');
		}else if(Baixa < BaixaParcela){
			
			$(this).val(float2br(br2float($(this).val()).toFixed(2)));
			$("input[name^='txt_valor_devedor_"+id+"']").val(float2br(devedor.toFixed(2)));
		}
		
		//faz a conta para aparecer no total de baixa
		$('input.valor_baixa').each(function(){
			if($(this).val() != ''){
				baixaTotal += br2float($(this).val());	
			}
		});
		
		$('#txt_total_baixa').val(float2br(baixaTotal.toFixed(2)));
		
	});
	
	/*----------------------------------------------------------------------------------------------*/
	//CLIENTE - PARCELAS Multa
		$("input[name^='txt_multa_']").change(function(){
													   
			var id = ($(this).attr("name")).substring(10);
			
			tipoMulta		= $('#hid_multa_tipo').val();
			valorMulta 		= ifNumberNull(br2float($(this).val()).toFixed(2));
			valorDesconto 	= ifNumberNull(br2float($("input[name^='txt_desconto_"+id+"']").val()).toFixed(2));
			valorJuros 		= ifNumberNull(br2float($("input[name^='txt_juros_"+id+"']").val()).toFixed(2));
			valorBaixa		= $("input[name^='hid_baixa_valor_"+id+"']").val();
			//#################################################################
			
			if(tipoMulta == '2'){ //1=reais, 2=porcentagem 
				valorMulta		= ((valorBaixa * valorMulta ) / 100).toFixed(2);
			}
			
			valorBaixaJuros		= ((valorBaixa * valorJuros ) / 100).toFixed(2);
			valorBaixaParcela	= Number(valorBaixa) + Number(valorBaixaJuros);
			
			if(($("input#hid_juros_tipo").val() == 2) && (valorJuros > 0.00)){ //se for juros composto
				valor_juros = ((valorJuros * valorBaixaParcela) / 100).toFixed(2);
				valorBaixaParcela = Number(valorBaixaParcela) + Number(valor_juros);
			}
			
			valorBaixaParcela = Number(valorBaixaParcela) + Number(valorMulta);
			
			$("input[name^='txt_valor_baixa_"+id+"']").val(float2br(valorBaixaParcela.toFixed(2)));
			$("input[name^='hid_baixa_valor_calculado_"+id+"']").val(float2br(valorBaixaParcela.toFixed(2)));
			propagarValorBaixa();
			
			if($(this).val() == ''){
				$(this).val('0,00');
			}else{
				$(this).val(float2br(br2float($(this).val()).toFixed(2)));
			}
			
			$("input[name^='txt_valor_devedor_"+id+"']").val('0,00');
			$("input[name^='txt_desconto_"+id+"']").val('0,00');
		});	
		
	//CLIENTE - PARCELAS Desconto
		$("input[name^='txt_desconto_']").change(function(){
			
			var id 			= ($(this).attr("name")).substring(13);
			tipoMulta		= $('#hid_multa_tipo').val();
			valorReceber 	= $("input[name^='hid_baixa_valor_"+id+"']").val();
			valorJuros	 	= br2float($("input[name^='txt_juros_"+id+"']").val()).toFixed(2);
			valorDesconto 	= ifNumberNull(br2float($(this).val()).toFixed(2));
			
			valorMulta 			= ifNumberNull(br2float($("input[name^='txt_multa_"+id+"']").val()).toFixed(2));
			valorBaixa			= $("input[name^='hid_baixa_valor_"+id+"']").val();
			valorBaixaJuros		= ((valorBaixa * valorJuros ) / 100).toFixed(2);
			
			valorBaixaParcela	= Number(valorBaixa) + Number(valorBaixaJuros);
			if(($("input#hid_juros_tipo").val() == 2) && (valorJuros > 0.00)){ //se for juros composto
				valor_juros = ((valorJuros * valorBaixaParcela) / 100).toFixed(2);
				valorBaixaParcela = Number(valorBaixaParcela) + Number(valor_juros);
			}
			
			if(tipoMulta == '2'){ //1=reais, 2=porcentagem 
				valorMulta		= ((valorBaixaParcela * valorMulta ) / 100).toFixed(2);
			}
			valorBaixaParcela = valorBaixaParcela.toFixed(2);
			valorBaixaParcela = (Number(valorBaixaParcela) + Number(valorMulta)) - Number(valorDesconto);
			
			$("input[name^='txt_valor_baixa_"+id+"']").val(float2br(valorBaixaParcela.toFixed(2)));
			$("input[name^='hid_baixa_valor_calculado_"+id+"']").val(float2br(valorBaixaParcela.toFixed(2)));
			propagarValorBaixa();
			
			if(Number(valorDesconto) > Number(valorReceber) || $(this).val() == ''){
				$(this).val("0,00");
			}else{
				
				$(this).val(float2br(br2float($(this).val()).toFixed(2)));
				$("input[name^='txt_valor_devedor_"+id+"']").val('0,00');
			}
			
		});	
		
	//CLIENTE - PARCELAS - CALCULAR JUROS	
		$("input[name^='txt_juros_']").change(function(){
													   
			var id = ($(this).attr("name")).substring(10);
			valorJuros = br2float($(this).val()).toFixed(2);
			if(isNaN(valorJuros)){valorJuros=0};
			
			tipoMulta			= $('#hid_multa_tipo').val();
			valorMulta 			= ifNumberNull(br2float($("input[name^='txt_multa_"+id+"']").val()).toFixed(2));
			valorBaixa			= $("input[name^='hid_baixa_valor_"+id+"']").val();
			valorBaixaJuros		= ((valorBaixa * valorJuros ) / 100).toFixed(2);
			
			valorBaixaParcela	= parseFloat(valorBaixa) + parseFloat(valorBaixaJuros);
			if(tipoMulta == '2'){ //1=reais, 2=porcentagem 
				valorMulta		= ((valorBaixaParcela * valorMulta ) / 100).toFixed(2);
			}
			
			if(($("input#hid_juros_tipo").val() == 2) && (valorJuros > 0.00)){ //se for juros composto
				valor_juros = ((valorJuros * valorBaixaParcela) / 100).toFixed(2);
				valorBaixaParcela = parseFloat(valorBaixaParcela) + parseFloat(valor_juros);	
			}
			
			
			valorBaixaParcela = valorBaixaParcela.toFixed(2);
			valorBaixaParcela = Number(valorBaixaParcela) + Number(valorMulta);
			
			if($(this).val() == ''){
				$(this).val('0,00');
			}else{
				$(this).val(float2br(br2float($(this).val()).toFixed(2)));
			}
			
			$("input[name^='txt_valor_baixa_"+id+"']").val(float2br(valorBaixaParcela.toFixed(2)));
			$("input[name^='hid_baixa_valor_calculado_"+id+"']").val(float2br(valorBaixaParcela.toFixed(2)));
			propagarValorBaixa();
			
			$("input[name^='txt_desconto_"+id+"']").val('0,00');
			$("input[name^='txt_valor_devedor_"+id+"']").val('0,00');
			
			//$('input#txt_pago_total').val('0,00');
			
		});	
	//#############################################################################################################################################

		//VERIFICA SE HA VALOR DE BAIXA
		$("#frm_parcela_editar #btn_baixa").click(function(){	
			if($("input[name^='txt_valor_baixa_']").val() == '0,00'){
				var id = ($(this).attr("name")).substring(16);
				if($("input[name^='txt_desconto_"+id+"']").val() == '0,00'){
					alert('Uma ou mais parcelas não possuem valor de baixa!');
					return false;
				};
			};
		});	
	
		//VERIFICA SE NAO ESTA TENTANDO DAR BAIXA EM PARCELAS TOTALMENTE PAGAS
		/*$("#frm_cliente_parcela #btn_baixa").click(function(){
			var checkboxs = document.getElementsByTagName("INPUT");// varre todas as inputs conferindo se é um checkbox e se está checked
			for (loop = 0; loop < checkboxs.length; loop++){
				var item = checkboxs[loop];
				if (item.type == "checkbox" && item.checked && $(item).attr('class') == 'pago'){            
					alert('Uma ou mais parcelas selecionadas já está paga!');  
					return false;
				}
			}
		});*/
		
		/*
		 *	Melhorado a verificação de tentativas de baixas em parcelas totalmente pagas
		 *	Agora, mesmo selecionando uma parcela paga o processo segue em frente, mas apenas levando as parcelas não pagas
		 *	Se por acaso apenas parcelas pagas forem selecionadas, o usuário recebe uma mensagem alertando e o processo para
		 */
		if($('#frm_cliente_parcela').length) {
			$('#frm_cliente_parcela #btn_baixa').click(function() {
				
				//checar antes de tudo se o usuário selecionou alguma parcela para dar a baixa
				if($('input:checked:not(#chk_todos)').length == 0) {
					alert('Nenhuma parcela foi selecionada para dar baixa!');
					return false;
				}
				
				var totalRegSelecionado = 0;
				var totalRegPago 		= 0;
				
				$('input:checked:not(#chk_todos)').each(function() {
					if($(this).attr('class') == 'pago') {
						totalRegPago++;
						$(this).attr('checked', false);
					}
					
					totalRegSelecionado++;
				});
				
				if(totalRegPago == totalRegSelecionado) {
					if(totalRegPago > 1) alertMsg = 'As parcelas selecionadas já estão pagas!';
					else alertMsg = 'A parcela selecionada já está paga!';
					
					alert(alertMsg);
					return false;
				}
				else{
					return true;	
				}
				
			});
		}
		
	//#############################################################################################################################################
	//Baixa geral
	$("input#txt_total_baixa").change(function(){
		var id 		   = '';
		var id_filtro  = '';
		var totalPago  = br2float($(this).val());
		var valorTotal = 0.00;
		var novoValor  = 0.00;
		var devedor    = 0.00;
		
		//checando se valor digitado é válido
		if(checarCampo($(this)) === false) {
			$('.valor_baixa').each(function() {
				id = ($(this).attr('name')).substring(16);
				$(this).val(float2br($('#hid_baixa_valor_calculado_'+id).val()));
				$('input[name=txt_valor_devedor_'+id+']').val('0,00');
			});
			
			return false;
		}
		
		$('.valor_baixa').each(function() {
			
			id = ($(this).attr('name')).substring(16);
			valorAtual = br2float($('#hid_baixa_valor_calculado_'+id).val());
			
			if(totalPago >= valorAtual) {
				totalPago = totalPago - valorAtual;
				$(this).val($('#hid_baixa_valor_calculado_'+id).val());
			}
			else {
				$(this).val(float2br((totalPago).toFixed(2)));
				totalPago = totalPago - valorAtual;
				
				if(totalPago <= 0) { totalPago = 0; }
			}
			
			devedor = br2float($('#hid_baixa_valor_calculado_'+id).val()) - br2float($(this).val());
			$('input[name=txt_valor_devedor_'+id+']').val(float2br(devedor.toFixed(2)));
			
			//verificando quais id's/parcelas serão dado baixa
			if(br2float($(this).val()) > 0) { id_filtro = id_filtro + id + ', '; }
			
		});
		
		//retirar a última vírgula + espaço
		id_filtro = id_filtro.substring(0, (id_filtro.length - 2));
		
		//substituir o valor de hid_filtro pelos ids que serão realmente dado baixa
		$('#hid_filtro').val(id_filtro);
		
		$(this).val(float2br(br2float($(this).val()).toFixed(2)));
		return true;
	});
	
});

/*
 *	Funções
 */
 
 
function checarCampo(seletor) {
	elemento = $(seletor);
	
	if(isNaN(br2float(elemento.val())) || elemento.val() == '' || elemento.val() <= 0) {
		elemento.val('0,00');
		return false;
	}
	
	return true;
}

function propagarValorBaixa() {
	var baixaTotal = 0;
	
	$('.valor_baixa').each(function() { baixaTotal += br2float($(this).val()); });
	$('input#txt_total_baixa').val(float2br(baixaTotal.toFixed(2)));
}