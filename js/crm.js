/**
 * Scripts relacionados ao módulo de Acompanhamento de Clientes
 */

$(document).ready(function() {
    
    //verificando se no documento atual existe um formulário com o id. Assim é evitado ficar chamando toda hora essa rotina quando fechar o modal.
    if($('form#frm_acompanhamento_chamado_novo').length || $('form#frm_acompanhamento_chamado_detalhe').length) {
        
        $('.atualizar_select').live('click', function(){
            
            $.get('./crm_ajax.php', { carregar_select_agendamento_tipo : true }, function(tipos) {
                
                var htmlDados;
                if(tipos != null) {
                    var totalRegistros = tipos.length;
                    
                    if(totalRegistros > 0) {
                        for(i=0; i<totalRegistros; i++) {
                            htmlDados += '<option value="'+tipos[i].id+'">'+tipos[i].tipo+'</option>';
                        }
                    }
                }
                else {
                    htmlDados = '<option value="0">Cadastre antes um tipo de agendamento</option>';
                }
                
                $('form select#sel_chamado_agendamento_tipo').html(htmlDados);
                
            }, 'json');
            
        });
        
        //verificar se o funcionário já possui compromisso agendado
        $('form#frm_acompanhamento_chamado_novo #btn_gravar, form#frm_acompanhamento_agenda #btn_gravar').live('click', function(e) {
            
            e.preventDefault();
            
            var funcionario   = $(this).parents('form').find('#sel_chamado_agendamento_funcionario').val();
            var data          = $(this).parents('form').find('#txt_chamado_agendamento_data').val();
            var horario       = $(this).parents('form').find('#txt_chamado_agendamento_hora').val() + ':00';
            var agendamentoId = ($(this).parents('form').find('#hid_agendamento_id').length) ? $(this).parents('form').find('#hid_agendamento_id').val() : 0;
            
            $.get('./crm_ajax.php', { checar_funcionario_agenda : true, funcionario_id : funcionario, data_agenda : data, horario_agenda : horario, agendamento_id : agendamentoId }, function(resposta) {
                
                if(resposta.preenchida === true) {
                    alert('O funcionário selecionado já tem um compromisso marcado para esta data e horário!\n\nPor favor escolha outra data ou funcionário.');
                }
                else {
					//envia os dados do frm_acompanhamento_chamado_detalhe junto, pois se usuário estiver editando não haverá perda.
                    $.post('crm_acompanhamento_cliente_detalhe.php', $('#frm_acompanhamento_chamado_detalhe').serialize(), function(){
						$('form#frm_acompanhamento_chamado_novo, form#frm_acompanhamento_agenda').submit();
					});
                }
                
            }, 'json');
        
        });
        
        //verificar se há agendamentos para poder ou não, alterar a situação para "Em espera" ao editar o chamado
        if($('form#frm_acompanhamento_chamado_detalhe').length) {
            
            var objSelect     = $('form#frm_acompanhamento_chamado_detalhe').find('select#sel_chamado_situacao');
            var situacaoAtual = objSelect.val();
            
            objSelect.change(function() {
                
                var chamadoId = $('form#frm_acompanhamento_chamado_detalhe').find('input#hid_chamado_id').val();
                
                if($(this).val() == 1) {
                    $.get('./crm_ajax.php', { checar_situacao_chamado : true, chamado_id : chamadoId }, function(resposta) {
                        
                        if(resposta.mudar_situacao === false) {
                            objSelect.val(situacaoAtual);
                            alert('Você não pode mudar a situação para "Em espera" enquanto houver um ou mais agendamentos!');
                        }
                        
                    }, 'json');
                }
                
            });
            
        }
        
    }
    
    //carregar ou não o fieldset com os campos para agendamento
    if($('fieldset#acompanhamento_agenda').length) {
		
		var objFieldset = $('fieldset#acompanhamento_agenda');
		objFieldset.hide();
        
        $('select#sel_chamado_situacao').change(function(){
            
            var option = $(this).find('option').filter(':selected').val();
            
            if(option > 1) {
                objFieldset.show();
                objFieldset.removeAttr('disabled');
            }
            else{
                objFieldset.hide();
                objFieldset.attr('disabled', 'disabled');
            }
            
        });
		
	}
    
});