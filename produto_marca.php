<?

	require("produto_marca_filtro.php");
	
	//ações em grupo
	if(isset($_POST['hid_action'])){
		require("produto_marca_action.php");
	}
	
	if(isset($_GET['mensagem']) && $_GET['mensagem'] == "ok"){
?>		<div class="alert">
			<p class="ok">Registro gravado com sucesso!<p>
        </div>
<?	}
	

/**************************** ORDER BY *******************************************/
	$filtroOrder  = 'fldNome ';
	$class 		  = 'asc';
	$order_sessao = explode(" ", $_SESSION['order_produto_marca']);
	if(isset($_GET['order'])){
		switch($_GET['order']){
			
			case 'codigo'	:  $filtroOrder = "fldId";   break;
			case 'marca'	:  $filtroOrder = "fldNome"; break;
		}
		if($order_sessao[0] == $filtroOrder){
			$class = ($order_sessao[1] == 'asc') ? 'desc' : 'asc';
		}
	}
	
	//definir icone para ordem
	$_SESSION['order_produto_marca'] = (!$_SESSION['order_produto_marca'] || $_GET['order']) ? $filtroOrder.' '.$class : $_SESSION['order_produto_marca'];
	$pag	= ($_GET['pagina'])? '&pagina='.$_GET['pagina'] : ''; 
	$raiz 	= "index.php?p=produto&modo=marca$pag&amp;order=";
	
	$order_sessao = explode(" ", $_SESSION['order_produto_marca']);
	$filtroOrder  = $order_sessao[0]; //pra poder comparar na listagem e exibir a class
	
/**************************** PAGINAÇÃO *******************************************/
	$sSQL = "select * from tblmarca ". $_SESSION['filtro_marca']." order by " .  $_SESSION['order_produto_marca'];
	$rsTotal = mysql_query($sSQL);
	$rowsTotal = mysql_num_rows($rsTotal);
	
	//definição dos limites
	$limite = 50;
	$n_paginas = 7;
	
	$total_paginas = ceil(mysql_num_rows($rsTotal) / $limite);
	
	if(isset($_GET["pagina"]) && $_GET["pagina"] > $total_paginas){
		$inicio = 0;
	}elseif(isset($_GET['pagina'])){
		$inicio = ($_GET['pagina'] - 1) * $limite;
	}else{
		$inicio = 0;
	}
	
	$sSQL 	.= " limit " . $inicio . "," . $limite;
	$rsMarca = mysql_query($sSQL);
	$pagina  = (isset($_GET['pagina'])) ? $_GET['pagina'] : "1";
	
/*************************************************************************************/		
?>
    <form class="table_form" id="frm_marca" action="" method="post">
    	<div id="table">
            <div id="table_cabecalho">
                <ul class="table_cabecalho">
                    <li style="width:10px">&nbsp;</li>
                    <li class="order" style="width:80px">
                    	<a <?= ($filtroOrder == 'fldId') 	? "class='$class'" : '' ?> style="width:45px" href="<?=$raiz?>codigo">C&oacute;d.</a>
                    </li>
                    <li class="order" style="width:340px">
                    	<a <?= ($filtroOrder == 'fldNome') 	? "class='$class'" : '' ?> style="width:325px" href="<?=$raiz?>marca">Marca</a>
                    </li>
                    <li style="width:320px">Website</li>
                    <li style="width:100px; text-align: center;">Produtos</li>
                    <li style="width:49px">&nbsp;</li>
                    <li style="width:25px"><input type="checkbox" name="chk_todos" id="chk_todos" /></li>
                </ul>
            </div>
            <div id="table_container">       
                <table id="table_general" class="table_general" summary="Lista de marcas">
                <tbody>	
<?					
					$id_array = array();
					$n = 0;
					
					$linha 	= "row";
					$rows 	= mysql_num_rows($rsMarca);
					while($rowMarca = mysql_fetch_array($rsMarca)){
					
						$id_array[$n] = $rowMarca["fldId"];
						$n += 1;

?>							<tr class="<?= $linha; ?>">
<?				 		 		$icon 	= ($rowMarca["fldDisabled"] ? "bg_disable" : "bg_enable");
								$title 	= ($rowMarca["fldDisabled"] ? "desabilitado" : "habilitado");
?>								<td style="width:20px;text-align:center;"><img src="image/layout/<?=$icon?>.gif" alt="status" title="<?=$title?>"/></td>
								<td class="cod" style="width:70px;text-align:right;padding-right:10px"><?=$rowMarca['fldId']?></td>
								<td style="width:340px; text-align:left"><?=$rowMarca['fldNome']?></td>
								<td style="width:320px; text-align:left"><?=$rowMarca['fldWebsite']?></td>
	
<?								$sql 		= mysql_query("select * from tblproduto where fldMarca_Id =".$rowMarca['fldId']);
								$produtos 	= mysql_num_rows($sql);
?>			
								<td style="width:126px; text-align:center;"><?=$produtos?></td>
								<td style="width:auto; text-align:center;"><a class="edit" href="index.php?p=produto&modo=marca_detalhe&amp;id=<?=$rowMarca['fldId']?>" title="editar"></a></td>
								<td style="width:auto;"><input type="checkbox" name="chk_marca_<?=$rowMarca['fldId']?>" id="chk_marca_<?=$rowMarca['fldId']?>" title="selecionar o registro posicionado" /></td>
							</tr>
							
<?							$linha = ($linha == "row") ? "dif-row" : "row";
						}
?>		 			</tbody>	
				</table>
            </div>
       	  
            <input type="hidden" name="hid_array" id="hid_array" value="<?=urlencode(serialize($id_array))?>" />
            <input type="hidden" name="hid_action" id="hid_action" value="true" />
            
			<div id="table_action">
                <ul id="action_button">
                    <li><a class="btn_novo" href="index.php?p=produto&modo=marca_novo">novo</a></li>
                    <li><input type="submit" name="btn_action" id="btn_excluir" 	value="excluir" 	title="Excluir registro(s) selecionado(s)" onclick="return confirm('Deseja excluir os registros selecionados?')" /></li>
                    <li><input type="submit" name="btn_action" id="btn_habilitar" 	value="habilitar" 	title="Habilitar registro(s) selecionado(s)" /></li>
                    <li><input type="submit" name="btn_action" id="btn_desabilitar" value="desabilitar" title="Desabilitar registro(s) selecionado(s)" /></li>
                </ul>
        	</div>
            <div id="table_paginacao">
<?				$paginacao_destino = "?p=marca";
				include("paginacao.php")
?>		
            </div>  
            <div class="table_registro">
            	<span>Exibindo registros <?=($pagina*$limite-$limite+1).' a '.($pagina*$limite-$limite+$rows)?> do total de <?=$rowsTotal?></span>
            </div>      
        </div>
	</form>
