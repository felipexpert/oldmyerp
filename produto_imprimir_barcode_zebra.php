<?php

	ob_start();
	session_start();
    
	require("inc/con_db.php");
	require("inc/fnc_general.php");
	require("inc/fnc_identificacao.php");

	if($_SERVER['REQUEST_METHOD'] == "POST"){
		$p_id = $_POST['id'];
		$qtd  = $_POST['qtd'];
		$tipo = $_POST['tipo'];
		
		$rowProduto = mysql_fetch_assoc(mysql_query("SELECT * FROM tblproduto WHERE fldId = '$p_id'"));
		$dados['produto_nome'] = $rowProduto['fldNome'];

		if($rowProduto['fldCodigoBarras'] != NULL and $rowProduto['fldCodigoBarras'] != ""){

			$timestamp  				= md5(date("Ymd_His").rand(10,100));
			$local_file 				= "impressao///inbox///imprimir_barcode_zebra_$timestamp.txt"; // Definimos o local para salvar o arquivo de texto
			$fp 						= fopen($local_file, "w+"); //utilizamos o operador w+ para criar o arquivo imprimir.txt, e APAGAR tudo que já existe nele, caso ele já exista.
			$comando 					= "";
			$impressao_local 			= fnc_estacao_impressora($_SESSION['remote_name'], '1');
			if(!$impressao_local){ $impressao_local = fnc_estacao_impressora('todos', '1'); }
			$comando 				 	.= $impressao_local."\r\n";
			$comando 				 	.= "\r\n";

			if($tipo == 'pequeno_amaxima'){
				$codBarras 			= $rowProduto['fldCodigoBarras'];
				$nome_produto 		= substr($rowProduto['fldNome'], 0, 50);
				$preco 				= format_number_out($rowProduto['fldValorVenda']);
				$ref 				= $rowProduto['fldDescricao'];

				$codBarras 			= $rowProduto['fldCodigoBarras'];
				$preco 				= "R$".format_number_out($rowProduto['fldValorVenda']);
				$ref 				= $rowProduto['fldDescricao'];
				
				$comando .= "N\r\n";
				$comando .= "q900\r\n";
				$comando .= "Q224,01\r\n";
				$comando .= "R0,0\r\n";
				$comando .= "D15\r\n";
				$comando .= "ZB\r\n";
				$comando .= "A253,220,2,2,1,1,N,\"".$codBarras."\"\r\n";
				$comando .= "B253,200,2,1,2,4,20,N,\"".$codBarras."\"\r\n";
				$comando .= "A253,170,2,2,1,1,N,\"".$preco."\"\r\n";
				$comando .= "P".$qtd;
			}

			if($tipo == 'grande_amaxima'){
				$codBarras 			= $rowProduto['fldCodigoBarras'];
				$preco 				= "R$".format_number_out($rowProduto['fldValorVenda']);
				$ref 				= $rowProduto['fldNome'];
				
				$comando .= "N\r\n";
				$comando .= "q900\r\n";
				$comando .= "Q450,01\r\n";
				$comando .= "R0,0\r\n";
				$comando .= "D15\r\n";
				$comando .= "ZB\r\n";
				
				$comando .= "A750,275,2,2,1,1,N,\"".$ref."\"\r\n";
				$comando .= "B750,255,2,1,2,4,30,N,\"".$codBarras."\"\r\n";
				$comando .= "A750,220,2,2,1,1,N,\"".$codBarras."\"\r\n";
				$comando .= "A750,195,2,2,1,1,N,\"".$preco."\"\r\n";

				$comando .= "A320,275,2,2,1,1,N,\"".$ref."\"\r\n";
				$comando .= "B320,255,2,1,2,4,30,N,\"".$codBarras."\"\r\n";
				$comando .= "A320,220,2,2,1,1,N,\"".$codBarras."\"\r\n";
				$comando .= "A320,195,2,2,1,1,N,\"".$preco."\"\r\n";
				$comando .= "P".$qtd;
			}

			$salva	= fwrite($fp, $comando);
			fclose($fp);
		}
		
		echo json_encode($dados);
		die();
	}
	 
	$sSQL 			= "select * from tblproduto where tblproduto.fldId IN (".str_replace('-', ',', $_GET['ids']).")";
	$rsProduto 		= mysql_query($sSQL);
	$rowsProduto 	= mysql_num_rows($rsProduto);
	$produtos 		= array();
	while($rowProduto = mysql_fetch_array($rsProduto)){ $produtos[] = $rowProduto['fldId']; }

?>

<html>
<head>
	<title>MyERP Etiqueta</title>
	<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
	
	<style>
		body { padding:20px; font-family: Arial; }
	</style>

	<script>
		var produtos 	= <?=json_encode($produtos);?>;
		var produto_qtd = produtos.length;
		var timer 		= [];

		function startInterval( fn, delay ) {
		    fn();
		    timer = setInterval(fn, delay);
		}

		function intervalAction(){
			if($('#loading').length) $('#loading').remove();
			var atual = parseInt($('#produto_atual').html()) + 1;
			$('#produto_atual').html(atual);

			var id 		= produtos[0];
			var tipo 	= '<?=$_GET['tipo'];?>';
			var qtd 	= '<?=$_GET['qtd'];?>';
			$.ajax({
				type: "POST",
				url: 'produto_imprimir_barcode_zebra.php',
				data: {id: id, tipo: tipo, qtd: qtd},
				success: function(data){
					$('#produto_nome').html(data.produto_nome);
				},
				dataType: 'json'
			});

			produtos.shift();
			if(produto_qtd == atual) clearInterval(timer);
		}

		$(document).ready(function(){
			var intervalo = startInterval(intervalAction, <?=($_GET['qtd'] * 4) * 1000?>);

			$('#pararTxt').click(function(){
				if($(this).hasClass('parar')){
					$(this).attr('class', 'continuar');
					$(this).html("CONTINUAR");
					clearInterval(intervalo);
				} else {
					$(this).attr('class', 'parar');
					startInterval(time);
					$(this).html("PARAR");
				}
			});
		});
	</script
</head>

<body>
	<span id="loading">PREPARANDO IMPRESSAO, AGUARDE <?=($_GET['qtd'] * 4);?> SEGUNDOS</span>
	<h1>Imprimindo etiqueta do produto: <span id="produto_nome"> ---- </span></h1>
	<span style="display:block; margin-bottom: 10px">Produto <span id="produto_atual">0</span> de <?=$rowsProduto;?></span>
	<button id="pararTxt" class="parar">PARAR</button>
</body>
</html>
