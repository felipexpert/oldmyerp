<?
	//limpando sessão se foi criado uma categoria nova
	unset($_SESSION['categoria_id']);
	
	require("produto_categoria_filtro.php");
	
	//ações em grupo
	if(isset($_POST["hid_action"])){
		require("produto_categoria_action.php");
	}
	
	if(isset($_GET['mensagem']) && $_GET['mensagem'] == "ok"){
?>		<div class="alert">
			<p class="ok">Registro gravado com sucesso!<p>
        </div>
<?	}

	if(isset($_GET['mensagem']) && $_GET['mensagem'] == "excluido"){
?>		
        <div class="alert">
			<p class="ok">Registro exclu&iacute;do!<p>
        </div>
<?	}

		
/**************************** ORDER BY *******************************************/
	$filtroOrder = 'fldNome';
	$class 		 = 'asc';
	$order_sessao = explode(" ", $_SESSION['order_produto_categoria']);
	if(isset($_GET['order'])){
		switch($_GET['order']){
			
			case 'codigo'		:  $filtroOrder = "fldId";  	break;
			case 'categoria'	:  $filtroOrder = "fldNome"; 	break;
		}
		if($order_sessao[0] == $filtroOrder){
			$class = ($order_sessao[1] == 'asc') ? 'desc' : 'asc';
		}
	}
	
	//definir icone para ordem
	$_SESSION['order_produto_categoria'] = (!$_SESSION['order_produto_categoria'] || $_GET['order']) ? $filtroOrder.' '.$class : $_SESSION['order_produto_categoria'];
	$pag	= ($_GET['pagina'])? '&pagina='.$_GET['pagina'] : ''; 
	$raiz 	= "index.php?p=produto&modo=categoria$pag&amp;order=";
	
	$order_sessao = explode(" ", $_SESSION['order_produto_categoria']);
	$filtroOrder  = $order_sessao[0]; //pra poder comparar na listagem e exibir a class
	
/**************************** PAGINAÇÃO *******************************************/
	$sSQL 		= "SELECT * FROM tblcategoria WHERE fldExcluido = '0' " . $_SESSION["filtro_categoria"]." order by " . $_SESSION['order_produto_categoria'];
	$rsTotal 	= mysql_query($sSQL);
	$rowsTotal 	= mysql_num_rows($rsTotal);
	
	$_SESSION['categoria_relatorio'] = $sSQL;
	
	
	//definição dos limites
	$limite = 150;
	$n_paginas = 7;
	
	$total_paginas = ceil(mysql_num_rows($rsTotal) / $limite);
	
	if(isset($_GET["pagina"]) && $_GET["pagina"] > $total_paginas){
		$inicio = 0;
	}elseif(isset($_GET['pagina'])){
		$inicio = ($_GET['pagina'] - 1) * $limite;
	}else{
		$inicio = 0;
	}
	
	$sSQL .= " limit " . $inicio . "," . $limite;

	$rsCategoria = mysql_query($sSQL);
	$pagina = (isset($_GET['pagina'])) ? $_GET['pagina'] : "1";
	
#########################################################################################
?>

    <form class="table_form" id="frm_categoria" action="" method="post">
    	<div id="table">
            <div id="table_cabecalho">
                <ul class="table_cabecalho">
                    <li style="width:10px">&nbsp;</li>
                    <li class="order" style="width:90px">
                    	<a <?= ($filtroOrder == 'fldId') 	? "class='$class'" : '' ?> style="width:65px" href="<?=$raiz?>codigo">C&oacute;d.</a>
                    </li>
                    <li class="order" style="width:600px">
                    	<a <?= ($filtroOrder == 'fldNome') 	? "class='$class'" : '' ?> style="width:585px" href="<?=$raiz?>categoria">Categoria</a>
                    </li>
					<li style="width:60px; text-align:center">Desconto</li>
                    <li style="width:100px; text-align:center">Produtos</li>
                    <li style="width:38px">&nbsp;</li>
                    <li style="width:25px; text-align:left"><input type="checkbox" name="chk_todos" id="chk_todos" /></li>
                </ul>
            </div>
            <div id="table_container">       
                <table id="table_general" class="table_general" summary="Lista de Categorias">
                	<tbody>
<?					
					$id_array = array();
					$n = 0;
					
					$linha 	= "row";
					$rows 	= mysql_num_rows($rsCategoria);
					while($rowCategoria = mysql_fetch_array($rsCategoria)){
						
						$id_array[$n] = $rowCategoria["fldId"];
						$n += 1;
				
?>						<tr class="<?= $linha; ?>">
<?				 	 		$icon = ($rowCategoria["fldDisabled"] ? "bg_disable" : "bg_enable");
							$title = ($rowCategoria["fldDisabled"] ? "desabilitado" : "habilitado");
?>							<td style="width:20px;text-align:center;"><img src="image/layout/<?=$icon?>.gif" alt="status" title="<?=$title?>"/></td>
							<td class="cod" style="width:70px;text-align:right;padding-right:10px"><?=$rowCategoria['fldId']?></td>
                            <td style="width:600px;"><?=$rowCategoria['fldNome']?></td>
							<td style="width:60px; text-align:center"><?=format_number_out($rowCategoria['fldDesconto'])?></td>
<?                         	
							$sql 		= mysql_query("SELECT * from tblproduto WHERE fldCategoria_Id = " . $rowCategoria['fldId']." AND fldExcluido = 0");
							$produtos 	= mysql_num_rows($sql);
							
?>							<td style="width:125px; text-align:center"><?=$produtos?></td>
                            <td style="width:auto; text-align:center"><a class="edit" href="index.php?p=produto&modo=categoria_detalhe&amp;id=<?=$rowCategoria['fldId']?>"></a></td>
                            <td style="width:auto"><input type="checkbox" name="chk_categoria_<?=$rowCategoria['fldId']?>" id="chk_categoria_<?=$rowCategoria['fldId']?>" title="selecionar o registro posicionado" /></td>
                        </tr>
<?                  	$linha = ($linha == "row") ? "dif-row" : "row"; 
					}
?>		 			</tbody>
				</table>
            </div>
       	  
            <input type="hidden" name="hid_array"	id="hid_array" 	value="<?=urlencode(serialize($id_array))?>" />
            <input type="hidden" name="hid_action" 	id="hid_action" value="true" />
            
			<div id="table_action">
                <ul id="action_button">
                    <li><a class="btn_novo" href="index.php?p=produto&modo=categoria_novo">novo</a></li>
                    <li><input type="submit" name="btn_action" id="btn_excluir" 	value="excluir" 	title="Excluir registro(s) selecionado(s)" onclick="return confirm('Deseja excluir os registros selecionados?')" /></li>
                    <li><input type="submit" name="btn_action" id="btn_habilitar" 	value="habilitar" 	title="Habilitar registro(s) selecionado(s)" /></li>
                    <li><input type="submit" name="btn_action" id="btn_desabilitar" value="desabilitar" title="Desabilitar registro(s) selecionado(s)" /></li>
                	<li><button type="submit"name="btn_action" id="btn_print"		value="imprimir" 	title="Imprimir relat&oacute;rio" ></button></li>
                </ul>
        	</div>
            <div id="table_paginacao">
<?				$paginacao_destino = "?p=produto&modo=categoria";
				include("paginacao.php")
?>		
            </div>
            <div class="table_registro">
            	<span>Exibindo registros <?=($pagina*$limite-$limite+1).' a '.($pagina*$limite-$limite+$rows)?> do total de <?=$rowsTotal?></span>
            </div>    
        </div>
	</form>