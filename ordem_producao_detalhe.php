<?	$ordem_id = $_GET['id']; ?>

<div id="voltar">
    <p><a href="index.php?p=ordem_producao">n&iacute;vel acima</a></p>
</div>

<h2>Editar Ordem de Produ&ccedil;&atilde;o</h2>
<div id="principal">
	<?
	$rsDadosOP 	= mysql_query("SELECT * FROM tblop WHERE fldId = $ordem_id");
	$rowDadosOP = mysql_fetch_assoc($rsDadosOP);

	$usuario_id 				= $rowDadosOP['fldUsuario_Id'];
	$verificar_limite_credito 	= $_SESSION["sistema_venda_alerta_limite_credito"];
	$sistemaStatus				= fnc_sistema('pedido_status');
	$vendaDecimal 				= fnc_sistema('venda_casas_decimais');
	$qtdeDecimal 				= fnc_sistema('quantidade_casas_decimais');
	$servico_valor 				= fnc_sistema('venda_servico_valor');

	//a��es
	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['btn_gravar_op']) && $_POST['btn_gravar_op'] == 'Gravar'){ //se for salvar

		########################################################################################################################################################
		$dataAbertura	= format_date_in($_POST['txt_abertura_op']);
		$dataPrevisao	= format_date_in($_POST['txt_previsao_op']);
		$impressao		= $_POST['sel_impressao_op'];
		$statusOP		= $_POST['sel_status_op'];
		$dataFinalizado = format_date_in($_POST['txt_finalizado_op']);
		$prioridadeOP	= $_POST['sel_prioridade_op'];
		$obs			= $_POST['txt_obs_op'];
		
		$insert_OP = "UPDATE tblop SET
		fldAbertura 	= '$dataAbertura',
		fldPrevisao 	= '$dataPrevisao',
		fldFinalizado 	= '$dataFinalizado',
		fldStatus 		= '$statusOP',
		fldPrioridade 	= '$prioridadeOP',
		fldImpressao 	= '$impressao',
		fldObs			= '$obs'
		WHERE fldId = $ordem_id";
		
		if(mysql_query($insert_OP)){

			$op_id	= $ordem_id; //ID DA OP
			
			###############################################################################################
			# INSERE NA OP AUXILIAR #######################################################################
			###############################################################################################
			$chkLote = ($_POST['chk_lote_op'] == '1') ? '1' : '0';
			($chkLote == '1') ? $lote = $_POST['hid_numero_lote'] : $lote = NULL;
			
			$insert_auxiliar = "UPDATE tblop_auxiliar SET fldLote = '$lote' WHERE fldOp_Id = '$op_id'";
			mysql_query($insert_auxiliar);
			###############################################################################################

			####COME�A INSERIR OS PRODUTOS####
		
			$n = 1;
			$limite = $_POST['hid_controle'];
			
			while($n <= $limite){
				if(isset($_POST['hid_item_op_produto_id_'.$n])){
					$produto_id		= $_POST['hid_item_op_produto_id_'.$n];
					$item_salvo 	= $_POST['hid_item_op_salvo_'.$n];
					$fldOP_id		= $op_id;
					$id_item_bd 	= $_POST['hid_item_op_idbd_'.$n];
					$fldDesc		= $_POST['txt_produto_op_desc_'.$n];
					$fldQtd			= $_POST['txt_produto_op_qtd_'.$n];
					$fldEstocado	= $_POST['txt_produto_op_estocado_'.$n];
					$fldExcluido	= $_POST['hid_item_op_excluido_'.$n];
					
					#############################################################################
					# Especificacoes
					#############################################################################
					$fldMaquina 					= (isset($_POST['hid_item_op_fldMaquina_'.$n]) and $_POST['hid_item_op_fldMaquina_'.$n] != '') 											? $_POST['hid_item_op_fldMaquina_'.$n] 						: null;
					$fldAcerto_Maquina 				= (isset($_POST['hid_item_op_fldAcerto_Maquina_'.$n]) and $_POST['hid_item_op_fldAcerto_Maquina_'.$n] != '') 							? $_POST['hid_item_op_fldAcerto_Maquina_'.$n] 				: null;
					$fldPapel_Produzido 			= (isset($_POST['hid_item_op_fldPapel_Produzido_'.$n]) and $_POST['hid_item_op_fldPapel_Produzido_'.$n] != '') 							? $_POST['hid_item_op_fldPapel_Produzido_'.$n] 				: null;
					$fldRetorno_Papel 				= (isset($_POST['hid_item_op_fldRetorno_Papel_'.$n]) and $_POST['hid_item_op_fldRetorno_Papel_'.$n] != '') 								? $_POST['hid_item_op_fldRetorno_Papel_'.$n] 				: null;
					$fldPapel_Entregue 				= (isset($_POST['hid_item_op_fldPapel_Entregue_'.$n]) and $_POST['hid_item_op_fldPapel_Entregue_'.$n] != '') 							? $_POST['hid_item_op_fldPapel_Entregue_'.$n] 				: null;
					$fldAcerto_Papel 				= (isset($_POST['hid_item_op_fldAcerto_Papel_'.$n]) and $_POST['hid_item_op_fldAcerto_Papel_'.$n] != '') 								? $_POST['hid_item_op_fldAcerto_Papel_'.$n] 				: null;
					$fldPapel_Perdido 				= (isset($_POST['hid_item_op_fldPapel_Perdido_'.$n]) and $_POST['hid_item_op_fldPapel_Perdido_'.$n] != '') 								? $_POST['hid_item_op_fldPapel_Perdido_'.$n] 				: null;
					$fldQtd_Perdida 				= (isset($_POST['hid_item_op_fldQtd_Perdida_'.$n]) and $_POST['hid_item_op_fldQtd_Perdida_'.$n] != '') 									? $_POST['hid_item_op_fldQtd_Perdida_'.$n] 					: null;
					$fldTotal_Etiquetas 			= (isset($_POST['hid_item_op_fldTotal_Etiquetas_'.$n]) and $_POST['hid_item_op_fldTotal_Etiquetas_'.$n] != '') 							? $_POST['hid_item_op_fldTotal_Etiquetas_'.$n] 				: null;
					$fldAmostra 					= (isset($_POST['hid_item_op_fldAmostra_'.$n]) and $_POST['hid_item_op_fldAmostra_'.$n] != '') 											? $_POST['hid_item_op_fldAmostra_'.$n] 						: null;
					$fldAmostra_De					= (isset($_POST['hid_item_op_fldAmostra_De_'.$n]) and $_POST['hid_item_op_fldAmostra_De_'.$n] != '') 									? $_POST['hid_item_op_fldAmostra_De_'.$n] 					: null;
					$fldRemetente 					= (isset($_POST['hid_item_op_fldRemetente_'.$n]) and $_POST['hid_item_op_fldRemetente_'.$n] != '') 										? $_POST['hid_item_op_fldRemetente_'.$n] 					: null;
					$fldContagem_Funcionario_Id 	= (isset($_POST['hid_item_op_fldContagem_Funcionario_Id_'.$n]) and $_POST['hid_item_op_fldContagem_Funcionario_Id_'.$n] != '') 			? $_POST['hid_item_op_fldContagem_Funcionario_Id_'.$n] 		: null;
					$fldEmpacotagem_Funcionario_Id 	= (isset($_POST['hid_item_op_fldEmpacotagem_Funcionario_Id_'.$n]) and $_POST['hid_item_op_fldEmpacotagem_Funcionario_Id_'.$n] != '') 	? $_POST['hid_item_op_fldEmpacotagem_Funcionario_Id_'.$n] 	: null;
					$fldQtd_Pacote 					= (isset($_POST['hid_item_op_fldQtd_Pacote_'.$n]) and $_POST['hid_item_op_fldQtd_Pacote_'.$n] != '') 									? $_POST['hid_item_op_fldQtd_Pacote_'.$n] 					: null;
					#############################################################################
					
					$componente_id	= explode(',', $_POST['hid_produto_componente_op_'.$n]);
					$componente_qtd	= explode(',', $_POST['hid_produto_componente_qtd_op_'.$n]);
					$componente_dsc = explode(',', $_POST['hid_produto_componente_desc_op_'.$n]);
					
					if($item_salvo != '1' && $produto_id != '0'){
						//inserre o produto
						mysql_query("INSERT INTO tblop_item
						(fldProduto_Id, fldOP_Id, fldDesc, fldQuantidade, fldEstocado, fldExcluido)
						VALUES
						('$produto_id', '$fldOP_id', '$fldDesc', '$fldQtd', '$fldEstocado', '$fldExcluido')");

						$rsItemOP = mysql_query("SELECT last_insert_id() as lastIDItem");
						$rowItemOP = mysql_fetch_assoc($rsItemOP);
						$itembd_id = $rowItemOP['lastIDItem'];
						
						mysql_query("INSERT INTO tblop_item_auxiliar (fldItem_Id, fldMaquina, fldAcerto_Maquina, fldPapel_Produzido, fldRetorno_Papel,
									fldPapel_Entregue, fldAcerto_Papel, fldPapel_Perdido, fldQtd_Perdida, fldTotal_Etiquetas, fldAmostra, fldAmostra_De, 
									fldRemetente, fldContagem_Funcionario_Id, fldEmpacotagem_Funcionario_Id, fldQtd_Pacote) VALUES 
									('$itembd_id', '$fldMaquina', '$fldAcerto_Maquina', '$fldPapel_Produzido', '$fldRetorno_Papel', '$fldPapel_Entregue', 
									'$fldAcerto_Papel', '$fldPapel_Perdido', '$fldQtd_Perdida', '$fldTotal_Etiquetas', '$fldAmostra', '$fldAmostra_De', 
									'$fldRemetente', '$fldContagem_Funcionario_Id', '$fldEmpacotagem_Funcionario_Id', '$fldQtd_Pacote')");

						//depois seu componente(se tiver)
						if(isset($_POST['hid_produto_componente_op_'.$n]) && $componente_id[0] != ''){
							for($iC = 0; $iC < count($componente_id); $iC++){
								mysql_query("INSERT INTO tblop_item_componente
												(fldComponente_Id, fldProduto_Id, fldOP_Id, fldComponente_Qtd, fldDescricao, fldExcluido, fldId_Item_OP)
											VALUES
												('".$componente_id[$iC]."', '".$produto_id."', '".$fldOP_id."', '".$componente_qtd[$iC]."', '".$componente_dsc[$iC]."', '0', '".$itembd_id."')");
							}
						}
					}
					elseif($fldExcluido == '1'){
						//update excluindo campo
						mysql_query("UPDATE tblop_item SET fldExcluido = '1' WHERE fldProduto_Id = '".$produto_id."' AND fldOP_Id = '".$fldOP_id."'");
						mysql_query("UPDATE tblop_item_componente SET fldExcluido = '1' WHERE fldProduto_Id = '".$produto_id."' AND fldOP_Id = '".$fldOP_id."' AND fldId_Item_OP = '".$id_item_bd."'");
					}
					else{
						//apenas editando
						mysql_query("UPDATE tblop_item_auxiliar SET
									fldMaquina 						= '$fldMaquina',
									fldAcerto_Maquina 				= '$fldAcerto_Maquina',
									fldPapel_Produzido 				= '$fldPapel_Produzido',
									fldRetorno_Papel 				= '$fldRetorno_Papel',
									fldPapel_Entregue 				= '$fldPapel_Entregue',
									fldAcerto_Papel 				= '$fldAcerto_Papel',
									fldPapel_Perdido 				= '$fldPapel_Perdido',
									fldQtd_Perdida 					= '$fldQtd_Perdida',
									fldTotal_Etiquetas 				= '$fldTotal_Etiquetas',
									fldAmostra 						= '$fldAmostra',
									fldAmostra_De					= '$fldAmostra_De',
									fldRemetente 					= '$fldRemetente',
									fldContagem_Funcionario_Id	 	= '$fldContagem_Funcionario_Id',
									fldEmpacotagem_Funcionario_Id 	= '$fldEmpacotagem_Funcionario_Id',
									fldQtd_Pacote 					= '$fldQtd_Pacote'
									WHERE fldItem_Id = $id_item_bd");
					}					
				}
				$n+=1;
			}
		}

		########################################################################################################################################################
		
		if(!mysql_error()){
			header("location:index.php?p=ordem_producao");
		}else{
?>			<div class="alert">
				<p class="erro">N&atilde;o foi poss&iacute;vel gravar os dados</p>
				<a class="voltar" href="javascript:history.back();">voltar</a>
			</div>
<?				echo mysql_error();
		}
		
	}else{
		$remote_ip 		= gethostbyname($REMOTE_ADDR);
		$_SESSION['ref_timestamp'] = $remote_ip.date("YmdHis");
		$rowUsuario 	= mysql_fetch_array(mysql_query("SELECT * FROM tblusuario where fldId =".$usuario_id));
		?>

		<span style="display:block; clear:both;"></span>

        <div class="form">
            <form class="frm_detalhe" style="width:890px; margin:0 auto;" id="frm_op_novo" action="" method="post">
                <ul style="width:915px; margin:8px auto 0 auto; float:none">
                    <li>
                        <label for="txt_codigo_op">ID</label>
                        <input type="text" style="width:70px; text-align:center" class="txt_codigo_op" id="txt_codigo_op" name="txt_codigo_op" disabled="disabled" value="<?=$ordem_id?>"/>
                    </li>
                    <li>
                        <label for="txt_abertura_op">Data de Abertura</label>
                        <input type="text" style="width:120px;text-align:center" class="calendario-mask" id="txt_abertura_op" name="txt_abertura_op" value="<?=format_date_out($rowDadosOP['fldAbertura']);?>" />
                        <a href="#" title="Exibir calend&aacute;rio" class="exibir-calendario-data-atual"></a>
                    </li>
                    <li>
                        <label for="txt_abertura_hora_op">Hora</label>
                        <input type="text" style="width:70px; text-align: center" id="txt_abertura_hora_op" name="txt_abertura_hora_op" value="<?=$rowDadosOP['fldCadastroHora'];?>" readonly="readonly" />
                    </li>
                    <li>
                        <label for="txt_previsao_op">Previsto para</label>
                        <input type="text" style="width:120px;text-align:center" class="calendario-mask" id="txt_previsao_op" name="txt_previsao_op" value="<?=format_date_out($rowDadosOP['fldPrevisao']);?>" />
                        <a href="#" title="Exibir calend&aacute;rio" class="exibir-calendario-data-atual"></a>
                    </li>
                    <li>
                        <label for="txt_usuario">Usu&aacute;rio</label>
                        <input type="text" style="width:130px" id="txt_usuario" name="txt_usuario" value="<?=$rowUsuario['fldUsuario']?>" readonly="readonly"/>
                    </li>
                    <li>
                        <label for="sel_impressao_op">Impress&atilde;o</label>
                        <select class="sel_impressao_op" style="width:130px" id="sel_impressao_op" name="sel_impressao_op" >
							<option <?= ($rowDadosOP['fldImpressao'] == '1') ? 'selected="selected"' : ''; ?> value="1">Liberado</option>
							<option <?= ($rowDadosOP['fldImpressao'] == '0') ? 'selected="selected"' : ''; ?> value="0">N&atilde;o liberado</option>
                 		</select>
                    </li>
                    <? if(fnc_campo_auxiliar("tblop_auxiliar_controle", "fldLote") == "1"){ ?>
                    <li>
                        <label for="chk_lote_op">Lote <span style="cursor:help; float:right; margin-right:20px; border-bottom:1px dotted #000; font-size:10px" title="n&uacute;mero &uacute;nico e sequ&ecirc;ncial para controle interno">[?]</span></label>
						<input type="text" value="<?= ($rowDadosOP['fldLote'] != '0' || $rowDadosOP['fldLote'] != NULL) ? $rowDadosOP['fldLote'] : ''; ?>" name="txt_lote_op" id="txt_lote_op" class="txt_lote_op" style="width:110px" title="ativar lote" disabled="disabled" />
                        <input type="checkbox" <?= ($rowDadosOP['fldLote'] != '0' || $rowDadosOP['fldLote'] != NULL) ? 'checked="checked"' : ''; ?> name="chk_lote_op" id="chk_lote_op" class="chk_lote_op" value="1" style="width:15px; height:15px; margin:4px 0 0 5px" />
						<input type="hidden" name="hid_numero_lote" id="hid_numero_lote" class="hid_numero_lote" value="<?=fnc_sistema('lote_op');?>" />
            		</li>
					<? } ?>
                </ul>
                <ul style="width:915px; margin:0 auto; float:none">
                    <li>
                        <label for="sel_status_op">Status</label>
                        <select class="sel_status" style="width:130px" id="sel_status_op" name="sel_status_op">
							<option <?= ($rowDadosOP['fldStatus'] == '2') ? 'selected="selected"' : '';?> value="2">Em andamento</option>
							<option <?= ($rowDadosOP['fldStatus'] == '3') ? 'selected="selected"' : '';?> value="3">Finalizado</option>
                 		</select>
                    </li>
					<li>
                        <label for="txt_finalizado_op">Finalizado em</label>
                        <input type="text" class="calendario-mask" <?= ($rowDadosOP['fldFinalizado'] != '0000-00-00') ? 'value="'.format_date_out($rowDadosOP['fldFinalizado']).'"' : 'disabled="disabled"';?> name="txt_finalizado_op" id="txt_finalizado_op" class="txt_finalizado_op" style="width:120px; text-align:center">
						<input type="hidden" name="data_atual_op" id="data_atual_op" class="data_atual_op" value="<?=date('d/m/Y');?>">
                    </li>
					<li>
						<label for="sel_prioridade_op">Prioridade</label>
                        <select class="sel_prioridade_op" style="width:130px" id="sel_prioridade_op" name="sel_prioridade_op">
<?						$rsPrioridade = mysql_query("SELECT * FROM tblop_prioridade ORDER BY fldId ASC");
						while($rowPrioridade= mysql_fetch_array($rsPrioridade)){
?>							<option <?= ($rowDadosOP['fldPrioridade'] == $rowPrioridade['fldId']) ? 'selected="selected"' : '';?> value="<?=$rowPrioridade['fldId']?>"><?=$rowPrioridade['fldPrioridade']?></option>
<?						}
?>                 		</select>
					</li>
					<li>
                 	   <label for="txt_obs_op">Observa&ccedil;&atilde;o</label>
                       <textarea style="width:900px; height:80px; margin-bottom:10px" id="txt_obs_op" name="txt_obs_op"><?=$rowDadosOP['fldObs'];?></textarea>
                    </li>
				</ul>
				
				<span style="clear:both; display:block"></span>

				<h3 style="padding:0; margin:10px 0">Produtos</h3>

                <div id="lista_produtos" style="width:960px; display:table">
					
                    <div id="pedido_produto">
                    	<ul style="width:962px">
                            <li>
                                <label for="txt_op_produto_codigo">C&oacute;digo</label>
                                <input type="text" class="codigo" style="width: 80px" id="txt_produto_codigo" name="txt_op_produto_codigo" value="" />
                                <a href="produto_busca" title="Localizar" class="modal" rel="950-450"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                                <input type="hidden" id="hid_op_produto_id" 				name="hid_op_produto_id" 				value="" />
                            </li>
                            <li>
                                <label for="txt_op_produto_nome">Produto</label>
                                <input type="text" style="width:470px;" id="txt_op_produto_nome" name="txt_op_produto_nome" />
                            </li>
                            <li>
                                <label for="txt_op_produto_quantidade">Qtde</label>
                                <input type="text" style="width:50px; text-align:right;" id="txt_op_produto_quantidade" name="txt_op_produto_quantidade" value="" />
                                <input type="hidden" name="hid_op_quantidade_decimal" id="hid_op_quantidade_decimal" value="<?=$qtdeDecimal?>" />
                            </li>
                            <li>
                            	<button class="btn_sub_small" name="btn_op_item_inserir" id="btn_op_item_inserir" title="Inserir" >ok</button>
                                <input type="hidden" id="hid_op_produto_UM" name="hid_op_produto_UM" value="" />
								<input type="hidden" id="hid_op_componente_id" name="hid_op_componente_id" value="" />
								<input type="hidden" id="hid_op_componente_qtd" name="hid_op_componente_qtd" value="" />
								<input type="hidden" id="hid_op_componente_desc" name="hid_op_componente_desc" value="" />
								<input type="hidden" id="hid_op_componente_um" name="hid_op_componente_um" class="hid_op_componente_um" value="">
								<input type="hidden" id="hid_op_estocado" name="hid_op_estocado" value="" />
                            </li>
                        </ul>
					</div>
                
                	<div id="pedido_lista" style="width:960px">

                        <ul id="pedido_lista_cabecalho" style="width: 1868px">
							<li style="border-right:0; width:20px"></li>
							<li style="width:20px">&nbsp;</li>
                            <li style="width:90px">C&oacute;d. Produto</li>
                            <li style="width:658px">Produto</li>
                            <li style="width:45px">Qtd</li>
							<li style="width:40px;" title="Unidade de Medida">U.M</li>
                            <li style="width:80px">Em estoque</li>
							<? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldMaquina") == "1"){ ?>
                            <li style="width:100px">M&aacute;quina</li>
                            <?	} ?>
							<? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldAcerto_Maquina") == "1"){ ?>
                            <li style="width:100px">Acerto m&aacute;quina</li>
                            <?	} ?>
							<? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldPapel_Produzido") == "1"){ ?>
                            <li style="width:100px">Papel produzido</li>
                            <?	} ?>
							<? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldRetorno_Papel") == "1"){ ?>
                            <li style="width:100px">Retorno papel</li>
                            <?	} ?>
							<? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldPapel_Entregue") == "1"){ ?>
                            <li style="width:100px">Papel entregue</li>
                            <?	} ?>
							<? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldAcerto_Papel") == "1"){ ?>
                            <li style="width:100px">Acerto papel</li>
                            <?	} ?>
							<? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldPapel_Perdido") == "1"){ ?>
                            <li style="width:100px">Papel perdido</li>
                            <?	} ?>
							<? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldQtd_Perdida") == "1"){ ?>
                            <li style="width:100px">Qtd perdida</li>
                            <?	} ?>
							<? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldTotal_Etiquetas") == "1"){ ?>
                            <li style="width:100px">Total etiquetas</li>
                            <?	} ?>
							<? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldAmostra") == "1"){ ?>
                            <li style="width:100px">Amostra</li>
                            <?	} ?>
							<? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldRemetente") == "1"){ ?>
                            <li style="width:100px">Remetente</li>
                            <?	} ?>
							<? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldContagem_Funcionario_Id") == "1"){ ?>
                            <li style="width:100px">Contagem</li>
                            <?	} ?>
							<? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldEmpacotagem_Funcionario_Id") == "1"){ ?>
                            <li style="width:100px">Empacotagem</li>
                            <?	} ?>
							<? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldQtd_Pacote") == "1"){ ?>
                            <li style="width:100px">Qtd pacote</li>
                            <?	} ?>
                        </ul>

                   		<div id="hidden">
							<ul id="pedido_lista_item" style="width:1868px">
								<li style="width:20px">
									<a class="a_op_excluir" id="excluir" href="" title="Excluir item"></a>
									<input type="hidden" id="hid_item_op_id" name="hid_item_op_id" class="hid_item_op_id" value="">
									<input type="hidden" id="hid_item_op_excluido" name="hid_item_op_excluido" class="hid_item_op_excluido" value="">
									<input type="hidden" id="hid_produto_componente_op" name="hid_produto_componente_op" class="hid_produto_componente_op" value="">
									<input type="hidden" id="hid_produto_componente_qtd_op" name="hid_produto_componente_qtd_op" class="hid_produto_componente_qtd_op" value="">
									<input type="hidden" id="hid_produto_componente_desc_op" name="hid_produto_componente_desc_op" class="hid_produto_componente_desc_op" value="">
								</li>
                                <li style="width:20px;">
                                    <a class="edit edit_js modal" href="ordem_producao_item" name='' rel='600-240' title="Editar item"></a>
                                </li>
								<li>
									<input class="txt_item_op_codigo" type="text" style="width: 89px" id="txt_item_op_codigo" name="txt_item_op_codigo" value="" readonly="readonly"/>
									<input class="hid_item_op_produto_id" type="hidden" id="hid_item_op_produto_id" name="hid_item_op_produto_id" value="" />
								</li>
								<li>
									<input class="txt_produto_op_desc" type="text" style="width:658px; text-align:left;" id="txt_produto_op_desc" name="txt_produto_op_desc" value="" readonly="readonly" />
								</li>
								<li>
									<input class="txt_produto_op_qtd" type="text" style="width:45px" id="txt_produto_op_qtd" name="txt_produto_op_qtd" value="" readonly="readonly" />
								</li>
								<li>
									<input class="txt_produto_op_um" type="text" style="width:40px; text-align:center" id="txt_produto_op_um" name="txt_produto_op_um" value="" readonly="readonly" />
								</li>
								<li>
									<input class="txt_produto_op_estocado" type="text" style="width:80px; text-align:center;" id="txt_produto_op_estocado" name="txt_produto_op_estocado" value="" readonly="readonly" />
								</li>
                                <? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldMaquina") == "1"){ ?>
                                <li>
                                    <input type="text" style="width:100px" readonly="readonly" id="hid_item_op_fldMaquina" name="hid_item_op_fldMaquina" class="hid_item_op_fldMaquina" value="" />
                                </li>
                                <?	} ?>
                                <? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldAcerto_Maquina") == "1"){ ?>
								<li>
                                    <input type="text" style="width:100px" readonly="readonly" id="hid_item_op_fldAcerto_Maquina" name="hid_item_op_fldAcerto_Maquina" class="hid_item_op_fldAcerto_Maquina" value="" />
                                </li>
                                <?	} ?>
                                <? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldPapel_Produzido") == "1"){ ?>
                                <li>
                                    <input type="text" style="width:100px" readonly="readonly" id="hid_item_op_fldPapel_Produzido" name="hid_item_op_fldPapel_Produzido" class="hid_item_op_fldPapel_Produzido" value="" />
                               	</li>
                                <?	} ?>
                                <? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldRetorno_Papel") == "1"){ ?>
                                <li>
                                    <input type="text" style="width:100px" readonly="readonly" id="hid_item_op_fldRetorno_Papel" name="hid_item_op_fldRetorno_Papel" class="hid_item_op_fldRetorno_Papel" value="" />
                               	</li>
                                <?	} ?>
                                <? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldPapel_Entregue") == "1"){ ?>
                                <li>
                                    <input type="text" style="width:100px" readonly="readonly" id="hid_item_op_fldPapel_Entregue" name="hid_item_op_fldPapel_Entregue" class="hid_item_op_fldPapel_Entregue" value="" />
                                </li>
                                <?	} ?>
                                <? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldAcerto_Papel") == "1"){ ?>
                                <li>
                                    <input type="text" style="width:100px" readonly="readonly" id="hid_item_op_fldAcerto_Papel" name="hid_item_op_fldAcerto_Papel" class="hid_item_op_fldAcerto_Papel" value="" />
                                </li>
                                <?	} ?>
                                <? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldPapel_Perdido") == "1"){ ?>
                                <li>
                                    <input type="text" style="width:100px" readonly="readonly" id="hid_item_op_fldPapel_Perdido" name="hid_item_op_fldPapel_Perdido" class="hid_item_op_fldPapel_Perdido" value="" />
                                </li>
                                <?	} ?>
                                <? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldQtd_Perdida") == "1"){ ?>
                                <li>
                                    <input type="text" style="width:100px" readonly="readonly" id="hid_item_op_fldQtd_Perdida" name="hid_item_op_fldQtd_Perdida" class="hid_item_op_fldQtd_Perdida" value="" />
                                </li>
                                <?	} ?>
                                <? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldTotal_Etiquetas") == "1"){ ?>
                                <li>
                                    <input type="text" style="width:100px" readonly="readonly" id="hid_item_op_fldTotal_Etiquetas" name="hid_item_op_fldTotal_Etiquetas" class="hid_item_op_fldTotal_Etiquetas" value="" />
                                </li>
                                <?	} ?>
                                <? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldAmostra") == "1"){ ?>
                                <li>
                                    <input type="text" style="width:100px" readonly="readonly" id="hid_item_op_fldAmostra" name="hid_item_op_fldAmostra" class="hid_item_op_fldAmostra" value="" />
                                    <input type="hidden" readonly="readonly" id="hid_item_op_fldAmostra_De" name="hid_item_op_fldAmostra_De" class="hid_item_op_fldAmostra_De" value="" />
                                </li>
                                <?	} ?>
                                <? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldRemetente") == "1"){ ?>
                                <li>
                                    <input type="text" style="width:100px" readonly="readonly" id="hid_item_op_fldRemetente" name="hid_item_op_fldRemetente" class="hid_item_op_fldRemetente" value="" />
                                </li>
                                <?	} ?>
                                <? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldContagem_Funcionario_Id") == "1"){ ?>
                                <li>
                                    <input type="text" style="width:100px" readonly="readonly" id="hid_item_op_fldContagem_Funcionario_Id" name="hid_item_op_fldContagem_Funcionario_Id" class="hid_item_op_fldContagem_Funcionario_Id" value="" />
                                </li>
                                <?	} ?>
                                <? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldEmpacotagem_Funcionario_Id") == "1"){ ?>
                                <li>
                                    <input type="text" style="width:100px" readonly="readonly" id="hid_item_op_fldEmpacotagem_Funcionario_Id" name="hid_item_op_fldEmpacotagem_Funcionario_Id" class="hid_item_op_fldEmpacotagem_Funcionario_Id" value="" />
                                </li>
                                <?	} ?>
                                <? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldQtd_Pacote") == "1"){ ?>
                                <li>
                                    <input type="text" style="width:100px" readonly="readonly" id="hid_item_op_fldQtd_Pacote" name="hid_item_op_fldQtd_Pacote" class="hid_item_op_fldQtd_Pacote" value="" />
								</li>
                                <?	} ?>
							</ul>
                    	</div>

<?						$sqlImportar = mysql_query("SELECT 
														tblop_item.fldId AS fldId_Item,
														tblop_item.fldDesc AS fldDescricao, 
														tblop_item.fldProduto_Id, 
														tblop_item.fldQuantidade, 
														tblproduto.fldCodigo, 
														tblproduto.fldUN_Medida_Id, 
														tblproduto_unidade_medida.fldNome AS fldUnidadeMedida, 
														tblproduto_unidade_medida.fldSigla AS fldSiglaUM, 
														tblop_item.fldEstocado AS fldEstoqueQuantidade,
														
														tblop_item_auxiliar.fldMaquina,
														tblop_item_auxiliar.fldAcerto_Maquina,
														tblop_item_auxiliar.fldPapel_Produzido,
														tblop_item_auxiliar.fldRetorno_Papel,
														tblop_item_auxiliar.fldPapel_Entregue,
														tblop_item_auxiliar.fldAcerto_Papel,
														tblop_item_auxiliar.fldPapel_Perdido,
														tblop_item_auxiliar.fldQtd_Perdida,
														tblop_item_auxiliar.fldTotal_Etiquetas,
														tblop_item_auxiliar.fldAmostra,
														tblop_item_auxiliar.fldAmostra_De,
														tblop_item_auxiliar.fldRemetente,
														tblop_item_auxiliar.fldContagem_Funcionario_Id,
														tblop_item_auxiliar.fldEmpacotagem_Funcionario_Id,
														tblop_item_auxiliar.fldQtd_Pacote
														
													FROM `tblop_item` 
													
														LEFT JOIN tblproduto ON tblproduto.fldId = tblop_item.fldProduto_Id 
														LEFT JOIN tblproduto_unidade_medida ON tblproduto_unidade_medida.fldId = tblproduto.fldUN_Medida_Id
														LEFT JOIN tblop_item_auxiliar ON tblop_item.fldId = tblop_item_auxiliar.fldItem_Id
														
													WHERE tblop_item.fldOP_Id = $ordem_id AND tblop_item.fldExcluido = '0'");
		
						$i = 1;
		
						while($rowImportar = mysql_fetch_assoc($sqlImportar)){
						
							$sqlComponentes_Atual = mysql_query("SELECT
																	CONVERT(GROUP_CONCAT(fldComponente_Id ORDER BY fldId ASC), CHAR) AS fldComponente_Id,
																	CONVERT(GROUP_CONCAT(fldComponente_Qtd ORDER BY fldId ASC), CHAR) AS fldComponente_Qtd,
																	CONVERT(GROUP_CONCAT(fldDescricao ORDER BY fldId ASC), CHAR) AS fldDesc
																FROM tblop_item_componente 
																WHERE fldProduto_Id = ".$rowImportar['fldProduto_Id']." 
																AND fldOP_Id = $ordem_id AND fldExcluido = 0");
							$rowComponentes_Atual = mysql_fetch_assoc($sqlComponentes_Atual);
							
							?>
						
							<ul id="pedido_lista_item" style="width:1868px">
								<li style="width:20px">
									<a class="a_op_excluir" id="excluir" href="" title="Excluir item"></a>
									<input type="hidden" id="hid_item_op_salvo_<?=$i?>" name="hid_item_op_salvo_<?=$i?>" class="hid_item_op_salvo" value="1" />
									<input type="hidden" id="hid_item_op_idbd_<?=$i?>" name="hid_item_op_idbd_<?=$i?>" class="hid_item_op_idbd" value="<?=$rowImportar['fldId_Item']?>" />
									<input type="hidden" id="hid_item_op_excluido_<?=$i?>" name="hid_item_op_excluido_<?=$i?>" class="hid_item_op_excluido" value="">
									<input type="hidden" id="hid_item_op_id_<?=$id?>" name="hid_item_op_id_<?=$id?>" class="hid_item_op_id" value="<?=$rowImportar['fldProduto_Id']?>">
									<input type="hidden" id="hid_produto_componente_op_<?=$i?>" name="hid_produto_componente_op_<?=$i?>" class="hid_produto_componente_op" value="<?=$rowComponentes_Atual['fldComponente_Id'];?>">
									<input type="hidden" id="hid_produto_componente_qtd_op_<?=$i?>" name="hid_produto_componente_qtd_op_<?=$i?>" class="hid_produto_componente_qtd_op" value="<?=$rowComponentes_Atual['fldComponente_Qtd'];?>">
									<input type="hidden" id="hid_produto_componente_desc_op_<?=$i?>" name="hid_produto_componente_desc_op_<?=$i?>" class="hid_produto_componente_desc_op" value="<?=$rowComponentes_Atual['fldDesc'];?>">
								</li>
                                <li style="width:20px;">
                                    <a class="edit edit_js modal" id="edit_0" href="ordem_producao_item,<?=$i?>" name='' rel='600-240' title="Editar item"></a>
                                </li>
								<li>
									<input class="txt_item_op_codigo" type="text" style="width: 89px" id="txt_item_op_codigo_<?=$i?>" name="txt_item_op_codigo_<?=$d?>" value="<?=$rowImportar['fldCodigo'];?>" readonly="readonly"/>
									<input class="hid_item_op_produto_id" type="hidden" id="hid_item_op_produto_id_<?=$i;?>" name="hid_item_op_produto_id_<?=$i;?>" value="<?=$rowImportar['fldProduto_Id'];?>" />
								</li>
								<li>
									<input class="txt_produto_op_desc" type="text" style="width:658px; text-align:left;" id="txt_produto_op_desc_<?=$i;?>" name="txt_produto_op_desc_<?=$i;?>" value="<?=$rowImportar['fldDescricao']?>" readonly="readonly" />
								</li>
								<li>
									<input class="txt_produto_op_qtd" type="text" style="width:45px" id="txt_produto_op_qtd_<?=$i;?>" name="txt_produto_op_qtd_<?=$i;?>" value="<?=$rowImportar['fldQuantidade']?>" readonly="readonly" />
								</li>
								<li>
									<input class="txt_produto_op_um" type="text" style="width:40px; text-align:center" id="txt_produto_op_um_<?=$i?>" name="txt_produto_op_um_<?=$i?>" title="<?=$rowImportar['fldUnidadeMedida']?>" value="<?=$rowImportar['fldSiglaUM']?>" readonly="readonly" />
								</li>
								<li>
									<input class="txt_produto_op_estocado" type="text" style="width:80px; text-align:center;" id="txt_produto_op_estocado_<?=$i;?>" name="txt_produto_op_estocado_<?=$i;?>" value="<?=format_number_out($rowImportar['fldEstoqueQuantidade'])?>" readonly="readonly" />
								</li>
                                <? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldMaquina") == "1"){ ?>
                                <li>
                                    <input type="text" style="width:100px" readonly="readonly" id="hid_item_op_fldMaquina_<?=$i?>" name="hid_item_op_fldMaquina_<?=$i?>" class="hid_item_op_fldMaquina" value="<?=$rowImportar['fldMaquina']?>" />
                                </li>
                                <?	} ?>
                                <? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldAcerto_Maquina") == "1"){ ?>
								<li>
                                    <input type="text" style="width:100px" readonly="readonly" id="hid_item_op_fldAcerto_Maquina_<?=$i?>" name="hid_item_op_fldAcerto_Maquina_<?=$i?>" class="hid_item_op_fldAcerto_Maquina" value="<?=$rowImportar['fldAcerto_Maquina']?>" />
                                </li>
                                <?	} ?>
                                <? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldPapel_Produzido") == "1"){ ?>
                                <li>
                                    <input type="text" style="width:100px" readonly="readonly" id="hid_item_op_fldPapel_Produzido_<?=$i?>" name="hid_item_op_fldPapel_Produzido_<?=$i?>" class="hid_item_op_fldPapel_Produzido" value="<?=$rowImportar['fldPapel_Produzido']?>" />
                               	</li>
                                <?	} ?>
                                <? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldRetorno_Papel") == "1"){ ?>
                                <li>
                                    <input type="text" style="width:100px" readonly="readonly" id="hid_item_op_fldRetorno_Papel_<?=$i?>" name="hid_item_op_fldRetorno_Papel_<?=$i?>" class="hid_item_op_fldRetorno_Papel" value="<?=$rowImportar['fldRetorno_Papel']?>" />
                               	</li>
                                <?	} ?>
                                <? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldPapel_Entregue") == "1"){ ?>
                                <li>
                                    <input type="text" style="width:100px" readonly="readonly" id="hid_item_op_fldPapel_Entregue_<?=$i?>" name="hid_item_op_fldPapel_Entregue_<?=$i?>" class="hid_item_op_fldPapel_Entregue" value="<?=$rowImportar['fldPapel_Entregue']?>" />
                                </li>
                                <?	} ?>
                                <? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldAcerto_Papel") == "1"){ ?>
                                <li>
                                    <input type="text" style="width:100px" readonly="readonly" id="hid_item_op_fldAcerto_Papel_<?=$i?>" name="hid_item_op_fldAcerto_Papel_<?=$i?>" class="hid_item_op_fldAcerto_Papel" value="<?=$rowImportar['fldAcerto_Papel']?>" />
                                </li>
                                <?	} ?>
                                <? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldPapel_Perdido") == "1"){ ?>
                                <li>
                                    <input type="text" style="width:100px" readonly="readonly" id="hid_item_op_fldPapel_Perdido_<?=$i?>" name="hid_item_op_fldPapel_Perdido_<?=$i?>" class="hid_item_op_fldPapel_Perdido" value="<?=$rowImportar['fldPapel_Perdido']?>" />
                                </li>
                                <?	} ?>
                                <? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldQtd_Perdida") == "1"){ ?>
                                <li>
                                    <input type="text" style="width:100px" readonly="readonly" id="hid_item_op_fldQtd_Perdida_<?=$i?>" name="hid_item_op_fldQtd_Perdida_<?=$i?>" class="hid_item_op_fldQtd_Perdida" value="<?=$rowImportar['fldQtd_Perdida']?>" />
                                </li>
                                <?	} ?>
                                <? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldTotal_Etiquetas") == "1"){ ?>
                                <li>
                                    <input type="text" style="width:100px" readonly="readonly" id="hid_item_op_fldTotal_Etiquetas_<?=$i?>" name="hid_item_op_fldTotal_Etiquetas_<?=$i?>" class="hid_item_op_fldTotal_Etiquetas" value="<?=$rowImportar['fldTotal_Etiquetas']?>" />
                                </li>
                                <?	} ?>
                                <? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldAmostra") == "1"){ ?>
                                <li>
                                    <input type="text" style="width:100px" readonly="readonly" id="hid_item_op_fldAmostra_<?=$i?>" name="hid_item_op_fldAmostra_<?=$i?>" class="hid_item_op_fldAmostra" value="<?=$rowImportar['fldAmostra']?>" />
                                    <input type="hidden" style="width:100px" readonly="readonly" id="hid_item_op_fldAmostra_De_<?=$i?>" name="hid_item_op_fldAmostra_De_<?=$i?>" class="hid_item_op_fldAmostra_De" value="<?=$rowImportar['fldAmostra_De']?>" />
                                </li>
                                <?	} ?>
                                <? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldRemetente") == "1"){ ?>
                                <li>
                                    <input type="text" style="width:100px" readonly="readonly" id="hid_item_op_fldRemetente_<?=$i?>" name="hid_item_op_fldRemetente_<?=$i?>" class="hid_item_op_fldRemetente" value="<?=$rowImportar['fldRemetente']?>" />
                                </li>
                                <?	} ?>
                                <? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldContagem_Funcionario_Id") == "1"){ ?>
                                <li>
                                <?	$rowFuncionario = mysql_fetch_assoc(mysql_query("select * from tblfuncionario where fldId = ".$rowImportar['fldContagem_Funcionario_Id'])); ?>
                                    <input type="text" style="width:100px" readonly="readonly" id="hid_item_op_fldContagem_Funcionario_Id_Label_<?=$i?>" class="hid_item_op_fldContagem_Funcionario_Id_Label" value="<?=$rowFuncionario['fldNome']?>" />
                                    <input type="hidden" style="width:100px" readonly="readonly" id="hid_item_op_fldContagem_Funcionario_Id_<?=$i?>" name="hid_item_op_fldContagem_Funcionario_Id_<?=$i?>" class="hid_item_op_fldContagem_Funcionario_Id" value="<?=$rowImportar['fldContagem_Funcionario_Id']?>" />
                                </li>
                                <?	} ?>
                                <? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldEmpacotagem_Funcionario_Id") == "1"){ ?>
                                <li>
                                <?	$rowFuncionario = mysql_fetch_assoc(mysql_query("select * from tblfuncionario where fldId = ".$rowImportar['fldEmpacotagem_Funcionario_Id'])); ?>
                                    <input type="text" style="width:100px" readonly="readonly" id="hid_item_op_fldEmpacotagem_Funcionario_Id_Label_<?=$i?>" class="hid_item_op_fldEmpacotagem_Funcionario_Id_Label" value="<?=$rowFuncionario['fldNome']?>" />
                                    <input type="hidden" style="width:100px" readonly="readonly" id="hid_item_op_fldEmpacotagem_Funcionario_Id_<?=$i?>" name="hid_item_op_fldEmpacotagem_Funcionario_Id_<?=$i?>" class="hid_item_op_fldEmpacotagem_Funcionario_Id" value="<?=$rowImportar['fldEmpacotagem_Funcionario_Id']?>" />
                                </li>
                                <?	} ?>
                                <? 	if(fnc_campo_auxiliar("tblop_item_auxiliar_controle", "fldQtd_Pacote") == "1"){ ?>
                                <li>
                                    <input type="text" style="width:100px" readonly="readonly" id="hid_item_op_fldQtd_Pacote_<?=$i?>" name="hid_item_op_fldQtd_Pacote_<?=$i?>" class="hid_item_op_fldQtd_Pacote" value="<?=$rowImportar['fldQtd_Pacote']?>" />
								</li>
                                <?	} ?>
							</ul>
<?						$i+=1; } ?>
	               	</div>
					
                    <div><input class="hid_controle" type="hidden" name="hid_controle" id="hid_controle" value="<?=$i?>" /></div>
					
				</div> <!-- div de produtos-->

				<h3 style="padding:0; margin:10px 0">Componentes</h3>

				<div style="width:962px; background:#6C85C6; margin:10px 0; clear:both">
                    <div id="pedido_lista_componente" style="width:960px; margin-top:0px;">
                        <ul id="pedido_lista_cabecalho" style="width:960px; padding:3px 0 0 0">
							<li style="border-right:0; width:10px"></li>
                            <li style="width:450px; text-align:left;">Descri&ccedil;&atilde;o</li>
                            <li style="width:60px">Qtd</li>
							<li style="width:40px;" title="Unidade de Medida">U.M</li>
							<li style="border-right:0; width:10px"></li>
							<li style="width:386px; border-right:0; text-align:left;">Usado para:</li>
                        </ul>

						<div id="hidden">
							<ul id="pedido_lista_item_componente" style="width:960px">
								<li>
									<input type="hidden" name="hid_componente_id" id="hid_componente_id" class="hid_componente_id" value="" />
									<input class="txt_componente_op_desc" type="text" style="width:460px; text-align:left;" id="txt_componente_op_desc" name="txt_componente_op_desc" value="" readonly="readonly" />
								</li>
								<li>
									<input class="txt_componente_op_qtd" type="text" style="width:60px" id="txt_componente_op_qtd" name="txt_componente_op_qtd" value="" readonly="readonly" />
								</li>
								<li>
									<input class="txt_componente_op_um" type="text" style="width:40px; text-align:center" id="txt_componente_op_um" name="txt_componente_op_um" value="" readonly="readonly" />
								</li>
								<li>
									<input class="txt_componente_usado_para" type="text" style="width:396px; text-align:left;" id="txt_componente_usado_para" name="txt_componente_usado_para" value="" readonly="readonly" />
								</li>
							</ul>							
						</div>

<?				$sqlImportar = mysql_query("SELECT
											fldComponente_Id AS componenteID,
											fldProduto_Id,
											fldDescricao,
											SUM(fldComponente_Qtd) AS fldComponente_Qtd,
											(SELECT fldSigla FROM tblproduto_unidade_medida WHERE fldId = (SELECT fldUN_Medida_Id FROM tblproduto WHERE fldId = fldComponente_Id)) AS fldUM,
											GROUP_CONCAT(DISTINCT (SELECT fldNome FROM tblproduto WHERE fldId = fldProduto_Id) SEPARATOR ', ') AS fldUsadoPara,
											GROUP_CONCAT(DISTINCT CONVERT((SELECT fldId FROM tblproduto WHERE fldId = fldProduto_Id), CHAR(255)) SEPARATOR ', ') AS fldUsadoParaId
										FROM `tblop_item_componente`
											WHERE fldOP_Id = $ordem_id AND fldExcluido = '0' GROUP BY fldComponente_Id");
					$i = 1;
					
					###########APENAS PARA VISUALIZACAO, NO BD SAO INSERIDOS PRODUTO E COMPONENTE INDIVIDUALMENTE!##############
						while($rowImportar = mysql_fetch_assoc($sqlImportar)){ ?>
							<ul id="pedido_lista_item_componente" style="width:960px">
								<li>
									<input type="hidden" name="hid_componente_id_<?=$i?>" id="hid_componente_id_<?=$i?>" class="hid_componente_id_<?=$rowImportar['componenteID'];?>" value="<?=$rowImportar['componenteID'];?>" />
									<input class="txt_componente_op_desc" type="text" style="width:460px; text-align:left;" id="txt_componente_op_desc_<?=$i;?>" name="txt_componente_op_desc_<?=$i;?>" value="<?=$rowImportar['fldDescricao']?>" readonly="readonly" />
								</li>
								<li>
									<input class="txt_componente_op_qtd" type="text" style="width:60px" id="txt_componente_op_qtd_<?=$i;?>" name="txt_componente_op_qtd_<?=$i;?>" value="<?=format_number_out($rowImportar['fldComponente_Qtd'])?>" readonly="readonly" />
								</li>
								<li>
									<input class="txt_componente_op_um" type="text" style="width:40px; text-align:center" id="txt_componente_op_um" name="txt_componente_op_um" value="<?=$rowImportar['fldUM']?>" readonly="readonly" />
								</li>
								<li>
									<input class="txt_componente_usado_para" type="text" style="width:396px; text-align:left;" id="txt_componente_usado_para" name="txt_componente_usado_para" value="<?=$rowImportar['fldUsadoPara'];?>" readonly="readonly" />
								</li>
							</ul>
<?					$i+=1; }  ?>
	              	</div>
                </div>

                <div style="float:right">
                    <input type="submit" style="margin:10px 0 0 0" class="btn_enviar" name="btn_gravar_op" id="btn_gravar_op" value="Gravar" title="Gravar" />
                </div>
            </form>

		</div>
<?	} ?>    
</div>