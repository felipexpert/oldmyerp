<?php
	
	//recebendo a data do calendário de período	
	$_SESSION['txt_compra_item_data_inicial'] 		= (isset($_SESSION['txt_compra_item_data_inicial'])) ? $_SESSION['txt_compra_item_data_inicial'] 	: format_date_out(date('Y') . '-' . date('m') . '-' . date("d", mktime(0, 0, 0, date('m'), 1, date('Y')) + 1));
	$_SESSION['txt_compra_item_data_final'] 		= (isset($_SESSION['txt_compra_item_data_final'])) 	? $_SESSION['txt_compra_item_data_final'] 	: format_date_out(date('Y') . '-' . date('m') . '-' . date("d", mktime(0, 0, 0, date('m') + 1, 1, date('Y')) - 1));
	
	$_SESSION['txt_compra_item_data_inicial'] 		= (isset($_POST['txt_calendario_data_inicial'])) 	? $_POST['txt_calendario_data_inicial'] : $_SESSION['txt_compra_item_data_inicial'];
	$_SESSION['txt_compra_item_data_final'] 		= (isset($_POST['txt_calendario_data_final'])) 		? $_POST['txt_calendario_data_final'] 	: $_SESSION['txt_compra_item_data_final'];
	
	//memorizar os filtros para exibição nos selects
	if(isset($_POST['btn_limpar'])){
		$_SESSION['txt_compra_item_data_inicial'] 	 = format_date_out(date('Y') . '-' . date('m') . '-' . date("d", mktime(0, 0, 0, date('m'), 1, date('Y')) + 1));
		$_SESSION['txt_compra_item_data_final'] 	 = format_date_out(date('Y') . '-' . date('m') . '-' . date("d", mktime(0, 0, 0, date('m') + 1, 1, date('Y')) - 1));
		$_SESSION['txt_compra_item_cod']			 = "";
		$_SESSION['txt_compra_item_desc'] 			 = "";
		$_SESSION['sel_compra_item_fornecedor'] 	 = "";
		$_SESSION['txt_compra_nota'] 				 = "";
		$_SESSION['txt_compra_item_ref'] 			 = "";
		$_POST['chk_item_desc'] 					 = false;
	}
	else{
		$_SESSION['txt_compra_item_data_inicial'] 	= (isset($_POST['txt_compra_item_data_inicial']) ? $_POST['txt_compra_item_data_inicial'] : $_SESSION['txt_compra_item_data_inicial']);
		$_SESSION['txt_compra_item_data_final']   	= (isset($_POST['txt_compra_item_data_final']) ? $_POST['txt_compra_item_data_final'] : $_SESSION['txt_compra_item_data_final']);
		$_SESSION['txt_compra_item_cod'] 		  	= (isset($_POST['txt_compra_item_cod']) ? $_POST['txt_compra_item_cod'] : $_SESSION['txt_compra_item_cod']);
		$_SESSION['txt_compra_item_desc'] 			= (isset($_POST['txt_compra_item_desc']) ? $_POST['txt_compra_item_desc'] : $_SESSION['txt_compra_item_desc']);
		$_SESSION['sel_compra_item_fornecedor'] 	= (isset($_POST['sel_compra_item_fornecedor']) ? $_POST['sel_compra_item_fornecedor'] : $_SESSION['sel_compra_item_fornecedor']);
		$_SESSION['txt_compra_nota'] 				= (isset($_POST['txt_compra_nota']) ? $_POST['txt_compra_nota'] : $_SESSION['txt_compra_nota']);
		$_SESSION['txt_compra_item_ref'] 			= (isset($_POST['txt_compra_item_ref']) ? $_POST['txt_compra_item_ref'] : $_SESSION['txt_compra_item_ref']);
	}
	
?>
<form id="frm-filtro" action="index.php?p=compra&amp;modo=item" method="post">
	<fieldset>
  	<legend>Buscar por:</legend>
    <ul>
		<li>
			<label for="txt_compra_item_data_inicial">Per&iacute;odo: </label>
	      	<input title="Data inicial" style="width: 70px; text-align: center;" class="calendario-mask" type="text" name="txt_compra_item_data_inicial" id="txt_compra_item_data_inicial" value="<?=$_SESSION['txt_compra_item_data_inicial']?>" />
		</li>
		<li>
	      	<input title="Data final" style="width: 70px; text-align: center;" class="calendario-mask" type="text" name="txt_compra_item_data_final" id="txt_compra_item_data_final" value="<?=$_SESSION['txt_compra_item_data_final']?>" />
			<a href="calendario_periodo,<?=format_date_in($_SESSION['txt_compra_item_data_inicial']) . ',' . format_date_in($_SESSION['txt_compra_item_data_final'])?>,compra&modo=item" id="exibir-calendario" title="Exibir calend&aacute;rio" class="modal calendario-modal" rel="600-320"></a>
		</li>
    	<li>
<?			$codigo_compra_item = ($_SESSION['txt_compra_item_cod'] ? $_SESSION['txt_compra_item_cod'] : "c&oacute;d. compra");
			($_SESSION['txt_compra_item_cod'] == "cód. compra") ? $_SESSION['txt_compra_item_cod'] = '' : '';
?>      	<input style="width: 75px" type="text" name="txt_compra_item_cod" id="txt_compra_item_cod" onfocus="limpar (this,'c&oacute;d. compra');" onblur="mostrar (this, 'c&oacute;d. compra');" value="<?=$codigo_compra_item?>"/>
		</li>
        <li>
        	<input type="checkbox" name="chk_item_desc" id="chk_item_desc" <?=($_POST['chk_item_desc'] == true ? print 'checked ="checked"' : '')?>/>
<?			$item_descricao = ($_SESSION['txt_compra_item_desc'] ? $_SESSION['txt_compra_item_desc'] : "item");
			($_SESSION['txt_compra_item_desc'] == "item") ? $_SESSION['txt_compra_item_desc'] = '' : '';
?>      	<input style="width: 165px" type="text" name="txt_compra_item_desc" id="txt_compra_item_desc" onfocus="limpar (this,'item');" onblur="mostrar (this, 'item');" value="<?=$item_descricao?>"/>
			<small>marque para qualquer parte do campo</small>
        </li>
        <li>
            <select style="width: 135px" id="sel_compra_item_fornecedor" name="sel_compra_item_fornecedor" >
                <option value="">Fornecedor</option>
<?				$rsFornecedor = mysql_query("select fldId, fldNomeFantasia from tblfornecedor order by fldNomeFantasia");
				while($rowFornecedor = mysql_fetch_array($rsFornecedor)){
?>					<option <?=($_SESSION['sel_compra_item_fornecedor'] == $rowFornecedor['fldId']) ? 'selected="selected"' : '' ?> value="<?= $rowFornecedor['fldId'] ?>"><?= $rowFornecedor['fldNomeFantasia'] ?></option>
<?				}
?>			</select>
		</li>
		<li>
<?			$compra_nota = ($_SESSION['txt_compra_nota'] ? $_SESSION['txt_compra_nota'] : "n&ordm; da nota");
			($_SESSION['txt_compra_nota'] == "nº da nota") ? $_SESSION['txt_compra_nota'] = '' : '';
?>      	<input style="width: 105px" type="text" name="txt_compra_nota" id="txt_compra_nota" onfocus="limpar (this,'n&ordm; da nota');" onblur="mostrar (this, 'n&ordm; da nota');" value="<?=$compra_nota?>" />
        </li>
		<li>
<?			$item_referencia = ($_SESSION['txt_compra_item_ref'] ? $_SESSION['txt_compra_item_ref'] : "refer&ecirc;ncia");
			($_SESSION['txt_compra_item_ref'] == "referência") ? $_SESSION['txt_compra_item_ref'] = '' : '';
?>      	<input style="width: 105px" type="text" name="txt_compra_item_ref" id="txt_compra_item_ref" onfocus="limpar (this,'refer&ecirc;ncia');" onblur="mostrar (this, 'refer&ecirc;ncia');" value="<?=$item_referencia?>" />
        </li>
    </ul>
    
    <button type="submit" name="btn_exibir" title="Exibir">Exibir</button>
    <button type="submit" name="btn_limpar" title="Limpar Filtro">Limpar filtro</button>
  </fieldset>
</form>

<?

	if(format_date_in($_SESSION['txt_compra_item_data_inicial']) != "" || format_date_in($_SESSION['txt_compra_item_data_final']) != ""){
			$filtro .= " AND tblcompra.fldCompraData BETWEEN '".format_date_in($_SESSION['txt_compra_item_data_inicial'])."' AND '".format_date_in($_SESSION['txt_compra_item_data_final'])."'";
	}

	if(($_SESSION['txt_compra_item_cod']) != ""){
		
		$filtro .= "and tblcompra_item.fldCompra_Id = '".$_SESSION['txt_compra_item_cod']."'";
	}
	
	if(($_SESSION['txt_compra_item_desc']) != ""){
		
		if($_POST['chk_item_desc'] == true){
			$filtro .= "and tblcompra_item.fldDescricao like '%".$_SESSION['txt_compra_item_desc']."%'";
		}else{
			$filtro .= "and tblcompra_item.fldDescricao like '".$_SESSION['txt_compra_item_desc']."%'";
		}
	}
	
	if(($_SESSION['sel_compra_item_fornecedor']) != ""){
		
		$filtro .= "and tblfornecedor.fldId = '".$_SESSION['sel_compra_item_fornecedor']."'";
	}
	
	if(($_SESSION['txt_compra_nota']) != ""){
		
		$filtro .= "and tblcompra.fldNota = '".$_SESSION['txt_compra_nota']."'";
	}
	
	if(($_SESSION['txt_compra_item_ref']) != ""){
		
		$filtro .= "and tblcompra_item.fldReferencia like '%".$_SESSION['txt_compra_item_ref']."%'";
	}

	//transferir para a sessão
	$_SESSION['filtro_compra_item'] = $filtro;

?>
