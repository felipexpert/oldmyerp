<?php
	$filtro = '';
	
	$data2 	= date('Y-m-d');
	$data1 	= date('Y-m-d', strtotime($data2 .' - 1 month'));

	if($_SESSION['funcionario_ponto_filtro_data1'] == ""){
		$_SESSION['funcionario_ponto_filtro_data1'] = format_date_out($data1);
	}
	if($_SESSION['funcionario_ponto_filtro_data2'] == "" || $_POST['txt_ponto_data2'] > date('d-m-Y')){
		$_SESSION['funcionario_ponto_filtro_data2'] = format_date_out($data2);
	}

	//memorizar os filtros para exibição nos selects
	if(isset($_POST['btn_limpar'])){
		
		$_SESSION['funcionario_ponto_filtro_data1']	= format_date_out($data1);
		$_SESSION['funcionario_ponto_filtro_data2']	= format_date_out($data2);
	}else{
		#NAO PODE FICAR SEM DATA O FILTRO
		$_SESSION['funcionario_ponto_filtro_data1']	= (!empty($_POST['txt_ponto_data1'])) ? $_POST['txt_ponto_data1'] : $_SESSION['funcionario_ponto_filtro_data1'];
		$_SESSION['funcionario_ponto_filtro_data2']	= (!empty($_POST['txt_ponto_data2']) && $_POST['txt_ponto_data2'] <= date('d/m/Y')) ? $_POST['txt_ponto_data2'] : $_SESSION['funcionario_ponto_filtro_data2'];
	}
	
?>

<form id="frm-filtro" action="index.php?p=funcionario_detalhe&modo=ponto_listar&id=<?=$funcionario_id?>" method="post">
	<fieldset>
  	<legend>Buscar por:</legend>
    <ul>
        <li class="large_height">
      		<label for="txt_data_ponto1">Per&iacute;odo</label>
<?			$data1 = $_SESSION['funcionario_ponto_filtro_data1'];
?>     		<input style="text-align:center;width: 70px" type="text" name="txt_ponto_data1" id="txt_ponto_data1" class="calendario-mask" value="<?=$data1?>"/>
      	</li>
        <li class="large_height">
      		<label for="txt_data_ponto2"></label>
<?			$data2 = $_SESSION['funcionario_ponto_filtro_data2'];
?>     		<input style="text-align:center;width: 70px" type="text" name="txt_ponto_data2" id="txt_ponto_data2" class="calendario-mask" value="<?=$data2?>"/>
			<a href="calendario_periodo,<?=format_date_in($data1) . ',' . format_date_in($data2)?>,funcionario_detalhe&modo=ponto_listar&id=<?=$funcionario_id?>" id="exibir-calendario" title="Exibir calend&aacute;rio" class="modal calendario-modal" rel="600-320"></a>
      	</li>
        <li style="float:right">
      	    <button type="submit" name="btn_exibir" title="Exibir">Exibir</button>
    		<button type="submit" name="btn_limpar" title="Limpar Filtro">Limpar filtro</button>
        </li>
    </ul>
    
  </fieldset>
</form>

<?
	if(isset($_SESSION['funcionario_ponto_filtro_data1']) && isset($_SESSION['funcionario_ponto_filtro_data2'])){
		$filtro = " AND fldData between '".format_date_in($_SESSION['funcionario_ponto_filtro_data1'])."' AND '".format_date_in($_SESSION['funcionario_ponto_filtro_data2'])."'";
	}
	
	//transferir para a sessão
	$_SESSION['filtro_funcionario_ponto'] = $filtro;
	
?>
