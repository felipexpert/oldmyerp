<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
      	<title>myERP - Imprimir venda</title>
        
	</head>
	<body>
<?php
		ob_start();
		session_start();
		
		require_once("inc/con_db.php");
		require_once("inc/fnc_ibge.php");
		require_once("inc/fnc_general.php");
		require_once("inc/fnc_imprimir.php");
		require_once("inc/fnc_identificacao.php");
		$usuario_sessao = $_SESSION['usuario_id'];
		
		$impressao_local = fnc_estacao_impressora($_SESSION['remote_name']);
		if(!$impressao_local){
			$impressao_local = fnc_estacao_impressora('todos');
		}
		$texto 		= $impressao_local." \r\n";
	
		$pedido_id  = $_GET['id'];
		$raiz 		= $_GET['raiz'];
		$data 		= date("Y-m-d");
		
		$vendaDecimal 		= fnc_sistema('venda_casas_decimais');
		$quantidadeDecimal 	= fnc_sistema('quantidade_casas_decimais');
		
		
		$rsPedido  			= mysql_query("SELECT tblpedido.*, SUM((tblpedido_item.fldValor * (tblpedido_item.fldExcluido * -1 + 1)) * tblpedido_item.fldQuantidade) as fldTotalItem,
								(SELECT SUM(fldValor) FROM tblpedido_funcionario_servico WHERE fldFuncao_Tipo = 2 AND fldPedido_Id = tblpedido.fldId) as fldTotalServico,
								tblpedido.fldVeiculo_Id, tblpedido.fldEndereco AS fldEnderecoPedido, tblpedido.fldReferencia, tblpedido.fldPedidoData, tblpedido.fldServico, tblpedido.fldDependente_Id, tblpedido.fldDesconto, tblpedido.fldValor_Terceiros, tblpedido.fldId as fldPedidoId, tblpedido.fldDescontoReais, tblcliente.*, tblcliente.fldId AS clienteID
								FROM tblpedido 
								LEFT JOIN tblpedido_item ON tblpedido.fldId = tblpedido_item.fldPedido_Id
								LEFT JOIN tblcliente ON tblpedido.fldCliente_Id = tblcliente.fldId
								WHERE tblpedido.fldId = $pedido_id GROUP BY tblpedido_item.fldPedido_Id");
		$rowPedido 			= mysql_fetch_array($rsPedido);
		$cliente_id			= $rowPedido['fldCliente_Id'];
		/*----------------------------------------------------------------------------------------------*/
		
		$rsEmpresa  = mysql_query("SELECT * FROM tblempresa_info");
		$rowEmpresa = mysql_fetch_array($rsEmpresa);
		
		$municipio 	= fnc_ibge_municipio($rowEmpresa['fldMunicipio_Codigo']);
		$siglaUF	= fnc_ibge_uf_sigla($rowEmpresa['fldMunicipio_Codigo']);

		$endereco 	= $rowEmpresa['fldEndereco'];
		$numero 	= $rowEmpresa['fldNumero'];
		$bairro 	= $rowEmpresa['fldBairro'];
		$cidade 	= $rowMunicipio_Empresa['fldNome'];
		$uf 		= $rowMunicipio_Empresa['fldSigla'];
		$site		= $rowEmpresa['fldWebsite'];
		
		$texto .= format_margem_print(acentoRemover($rowEmpresa['fldNome_Fantasia']),40,'esquerda')." \r\r\n";
		$texto .="Fone: ". $rowEmpresa['fldTelefone1']." \r\n";
		$texto .="Data: ".format_date_out(date("Y-m-d"))." Hora: ".date("H:i:s")." \r\n\r\n";
		$texto .="Data venda: ".format_date_out($rowPedido['fldPedidoData'])." \r\n";
		$texto .="Num. venda: ".str_pad($rowPedido['fldPedidoId'], 6, "0", STR_PAD_LEFT)."     Ref.:".$rowPedido['fldReferencia']." \r\n";
		$texto .="Cli: ".str_pad($rowPedido['clienteID'], 5, 0, STR_PAD_LEFT)."  ".acentoRemover($rowPedido['fldNome']);
		if(fnc_sistema('exibir_dependente') > 0){
			$rowDependente = mysql_fetch_array(mysql_query("SELECT fldNome FROM tblcliente WHERE fldId =".$rowPedido['fldDependente_Id']));
			$texto .="\r\nDependente: ".acentoRemover($rowDependente['fldNome']);
		}
		if($_SESSION['sistema_tipo'] == 'automotivo'){
			$rowPlaca = mysql_fetch_array(mysql_query("SELECT fldPlaca FROM tblcliente_veiculo WHERE fldId =".$rowPedido['fldVeiculo_Id']));
			$texto .="\r\nPlaca: ".$rowPlaca['fldPlaca'];
		}
		if(fnc_sistema('pedido_exibir_endereco') > 0){

			$endereco_pedido = explode(', ', $rowPedido['fldEnderecoPedido']);

			if($endereco_pedido == $rowPedido['fldEndereco'] || $rowPedido['fldEnderecoPedido'] == ''){
				$rowEndereco = mysql_fetch_array(mysql_query("SELECT fldEndereco, fldNumero, fldBairro FROM tblcliente WHERE fldId = $cliente_id"));
				$endereco 	 = $rowEndereco['fldEndereco'].', '.$rowEndereco['fldNumero']."\r\n";
				if(fnc_sistema('pedido_exibir_endereco') != '2'){
					$endereco 	 .= $rowEndereco['fldBairro'];
				}

				$texto .= "\r\nEND: ".strtoupper(acentoRemover($endereco));
			}else{
				$rowEndereco = mysql_fetch_array(mysql_query("SELECT fldEndereco, fldNumero, fldBairro FROM tblcliente WHERE fldId = $cliente_id"));
				$endereco 	 = $rowEndereco['fldEndereco'].', '.$rowEndereco['fldNumero'];
				if(fnc_sistema('pedido_exibir_endereco') != '2'){
					$endereco 	 .= "\r\n".$rowEndereco['fldBairro']."\r\n";
				}
				$texto .= "\r\nEND: ".strtoupper(acentoRemover($endereco))."\r\n";
				$texto .= "ENTREGA: ".strtoupper(acentoRemover($rowPedido['fldEnderecoPedido']));
			}
		}		
		
		$texto .="\r\n\r\nCod. Produto             Nome do produto \r\n";
		$texto .="----------------------------------------\r\n";
						
		$rsItem = mysql_query("SELECT 
								tblpedido_item.fldId,
								tblpedido_item.fldProduto_Id,
								tblpedido_item.fldPedido_Id,
								tblpedido_item.fldValor as fldValor,
								tblpedido_item.fldValor_Compra as fldValor_Compra,
								SUM(tblpedido_item.fldDesconto) as fldDesconto,
								SUM(tblpedido_item.fldQuantidade) as fldQuantidade,
								tblpedido_item.fldDescricao,
								tblpedido_item.fldExcluido,
								tblpedido_item.fldTabela_Preco_Id,
								tblpedido_item.fldFardo_Id,
								tblpedido_item.fldEstoque_Id,
								tblpedido_item.fldReferencia,
								tblpedido_item.fldEntregue,
								tblpedido_item.fldEntregueData,
								tblpedido_item.fldExibicaoOrdem,
								tblpedido_item.fldLote,
								tblpedido_item.fldProduto_Tipo_Id,
								tblproduto.fldCodigo 
								FROM tblpedido_item INNER JOIN tblproduto ON tblpedido_item.fldProduto_Id = tblproduto.fldId
								WHERE tblpedido_item.fldExcluido = '0'
								and tblpedido_item.fldPedido_Id = '".$rowPedido['fldPedidoId']."'
								group by fldProduto_Id");
		while($rowItem = mysql_fetch_array($rsItem)){
		
			$quantidade 	= $rowItem['fldQuantidade'];
			$valor 			= $rowItem['fldValor'];
			$total 			= $valor * $quantidade;
			$desconto 		= $rowItem['fldDesconto'];
			$descontoItem 	= ($total * $desconto) / 100;
			$totalItem 		= $total - $descontoItem;
	
			/*** gravando no txt ********************************************************/
			$texto.= format_margem_print($rowItem['fldCodigo'],10)." ";
			
			//AQUI BUSCAR DADOS DA NFE
			$rowItemNFe 	= mysql_fetch_array(mysql_query("SELECT * FROM tblpedido_item_fiscal WHERE fldItem_Id= " .$rowItem['fldId']));
			$complemento 	= (isset($rowItemNFe['fldinformacoes_adicionais'])) ? ' - '.$rowItemNFe['fldinformacoes_adicionais']: '' ;
			$descricao 		= $rowItem['fldDescricao'].$complemento;
		
			$x = 0; //quebrando linha a cada 29 caracteres
			while(strlen(substr($descricao,$x * 29, 29)) >0){
				$texto .= substr(acentoRemover($descricao),$x * 29, 29) . "\r\n";
				$x +=1;
			}
			
			/*** gravando no txt ********************************************************/
			$texto.= format_margem_print(format_number_out($quantidade,$quantidadeDecimal),7)." x ". format_margem_print(format_number_out($valor,$vendaDecimal),10)." ".format_margem_print(format_number_out($desconto),6)."% ".format_margem_print(format_number_out($totalItem),11)."\r\n";
			$total_item += $totalItem;
		}
	
		$total_pedido 			= $total_item + $rowPedido['fldTotalServico'] + $rowPedido['fldValor_Terceiros'];
		$desconto_pedido 		= $rowPedido['fldDesconto'];
		$desconto_reais_pedido 	= $rowPedido['fldDescontoReais'];
		$comissao_garcom	 	= $rowPedido['fldComissao'];
		
		$total_pedido_apagar	= $total_pedido + $comissao_garcom;
		$desconto 				= ($total_pedido_apagar * $desconto_pedido) / 100;
		$total_descontos 		= $desconto + $desconto_reais_pedido;
		
		$total_pedido_apagar 	= ($total_pedido_apagar - $total_descontos);
		
		/**PARCELAS **********************************/
		$sSQL = "SELECT tblpedido_parcela.fldId, SUM(tblpedido_parcela_baixa.fldValor * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldBaixaValor
				FROM tblpedido_parcela LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
				WHERE tblpedido_parcela.fldPedido_Id = $pedido_id AND tblpedido_parcela.fldExcluido = 0 GROUP BY tblpedido_parcela.fldPedido_Id";
		$rsBaixa = mysql_query($sSQL);
		
		$rowBaixa 				= mysql_fetch_array($rsBaixa);
		echo mysql_error();
		$valorBaixa 			= $rowBaixa['fldBaixaValor'];
		$valorDevedor 			= $total_pedido_apagar - $rowBaixa['fldBaixaValor'];
	
		$total_produto	 		= format_number_out($total_item);
		$total_servico			= format_number_out($rowPedido['fldTotalServico']);
		$total_terceiros		= format_number_out($rowPedido['fldValor_Terceiros']);
		$desconto_pedido 		= format_number_out($desconto_pedido);
		$desconto_reais_pedido	= format_number_out($desconto_reais_pedido);
		$valorBaixa 			= format_number_out($valorBaixa);
		$valorDevedor 			= format_number_out($valorDevedor);
		
		/*** gravando no txt ********************************************************/
		$texto 	.= "---------------------------------------- \r\n";
		if(fnc_sistema('pedido_produtos_impressao') == '1'){
			$texto 	.= "             Produtos +";
			$texto	.= format_margem_print($total_produto,17, 'direita')."\r\n";
		}

		if(fnc_sistema('pedido_servicos_impressao') == '1'){
			$texto 	.= "             Servicos +";
			$texto	.= format_margem_print($total_servico,17, 'direita')."\r\n";
		}

		if(fnc_sistema('pedido_terceiros_impressao') == '1'){
			$texto 	.= "            Terceiros +";
			$texto	.= format_margem_print($total_terceiros,17, 'direita')."\r\n";
		}

		if($rowPedido['fldComanda_Numero'] > 0){
				$texto 	.= "  Taxa Servico (10%) + ";
				$texto	.= format_margem_print(format_number_out($comissao_garcom),17, 'direita')."\r\n";
		}

		if(fnc_sistema('pedido_desconto_porcent_impressao') == '1'){
			$texto 	.= "         Desconto(%) - ";
			$texto	.= format_margem_print($desconto_pedido,17, 'direita')."\r\n";
		}

		if(fnc_sistema('pedido_desconto_reais_impressao') == '1'){
			$texto 	.= "        Desconto(R$) - ";
			$texto	.= format_margem_print($desconto_reais_pedido,17, 'direita')."\r\n";
		}
		
		$texto 	.= "               Total = ";
		$texto	.= format_margem_print(format_number_out($total_pedido_apagar),17, 'direita')."\r\n";
		
		$texto 	.= "          Total Pago = ";
		$texto	.= format_margem_print($valorBaixa,17, 'direita')."\r\n";
		
		$texto 	.= "       Total Devedor = ";
		$texto 	.= format_margem_print($valorDevedor,17, 'direita')."\r\n";
		$texto 	.="\r\n Parc Venc  Valor  Pago   Devedor  Forma\r\n";
		$texto 	.="---------------------------------------- \r\n";
		
		/**************************************************************************/
		$sSQL = "SELECT tblpedido_parcela.*, tblpagamento_tipo.fldSigla as fldForma_Pagamento, SUM(tblpedido_parcela_baixa.fldValor * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldTotalBaixa
						FROM tblpedido_parcela LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
						LEFT JOIN tblpagamento_tipo ON tblpedido_parcela.fldPagamento_Id = tblpagamento_tipo.fldId
						WHERE tblpedido_parcela.fldPedido_Id = $pedido_id AND tblpedido_parcela.fldExcluido = 0 GROUP BY tblpedido_parcela.fldId ORDER BY tblpedido_parcela.fldParcela";
		
		$rsParcela = mysql_query($sSQL);
		while($rowParcela = mysql_fetch_array($rsParcela)){
			
			$pago = $rowParcela['fldTotalBaixa'];
			if($rowParcela['fldTotalBaixa'] == 0 or $rowParcela['fldTotalBaixa'] == NULL ){
				$pago = "0.00";
			}
			
			$devedor 	= $rowParcela['fldValor'] - $pago;
			$valor 		= format_number_out($rowParcela['fldValor']);
			$parcela 	= str_pad($rowParcela['fldParcela'], 2, "0", STR_PAD_LEFT);
			$forma_pg 	= $rowParcela['fldForma_Pagamento'];
			/*** gravando no txt ********************************************************/
			$texto.= format_margem_print($parcela,4,'centro');
			$texto.= format_margem_print(format_date_out4($rowParcela['fldVencimento']),6,'esquerda');
			$texto.= format_margem_print($valor,9, 'direita');
			$texto.= format_margem_print(format_number_out($pago),9, 'direita');
			$texto.= format_margem_print(format_number_out($devedor),9, 'direita');
			$texto.= format_margem_print($forma_pg,3, 'direita')."\r\n";
		}
		
		/*** gravando no txt ********************************************************/
		$texto .="        Saldo devedor". format_margem_print($valorDevedor,19, 'direita')."\r\n\r\n";
		
		$data 	= date("d/m/Y");
		$rodape = strlen($municipio.", ".$data);

		$texto .= format_margem_print($municipio.", ".date("d/m/Y"),40, 'centro')."\r\n\r\n\r\n";
		$texto .= format_margem_print("------------------------------", 40, 'centro')." \r\n";
		$texto .= format_margem_print(acentoRemover($rowPedido['fldNome']),40,'centro')."\r\n\r\n\r\n";
		
		if(mysql_num_rows(mysql_query("SELECT * FROM tblsistema_impressao_campo WHERE fldCampo = 'entregador' AND fldImpressao = 'venda' AND fldExibir = 1"))){
			$texto .= format_margem_print("------------------------------", 40, 'centro')." \r\n";
			$texto .= format_margem_print("ENTREGADOR", 40, 'centro')." \r\n";
		}
		
		#APARECER DEVEDOR DE OUTRAS VENDAS  #####################################################################################################################
		if(fnc_sistema('pedido_exibir_devedor') > 0 && $cliente_id	> 0){
			$sSQL = "SELECT tblpedido_parcela.*, 
				SUM(tblpedido_parcela_baixa.fldValor * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldValorBaixa,
				SUM(tblpedido_parcela_baixa.fldJuros * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldJuros,
				SUM(tblpedido_parcela_baixa.fldMulta * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldMulta,
				SUM(tblpedido_parcela_baixa.fldDesconto * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldDesconto,
				tblpedido.fldId as fldPedidoId
				FROM 
				(tblpedido_parcela LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id)
				INNER JOIN tblpedido ON tblpedido.fldId = tblpedido_parcela.fldPedido_Id
				WHERE tblpedido.fldCliente_Id = $cliente_id
				AND tblpedido_parcela.fldStatus = '1' 
				AND tblpedido_parcela.fldExcluido = '0'
				AND (tblpedido.fldPedido_Destino_Nfe_Id IS NULL OR tblpedido.fldPedido_Destino_Nfe_Id <= 0) GROUP BY tblpedido_parcela.fldId";
			$rsParcela = mysql_query($sSQL);
			while($rowParcela = mysql_fetch_array($rsParcela)){	
				$valorBaixa = $rowParcela['fldValorBaixa'] + $rowParcela['fldJuros'] + $rowParcela['fldMulta'];
				
				$rsDevedor = mysql_query("SELECT fldDevedor FROM tblpedido_parcela_baixa WHERE fldParcela_Id = ".$rowParcela['fldId']." AND fldExcluido = 0 ORDER BY fldId desc LIMIT 1 ");
				$rowDevedor = mysql_fetch_array($rsDevedor);
				if(mysql_num_rows($rsDevedor)){
					$devedor = $rowDevedor['fldDevedor'];
				}else{
					$devedor = $rowParcela['fldValor'];
				}
				
				$pedido_id 	= format_number_out($rowParcela['fldPedidoId']);  
				$valor	 	= format_number_out($rowParcela['fldValor']); 
				
				$totalDevedor += $devedor;
				$pedido_id 	= str_pad($rowParcela['fldPedidoId'], 5, "0", STR_PAD_LEFT);
				
				if($devedor > 0){
					
					/*** gravando no txt ********************************************************/
					$rodape_devedor.= format_margem_print($pedido_id,7,'direita');
					$rodape_devedor.= format_margem_print(format_date_out4($rowParcela['fldVencimento']),7,'direita');
					$rodape_devedor.= format_margem_print($valor,10, 'direita');
					$rodape_devedor.= format_margem_print(format_number_out($valorBaixa),8, 'direita');
					$rodape_devedor.= format_margem_print(format_number_out($devedor),8, 'direita')."\r\n";
					
				}
			}
			
			if($totalDevedor > 0 ){
				
				$texto 	.="\r\n Venda  Venc      Valor   Pago   Devedor  \r\n";
				$texto 	.="---------------------------------------- \r\n";
				$texto	.=$rodape_devedor."\r\n\r\n\r\n";	
			}
		}
		# MENSAGENS #############################################################################################################################################
		$rsMensagem = mysql_query("SELECT * FROM tblsistema_impressao_mensagem WHERE fldImpressao = 'venda' ORDER BY fldOrdem");
		while($rowMensagem = mysql_fetch_array($rsMensagem)){
			$mensagem = strtoupper(acentoRemover($rowMensagem['fldMensagem']));
			$x = 0; //quebrando linha a cada 36 caracteres
			while(strlen(substr($mensagem,$x * 36, 36)) > 0){
				$texto .= format_margem_print(substr($mensagem,$x * 36, 36),40, 'centro')." \r\n";
				$x +=1;
			}
		}
		
		if($_GET['np'] > 0){
			$texto .="\r\n\r\n\r\n";
		}else{
			$texto .="\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n";
		}
		#########################################################################################################################################################
		
		$timestamp  = date("Ymd_His");
		//$local_file = "impressao\inbox\imprimir_pedido_$timestamp.txt"; // Definimos o local para salvar o arquivo de texto
		$local_file = "impressao///inbox///imprimir_pedido_$timestamp.txt"; // Definimos o local para salvar o arquivo de texto
		$fp 		= fopen($local_file, "w+"); //utilizamos o operador w+ para criar o arquivo imprimir.txt, e APAGAR tudo que já existe nele, caso ele já exista.
		$salva		= fwrite($fp, $texto);
		fclose($fp);
	
		unset($total_item);
		if($_GET['np'] > 0){
			require('pedido_imprimir_np_nfiscal.php');
		}
?>    
	<script type="text/javascript">
		window.close();
	</script>
</html>	
