﻿<?	
	($_SESSION['sel_conta_id'] == '')	? $_SESSION['sel_conta_id'] = 1 : ''; //SE NAO FOI SELECIONADA CONTA AINDA, FILTRA COM A CONTA DO MOVIMENTO CAIXA
	($_GET['conta_id'] != '') 			? $_SESSION['sel_conta_id'] = $_GET['conta_id'] : '';
	
	$contaId 	= $_SESSION['sel_conta_id'];
	$data_caixa = fnc_caixa_data($contaId);	
	if($_GET['fecha'] == 'ok'){
		$_SESSION['txt_conta_data1'] = format_date_out($data_caixa);
		$_SESSION['txt_conta_data2'] = format_date_out($data_caixa);
	}
	
	$data_atual = date("Y-m-d");
	if(isset($_POST['btn_fecha_caixa'])){
		$conta = $_POST['sel_conta_id'];
		
		if($conta == 'todas'){
			$rsConta = mysql_query("SELECT * FROM tblfinanceiro_conta");
			while($rowConta = mysql_fetch_array($rsConta)){
				echo mysql_error();
				
				$data_caixa = $rowConta['fldData_Caixa'];
	
				$data_update = date('d/m/Y',strtotime( "+1 day", strtotime($data_caixa)));
				$data_update = format_date_in($data_update);
				
				mysql_query("UPDATE tblfinanceiro_conta SET fldData_Caixa = '$data_update' WHERE fldId = ".$rowConta['fldId']);
			}
		}else{
			$data_update = date('d/m/Y',strtotime( "+1 day", strtotime($data_caixa)));
			$data_update = format_date_in($data_update);
			
			mysql_query("UPDATE tblfinanceiro_conta SET fldData_Caixa = '$data_update' WHERE fldId = $conta");
		}
		header("Location: index.php?p=financeiro&fecha=ok");
	}
?>    
    <form id="frm-financeiro_conta"  class="table_form" action="#" method="post">
    	<ul>
    		<li style="width:350px">
            	<select class="config" id="sel_conta_id" name="sel_conta_id" >
<?					$rsConta = mysql_query("select * from tblfinanceiro_conta");
					while($rowConta= mysql_fetch_array($rsConta)){
?>						<option <?=($_SESSION['sel_conta_id'] == $rowConta['fldId']) ? 'selected="selected"' : '' ?> value="<?= $rowConta['fldId'] ?>"><?= $rowConta['fldNome'] ?></option>
<?					}
?>					<option <?=($_SESSION['sel_conta_id'] =='todas') ? 'selected="selected"' : '' ?> value="todas">Todas</option>
				</select>
			</li>
            <li style="width:400px">
            	<span <?=($data_caixa != $data_atual) ? print "class='alert'" : '' ?>><?=format_date_out($data_caixa)?></span>
            </li>
            <li>
            	<a class="edit modal btn_gray" title="Editar" href="financeiro_conta_fluxo_fecha_caixa" rel="250-150">FECHAR CAIXA</a>
            </li>
    	</ul>
	</form>

<?
	require("financeiro_conta_fluxo_filtro.php");
	
	//ações em grupo
	if(isset($_POST['hid_action'])){
		require("financeiro_conta_fluxo_action.php");
	}
	
	/*********************************************************/
	if(isset($_GET['mensagem']) && $_GET['mensagem'] == "ok"){
?>		<div class="alert">
			<p class="ok">Registro gravado com sucesso!<p>
        </div>
<?	}
	/*********************************************************/
	if(isset($_GET['mensagem']) && $_GET['mensagem'] == "transferencia"){
?>		<div class="alert">
			<p class="ok">Transfer&ecirc;cia realizada com sucesso!<p>
        </div>
<?	}
	/*********************************************************/
	if(isset($_GET['mensagem']) && $_GET['mensagem'] == "estorno"){
?>		<div class="alert">
			<p class="ok">Estorno do(s) registro(s) "<?=$_GET['filtro']?>" efetuado(s) para data atual.<p>
        </div>
<?	}
	/*********************************************************/

	$conta_filtro = ($_SESSION['sel_conta_id'] == 'todas') ? '' : "AND vwfinanceiro_conta_fluxo.fldConta_Id =".$_SESSION['sel_conta_id'];

	if(format_date_in($_SESSION['txt_conta_data1']) != "" || format_date_in($_SESSION['txt_conta_data2']) != ""){
			if($_SESSION['txt_conta_data1'] != "" && $_SESSION['txt_conta_data2'] != ""){
				$data = "vwfinanceiro_conta_fluxo.fldData between '".format_date_in($_SESSION['txt_conta_data1'])."' AND '".format_date_in($_SESSION['txt_conta_data2'])."'";
			}elseif($_SESSION['txt_conta_data1'] != "" && $_SESSION['txt_conta_data2'] == ""){
				$data = "vwfinanceiro_conta_fluxo.fldData >= '".format_date_in($_SESSION['txt_conta_data1'])."'";
			}elseif($_SESSION['txt_conta_data2'] != "" && $_SESSION['txt_conta_data1'] == ""){
				$data = "vwfinanceiro_conta_fluxo.fldData <= '".format_date_in($_SESSION['txt_conta_data2'])."'";
			}
	}else{
		$data = "vwfinanceiro_conta_fluxo.fldData between '".$data_caixa."' AND '".$data_caixa."'";
	}

	//ADICIONA O FILRO PARA VERIFICAR SE É A VISTA
	if($_SESSION['sel_movimento_fluxo'] == "pedidoAvista"){
		$sSQL = "SELECT vwfinanceiro_conta_fluxo.*, 
		vwfinanceiro_conta_fluxo.fldConta_Id as fldFluxo_ContaId,
		tblpagamento_tipo.fldSigla AS fldPagamento_Sigla,
		tblpagamento_tipo.fldTipo AS fldPagamento_Tipo
		FROM vwfinanceiro_conta_fluxo 
		INNER JOIN tblpagamento_tipo ON vwfinanceiro_conta_fluxo.fldPagamento_Tipo_Id = tblpagamento_tipo.fldId
		INNER JOIN 
		(SELECT tblpedido_parcela_baixa.fldId as fldBaixaId FROM tblpedido_parcela 
			INNER JOIN tblpedido ON tblpedido_parcela.fldPedido_Id = tblpedido.fldId
			INNER JOIN tblpedido_parcela_baixa	ON tblpedido_parcela_baixa.fldParcela_Id = tblpedido_parcela.fldId 
			WHERE tblpedido.fldCadastroData = tblpedido_parcela_baixa.fldDataCadastro
		) as tblbaixa
		ON vwfinanceiro_conta_fluxo.fldReferencia_Id = tblbaixa.fldBaixaId
		WHERE $data $conta_filtro $filtro";
	}
	else if($_SESSION['sel_movimento_fluxo'] == "pedidoAPrazo"){
		$sSQL = "SELECT vwfinanceiro_conta_fluxo.*, 
		vwfinanceiro_conta_fluxo.fldConta_Id as fldFluxo_ContaId,
		tblpagamento_tipo.fldSigla AS fldPagamento_Sigla,
		tblpagamento_tipo.fldTipo AS fldPagamento_Tipo
		FROM vwfinanceiro_conta_fluxo 
		INNER JOIN tblpagamento_tipo ON vwfinanceiro_conta_fluxo.fldPagamento_Tipo_Id = tblpagamento_tipo.fldId
		INNER JOIN 
		(SELECT tblpedido_parcela_baixa.fldId as fldBaixaId FROM tblpedido_parcela 
			INNER JOIN tblpedido ON tblpedido_parcela.fldPedido_Id = tblpedido.fldId
			INNER JOIN tblpedido_parcela_baixa	ON tblpedido_parcela_baixa.fldParcela_Id = tblpedido_parcela.fldId 
			WHERE tblpedido.fldCadastroData != tblpedido_parcela_baixa.fldDataCadastro
		) as tblbaixa
		ON vwfinanceiro_conta_fluxo.fldReferencia_Id = tblbaixa.fldBaixaId
		WHERE $data $conta_filtro $filtro";
	}else{
		$sSQL = "SELECT vwfinanceiro_conta_fluxo.*, vwfinanceiro_conta_fluxo.fldConta_Id as fldFluxo_ContaId,
		tblpagamento_tipo.fldSigla AS fldPagamento_Sigla, tblpagamento_tipo.fldTipo AS fldPagamento_Tipo
		FROM vwfinanceiro_conta_fluxo
		INNER JOIN tblpagamento_tipo ON vwfinanceiro_conta_fluxo.fldPagamento_Tipo_Id = tblpagamento_tipo.fldId
		WHERE $data $conta_filtro $filtro";
	}
	
	$sSQL = $sSQL." order by vwfinanceiro_conta_fluxo.fldDataInserida, vwfinanceiro_conta_fluxo.fldHora asc";
	
/* VIEW:
SELECT 
tblfinanceiro_conta_fluxo.*,

IF(
 (fldEntidade = ''),
 (CASE fldMovimento_Tipo  
  WHEN '1' THEN fldEntidade

  WHEN '2' THEN (SELECT fldNome FROM tblfinanceiro_conta WHERE fldId = IF(fldCredito > 0, fldConta_Origem, fldConta_Destino) LIMIT 1)  

  WHEN '3' THEN (SELECT tblcliente.fldNome FROM tblpedido_parcela_baixa 
				 LEFT JOIN tblpedido_parcela ON tblpedido_parcela_baixa.fldParcela_Id = tblpedido_parcela.fldId
				 LEFT JOIN tblpedido ON tblpedido_parcela.fldPedido_Id = tblpedido.fldId
				 LEFT JOIN tblcliente ON tblpedido.fldCliente_Id = tblcliente.fldId
				 WHERE tblpedido_parcela_baixa.fldId = fldReferencia_Id LIMIT 1)

  WHEN '4' THEN (SELECT tblfornecedor.fldNomeFantasia FROM tblcompra_parcela_baixa
				 LEFT JOIN tblcompra_parcela ON tblcompra_parcela_baixa.fldParcela_Id = tblcompra_parcela.fldId
				 LEFT JOIN tblcompra ON tblcompra_parcela.fldCompra_Id = tblcompra.fldId
				 LEFT JOIN tblfornecedor ON tblcompra.fldFornecedor_Id = tblfornecedor.fldId
				 WHERE tblcompra_parcela_baixa.fldId = fldReferencia_Id LIMIT 1)

  WHEN '6' THEN (SELECT tblfinanceiro_conta_pagar_programada.fldNome FROM tblfinanceiro_conta_pagar_programada_baixa
				 LEFT JOIN tblfinanceiro_conta_pagar_programada ON 
				 tblfinanceiro_conta_pagar_programada_baixa.fldContaProgramada_Id = tblfinanceiro_conta_pagar_programada.fldId
				 WHERE tblfinanceiro_conta_pagar_programada_baixa.fldId LIMIT 1)

  WHEN '7' THEN (SELECT tblfuncionario.fldNome FROM tblfuncionario_conta_fluxo
				 LEFT JOIN tblfuncionario ON tblfuncionario_conta_fluxo.fldFuncionario_Id = tblfuncionario.fldId
				 WHERE tblfuncionario_conta_fluxo.fldId = fldReferencia_Id LIMIT 1)

  WHEN '8' THEN (SELECT tblfuncionario.fldNome FROM tblfuncionario_conta_fluxo
				 LEFT JOIN tblfuncionario ON tblfuncionario_conta_fluxo.fldFuncionario_Id = tblfuncionario.fldId
				 WHERE tblfuncionario_conta_fluxo.fldId = fldReferencia_Id LIMIT 1)

  WHEN '9' THEN (SELECT tblfuncionario.fldNome FROM tblfuncionario_conta_fluxo
				 LEFT JOIN tblfuncionario ON tblfuncionario_conta_fluxo.fldFuncionario_Id = tblfuncionario.fldId
				 WHERE tblfuncionario_conta_fluxo.fldId = fldReferencia_Id LIMIT 1)
 END),
 (fldEntidade)
) as Entidade

FROM `tblfinanceiro_conta_fluxo`
*/

	//INNER JOIN tblfinanceiro_conta_fluxo_marcador ON tblfinanceiro_conta_fluxo.fldMarcador_Id = tblfinanceiro_conta_fluxo_marcador.fldId
	$rsFluxo = mysql_query($sSQL);
	echo mysql_error();

	$_SESSION['relatorio_caixa_sql'] = $sSQL;

	$conta_id 	  	= $_SESSION['sel_conta_id'];
	$filtro_conta 	= ($_SESSION['sel_conta_id'] == 'todas') ? '' : "AND fldConta_Id =".$_SESSION['sel_conta_id'];
	$data_anterior 	= format_date_in($_SESSION['txt_conta_data1']);

	$rowSaldoAnterior = mysql_fetch_array(mysql_query("SELECT sum(fldCredito - fldDebito) as fldSaldoAnterior from vwfinanceiro_conta_fluxo 
														WHERE fldData < '".$data_anterior."' ".$filtro_conta));

	$saldoAnterior = $rowSaldoAnterior['fldSaldoAnterior'];
	$_SESSION['saldo_anterior_valor'] = $saldoAnterior;

?>
    <div class="data_conta">
        <a class="anterior" href="index.php?p=financeiro&modo=conta_fluxo&ant"></a><span style="margin-right: 10px;">dia anterior</span>
        <span>pr&oacute;ximo dia</span><a class="proximo" href="index.php?p=financeiro&modo=conta_fluxo&prox"></a>
    </div>
    <div class="saldo" style="margin-left: 544px">
        <span style="width: 120px">Saldo anterior:</span>
        <span class="<?=fnc_status_saldo($saldoAnterior)?>"><?=format_number_out($saldoAnterior)?></span>
    </div>
        <form class="table_form" id="frm_financeiro_conta_fluxo" action="" method="post">
            <div id="table">
                <div id="table_cabecalho">
                    <ul class="table_cabecalho">
                        <li style="width:48px; text-align:center;">Id</li>
                        <li style="width:60px; text-align:center;">Data</li>
                        <li style="width:40px; text-align:center;">Hora</li>
                        <li style="width:170px;">Descri&ccedil;&atilde;o</li>
                        <li style="width:210px;">Entidade</li>
                        <li style="width:30px; text-align:center;">Tipo</li>
                        <li style="width:120px; text-align:center;">Conta</li>
                        <li style="width:60px; text-align:right;">Cr&eacute;dito</li>
                        <li style="width:60px; text-align:right;">D&eacute;bito</li>
                        <li style="width:60px; text-align:right;">Saldo</li>
						<li style="width:20px;"></li>
                        <li style="width:15px;"><input type="checkbox" name="chk_todos" id="chk_todos" /></li>
                    </ul>
                </div>
                <div id="table_container" style="height:400px">       
                    <table id="table_general" class="table_general" summary="Lista de fluxo de caixa">
                    	<tbody>
<?						
						$id_array 	= array();
						$n 			= 0;	
						$linha 		= "row";
						while($rowFluxo = mysql_fetch_array($rsFluxo)){
							echo mysql_error();	
						
							$rsConta 		= mysql_query("SELECT * FROM tblfinanceiro_conta WHERE fldId =".$rowFluxo['fldFluxo_ContaId']);
							$rowConta 		= mysql_fetch_array($rsConta);
							$id_array[$n]	= $rowFluxo["fldId"];
							$n += 1;
							
							/*********************************************************************************/
							//DEFINIR DESCRICAO
							//tipo de movimentação (por id)
							//1 = Movimento caixa
							//2 = Transferência de conta
							//3 = Recebimento de parcela
							//4 = Pagamento de parcela
							//5 = Estorno
							//6 = Pagamento conta programada
							$descricao = $rowFluxo['fldDescricao'];
							if($rowFluxo['fldMovimento_Tipo'] != 1 && $rowFluxo['fldMovimento_Tipo'] != 5){
								$rowTipo 	= mysql_fetch_array(mysql_query("SELECT fldTipo FROM tblfinanceiro_conta_fluxo_tipo WHERE fldId = ".$rowFluxo['fldMovimento_Tipo']));
								$descricao 	= $rowTipo['fldTipo'];
							}
							/*********************************************************************************/
							//DEFINIR ENTIDADE
							$Entidade = $rowFluxo['Entidade'];
							switch($rowFluxo['fldMovimento_Tipo']){
								case '3': //recebimento de parcela
									if($rowFluxo['fldPagamento_Tipo_Id'] == 2){
										$descricao .= " ( C. Cr&eacute;dito ".format_date_out($rowClienteParcela['fldVencimento']).")";
									}elseif($rowFluxo['fldPagamento_Tipo_Id'] == 4){
										$descricao .= " ( Cheque ".format_date_out($rowClienteParcela['fldVencimento']).")";
									}
								break;
								case '4': // pagamento de parcela
									if($rowFluxo['fldPagamento_Tipo_Id'] == 2){
										$descricao .= " ( C. Cr&eacute;dito ".format_date_out($rowClienteParcela['fldVencimento']).")";
									}elseif($rowFluxo['fldPagamento_Tipo_Id'] == 4){
										$descricao .= " ( Cheque ".format_date_out($rowClienteParcela['fldVencimento']).")";
									}
								break;
							}
							echo mysql_error();
							$tipoFluxo = $rowFluxo['fldMovimento_Tipo'];
							
?>							<tr class="<?= $linha; ?>">
<?								$valor_fluxo = $rowFluxo['fldCredito'] - $rowFluxo['fldDebito'];
                                $saldo 		+= $rowFluxo['fldCredito'] - $rowFluxo['fldDebito'];
                                $saldo_final = $saldoAnterior + $saldo;
								
?>                       		<td class="cod"	style="width:50px; text-align:center;"><?=str_pad($rowFluxo['fldId'], 4, "0", STR_PAD_LEFT)?></td>
                           		<!-- LUCAS 20121023 -->
                                <td style="width:60px; text-align:center; <? if(($rowFluxo['fldData'] != $rowFluxo['fldDataInserida']) && ($rowFluxo['fldDataInserida'] != '0000-00-00')) { echo 'color:#F60;'; } ?>" title="<? if(($rowFluxo['fldData'] != $rowFluxo['fldDataInserida']) && ($rowFluxo['fldDataInserida'] != '0000-00-00')) { echo 'Registro inserido em: '.format_date_out($rowFluxo['fldDataInserida']); } ?>"><?=format_date_out($rowFluxo['fldData'])?></td>
                                <!-- LUCAS 20121023 -->
                                <td style="width:40px; text-align:center;"><?=format_time_short($rowFluxo['fldHora'])?></td>
                                <td style="width:170px;" class="<?=fnc_status_saldo($valor_fluxo)?>"><?=$descricao;?></td>
                                <td style="width:210px;"><?=$Entidade;?></td>
                                <td class="tipo" style="width:30px; text-align:center;" title="<?=$rowFluxo['fldPagamento_Tipo'];?>"><?=$rowFluxo['fldPagamento_Sigla']?></td>
                                <td style="width:120px; text-align:center;"><?=substr($rowConta['fldNome'],0,18)?></td>
                                <td class="credito" style="width:60px; text-align:right;"><?=format_number_out($rowFluxo['fldCredito'])?></td>
                                <td class="debito" style="width:60px; text-align:right;"><?=format_number_out($rowFluxo['fldDebito'])?></td>
                                <td style="width:60px; text-align:right;" class="<?=fnc_status_saldo($saldo_final)?>"><?=format_number_out($saldo_final)?></td> 
                                <td style="width:20px;"><a class='modal edit' href='financeiro_conta_fluxo_detalhe,<?=$rowFluxo['fldId']?>' rel="880-190" title='editar'></a></td>
                                <td style="width:15px;">
<?								if($tipoFluxo == 1 || $tipoFluxo == 3 || $tipoFluxo ==  4 || $tipoFluxo == 6){
?>									<input type="checkbox" name="chk_fluxo_<?=$rowFluxo['fldId']?>" id="chk_fluxo_<?=$rowFluxo['fldId']?>" title="selecionar o registro posicionado" />
<?								}
?>                          	</td>
							</tr>
<?                     		$linha = ($linha == "row" ? "dif-row" : "row");

							$credito	+= $rowFluxo['fldCredito'];
							$debito 	+= $rowFluxo['fldDebito'];
						}
						$saldo_total = $saldoAnterior + $credito - $debito;
							
?>					</tbody>
                </table>
            </div>
           
            <input type="hidden" name="hid_array" 	id="hid_array" 	value="<?=urlencode(serialize($id_array))?>" />
            <input type="hidden" name="hid_action" 	id="hid_action" value="true" />
        
            <div id="table_action">
                <ul id="action_button">
                    <li><a class="btn_novo modal" 			href="financeiro_conta_fluxo_novo" 			rel="880-150">novo</a></li>
                    <li><a class="btn_transferencia modal"	href="financeiro_conta_fluxo_transferencia" rel="680-110">transfer&ecirc;ncia</a></li>
                    <li><input type="submit" 	name="btn_action" id="btn_estorno" 	value="estorno" 	title="Estornar registro(s) selecionado(s)"  onclick="return confirm('Deseja estornar os registros selecionados?')" /></li>
                    <!--<li><button name="btn_action" id="btn_print" value="imprimir" title="Imprimir relat&oacute;rio"></button></li>-->
                    <li><a id="btn_print" class="modal" href="financeiro_conta_fluxo_relatorio,<?=format_date_in($_SESSION['txt_conta_data1'])?>,<?=format_date_in($_SESSION['txt_conta_data2'])?>" rel="530-135"></a></li>
                </ul>
            </div>
            <div class="saldo" style=" width: 415px;margin-left:83px">
                <span style="margin-left: 125px" class="credito"><?=format_number_out($credito)?></span>
                <span class="debito"><?=format_number_out($debito)?></span>
                <span style="margin-left: 10px" class="<?= fnc_status_saldo($saldo_total)?>"><?= format_number_out($saldo_total)?></span>
            </div>
        </div>
    </form>
  
	<script type="text/javascript">
		$('.btn_novo, .btn_transferencia').click(function(event){
			var sessao = '<?= $_SESSION['sel_conta_id'] ?>';
			
			if(sessao == 'todas'){
				alert('Selecione uma CONTA antes!');
				return false;
			}else{
				return true;
			}
		});
	</script>