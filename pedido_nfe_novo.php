<div id="voltar"><p><a href="index.php?p=pedido">n&iacute;vel acima</a></p></div>

<h2>Nota Fiscal Eletr&ocirc;nica</h2>
<div id="principal">
<?	$usuario_id 				= $_SESSION['usuario_id'];
	$verificar_limite_credito 	= $_SESSION['sistema_venda_alerta_limite_credito'];
	$vendaDecimal 				= fnc_sistema('venda_casas_decimais');
	$qtdeDecimal 				= fnc_sistema('quantidade_casas_decimais');
	$servico_valor 				= fnc_sistema('venda_servico_valor');
	$tipo_ambiente				= fnc_sistema('nfe_ambiente_tipo');

	//PEGO ALGUMAS INFORMACOES DE PERFIL DA NOTA, SE HOUVER
	$rsPerfil  = mysql_query('SELECT * FROM tblnfe_perfil');
	$rowPerfil = mysql_fetch_array($rsPerfil);

	#EFINO NATUREZA OP SE VEIO PELO MODAL OU NOVA NFe COMUM
	$nfe_natureza_operacao_id 	= (isset($_GET['operacao'])) ? $_GET['operacao'] : $rowPerfil['fldNatureza_Operacao_Id'];
	$rsNatureza_Operacao 		= mysql_query("SELECT * FROM tblnfe_natureza_operacao WHERE fldId = $nfe_natureza_operacao_id");
	$rowNatureza_Operacao 		= mysql_fetch_assoc($rsNatureza_Operacao);
	$nfe_parcelamento_exibir 	= $rowNatureza_Operacao['fldParcelas'];
	$nfe_estoque_controlar	 	= $rowNatureza_Operacao['fldEstoque'];
	$nfe_natureza_operacao	 	= $rowNatureza_Operacao['fldNatureza_Operacao'];
	
	#DEFININDO TIPO DE DOCUMENTO (ENTRADA-SAIDA)		
	if($_GET['documento'] == 'compra'){
		switch($nfe_natureza_operacao_id ){
			case 5: 
				$nfe_operacao_tipo = 0; #entrada
			break;
			case 2:
			case 6:
				$nfe_operacao_tipo = 1; #saida
			break;
		}
	}else{
		switch($nfe_natureza_operacao_id ){
			case 1: 
			case 3: 
			case 4: 
			case 5: 
				$nfe_operacao_tipo = 1; #saida
			break;
			case 2:
			case 6:
			case 7:
				$nfe_operacao_tipo = 0; #entrada
			break;
		}
	}#end if compra ou venda

		
	#ACOES AO GRAVAR NFe ########################################################################################################################################################################
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		
		//antes de seguir em frente com o processamento das vendas que ser�o convertidas em NFe, � feito o tratamento de n�meros de NFe ausentes, caso exista!
		if(isset($_POST['hid_controle_nfe_numero_perdido_enviar'])) {
			
			if(!is_null($_POST['rad_nnfe_n_perdido'])) { $numeroAusenteNFe = $_POST['rad_nnfe_n_perdido']; }
			
			$sql  = "INSERT INTO tblpedido_nfe_numero_inutilizado(fldNFe_Numero, fldMotivo, fldObservacao, fldData, fldHora, fldUsuario_Id) VALUES";
			$data = date('Y-m-d');
			$hora = date('H:i:s');
			$user = $_SESSION['usuario_id'];
			
			$exec_sql  = false;
			$limite    = $_POST['hid_controle_qtd_numeros'];
			for($x=1;$x<=$limite;$x++) {
				
				if(isset($_POST['sel_nnfe_nao_utilizar_'.$x]) && $_POST['sel_nnfe_nao_utilizar_'.$x] > 0) {
					
					$numero = $_POST['hid_nfe_numero_'.$x];
					$motivo = $_POST['sel_nnfe_nao_utilizar_'.$x];
					$obs 	= $_POST['txt_nnfe_observacao_'.$x];
					$sql 	.= "('{$numero}', '{$motivo}', '{$obs}', '{$data}', '{$hora}', '{$user}'),";
					$exec_sql = true;
				}
			}
			if($exec_sql == true) {
				//retirar a �ltima v�rgula
				$sql = substr($sql, 0, -1);
				if(!mysql_query($sql)) { echo mysql_error(); exit(); }
			}
			unset($sql, $data, $hora, $user, $exec_sql, $limite, $x, $numero, $motivo, $obs);
		}
		########################################################################################################################################################
		$referencia			= $_POST['txt_referencia'];
		$cliente_id 		= $_POST['hid_cliente_id'];
		$nfe_estoque_controle	= $_POST['hid_estoque_controlar'];
		$dependente_id 		= ($_POST['hid_dependente_id'] > 0 ) ? $_POST['hid_dependente_id'] : "NULL" ;
		$veiculo_id 		= $_POST['sel_veiculo']; //automotivo
		$veiculo_km			= $_POST['txt_veiculo_km']; //automotivo
		$conta_bancaria_id	= $_POST['sel_conta_bancaria']; //financeira
		$desconto 			= format_number_in($_POST['txt_pedido_desconto']);
		$desconto_reais 	= format_number_in($_POST['txt_pedido_desconto_reais']);
		$obs 				= mysql_escape_string($_POST['txt_pedido_obs']);
		$data				= date("Y-m-d");
		$hora				= $_POST['txt_pedido_hora'];
		$pedido_data 		= format_date_in($_POST['txt_pedido_data']);
		$entrega_data 		= format_date_in($_POST['txt_pedido_entrega']);
		$status_venda		= 4; #entregue;
		$marcador 			= $_POST['sel_marcador'];
		$cliente_pedido		= $_POST['txt_cliente_pedido'];
		$tipo_pedido		= 3; //NFe
		
		$outros_servicos	= mysql_escape_string($_POST['txa_outros_servicos']);
		$servicos_terceiros	= $_POST['txt_pedido_terceiros'];
		$faturado			= ($_POST['chk_faturado'] == 'faturado') ? 1 : 0;
	
		$sqlInsert_pedido = "INSERT INTO tblpedido
		(fldUsuario_Id, fldCliente_Id, fldReferencia, fldDependente_Id, fldVeiculo_Id, fldVeiculo_Km, fldContaBancaria_Id, fldDesconto, 
		fldDescontoReais, fldObservacao, fldCadastroData, fldCadastroHora, fldPedidoData, fldEntregaData, 
		fldStatus, fldMarcador_Id, fldCliente_Pedido, fldTipo_Id, fldServico, fldValor_Terceiros, fldFaturado)
		VALUES(
			'$usuario_id', 	'$cliente_id', 		'$referencia',			'$dependente_id',
			'$veiculo_id',	'$veiculo_km', 		'$conta_bancaria_id',	'$desconto',		'$desconto_reais ',	
			'$obs',			'$data',			'$hora',			 	'$pedido_data',		
			'$entrega_data','$status_venda',	'$marcador',			'$cliente_pedido',	
			'$tipo_pedido',	'$outros_servicos',	'$servicos_terceiros', 	'$faturado'
		)";
		
		if(mysql_query($sqlInsert_pedido)){
			echo mysql_error();
			#BUSCA ID DE VENDA QUE ACABOU DE CADASTRAR na tblpedido
			$rsPedido 	= mysql_query('SELECT last_insert_id() as lastID');
			$LastId 	= mysql_fetch_array($rsPedido);
			$pedido_id 	= $LastId['lastID'];
			echo mysql_error();
			
			for($x=1; $x <= 3; $x++){
				#INSERE OS SERVICOS NA TBL DO FUNCIONARIO
				$Funcionario_Id			= $_POST['sel_funcionario'.$x];
				if($Funcionario_Id > 0){
					$Funcionario_Servico	= mysql_escape_string($_POST['txa_funcionario'.$x.'_servico']);
					$Funcionario_Tempo		= format_number_in($_POST['txt_funcionario'.$x.'_tempo']);
					$Funcionario_Valor		= format_number_in($_POST['txt_funcionario'.$x.'_valor']);
					
					$rsComissao  			= mysql_query("SELECT fldFuncao2_Comissao FROM tblfuncionario WHERE fldId = $Funcionario_Id");
					$rowComissao 			= mysql_fetch_array($rsComissao);				
					$Comissao				= $rowComissao['fldFuncao2_Comissao'];
					
					$insertServico			= "INSERT INTO tblpedido_funcionario_servico (fldPedido_Id, fldFuncionario_Id, fldServico, fldTempo, fldValor, fldComissao, fldFuncao_Tipo, fldOrdem)
												VALUES ($pedido_id, $Funcionario_Id, '$Funcionario_Servico', '$Funcionario_Tempo', '$Funcionario_Valor', '$Comissao', '2', $x)";
					
					mysql_query($insertServico) or die(mysql_error());
				}
			}
			// DADOS NFE *************************************************************************************************************************************************************************************************************************************************************************************************
				#POR ENQUANTO O NOVO ESTA APENAS PARA VENDA, POR ISSO DEIXEI FIXO
				$documento_tipo						= '1';
				//nfe
				$regime								= fnc_nfe_regime();
				$nfe_serie							= fnc_sistema('nfe_serie');
				$nfe_numero							= (isset($numeroAusenteNFe)) ? $numeroAusenteNFe : fnc_sistema('nfe_numero_nota');
				$nfe_operacao_tipo					= $_POST['sel_pedido_operacao_tipo'];
				$nfe_saida_data						= format_date_in($_POST['txt_pedido_saida_data']);
				$nfe_saida_hora						= $_POST['txt_pedido_saida_hora'];
				$nfe_operacao_natureza_id			= $_POST['hid_pedido_natureza_operacao_id'];				
				$nfe_pagamento_forma_codigo			= $_POST['sel_pedido_nfe_pagamento_forma'];
					
				$nfe_total_produtos					= format_number_in($_POST['txt_pedido_nfe_total_produtos']);
				$nfe_total_bc_icms					= format_number_in($_POST['txt_pedido_nfe_total_bc_icms']);
				$nfe_total_icms						= format_number_in($_POST['txt_pedido_nfe_total_icms']);
				$nfe_total_bc_icms_substituicao		= format_number_in($_POST['txt_pedido_nfe_total_bc_icms_substituicao']);
				$nfe_total_icms_substituicao		= format_number_in($_POST['txt_pedido_nfe_total_icms_substituicao']);
				$nfe_total_frete					= format_number_in($_POST['txt_pedido_nfe_total_frete']);
				$nfe_total_seguro					= format_number_in($_POST['txt_pedido_nfe_total_seguro']);
				$nfe_total_desconto					= format_number_in($_POST['txt_pedido_nfe_total_desconto']);
				$nfe_total_valor_ipi				= format_number_in($_POST['txt_pedido_nfe_total_ipi']);
					
				$nfe_total_valor_ii					= format_number_in($_POST['txt_pedido_nfe_total_ii']);
				$nfe_total_valor_pis				= format_number_in($_POST['txt_pedido_nfe_total_pis']);
				$nfe_total_valor_cofins				= format_number_in($_POST['txt_pedido_nfe_total_cofins']);
				$nfe_out_despesas_acessorias		= format_number_in($_POST['txt_pedido_nfe_outras_despesas_acessorias']);
				$nfe_total_valor_nota				= format_number_in($_POST['txt_pedido_nfe_total_nfe']);
				$nfe_total_tributo					= format_number_in($_POST['txt_pedido_nfe_total_tributos']);
				
				$nfe_total_bc_iss					= format_number_in($_POST['txt_pedido_nfe_total_bc_iss']);
				$nfe_total_iss						= format_number_in($_POST['txt_pedido_nfe_total_iss']);
				$nfe_total_pis_servicos				= format_number_in($_POST['txt_pedido_nfe_total_pis_servicos']);
				$nfe_total_cofins_servicos			= format_number_in($_POST['txt_pedido_nfe_total_cofins_servicos']);
				$nfe_total_icms_nao_incidencia		= format_number_in($_POST['txt_pedido_nfe_total_servicos_nao_incidencia_icms']);
				
				$nfe_total_retido_pis				= format_number_in($_POST['txt_pedido_nfe_total_retido_pis']);
				$nfe_total_retido_cofins			= format_number_in($_POST['txt_pedido_nfe_total_retido_cofins']);
				$nfe_total_retido_csll				= format_number_in($_POST['txt_pedido_nfe_total_retido_csll']);
				$nfe_total_bc_irrf					= format_number_in($_POST['txt_pedido_nfe_total_bc_irrf']);
				$nfe_total_retido_irrf				= format_number_in($_POST['txt_pedido_nfe_total_retido_irrf']);
				$nfe_total_bc_retencao_prev_social	= format_number_in($_POST['txt_pedido_nfe_bc_retencao_prev_social']);
				$nfe_total_retencao_prev_social		= format_number_in($_POST['txt_pedido_nfe_total_retencao_prev_social']);
				
				$nfe_entrega_endereco				= $_POST['txt_pedido_nfe_entrega_endereco'];
				$nfe_entrega_numero					= $_POST['txt_pedido_nfe_entrega_numero'];
				$nfe_entrega_complemento			= $_POST['txt_pedido_nfe_entrega_complemento'];
				$nfe_entrega_bairro					= $_POST['txt_pedido_nfe_entrega_bairro'];
				$nfe_entrega_cep					= $_POST['txt_pedido_nfe_entrega_cep'];
				$nfe_entrega_municipio_codigo		= $_POST['txt_municipio_codigo_pedido_nfe'];
				$nfe_entrega_cpf_cnpj 				= $_POST['txt_pedido_nfe_entrega_cpf_cnpj'];
				$nfe_entrega_diferente_destinatario = $_POST['chk_local_entrega_diferente'];

				$nfe_transporte_modalidade			= $_POST['sel_pedido_nfe_transporte_modalidade'];
				$nfe_transportador					= $_POST['sel_pedido_nfe_transportador'];
				$nfe_transporte_icms_isento			= $_POST['chk_nfe_transporte_icms_isento'];
				
				$nfe_transporte_veiculo_placa		= $_POST['txt_pedido_nfe_transporte_veiculo_placa'];
				$nfe_transporte_veiculo_uf			= $_POST['sel_pedido_nfe_transporte_veiculo_uf'];
				$nfe_transporte_veiculo_rntc		= $_POST['txt_pedido_nfe_transporte_veiculo_rntc'];
				$nfe_transporte_balsa				= $_POST['txt_pedido_nfe_transporte_balsa_identificacao'];
				$nfe_transporte_vagao				= $_POST['txt_pedido_nfe_transporte_vagao_identificacao'];
				
				$nfe_retencao_bc					= format_number_in($_POST['txt_pedido_nfe_transporte_retencao_bc']);
				$nfe_retencao_aliquota				= format_number_in($_POST['txt_pedido_nfe_transporte_retencao_aliquota']);
				$nfe_retencao_valor_servico			= format_number_in($_POST['txt_pedido_nfe_transporte_retencao_valor_servico']);
				$nfe_retencao_municipio_codigo		= $_POST['sel_pedido_nfe_transporte_retencao_uf'].$_POST['sel_pedido_nfe_transporte_retencao_municipio'];
				$nfe_retencao_cfop					= $_POST['sel_pedido_nfe_transporte_retencao_cfop'];
				$nfe_retencao_icms_retido			= format_number_in($_POST['txt_pedido_nfe_transporte_retencao_icms_retido']);
				$nfe_info_fisco						= $_POST['txt_pedido_nfe_fisco'];
				$nfe_info_contribuinte				= $_POST['txt_pedido_nfe_contribuite'];
			
				$sqlInsert_pedido_fiscal = "INSERT INTO tblpedido_fiscal (
					fldpedido_id,
					fldOperacao_Tipo,
					fldregime_tributario_codigo,
					fldserie,
					fldambiente,
					fldnumero,
					fldDocumento_Tipo,
					fldsaida_data,
					fldsaida_hora,
					fldnatureza_operacao_id,
					fldpagamento_forma_codigo,
					fldprodutos_servicos_total,
					fldicms_base_calculo,
					fldicms_total,
					fldicmsst_base_calculo,
					fldicmsst_total,
					fldfrete_total,
					fldseguro_total,
					
					fldipi_total,
					fldii_total,
					fldpis_total,
					fldcofins_total,
					flddespesas_acessorias_total,
					flddesconto_total,
					fldnfe_total,
					fldnfe_total_tributos,
					
					fldiss_base_calculo,
					fldiss_total,
					fldpis_servicos_total,
					fldcofins_servicos_total,
					fldicms_nao_incidencia_total,	
					
					fldpis_retido_total,
					fldcofins_retido_total,
					fldcsll_retido_total,
					fldirrf_base_calculo,
					fldirrf_retido_total,
					fldretencao_prev_social_base_calculo,
					fldretencao_prev_social_total,
					
					fldentrega_endereco,
					fldentrega_numero,
					fldentrega_complemento,
					fldentrega_bairro,
					fldentrega_cep,
					fldentrega_municipio_codigo,
					fldentrega_cpf_cnpj,
					fldentrega_diferente_destinatario,
					
					fldtransporte_modalidade_codigo,
					fldtransporte_transportador_id,
					fldtransporte_icms_isento,
					
					fldtransporte_retencao_icms_base_calculo,
					fldtransporte_retencao_icms_aliquota,
					fldtransporte_retencao_icms_servico_valor,
					fldtransporte_retencao_icms_municipio_codigo,
					fldtransporte_retencao_icms_cfop,
					fldtransporte_retencao_icms_retencao_valor,
					
					fldtransporte_veiculo_placa,
					fldtransporte_veiculo_uf,
					fldtransporte_veiculo_rntc,
					fldtransporte_balsa_identificacao,
					fldtransporte_vagao_identificacao,
					
					fldinfo_adicionais_fisco,
					fldinfo_adicionais_contribuinte
				)
				VALUES(
					'$pedido_id',
					'$nfe_operacao_tipo',
					'$regime',
					'$nfe_serie',
					'$tipo_ambiente',
					'$nfe_numero',
					'$documento_tipo',
					'$nfe_saida_data',
					'$nfe_saida_hora',
					'$nfe_operacao_natureza_id',
				
					'$nfe_pagamento_forma_codigo',
					
					'$nfe_total_produtos',
					'$nfe_total_bc_icms',
					'$nfe_total_icms',
					'$nfe_total_bc_icms_substituicao',
					'$nfe_total_icms_substituicao',
					'$nfe_total_frete',
					'$nfe_total_seguro',
					
					'$nfe_total_valor_ipi',
					'$nfe_total_valor_ii',
					'$nfe_total_valor_pis',
					'$nfe_total_valor_cofins',
					'$nfe_out_despesas_acessorias',
					'$nfe_total_desconto',
					'$nfe_total_valor_nota',
					'$nfe_total_tributo',
					
					'$nfe_total_bc_iss',
					'$nfe_total_iss',
					'$nfe_total_pis_servicos',
					'$nfe_total_cofins_servicos',
					'$nfe_total_icms_nao_incidencia',
					
					'$nfe_total_retido_pis',
					'$nfe_total_retido_cofins',
					'$nfe_total_retido_csll',
					'$nfe_total_bc_irrf',
					'$nfe_total_retido_irrf',
					'$nfe_total_bc_retencao_prev_social',
					'$nfe_total_retencao_prev_social',	
					
					'$nfe_entrega_endereco',
					'$nfe_entrega_numero',
					'$nfe_entrega_complemento',
					'$nfe_entrega_bairro',
					'$nfe_entrega_cep',
					'$nfe_entrega_municipio_codigo',
					'$nfe_entrega_cpf_cnpj',
					'$nfe_entrega_diferente_destinatario',
					
					'$nfe_transporte_modalidade',
					'$nfe_transportador',
					'$nfe_transporte_icms_isento',
					
					'$nfe_retencao_bc',
					'$nfe_retencao_aliquota',
					'$nfe_retencao_valor_servico',
					'$nfe_retencao_municipio_codigo',
					'$nfe_retencao_cfop',
					'$nfe_retencao_icms_retido',
					
					'$nfe_transporte_veiculo_placa',
					'$nfe_transporte_veiculo_uf',
					'$nfe_transporte_veiculo_rntc',
					'$nfe_transporte_balsa',
					'$nfe_transporte_vagao',
					
					'$nfe_info_fisco',
					'$nfe_info_contribuinte'
				)";
				
				if(mysql_query($sqlInsert_pedido_fiscal)){
					if(!isset($numeroAusenteNFe)){
						$nfe_numero += 1;
						fnc_sistema_update('nfe_numero_nota', $nfe_numero);
					}
				}
				else{
					die(mysql_error());
				}
				
				//INSERINDO REBOQUES ***********************************************************************************************************************************************************************************************************************************************************************************
				
					$n= 1;
					$limite 	= $_POST["hid_reboque_controle"];
					while($n 	<= $limite){
						if($_POST['hid_reboque_controle'] > 0){
							
							$reboquePlaca 	= $_POST['txt_pedido_nfe_transporte_reboque_placa_'.$n];
							$reboqueUF	 	= $_POST['hid_pedido_nfe_transporte_reboque_uf_'.$n];
							$reboqueRNTC 	= $_POST['txt_pedido_nfe_transporte_reboque_rntc_'.$n];
							
							mysql_query("INSERT INTO tblpedido_fiscal_transporte_reboque
							(fldPedido_Id, fldPlaca, fldUF, fldRNTC)
							VALUES(
							'$pedido_id',
							'$reboquePlaca',
							'$reboqueUF',
							'$reboqueRNTC'
							)");
						}
						$n++;
					}
					
				//INSERINDO VOLUMES *************************************************************************************************************************************************************************************************************************************************************************************************
					$n= 1;
					$limite = $_POST["hid_volume_controle"];
					while($n <= $limite){
						if($_POST['hid_volume_controle'] > 0){
							
							$volumeQtd 			= format_number_in($_POST['txt_pedido_nfe_transporte_volume_qtd_'.$n]);
							$volumeEspecie 		= $_POST['txt_pedido_nfe_transporte_volume_especie_'.$n];
							$volumeMarca 		= $_POST['txt_pedido_nfe_transporte_volume_marca_'.$n];
							$volumeNummero 		= $_POST['txt_pedido_nfe_transporte_volume_numero_'.$n];
							$volumePesoLiq 		= format_number_in($_POST['txt_pedido_nfe_transporte_volume_peso_liquido_'.$n]);
							$volumePesoBruto 	= format_number_in($_POST['txt_pedido_nfe_transporte_volume_peso_bruto_'.$n]);
							$volumeLacres 		= $_POST['hid_pedido_nfe_transporte_volume_lacre_'.$n];
							
							mysql_query("INSERT INTO tblpedido_fiscal_transporte_volume
							(fldPedido_Id, fldQuantidade, fldEspecie, fldMarca, fldNumero, fldPeso_Liquido, fldPeso_Bruto)
							VALUES(
							'$pedido_id',
							'$volumeQtd',
							'$volumeEspecie',
							'$volumeMarca',
							'$volumeNummero',
							'$volumePesoLiq',
							'$volumePesoBruto'
							)") or die (mysql_error());
							
							$rsLastId 	= mysql_query("SELECT last_insert_id() as lastID");
							$LastId 	= mysql_fetch_array($rsLastId);
							$volumeId 	= $LastId['lastID'];
							
							$volumeLacres = substr($volumeLacres, 0 ,strlen($volumeLacres) - 1);
							$volumeLacres = explode(',',$volumeLacres);
							foreach($volumeLacres as $lacre){
								
								mysql_query("INSERT INTO tblpedido_fiscal_transporte_volume_lacre
								(fldVolume_Id, fldLacre)
								VALUES(
								'$volumeId',
								'$lacre'
								)") or mysql_error(die);
							}
						}
						$n++;
					}
			// *OTICA  *********************************************************************************************************************************************************************************************************************************************************************************
				$otica_valores = array(
					'perto' => array(
						'armacao' 			=>	$_POST['hid_pedido_otica_perto_armacao'],
						'material'			=>	$_POST['hid_pedido_otica_perto_material'],	
						'OD' => array(
							'esferico' 		 => $_POST['txt_pedido_otica_perto_od_esferico'],
							'cilindrico'	 => $_POST['txt_pedido_otica_perto_od_cilindrico'],
							'eixo' 			 => $_POST['txt_pedido_otica_perto_od_eixo'],
							'DP'			 => $_POST['txt_pedido_otica_perto_od_dp']
						),
						'OE' => array(
							'esferico' 		 => $_POST['txt_pedido_otica_perto_oe_esferico'],
							'cilindrico'	 => $_POST['txt_pedido_otica_perto_oe_cilindrico'],
							'eixo' 			 => $_POST['txt_pedido_otica_perto_oe_eixo'],
							'DP'			 => $_POST['txt_pedido_otica_perto_oe_dp']
						)
					),
					'longe' => array(
						'armacao' 			=>	$_POST['hid_pedido_otica_longe_armacao'],
						'material'			=>	$_POST['hid_pedido_otica_longe_material'],	
						'OD' => array(
							'esferico' 		 => $_POST['txt_pedido_otica_longe_od_esferico'],
							'cilindrico'	 => $_POST['txt_pedido_otica_longe_od_cilindrico'],
							'eixo' 			 => $_POST['txt_pedido_otica_longe_od_eixo'],
							'DP'			 => $_POST['txt_pedido_otica_longe_od_dp']
						),
						'OE' => array(
							'esferico' 		 => $_POST['txt_pedido_otica_longe_oe_esferico'],
							'cilindrico'	 => $_POST['txt_pedido_otica_longe_oe_cilindrico'],
							'eixo' 			 => $_POST['txt_pedido_otica_longe_oe_eixo'],
							'DP'			 => $_POST['txt_pedido_otica_longe_oe_dp']
						)
					),
					'bifocal' => array(
						'marca' 			=>	$_POST['sel_otica_marca'],
						'lente'				=>	$_POST['sel_otica_lente'],	
						'OD' => array(
							'altura'		=>	$_POST['txt_pedido_otica_bifocal_od_altura'],
							'DNP'			=>	$_POST['txt_pedido_otica_bifocal_od_dnp'],
							'adicao'		=>	$_POST['txt_pedido_otica_bifocal_od_adicao']
						),
						'OE' => array(
							'altura'		=>	$_POST['txt_pedido_otica_bifocal_oe_altura'],
							'DNP'			=>	$_POST['txt_pedido_otica_bifocal_oe_dnp'],
							'adicao'		=>	$_POST['txt_pedido_otica_bifocal_oe_adicao']
						)
					)
				);
			###########################################################################################################################################################################################################################################################################################################################################################################	
			$n= 1;
			$limite = $_POST["hid_controle"];
			while($n <= $limite){
				if(isset($_POST['hid_item_produto_id_'.$n])){
					
					$produto_id 		= $_POST['hid_item_produto_id_'.$n];
					$item_quantidade 	= format_number_in($_POST['txt_item_quantidade_'.$n]);
					$item_valor 		= format_number_in($_POST['txt_item_valor_'.$n]);
					$item_desconto 		= format_number_in($_POST['txt_item_desconto_'.$n]);
					$item_nome 			= mysql_escape_string($_POST['txt_item_nome_'.$n]);
					$item_fardo		 	= $_POST['hid_item_fardo_'.$n];
					$item_tabela_preco 	= $_POST['hid_item_tabela_preco_'.$n];
					$item_estoque_id	= $_POST['hid_item_estoque_id_'.$n];
					$item_referencia	= $_POST['txt_item_referencia_'.$n];
					$item_entregue		= ($_POST['chk_entregue_'.$n] == true)? '1' : '0';
					$item_entregue_data = "'" . date('Y-m-d') . "'";
					
					$rsDadosProduto		= mysql_query("SELECT fldEstoque_Controle, fldValorCompra FROM tblproduto WHERE fldId = $produto_id ");
					$rowDadosProduto 	= mysql_fetch_array($rsDadosProduto);
					$valor_compra 		= $rowDadosProduto['fldValorCompra'];
					$estoque_controle	= $rowDadosProduto['fldEstoque_Controle'];
					$rowTipo 			= mysql_fetch_array(mysql_query("SELECT fldTipo_Id FROM tblproduto WHERE fldId = $produto_id"));
					$tipo_id 			= $rowTipo['fldTipo_Id'];
					
					if(fnc_sistema('pedido_comissao') == '2'){
						$func_id				= $_POST['txt_funcionario_codigo'];
						$rsDadosComissao 		= mysql_query("SELECT * FROM tblproduto_comissao WHERE fldProduto_Id = $produto_id AND fldFuncionario_Id = $func_id");
						$count_comissao			= mysql_num_rows($rsDadosComissao);

						if($count_comissao > 0){
							$item_valor_comissao	= $item_valor * $item_quantidade;
							$rowComissao_Produto 	= mysql_fetch_assoc($rsDadosComissao);
							$comissao_porc 			= ($rowComissao_Produto['fldComissao_Tipo'] == '2') ? $rowComissao_Produto['fldComissao_Valor'] : '';
							$comissao_reais			= ($rowComissao_Produto['fldComissao_Tipo'] == '1') ? $rowComissao_Produto['fldComissao_Valor'] : '';

							mysql_query("INSERT INTO tblpedido_funcionario_servico 
													  (fldPedido_Id, fldFuncionario_Id, fldServico, fldTempo, fldValor, fldComissao, fldComissao_Reais, fldFuncao_Tipo)
													  VALUES ('$pedido_id', '$func_id', '', '', '$item_valor_comissao', '$comissao_porc', '$comissao_reais', '1')");	
						}
					}
					
					$sqlInsert_Item = mysql_query("INSERT INTO tblpedido_item
					(fldProduto_Id, fldPedido_Id, fldQuantidade, fldValor, 	fldValor_Compra, fldDesconto, fldDescricao, fldFardo_Id,  fldTabela_Preco_Id, fldEstoque_Id, fldReferencia, fldProduto_Tipo_Id, fldEntregue, fldEntregueData)
					VALUES(
					'$produto_id',			'$pedido_id',
					'$item_quantidade',		'$item_valor',
					'$valor_compra',		'$item_desconto',
					'$item_nome',			'$item_fardo',
					$item_tabela_preco,		'$item_estoque_id',
					'$item_referencia',
					'$tipo_id',				'$item_entregue',
					$item_entregue_data
					)") or die (mysql_error());
					
					$rsPedido_Item 	= mysql_query("SELECT last_insert_id() as lastID");
					$LastItemId 	= mysql_fetch_array($rsPedido_Item);
					$item_id 		= $LastItemId['lastID'];
					
					##LANCAR NO ESTOQUE CASO NATUREZA DA OPERACAO ESTEJA PARA CONTROLAR ESTOQUE
					if($nfe_estoque_controle == '1'){
						fnc_estoque_movimento_lancar($produto_id, '', '', $item_quantidade, 1, $pedido_id, $item_estoque_id, $item_estoque_id, '', $item_id);
						
						//insiro os componentes.
						############################################################################################################################################################################################					
						//agora tem 2 tipos de relacionamento, pesquiso por um, dps por outro!
						//primeiro o comum
						$sqlComponente = mysql_query("SELECT tblproduto_componente.* FROM tblproduto_componente WHERE fldProduto_Id = $produto_id");
						if(mysql_num_rows($sqlComponente) > 0){ //normal
							while($rowComponente = mysql_fetch_assoc($sqlComponente)){
								//insere todos os componentes, al�m de mexer no estoque!
								$componente_id		= $rowComponente['fldProduto_Componente_Id'];
								$componente_qtd		= $rowComponente['fldComponente_Qtd'] * $item_quantidade;
	
								//insert na tblpedido_item_componente
								mysql_query("INSERT INTO tblpedido_item_componente
											(fldProduto_Id, fldItem_Id, fldPedido_Id, fldComponente_Id, fldQtd, fldExcluido)
											VALUES ('$produto_id', '$item_id', '$pedido_id', '$componente_id', '$componente_qtd', '0')") or die(mysql_error());
											
								fnc_estoque_movimento_lancar($componente_id, '', '', $componente_qtd, 12, $pedido_id, $item_estoque_id, $item_estoque_id, '', $item_id);
							}
						}
						
						##############################################################################################################################################################################################
						//agora inverso
						$sqlComponente = mysql_query("SELECT tblproduto_componente.* FROM tblproduto_componente WHERE fldProduto_Componente_Id = $produto_id AND fldRelacionamento = 2");
						if(mysql_num_rows($sqlComponente) > 0){ //inverso
							while($rowComponente = mysql_fetch_assoc($sqlComponente)){
								//insere todos os componentes, al�m de mexer no estoque!
								$componente_id		= $rowComponente['fldProduto_Id'];
								$componente_qtd		= $rowComponente['fldQtd_Proporcional'] * $item_quantidade;
	
								//insert na tblpedido_item_componente
								mysql_query("INSERT INTO tblpedido_item_componente
											(fldProduto_Id, fldItem_Id, fldPedido_Id, fldComponente_Id, fldQtd, fldExcluido)
											VALUES ('$produto_id', '$item_id', '$pedido_id', '$componente_id', '$componente_qtd', '0')") or die(mysql_error());
											
								fnc_estoque_movimento_lancar($componente_id, '', '', $componente_qtd, 12, $pedido_id, $item_estoque_id, $item_estoque_id, '', $item_id);
							}
						}
					}
				
					### OTICA ################################################################################################################################################################################################
					#CONFERE QUAL TEM O NUMERO DA LISTAGEM E GRAVA O ID DO ITEM NA VARIAVEL 
						
					if($otica_valores['perto']['armacao'] == $n){
						$Otica_Perto_Armacao = $item_id;
					}
					if($otica_valores['perto']['material'] == $n){
						$Otica_Perto_Material = $item_id;
					}
					if($otica_valores['longe']['armacao'] == $n){
						$Otica_Longe_Armacao = $item_id;
					}
					if($otica_valores['longe']['material'] == $n){
						$Otica_Longe_Material = $item_id;
					}
					
					### NFE ##################################################################################################################################################################################################
					$NCM 		 							= $_POST['txt_nfe_pedido_item_principal_ncm_'.$n];
					$EXTIPI 	 							= $_POST['txt_nfe_pedido_item_principal_ex_tipi_'.$n];
					$CFOP 		 							= $_POST['txt_nfe_pedido_item_principal_cfop_'.$n];
					$UnidadeComercial 						= $_POST['txt_nfe_pedido_item_principal_unidade_comercial_'.$n];
					
					$QtdComercial 							= format_number_in($_POST['txt_item_quantidade_'.$n]); //ser� mesma que quantidade do item
					$ValorUnitComercial 					= format_number_in($_POST['txt_item_valor_'.$n]);//ser� mesma que valor do item
					$desconto			 					= format_number_in($_POST['txt_nfe_pedido_item_principal_desconto_'.$n]);//ser� mesma que valor do item
					$UnidadeTributavel 						= format_number_in($_POST['txt_nfe_pedido_item_principal_unidade_tributavel_'.$n]);
					$QtdTributavel 							= format_number_in($_POST['txt_nfe_pedido_item_principal_quantidade_tributavel_'.$n]);
					$ValorUnitTributavel 					= format_number_in($_POST['txt_nfe_pedido_item_principal_valor_unidade_tributavel_'.$n]);
					$TotalSeguro 							= format_number_in($_POST['txt_nfe_pedido_item_principal_total_seguro_'.$n]);
					
					$sub_total								= $item_valor * $item_quantidade;
					$total_desconto							= ($item_desconto / 100) * $sub_total;
					$total_item								+= $sub_total - $total_desconto;
					
					$Frete 									= format_number_in($_POST['txt_nfe_pedido_item_principal_frete_'.$n]);
					$EAN 									= format_number_in($_POST['txt_nfe_pedido_item_principal_ean_'.$n]);
					$EANTributavel 							= format_number_in($_POST['txt_nfe_pedido_item_principal_ean_tributavel_'.$n]);
					$OutrasDespesasAcessorias		 		= format_number_in($_POST['txt_nfe_pedido_item_principal_outras_despesas_acessorias_'.$n]);
					$ValorTotalBruto 						= format_number_in($_POST['txt_nfe_pedido_item_principal_valor_total_bruto_'.$n]);
					$PedidoCompra 							= format_number_in($_POST['txt_nfe_pedido_item_principal_pedido_compra_'.$n]);
					$NumeroItemPedidoCompra	 				= format_number_in($_POST['txt_nfe_pedido_item_principal_numero_item_pedido_compra_'.$n]);
					$ValorTotalBrutoChk 					= ($_POST['chk_nfe_pedido_item_principal_valor_total_bruto_'.$n] == true)? '1' : '0';
					
					$InformacoesAdicionais					= $_POST['txt_nfe_item_pedido_informacoes_adicionais_'.$n];
					$radImposto								= $_POST['hid_nfe_tributo_rad_'.$n];
					
					$ICMSSituacaoTributaria					= $_POST['hid_nfe_pedido_item_tributos_icms_situacao_tributaria_'.$n];
					$ICMSOrigem 							= $_POST['hid_nfe_pedido_item_tributos_icms_origem_'.$n];
					
					$ICMSAliquotaAplicavelCalculoCredito	= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_pCredSN_'.$n]);
					$ICMSValorCreditoAproveitado			= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_vCredICMSSN_'.$n]);
					
					$ICMSModalidadeDeterminacaoBC 			= format_number_in($_POST['hid_nfe_pedido_item_tributos_icms_modBC_'.$n]);
					$ICMSValorBC				 			= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_vBC_'.$n]);
					$ICMSPorcentReducaoBC	 				= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_pRedBC_'.$n]);
					$ICMSAliquota 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_pICMS_'.$n]);
					$ICMSValor	 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_vICMS_'.$n]);
					$ICMSPorcentBCOperacaoPropria 			= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_pBCOp_'.$n]);
					$ICMSMotivoDesoneracao 					= $_POST['hid_nfe_pedido_item_tributos_icms_motDesICMS_'.$n];
					$ICMSSTModalidadeDetermicaoBC	 		= $_POST['hid_nfe_pedido_item_tributos_icms_st_modBCST_'.$n];
					$ICMSSTPorcentReducaoBC 				= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_st_pRedBCST_'.$n]);
					$ICMSSTPorcentMargemValorAdicional 		= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_st_pMVAST_'.$n]);
					$ICMSSTValorBC 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_st_vBCST_'.$n]);
					$ICMSSTAliquota 						= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_st_pICMSST_'.$n]);
					$ICMSSTValor 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_st_vICMSST_'.$n]);
						
					$ICMSSTUFDevido 						= $_POST['hid_nfe_pedido_item_tributos_icms_st_UFST_'.$n];
					
					$ICMSSTValorBCRetidoAnterior			= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_st_vBCSTRet_'.$n]);
					$ICMSSTValorRetidoAnterior 				= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_st_vICMSSTRet_'.$n]);
					$ICMSSTValorBCUFDestino					= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_st_vBCSTDest_'.$n]);
					$ICMSSTValorUFDestino					= format_number_in($_POST['txt_nfe_pedido_item_tributos_icms_st_vICMSSTDest_'.$n]);
					
					$COFINSSituacaoTributaria	 			= $_POST['hid_nfe_pedido_item_tributos_cofins_situacao_tributaria_'.$n];
					$COFINSTipoCalculo 						= $_POST['hid_nfe_pedido_item_tributos_cofins_calculo_tipo_'.$n];
					$COFINSValorBC 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_cofins_valor_base_calculo_'.$n]);
					$COFINSAliquotaPercentual 				= format_number_in($_POST['txt_nfe_pedido_item_tributos_cofins_aliquota_percentual_'.$n]);
					$COFINSAliquotaReais 					= format_number_in($_POST['txt_nfe_pedido_item_tributos_cofins_aliquota_reais_'.$n]);
					$COFINSQtdVendida 						= format_number_in($_POST['txt_nfe_pedido_item_tributos_cofins_quantidade_vendida_'.$n]);
					$COFINSValor 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_cofins_valor_'.$n]);
					
					$COFINSSTTipoCalculo 					= $_POST['hid_nfe_pedido_item_tributos_cofins_st_calculo_tipo_'.$n];
					$COFINSSTValorBC 						= format_number_in($_POST['txt_nfe_pedido_item_tributos_cofins_st_valor_base_calculo_'.$n]);
					$COFINSSTAliquotaPercentual 			= format_number_in($_POST['txt_nfe_pedido_item_tributos_cofins_st_aliquota_percentual_'.$n]);
					$COFINSSTAliquotaReais 					= format_number_in($_POST['txt_nfe_pedido_item_tributos_cofins_st_aliquota_reais_'.$n]);
					$COFINSSTQtdVendida 					= format_number_in($_POST['txt_nfe_pedido_item_tributos_cofins_st_quantidade_vendida_'.$n]);
					$COFINSSTValor 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_cofins_st_valor_'.$n]);
					
					$IIValorBC 								= format_number_in($_POST['txt_nfe_pedido_item_tributos_ii_valor_base_calculo_'.$n]);
					$IIValorDespesasAduaneiras 				= format_number_in($_POST['txt_nfe_pedido_item_tributos_ii_valor_aduaneiras_'.$n]);
					$IIValorIOF 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_ii_valor_iof_'.$n]);
					$IIValor 								= format_number_in($_POST['txt_nfe_pedido_item_tributos_ii_valor_'.$n]);
					
					$IPISituacaoTributaria					= $_POST['hid_nfe_pedido_item_tributos_ipi_situacao_tributaria_'.$n];
					$IPIClasseEnquadramento 				= $_POST['txt_nfe_pedido_item_tributos_ipi_enquadramento_classe_'.$n];
					$IPICodigoEnquadramentoLegal			= $_POST['txt_nfe_pedido_item_tributos_ipi_enquadramento_codigo_'.$n];
					$IPICNPJProdutor						= $_POST['txt_nfe_pedido_item_tributos_ipi_produtor_cnpj_'.$n];
					$IPICodigoSeloControle 					= $_POST['txt_nfe_pedido_item_tributos_ipi_selo_controle_codigo_'.$n];
					$IPIQtdSeloControle 					= $_POST['txt_nfe_pedido_item_tributos_ipi_selo_controle_quantidade_'.$n];
					$IPITipoCalculo 						= $_POST['hid_nfe_pedido_item_tributos_ipi_calculo_tipo_'.$n];
					$IPIValorBC 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_ipi_valor_base_calculo_'.$n]);
					$IPIAliquota 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_ipi_aliquota_'.$n]);
					$IPIQtdTotalUnidadePadrao				= format_number_in($_POST['txt_nfe_pedido_item_tributos_ipi_unidade_padrao_quantidade_total_'.$n]);
					$IPIValorUnidade 						= format_number_in($_POST['txt_nfe_pedido_item_tributos_ipi_unidade_valor_'.$n]);
					$IPIValor 								= format_number_in($_POST['txt_nfe_pedido_item_tributos_ipi_valor_'.$n]);
					
					$ISSQNTributacao 						= $_POST['hid_nfe_pedido_item_tributos_issqn_tributacao_'.$n];
					$ISSQNValorBC 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_issqn_valor_base_calculo_'.$n]);
					$ISSQNAliquota 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_issqn_aliquota_'.$n]);
					$ISSQNListaServico 						= $_POST['hid_nfe_pedido_item_tributos_issqn_servico_lista_'.$n];
					$ISSQNUF 								= $_POST['hid_nfe_pedido_item_tributos_issqn_uf_'.$n];
					$ISSQNMunicipioOcorrencia 				= $_POST['hid_nfe_pedido_item_tributos_issqn_municipio_'.$n];
					$ISSQNValor 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_issqn_valor_issqn_'.$n]);
							
					$PISSituacaoTributaria	 				= $_POST['hid_nfe_pedido_item_tributos_pis_situacao_tributaria_'.$n];
					$PISTipoCalculo 						= $_POST['hid_nfe_pedido_item_tributos_pis_calculo_tipo_'.$n];
					$PISValorBC 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_pis_valor_base_calculo_'.$n]);
					$PISAliquotaPercentual 					= format_number_in($_POST['txt_nfe_pedido_item_tributos_pis_aliquota_percentual_'.$n]);
					$PISAliquotaReais 						= format_number_in($_POST['txt_nfe_pedido_item_tributos_pis_aliquota_reais_'.$n]);
					$PISQtdVendida 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_pis_quantidade_vendida_'.$n]);
					$PISValor 								= format_number_in($_POST['txt_nfe_pedido_item_tributos_pis_valor_'.$n]);
					$PISSTTipoCalculo 						= $_POST['hid_nfe_pedido_item_tributos_pis_st_calculo_tipo_'.$n];
					$PISSTValorBC 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_pis_st_valor_base_calculo_'.$n]);
					$PISSTAliquotaPercentual 				= format_number_in($_POST['txt_nfe_pedido_item_tributos_pis_st_aliquota_percentual_'.$n]);
					$PISSTAliquotaReais 					= format_number_in($_POST['txt_nfe_pedido_item_tributos_pis_st_aliquota_reais_'.$n]);
					$PISSTQtdVendida 						= format_number_in($_POST['txt_nfe_pedido_item_tributos_pis_st_quantidade_vendida_'.$n]);
					$PISSTValor 							= format_number_in($_POST['txt_nfe_pedido_item_tributos_pis_st_valor_'.$n]);

					$Tipo_Especifico 						= $_POST['txt_nfe_item_produto_especifico_id_'.$n];
					$Combustivel_Cod_ANP 					= $_POST['txt_nfe_item_produto_especifico_combustivel_anp_'.$n];
					$Combustivel_Cod_IF 					= $_POST['txt_nfe_item_produto_especifico_combustivel_if_'.$n];
					$Combustivel_UF 						= $_POST['txt_nfe_item_produto_especifico_combustivel_uf_'.$n];
					$Combustivel_Qtd_Faturada 				= $_POST['txt_nfe_item_produto_especifico_combustivel_qtd_faturada_'.$n];
					$Combustivel_Cide_bCalculo 				= $_POST['txt_nfe_item_produto_especifico_combustivel_cide_bc_'.$n];
					$Combustivel_Cide_Aliquota 				= $_POST['txt_nfe_item_produto_especifico_combustivel_cide_aliquota_'.$n];
					$Combustivel_Cide_Valor 				= $_POST['txt_nfe_item_produto_especifico_combustivel_cide_valor_'.$n];
					
					$Total_Tributo							= format_number_in($_POST['txt_nfe_item_produto_especifico_total_tributos_'.$n]);
					
					$sqlInsert_Item_Fiscal =
					"INSERT INTO tblpedido_item_fiscal (
						fldItem_Id,
						fldncm,
						fldex_tipi,
						fldcfop,
						fldunidade_comercial,
						fldquantidade_comercial,
						fldvalor_unitario_comercial,
						fldunidade_tributavel,
						fldquantidade_tributavel,
						fldvalor_unitario_tributavel,
						fldtotal_seguro,
						fldoutras_despesas_acessorias,
						fldfrete,
						flddesconto,
						fldean,
						fldean_unidade_tributavel,
						fldvalor_total_bruto,
						fldpedido_compra,
						fldnumero_item_pedido_compra,
						fldvalor_bruto_compoe_total,
						fldinformacoes_adicionais,
						fldrad_imposto,
						fldgenero,
						fldicms_origem,
						
						fldicms_situacao_tributaria,
						fldicms_base_calculo_modalidade,
						fldicms_calculo_credito_aliquota_aplicavel,
						fldicms_credito_aproveitado,
						fldicms_base_calculo_percentual_reducao,
						fldicms_aliquota,
						fldicms_base_calculo,
						fldicms_valor,
						fldicms_base_calculo_operacao_propria,
						fldicms_desoneracao_motivo,
						fldicmsst_base_calculo_modalidade,
						fldicmsst_base_calculo_percentual_reducao,
						fldicmsst_mva,
						fldicmsst_aliquota,
						fldicmsst_valor,
						fldicmsst_base_calculo,
						fldicmsst_base_calculo_retido_uf_remetente,
						fldicmsst_valor_retido_uf_remetente,
						fldicmsst_base_calculo_uf_destino,
						fldicmsst_valor_uf_destino,
						fldicmsst_uf,
						fldpis_situacao_tributaria,
						fldpis_tipo_calculo,
						fldpis_base_calculo,
						fldpis_aliquota_percentual,
						fldpis_aliquota_reais,
						fldpis_quantidade_vendida,
						fldpis_valor,
						fldpisst_tipo_calculo,
						fldpisst_base_calculo,
						fldpisst_aliquota_percentual,
						fldpisst_aliquota_reais,
						fldpisst_quantidade_vendida,
						fldpisst_valor,
						fldipi_situacao_tributaria,
						fldipi_enquadramento_classe,
						fldipi_enquadramento_codigo,
						fldipi_produtor_cnpj,
						fldipi_calculo_tipo,
						fldipi_aliquota,
						fldipi_unidade_quantidade_total,
						fldipi_unidade_valor,
						fldipi_selo_controle_codigo,
						fldipi_selo_controle_quantidade,
						fldipi_base_calculo,
						fldipi_valor,
						fldcofins_situacao_tributaria,
						fldcofins_tipo_calculo,
						fldcofins_base_calculo,
						fldcofins_aliquota_percentual,
						fldcofins_aliquota_reais,
						fldcofins_quantidade_vendida,
						fldcofins_valor,
						fldcofinsst_tipo_calculo,
						fldcofinsst_base_calculo,
						fldcofinsst_aliquota_percentual,
						fldcofinsst_aliquota_reais,
						fldcofinsst_quantidade_vendida,
						fldcofinsst_valor,
						fldii_base_calculo,
						fldii_despesas_aduaneiras_valor,
						fldii_iof_valor,
						fldii_valor,
						fldissqn_tributacao,
						fldissqn_base_calculo,
						fldissqn_aliquota,
						fldissqn_lista_servico,
						fldissqn_uf,
						fldissqn_municipio_ocorrencia,
						fldissqn_valor,
						
						fldTipo_Especifico,
						fldCombustivel_Codigo_ANP,
						fldCombustivel_Codigo_CodIF,
						fldCombustivel_UF,
						fldCombustivel_Qtd_Faturada,
						fldCombustivel_Cide_bCalculo,
						fldCombustivel_Cide_Aliquota,
						fldCombustivel_Cide_Valor,
						
						fldTotal_Tributo
					)
					VALUES(
						'$item_id',
						'$NCM',
						'$EXTIPI',
						'$CFOP',
						'$UnidadeComercial',
						'$QtdComercial',
						'$ValorUnitComercial',
						'$UnidadeTributavel',
						'$QtdTributavel',
						'$ValorUnitTributavel',
						'$TotalSeguro',
						'$OutrasDespesasAcessorias',
						'$Frete',
						'$desconto',
						'$EAN',
						'$EANTributavel',
						'$ValorTotalBruto',
						'$PedidoCompra',
						'$NumeroItemPedidoCompra',
						'$ValorTotalBrutoChk',
						'$InformacoesAdicionais',
						'$radImposto',
						'$genero',
						'$ICMSOrigem',
						'$ICMSSituacaoTributaria',
						'$ICMSModalidadeDeterminacaoBC',
						'$ICMSAliquotaAplicavelCalculoCredito',
						'$ICMSValorCreditoAproveitado',
						'$ICMSPorcentReducaoBC',
						'$ICMSAliquota',
						'$ICMSValorBC',
						'$ICMSValor',
						'$ICMSPorcentBCOperacaoPropria',
						'$ICMSMotivoDesoneracao',
						
						'$ICMSSTModalidadeDetermicaoBC',
						'$ICMSSTPorcentReducaoBC',
						'$ICMSSTPorcentMargemValorAdicional',
						'$ICMSSTAliquota',
						'$ICMSSTValor',
						'$ICMSSTValorBC',
						'$ICMSSTValorBCRetidoAnterior',
						'$ICMSSTValorRetidoAnterior',
						'$ICMSSTValorBCUFDestino',
						'$ICMSSTValorUFDestino',
						'$ICMSSTUFDevido',
						'$PISSituacaoTributaria',
						'$PISTipoCalculo',
						'$PISValorBC',
						'$PISAliquotaPercentual',
						'$PISAliquotaReais',
						'$PISQtdVendida',
						'$PISValor',
						'$PISSTTipoCalculo',
						'$PISSTValorBC',
						'$PISSTAliquotaPercentual',
						'$PISSTAliquotaReais',
						'$PISSTQtdVendida',
						'$PISSTValor',
						'$IPISituacaoTributaria',
						'$IPIClasseEnquadramento',
						'$IPICodigoEnquadramentoLegal',
						'$IPICNPJProdutor',
						'$IPITipoCalculo',
						'$IPIAliquota',
						'$IPIQtdTotalUnidadePadrao',
						'$IPIValorUnidade',
						'$IPICodigoSeloControle',
						'$IPIQtdSeloControle',
						'$IPIValorBC',
						'$IPIValor',
						'$COFINSSituacaoTributaria',
						'$COFINSTipoCalculo',
						'$COFINSValorBC',
						'$COFINSAliquotaPercentual',
						'$COFINSAliquotaReais',
						'$COFINSQtdVendida',
						'$COFINSValor',
						'$COFINSSTTipoCalculo',
						'$OFINSSTValBC',
						'$COFINSSTAliquotaPercentual',
						'$COFINSSTAliquotaReais',
						'$COFINSSTQtdVendida',
						'$COFINSSTValor',
						'$IIValorBC',
						'$IIValorDespesasAduaneiras',
						'$IIValorIOF',
						'$IIValor',
						'$ISSQNTributacao',
						'$ISSQNValorBC',
						'$ISSQNAliquota',
						'$ISSQNListaServico',
						'$ISSQNUF',
						'$ISSQNMunicipioOcorrencia',
						'$ISSQNValor',
						
						'$Tipo_Especifico',
						'$Combustivel_Cod_ANP',
						'$Combustivel_Cod_IF',
						'$Combustivel_UF',
						'$Combustivel_Qtd_Faturada',
						'$Combustivel_Cide_bCalculo',
						'$Combustivel_Cide_Aliquota',
						'$Combustivel_Cide_Valor',
						'$Total_Tributo'
					)";

					mysql_query($sqlInsert_Item_Fiscal) or die (mysql_error());
				}
				$n += 1;


				//REGISTA OS COMPONENTES DOS PRODUTOS
				$sqlInsert_Componente = mysql_query("SELECT tblproduto_componente.*, tblproduto.fldEstoque_Controle FROM tblproduto_componente 
													 LEFT JOIN tblproduto ON tblproduto.fldId = tblproduto_componente.fldProduto_Componente_Id
													 WHERE fldProduto_Id = $produto_id");

				while($rowInsert_Componente = mysql_fetch_assoc($sqlInsert_Componente)){
					$id_componente		= $rowInsert_Componente['fldProduto_Componente_Id'];
					$sqlComponente		= mysql_query("SELECT fldNome FROM tblproduto WHERE fldId = $id_componente");
					$rowComponente		= mysql_fetch_assoc($sqlComponente);
					$componente_qtd		= $rowInsert_Componente['fldComponente_Qtd'] * $item_quantidade;
					$componente_desc	= $rowComponente['fldNome'];
					$controlar_estoque 	= $rowInsert_Componente['fldEstoque_Controle'];
					
					mysql_query("INSERT INTO tblpedido_item_componente
					(fldComponente_Id, fldProduto_Id, fldItem_Id, fldPedido_Id, fldComponente_Qtd, fldDescricao)
					VALUES(
					'$id_componente',
					'$produto_id',
					'$item_id',
					'$pedido_id',
					'$componente_qtd',
					'$componente_desc'
					)");
					
					if($controlar_estoque == '1' || $controlar_estoque == '3'){
						fnc_estoque_movimento_lancar($id_componente, '', '', $componente_qtd, 12, $pedido_id, $item_estoque_id, $item_estoque_id, '', $item_id);
					}
				}


			}
			
			### OTICA ################################################################################################################################################################################################
			//Inser��o dos dados recebidos do Pedido (�tica) no Banco de Dados.
			if($_SESSION["sistema_tipo"] == "otica"){
				$inserir_dados_otica = mysql_query("
				INSERT INTO tblpedido_otica (
				fldPedido_Id,
				fldResponsavel,
				fldPerto_Armacao_Item_Id,
				fldPerto_Material_Item_Id,
				fldPerto_OD_Esferico,
				fldPerto_OD_Cilindro,
				fldPerto_OD_Eixo,
				fldPerto_OD_DP,
				fldPerto_OE_Esferico,
				fldPerto_OE_Cilindro,
				fldPerto_OE_Eixo,
				fldPerto_OE_DP,
				fldLonge_Armacao_Item_Id,
				fldLonge_Material_Item_Id,
				fldLonge_OD_Esferico,
				fldLonge_OD_Cilindro,
				fldLonge_OD_Eixo,
				fldLonge_OD_DP,
				fldLonge_OE_Esferico,
				fldLonge_OE_Cilindro,
				fldLonge_OE_Eixo,
				fldLonge_OE_DP,
				fldBifocal_Marca_Id,
				fldBifocal_Lente_Id,
				fldBifocal_OD_Altura,
				fldBifocal_OD_DNP,
				fldBifocal_OD_Adicao,
				fldBifocal_OE_Altura,
				fldBifocal_OE_DNP,
				fldBifocal_OE_Adicao,
				fldMedico,
				fldMedico_crm
				)
				VALUES (
				'".$pedido_id."',
				'".$_POST['sel_cliente_responsavel']."',
				'".$Otica_Perto_Armacao."',
				'".$Otica_Perto_Material."',
				'".$otica_valores['perto']['OD']['esferico']."',
				'".$otica_valores['perto']['OD']['cilindrico']."',
				'".$otica_valores['perto']['OD']['eixo']."',
				'".$otica_valores['perto']['OD']['DP']."',
				'".$otica_valores['perto']['OE']['esferico']."',
				'".$otica_valores['perto']['OE']['cilindrico']."',
				'".$otica_valores['perto']['OE']['eixo']."',
				'".$otica_valores['perto']['OE']['DP']."',
				'".$Otica_Longe_Armacao."',
				'".$Otica_Longe_Material."',
				'".$otica_valores['longe']['OD']['esferico']."',
				'".$otica_valores['longe']['OD']['cilindrico']."',
				'".$otica_valores['longe']['OD']['eixo']."',
				'".$otica_valores['longe']['OD']['DP']."',
				'".$otica_valores['longe']['OE']['esferico']."',
				'".$otica_valores['longe']['OE']['cilindrico']."',
				'".$otica_valores['longe']['OE']['eixo']."',
				'".$otica_valores['longe']['OE']['DP']."',
				'".$otica_valores['bifocal']['marca']."',
				'".$otica_valores['bifocal']['lente']."',
				'".$otica_valores['bifocal']['OD']['altura']."',
				'".$otica_valores['bifocal']['OD']['DNP']."',
				'".$otica_valores['bifocal']['OD']['adicao']."',
				'".$otica_valores['bifocal']['OE']['altura']."',
				'".$otica_valores['bifocal']['OE']['DNP']."',
				'".$otica_valores['bifocal']['OE']['adicao']."',
				'".$_POST['txt_pedido_otica_medico']."',
				'".$_POST['txt_pedido_otica_medico_crm']."'
				)") or die (mysql_error());
			}
			//INSERIR FUNCIONARIO DE VENDA - TOTAL DE ITENS PARA ARMAZENAR JUNTO
			if(fnc_sistema('pedido_comissao') != '2'){
				$Funcionario_Id		= $_POST['txt_funcionario_codigo'];
				if($Funcionario_Id > 0){
					$Funcionario_Valor		= $total_item;
					
					$rsComissao  			= mysql_query("SELECT fldFuncao1_Comissao FROM tblfuncionario WHERE fldId = '$Funcionario_Id'");
					$rowComissao 			= mysql_fetch_array($rsComissao);				
					$Comissao				= $rowComissao['fldFuncao1_Comissao'];
					
					$insertServico			= "INSERT INTO tblpedido_funcionario_servico (fldPedido_Id, fldFuncionario_Id, fldServico, fldTempo, fldValor, fldComissao, fldFuncao_Tipo)
												VALUES ('$pedido_id', '$Funcionario_Id', '', '', '$Funcionario_Valor', '$Comissao', '1')";
					
					mysql_query($insertServico);
					echo mysql_error();
				}
			}
			###################################################################################################################################################################################################
			//adicionando as parcelas, na tabela
			$y= 1;
			$limite = $_POST["hid_controle_parcela"];
			while($y <= $limite){
				if(isset($_POST['txt_parcela_numero_'.$y])){
					
					$parcela_numero 	= $_POST['txt_parcela_numero_'.$y];
					$parcela_vencimento = format_date_in($_POST['txt_parcela_data_'.$y]);
					$parcela_valor		= format_number_in($_POST['txt_parcela_valor_'.$y]);
					$pagamento_tipo 	= $_POST['sel_pagamento_tipo_'.$y];
					$parcela_obs		= $_POST['txt_pedido_obs'];
					
					if($parcela_valor > 0){	
						mysql_query ("INSERT INTO tblpedido_parcela
						(fldPedido_Id, fldParcela, fldVencimento, fldValor, fldPagamento_Id, fldObservacao, fldStatus)
						values(
							'$pedido_id',
							'$parcela_numero',
							'$parcela_vencimento',
							'$parcela_valor',
							'$pagamento_tipo',
							'$parcela_obs',
							'1'
						)") or die (mysql_error());
						
						$LastId = mysql_fetch_array(mysql_query("Select last_insert_id() as lastID"));
						
						//ACOES DE PAGAMENTO CONFORME TIPO 
						$rowPagamentoTipo = mysql_fetch_array(mysql_query("SELECT * FROM tblpagamento_tipo WHERE fldId = $pagamento_tipo"));
						$sigla = $rowPagamentoTipo['fldSigla'];
						echo mysql_error();
							
						$acao = fnc_sistema("pedido_parcela_acao_$sigla");
						
						//verifica se eh a vista, se for substitui o $acao
						if($data == $parcela_vencimento){
							$acao = fnc_sistema('pedido_parcela_acao_AV');
						}
						
						if($acao == 2){ 
							$insertBaixa = mysql_query("insert into tblpedido_parcela_baixa
								(fldParcela_Id, fldDataCadastro, fldValor, fldDataRecebido)
								values(
								'".$LastId['lastID']."',
								'$data',
								'$parcela_valor',
								'$data'
							)");
							
							if($insertBaixa){
								//lan�ar no caixa
								$conta = fnc_sistema("pedido_parcela_conta_$sigla");
								$LastIdBaixa = mysql_fetch_array(mysql_query("Select last_insert_id() as lastID from tblpedido_parcela_baixa"));
								//$descricao, $credito, $debito, $entidade, $pagamento_tipo_id, $referencia_id, $movimento_tipo, $marcador, $conta
								fnc_financeiro_conta_fluxo_lancar('', $parcela_valor, 0, '', $pagamento_tipo, $LastIdBaixa['lastID'], 3, '', $conta);
							}
						}
						/****************************************************************************************/
					
					}
				}
				$y += 1;
			}
		}
		//cheques
		if(!empty($_SESSION['ref_timestamp'])){ //CASO A SESSAO ESTIVER EM BRANCO, PRA NAO ATUALIZAR TODOS OS CHEQUES 
			/*
			$origem_movimento_id = '3'; //recebimento de parcela
			$timestamp			= $_SESSION['ref_timestamp'];
			$origem_id 			= $pedido_id;
			fnc_cheque_update($origem_id, '', '', $origem_movimento_id, '', $timestamp);
			*/
			############################################################################################v#######################################
			
			$timestamp	 = $_SESSION['ref_timestamp'];
			$movimento_id= '3';
			$registro_id = $pedido_id;
			$conta_id	 = $conta; //ja definido nas parcelas acima
			fnc_cheque_movimento_lancar($timestamp, $movimento_id, $registro_id, $conta_id);
			
		}
		unset($_SESSION['ref_timestamp']);
		
		if(!mysql_error()){
			header("location:index.php?p=pedido_novo_confirma&tipo=3&id=$pedido_id");
		}else {
?>			<div class="alert">
				<p class="erro">N&atilde;o foi poss&iacute;vel gravar os dados</p>
				<a class="voltar" href="javascript:history.back();">voltar</a>
			</div>
<?			echo mysql_error();
		}
	}else{
		$remote_ip 		= gethostbyname($REMOTE_ADDR);
		$_SESSION['ref_timestamp'] = $remote_ip.date("YmdHis");
		$rowUsuario 	= mysql_fetch_array(mysql_query("SELECT * FROM tblusuario where fldId =".$usuario_id));
?>
		<ul class="header_bar">
			<li><a class="modal btn_novo" id="importar-vendas" rel="600-480" href="nfe_importar_vendas,<?=$nfe_natureza_operacao_id?>" title="Importar venda(s)">Importar venda(s)</a></li>
		</ul>
		
        <div class="form">
            <form class="frm_detalhe gravar_nfe" style="width:890px" id="frm_pedido_nfe_novo" action="" method="post">
                <ul>
					<li>
                        <label for="txt_codigo">Id</label>
                        <input type="text" style="width:70px; text-align:right" id="txt_codigo" name="txt_codigo" disabled="disabled" value="novo"/>
                    </li>
                    <li>
                        <label for="txt_usuario">Usu&aacute;rio</label>
                        <input type="text" style="width:200px" id="txt_usuario" name="txt_usuario" value="<?=$rowUsuario['fldUsuario']?>" readonly="readonly"/>
                    </li>
                    <li>
                        <label for="sel_marcador">Marcador</label>
                        <select style="width:205px" id="sel_marcador" name="sel_marcador" >
	                        <option value="0">Selecionar</option>
<?							$rsMarcador = mysql_query("SELECT * FROM tblpedido_marcador WHERE fldExcluido = 0 ORDER BY fldMarcador");
							while($rowMarcador = mysql_fetch_array($rsMarcador)){                            
?>                   	       <option value="<?=$rowMarcador['fldId']?>"><?=$rowMarcador['fldMarcador']?></option>
<?							}
?>                 		</select>
                    </li>
                    <li>
                        <label for="txt_referencia">Referencia</label>
                        <input type="text" style="width:80px" id="txt_referencia" name="txt_referencia" value="" onkeyup="numOnly(this)"/>
                    </li>
                </ul>
				<ul>
					<li>
                        <label for="txt_serie">S&eacute;rie</label>
                        <input type="text" style="width:70px;text-align:right" id="txt_serie" name="txt_serie" value="<?=fnc_sistema('nfe_serie')?>"/>
                    </li>
					<li>
                        <label for="txt_pedido_numero">NFe</label>
                        <input type="text" style="width:80px;text-align:right;background:#FFC" id="txt_pedido_numero" name="txt_pedido_numero" disabled="disabled" value="novo"/>
                    </li>
                    <li>
                        <label for="txt_pedido_data">Data Emiss&atilde;o</label>
                        <input type="text" style="width:80px; text-align: center;" class="calendario-mask" id="txt_pedido_data" name="txt_pedido_data" value="<?=date('d/m/Y')?>" />
                        <a href="#" title="Exibir calend&aacute;rio" class="exibir-calendario-data-atual"></a>
                    </li>
                    <li>
                        <label for="txt_pedido_hora">Hora Emiss&atilde;o</label>
                        <input type="text" style="width:80px; text-align: right" id="txt_pedido_hora" name="txt_pedido_hora" value="<?=date('H:i:s')?>" readonly="readonly" />
                    </li>
					<li>
                        <label for="txt_pedido_saida_data">Data Sa&iacute;da</label>
                        <input type="text" style="width:80px; text-align: center;" class="calendario-mask" id="txt_pedido_saida_data" name="txt_pedido_saida_data" value="<?=date('d/m/Y')?>" />
                        <a href="#" title="Exibir calend&aacute;rio" class="exibir-calendario-data-atual"></a>
                    </li>
					<li>
                        <label for="txt_pedido_saida_hora">Hora Sa&iacute;da</label>
                        <input type="text" style="width:80px;text-align:right" id="txt_pedido_saida_hora" name="txt_pedido_saida_hora" value="<?=date('H:i')?>"/>
                    </li>
                    <li>
                        <label for="txt_pedido_natureza_operacao">Natureza da Opera&ccedil;&atilde;o</label>
                        <input type="text" 	 name="txt_pedido_natureza_operacao" 	id="txt_pedido_natureza_operacao" 		value="<?=$nfe_natureza_operacao?>" readonly="readonly" style="width:200px" />
                        <input type="hidden" name="hid_pedido_natureza_operacao_id"	id="hid_pedido_natureza_operacao_id" 	value="<?=$nfe_natureza_operacao_id?>" />
                        <input type="hidden" name="hid_parcela_exibir" 		id="hid_parcela_exibir" 	value="<?=$nfe_parcelamento_exibir?>" />
                        <input type="hidden" name="hid_estoque_controlar" 	id="hid_estoque_controlar" 	value="<?=$nfe_estoque_controlar?>" />
                    </li>
                    <li>
                        <label for="sel_pedido_operacao_tipo">Tipo de documento</label>
                        <select class="sel_pedido_operacao_tipo" style="width:120px;" id="sel_pedido_operacao_tipo" name="sel_pedido_operacao_tipo" >
							<option <?=($nfe_operacao_tipo == 0 )? "selected ='selected'" : '' ?> value="0">Entrada</option>
							<option <?=($nfe_operacao_tipo == 1 )? "selected ='selected'" : '' ?> value="1">Sa&iacute;da</option>
                 		</select>
                    </li>
				</ul>
                <ul>
                    <li>
                        <label for="txt_cliente_codigo">Cliente</label>
                        <input type="text" style="width:70px; text-align:right" id="txt_cliente_codigo" name="txt_cliente_codigo" value="0" />
                        <a href="cliente_busca" title="Localizar" class="modal" rel="950-380"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                    </li>
                    <li>
                        <label for="txt_cliente_nome">&nbsp;</label>
                        <input type="text" style=" width:315px" id="txt_cliente_nome" name="txt_cliente_nome" value="Consumidor" readonly="readonly" />
                        <input type="hidden" id="hid_cliente_id" name="hid_cliente_id" value="0" />
                    </li>
                    <li>
                        <label for="txt_funcionario_codigo">Funcion&aacute;rio</label>
                        <input type="text" style="width:75px;text-align:right" id="txt_funcionario_codigo" name="txt_funcionario_codigo" value="" />
                        <a href="funcionario_busca,1" title="Localizar" class="modal" rel="680-380"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                    </li>
                    <li style="width:350px">
                        <label for="txt_funcionario_nome">&nbsp;</label>
                        <input type="text" style=" width:380px;" id="txt_funcionario_nome" name="txt_funcionario_nome" value="" readonly="readonly" />
                    </li>
<?					if($_SESSION["exibir_dependente"] > 0){
?>                  	<li>
                            <label for="txt_dependente_codigo">Dependente</label>
                            <input type="text" style="width:70px; text-align:right" id="txt_dependente_codigo" name="txt_dependente_codigo" value="" />
                            <a href="dependente_busca" title="Localizar" class="modal" rel="950-380"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                        </li>
                     
                        <li>
                            <label for="txt_dependente_nome">&nbsp;</label>
                            <input type="text"	 id="txt_dependente_nome" 	name="txt_dependente_nome" value="" style=" width:315px" readonly="readonly" />
                            <input type="hidden" id="hid_dependente_id" 	name="hid_dependente_id" 	value="0" />
                        </li>
<?                  }  
?>
                    <li>
                 	   <label for="txt_cliente_pedido">N&ordm; Servi&ccedil;o</label>
                       <input type="text" style=" width:70px; text-align:right" name="txt_cliente_pedido" id="txt_cliente_pedido" value="<?=$rowPedido['fldCliente_Pedido']?>" readonly="readonly"  />
                    </li>
<?					if($_SESSION["sistema_tipo"]=="automotivo"){
?>						<li>
                            <label for="sel_veiculo">Ve&iacute;culo</label>
                            <SELECT class="sel_veiculo" style="width:200px;" id="sel_veiculo" name="sel_veiculo" >
                            </SELECT>
						</li>
                        <a class="modal" style="display:none" id="modal_veiculo" href="cliente_veiculo_cadastro_venda" rel="680-200" title=""></a>
                        <li>
                            <label for="txt_veiculo_km">KM</label>
                            <input type="text" style="width:135px" id="txt_veiculo_km" name="txt_veiculo_km" value=""/>
                        </li>
                        <li>
                            <label for="txt_veiculo_ano">Ano</label>
                            <input type="text" style="width:70px" id="txt_veiculo_ano" name="txt_veiculo_ano" value="<?=$rowPedido['fldAno']?>" readonly="readonly"/>
                        </li>
                        <li>
                            <label for="txt_veiculo_chassi">Chassi</label>
                            <input type="text" style="width:150px" id="txt_veiculo_chassi" name="txt_veiculo_chassi" value="<?=$rowPedido['fldChassi']?>" readonly="readonly"/>
                        </li>
<?					}
					if($_SESSION["sistema_tipo"]=="financeira"){
?>						<li>
                            <label for="sel_conta_bancaria">Conta Banc&aacute;ria</label>
                            <select style="width:260px" id="sel_conta_bancaria" name="sel_conta_bancaria" >
                            </select>
						</li>
                        <li>
                            <label for="txt_agencia">Ag&ecirc;ncia</label>
                            <input style="width: 60px" type="text" id="txt_agencia" name="txt_agencia" readonly="readonly" value=""/>
						</li>
                        <li>
                            <label for="txt_titular">Titular</label>
                            <input type="text" name="txt_titular" id="txt_titular" value="" readonly="readonly"  />
						</li>
<?					}
					if($_SESSION["sistema_tipo"]=="otica"){                    
?>						
                        <li>
                           <label for="sel_cliente_responsavel">Respons&aacute;vel</label>
                           <select style="width:200px" id="sel_cliente_responsavel" name="sel_cliente_responsavel" >
                           </select>
                        </li>
						<li>
						   	<label for="txt_pedido_entrega">Entrega</label>
                    		<input type="text" style=" width:70px" name="txt_pedido_entrega" id="txt_pedido_entrega" class="calendario-mask" value="<?=date('d/m/Y')?>" />
						</li>
<?					}
?>
					<li>
                 	   <label for="txt_pedido_obs">Observa&ccedil;&atilde;o</label>
                       <textarea style=" <?=($_SESSION["sistema_tipo"]=="otica")? 'width:480px;' : 'width:780px;' ?> height:80px" id="txt_pedido_obs" name="txt_pedido_obs"></textarea>
                    </li>
                </ul>
                
                <ul id="pedido_modo_aba" class="menu_modo" style="width:952px;float:left; background:#FFF">
                    <li><a href="produto">produtos</a></li>
                    <li><a href="servico">servi&ccedil;os</a></li>
                </ul>
                <div id="modo_aba_produto" style="width:960px; display:table">        
                
                    <div id="pedido_produto">
                    	<ul style="width:962px">
                            <li>
                                <label for="txt_produto_codigo">C&oacute;digo</label>
                                <input type="text" class="codigo" style="width: 80px" id="txt_produto_codigo" name="txt_produto_codigo" value="" />
                                <a href="produto_busca" title="Localizar" class="modal" rel="950-450"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                                <input type="hidden" id="hid_produto_id" 				name="hid_produto_id" 				value="" />
                                <input type="hidden" id="hid_estoque_limite" 			name="hid_estoque_limite" 			value="0" />
                                <input type="hidden" id="hid_estoque_controle" 			name="hid_estoque_controle" 		value="0" />
                                <input type="hidden" id="hid_estoque_negativo_alerta" 	name="hid_estoque_negativo_alerta" 	value="<?= fnc_sistema('alerta_estoque_negativo')?>" />
                                <input type="hidden" id="hid_item_repetido_alerta"	 	name="hid_item_repetido_alerta" 	value="<?=$_SESSION["sistema_pedido_item_repetido"]?>" />
                            </li>
                            <li>
                                <label for="txt_produto_nome">Produto</label>
                                <textarea style="width:270px; height:40px" id="txt_produto_nome" name="txt_produto_nome"></textarea>
                            </li>
                            <li>
                                <label 	for="sel_produto_fardo">Fardo</label>
                                <SELECT name="sel_produto_fardo" id="sel_produto_fardo" style="width:60px">
                                </SELECT>
                            </li>
                            <li>
                                <label 	for="sel_produto_tabela">Tabela</label>
                                <SELECT name="sel_produto_tabela" id="sel_produto_tabela" style="width:75px">
                                    <option value="null">padr&atilde;o</option>
<?  	                        	$filtro_tabela 	 = (fnc_sistema('cliente_produto_preco') == 0 ) ? 'AND fldId > 0' : '';
									$rsTabela		 = mysql_query("SELECT * FROM tblproduto_tabela WHERE fldExcluido = '0' $filtro_tabela");
									while($rowTabela = mysql_fetch_array($rsTabela)){
?>          	                  		<option value="<?=$rowTabela['fldId']?>"><?=$rowTabela['fldSigla']?></option>
<?									}
?>								</SELECT>
                            </li>
                            <li>
                                <label for="sel_produto_estoque">Estoque</label>
                                <select name="sel_produto_estoque" id="sel_produto_estoque" style="width:75px">
<? 	        	                 	$rsEstoque 			= mysql_query("SELECT * FROM tblproduto_estoque WHERE fldExcluido = '0'");
									while($rowEstoque 	= mysql_fetch_array($rsEstoque)){
?>      	                      		<option value="<?=$rowEstoque['fldId']?>"><?=$rowEstoque['fldNome']?></option>
<?									}
?>								</select>
                        	</li>
                        	<li>
                                <label for="txt_produto_valor">Valor Un.</label>
                                <input type="text" style="width:60px; text-align:right;" id="txt_produto_valor" name="txt_produto_valor" value="" />
                                <input type="hidden" name="hid_venda_decimal" id="hid_venda_decimal" value="<?=$vendaDecimal?>" />
                            </li>
                            <li>
                                <label for="txt_produto_quantidade">Qtde</label>
                                <input type="text" style="width:50px; text-align:right;" id="txt_produto_quantidade" name="txt_produto_quantidade" value="" />
                                <input type="hidden" name="hid_quantidade_decimal" id="hid_quantidade_decimal" value="<?=$qtdeDecimal?>" />
                            </li>
                            <li>
                                <label for="txt_produto_desconto">Desc (%)</label>
                                <input type="text" style="width:50px; text-align:right;" id="txt_produto_desconto" name="txt_produto_desconto" value="" />
                            </li>
                            <li>
                            	<label for="txt_produto_referencia">Referencia</label>
                                <input type="text" style="width:60px; text-align:right;" id="txt_produto_referencia" name="txt_produto_referencia" value="" />
                            </li>
                            <li>
                            	<button class="btn_sub_small" name="btn_item_inserir" id="btn_item_inserir" title="Inserir" >ok</button>
                                <a class="modal" style="display:none" id="btn_nfe" href="pedido_nfe_item" rel='980-500' title=""></a>
                                <input type="hidden" id="hid_produto_UM" name="hid_produto_UM" value="" />
                            </li>
                        </ul>
                    </div>
                
                	<div id="pedido_lista" style="width:960px">
                        <ul id="pedido_lista_cabecalho" style="width: 11080px">
                            <li style="width:20px">&nbsp;</li>
                            <li style="width:20px">&nbsp;</li>
                            <li style="width:20px" class="entregue" title="entregue"></li>
                            <li style="width:80px">C&oacute;digo</li>
                            <li style="width:300px">Produto</li>
                            <li style="width:45px">Fardo</li>
                            <li style="width:50px">Tabela</li>
                            <li style="width:80px">Valor Un.</li>
                            <li style="width:55px">Qtde</li>
                            <li style="width:60px">Estoque</li>
                            <li style="width:30px">U.M.</li>
                            <li style="width:45px">Desc(%)</li>
                            <li style="width:73px">Subtotal</li>
                            <li style="width:70px">Referencia</li>
                            
                            <li style="width:70px">NCM</li>
                            <li style="width:70px">EX TIPI</li>
                            <li style="width:70px">CFOP</li>
                            <li style="width:90px">Uni. Comercial</li>
                            <li style="width:70px">Uni. Tribut.</li>
                            <li style="width:70px">Qtd. Tribut.</li>
                            <li style="width:110px">Valor Unit. Tribut.</li>
                            <li style="width:70px">Tot. Seguro</li>
                            <li style="width:85px">Desconto</li>
                            <li style="width:70px">Frete</li>
                            <li style="width:70px">EAN</li>
                            <li style="width:70px">EAN Tribut.</li>
                            <li style="width:120px">Out. Desp. Acess&oacute;rias</li>
                            <li style="width:90px">Val. Tot. Bruto</li>
                            <li style="width:70px">Ped. Compra</li>
                            <li style="width:110px">N&ordm; Item Ped. Compra</li>
                            <li style="width:110px">Val. Tot. Bruto</li>
                            
                            <li style="width:120px">ICMS Sit. Tribut.</li>
                            <li style="width:70px">Origem</li>
                            <li style="width:150px">Aliq. Aplic&aacute;vel Calc. Cr&eacute;d.</li>
                            <li style="width:150px">Cr&eacute;d. pode ser aproveitado</li>
                            <li style="width:150px">Modalid. Determ. BC ICMS</li>
                            <li style="width:80px">BC ICMS</li>
                            <li style="width:120px">% Red. BC ICMS</li>
                            
                            <li style="width:70px">Al&iacute;q. ICMS</li>
                            <li style="width:80px">ICMS</li>
                            <li style="width:150px">% BC Opera&ccedil;. Pr&oacute;pria</li>
                            <li style="width:150px">Motivo desonera&ccedil;&atilde;o ICMS</li>
                            <li style="width:180px">Modalid. Determ. BC ICMS ST</li>
                            
                            <li style="width:120px">% Redu&ccedil;. BC ICMS ST</li>
                            <li style="width:150px">% Marg. Val. Adic. ICMS ST</li>
                            <li style="width:100px">BC do ICMS ST</li>
                            <li style="width:100px">Al&iacute;q. ICMS ST</li>
                            <li style="width:80px">ICMS ST</li>
                            <li style="width:120px">UF Devido ICMS ST</li>
                            <li style="width:150px">BC ICMS retido ant.</li>
                            <li style="width:150px">ICMS retido ant.</li>
                            <li style="width:150px">BC BC UF dest.</li>
                            <li style="width:150px">BC ICMS UF dest.</li>
                            
                            <li style="width:120px">COFINS - Sit. Tribut.</li>
                            <li style="width:110px">COFINS - Tipo C&aacute;lc.</li>
                            <li style="width:100px">COFINS - Val. BC</li>
                            <li style="width:130px">COFINS - Al&iacute;q. Percent.</li>
                            <li style="width:130px">COFINS - Al&iacute;q. Reais</li>
                            <li style="width:120px">COFINS - Qtd. Vendida</li>
                            <li style="width:100px">COFINS Val.</li>
                            
                            <li style="width:120px">COFINS ST - Tipo C&aacute;lc.</li>
                            <li style="width:120px">COFINS ST - Val. BC</li>
                            <li style="width:140px">COFINS ST - Al&iacute;q. Percent.</li>
                            <li style="width:120px">COFINS ST - Al&iacute;q. Reais</li>
                            <li style="width:140px">COFINS ST - Qtd. Vendida</li>
                            <li style="width:120px">COFINS ST Val.</li>
                            
                            <li style="width:120px">II - Val. BC</li>
                            <li style="width:150px">II - Val. Desp. Aduaneiras</li>
                            <li style="width: 70px">II - Val. IOF</li>
                            <li style="width: 70px">II Val.</li>
                            
                            <li style="width:100px">IPI - Sit. Tribut.</li>
                            <li style="width:140px">IPI - Class. Enquadramento</li>
                            <li style="width:170px">IPI - C&oacute;d. Enquadramento Leg.</li>
                            <li style="width:120px">IPI - CNPJ Produtor</li>
                            <li style="width:140px">IPI - C&oacute;d. Selo Controle</li>
                            <li style="width:140px">IPI - Qtd. Selo Controle</li>
                            <li style="width:120px">IPI - Tipo C&aacute;lc.</li>
                            <li style="width:70px">IPI - Val. BC.</li>
                            <li style="width:120px">IPI - Al&iacute;quota</li>
                            <li style="width:150px">IPI - Qtd. Tot. Unid. Padr&atilde;o</li>
                            <li style="width:120px">IPI - Val. Unid.</li>
                            <li style="width: 70px">IPI Val.</li>
                            
                            <li style="width:120px">ISSQN - Tributa&ccedil;&atilde;o</li>
                            <li style="width:120px">ISSQN - Val. BC</li>
                            <li style="width:120px">ISSQN - Al&iacute;quota</li>
                            <li style="width:140px">ISSQN - List. Servi&ccedil;o</li>
                            <li style="width:70px">ISSQN - UF</li>
                            <li style="width:140px">ISSQN - Mun. Ocorr&ecirc;ncia</li>
                            <li style="width: 80px">ISSQN Val.</li>
                            
                            <li style="width:100px">PIS - Sit. Tribut.</li>
                            <li style="width:100px">PIS - Tipo Calc.</li>
                            <li style="width:100px">PIS - Val. BS Calc.</li>
                            <li style="width:110px">PIS - Al&iacute;q. Percent.</li>
                            <li style="width:100px">PIS - Al&iacute;q. Reais.</li>
                            <li style="width:100px">PIS - Qtd. Vendida</li>
                            <li style="width: 60px">PIS Val.</li>
                            
                            <li style="width:120px">PIS ST - Tipo Calc.</li>
                            <li style="width:100px">PIS ST - Val. BC</li>
                            <li style="width:120px">PIS ST - Al&iacute;q. Percent.</li>
                            <li style="width:120px">PIS ST - Al&iacute;q. Reais.</li>
                            <li style="width:120px">PIS ST - Qtd. Vendida</li>
                            <li style="width: 70px">PIS ST Val.</li>
                            <li style="width:200px">Info. Adicionais</li>
                            <li style="width:122px">Prod. Especifico</li>
                        </ul>
                    
                   		<div id="hidden">
                            <ul id="pedido_lista_item" style="width: 11080px">
                                <li style="width:20px;">
                                    <a class="a_excluir" id="excluir_0" href="" title="Excluir item"></a>
                                </li>
                                <li style="width:20px;">
                                    <a class="edit edit_js modal" id="edit_0" href="pedido_nfe_item" name='' rel='980-500' title="Editar item"></a>
                                </li>
                                <li style="width:20px; background:#FFF">
                                    <input class="chk_entregue" style="width:20px" type="checkbox" name="chk_entregue" id="chk_entregue" title="item entregue" onclick="return false" />
                                    <input class="hid_pedido_item_id" type="hidden" id="hid_pedido_item_id" name="hid_pedido_item_id" value="0" />
                                </li>
                                <li>
                                    <input class="txt_item_codigo" type="text" style="width: 80px" id="txt_item_codigo" name="txt_item_codigo" value="" readonly="readonly"/>
                                    <input class="hid_item_produto_id" 	type="hidden" id="hid_item_produto_id" 	name="hid_item_produto_id" 	value="" />
                                    <input class="hid_item_detalhe" 	type="hidden" id="hid_item_detalhe" 	name="hid_item_detalhe" 	value="0" />
                                </li>
                                <li>
                                    <input class="txt_item_nome" type="text" style=" width:300px; text-align:left" id="txt_item_nome" name="txt_item_nome" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input class="txt_item_fardo" type="text" style="width:45px" id="txt_item_fardo" name="txt_item_fardo" value="" readonly="readonly" />
                                    <input class="hid_item_fardo" type="hidden" id="hid_item_fardo" name="hid_item_fardo" value="" />
                                </li>
                                <li>
                                    <input class="txt_item_tabela_sigla" type="text" style="width:50px" id="txt_item_tabela_sigla" name="txt_item_tabela_sigla" value="" readonly="readonly" />
                                    <input class="hid_item_tabela_preco" type="hidden" id="hid_item_tabela_preco" name="hid_item_tabela_preco" value="" />
                                </li>
                                <li>
                                    <input class="txt_item_valor" type="text" style="width:80px; text-align:right" id="txt_item_valor" name="txt_item_valor" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input class="txt_item_quantidade" type="text" style="width:55px; text-align: right" id="txt_item_quantidade" name="txt_item_quantidade" value="0,00" readonly="readonly" />
                                </li>
                                <li>
                                    <input class="txt_item_estoque_nome" type="text" style="width:60px" id="txt_item_estoque_nome" name="txt_item_estoque_nome" value="" readonly="readonly" />
                                    <input class="hid_item_estoque_id" type="hidden" id="hid_item_estoque_id" name="hid_item_estoque_id" value="1" />
                                </li>
                                <li>
                                    <input class="txt_item_UM" type="text" style="width:30px; text-align:center" id="txt_item_UM" name="txt_item_UM" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input class="txt_item_desconto" type="text" style="width:45px; text-align: right" id="txt_item_desconto" name="txt_item_desconto" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input class="txt_item_subtotal" type="text" style="width:73px; text-align: right" id="txt_item_subtotal" name="txt_item_subtotal" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input class="txt_item_referencia" type="text"		id="txt_item_referencia" 	name="txt_item_referencia" 		value="" style="width:70px; text-align:right" readonly="readonly" />
                                </li>
                                
                                
                                <!-- nfe -->
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_ncm" id="txt_nfe_pedido_item_principal_ncm_0" name="txt_nfe_pedido_item_principal_ncm" style="width: 70px" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_ex_tipi" id="txt_nfe_pedido_item_principal_ex_tipi_0" name="txt_nfe_pedido_item_principal_ex_tipi" style="width: 70px" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_cfop" id="txt_nfe_pedido_item_principal_cfop_0" name="txt_nfe_pedido_item_principal_cfop" style="width: 70px" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_unidade_comercial" id="txt_nfe_pedido_item_principal_unidade_comercial_0" name="txt_nfe_pedido_item_principal_unidade_comercial" style="width: 90px; text-align:center" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_unidade_tributavel" id="txt_nfe_pedido_item_principal_unidade_tributavel_0" name="txt_nfe_pedido_item_principal_unidade_tributavel" style="width: 70px;text-align:center" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_quantidade_tributavel" id="" name="txt_nfe_pedido_item_principal_quantidade_tributavel" style="width: 70px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_valor_unidade_tributavel" id="txt_nfe_pedido_item_principal_valor_unidade_tributavel_0" name="txt_nfe_pedido_item_principal_valor_unidade_tributavel" style="width: 110px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_total_seguro" id="txt_nfe_pedido_item_principal_total_seguro_0" name="txt_nfe_pedido_item_principal_total_seguro" style="width: 70px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_desconto" style="width:85px; text-align: right" id="txt_nfe_pedido_item_principal_desconto_0" name="txt_nfe_pedido_item_principal_desconto" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_frete" id="txt_nfe_pedido_item_principal_frete_0" name="txt_nfe_pedido_item_principal_frete" style="width: 70px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_ean" id="txt_nfe_pedido_item_principal_ean_0" name="txt_nfe_pedido_item_principal_ean" style="width: 70px" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_ean_tributavel" id="txt_nfe_pedido_item_principal_ean_tributavel_0" name="txt_nfe_pedido_item_principal_ean_tributavel" style="width: 70px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_outras_despesas_acessorias" id="txt_nfe_pedido_item_principal_outras_despesas_acessorias_0" name="txt_nfe_pedido_item_principal_outras_despesas_acessorias" style="width: 120px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_valor_total_bruto" id="txt_nfe_pedido_item_principal_valor_total_bruto_0" name="txt_nfe_pedido_item_principal_valor_total_bruto" style="width: 90px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_pedido_compra" id="txt_nfe_pedido_item_principal_pedido_compra_0" name="txt_nfe_pedido_item_principal_pedido_compra" style="width: 70px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_principal_numero_item_pedido_compra" id="txt_nfe_pedido_item_principal_numero_item_pedido_compra_0" name="txt_nfe_pedido_item_principal_numero_item_pedido_compra" style="width: 110px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <!--
                                <li>
                                    <input type="text" style="width:70px;text-align:left" class="txt_nfe_pedido_item_principal_produto_especifico" id="txt_nfe_pedido_item_principal_produto_especifico_0" name="txt_nfe_pedido_item_principal_produto_especifico"  value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_principal_produto_especifico"  id="hid_nfe_pedido_item_principal_produto_especifico" name="hid_nfe_pedido_item_principal_produto_especifico" value="" />  
                                </li>
                                -->
                                <li>
                                    <input type="checkbox" class="chk_nfe_pedido_item_principal_valor_total_bruto" id="chk_nfe_pedido_item_principal_valor_total_bruto_0" name="chk_nfe_pedido_item_principal_valor_total_bruto" style="width: 110px;text-align:left" onclick="return false" />
                                </li>
                                <!--tributos-->
                                <li>
                                    <input type="text" style="width:120px;text-align:left" class="txt_nfe_pedido_item_tributos_icms_situacao_tributaria" id="txt_nfe_pedido_item_tributos_icms_situacao_tributaria_0" name="txt_nfe_pedido_item_tributos_icms_situacao_tributaria" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_icms_situacao_tributaria" id="hid_nfe_pedido_item_tributos_icms_situacao_tributaria" name="hid_nfe_pedido_item_tributos_icms_situacao_tributaria" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_tributo_rad" id="hid_nfe_tributo_rad" name="hid_nfe_tributo_rad" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:70px;text-align:center" class="txt_nfe_pedido_item_tributos_icms_origem" id="txt_nfe_pedido_item_tributos_icms_origem_0" name="txt_nfe_pedido_item_tributos_icms_origem" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_icms_origem" id="hid_nfe_pedido_item_tributos_icms_origem" name="hid_nfe_pedido_item_tributos_icms_origem" value="" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_pCredSN" id="txt_nfe_pedido_item_tributos_icms_pCredSN_0" name="txt_nfe_pedido_item_tributos_icms_pCredSN" style="width:150px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_vCredICMSSN" id="txt_nfe_pedido_item_tributos_icms_vCredICMSSN" name="txt_nfe_pedido_item_tributos_icms_vCredICMSSN" style="width:150px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text"  style="width:150px;text-align:left" class="txt_nfe_pedido_item_tributos_icms_modBC" id="txt_nfe_pedido_item_tributos_icms_modBC_0" name="txt_nfe_pedido_item_tributos_icms_modBC" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_icms_modBC" id="hid_nfe_pedido_item_tributos_icms_modBC" name="hid_nfe_pedido_item_tributos_icms_modBC" value="" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_vBC" id="txt_nfe_pedido_item_tributos_icms_vBC_0" name="txt_nfe_pedido_item_tributos_icms_vBC" style="width:80px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_pRedBC" id="txt_nfe_pedido_item_tributos_icms_pRedBC_0" name="txt_nfe_pedido_item_tributos_icms_pRedBC" style="width:120px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_pICMS" id="txt_nfe_pedido_item_tributos_icms_pICMS_0" name="txt_nfe_pedido_item_tributos_icms_pICMS" style="width:70px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_vICMS" id="txt_nfe_pedido_item_tributos_icms_vICMS_0" name="txt_nfe_pedido_item_tributos_icms_vICMS" style="width:80px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_pBCOp" id="txt_nfe_pedido_item_tributos_icms_pBCOp_0" name="txt_nfe_pedido_item_tributos_icms_pBCOp" style="width:150px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:150px;text-align:left" class="txt_nfe_pedido_item_tributos_icms_motDesICMS" id="txt_nfe_pedido_item_tributos_icms_motDesICMS_0" name="txt_nfe_pedido_item_tributos_icms_motDesICMS" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_icms_motDesICMS"  id="hid_nfe_pedido_item_tributos_icms_motDesICMS_0" name="hid_nfe_pedido_item_tributos_icms_motDesICMS" value="" />
                                </li>
                                <li>
                                    <input type="text" style="width:180px;text-align:left" class="txt_nfe_pedido_item_tributos_icms_st_modBCST" id="txt_nfe_pedido_item_tributos_icms_st_modBCST_0" name="txt_nfe_pedido_item_tributos_icms_st_modBCST" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_icms_st_modBCST" id="hid_nfe_pedido_item_tributos_icms_st_modBCST_0" name="hid_nfe_pedido_item_tributos_icms_st_modBCST" value="" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_pRedBCST" id="txt_nfe_pedido_item_tributos_icms_st_pRedBCST_0" name="txt_nfe_pedido_item_tributos_icms_st_pRedBCST" style="width:120px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_pMVAST" id="txt_nfe_pedido_item_tributos_icms_st_pMVAST_0" name="txt_nfe_pedido_item_tributos_icms_st_pMVAST" style="width:150px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_vBCST" id="txt_nfe_pedido_item_tributos_icms_st_vBCST_0" name="txt_nfe_pedido_item_tributos_icms_st_vBCST" style="width:100px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_pICMSST" id="txt_nfe_pedido_item_tributos_icms_st_pICMSST_0" name="txt_nfe_pedido_item_tributos_icms_st_pICMSST" style="width:100px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_vICMSST" id="txt_nfe_pedido_item_tributos_icms_st_vICMSST_0" name="txt_nfe_pedido_item_tributos_icms_st_vICMSST" style="width:80px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:120px;text-align:left" class="txt_nfe_pedido_item_tributos_icms_st_UFST" id="txt_nfe_pedido_item_tributos_icms_st_UFST_0" name="txt_nfe_pedido_item_tributos_icms_st_UFST" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_icms_st_UFST" id="hid_nfe_pedido_item_tributos_icms_st_UFST_0" name="hid_nfe_pedido_item_tributos_icms_st_UFST" value="" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_vBCSTRet" id="txt_nfe_pedido_item_tributos_icms_st_vBCSTRet_0" name="txt_nfe_pedido_item_tributos_icms_st_vBCSTRet" style="width:150px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_vICMSSTRet" id="txt_nfe_pedido_item_tributos_icms_st_vICMSSTRet_0" name="txt_nfe_pedido_item_tributos_icms_st_vICMSSTRet" style="width:150px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_vBCSTDest" id="txt_nfe_pedido_item_tributos_icms_st_vBCSTDest_0" name="txt_nfe_pedido_item_tributos_icms_st_vBCSTDest" style="width:150px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_icms_st_vICMSSTDest" id="txt_nfe_pedido_item_tributos_icms_st_vICMSSTDest_0" name="txt_nfe_pedido_item_tributos_icms_st_vICMSSTDest" style="width:150px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <!--tributos cofins-->
                                <li>
                                    <input type="text" style="width:120px;text-align:left"  class="txt_nfe_pedido_item_tributos_cofins_situacao_tributaria" id="txt_nfe_pedido_item_tributos_cofins_situacao_tributaria_0" name="txt_nfe_pedido_item_tributos_cofins_situacao_tributaria" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_cofins_situacao_tributaria" id="hid_nfe_pedido_item_tributos_cofins_situacao_tributaria" name="hid_nfe_pedido_item_tributos_cofins_situacao_tributaria" value="" />
                                </li>
                                <li>
                                    <input type="text" style="width:110px;text-align:center" class="txt_nfe_pedido_item_tributos_cofins_calculo_tipo" id="txt_nfe_pedido_item_tributos_cofins_calculo_tipo_0" name="txt_nfe_pedido_item_tributos_cofins_calculo_tipo" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_cofins_calculo_tipo" id="hid_nfe_pedido_item_tributos_cofins_calculo_tipo" name="hid_nfe_pedido_item_tributos_cofins_calculo_tipo" value="" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_cofins_valor_base_calculo" id="txt_nfe_pedido_item_tributos_cofins_valor_base_calculo_0" name="txt_nfe_pedido_item_tributos_cofins_valor_base_calculo" style="width:100px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_cofins_aliquota_percentual" id="txt_nfe_pedido_item_tributos_cofins_aliquota_percentual_0" name="txt_nfe_pedido_item_tributos_cofins_aliquota_percentual" style="width:130px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_cofins_aliquota_reais" id="txt_nfe_pedido_item_tributos_cofins_aliquota_reais_0" name="txt_nfe_pedido_item_tributos_cofins_aliquota_reais" style="width:130px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>	
                                    <input type="text" class="txt_nfe_pedido_item_tributos_cofins_quantidade_vendida" id="txt_nfe_pedido_item_tributos_cofins_quantidade_vendida_0" name="txt_nfe_pedido_item_tributos_cofins_quantidade_vendida" style="width:120px;text-align:right" value="" readonly="readonly" /> 
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_cofins_valor" id="txt_nfe_pedido_item_tributos_cofins_valor_0" name="txt_nfe_pedido_item_tributos_cofins_valor" style="width:100px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:120px;text-align:center" class="txt_nfe_pedido_item_tributos_cofins_st_calculo_tipo" id="txt_nfe_pedido_item_tributos_cofins_st_calculo_tipo_0" name="txt_nfe_pedido_item_tributos_cofins_st_calculo_tipo" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_cofins_st_calculo_tipo" id="hid_nfe_pedido_item_tributos_cofins_st_calculo_tipo" name="hid_nfe_pedido_item_tributos_cofins_st_calculo_tipo" value="" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_cofins_st_valor_base_calculo" id="txt_nfe_pedido_item_tributos_cofins_st_valor_base_calculo_0" name="txt_nfe_pedido_item_tributos_cofins_st_valor_base_calculo" style="width:120px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_cofins_st_aliquota_percentual" id="txt_nfe_pedido_item_tributos_cofins_st_aliquota_percentual_0" name="txt_nfe_pedido_item_tributos_cofins_st_aliquota_percentual" style="width:140px;" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_cofins_st_aliquota_reais" id="txt_nfe_pedido_item_tributos_cofins_st_aliquota_reais_0" name="txt_nfe_pedido_item_tributos_cofins_st_aliquota_reais" style="width:120px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>	
                                    <input type="text" class="txt_nfe_pedido_item_tributos_cofins_st_quantidade_vendida" id="txt_nfe_pedido_item_tributos_cofins_st_quantidade_vendida_0" name="txt_nfe_pedido_item_tributos_cofins_st_quantidade_vendida" style="width:140px;text-align:right" value="" readonly="readonly" /> 
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_cofins_st_valor" id="txt_nfe_pedido_item_tributos_cofins_st_valor_0" name="txt_nfe_pedido_item_tributos_cofins_st_valor" style="width:120px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <!--tributos ii-->
                                <li>
                                    <input type="text" style="width:120px;text-align:right" class="txt_nfe_pedido_item_tributos_ii_valor_base_calculo" id="txt_nfe_pedido_item_tributos_ii_valor_base_calculo_0" name="txt_nfe_pedido_item_tributos_ii_valor_base_calculo" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:150px;text-align:right" class="txt_nfe_pedido_item_tributos_ii_valor_aduaneiras" id="txt_nfe_pedido_item_tributos_ii_valor_aduaneiras_0" name="txt_nfe_pedido_item_tributos_ii_valor_aduaneiras" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:70px;text-align:right" class="txt_nfe_pedido_item_tributos_ii_valor_iof" id="txt_nfe_pedido_item_tributos_ii_valor_iof_0" name="txt_nfe_pedido_item_tributos_ii_valor_iof" value="" readonly="readonly" />
                                </li>
                                <li>	
                                    <input type="text" class="txt_nfe_pedido_item_tributos_ii_valor" id="txt_nfe_pedido_item_tributos_ii_valor_0" name="txt_nfe_pedido_item_tributos_ii_valor" style="width:70px;text-align:right" value="" readonly="readonly" /> 
                                </li>
                                <!--tributos ipi-->
                                <li>
                                    <input type="text" style="width:100px;text-align:left" class="txt_nfe_pedido_item_tributos_ipi_situacao_tributaria" id="txt_nfe_pedido_item_tributos_ipi_situacao_tributaria_0" name="txt_nfe_pedido_item_tributos_ipi_situacao_tributaria" value="" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_ipi_situacao_tributaria" id="hid_nfe_pedido_item_tributos_ipi_situacao_tributaria" name="hid_nfe_pedido_item_tributos_ipi_situacao_tributaria" value="" />
                                </li>
                                <li>	
                                    <input type="text" class="txt_nfe_pedido_item_tributos_ipi_enquadramento_classe" id="txt_nfe_pedido_item_tributos_ipi_enquadramento_classe_0" name="txt_nfe_pedido_item_tributos_ipi_enquadramento_classe" style="width:140px" value="" readonly="readonly" /> 
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_ipi_enquadramento_codigo" id="txt_nfe_pedido_item_tributos_ipi_enquadramento_codigo_0" name="txt_nfe_pedido_item_tributos_ipi_enquadramento_codigo" style="width:170px" value="" readonly="readonly"/>
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_ipi_produtor_cnpj" id="txt_nfe_pedido_item_tributos_ipi_produtor_cnpj_0" name="txt_nfe_pedido_item_tributos_ipi_produtor_cnpj" style="width:120px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_ipi_selo_controle_codigo" id="txt_nfe_pedido_item_tributos_ipi_selo_controle_codigo_0" name="txt_nfe_pedido_item_tributos_ipi_selo_controle_codigo" style="width:140px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_ipi_selo_controle_quantidade" id="txt_nfe_pedido_item_tributos_ipi_selo_controle_quantidade_0" name="txt_nfe_pedido_item_tributos_ipi_selo_controle_quantidade" style="width:140px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:120px;text-align:center" class="txt_nfe_pedido_item_tributos_ipi_calculo_tipo" id="txt_nfe_pedido_item_tributos_ipi_calculo_tipo_0" name="txt_nfe_pedido_item_tributos_ipi_calculo_tipo" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_ipi_calculo_tipo" id="hid_nfe_pedido_item_tributos_ipi_calculo_tipo" name="hid_nfe_pedido_item_tributos_ipi_calculo_tipo" value="" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_ipi_valor_base_calculo" id="txt_nfe_pedido_item_tributos_ipi_valor_base_calculo_0" name="txt_nfe_pedido_item_tributos_ipi_valor_base_calculo" style="width:70px;text-align:right" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_ipi_aliquota" id="txt_nfe_pedido_item_tributos_ipi_aliquota_0" name="txt_nfe_pedido_item_tributos_ipi_aliquota" style="width:120px;text-align:right" value="" readonly="readonly"/>
                                </li>
                                <li>
                                    <input type="text" class="txt_nfe_pedido_item_tributos_ipi_unidade_padrao_quantidade_total" id="txt_nfe_pedido_item_tributos_ipi_unidade_padrao_quantidade_total_0" name="txt_nfe_pedido_item_tributos_ipi_unidade_padrao_quantidade_total" style="width:150px;text-align:right"  readonly="readonly"/>
                                </li>
                                <li>
                                    <input type="text" style="width:120px;text-align:right" class="txt_nfe_pedido_item_tributos_ipi_unidade_valor" id="txt_nfe_pedido_item_tributos_ipi_unidade_valor_0" name="txt_nfe_pedido_item_tributos_ipi_unidade_valor" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:70px;text-align:right" class="txt_nfe_pedido_item_tributos_ipi_valor" id="txt_nfe_pedido_item_tributos_ipi_valor_0" name="txt_nfe_pedido_item_tributos_ipi_valor" value="" readonly="readonly" />
                                </li>
                                <!--tributos issqn-->
                                <li>
                                    <input type="text" style="width:120px;text-align:left" class="txt_nfe_pedido_item_tributos_issqn_tributacao" id="txt_nfe_pedido_item_tributos_issqn_tributacao_0" name="txt_nfe_pedido_item_tributos_issqn_tributacao" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_issqn_tributacao"  id="hid_nfe_pedido_item_tributos_issqn_tributacao" name="hid_nfe_pedido_item_tributos_issqn_tributacao" value="" />
                                </li>
                                <li>
                                    <input type="text" style="width:120px;text-align:right" class="txt_nfe_pedido_item_tributos_issqn_valor_base_calculo" id="txt_nfe_pedido_item_tributos_issqn_valor_base_calculo_0" name="txt_nfe_pedido_item_tributos_issqn_valor_base_calculo" value="" readonly="readonly" />
                                </li>
                                
                                <li>
                                    <input type="text" style="width:120px;text-align:right" class="txt_nfe_pedido_item_tributos_issqn_aliquota" id="txt_nfe_pedido_item_tributos_issqn_aliquota_0" name="txt_nfe_pedido_item_tributos_issqn_aliquota" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:140px;text-align:left" class="txt_nfe_pedido_item_tributos_issqn_servico_lista" id="txt_nfe_pedido_item_tributos_issqn_servico_lista_0" name="txt_nfe_pedido_item_tributos_issqn_servico_lista" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_issqn_servico_lista" id="hid_nfe_pedido_item_tributos_issqn_servico_lista" name="hid_nfe_pedido_item_tributos_issqn_servico_lista" value="" />
                                </li>
                                <li>
                                    <input type="text"  style="width:70px;text-align:left" class="txt_nfe_pedido_item_tributos_issqn_uf" id="txt_nfe_pedido_item_tributos_issqn_uf_0" name="txt_nfe_pedido_item_tributos_issqn_uf" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_issqn_uf" id="hid_nfe_pedido_item_tributos_issqn_uf" name="hid_nfe_pedido_item_tributos_issqn_uf" value="" />
                                </li>
                                <li>
                                    <input type="text" style="width:140px;text-align:left" class="txt_nfe_pedido_item_tributos_issqn_municipio" id="txt_nfe_pedido_item_tributos_issqn_municipio_0" name="txt_nfe_pedido_item_tributos_issqn_municipio" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_issqn_municipio" id="hid_nfe_pedido_item_tributos_issqn_municipio" name="hid_nfe_pedido_item_tributos_issqn_municipio" value="" />
                                </li>
                                <li>
                                    <input type="text" style="width:80px;text-align:right" class="txt_nfe_pedido_item_tributos_issqn_valor" id="txt_nfe_pedido_item_tributos_issqn_valor_0" name="txt_nfe_pedido_item_tributos_issqn_valor" value="" readonly="readonly" />
                                </li>
                                <!--tributos pis-->
                                <li>
                                    <input type="text" style="width:100px;text-align:left" class="txt_nfe_pedido_item_tributos_pis_situacao_tributaria" id="txt_nfe_pedido_item_tributos_pis_situacao_tributaria_0" name="txt_nfe_pedido_item_tributos_pis_situacao_tributaria" value="" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_pis_situacao_tributaria" id="hid_nfe_pedido_item_tributos_pis_situacao_tributaria" name="hid_nfe_pedido_item_tributos_pis_situacao_tributaria" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:100px;text-align:left" class="txt_nfe_pedido_item_tributos_pis_calculo_tipo" id="txt_nfe_pedido_item_tributos_pis_calculo_tipo_0" name="txt_nfe_pedido_item_tributos_pis_calculo_tipo" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_pis_calculo_tipo" id="hid_nfe_pedido_item_tributos_pis_calculo_tipo" name="hid_nfe_pedido_item_tributos_pis_calculo_tipo" value="" />         
                                </li>
                                <li>
                                    <input type="text" style="width:100px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_valor_base_calculo" id="txt_nfe_pedido_item_tributos_pis_valor_base_calculo_0" name="txt_nfe_pedido_item_tributos_pis_valor_base_calculo" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:110px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_aliquota_percentual" id="txt_nfe_pedido_item_tributos_pis_aliquota_percentual_0" name="txt_nfe_pedido_item_tributos_pis_aliquota_percentual" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:100px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_aliquota_reais" id="txt_nfe_pedido_item_tributos_pis_aliquota_reais_0" name="txt_nfe_pedido_item_tributos_pis_aliquota_reais" value="" readonly="readonly" />
                                </li>
                                <li>	
                                    <input type="text" style="width:100px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_quantidade_vendida" id="txt_nfe_pedido_item_tributos_pis_quantidade_vendida_0" name="txt_nfe_pedido_item_tributos_pis_quantidade_vendida" value="" readonly="readonly" /> 
                                </li>
                                <li>
                                    <input type="text" style="width:60px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_valor" id="txt_nfe_pedido_item_tributos_pis_valor_0" name="txt_nfe_pedido_item_tributos_pis_valor" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:120px;text-align:center" class="txt_nfe_pedido_item_tributos_pis_st_calculo_tipo" id="txt_nfe_pedido_item_tributos_pis_st_calculo_tipo_0" name="txt_nfe_pedido_item_tributos_pis_st_calculo_tipo" value="" readonly="readonly" />
                                    <input type="hidden" class="hid_nfe_pedido_item_tributos_pis_st_calculo_tipo" id="hid_nfe_pedido_item_tributos_pis_st_calculo_tipo" name="hid_nfe_pedido_item_tributos_pis_st_calculo_tipo" value="" />  
                                </li>
                                <li>
                                    <input type="text" style="width:100px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_st_valor_base_calculo" id="txt_nfe_pedido_item_tributos_pis_st_valor_base_calculo_0" name="txt_nfe_pedido_item_tributos_pis_st_valor_base_calculo" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:120px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_st_aliquota_percentual" id="txt_nfe_pedido_item_tributos_pis_st_aliquota_percentual_0" name="txt_nfe_pedido_item_tributos_pis_st_aliquota_percentual" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:120px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_st_aliquota_reais" id="txt_nfe_pedido_item_tributos_pis_st_aliquota_reais_0" name="txt_nfe_pedido_item_tributos_pis_st_aliquota_reais" value="" readonly="readonly" />
                                </li>
                                <li>	
                                    <input type="text" style="width:120px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_st_quantidade_vendida" id="txt_nfe_pedido_item_tributos_pis_st_quantidade_vendida_0" name="txt_nfe_pedido_item_tributos_pis_st_quantidade_vendida" value="" readonly="readonly" /> 
                                </li>
                                <li>
                                    <input type="text" style="width:70px;text-align:right" class="txt_nfe_pedido_item_tributos_pis_st_valor" id="txt_nfe_pedido_item_tributos_pis_st_valor_0" name="txt_nfe_pedido_item_tributos_pis_st_valor" value="" readonly="readonly" />
                                </li>
                                <li>
                                    <input type="text" style="width:200px;text-align:left" class="txt_nfe_item_pedido_informacoes_adicionais" id="txt_nfe_item_pedido_informacoes_adicionais_0" name="txt_nfe_item_pedido_informacoes_adicionais" value="" readonly="readonly" />
                                </li>
                                <!--- PRODUTO ESPECIFICO -->
                                <li>
                                    <input type="text" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico" id="txt_nfe_item_produto_especifico_0" name="txt_nfe_item_produto_especifico" value="" readonly="readonly" />
									<input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_id" id="txt_nfe_item_produto_especifico_id_0" name="txt_nfe_item_produto_especifico_id" value="" readonly="readonly" />
                                    <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_combustivel_anp" id="txt_nfe_item_produto_especifico_combustivel_anp_0" name="txt_nfe_item_produto_especifico_combustivel_anp" value="" readonly="readonly" />
                                    <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_combustivel_if" id="txt_nfe_item_produto_especifico_combustivel_if_0" name="txt_nfe_item_produto_especifico_combustivel_if" value="" readonly="readonly" />
                                    <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_combustivel_uf" id="txt_nfe_item_produto_especifico_combustivel_uf_0" name="txt_nfe_item_produto_especifico_combustivel_uf" value="" readonly="readonly" />
                                    <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_combustivel_qtd_faturada" id="txt_nfe_item_produto_especifico_combustivel_qtd_faturada_0" name="txt_nfe_item_produto_especifico_combustivel_qtd_faturada" value="" readonly="readonly" />
                                    <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_combustivel_cide_bc" id="txt_nfe_item_produto_especifico_combustivel_cide_bc_0" name="txt_nfe_item_produto_especifico_combustivel_cide_bc" value="" readonly="readonly" />
                                    <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_combustivel_cide_aliquota" id="txt_nfe_item_produto_especifico_combustivel_cide_aliquota_0" name="txt_nfe_item_produto_especifico_combustivel_cide_aliquota" value="" readonly="readonly" />
                                    <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_combustivel_cide_valor" id="txt_nfe_item_produto_especifico_combustivel_cide_valor_0" name="txt_nfe_item_produto_especifico_combustivel_cide_valor" value="" readonly="readonly" />
                                    <!-- TRIBUTO -->
                                    <input type="hidden" style="width:122px;text-align:left" class="txt_nfe_item_produto_especifico_total_tributos" id="txt_nfe_item_produto_especifico_total_tributos_0" name="txt_nfe_item_produto_especifico_total_tributos" value="" readonly="readonly" />
                                </li>
                                <!--- PRODUTO ESPECIFICO -->
                            </ul> 
                    	</div>
                	</div>
                    <div>
                        <input class="hid_controle" type="hidden" name="hid_controle" id="hid_controle" value="0" />
                    </div>
                  
<?					if($_SESSION["sistema_tipo"] == "otica"){
?>
                        <!-- In�cio da �tica -->
                        <div id="pedido_otica_opcoes">
                            <div class="pedido_otica_opcoes_detalhe">
                                <span>LONGE</span>
                                <ul class="pedido_otica_opcoes_dados" style="margin-left: 25px">
                                    <li><input style="width:235px;background:#F5F4D8" type="text" name="txt_pedido_otica_longe_armacao" readonly="readonly" value="Arma&ccedil;&atilde;o" /></li>
                                    <li><input type="hidden" name="hid_pedido_otica_longe_armacao" value="" /></li>
                                    <li><input style="width:235px;background:#D6F2F8" type="text" name="txt_pedido_otica_longe_material" readonly="readonly" value="Material" /></li>
                                    <li><input type="hidden" name="hid_pedido_otica_longe_material" value="" /></li>
                                </ul>
                                <ul class="pedido_otica_opcoes_cabecalho">
                                    <li style="width: 57px; text-align: right;">Esf&eacute;rico</li>
                                    <li style="width: 57px; text-align: center;">Cil&iacute;ndrico</li>
                                    <li style="width: 57px; text-align: center;">Eixo</li>
                                    <li style="width: 57px; text-align: center;">DP</li>
                                </ul>
                                <ul class="pedido_otica_opcoes_dados">
                                    <li style="width: 26px;">O.D</li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_longe_od_esferico" 	style="width: 54px; height: 19px; text-align: right;" value="" /></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_longe_od_cilindrico" style="width: 54px; height: 19px; text-align: right;" value="" /></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_longe_od_eixo" 		style="width: 54px; height: 19px; text-align: right;" value="" /></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_longe_od_dp" 		style="width: 54px; height: 19px; text-align: right;" value="" /></li>
                                </ul>
                                <ul class="pedido_otica_opcoes_dados">
                                    <li style="width: 26px;">O.E</li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_longe_oe_esferico"	style="width: 54px; height: 19px; text-align: right;" value="" /></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_longe_oe_cilindrico" style="width: 54px; height: 19px; text-align: right;" value="" /></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_longe_oe_eixo"		style="width: 54px; height: 19px; text-align: right;" value="" /></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_longe_oe_dp"			style="width: 54px; height: 19px; text-align: right;" value="" /></li>
                                </ul>
                            </div>
                            <!-- �tica perto -->
                            <a class="modal" style="display:none" id="modal_otica" href="pedido_otica_select" rel="345-130" title=""></a>
                            <div class="pedido_otica_opcoes_detalhe">
                                <span>PERTO</span>
                                <ul class="pedido_otica_opcoes_dados" style="margin-left: 25px">
                                    <li><input style="width:235px;background:#F5F4D8" type="text" name="txt_pedido_otica_perto_armacao" readonly="readonly" value="Arma&ccedil;&atilde;o" /></li>
                                    <li><input type="hidden" name="hid_pedido_otica_perto_armacao" value="" /></li>
                                    <li><input style="width:235px;background:#D6F2F8" type="text" name="txt_pedido_otica_perto_material" readonly="readonly" value="Material" /></li>
                                    <li><input type="hidden" name="hid_pedido_otica_perto_material" value="" /></li>
                                </ul>
                                <ul class="pedido_otica_opcoes_cabecalho">
                                    <li style="width: 57px; text-align: right;">Esf&eacute;rico</li>
                                    <li style="width: 57px; text-align: left;">Cil&iacute;ndrico</li>
                                    <li style="width: 57px; text-align: left;">Eixo</li>
                                    <li style="width: 57px; text-align: left;">DP</li>
                                </ul>
                                <ul class="pedido_otica_opcoes_dados">
                                    <li style="width: 26px;">O.D</li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_perto_od_esferico" 	style="width: 54px; height: 19px; text-align: right" value="" /></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_perto_od_cilindrico" style="width: 54px; height: 19px; text-align: right" value="" /></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_perto_od_eixo" 		style="width: 54px; height: 19px; text-align: right" value="" /></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_perto_od_dp" 		style="width: 54px; height: 19px; text-align: right" value="" /></li>
                                </ul>
                                <ul class="pedido_otica_opcoes_dados">
                                    <li style="width: 26px;">O.E</li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_perto_oe_esferico" 	style="width: 54px; height: 19px; text-align: right" value="" /></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_perto_oe_cilindrico" style="width: 54px; height: 19px; text-align: right" value="" /></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_perto_oe_eixo" 		style="width: 54px; height: 19px; text-align: right" value="" /></li>
                                    <li style="width: 57px;"><input type="text" name="txt_pedido_otica_perto_oe_dp" 		style="width: 54px; height: 19px; text-align: right" value="" /></li>
                                </ul>
                            </div>
                            <div class="pedido_otica_opcoes_detalhe">
                                <span>BIFOCAL / MULTIFOCAL</span>
                                <ul class="pedido_otica_opcoes_dados" style="margin-left: 25px">
                                    <li>
                                        <select style="width:180px;width:235px;background:#F5F4D8"" id="sel_otica_marca" name="sel_otica_marca" >
                                            <option value="0">Selecionar</option>
<?											$rsMarca = mysql_query("SELECT * FROM tblmarca WHERE fldDisabled = '0' ORDER BY fldNome");
                                            while($rowMarca = mysql_fetch_array($rsMarca)){                            
?>	                   	       				<option value="<?=$rowMarca['fldId']?>" ><?=$rowMarca['fldNome']?></option>
<?											}
?>	                 					</select>
                                    </li>
                                    <li>
                                        <select style="width:180px;width:235px;background:#D6F2F8"" id="sel_otica_lente" name="sel_otica_lente" >
                                            <option value="0">Selecionar</option>
<?											$rsLente = mysql_query("SELECT * FROM tblproduto_otica_lente ORDER BY fldLente");
                                            while($rowLente = mysql_fetch_array($rsLente)){                            
?>		                   	       				<option value="<?=$rowLente['fldId']?>"><?=$rowLente['fldLente']?></option>
<?											}
?>	                 					</select>
                                    </li>
                                </ul>
                                <ul class="pedido_otica_opcoes_cabecalho">
                                    <li style="width: 66px; text-align: right;">Altura</li>
                                    <li style="width: 60px; text-align: center;">DNP</li>
                                    <li style="width: 60px; text-align: center;">Adi&ccedil;&atilde;o</li>
                                </ul>
                                <ul class="pedido_otica_opcoes_dados">
                                    <li style="width: 26px;">O.D</li>
                                    <li style="width: 60px;"><input type="text" name="txt_pedido_otica_bifocal_od_altura" 	style="width: 56px; height: 19px; text-align: right;" 	value="<?=$otica_retorno['bifocal']['OD']['altura']?>"></li>
                                    <li style="width: 60px;"><input type="text" name="txt_pedido_otica_bifocal_od_dnp" 		style="width: 56px; height: 19px; text-align: right;" 	value="<?=$otica_retorno['bifocal']['OD']['DNP']?>"></li>
                                    <li style="width: 60px;"><input type="text" name="txt_pedido_otica_bifocal_od_adicao" 	style="width: 56px; height: 19px; text-align: right;" 	value="<?=$otica_retorno['bifocal']['OD']['adicao']?>"></li>
                                </ul>
                                <ul class="pedido_otica_opcoes_dados">
                                    <li style="width: 26px;">O.E</li>
                                    <li style="width: 60px;"><input type="text" name="txt_pedido_otica_bifocal_oe_altura" 	style="width: 56px; height: 19px; text-align: right;" 	value="<?=$otica_retorno['bifocal']['OE']['altura']?>"></li>
                                    <li style="width: 60px;"><input type="text" name="txt_pedido_otica_bifocal_oe_dnp" 		style="width: 56px; height: 19px; text-align: right;" 	value="<?=$otica_retorno['bifocal']['OE']['DNP']?>"></li>
                                    <li style="width: 60px;"><input type="text" name="txt_pedido_otica_bifocal_oe_adicao" 	style="width: 56px; height: 19px; text-align: right;" 	value="<?=$otica_retorno['bifocal']['OE']['adicao']?>"></li>
                                </ul>
                            </div>
                            <ul class="pedido_otica_opcoes_dados" style="margin:0">
                                <li>
                                    <label for="txt_pedido_otica_medico">M&eacute;dico</label>
                                    <input type="text" name="txt_pedido_otica_medico" style="width: 625px" value="" />
                                </li>
                                <li>
                                    <label for="txt_pedido_otica_medico_crm">CRM</label>
                                    <input type="text" name="txt_pedido_otica_medico_crm" style="width: 300px" value="" />
                                </li>
                            </ul>
                    	</div>
                    	<!-- Final da �tica -->
<?					}
					$perfil_pagamento_id		= $rowPerfil['fldPagamento_Id'];
					$perfil_frete_id 			= $rowPerfil['fldFrete_Id'];
					$perfil_transportador_id 	= $rowPerfil['fldTransportador_Id'];
					$perfil_info_fisco		 	= $rowPerfil['fldInfo_Fisco'];
					$perfil_info_contribuinte 	= $rowPerfil['fldInfo_Contribuinte'];
?>
				</div> <!-- div de produtos-->
                
                <div id="modo_aba_servico" style="width:962px;height:274px;display:table;background:#D3D3D3">
                	<input type="hidden" name="hid_servico_valor" id="hid_servico_valor" value="<?=$servico_valor?>" />
                	
<?					$sqlFuncionario = "SELECT tblfuncionario.fldNome, tblfuncionario.fldId
															 FROM tblfuncionario INNER JOIN tblfuncionario_funcao 
															 ON tblfuncionario.fldFuncao2_Id = tblfuncionario_funcao.fldId
															 WHERE tblfuncionario_funcao.fldTipo = '2'";
?>					<fieldset style="width:310px;float:left;margin:7px 7px 0px 0px;border:1px solid #FFF">
                        <ul style="width:310px">
                            <li style="margin-bottom:0">
                                <label for="sel_funcionario1">Funcionario 1</label>
                                <select style="width:300px" id="sel_funcionario1" name="sel_funcionario1" class="sel_funcionario">
                                	<option value="0">selecionar</option>
<?									$rsFuncionario = mysql_query($sqlFuncionario);
									while($rowFuncionario = mysql_fetch_array($rsFuncionario)){
?>										<option value="<?=$rowFuncionario['fldId']?>"><?=$rowFuncionario['fldNome']?></option>
<?									}
?>              	        	</select>	
                            </li>
                            <li style="margin-bottom:0">
                                <label for="txa_funcionario1_servicos">Servi&ccedil;o</label>
                                <textarea style="width:295px; height:50px" id="txa_funcionario1_servico" name="txa_funcionario1_servico" readonly="readonly"></textarea>
                            </li>
                            <li style="margin-bottom:0">
                                <label for="txt_funcionario1_tempo">Tempo</label>
                                <input type="text" style="width:142px;text-align:right" id="txt_funcionario1_tempo" name="txt_funcionario1_tempo" class="txt_funcionario_tempo" readonly="readonly">
                            </li>
                            <li>
                                <label for="txt_funcionario1_valor">Valor</label>
                                <input type="text" style="width:142px;text-align:right" id="txt_funcionario1_valor" name="txt_funcionario1_valor" value="0,00" readonly="readonly">
                            </li>
                        </ul>
                    </fieldset>
                    <fieldset style="width:310px;float:left;margin:7px 7px 0px 0px;border:1px solid #FFF">
                    	<ul style="width:310px">
                    		<li style="margin-bottom:0">
                            	<label for="sel_funcionario2">Funcionario 2</label>
                            	<select style="width:300px" id="sel_funcionario2" name="sel_funcionario2" class="sel_funcionario">
                                	<option value="0">selecionar</option>
<?									$rsFuncionario = mysql_query($sqlFuncionario);                        	
									while($rowFuncionario = mysql_fetch_array($rsFuncionario)){
?>										<option value="<?=$rowFuncionario['fldId']?>"><?=$rowFuncionario['fldNome']?></option>
<?									}
?>              	    	    </select>	
							</li>
                            <li style="margin-bottom:0">
                                <label for="txa_funcionario2_servico">Servi&ccedil;o</label>
                                <textarea style="width:295px; height:50px" id="txa_funcionario2_servico" name="txa_funcionario2_servico" readonly="readonly"></textarea>
                            </li>
                            <li>
                                <label for="txt_funcionario2_tempo">Tempo</label>
                                <input type="text" style="width:142px;text-align:right" id="txt_funcionario2_tempo" name="txt_funcionario2_tempo" class="txt_funcionario_tempo" readonly="readonly">
                            </li>
                            <li style="margin-bottom:0">
                                <label for="txt_funcionario2_valor">Valor</label>
                                <input type="text" style="width:142px;text-align:right" id="txt_funcionario2_valor" name="txt_funcionario2_valor" value="0,00" readonly="readonly">
                            </li>
                        </ul>
                    </fieldset>
                    <fieldset style="width:310px;float:left; margin-top:7px;border:1px solid #FFF">
                    	<ul style="width:310px">
                            <li style="margin-bottom:0">
                                <label for="sel_funcionario3">Funcionario 3</label>
                                <select style="width:300px" id="sel_funcionario3" name="sel_funcionario3" class="sel_funcionario">
                                	<option value="0">selecionar</option>
<?									$rsFuncionario = mysql_query($sqlFuncionario);                        	
									while($rowFuncionario = mysql_fetch_array($rsFuncionario)){
?>										<option value="<?=$rowFuncionario['fldId']?>"><?=$rowFuncionario['fldNome']?></option>
<?									}
?>              	       		</select>	
							</li>
                            <li style="margin-bottom:0">
                                <label for="txa_funcionario3_servico">Servi&ccedil;o</label>
                                <textarea style="width:295px; height:50px" id="txa_funcionario3_servico" name="txa_funcionario3_servico" readonly="readonly"></textarea>
                            </li>
                            <li style="margin-bottom:0">
                                <label for="txt_funcionario3_tempo">Tempo</label>
                                <input type="text" style="width:142px;text-align:right" id="txt_funcionario3_tempo" name="txt_funcionario3_tempo" class="txt_funcionario_tempo" readonly="readonly">
                            </li>
                            <li>
                                <label for="txt_funcionario2_valor">Valor</label>
                                <input type="text" style="width:142px;text-align:right" id="txt_funcionario3_valor" name="txt_funcionario3_valor" value="0,00" readonly="readonly">
                            </li>
                        </ul>
                    </fieldset>
                    <ul>
                    	<li>
                            <label for="txa_outros_servicos">Outros servi&ccedil;os</label>
                            <textarea style="width:935px; height:65px" id="txa_outros_servicos" name="txa_outros_servicos"></textarea>
						</li>
					</ul>
                </div>
                
                
                
                
              	<div id="dados_nfe">
                    <ul id="pedido_nfe_abas" class="menu_modo" style="width:470px; float:left">
                        <li><a href="totais">Totais</a></li>
                        <li><a href="entrega" onclick="blur();">Entrega</a></li>
                        <li><a href="transporte">Transporte</a></li>
                        <li><a href="informacoes">Inf. Adic.</a></li>
                    </ul>
					
                    <div id="pedido_nfe_totais">
                        <ul id="pedido_nfe_totais_abas" class="menu_modo" style="width:470px; float:left">
                            <li><a href="icms">ICMS</a></li>
                            <li><a href="issqn">ISSQN</a></li>
                            <li><a href="retencao">Reten&ccedil;&atilde;o de Tributos</a></li>
                        </ul>
                        <div id="totais_icms">
                            <ul class="dados_nfe">
                                <li class="dados" style="margin-bottom:20px; margin-top:10px; width: 470px">

                                    <label for="sel_pedido_nfe_pagamento_forma">Forma Pagamento</label>
                                    <select style="width:305px; float: right" id="sel_pedido_nfe_pagamento_forma" name="sel_pedido_nfe_pagamento_forma" class="sel_pedido_nfe_pagamento_forma" title="Forma Pagamento">
										<option <?=($perfil_pagamento_id == '0')? "selected = 'selected'":''?> value="0">Pagamento &agrave; vista</option>
										<option <?=($perfil_pagamento_id == '1')? "selected = 'selected'":''?> value="1">Pagamento &agrave; prazo</option>
										<option <?=($perfil_pagamento_id == '2')? "selected = 'selected'":''?> value="2">Outros</option>
                    				</select> 
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_produtos">Total dos Produtos</label>
                                    <input type="text" id="txt_pedido_nfe_total_produtos" name="txt_pedido_nfe_total_produtos" value="0,00" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_bc_icms">BC do ICMS</label>
                                    <input type="text" id="txt_pedido_nfe_total_bc_icms" name="txt_pedido_nfe_total_bc_icms" value="0,00" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_icms">Total ICMS</label>
                                    <input type="text" id="txt_pedido_nfe_total_icms" name="txt_pedido_nfe_total_icms" value="0,00" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_bc_icms_substituicao">BC ICMS Substitui&ccedil;&atilde;o</label>
                                    <input type="text" id="txt_pedido_nfe_total_bc_icms_substituicao" name="txt_pedido_nfe_total_bc_icms_substituicao" value="0,00" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_icms_substituicao">Total ICMS Substitui&ccedil;&atilde;o</label>
                                    <input type="text" id="txt_pedido_nfe_total_icms_substituicao" name="txt_pedido_nfe_total_icms_substituicao" value="0,00" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_frete">Total Frete</label>
                                    <input type="text" id="txt_pedido_nfe_total_frete" name="txt_pedido_nfe_total_frete" value="0,00" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_seguro">Total Seguro</label>
                                    <input type="text" id="txt_pedido_nfe_total_seguro" name="txt_pedido_nfe_total_seguro" value="0,00" readonly="readonly" />
                                </li>
                                <!--<li class="dados">
                                    <label for="txt_pedido_nfe_total_desconto">Total Desconto</label>
                                    <input type="text" id="txt_pedido_nfe_total_desconto" name="txt_pedido_nfe_total_desconto" value="0,00" readonly="readonly" />
                                </li>-->
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_ii">Total II</label>
                                    <input type="text" id="txt_pedido_nfe_total_ii" name="txt_pedido_nfe_total_ii" value="0,00" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_ipi">Total IPI</label>
                                    <input type="text" id="txt_pedido_nfe_total_ipi" name="txt_pedido_nfe_total_ipi" value="0,00" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_pis">Total PIS</label>
                                    <input type="text" id="txt_pedido_nfe_total_pis" name="txt_pedido_nfe_total_pis" value="0,00" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_cofins">Total Cofins</label>
                                    <input type="text" id="txt_pedido_nfe_total_cofins" name="txt_pedido_nfe_total_cofins" value="0,00" readonly="readonly" />
                                </li>
                                
                                <li class="dados">
                                    <label for="txt_pedido_nfe_outras_despesas_acessorias">Out. Despesas Acess&oacute;rias</label>
                                    <input type="text" id="txt_pedido_nfe_outras_despesas_acessorias" name="txt_pedido_nfe_outras_despesas_acessorias" value="0,00" readonly="readonly" />
                                </li>
                                <li class="dados" style="visibility: hidden;">
                                    <label for="txt_pedido_nfe_total_nfe">Total da Nota</label>
                                    <input type="text" id="txt_pedido_nfe_total_nfe" name="txt_pedido_nfe_total_nfe" value="0,00" readonly="readonly" />
                                </li>
                            </ul>
                            
                            <hr />
                            
                            <ul style="margin-top:20px">
                            	<li class="dados">
                                    <label for="txt_pedido_nfe_total_tributos">Total dos Tributos</label>
                                    <input type="text" id="txt_pedido_nfe_total_tributos" name="txt_pedido_nfe_total_tributos" value="0,00" readonly="readonly" />
                                    <img src="image/layout/loading.gif" style="float:left; margin:2px 0 0 10px; display:none" id="total_tributos_loading" />
                                </li>
                            </ul>
						</div>
                        
                        <div id="totais_issqn">
                            <ul class="dados_nfe">
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_bc_iss">Base de C&aacute;lculo do ISS</label>
                                    <input type="text" id="txt_pedido_nfe_total_bc_iss" name="txt_pedido_nfe_total_bc_iss" value="0,00" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_iss">Total do ISS</label>
                                    <input type="text" id="txt_pedido_nfe_total_iss" name="txt_pedido_nfe_total_iss" value="0,00" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_pis_servicos">PIS sobre servi&ccedil;os</label>
                                    <input type="text" id="txt_pedido_nfe_total_pis_servicos" name="txt_pedido_nfe_total_pis_servicos" value="0,00" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_cofins_servicos">COFINS sobre servi&ccedil;os</label>
                                    <input type="text" id="txt_pedido_nfe_total_cofins_servicos" name="txt_pedido_nfe_total_cofins_servicos" value="0,00" readonly="readonly" />
                                </li>
                                <li style="width: 470px" class="dados">
                                    <label for="txt_pedido_nfe_total_servicos_nao_incidencia_icms">Total dos servi&ccedil;os sob n&atilde;o incid&ecirc;ncia ou n&atilde;o tributados pelo ICMS</label>
                                    <input type="text" id="txt_pedido_nfe_total_servicos_nao_incidencia_icms" name="txt_pedido_nfe_total_servicos_nao_incidencia_icms" value="0,00" readonly="readonly" />
                                </li>
                            </ul>
						</div>     
                        <div id="totais_retencao">
                            <ul class="dados_nfe">
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_retido_pis">Valor retido de PIS</label>
                                    <input type="text" id="txt_pedido_nfe_total_retido_pis" name="txt_pedido_nfe_total_retido_pis" value="0,00" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_retido_cofins">Valor retido de COFINS</label>
                                    <input type="text" id="txt_pedido_nfe_total_retido_cofins" name="txt_pedido_nfe_total_retido_cofins" value="0,00" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_retido_csll">Valor retido de CSLL</label>
                                    <input type="text" id="txt_pedido_nfe_total_retido_csll" name="txt_pedido_nfe_total_retido_csll" value="0,00" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_bc_irrf">Base de C&aacute;lculo do IRRF</label>
                                    <input type="text" id="txt_pedido_nfe_total_bc_irrf" name="txt_pedido_nfe_total_bc_irrf" value="0,00" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_retido_irrf">Valor Retido do IRRF</label>
                                    <input type="text" id="txt_pedido_nfe_total_retido_irrf" name="txt_pedido_nfe_total_retido_irrf" value="0,00" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_bc_retencao_prev_social">BC Reten&ccedil;&atilde;o da Prev. Social</label>
                                    <input type="text" id="txt_pedido_nfe_bc_retencao_prev_social" name="txt_pedido_nfe_bc_retencao_prev_social" value="0,00" readonly="readonly" />
                                </li>
                                <li class="dados">
                                    <label for="txt_pedido_nfe_total_retencao_prev_social">Reten&ccedil;&atilde;o da Prev. Social</label>
                                    <input type="text" id="txt_pedido_nfe_total_retencao_prev_social" name="txt_pedido_nfe_total_retencao_prev_social" value="0,00" readonly="readonly" />
                                </li>
                            </ul>
						</div>                   
                                                    
                    </div>
                    <div id="pedido_nfe_entrega">
                    	<ul class="dados_nfe" style="margin:10px 0 0 3px">
							<li>
                    			<label for="txt_pedido_nfe_entrega_cpf_cnpj">CPF/CNPJ</label>
                    			<input type="text" style="width: 220px; text-align:left" id="txt_pedido_nfe_entrega_cpf_cnpj" readonly="readonly" name="txt_pedido_nfe_entrega_cpf_cnpj" value="" />
                    		</li>
                        	<li>
                                <label for="txt_pedido_nfe_entrega_endereco">Endereco</label>
                                <input type="text" style="width: 220px; text-align:left" id="txt_pedido_nfe_entrega_endereco" readonly="readonly" name="txt_pedido_nfe_entrega_endereco" value="" />
							</li>
                        	<li>
                                <label for="txt_pedido_nfe_entrega_numero">N&uacute;mero</label>
                                <input type="text" style="width: 220px; text-align:left" id="txt_pedido_nfe_entrega_numero" readonly="readonly" name="txt_pedido_nfe_entrega_numero" value="" />
							</li>
                        	<li>
                                <label for="txt_pedido_nfe_entrega_complemento">Complemento</label>
                                <input type="text" style="width: 220px; text-align:left" id="txt_pedido_nfe_entrega_complemento" readonly="readonly" name="txt_pedido_nfe_entrega_complemento" value="" />
							</li>
                        	<li>
                                <label for="txt_pedido_nfe_entrega_bairro">Bairro</label>
                                <input type="text" style="width: 220px; text-align:left" id="txt_pedido_nfe_entrega_bairro" readonly="readonly" name="txt_pedido_nfe_entrega_bairro" value="" />
							</li>
                        	<li>
                                <label for="txt_pedido_nfe_entrega_cep">Cep</label>
                                <input type="text" style="width: 220px; text-align:left" id="txt_pedido_nfe_entrega_cep" readonly="readonly" name="txt_pedido_nfe_entrega_cep" value="" />
							</li>
                            <!-- aqui teve que ficar com esses ids no codigo do municipio por causa da busca, qe ja esta sendo parametrizada com esse id -->
                            <li>
                                <label for="txt_municipio_codigo_pedido_nfe">C&oacute;d. Munic&iacute;pio</label>
                                <input type="hidden" id="hid_pedido_nfe_municipio_entrega" name="hid_pedido_nfe_municipio_entrega" value="" />
                                <input type="text" style=" width:185px; text-align:left"  id="txt_municipio_codigo_pedido_nfe" readonly="readonly" name="txt_municipio_codigo_pedido_nfe" value="" />
                                <a style="float:left;" href="municipio_busca,pedido_nfe" title="Localizar" class="modal" rel="680-380"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                            </li>
                            <li>
                                <label for="txt_pedido_nfe_municipio">&nbsp;</label>
                                <input type="text" style=" width:220px; text-align:left" id="txt_pedido_nfe_municipio" name="txt_pedido_nfe_municipio" readonly="readonly" value="" />
                            </li>
                            <li style="width:463px;">
                            	<a style="margin:10px 0px 10px 4px; float:right" id="pedido_nfe_entrega_busca" class="btn_novo modal" href="pedido_nfe_entrega_busca" rel="900-350">buscar</a>
                            	<a style="margin:10px 0; float:right" class="btn_general" id="btn_entrega_limpar" href="">limpar</a>
                            </li>
							<li style="float:right; width:300px;">
								<span style="float:right; width:275px">
									<input type="checkbox" style="margin:0px 3px 0 0; width:22px;" value="1" id="chk_local_entrega_diferente" name="chk_local_entrega_diferente">
									<label for="chk_local_entrega_diferente" style="margin-top:3px">Local de entrega diferente do destinat&aacute;rio</label>
								</span>
                    		</li>
						</ul>
                    </div>
                    <div id="pedido_nfe_transporte">
                    	<ul>
                 			<li>
                                <label for="sel_pedido_nfe_transporte_modalidade">Modalidade do frete</label>
                                <select id="sel_pedido_nfe_transporte_modalidade" name="sel_pedido_nfe_transporte_modalidade" class="sel_pedido_nfe_transporte_modalidade" title="Modalidade do frete">
<?									$rsTransporte = mysql_query("SELECT * FROM tblnfe_pedido_transporte");
                                    while($rowTransporte = mysql_fetch_array($rsTransporte)){
?>										<option <?=($perfil_frete_id == $rowTransporte['fldId'])? "selected = 'selected'":''?> value="<?= $rowTransporte['fldCodigo'] ?>"><?= $rowTransporte['fldCodigo'] ?> - <?= $rowTransporte['fldDescricao'] ?></option>
<?									}
?>                    			</select> 
                            </li>
                            <li>
                                <label for="sel_pedido_nfe_transportador">Transportador</label>
                                <select style="width: 250px" id="sel_pedido_nfe_transportador" name="sel_pedido_nfe_transportador" class="sel_pedido_nfe_transportador" title="Transportador">
                                	<option value="0">Selecionar</option>
<?									$rsTransportador = mysql_query("SELECT * FROM tbltransportador WHERE fldDisabled = 0");
                                    while($rowTransportador = mysql_fetch_array($rsTransportador)){
?>										<option <?=($perfil_transportador_id == $rowTransportador['fldId'])? "selected = 'selected'":'' ?> value="<?= $rowTransportador['fldId'] ?>"><?= $rowTransportador['fldNome'] ?></option>
<?									}
?>                    			</select> 
                            </li>  
                            <li style="width:20px; margin-bottom:0">
                            	<input style="width:20px" type="checkbox" name="chk_nfe_transporte_icms_isento" id="chk_nfe_transporte_icms_isento" value="true" />
                            </li>
                            <li style="margin-bottom:0">
                            	<label for="chk_nfe_transporte_icms_isento">Isento do ICMS</label>
                            </li>
						</ul>
                                
                        <ul id="pedido_nfe_transporte_abas" class="menu_modo" style="width:470px;float:left">
                            <li style="margin-left:0;margin-right:0"><a href="volumes">Volumes</a></li>
                            <li style="margin-left:0;margin-right:0"><a href="retencao">Reten&ccedil;&atilde;o ICMS</a></li>
                            <li style="margin-left:0;margin-right:0"><a href="veiculo">Ve&iacute;culo/Reboque/Balsa/Vag&atilde;o</a></li>
                        </ul>
                        
                        <div id="pedido_nfe_transporte_volumes" style="float:left">
                        	<input class="hid_volume_controle" type="hidden" name="hid_volume_controle" id="hid_volume_controle" value="0" />
                            <div class="dados_listar">
                                <ul class="dados_cabecalho">
                                    <li style="width:40px">qtd</li>
                                    <li style="width:80px">esp&eacute;cie</li>
                                    <li style="width:90px">marca</li>
                                    <li style="width:40px">num.</li>
                                    <li style="width:80px">peso liq.</li>
                                    <li style="width:80px; border:0">peso bruto</li>
                                    <li style="width:75px; border:0">&nbsp;</li>
                                </ul>
                                
                                <div class="dados_listar_dados" id="dados_listar_volume"> 
                                    <div id="hidden"> 
                                        <ul class="volume_listar">
                                            <li style="width:40px"><input type="text" style="width:40px;border:0;background:none" class="txt_pedido_nfe_transporte_volume_qtd" 			name="txt_pedido_nfe_transporte_volume_qtd" 		value="" readonly="readonly" /></li>
                                            <li style="width:80px"><input type="text" style="width:80px;border:0;background:none" class="txt_pedido_nfe_transporte_volume_especie" 		name="txt_pedido_nfe_transporte_volume_especie" 	value="" readonly="readonly" /></li>
                                            <li style="width:90px"><input type="text" style="width:90px;border:0;background:none" class="txt_pedido_nfe_transporte_volume_marca" 		name="txt_pedido_nfe_transporte_volume_marca" 		value="" readonly="readonly" /></li>
                                            <li style="width:40px"><input type="text" style="width:40px;border:0;background:none" class="txt_pedido_nfe_transporte_volume_numero" 		name="txt_pedido_nfe_transporte_volume_numero" 		value="" readonly="readonly" /></li>
                                            <li style="width:80px"><input type="text" style="width:80px;border:0;background:none" class="txt_pedido_nfe_transporte_volume_peso_liquido" 	name="txt_pedido_nfe_transporte_volume_peso_liquido"value="" readonly="readonly" /></li>
                                            <li style="width:80px"><input type="text" style="width:80px;border:0;background:none" class="txt_pedido_nfe_transporte_volume_peso_bruto" 	name="txt_pedido_nfe_transporte_volume_peso_bruto" 	value="" readonly="readonly" /></li>
                                            <li style="width:22px"><a style="width:22px;height:20px;border:0" name="" class="edit edit_volume modal" href="" title="editar" rel="620-450"></a></li>
                                            <li style="width:20px"><a style="height:20px;border:0" class="a_excluir" id="excluir_0" href="excluir_volume" title="Excluir item"></a></li>
                                            <li><input type="hidden" class="hid_pedido_nfe_transporte_volume_lacre" 	name="hid_pedido_nfe_transporte_volume_lacre" 	value=""/></li>
                                            
                                        </ul>
                                    </div>
								</div>
                            </div>
                            <a style="float:right; margin: 10px" class="btn_novo modal" href="pedido_nfe_transporte_volume" rel="620-450">incluir</a>
                        </div>
                        
                        <div style="float:left" id="pedido_nfe_transporte_retencao">
                        	<ul class="dados_nfe">
                                <li style="width: 146px">
                                    <label for="txt_pedido_nfe_transporte_retencao_bc">Base de C&aacute;lculo</label>
                                    <input type="text" style="width:146px" id="txt_pedido_nfe_transporte_retencao_bc" name="txt_pedido_nfe_transporte_retencao_bc" value="" />
                                </li>
                                <li style="width: 146px">
                                    <label for="txt_pedido_nfe_transporte_retencao_aliquota">Al&iacute;quota</label>
                                    <input type="text" style="width:146px" id="txt_pedido_nfe_transporte_retencao_aliquota" name="txt_pedido_nfe_transporte_retencao_aliquota" value="" />
                                </li>
                                <li style="width: 146px">
                                    <label for="txt_pedido_nfe_transporte_retencao_valor_servico">Valor do Servi&ccedil;o</label>
                                    <input type="text" style="width:146px" id="txt_pedido_nfe_transporte_retencao_valor_servico" name="txt_pedido_nfe_transporte_retencao_valor_servico" value="" />
                                </li>
                               	<li>
                                    <label for="sel_pedido_nfe_transporte_retencao_uf">UF</label>
                                    <select class="sel_uf" name="sel_pedido_nfe_transporte_retencao_uf" id="sel_pedido_nfe_transporte_retencao_uf" style="width: 225px">
                                    	<option value="0">Selecionar</option>
<?										$rsUF = mysql_query("SELECT * FROM tblibge_uf ORDER BY fldSigla");
                                    	while($rowUF = mysql_fetch_array($rsUF)){
                                   	  		echo '<option value="'.$rowUF['fldCodigo'].'">'.$rowUF['fldSigla'].'</option>';
                                    	}
?>									</select>
                                </li>
                                <li>
                                    <label for="sel_pedido_nfe_transporte_retencao_municipio">Munic&iacute;pio</label>
                                    <select class="sel_municipio" name="sel_pedido_nfe_transporte_retencao_municipio" style="width: 225px"></select>
                                </li>
                                <li style="width: 320px">
                                    <label for="sel_pedido_nfe_transporte_retencao_cfop">CFOP</label>
                                    <select name="sel_pedido_nfe_transporte_retencao_cfop" style="width: 320px">
                                    	<option value="0"></option>
<?										$rsCFOP = mysql_query('SELECT * FROM tblnfe_cfop WHERE fldModo = 2');
										while($rowCFOP = mysql_fetch_array($rsCFOP)){
?>											<option value="<?=$rowCFOP['fldId']?>"><?=$rowCFOP['fldCFOP']?> - <?=$rowCFOP['fldDescricao']?></option>
<?                              		}
?>									</select>
                                </li>
                                <li style="width: 130px">
                                    <label for="txt_pedido_nfe_transporte_retencao_icms_retido">ICMS Retido</label>
                                    <input type="text" style="width:130px" id="txt_pedido_nfe_transporte_retencao_icms_retido" name="txt_pedido_nfe_transporte_retencao_icms_retido" value="" />
                                </li>
                            </ul>
                        </div>
                        
                    	<div id="pedido_nfe_transporte_veiculo" style="float:left">
                        	<ul style="border-bottom: 1px solid;">
                            	<li style="width: 20px; margin-bottom:0">
	                                <input type="radio" style="width:20px" name="rad_transporte" value="veiculo" checked="checked" />
                                </li>
                                <li style="width:120px;margin-bottom:0">
                                	<label for="rad_veiculo">Veiculo/Reboque</label>
                                </li>
                            	<li style="width: 20px; margin-bottom:0">
	                                <input type="radio" style="width:20px" name="rad_transporte" value="balsa" />
                                </li>
                                <li style="width:120px;margin-bottom:0">
                                	<label for="rad_veiculo">Balsa</label>
                                </li>
                            	<li style="width: 20px; margin-bottom:0">
	                                <input type="radio" style="width:20px" value="vagao" name="rad_transporte" />
                                </li>
                                <li style="width:120px;margin-bottom:0">
                                	<label for="rad_veiculo">Vag&atilde;o</label>
                                </li>
                            </ul>
                            
                            <div id="rad_veiculo" style="float:left">
                                <ul class="dados_nfe" style="margin-bottom:5px">
                                    <li style="width: 146px">
                                        <label for="txt_pedido_nfe_transporte_veiculo_placa">Placa</label>
                                        <input type="text" style="width:146px; text-align:left" id="txt_pedido_nfe_transporte_veiculo_placa" name="txt_pedido_nfe_transporte_veiculo_placa" value="" />
                                    </li>
                                    <li style="width: 100px">
                                        <label for="sel_pedido_nfe_transporte_veiculo_uf">UF</label>
                                        <select name="sel_pedido_nfe_transporte_veiculo_uf" id="sel_pedido_nfe_transporte_veiculo_uf" style="width: 100px">
                                            <option value="0">Selecionar</option>
										<?	$rsUF = mysql_query("SELECT * FROM tblibge_uf ORDER BY fldSigla");
                                    		while($rowUF = mysql_fetch_array($rsUF)){
                                   	  			echo '<option value="'.$rowUF['fldCodigo'].'">'.$rowUF['fldSigla'].'</option>';
                                    		} ?>
	                                  	</select>
                                    </li>
                                    <li style="width: 146px">
                                        <label for="txt_pedido_nfe_transporte_veiculo_rntc">RNTC</label>
                                        <input type="text" style="width:146px; text-align:left" id="txt_pedido_nfe_transporte_veiculo_rntc" name="txt_pedido_nfe_transporte_veiculo_rntc" value="" />
                                    </li>
                                </ul>
                                <input class="hid_reboque_controle" type="hidden" name="hid_reboque_controle" id="hid_reboque_controle" value="0" />
                                <div class="dados_listar" style="height:80px">
                                    <ul class="dados_cabecalho">
                                        <li style="width:150px">Placa</li>
                                        <li style="width:80px">UF</li>
                                        <li style="width:247px">RNTC</li>
                                    </ul>
                                   
                                    <div class="dados_listar_dados" style="height:80px" id="dados_listar_reboque"> 
                                    	<div id="hidden">     
                                            <ul class="reboque_listar">
                                                <li style="width:150px"><input type="text" style="width:150px" class="txt_pedido_nfe_transporte_reboque_placa"					name="txt_pedido_nfe_transporte_reboque_placa" 	value="" readonly="readonly" /></li>
                                                <li style="width:80px"><input  type="text" style="width:80px; text-align:center" class="txt_pedido_nfe_transporte_reboque_uf" 	name="txt_pedido_nfe_transporte_reboque_uf" 	value="" readonly="readonly" /></li>
                                                <li><input  type="hidden" class="hid_pedido_nfe_transporte_reboque_uf" 	name="hid_pedido_nfe_transporte_reboque_uf" value="" 	readonly="readonly" /></li>
                                                <li style="width:180px"><input type="text" style="width:180px" class="txt_pedido_nfe_transporte_reboque_rntc" 					name="txt_pedido_nfe_transporte_reboque_rntc" 	value="" readonly="readonly" /></li>
                                                <li style="width:22px"><a style="width:22px;height:22px;background-position:5px" name="" class="edit edit_reboque modal" href="" title="editar" rel="470-150"></a></li>
                                                <li style="width:22px"><a style="height:22px" class="a_excluir excluir_reboque" id="excluir_0" href="" title="Excluir item"></a></li>
                                    		</ul>
                                    	</div>    
                                	</div>
                            	</div>
                            	<a style="float:right; margin: 10px" class="btn_novo" id="btn_reboque_novo" href="pedido_nfe_transporte_reboque" rel="470-150">incluir</a>
							</div>
                            <div id="rad_balsa" style="float:left">
                            	<ul class="dados_nfe">
                                    <li style="width: 420px">
                                        <label for="txt_pedido_nfe_transporte_balsa_identificacao">Identifica&ccedil;&atilde;o da Balsa</label>
                                        <input type="text" style="width:420px" id="txt_pedido_nfe_transporte_balsa_identificacao" name="txt_pedido_nfe_transporte_balsa_identificacao" />
                                    </li>
                                </ul>
                            </div>   
                            <div id="rad_vagao" style="float:left">
                            	<ul class="dados_nfe">
                                    <li style="width: 420px">
                                        <label for="txt_pedido_nfe_transporte_vagao_identificacao">Identifica&ccedil;&atilde;o do Vag&atilde;o</label>
                                        <input type="text" style="width:420px" id="txt_pedido_nfe_transporte_vagao_identificacao" name="txt_pedido_nfe_transporte_vagao_identificacao" />
                                    </li>
                                </ul>
                            </div>                                
                        </div>
                        
                    </div>
                    
                    <div id="pedido_nfe_informacoes">
                    	<ul class="dados_nfe" style="margin-left:20px">
                        	<li style="width: 420px">
                                <label for="txt_pedido_nfe_fisco">Fisco</label>
                                <textarea style=" width:420px; height:100px" id="txt_pedido_nfe_fisco" name="txt_pedido_nfe_fisco"><?=$perfil_info_fisco?></textarea>
							</li>
                        	<li style="width: 420px">
                                <label for="txt_pedido_nfe_contribuite">Contribuinte</label>
                                <textarea style=" width:420px; height:100px" id="txt_pedido_nfe_contribuite" name="txt_pedido_nfe_contribuite"><?=$perfil_info_contribuinte?></textarea>
							</li>
						</ul>
                    </div>
                </div>  
              
                <div id="pedido_pagamento" style="margin-left:35px; width:445px">
                    <div class="pagamento_bottom">
                    	<ul style="float:right; margin-right: 10px">
                            <li style="visibility: hidden;">
                                <label class="pedido" for="txt_pedido_produtos">Produtos</label>
                            	<input type="text" id="txt_pedido_produtos" name="txt_pedido_produtos" value="<?=format_number_out($subTotalPedido)?>" readonly="readonly" />
                            </li>
                            <li>
                                <label class="pedido" for="txt_pedido_servicos">Servi&ccedil;os</label>
                            	<input type="text" id="txt_pedido_servicos" name="txt_pedido_servicos" value="<?=format_number_out($subTotalPedido)?>" readonly="readonly" />
                            </li>
                            <li>
                                <label class="pedido" for="txt_pedido_terceiros">Terceiros</label>
                            	<input style="background:#FEF9BA; height:25px; font-size:18px" type="text" id="txt_pedido_terceiros" name="txt_pedido_terceiros" value="<?=format_number_out($subTotalPedido)?>" />
                            </li>
                            <li>
                                <label class="pedido" for="txt_pedido_subtotal">Subtotal</label>
                            	<input style="background:#D7F9C6; height:25px; font-size:18px" type="text" id="txt_pedido_subtotal" name="txt_pedido_subtotal" value="<?=format_number_out($subTotalPedido)?>" readonly="readonly" />
                            </li>
                            <li>
                                <label for="txt_pedido_desconto">Desconto (%)</label>
                                <input style="background:#C0E9F8; height:22px; font-size:16px" type="text" id="txt_pedido_desconto" name="txt_pedido_desconto" value="<?=format_number_out($descPedido)?>" />
                            </li>
                            <li>
                                <label for="txt_pedido_desconto_reais">Desconto (R$)</label>
                                <input style="background:#C0E9F8; height:22px; font-size:16px" type="text" id="txt_pedido_desconto_reais" name="txt_pedido_desconto_reais" value="<?=format_number_out($descPedidoReais)?>" />
                            </li>
							<li>
								<label for="txt_pedido_nfe_total_desconto">Desconto em itens</label>
								<input style="background:#C0E9F8; height:22px; font-size:16px" type="text" id="txt_pedido_nfe_total_desconto" name="txt_pedido_nfe_total_desconto" value="0,00" readonly="readonly" />
							</li>
                            <li>
                                <label class="total" for="txt_pedido_total">Total</label>
                                <input type="text" class="total" id="txt_pedido_total" name="txt_pedido_total" value="<?=format_number_out($totalPedido)?>" readonly="readonly" />
                            </li>
<?							if($nfe_parcelamento_exibir == '1'){                             
?>								<li>
                                    <a style="margin:0; float:right" class="modal btn_cheque" name="btn_cheque" id="btn_cheque" title="exibir cheques" href="financeiro_cheque_listar" rel="780-410">cheques</a>
                                </li>
<?							}                                
?>						</ul>
                    </div>
<?					if($nfe_parcelamento_exibir == '1'){
						require_once('componentes/parcelamento/parcelas_novas.php');
					}
?>				</div>

<?				//verifica��o de n�mero de NFe pulado
				require_once('componentes/nfe/tabela_nnfe_faltando.php'); ?>
                <div style="float:right">
                    <input type="submit" class="btn_enviar" name="btn_gravar" id="btn_gravar" value="Gravar" title="Gravar" />
                </div>
            </form>
		</div>
<?	} ?>        
</div>

<script type="text/javascript">
	
	$("input#btn_gravar").click(function(event){
		$(this).attr("disabled", 'disabled');
	});

	$('#txt_cliente_codigo').select();
	
	$('div#dados_nfe div').hide();
	$('div#pedido_nfe_totais').show();
	$('div#pedido_nfe_totais div#totais_icms').show();
	
	$('ul#pedido_nfe_abas a[href=totais]').addClass('ativo');
	$('ul#pedido_nfe_totais_abas a[href=icms]').addClass('ativo');
	
	$('ul#pedido_nfe_abas a').click(function(event){
		event.preventDefault();
		//marcar aba ativa
		$('ul#pedido_nfe_abas a').removeClass('ativo');
		$(this).addClass('ativo');
		
		//ocultar todas as abas
		$('div#dados_nfe div').hide();
		
		//exibir a selecionada
		var aba = $(this).attr('href');
		
		if(aba == 'transporte'){
			$('div#pedido_nfe_transporte div#pedido_nfe_transporte_volumes').show();
			$('div#pedido_nfe_transporte div#pedido_nfe_transporte_volumes div').show();
			$('div.dados_listar_dados div#hidden').hide(); // pra esconder div hidden
		}
		else if(aba == 'totais'){
			$('div#pedido_nfe_totais div#totais_icms').show();
			$('ul#pedido_nfe_totais_abas a').removeClass('ativo');
			$('ul#pedido_nfe_totais_abas  a[href=icms]').addClass('ativo');
		}
		
		$('div#pedido_nfe_'+aba).show();
		$('div#pedido_nfe_'+aba+' input:first, div#pedido_nfe_'+aba+' textarea:first').focus();
	});
	
	//#############################################################################################################################################
	$('ul#pedido_nfe_totais_abas a').click(function(event){
		event.preventDefault();
		//marcar aba ativa
		$('ul#pedido_nfe_totais_abas a').removeClass('ativo');
		$(this).addClass('ativo');
		
		//ocultar todas as abas
		$('div#pedido_nfe_totais div').hide();
		
		//exibir a selecionada
		var aba = $(this).attr('href');
		
		$('div#totais_'+aba).show();
		$('div#totais_'+aba+' input:first, div#totais_'+aba+' textarea:first').focus();
	});
	
	//#############################################################################################################################################
	$('div#pedido_nfe_transporte div').hide();
	$('ul#pedido_nfe_transporte_abas a[href=volumes]').addClass('ativo');
	
	$('ul#pedido_nfe_transporte_abas a').click(function(event){
		event.preventDefault();
		//marcar aba ativa
		$('ul#pedido_nfe_transporte_abas a').removeClass('ativo');
		$(this).addClass('ativo');
	   
		//ocultar todas as abas
		$('div#pedido_nfe_transporte div').hide();
		
		//exibir a selecionada
		var aba = $(this).attr('href');
		$('div#pedido_nfe_transporte_'+aba).show();
		$('div#pedido_nfe_transporte_'+aba+' div').show();
		$('div.dados_listar_dados div#hidden').hide(); // pra esconder div hidden
		$('div#pedido_nfe_transporte_'+aba+' input:first, div#pedido_nfe_transporte_'+aba+' textarea:first').focus();
	});
</script>