<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
      	<title>myERP - Relat&oacute;rio de Cota&ccedil;&atilde;o</title>
        <link rel="stylesheet" type="text/css" href="style/style_relatorio.css" />
        <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />
        
	</head>
	<body>
<?
		ob_start();
		session_start();
		
		require("inc/con_db.php");
		require("inc/fnc_general.php");
	
		$rsDados = mysql_query("select * from tblempresa_info");
		$rowDados = mysql_fetch_array($rsDados);
		
		if($rowDados['fldTipo'] == 1){
			$CPF_CNPJDados = format_cpf_out($rowDados['fldCPF_CNPJ']);
		}elseif($rowDados['fldTipo'] == 2){
			$CPF_CNPJDados = format_cnpj_out($rowDados['fldCPF_CNPJ']);
		}
		
		$rsUsuario = mysql_query("select * from tblusuario where fldId=".$_SESSION['usuario_id']);
		$rowUsuario = mysql_fetch_array($rsUsuario);
		
?>		
    	<div id="no-print">
            <a style="margin-top: 6px" class="print" href="#" onClick="window.print()">imprimir</a>
        </div>
<?
		$sSQL = $_SESSION['produto_cotacao_relatorio'];
	
		$rsCotacao = mysql_query($sSQL);
		echo mysql_error();
		$rowsCotacao = mysql_num_rows($rsCotacao);		
		
		$limite = 42;
		$n = 1;
		
		$pgTotal = $rowsCotacao / $limite;
		$p = 1;
		
		$rsProduto = mysql_query("SELECT * FROM tblproduto WHERE fldId =".$_GET['produto']);
		$rowProduto = mysql_fetch_array($rsProduto);
	
?>		<div id="relative">
            <table class="relatorio_print">
                <tr style="border-bottom: 2px solid">
                    <td style="width: 690px">
                    	<h1>Relat&oacute;rio de Cota&ccedil;&atilde;o - 
                    		<span style="font-size:15px">[<?=$rowProduto['fldCodigo']?>] <?=substr($rowProduto['fldNome'],0,48)?></span>
                        </h1>
                    </td>
                    <td style="width: 110px"><p class="pag">p&aacute;g. <?=$p?> de <?=ceil($pgTotal)?></p></td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 550px" name="table_relatorio_dados" class="table_relatorio_dados" summary="Relat&oacute;rio">
                            <tr>
                                <td style="width: 320px;">Raz&atilde;o Social: <?=$rowDados['fldNome']?></td>
                                <td style="width: 200px;">Nome Fantasia: <?=$rowDados['fldNome_Fantasia']?></td>
                                <td style="width: 320px;">CPF/CNPJ: <?=$CPF_CNPJDados?></td>
                                <td style="width: 200px;">Telefone: <?=$rowDados['fldTelefone1']?></td>
                            </tr>
                        </table>	
                    </td>
                    <td>        
                        <table class="dados_impressao">
                            <tr>
                                <td><b>Data: </b><span><?=format_date_out(date("Y-m-d"))?></span></td>
                                <td><b>Hora: </b><span><?=format_time_short(date("H:i:s"))?></span></td>
                                <td><b>Usu&aacute;rio: </b><span><?=$rowUsuario['fldUsuario']?></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table name="table_relatorio" class="table_relatorio" summary="Relat&oacute;rio">
                            <tr style="border:none">
                                <td class="valor" style="text-align:center">Data</td>
                                <td style="width:315px">Fornecedor</td>
                                <td class="valor">Pre&ccedil;o</td>
                                <td style="width:315px">Observa&ccedil;&atilde;o</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table name="table_relatorio" class="table_relatorio" summary="Relat&oacute;rio">
<?
							while($rowCotacao = mysql_fetch_array($rsCotacao)){
							$x+= 1;
?>	
                                <tr>
                                    <td style="margin-left:4px" class="valor"><?=format_date_out($rowCotacao['fldData'])?></td>
                                    <td style="width:315px"><?=substr($rowCotacao['fldNomeFantasia'],0, 28)?></td>
                                    <td class="valor"><?=format_number_out($rowCotacao['fldValor'])?></td>
                                    <td style="width:315px"><?=substr($rowCotacao['fldObservacao'],0, 28)?></td>
                                </tr>
<?							
								if(($n == $limite) or ($x == $rowsItem)){
?>												</table>    
											</td>
										</tr>
									</table>	
<?       							$n = 1;

									if($x < $rowsItem){
										$p += 1;
										
?>										<table class="relatorio_print">
                                            <tr style="border-bottom: 2px solid">
                                                <td style="width: 600px"><h1>Relat&oacute;rio de Cota&Ccedil;&atilde;o</h1></td>
                                                <td style="width: 200px"><p class="pag">p&aacute;g. <?=$p?> de <?=ceil($pgTotal)?></p></td>
                                            </tr>
                                            
                                            <tr>
                                                <td>
                                                    <table style="width: 550px" name="table_relatorio_dados" class="table_relatorio_dados" summary="Relat&oacute;rio">
                                                        <tr>
                                                            <td style="width: 320px;">Raz&atilde;o Social: <?=$rowDados['fldNome']?></td>
                                                            <td style="width: 200px;">Nome Fantasia: <?=$rowDados['fldNome_Fantasia']?></td>
                                                            <td style="width: 320px;">CPF/CNPJ: <?=$CPF_CNPJDados?></td>
                                                            <td style="width: 200px;">Telefone: <?=$rowDados['fldTelefone1']?></td>
                                                        </tr>
                                                    </table>	
                                                </td>
                                                <td>        
                                                    <table class="dados_impressao">
                                                        <tr>
                                                            <td><b>Data: </b><span><?=format_date_out(date("Y-m-d"))?></span></td>
                                                            <td><b>Hora: </b><span><?=format_time_short(date("H:i:s"))?></span></td>
                                                            <td><b>Usu&aacute;rio: </b><span><?=$rowUsuario['fldUsuario']?></span></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table name="table_relatorio" class="table_relatorio" summary="Relat&oacute;rio">
                                                        <tr style="border:none">
                                                            <td class="valor" style="text-align:center">Data</td>
                                                            <td style="width:315px">Fornecedor</td>
                                                            <td class="valor">Pre&ccedil;o</td>
                                                            <td style="width:315px">Observa&ccedil;&atilde;o</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                        		<td>
                                           			<table name="table_relatorio" class="table_relatorio" summary="Relat&oacute;rio">
<?									}
								}else{
									$n += 1;
								}
							}
?>
		</div>
	</body>
</html>
