<script type="text/javascript">
	window.print();
</script>

<?
	if (!isset($_SESSION['logado'])){
		session_start();
	}

	ob_start();
	session_start();
	
	//conectar ao db
	require_once("inc/con_db.php");
	//funções gerais
	require_once("inc/fnc_general.php");
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html lang="pt-br">
<head>
<title>My ERP</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="Robots" content="none">

    <link rel="stylesheet" type="text/css" media="all" href="style/impressao/style_pedido_imprimir_recibo1.css">
    <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css">
    <link rel="stylesheet" type="text/css" media="screen" href="style/style_filtro.css" />
    <script type="text/javascript" src="js/general.js"></script>
    
</head>
<body>

<?	$pedido_id = $_GET['id'];
	
	$data = date("Y-m-d");
	
	$rsPedido = mysql_query("select * from tblpedido where fldId = ".$pedido_id);
	$rowPedido = mysql_fetch_array($rsPedido);
	
	$rsCliente = mysql_query("select * from tblcliente where fldId = ".$rowPedido['fldCliente_Id']);
	$rowCliente = mysql_fetch_array($rsCliente);
	
	//pegando o vencimento da primeira parcela
	$rsParcela = mysql_query("select fldVencimento from tblpedido_parcela where fldPedido_Id = $pedido_id LIMIT 1");
	$rowParcela = mysql_fetch_array($rsParcela);
	
	if($rowCliente['fldMunicipio_Codigo']){
		
		$queryString = $rowCliente['fldMunicipio_Codigo'];
		$municipio =  substr($queryString,2,5);	
		$uf =  substr($queryString,0,2);
			
		$rsMunicipio = mysql_query("select * from tblibge_municipio where fldCodigo = ".$municipio);
		$rowMunicipio = mysql_fetch_array($rsMunicipio);
	
		$rsUF = mysql_query("select * from tblibge_uf where fldCodigo = ".$uf);
		$rowUF = mysql_fetch_array($rsUF);
	}	
	
	$rsPedidoItem = mysql_query("SELECT * from tblpedido_item WHERE fldPedido_Id =".$pedido_id);
	
	if(isset($_POST['sel_contato'])){
		$sql = "SELECT * FROM tblcliente_contato WHERE fldId = ".$_POST['sel_contato'];
		echo mysql_error();
	}else{
		$sql = "SELECT * FROM tblcliente_contato WHERE fldCliente_Id =".$rowCliente['fldId']." ORDER BY fldId ASC LIMIT 1";
		echo mysql_error();
	}
	
	$rsContato = mysql_query($sql);
	$rowContato = mysql_fetch_array($rsContato);
	
	
	
?>
<div id="no-print">
    <form id="frm-filtro" action="pedido_imprimir_recibo1.php" method="post" style="width:700px">
        <fieldset>
            <ul style="width:700px; display:inline">
                <li>
                    <label for="sel_contato">Contato adicional</label>
                    <select class="sel_contato" style="width:150px;" id="sel_contato" name="sel_contato" >
                        <option <?=($_POST['sel_cliente_exibir'] == "todos") ? "selected='selected'" : ''?> value="todos">Todos</option>
<?						$rsSelContato = mysql_query("SELECT * FROM tblcliente_contato WHERE fldCliente_Id =".$rowPedido['fldCliente_Id']);     
                       	while($rowSelContato = mysql_fetch_array($rsSelContato)){
?>               	  		<option <?=($rowSelContato['fldId'] == $_POST['sel_contato']) ? "selected='selected'" : ''?> value="<?=$rowSelContato['fldId']?>"><?=$rowSelContato['fldNome']?></option>
<?						}
?>                	 </select>
                </li>
            </ul>
            <button style="float:left; margin-right:20px" type="submit" name="btn_exibir" title="Exibir">Exibir</button>
            <ul id="bts" style="width:260px">
                <li><a href="#" onClick="window.print()"><span>Imprimir</span></a></li>
                <li><a href="index.php?p=pedido&mensagem=ok"><span>Finalizar</span></a></li>
            </ul>
        </fieldset>
    </form>
</div>


<div class="pedido_imprimir_001" style="margin-top:1cm" >
    <ul class="dados_cliente">
        <li><?=acentoRemover($rowCliente['fldNomeFantasia'])?></li>
        <li style="top:1cm"><?=acentoRemover($rowCliente['fldEndereco']).", ".$rowCliente['fldNumero']?></li>
    </ul>
    <ul class="dados_pedido">
        <li style="top:-0.5cm"><?=str_pad($rowPedido['fldId'], 4, "0", STR_PAD_LEFT)?></li>
        <li><?=format_date_out($rowPedido['fldCadastroData'])?></li>
        <li style="height: 0.4cm;margin-top:0.2cm"><?=format_date_out($rowParcela['fldVencimento'])?></li>
    </ul>
    
    <ul class="dados_gerais">
        <li style="width:4.7cm; height: 0.4cm; padding-left:1.3cm"><?=acentoRemover($rowContato['fldNome'])?></li>
        <li style="width:3cm"><?=$rowCliente['fldTelefone1']?></li>
        <li style="padding-left:1.7cm; width:8.3cm; height: 1.1cm"><?=substr($rowPedido['fldObservacao'],0,100)?></li>
    </ul>
    
    <div style="height:6.7cm; display:table">
<?		$vendaDecimal 		= fnc_sistema('venda_casas_decimais');
		$quantidadeDecimal 	= fnc_sistema('quantidade_casas_decimais');
		while($rowPedidoItem = mysql_fetch_array($rsPedidoItem)){  
			$rowProduto 	= mysql_fetch_array(mysql_query("SELECT * FROM tblproduto where fldId =".$rowPedidoItem['fldProduto_Id']));
			$qtd 			= $rowPedidoItem['fldQuantidade'];
			$valor			= $rowPedidoItem['fldValor'];
			$desconto 		= $valor * $rowPedidoItem['fldDesconto'] / 100;
			$desconto_reais = $rowPedidoItem['fldDescontoReais'];
			$total_item 	= $valor * $qtd;
			$total_item 	= $total_item - $desconto - $desconto_reais;
			
			//AQUI BUSCAR DADOS DA NFE
			$rowItemNFe = mysql_fetch_array(mysql_query("SELECT * FROM tblpedido_item_fiscal WHERE fldItem_Id= " .$rowPedidoItem['fldId']));
			
?>	    	<ul class="dados_gerais" style="margin:0">
    	        <li style="width:0.4cm; margin:0; ">&nbsp;</li>
                <li style="width:4.3cm; margin:0; "><?=acentoRemover($rowPedidoItem['fldDescricao'])?></li>
                <li style="width:0.7cm; margin:0; text-align:right"><?=format_number_out($rowPedidoItem['fldQuantidade'],$quantidadeDecimal)?></li>
                <li style="width:1.3cm; margin:0; text-align:right"><?=format_number_out($rowPedidoItem['fldValor'],$vendaDecimal)?></li>
                <li style="width:1.3cm; margin:0; text-align:right"><?=format_number_out($total_item)?></li>
            </ul>
            
<?			$total 					+= $total_item;
			$pedido_desconto 		= $total * $rowPedido['fldDesconto'] / 100;
			$pedido_desconto_reais 	= $rowPedido['fldDescontoReais'];
			$total_pedido 			= $total - $pedido_desconto - $pedido_desconto_reais;
		}
		//AQUI!! ####################################################################################################################//
		if($pedido_desconto > 0 || $pedido_desconto_reais > 0 ){
			$desconto = ($pedido_desconto > 0) ? $pedido_desconto : $pedido_desconto_reais;
?>			
			<ul class="dados_gerais" style="margin:0">
    	        <li style="width:0.3cm; margin:0; ">&nbsp;</li>
                <li style="width:4.4cm; margin:0; ">Desconto</li>
                <li style="width:0.7cm; margin:0; text-align:right">&nbsp;</li>
                <li style="width:1.3cm; margin:0; text-align:right">&nbsp;</li>
                <li style="width:1.3cm; margin:0; text-align:right"><?=format_number_out($desconto)?></li>
            </ul>			
<?		}
		// ###########################################################################################################################//
?>  </div>
    
    <ul class="dados_gerais">
        <li style="float:right;width:2.3cm;padding-right:2.2cm;text-align:right"><?=format_number_out($total_pedido)?></li>
    </ul>
</div>
</body>
</html>