<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
      	<title>myERP - Relat&oacute;rio de Movimenta&ccedil;&atilde;o de Estoque</title>
        <link rel="stylesheet" type="text/css" href="style/style_relatorio.css" />
        <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />
	</head>
	<body>
<? 
		ob_start();
		session_start();
		
		require("inc/con_db.php");
		require("inc/fnc_general.php");
		
		$rowDados 		= mysql_fetch_array(mysql_query("SELECT * FROM tblempresa_info"));
		$CPF_CNPJDados 	= formatCPFCNPJTipo_out($rowDados['fldCPF_CNPJ'], $rowDados['fldTipo']);
		$rowUsuario 	= mysql_fetch_array(mysql_query("SELECT * FROM tblusuario WHERE fldId=".$_SESSION['usuario_id']));
		
		$produto_id			= $_GET['produto_id'];
		$quantidadeDecimal 	= fnc_sistema('quantidade_casas_decimais');	
		$rsProduto 			= mysql_query("SELECT fldNome FROM tblproduto WHERE fldId = $produto_id");
		$rowProduto			= mysql_fetch_array($rsProduto);
		#ABAIXO CRIO O CABECALHO #########################################################################################################################################################
		
		
		$tabelaCabecalho =' 
			<tr style="border-bottom: 2px solid">
                <td style="width: 600px"><h1>Relat&oacute;rio de Movimenta&ccedil;&atilde;o de Estoque</h1></td>';
		$tabelaCabecalho2 =' 
            </tr>
            <tr>
                <td>
                    <table style="width: 580px" class="table_relatorio_dados" summary="Relat&oacute;rio">
                        <tr>
                            <td style="width: 320px;">Raz&atilde;o Social: '.$rowDados['fldNome'].'</td>
                            <td style="width: 200px;">Nome Fantasia: '.$rowDados['fldNome_Fantasia'].'</td>
                            <td style="width: 320px;">CPF/CNPJ: '.$CPF_CNPJDados.'</td>
                            <td style="width: 200px;">Telefone: '.$rowDados['fldTelefone1'].'</td>
                        </tr>
                    </table>	
                </td>
                <td>        
                    <table class="dados_impressao">
                        <tr>
                            <td><b>Data: 			</b><span>'.format_date_out(date("Y-m-d")).'</span></td>
                            <td><b>Hora: 			</b><span>'.format_time_short(date("H:i:s")).'</span></td>
                            <td><b>Usu&aacute;rio: 	</b><span>'.$rowUsuario['fldUsuario'].'</span></td>
                        </tr>
                    </table>
                </td>
            </tr>
			<tr>
				<td style="margin:0"><h2 style="background:#E1E1E1;width:790px;padding-left:10px">'.$rowProduto['fldNome'].'</h2></td>
			</tr>
            <tr>
                <td style="margin:0">
                    <table class="table_relatorio" summary="Relat&oacute;rio">
                        <tr style="border:none">
							<td style="width:100px;text-align:center;font-weight:bold">Movimenta&ccedil;&atilde;o</td>
							<td style="width:190px;text-align:right;font-weight:bold" >Entrada</td>
							<td style="width:150px;text-align:right;font-weight:bold" >Sa&iacute;da</td>
							<td style="width:100px;text-align:center;font-weight:bold">Validade</td>
							<td style="width:100px;text-align:right;font-weight:bold" >Saldo</td>
							<td style="width:100px;text-align:right;font-weight:bold" >Saldo Final</td>
						</tr>
					</table>
				</td>
			</tr>';
			$tabelaCabecalho3 ='
			<tr>
				<td>
					<table class="table_relatorio" summary="Relat&oacute;rio">';
	######################################################################################################################################################################################
	# AGORA CRIO VARIAVEIS, ARMAZENANDO OS REGISTROS DE ITENS, ABAIXO APENAS FAÇO IMPRIMIR NA TELA COM AS QUEBRAS DE LINHAS
	# BLOCO DE ITENS LANCADOS ############################################################################################################################################################
		
		$modo			= $_GET['modo'];
		$tipo			= $_GET['tipo'];
		
		if($modo == 'ordenar'){
			$GROUP_BY = 'tblproduto_estoque_movimento.fldId';
			$ORDER_BY = ($tipo == 'validade') ? 'tblproduto_estoque_movimento.fldValidade_Data,' : '';
			
		}elseif($modo == 'agrupar'){
			
			$GROUP_BY = ($tipo == 'validade')? 'fldValidade_Data' : 'tblproduto_estoque_movimento.fldData';
		}
		
		$sSQL 			= $_SESSION['produto_estoque_relatorio']. " GROUP BY $GROUP_BY ORDER BY $ORDER_BY tblproduto_estoque_movimento.fldData, tblproduto_estoque_movimento.fldHora, tblproduto_estoque_movimento.fldId ";
		$rsEstoque 		= mysql_query($sSQL);
		$rowsEstoque	= mysql_num_rows($rsEstoque);
		
		$n	 			= 1; #DEFINE O NUMERO DO BLOCO
		$countItem 		= 1; #DEFINE CONTAGEM DE ITENS NO WHILE PARA QUEBRA DE BLOCO AO ATINGIR LIMITE 
		$x				= 1; #DEFINE CONTAGEM DE ITENS TOTAIS, PRA SABER SE JA TERMINOU WHILE MAS AINDA FALTA ESPACO
		$limite 		= 43;
		
		$pgTotal 		= ceil($rowsEstoque / $limite);
		$p = 1;
		
		while($rowEstoque = mysql_fetch_array($rsEstoque)){
			echo mysql_error();
			
			$saldo		 = $rowEstoque['fldEntrada'] - $rowEstoque['fldSaida'];
			$saldo_final += $saldo;
			$pagina[$n] .= '
			<tr> 
				<td style="width:100px;text-align:center">'.format_date_out($rowEstoque['fldData']).'</td>
				<td style="width:190px;text-align:right" >'.format_number_out($rowEstoque['fldEntrada'], $quantidadeDecimal).'</td>
				<td style="width:150px;text-align:right" >'.format_number_out($rowEstoque['fldSaida'], $quantidadeDecimal).'</td>
				<td style="width:100px;text-align:center">'.format_date_out($rowEstoque['fldValidade_Data']).'</td>
				<td style="width:100px;text-align:right" >'.format_number_out($saldo, $quantidadeDecimal).'</td>
				<td style="width:100px;text-align:right" >'.format_number_out($saldo_final, $quantidadeDecimal).'</td>
			</tr>';
			
			#SE CHEGAR A 20 LINHAS, MUDA DE 'BLOCO' E RECMECA CONTAGEM
			if($countItem == $limite){
				$countItem = 1;
				$n ++;
			}elseif($rowsEstoque == $x && $countItem < $limite){ #SE JA TERMINOU O WHILE DE REGISTROS MAS AINDA NAO ATINGIU 20 LINHAS, CONTINUAR CRIANDO LINHAS ATE O LIMITE
				while($countItem <= $limite){ $pagina[$n] .='<tr style="border:0; width:800px"></tr>'; $countItem++;}
			}else{
				$countItem ++;
			}
			$x ++;
		}
	
	#AGORA MANDO GERAR NA TELA PARA IMPRESSAO ############################################################################################################################################

		$x = 1;
		while($x <= $n){
			$tabelaCabecalho1 = ($x == 1)? '<table class="relatorio_print" style="page-break-before:avoid">'.$tabelaCabecalho : '<table class="relatorio_print">'.$tabelaCabecalho;
			#PRIMEIRO BLOCO (LANCADOS) ###################################################################################################################################################
				print $tabelaCabecalho1;
				
                print '<td style="width: 200px"><p class="pag">'.$p.' de '.$pgTotal.'</p></td>';
				
				print $tabelaCabecalho2;
				print $tabelaCabecalho3;
				echo  $pagina[$x];
?>
						</table>
					</td >
				</tr>
			</table>			
<?			$x ++;
			$p ++;
		}
?>                
	</body>
</html>	
