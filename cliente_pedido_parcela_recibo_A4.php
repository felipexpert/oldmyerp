<script type="text/javascript">
	window.print();
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
        <title>Impress&atilde;o de recibo</title>
		<link rel="stylesheet" type="text/css" href="style/impressao/style_pedido_imprimir_recibo_A4.css" />
    	<link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />
    
	</head>
	<body>
    
<? 
	ob_start();
	session_start();
	
	require("inc/con_db.php");
	require("inc/fnc_general.php");
	
	$filtro = $_GET['filtro'];
	$id_parcela =  $_GET['id'];
	
	$rsDados = mysql_query("select * from tblempresa_info");
	$rowDados = mysql_fetch_array($rsDados);
	
	if(isset($filtro)){
		$rsBaixa = mysql_query("select tblpedido_parcela_baixa.*, 
								tblpedido_parcela_baixa.fldValor as fldValorBaixa, 
								tblpedido_parcela_baixa.fldDesconto as fldBaixaDesconto, 
								tblpedido_parcela_baixa.fldMulta as fldBaixaMulta,
								tblpedido_parcela.fldValor as fldValorParcela, 
								tblpedido_parcela.*,
								tblpagamento_tipo.fldSigla as fldPagamento_Tipo
								
								FROM tblpedido_parcela LEFT JOIN tblpedido_parcela_baixa
								ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
								LEFT JOIN tblpagamento_tipo ON tblpedido_parcela.fldPagamento_Id = tblpagamento_tipo.fldId
								WHERE tblpedido_parcela_baixa.fldId IN (".$filtro.")");
		
	}elseif(isset($id_parcela)){
		
		$rsBaixa = mysql_query("SELECT tblpedido.*, tblpedido_parcela.fldValor as fldValorParcela,
								tblpedido_parcela.fldId as fldParcela_Id,
								tblpagamento_tipo.fldSigla as fldPagamento_Tipo,
								tblpedido_parcela.* FROM tblpedido_parcela INNER JOIN tblpedido
								ON tblpedido_parcela.fldPedido_Id = tblpedido.fldId
								LEFT JOIN tblpagamento_tipo ON tblpedido_parcela.fldPagamento_Id = tblpagamento_tipo.fldId
								WHERE tblpedido_parcela.fldId IN (".$id_parcela.")");
	}
	
	echo mysql_error();
	$data = date("Y-m-d");
	$hora = date("H:i:s");
?>
    <div id="no-print">
        <a class="print" href="#" onClick="window.print()">imprimir</a>
    </div>
<?	
	while($rowBaixa = mysql_fetch_array($rsBaixa)){
		echo mysql_error();
		
		//se eh por selecao de parcelas, busca o valor das baixa em outra consulta
		if(isset($id_parcela)){
			$rsBaixaValor = mysql_query("SELECT SUM(tblpedido_parcela_baixa.fldValor * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldValorBaixa,
										SUM(tblpedido_parcela_baixa.fldMulta * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldBaixaMulta,
										SUM(tblpedido_parcela_baixa.fldDesconto * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldBaixaDesconto,
										SUM(tblpedido_parcela_baixa.fldJuros * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldJurosTotal, 
										   tblpedido_parcela_baixa.*,
										   tblpedido_parcela.fldValor as fldValorParcela,
										   tblpedido_parcela.* FROM tblpedido_parcela LEFT JOIN tblpedido_parcela_baixa
										   ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
										   WHERE tblpedido_parcela.fldId =".$rowBaixa['fldParcela_Id']." GROUP BY tblpedido_parcela_baixa.fldParcela_Id");
			$rowBaixaValor = mysql_fetch_array($rsBaixaValor);
			echo mysql_error();
			
			$valor_apagar	= $rowBaixaValor['fldValorParcela'];
			$baixaValor 	= $rowBaixaValor['fldValorBaixa'] + $rowBaixaValor['fldJurosTotal'] + $rowBaixaValor['fldBaixaMulta'];
			$dataRecebido 	= $rowBaixaValor['fldDataRecebido'];
			$juros 			= ($_SESSION["sistema_recibo_exibir_juros"] == '1') ? $rowBaixa['fldJurosTotal'] : '';
			$desconto 		= $rowBaixaValor['fldBaixaDesconto'];
			$multa 			= ($_SESSION["sistema_recibo_exibir_multa"] == '1') ? $rowBaixa['fldBaixaMulta'] : '';
			$devedor 		= ($valor_apagar  + $multa + $juros) - ($baixaValor + $rowBaixaValor['fldBaixaDesconto']);
		}else{
			$devedor_anterior = $rowBaixa['fldDevedor'];
			$valor_apagar 	= $rowBaixa['fldDevedor'] + $rowBaixa['fldValorBaixa'];
			$baixaValor 	= $rowBaixa['fldValorBaixa'] + $rowBaixa['fldJuros'] + $rowBaixa['fldBaixaMulta'];
			$dataRecebido 	= $rowBaixa['fldDataRecebido'];
			$juros 			= ($_SESSION["sistema_recibo_exibir_juros"] == '1') ? $rowBaixa['fldJuros'] : '';
			$desconto 		= $rowBaixa['fldBaixaDesconto'];
			$multa 			= ($_SESSION["sistema_recibo_exibir_multa"] == '1') ? $rowBaixa['fldBaixaMulta'] : '';
			$devedor 		= ($valor_apagar + $multa + $juros) - ($baixaValor + $rowBaixa['fldBaixaDesconto']);
		}
		
		$rows = mysql_num_rows(mysql_query("SELECT * FROM tblpedido_parcela WHERE fldPedido_Id=".$rowBaixa['fldPedido_Id']));
		$rowCliente = mysql_fetch_array(mysql_query("select tblcliente.*, tblpedido.fldId from tblcliente inner join tblpedido
													on tblcliente.fldId = tblpedido.fldCliente_Id where tblpedido.fldId=".$rowBaixa['fldPedido_Id']));
?>
		<div style="width:750px">
            <div class="recibo_print" style="float:left;width:490px">
                <h1>Recibo de pagamento</h1>
                <p style="width:360px">
                    <span style="font-size: 15px"><?=$rowDados['fldNome_Fantasia']?></span><br /><span>CNPJ: <?=format_cnpj_out($rowDados['fldCPF_CNPJ'])?></span>
                </p>
                <p class="data">
                    <span><?=format_date_out($data)?></span>&nbsp;<span><?=format_time_short($hora)?></span>
                </p>
                
                <p>Cliente <span><?=str_pad($rowCliente['fldCodigo'],5,"0",STR_PAD_LEFT)?></span><br /><span><?=$rowCliente['fldNome']?>&nbsp;<?=format_cpf_out($rowCliente['fldCPF_CNPJ'])?></span></p>
                
                <div class="dados" style="width:480px">
                    <ul class="cabecalho" style="width: 470px">
                        <li style="width:50px">Receb.</li>
                        <li style="width:40px;text-align:center">Venda</li>
                        <li style="width:40px;text-align:center">Parc.</li>
                        <li style="width:50px">Vencto</li>
                        <li style="width:50px;text-align:right">Valor</li>
<? 						if($_SESSION["sistema_recibo_exibir_multa"] == '1'){  
							print '<li style="width:50px;text-align:right">Multa</li>';
						}
						if($_SESSION["sistema_recibo_exibir_juros"] == '1'){
                        	print '<li style="width:50px;text-align:right">Juros</li>';
						}
?>						
                        <li style="width:50px;text-align:right">Desc.</li>
                        <li style="width:50px;text-align:right">Devedor</li>
                    </ul>
                    <ul class="dados">
                        <li style="width:50px"><?=format_date_out3($dataRecebido)?></li>
                        <li style="width:40px;text-align:center"><?=$rowBaixa['fldPedido_Id']?></li>
                        <li style="width:40px;text-align:center"><?=$rowBaixa['fldParcela']?>/<?=$rows?></li>
                        <li style="width:50px"><?=format_date_out3($rowBaixa['fldVencimento'])?></li>
                        <li style="width:50px;text-align:right"><?=format_number_out($valor_apagar)?></li>
<? 						if($_SESSION["sistema_recibo_exibir_multa"] == '1'){  
							print '<li style="width:50px;text-align:right">'.format_number_out($multa).'</li>';
						}
						if($_SESSION["sistema_recibo_exibir_juros"] == '1'){
                        	print '<li style="width:50px;text-align:right">'.format_number_out($juros).'</li>';
						}
?>                        
                        <li style="width:50px;text-align:right"><?=format_number_out($desconto)?></li>
                        <li style="width:50px;text-align:right"><?=format_number_out($devedor)?></li>
                        <li style="width:30px;text-align:right; color:green;"><?=$rowBaixa['fldPagamento_Tipo'];?></li>
                    </ul>
                </div>
                <div class="valor">
                    <p>valor pago: <span><?=format_number_out($baixaValor)?></span></p>
                </div>
            </div>
<?
			if(fnc_sistema('recibo_via') == 2){
?>        
                <div class="recibo_print" style="width:245px;float:left">
                    <h1>Recibo de pagamento</h1>
                    <p style="width:245px">
                        <span style="font-size: 14px"><?=$rowDados['fldNome_Fantasia']?></span><br /><span>CNPJ: <?=format_cnpj_out($rowDados['fldCPF_CNPJ'])?></span>
                    </p>
                    <p class="data">
                        <span><?=format_date_out($data)?></span>&nbsp;<span><?=format_time_short($hora)?></span>
                    </p>
                    <p>Cliente <span><?=$rowCliente['fldCodigo']?></span><br /><span ><?=$rowCliente['fldNome']?>&nbsp;<?=format_cpf_out($rowCliente['fldCPF_CNPJ'])?></span></p>
                    <div class="dados" style="width:230px">
                        <ul class="cabecalho">
                            <li style="width:50px">Receb.</li>
                            <li style="width:40px;text-align:center">Venda</li>
                            <li style="width:40px;text-align:center">Parc.</li>
                            <li style="width:70px;text-align:right">Pago</li>
                        </ul>
                        <ul class="dados">
                            <li style="width:50px"><?=format_date_out3($dataRecebido)?></li>
                            <li style="width:40px;text-align:center"><?=$rowBaixa['fldPedido_Id']?></li>
                            <li style="width:40px;text-align:center"><?=$rowBaixa['fldParcela']?>/<?=$rows?></li>
                            <li style="width:80px;text-align:right"><?=format_number_out($baixaValor)?></li>
                        </ul>
                    </div>
                </div>
            
<?			}
		}
?>
		</div>
    </body>
</html>