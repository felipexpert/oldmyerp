<?php
	include "inc/con_db.php";
	include "inc/fnc_general.php";
	require_once("inc/fnc_produto_preco.php");
	require_once("inc/fnc_ibge.php");
	ob_start();
	session_start();
	$id = strip_tags($_GET['id']);
	// busca do cliente pelo código
	if(isset($_GET['busca_cliente'])){
			$pedidoId	 = $_GET['pedidoId'];
			$queryString = $_GET['busca_cliente'];
			$rs 		 = "SELECT tblcliente.*, tblcliente.fldId as clienteId, tblcliente.fldNome as clienteNome, tblcliente.fldObservacao as clienteObservacao,
							tblfuncionario.fldId as funcionarioId, tblfuncionario.fldNome as funcionarioNome
							FROM tblcliente 
							LEFT JOIN tblfuncionario ON tblcliente.fldFuncionario_Id = tblfuncionario.fldId 
							WHERE tblcliente.fldCodigo = '$queryString' ";
			$rows = mysql_query($rs);
			$row  = mysql_fetch_array($rows);
			
			$exibir_observacao 	= fnc_sistema("pedido_observacao_cliente");
			$cliente_id 		= $row['clienteId'];
			$disabled 			= $row['fldDisabled'];
			$cliente_endereco 	= rawurlencode(utf8_decode($row['fldEndereco'])).', '.$row['fldNumero'];
			
			$cliente_nome 		= rawurlencode(utf8_decode($row['clienteNome']));
			$cliente_observacao = rawurlencode(utf8_decode($row['clienteObservacao']));
			$Funcionario_Id 	= $row['funcionarioId'];
			$funcionario_nome 	= rawurlencode(utf8_decode($row['funcionarioNome']));
			
			if($cliente_id != 0){
				$rsVendas = mysql_query("SELECT count(fldId) as fldTotal FROM tblpedido WHERE fldCliente_Id = '$cliente_id' AND fldExcluido = '0'");
				$rowVendas = mysql_fetch_array($rsVendas);
				$totalVendas = $rowVendas['fldTotal'];
			
				//verifica se a venda esta sendo editada, se é o mesmo cliente ou fio alterado
				$rsVenda = mysql_query("SELECT fldCliente_Pedido FROM tblpedido WHERE fldId = $pedidoId AND fldCliente_Id = $cliente_id");
				if(mysql_num_rows($rsVenda)){
					$rowVenda = mysql_fetch_array($rsVenda);
					$totalVendas = $rowVenda['fldCliente_Pedido'] - 1;
					$disabled = 0;
				}	
			}
			$xml = "\t\t<cliente_id>$cliente_id</cliente_id>\n";
			$xml .= "\t\t<disabled>$disabled</disabled>\n";
			$xml .= "\t\t<cliente_nome>$cliente_nome</cliente_nome>\n";
			$xml .= "\t\t<cliente_endereco>$cliente_endereco</cliente_endereco>\n";
			$xml .= "\t\t<exibir_observacao>$exibir_observacao</exibir_observacao>\n";
			$xml .= "\t\t<cliente_observacao>$cliente_observacao</cliente_observacao>\n";
			$xml .= "\t\t<funcionario_id>$Funcionario_Id</funcionario_id>\n";
			$xml .= "\t\t<funcionario_nome>$funcionario_nome</funcionario_nome>\n";
			$xml .= "\t\t<totalVendas>$totalVendas</totalVendas>\n";
			
			if(isset($_GET['pedido_nfe'])){
				
				$entregaCPF_CNPJ = $row['fldCPF_CNPJ'];
				$rsEntregaPadrao = mysql_query("SELECT * FROM tblcliente_endereco_entrega WHERE fldCliente_Id = $cliente_id AND fldPadrao = 1");
				if(mysql_num_rows($rsEntregaPadrao)){
					
					$rowEntregaPadrao 	= mysql_fetch_array($rsEntregaPadrao);
					$entregaEndereco 	= $rowEntregaPadrao['fldEndereco'];
					$entregaNumero		= $rowEntregaPadrao['fldNumero'];
					$entregaComplemento = $rowEntregaPadrao['fldComplemento'];
					$entregaBairro 		= $rowEntregaPadrao['fldBairro'];
					$entregaCEP			= $rowEntregaPadrao['fldCep'];
					$entregaCodigo	 	= $rowEntregaPadrao['fldMunicipio_Codigo'];
					$entregaMunicipio 	= fnc_ibge_municipio($rowEntregaPadrao['fldMunicipio_Codigo']);
				}else{
					$entregaCodigo	 	= $row['fldMunicipio_Codigo'];
				}
				
				$xml .= "\t\t<entregaEndereco>$entregaEndereco</entregaEndereco>\n";
				$xml .= "\t\t<entregaNumero>$entregaNumero</entregaNumero>\n";
				$xml .= "\t\t<entregaComplemento>$entregaComplemento</entregaComplemento>\n";
				$xml .= "\t\t<entregaBairro>$entregaBairro</entregaBairro>\n";
				$xml .= "\t\t<entregaCEP>$entregaCEP</entregaCEP>\n";
				$xml .= "\t\t<entregaCodigo>$entregaCodigo</entregaCodigo>\n";
				$xml .= "\t\t<entregaMunicipio>$entregaMunicipio</entregaMunicipio>\n";
				$xml .= "\t\t<entregaCPF_CNPJ>$entregaCPF_CNPJ</entregaCPF_CNPJ>\n";
			}
			
			header('Content-Type: application/xml; charset=utf-8');
			echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
			echo "\t<dados>\n";
			echo $xml;
			echo "\t</dados>\n";
	}
	elseif(isset($_POST['item_nfe_inserir'])){
			$cfop = $_POST['cfop_codigo'];	
			$rsCFOP = mysql_query("SELECT * FROM tblnfe_cfop WHERE fldCFOP = $cfop");
			$rows = mysql_num_rows($rsCFOP);
			
			$xml = "\t\t<rows>$rows</rows>\n";
			
			header('Content-Type: application/xml; charset=utf-8');
			echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
			echo "\t<dados>\n";
			echo $xml;
			echo "\t</dados>\n";
	}
	elseif(isset($_GET['item_fiscal_estado_busca'])){
			$uf_codigo  	= $_GET['item_fiscal_estado_busca'];	
			$produto_id 	= $_GET['produto_id'];
			$emitente_estado = $_GET['emitente_estado'];
			
			//CFOP DENTRO OU FORA DO ESTADO
			$rowCFOP = mysql_fetch_array(mysql_query('SELECT fldcfop_id, fldcfop_fora_id FROM tblproduto_fiscal WHERE fldProduto_Id = '.$produto_id));
			if($emitente_estado == $uf_codigo ){
				$rowCFOP = mysql_fetch_array(mysql_query('SELECT fldCFOP FROM tblnfe_cfop WHERE fldId = '.$rowCFOP['fldcfop_id']));
				$cfop = $rowCFOP['fldCFOP'];
			}else{
				$rowCFOP = mysql_fetch_array(mysql_query('SELECT fldCFOP FROM tblnfe_cfop WHERE fldId = '.$rowCFOP['fldcfop_fora_id']));
				$cfop = $rowCFOP['fldCFOP'];
			}
			
			$sql 		= "SELECT * FROM tblproduto_fiscal_estado WHERE fldProduto_Id = $produto_id AND fldUF_Codigo = $uf_codigo";
			$rsEstado 	= mysql_query($sql);
			$rowEstado 	= mysql_fetch_array($rsEstado);
			
			echo mysql_error();
			$aliquota 	= format_number_out($rowEstado['fldicmsst_aliquota']);
			$mva 		= format_number_out($rowEstado['fldicmsst_mva']);
			
			$xml = "\t\t<aliquota>$aliquota</aliquota>\n";
			$xml .= "\t\t<mva>$mva</mva>\n";
			$xml .= "\t\t<cfop>$cfop</cfop>\n";
			
			header('Content-Type: application/xml; charset=utf-8');
			echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
			echo "\t<dados>\n";
			echo $xml;
			echo "\t</dados>\n";
	}
	elseif (isset($_POST['sel_cliente_responsavel'])){
			$queryString = $_POST['sel_cliente_responsavel'];
			$responsavel_id = $_POST['responsavel_id'];
			
			$rs = "SELECT tblcliente.fldNome, tblcliente_dados_adicionais.fldResponsavel 
					FROM tblcliente LEFT JOIN tblcliente_dados_adicionais ON tblcliente.fldId = tblcliente_dados_adicionais.fldCliente_Id
					WHERE tblcliente.fldId = '$queryString' AND fldStatus_Id != '5'";
			$rs = mysql_query($rs);
			$row = mysql_fetch_array($rs);
				
?>			<option value="1"><?=$row['fldNome']?></option>
<?			if($row['fldResponsavel']){
?>				<option value="2"><?=$row['fldResponsavel']?></option>
<?			}            
	}
	// busca do municipio pelo código
	elseif (isset($_GET['municipio_codigo_busca'])){
			$queryString = $_GET['municipio_codigo_busca'];
	
			$rs = "SELECT tblibge_municipio.fldNome, tblibge_uf.fldSigla 
			FROM tblibge_municipio inner join tblibge_uf
			ON tblibge_municipio.fldUF_Codigo = tblibge_uf.fldCodigo 
			WHERE tblibge_municipio.fldCodigo  = ".substr($queryString,2,5)." AND tblibge_municipio.fldUF_codigo = ".substr($queryString,0,2);
			$rows = mysql_query($rs);
			
			$row 	= mysql_fetch_array($rows);
			$nome 	= rawurlencode(utf8_decode($row['fldNome']));
			$uf 	= $row['fldSigla'];
			
			$xml = "\t\t<nome>$nome</nome>\n";
			$xml .= "\t\t<uf>$uf</uf>\n";

			header('Content-Type: application/xml; charset=utf-8');
			echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
			echo "\t<dados>\n";
			echo $xml;
			echo "\t</dados>\n";
	}
	// busca do endereco
	elseif (isset($_GET['endereco_codigo_busca'])){
			$queryString = $_GET['endereco_codigo_busca'];
	
			$sql = "SELECT tblendereco.*, tblendereco_bairro.fldBairro, tblendereco_bairro.fldSetor
			FROM tblendereco 
			LEFT JOIN tblendereco_bairro ON tblendereco.fldBairro_Id = tblendereco_bairro.fldId
			WHERE tblendereco.fldId = $queryString";
			
			$rows 		= mysql_query($sql);
			$row 		= mysql_fetch_array($rows);
			$endereco 	= rawurlencode(utf8_decode($row['fldRua']));
			$bairro	 	= rawurlencode(utf8_decode($row['fldBairro']));
			$setor	 	= $row['fldSetor'];
			$cep	 	= $row['fldCep'];
			
			$xml = "\t\t<endereco>$endereco</endereco>\n";
			$xml .= "\t\t<bairro>$bairro</bairro>\n";
			$xml .= "\t\t<setor>$setor</setor>\n";
			$xml .= "\t\t<cep>$cep</cep>\n";

			header('Content-Type: application/xml; charset=utf-8');
			echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
			echo "\t<dados>\n";
			echo $xml;
			echo "\t</dados>\n";
	}
	elseif (isset($_POST['busca_funcionario'])){
			
			$queryString 	= $_POST['busca_funcionario'];
			$sql 			= "SELECT fldNome, fldFuncao1_Comissao FROM tblfuncionario WHERE fldId = '$queryString'";
			//"SELECT fldId, fldNome, fldFuncao1_Comissao FROM tblfuncionario WHERE fldFuncao1_Comissao > 0 ORDER BY fldId DESC LIMIT 1 "
			$rs 			= mysql_query($sql);
			$row 			= mysql_fetch_array($rs);
			//echo $_POST['busca_funcionario'];
			$nome 		= $row['fldNome'];
			$comissao 	= $row['fldFuncao1_Comissao'];
			
			$xml  = "\t\t<nome>$nome</nome>\n";
			$xml .= "\t\t<comissao>$comissao</comissao>\n";

			header('Content-Type: application/xml; charset=utf-8');
			echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
			echo "\t<dados>\n";
			echo $xml;
			echo "\t</dados>\n";
	}
	//busca do fornecedor pelo código
	elseif(isset($_POST['busca_fornecedor'])){
			$queryString = $_POST['busca_fornecedor'];
			$s = "SELECT fldNomeFantasia FROM tblfornecedor WHERE fldId = '$queryString'";
			$s1 = mysql_query($s);
			
			while ($l = mysql_fetch_array($s1)){
				echo $l['fldNomeFantasia'];
			}
	}
	//VERIFICAR CPF DO CLIENTE **********
	elseif(isset($_POST['checar_cpfcnpj'])){
		$cpf = $_POST['checar_cpfcnpj'];
		$tabela = $_POST['tabela'];
		$sqlChecarCPF = mysql_num_rows(mysql_query("SELECT fldCPF_CNPJ FROM ".$tabela." WHERE fldCPF_CNPJ = '$cpf'"));
		if($sqlChecarCPF > 0){echo 'erro';}
	}
	//busca do produto pelo codigo
	elseif(isset($_GET['busca_produto'])){
			$compraDecimal 			= fnc_sistema('compra_casas_decimais');
			$vendaDecimal 			= fnc_sistema('venda_casas_decimais');
			$quantidadeDecimal 		= fnc_sistema('quantidade_casas_decimais');
			
			$queryString 			= $_GET['busca_produto'];
			
			$cliente_id 			= $_GET['cliente_id'];
			$estoque_id 			= $_GET['estoque_id'];
			$codigoProduto 			= $queryString;
			
			$queryString = ($queryString == '') ? '0' : $_GET['busca_produto'];
			
			//identificar produto lido por código de barras de balança toledo
			$codigoLen = strlen($queryString);
			if(substr($queryString,0,1)=='2' && $codigoLen > 10){
				$queryStringCheck = (int)(substr($queryString,1,5));
				$sqlCheckBalanca = mysql_query("SELECT * FROM tblproduto WHERE fldCodigo = '$queryStringCheck' AND fldBalanca = 1");
				if(mysql_num_rows($sqlCheckBalanca) != 0){
					$quantidade = substr($queryString,6,6);
					$queryString = (int) (substr($queryString,1,5));
				}
			}
			###################################################

			$rs = "SELECT tblproduto.*,
			tblproduto_unidade_medida.fldSigla as UnidadeMedida,
			tblcategoria.fldDesconto AS fldDesconto
			FROM tblproduto
			LEFT JOIN tblcategoria ON tblproduto.fldCategoria_Id = tblcategoria.fldId
			LEFT JOIN tblproduto_unidade_medida ON tblproduto_unidade_medida.fldId = tblproduto.fldUN_Medida_Id
			WHERE tblproduto.fldExcluido = '0' AND (tblproduto.fldCodigo = '$queryString' OR tblproduto.fldCodigoBarras = '$queryString')";

			$rows = mysql_query($rs);
			
			if(mysql_num_rows($rows)){
				
				$row = mysql_fetch_array($rows);
	
				if(isset($estoque_id)){
					$rs = "SELECT
					SUM(tblproduto_estoque_movimento.fldEntrada) as fldEntrada,
					SUM(tblproduto_estoque_movimento.fldSaida) as fldSaida
					FROM  tblproduto_estoque_movimento
					WHERE fldEstoque_Id = $estoque_id AND fldProduto_id = ".$row['fldId']."
					GROUP BY fldProduto_Id";
					$rows = mysql_query($rs);
	
					$rowEstoque = mysql_fetch_array($rows);
					$estoque 	= $rowEstoque['fldEntrada'] - $rowEstoque['fldSaida'];
				}
	
				$produto_id 		= $row['fldId'];
				$produto_codigo 	= $row['fldCodigo'];
				$produto 			= rawurlencode(utf8_decode($row['fldNome']));
				$valor 				= $row['fldValorVenda'];
				$valor_compra 		= format_number_out($row['fldValorCompra'],$compraDecimal);
				$unidade_medida 	= $row['UnidadeMedida'];
	
				$estoqueMinimo 		= $row['fldEstoque_Minimo'];
				$tabela_preco 		= 'null';
				$tabela_sigla 		= 'cadastro';
				$estoqueControle	= $row['fldEstoque_Controle'];
				$desconto 			= ($row['fldDesconto'] > 0) ? $row['fldDesconto'] : 0;
				$cliente_valor		= 'null';
				$cliente_produto_preco = fnc_sistema('cliente_produto_preco');

				if(!empty($cliente_id)){
					//pegar a tabela de preco padrao do cliente ou, se nao tiver no cliente cadastrado da tabela padrao do sistema
					$rsTabela 		= mysql_query("SELECT tblcliente.fldTabela_Preco_Id, tblproduto_tabela.* , tblproduto_tabela_preco.fldValor
											 FROM tblcliente
											 LEFT JOIN tblproduto_tabela 		ON tblcliente.fldTabela_Preco_Id 		= tblproduto_tabela.fldId
											 LEFT JOIN tblproduto_tabela_preco 	ON tblproduto_tabela_preco.fldTabela_Id = tblproduto_tabela.fldId AND tblproduto_tabela_preco.fldProduto_Id = $produto_id
											 WHERE tblcliente.fldId = $cliente_id");
					$rowTabela 		= mysql_fetch_array($rsTabela);
	
					if($rowTabela['fldTabela_Preco_Id'] == '0'){
						if($cliente_produto_preco > 0 ){
							$rsPreco = mysql_query("SELECT fldValor FROM tblcliente_produto_preco WHERE fldProduto_Id = $produto_id AND fldCliente_Id = $cliente_id");
							if(mysql_num_rows($rsPreco) > 0){
								$rowPreco 		= mysql_fetch_array($rsPreco);
								$valor 			= $rowPreco['fldValor'];
								$cliente_valor	= 1;
								$tabela_preco 	= $rowTabela['fldTabela_Preco_Id'];
								$tabela_sigla	= $rowTabela['fldSigla'];
							}else{
								$tabela_preco 	= 'null';
								$cliente_valor	= 0;
							}
						}
					}elseif($rowTabela['fldTabela_Preco_Id'] > 0){
						$valor 			= $rowTabela['fldValor'];
						$tabela_preco 	= $rowTabela['fldTabela_Preco_Id'];
						$tabela_sigla	= $rowTabela['fldSigla'];
					}
				}
	
				$valor = format_number_out($valor, $vendaDecimal);
	
				if(substr($codigoProduto,0,1)=='2' && $codigoLen > 10){
					if(mysql_num_rows($sqlCheckBalanca) != 0){
						$quantidade = $quantidade/1000;
						$quantidade = format_number_out($quantidade,$quantidadeDecimal);
					}
					else{$quantidade = format_number_out('1.00',$quantidadeDecimal);}
				}
				
				else{$quantidade = format_number_out('1.00',$quantidadeDecimal);}
				
				###################################################################################################################//
				# BUSCA O CODIGO DO PRODUTO CITADO PELO CLIENTE #
				if($cliente_id != "0" && $cliente_id != '' && $cliente_id != null){
					$query 					= "SELECT * FROM tblproduto_cliente WHERE fldProduto_Id = $produto_id AND fldCliente_Id = $cliente_id";
					$query 					= mysql_query($query);
					if(mysql_num_rows($query) > 0){
						$row 					= mysql_fetch_assoc($query);
						$produto_codigo_cliente = $row['fldProduto_Codigo'];
					}
				}
				//###################################################################################################################//
	
				$xml  = "\t\t<produto_id>$produto_id</produto_id>\n";
				$xml .= "\t\t<produto>$produto</produto>\n";
				$xml .= "\t\t<produto_codigo_cliente>$produto_codigo_cliente</produto_codigo_cliente>\n";
				$xml .= "\t\t<valor>$valor</valor>\n";
				$xml .= "\t\t<valor_compra>$valor_compra</valor_compra>\n";
				$xml .= "\t\t<unidade_medida>$unidade_medida</unidade_medida>\n";
				$xml .= "\t\t<estoque>$estoque</estoque>\n";
				$xml .= "\t\t<estoqueMinimo>$estoqueMinimo</estoqueMinimo>\n";
				$xml .= "\t\t<tabela_preco>$tabela_preco</tabela_preco>\n";
				$xml .= "\t\t<tabela_sigla>$tabela_sigla</tabela_sigla>\n";
				$xml .= "\t\t<estoqueControle>$estoqueControle</estoqueControle>\n";
				$xml .= "\t\t<desconto>$desconto</desconto>\n";
				$xml .= "\t\t<quantidade>$quantidade</quantidade>\n";
				$xml .= "\t\t<cliente_valor>$cliente_valor</cliente_valor>\n";
			}
			else{
				$xml  = "\t\t<error>Produto não encontrado</error>\n";
			}
			
			header('Content-Type: application/xml; charset=utf-8');
			echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
			echo "\t<dados>\n";
			echo $xml;
			echo "\t</dados>\n";
			
	}
	
	//busca do produto pelo codigo (simples)
	elseif(isset($_GET['busca_produto_simples'])){
			$queryString 			= $_GET['busca_produto_simples'];
			$codigoProduto 			= $queryString;
			
			$queryString = ($queryString == "") ? '0' : $_GET['busca_produto_simples'];
			
			$rs = "SELECT tblproduto.*,
			tblproduto_unidade_medida.fldSigla as UnidadeMedida,
			tblcategoria.fldDesconto AS fldDesconto
			FROM tblproduto
			LEFT JOIN tblcategoria ON tblproduto.fldCategoria_Id = tblcategoria.fldId
			LEFT JOIN tblproduto_unidade_medida ON tblproduto_unidade_medida.fldId = tblproduto.fldUN_Medida_Id
			WHERE tblproduto.fldExcluido = '0' 
      AND (tblproduto.fldCodigo = '$queryString' OR tblproduto.fldCodigoBarras = '$queryString')";

			$rows = mysql_query($rs);
			
			if(mysql_num_rows($rows)){
				$row = mysql_fetch_array($rows);
				$produto_id 		= $row['fldId'];
				$produto_codigo 	= $row['fldCodigo'];
				$produto 			= utf8_encode(utf8_decode($row['fldNome']));
				$xml  = "\t\t<produto_id>$produto_id</produto_id>\n";
				$xml .= "\t\t<produto>$produto</produto>\n";
			}
			else{ $xml  = "\t\t<error>$rs</error>\n"; }
			
			header('Content-Type: application/xml; charset=utf-8');
			echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
			echo "\t<dados>\n";
			echo $xml;
			echo "\t</dados>\n";
			
	}
	
	//busca de produto para OP
	elseif(isset($_GET['busca_produto_op'])){

		$queryString = $_GET['busca_produto_op'];
		
		$rowComponente = mysql_num_rows(mysql_query("SELECT * FROM tblproduto_componente WHERE fldProduto_Id = $queryString"));
		
		if($rowComponente > 0){ //com componente
			$rs =	"SELECT
						tblproduto.fldId,
						tblproduto.fldCodigo,
						tblproduto.fldNome,
						tblproduto.fldUN_Medida_Id,
						(SELECT fldSigla FROM tblproduto_unidade_medida WHERE fldId = tblproduto.fldUN_Medida_Id) AS fldUM,
						(SELECT SUM(tblproduto_estoque_movimento.fldEntrada - tblproduto_estoque_movimento.fldSaida) FROM tblproduto_estoque_movimento WHERE fldProduto_Id = tblproduto.fldId) AS fldEstoqueQuantidade,
						CONVERT((SELECT GROUP_CONCAT(fldProduto_Componente_Id ORDER BY fldProduto_Componente_Id) FROM tblproduto_componente WHERE fldProduto_Id = tblproduto.fldId GROUP BY fldProduto_Id), CHAR) AS fldComponentes,
						CONVERT((SELECT GROUP_CONCAT(fldComponente_Qtd ORDER BY fldProduto_Id) FROM tblproduto_componente WHERE fldProduto_Id = tblproduto.fldId GROUP BY fldProduto_Id), CHAR) AS fldComponentes_Qtd
					FROM tblproduto WHERE fldId = '$queryString' OR fldCodigo = '$queryString'";
					
			$rSQL = mysql_query($rs);
			$row  = mysql_fetch_assoc($rSQL);
			
			$produto_id 				= $row['fldId'];
			$produto_nome				= $row['fldNome'];
			$produto_UM					= $row['fldUM'];
			$produto_estocado			= $row['fldEstoqueQuantidade'];
			$produto_componentes 		= $row['fldComponentes'];
			$produto_componentes_qtd	= $row['fldComponentes_Qtd'];
			
			$rsCompDesc 	= mysql_query("SELECT GROUP_CONCAT(fldNome ORDER BY fldId ASC) AS fldNome FROM tblproduto WHERE fldId IN ($produto_componentes)");
			$rsCompUM 		= mysql_query("SELECT GROUP_CONCAT(fldSigla ORDER BY tblproduto.fldId) AS fldSigla FROM tblproduto_unidade_medida INNER JOIN tblproduto ON tblproduto.fldUN_Medida_Id = tblproduto_unidade_medida.fldId WHERE tblproduto.fldId IN ($produto_componentes)");
			$rowCompDesc 	= mysql_fetch_assoc($rsCompDesc);
			$rowCompUM 		= mysql_fetch_assoc($rsCompUM);
			
			$produto_componentes_desc	= $rowCompDesc['fldNome'];
			$produto_componentes_um		= $rowCompUM['fldSigla'];
		}
		else{ //sem componente
			$rs =	"SELECT
						tblproduto.fldId,
						tblproduto.fldCodigo,
						tblproduto.fldNome,
						tblproduto.fldUN_Medida_Id,
						(SELECT fldSigla FROM tblproduto_unidade_medida WHERE fldId = tblproduto.fldUN_Medida_Id) AS fldUM,
						(SELECT SUM(tblproduto_estoque_movimento.fldEntrada - tblproduto_estoque_movimento.fldSaida) FROM tblproduto_estoque_movimento WHERE fldProduto_Id = tblproduto.fldId) AS fldEstoqueQuantidade
					FROM tblproduto WHERE fldId = '$queryString' OR fldCodigo = '$queryString'";
					
			$rSQL = mysql_query($rs);
			$row  = mysql_fetch_assoc($rSQL);
			
			$produto_id 				= $row['fldId'];
			$produto_nome				= $row['fldNome'];
			$produto_UM					= $row['fldUM'];
			$produto_estocado			= $row['fldEstoqueQuantidade'];
			$produto_componentes 		= '';
			$produto_componentes_qtd	= '';
			$produto_componentes_desc	= '';
			$produto_componentes_um		= '';
		}

		$xml  = "\t\t<produto_id>$produto_id</produto_id>\n";
		$xml .= "\t\t<produto>$produto_nome</produto>\n";
		$xml .= "\t\t<produto_um>$produto_UM</produto_um>\n";
		$xml .= "\t\t<produto_estocado>$produto_estocado</produto_estocado>\n";
		$xml .= "\t\t<produto_componentes>$produto_componentes</produto_componentes>\n";
		$xml .= "\t\t<produto_componentes_qtd>$produto_componentes_qtd</produto_componentes_qtd>\n";
		$xml .= "\t\t<produto_componentes_desc>$produto_componentes_desc</produto_componentes_desc>\n";
		$xml .= "\t\t<produto_componentes_um>$produto_componentes_um</produto_componentes_um>\n";

		header('Content-Type: application/xml; charset=utf-8');
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
		echo "\t<dados>\n";
		echo $xml;
		echo "\t</dados>\n";

	}

	//ao trocar a tabela de preco na venda, volta o valor cadastrado
	elseif(isset($_GET['sel_pedido_tabela_preco'])){
			$queryString 	= $_GET['sel_pedido_tabela_preco'];
			$produto_id 	= $_GET['produto_id'];
			$cliente_id 	= $_GET['cliente_id'];
				
			$rowProduto 	= mysql_fetch_array(mysql_query("SELECT fldValorVenda FROM tblproduto	WHERE fldId = $produto_id"));
			$rowTabela 		= mysql_fetch_array(mysql_query("SELECT fldSigla FROM tblproduto_tabela WHERE fldId = $queryString"));
			$tabela_sigla 	= $rowTabela['fldSigla'];
			
			//TABELA DE PRECO DE ACORDO COM CLIENTE = 0
			if($queryString == 'null'){
				$valor = $rowProduto['fldValorVenda'];
			}elseif($queryString == 0){
				$rsPreco 	= mysql_query("SELECT fldValor FROM tblcliente_produto_preco WHERE fldProduto_Id = $produto_id AND fldCliente_Id = $cliente_id");
				$rowPreco	= mysql_fetch_array($rsPreco);
				$valor 	 	= $rowPreco['fldValor'];
			}elseif($queryString > 0){
				$valor 		= produtoPreco($produto_id, $queryString, 'false');
			}
			
			$xml = "\t\t<valor>$valor</valor>\n";
			$xml .= "\t\t<tabela_sigla>$tabela_sigla</tabela_sigla>\n";

			header('Content-Type: application/xml; charset=utf-8');
			echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
			echo "\t<dados>\n";
			echo $xml;
			echo "\t</dados>\n";
	}
	//ao selecionar produto, buscar fardo 
	elseif(isset($_POST['sel_produto_fardo'])){
			$queryString = $_POST['sel_produto_fardo'];
			
			$rs = "SELECT *	FROM tblproduto_fardo WHERE fldProduto_Id = '$queryString' AND fldExcluido = 0";
			$rs = mysql_query($rs);
			
?>			<option value="0">selecionar</option>
<?			while ($rows = mysql_fetch_array($rs)){
?>				<option value="<?=$rows['fldId']?>"><?=$rows['fldCodigo']?></option>
<?			}

	}
	//ao selecionar produto, buscar fardos 
	elseif(isset($_POST['sel_produto_fardo_qtd'])){
			$queryString = $_POST['sel_produto_fardo_qtd'];
			
			$rs = "SELECT SUM(tblpedido_item.fldQuantidade * (tblpedido_item.fldExcluido * -1 + 1)) AS fldSaida, 
			tblproduto_fardo.fldQuantidade
			FROM tblproduto_fardo 
			LEFT JOIN tblpedido_item ON tblproduto_fardo.fldId = tblpedido_item.fldFardo_Id
			WHERE tblproduto_fardo.fldId = $queryString AND tblproduto_fardo.fldExcluido = '0' 
			GROUP BY tblproduto_fardo.fldId
			ORDER BY tblproduto_fardo.fldId DESC ";
			$rs	= mysql_query($rs);
			$row = mysql_fetch_array($rs);
			
			$quantidade = $row['fldQuantidade'] - $row['fldSaida'];
			$xml = "\t\t<quantidade>$quantidade</quantidade>\n";

			header('Content-Type: application/xml; charset=utf-8');
			echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
			echo "\t<dados>\n";
			echo $xml;
			echo "\t</dados>\n";
	}
	elseif (isset($_POST['busca_transportador'])){
			$queryString = $_POST['busca_transportador'];
			$s = "SELECT fldNome FROM tbltransportador WHERE fldId = '$queryString'";
			$s1 = mysql_query($s);
			
			while ($l = mysql_fetch_array($s1)){
				echo $l['fldNome'];
			}
	}
	elseif(isset($_POST['sel_categoria_id'])){
			$queryString = $_POST['sel_categoria_id'];
			
			$rs = "SELECT * FROM tblsubcategoria WHERE fldExcluido = '0' and  fldCategoria_Id = $queryString";
			$rs = mysql_query($rs);
			
?>			<option value="0">selecionar</option>
<?			while($rows = mysql_fetch_array($rs)){
?>				<option value="<?=$rows['fldId'] ?>"><?=$rows['fldNome'] ?></option>
<?			}
	}
	elseif(isset($_POST['sel_veiculo_id'])){
			$queryString 	= $_POST['sel_veiculo_id'];
			$veiculo_id 	= (isset($_SESSION['cliente_placa_id'])) ? $_SESSION['cliente_placa_id'] : $_POST['veiculo_id'];
			$exibir = fnc_sistema('venda_exibir_veiculo');
			
			$order = ($exibir == 'placa') ? "fldPlaca" : "fldVeiculo, fldPlaca";
			
			$rs = "SELECT * FROM tblcliente_veiculo WHERE fldCliente_Id = $queryString ORDER BY $order";
			$rs = mysql_query($rs);
			
?>			<option value="0">selecionar</option>
<?			if($queryString > 0){
				echo "<option value='novo' style='font-weight:bold;background-color:#FC0'>novo veiculo</option>";				
			}
			while($rows = mysql_fetch_array($rs)){
?>				<option value="<?=$rows['fldId'] ?>" <?=($rows['fldId'] == $veiculo_id)? "selected='selected'" : ''?>><?= ($exibir == 'placa') ? $rows['fldPlaca'] : $rows['fldVeiculo']." [".$rows['fldPlaca']."]"?></option>
<?			}
			unset($_SESSION['cliente_placa_id']);
			
	}#SELECIONANDO O RESTANDO DOS DADOS DO VEICULO PARA VENDA
	elseif(isset($_GET['sel_veiculo_dados'])){
			$veiculo_id 	= $_GET['sel_veiculo_dados'];
			
			$rs = "SELECT fldAno, fldChassi FROM tblcliente_veiculo WHERE fldId = $veiculo_id ";
			$rs = mysql_query($rs);
			$row = mysql_fetch_array($rs);
						
			$ano 	= $row['fldAno'];
			$chassi	= $row['fldChassi'];

			$xml  = "\t\t<veiculo_ano>$ano</veiculo_ano>\n";
			$xml .= "\t\t<veiculo_chassi>$chassi</veiculo_chassi>\n";

			header('Content-Type: application/xml; charset=utf-8');
			echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
			echo "\t<dados>\n";
			echo $xml;
			echo "\t</dados>\n";
			
	}
	// ## AO SELECIONAR CLIENTE MOSTRAR AS CONTAS DO CLIENTE NA VENDA
	elseif(isset($_POST['sel_conta_bancaria_id'])){
			$queryString = $_POST['sel_conta_bancaria_id'];
			
			//$rs = "SELECT * FROM tblcliente_contabancaria WHERE fldCliente_Id = " . $queryString;
			$rs = "SELECT tblcliente_contabancaria.fldConta, tblbanco_codigo.fldBanco, tblcliente_contabancaria.fldId as fldContaId
					FROM tblcliente_contabancaria INNER JOIN tblbanco_codigo ON 
					tblcliente_contabancaria.fldBanco_Numero = tblbanco_codigo.fldNumero WHERE tblcliente_contabancaria.fldCliente_Id = $queryString";
					
			$rs = mysql_query($rs);
				
?>			<option value="0">selecionar</option>
<?			while($row = mysql_fetch_array($rs)){
?>				<option value="<?=$row['fldContaId']?>" <?=($row['fldContaId'] == $_POST['contaId'])? "selected='selected'" : ''?>><?=$row['fldBanco']." - ".$row['fldConta']?></option>
<?			}
	}
	
	// ## AO SELECIONAR A CONTA DO CLIENTE MOSTRAR O RESTANTEDE INFORMACOES
	elseif(isset($_POST['sel_conta_bancaria_dados'])){
			$queryString = $_POST['sel_conta_bancaria_dados'];
			
			$rs = "SELECT * FROM tblcliente_contabancaria WHERE fldId = $queryString";
			
			$rs	 = mysql_query($rs);
			$row = mysql_fetch_array($rs);
			
			$agencia = $row['fldAgencia'];
			$titular = $row['fldTitular'];
			
			$xml  = "\t\t<agencia>$agencia</agencia>\n";
			$xml .= "\t\t<titular>$titular</titular>\n";

			header('Content-Type: application/xml; charset=utf-8');
			echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
			echo "\t<dados>\n";
			echo $xml;
			echo "\t</dados>\n";
	}
	
	// ################################################## //
	elseif (isset($_GET['banco_codigo'])){
			$queryString = $_GET['banco_codigo'];
		
			$rs = "SELECT * FROM tblbanco_codigo WHERE fldNumero = $queryString";
			$rows = mysql_query($rs);
			$row = mysql_fetch_array($rows);
			$nome = $row['fldBanco'];
			
			$xml = "\t\t<nome>$nome</nome>\n";
			header('Content-Type: application/xml; charset=utf-8');
			echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
			echo "\t<dados>\n";
			echo $xml;
			echo "\t</dados>\n";			

	}
	
	//VERIFICA SE JA EXISTE PRODUTO COM ESSE NOME OU CODIGO
	elseif(isset($_GET['produto_codigo'])){
			$produtoId 	= $_GET['produtoId'];
			$produto 	= rawurlencode(utf8_decode($_GET['produto']));
			$codigo 	= $_GET['produto_codigo'];
			$duplica 	= $_GET['duplica'];
			
			if($produtoId != '0' && $duplica != '1'){
				$filtro = " AND fldId != $produtoId";
			}
			
			$rsCodigo 	= mysql_query("SELECT * FROM tblproduto WHERE fldExcluido = '0' AND fldCodigo = '$codigo' $filtro");
			$rsNome 	= mysql_query("SELECT * FROM tblproduto WHERE fldExcluido = '0' AND fldNome = '$produto' $filtro");
			
			$codigo = 'ok';
			$nome = 'ok';
			
			if(mysql_num_rows($rsCodigo)){
				$codigo = 'erro';
			}
			if(mysql_num_rows($rsNome)){
				$nome = 'erro';
			}
			
			//VERIFICAR MODO DE GERAR CODIGO DO PRODUTO
			$categoriaId 	= $_GET['categoriaId'];
			$marcaId 		= $_GET['marcaId'];
			$gerarCod		= fnc_sistema('produto_codigo');
			$categoria 		= 'ok';
			$marca			= 'ok';
	
			if($gerarCod == 'categoria'){
				if($categoriaId == 0){
					$categoria = 'erro';
				}
			}elseif($gerarCod == 'marca'){
				if($marcaId == 0){
					$marca = 'erro';
				}
			}
			
			$xml  = "\t\t<codigo>$codigo</codigo>\n";
			$xml .= "\t\t<nome>$nome</nome>\n";
			$xml .= "\t\t<categoria>$categoria</categoria>\n";
			$xml .= "\t\t<marca>$marca</marca>\n";

			header('Content-Type: application/xml; charset=utf-8');
			echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
			echo "\t<dados>\n";
			echo $xml;
			echo "\t</dados>\n";
	}

	//COTAÇÃO COMPRA ######################################################################################################//
	elseif (isset($_POST['compra_cotacao'])){ //COTACAO COMPRA
		$queryString = $_POST['compra_cotacao'];
		$url = $_POST['url'];
		$cotacaoId = $_POST['cotacaoId'];
		
		$rs = "SELECT tblcompra_cotacao_fornecedor.*, tblfornecedor.fldNomeFantasia
													   FROM tblcompra_cotacao_fornecedor 
													   LEFT JOIN tblfornecedor 
													   ON tblcompra_cotacao_fornecedor.fldFornecedor_Id = tblfornecedor.fldId
													   WHERE tblcompra_cotacao_fornecedor.fldCotacao_Item_Id =".$queryString;
		$rows = mysql_query($rs);
		$linha = "row";
		while($row = mysql_fetch_array($rows)){
?>			<tr class="<?=$linha;?>">
                <td style='width:5px'></td>
                <td style='width:80px'><?=str_pad($row['fldFornecedor_Id'], 8, '0', STR_PAD_LEFT)?></td>
                <td style='width:250px'><?=$row['fldNomeFantasia']?></td>
                <td style='width:250px'><?=$row['fldObservacao']?></td>
                <td style='width:100px'><input style="width:100px; text-align:right" class="cotacao_fornecedor_valor" type='text' id='txt_valor_<?=$row['fldId']?>' name='txt_valor_<?=$row['fldId']?>' value='<?=format_number_out($row['fldValor'])?>' /></td>
                <td style='width:20px'><a class="delete" href="<?=$url?>&fornecedorDel=<?=$row['fldId']?>&itemId=<?=$row['fldCotacao_Item_Id']?>"></a></td>
            </tr>
<?			$linha = ($linha == "row" ?	$linha = "dif-row": $linha = "row");
		}
	}
	//COTACAO COMPRA ####################################################################################################//
	elseif(isset($_POST['cotacao_fornecedor_valor'])){ 
		$queryString = $_POST['cotacao_fornecedor_valor'];
		$valor = format_number_in($_POST['valor']);
		
		$rs = "UPDATE tblcompra_cotacao_fornecedor SET fldValor = $valor WHERE fldId= $queryString";
		$rows = mysql_query($rs);
			
	}elseif(isset($_POST['sel_pagamento_id'])){ //COTACAO COMPRA
		$queryString = $_POST['sel_pagamento_id'];
		
		$rs = "SELECT * FROM tblpagamento_tipo WHERE fldId = $queryString";
		$rows = mysql_query($rs);
		$row = mysql_fetch_array($rows);
		
		$pagamentoTipo = $row['fldTipo'];
		
		$xml = "\t\t<pagamentoTipo>$pagamentoTipo</pagamentoTipo>\n";
		header('Content-Type: application/xml; charset=utf-8');
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
		echo "\t<dados>\n";
		echo $xml;
		echo "\t</dados>\n";			
	}
	//##################################################################
	//VERIFICAR SE TABELA DE PREÇO OU SIGLA JA EXISTE 
	elseif(isset($_POST['tabelaPrecoNome'])){
		
		$tabelaId 	= $_POST['tabelaId'];
		$nome 		= $_POST['tabelaPrecoNome'];
		$sigla 		= $_POST['tabelaSigla'];
			
		if($tabelaId != '') {
			$filtro = " AND fldId != $tabelaId";
		}
		
		$rsSigla 	= mysql_query("SELECT * FROM tblproduto_tabela WHERE fldExcluido = '0' AND fldSigla = '$sigla'".$filtro);
		$rsNome 	= mysql_query("SELECT * FROM tblproduto_tabela WHERE fldExcluido = '0' AND fldNome = '$nome'".$filtro);
		
		$sigla = 'ok';
		$nome = 'ok';
		
		if(mysql_num_rows($rsSigla)){
			$sigla = 'erro';
		}
		if(mysql_num_rows($rsNome)){
			$nome = 'erro';
		}
		
		$xml  = "\t\t<sigla>$sigla</sigla>\n";
		$xml .= "\t\t<nome>$nome</nome>\n";

		header('Content-Type: application/xml; charset=utf-8');
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
		echo "\t<dados>\n";
		echo $xml;
		echo "\t</dados>\n";			
	}
	//verificar qual o nome do estoque/*
	elseif($_GET['produto_estoque_id']){
		$queryString 	= $_GET['produto_estoque_id'];
		$produtoId 		= $_GET['produtoId'];
	
		$rs = "SELECT 
		SUM(tblproduto_estoque_movimento.fldEntrada) as fldEntrada,
		SUM(tblproduto_estoque_movimento.fldSaida) as fldSaida
		FROM tblproduto_estoque_movimento RIGHT JOIN tblproduto_estoque ON tblproduto_estoque_movimento.fldEstoque_Id = tblproduto_estoque.fldId
		WHERE tblproduto_estoque_movimento.fldEstoque_Id = $queryString AND tblproduto_estoque_movimento.fldProduto_Id = $produtoId
		GROUP BY tblproduto_estoque_movimento.fldProduto_Id";
		$rows = mysql_query($rs);
		$row = mysql_fetch_array($rows);

		$estoque = $row['fldEntrada'] - $row['fldSaida'];
		
		$xml = "\t\t<estoque>$estoque</estoque>\n";
		header('Content-Type: application/xml; charset=utf-8');
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
		echo "\t<dados>\n";
		echo $xml;
		echo "\t</dados>\n";			
	}
	elseif(isset($_GET['totalPedido'])){ //COTACAO COMPRA
		$totalPedido 	= $_GET['totalPedido'];
		$cliente_id 	= $_GET['cliente_id'];
		
		$rsCliente		= mysql_query("SELECT * FROM tblcliente WHERE fldId = $cliente_id");
		$rowCliente 	= mysql_fetch_array($rsCliente);
		$limite_credito = $rowCliente['fldCreditoLimite'];
		$rsParcelaBaixa = mysql_query("SELECT 
			SUM(tblpedido_parcela_baixa.fldValor * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldSomaBaixa
			FROM 
			(tblpedido_parcela LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id)
			INNER JOIN tblpedido ON tblpedido.fldId = tblpedido_parcela.fldPedido_Id
			RIGHT JOIN tblcliente ON tblpedido.fldCliente_Id = tblcliente.fldId
			WHERE tblpedido.fldCliente_Id = $cliente_id
			AND tblpedido_parcela.fldStatus = '1' AND tblpedido_parcela.fldExcluido = 0
			");
			
		$rowParcelaBaixa = mysql_fetch_array($rsParcelaBaixa);
		$rsParcela = mysql_query("SELECT 
			SUM(tblpedido_parcela.fldValor * (tblpedido_parcela.fldExcluido * -1 + 1)) as fldSomaParcela
			FROM 
			tblpedido
			INNER JOIN tblpedido_parcela ON tblpedido.fldId = tblpedido_parcela.fldPedido_Id
			RIGHT JOIN tblcliente ON tblpedido.fldCliente_Id = tblcliente.fldId
			WHERE tblpedido.fldCliente_Id = $cliente_id
			AND tblpedido_parcela.fldStatus = '1' AND tblpedido_parcela.fldExcluido = 0
			");
			
		$rowParcela = mysql_fetch_array($rsParcela);
		$parcelas_abertas = $rowParcela['fldSomaParcela'] - $rowParcelaBaixa['fldSomaBaixa']; //parcelas em aberto.
		$soma_aberto = $totalPedido + $parcelas_abertas; //soma o total do pedido, com o valor das parcelas abertas.
		
		if($soma_aberto > $limite_credito){
			$excedido = 1;
			$valorExcedido = $soma_aberto - $limite_credito;
		}else{
			$excedido = 0;
		}

		$xml = "\t\t<valorExcedido>$valorExcedido</valorExcedido>\n";
		$xml .= "\t\t<parcelasEmAberto>$parcelas_abertas</parcelasEmAberto>\n";
		$xml .= "\t\t<limiteCredito>$limite_credito</limiteCredito>\n";
		$xml .= "\t\t<excedido>$excedido</excedido>\n";
	
		header("Content-Type: application/xml; charset=utf-8");
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
		echo "\t<dados>\n";
		echo $xml;
		echo "\t</dados>\n";
	}
	/*
	//CHEQUES - INSERIR ######################################################################################################//
	elseif (isset($_POST['cheque_inserir'])){ 
	
		$queryString 	= $_POST['cheque_inserir'];
		$timestamp 		= $_SESSION['ref_timestamp'];
		$telaId 		= $_POST['telaId'];
		$refId 			= $_POST['referenciaId'];
		
		$filtroCheque = '';
		
		if($refId != 0){
			if($telaId == '8, 5'){//pedido
				$filtroCheque .= " or (fldOrigem in (".$refId.") and fldTela_Id in (8, 5))";
			}elseif($telaId == '9, 6'){//compra
				$filtroCheque .= " or (fldDestino in (".$refId.") and fldTela_Id in (9, 6))";
			}elseif($telaId == 10){//conta programada
				$filtroCheque .= " or (fldDestino in (".$refId.") and fldTela_Id in (10))";
			}
		}
		
		if(isset($_SESSION['contas_pagar_baixa'])){
			$filtro = ", fldDestino = '".$_SESSION['contas_pagar_baixa']."', fldTela_Id = 10";
		}
		
		mysql_query("UPDATE tblcheque SET fldTimeStamp = '". $_SESSION['ref_timestamp'] ."' ".$filtro." WHERE fldId =".$queryString);
		$rs 		= "SELECT * FROM tblcheque WHERE fldTimeStamp = '". $_SESSION['ref_timestamp'] ."' ".$filtroCheque." ORDER BY fldId DESC";	
		$rows 		= mysql_query($rs);
		while($row 	= mysql_fetch_array($rows)){
?>			
			<tr class="tr_cheque_listar" id="<?=$row['fldId']?>">
                <td style="width:40px"><?=$row['fldBanco']?></td>
                <td style="width:50px"><?=$row['fldAgencia']?></td>
                <td style="width:70px"><?=$row['fldCheque']?></td>
                <td style="width:70px; text-align:right"><?=format_number_out($row['fldValor'])?></td>
                <td style="width:70px"><?=format_date_out($row['fldVencimento'])?></td>
                <td style="width:100px; text-align:left"><?=$row['fldNome']?></td>
            </tr> 
<?		}	
	}//CHEQUES - REMOVER ######################################################################################################//
	elseif (isset($_POST['cheque_remover'])){ 
	
		$queryString = $_POST['cheque_remover'];
		
		mysql_query("UPDATE tblcheque SET fldTimeStamp = '', fldDestino_Id = '0' WHERE fldId =".$queryString);
		$rs 		= "SELECT * FROM tblcheque WHERE fldTimeStamp = '' AND fldDestino_Id = '0' ORDER BY fldId DESC";
		$rows 		= mysql_query($rs);
		while($row 	= mysql_fetch_array($rows)){
?>			
			<tr class="tr_cheque_listar" id="<?=$row['fldId']?>">
                <td style="width:40px"><?=$row['fldBanco']?></td>
                <td style="width:50px"><?=$row['fldAgencia']?></td>
                <td style="width:70px"><?=$row['fldCheque']?></td>
                <td style="width:70px; text-align:right"><?=format_number_out($row['fldValor'])?></td>
                <td style="width:70px"><?=format_date_out($row['fldVencimento'])?></td>
                <td style="width:100px; text-align:left"><?=$row['fldNome']?></td>
            </tr> 
<?		}	
	}//CHEQUES - EDITAR ######################################################################################################//
	elseif(isset($_POST['cheque_editar'])){ 
	
		$queryString = $_POST['cheque_editar'];
		
		$rs 		= "SELECT * FROM tblcheque WHERE fldId = ".$queryString;		
		$rows 		= mysql_query($rs);
		$row 		= mysql_fetch_array($rows);
		
		$banco 		= $row['fldBanco'];
		$agencia 	= $row['fldAgencia'];
		$conta 		= $row['fldConta'];
		$cheque		= $row['fldCheque'];
		$valor 		= format_number_out($row['fldValor']);
		$vencimento = format_date_out($row['fldVencimento']);
		$nome 		= $row['fldNome'];
		$cpfCNPJ 	= $row['fldCPF_CNPJ'];
		$entidade 	= $row['fldEntidade'];
			
		$xml  = "\t\t<banco>$banco</banco>\n";
		$xml .= "\t\t<agencia>$agencia</agencia>\n";
		$xml .= "\t\t<conta>$conta</conta>\n";
		$xml .= "\t\t<cheque>$cheque</cheque>\n";
		$xml .= "\t\t<valor>$valor</valor>\n";
		$xml .= "\t\t<vencimento>$vencimento</vencimento>\n";
		$xml .= "\t\t<nome>$nome</nome>\n";
		$xml .= "\t\t<cpfCNPJ>$cpfCNPJ</cpfCNPJ>\n";
		$xml .= "\t\t<entidade>$entidade</entidade>\n";

		header('Content-Type: application/xml; charset=utf-8');
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
		echo "\t<dados>\n";
		echo $xml;
		echo "\t</dados>\n";
		
	}
	*/
	elseif($_POST['action'] == 'sel_uf'){
		$uf = $_POST['uf'];
		$rsMunicipio = mysql_query("SELECT * FROM tblibge_municipio WHERE fldUF_Codigo = '$uf'");
		while($rowMunicipio = mysql_fetch_array($rsMunicipio)){
			echo '<option value="'.$rowMunicipio['fldCodigo'].'">'.$rowMunicipio['fldNome'].'</option>';
		}
	}
	elseif(isset($_POST['verificar_endereco_padrao'])){
		$rsEndereco = mysql_query("SELECT * FROM tblcliente_endereco_entrega WHERE fldPadrao = 1");
		$resultado = mysql_num_rows($rsEndereco);
		echo $resultado;
	}# VERIFICAR ESTOQUE DE PRODUTO ######################################################################################################//
	elseif(isset($_GET['produto_estoque_confere'])){ 
	
		$produtoId 	= $_GET['produto_estoque_confere'];
		$estoqueId 	= $_GET['estoque_id'];
		$itemId		= $_GET['item_id'];
		$pedido_id	= $_GET['pedido_id'];
		
		$filtro = ($pedido_id > 0) ? "AND tblpedido.fldId = $pedido_id" : '';
		
		$rs = 	"SELECT tblproduto.fldEstoque_Controle, 
				SUM(tblproduto_estoque_movimento.fldEntrada - tblproduto_estoque_movimento.fldSaida) as fldEstoqueSaldo
				FROM tblproduto
				LEFT JOIN tblproduto_estoque_movimento ON tblproduto_estoque_movimento.fldProduto_Id = tblproduto.fldId
				WHERE tblproduto.fldId = $produtoId AND fldEstoque_Id = $estoqueId GROUP BY tblproduto_estoque_movimento.fldProduto_Id ";
		
		$rows 			 = mysql_query($rs);
		$row 			 = mysql_fetch_array($rows);
		$estoqueSaldo	 = $row['fldEstoqueSaldo'];
		$estoqueControle = $row['fldEstoque_Controle'];
		$totalInserido	 = 0;
		
		if($pedido_id > 0){
			/*
			$rs = 	"SELECT SUM(fldQuantidade * (fldExcluido * -1 + 1)) AS fldTotalItemVenda FROM tblpedido_item 
					WHERE fldProduto_Id = $produtoId AND fldPedido_Id = $pedido_id AND fldEntregue = 1";
			*/
			$rs = "SELECT SUM(fldEntrada - fldSaida) as fldQuantidade FROM tblproduto_estoque_movimento WHERE fldPedido_Item = $itemId AND fldEstoque_Id = $estoqueId GROUP BY fldEstoque_Id";
			
			$rows 				 = mysql_query($rs);
			$row 			 	 = mysql_fetch_array($rows);
			$totalInseridoAntigo = $row['fldQuantidade'];
		}
		
		$xml = "\t\t<estoqueSaldo>$estoqueSaldo</estoqueSaldo>\n";
		$xml .= "\t\t<estoqueControle>$estoqueControle</estoqueControle>\n";
		$xml .= "\t\t<totalInseridoAntigo>$totalInseridoAntigo</totalInseridoAntigo>\n";
		header('Content-Type: application/xml; charset=utf-8');
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
		echo "\t<dados>\n";
		echo $xml;
		echo "\t</dados>\n";	
		
	}# VERIFICAR DUPLICIDADE DE CODIGO DIGITADO DO CLIENTE ######################################################################################################//
	elseif(isset($_GET['verificar_cliente_codigo'])){ 
		
		$filtro = '';
		if($_GET['cliente_id'] > 0){
			$cliente_id = $_GET['cliente_id'];
			$filtro = "AND fldId != $cliente_id";
		}
		
		$Codigo 	= $_GET['codigo'];
		$rsCodigo 	= mysql_query("SELECT * FROM tblcliente WHERE fldCodigo = '$Codigo' $filtro");
		echo mysql_error();
		if(mysql_num_rows($rsCodigo)){
			$codigoErro = 1;
		}else{
			$codigoErro = 0;
		}
		
		$xml = "\t\t<codigoErro>$codigoErro</codigoErro>\n";
		header('Content-Type: application/xml; charset=utf-8');
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
		echo "\t<dados>\n";
		echo $xml;
		echo "\t</dados>\n";	
		
	}elseif(isset($_GET['verifica_pagamento_sigla'])){ 
		
		$filtro = '';
		if($_GET['pagtoId'] > 0){
			$pagtoId = $_GET['pagtoId'];
			$filtro = "AND fldId != $pagtoId";
		}
		
		$sigla 		= $_GET['sigla'];
		$rsSigla 	= mysql_query("SELECT * FROM tblpagamento_tipo WHERE fldSigla = '$sigla' AND fldExcluido = 0 $filtro");
		echo mysql_error();
		if(mysql_num_rows($rsSigla)){
			$siglaErro = 1;
		}else{
			$siglaErro = 0;
		}
		
		$xml = "\t\t<siglaErro>$siglaErro</siglaErro>\n";
		header('Content-Type: application/xml; charset=utf-8');
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
		echo "\t<dados>\n";
		echo $xml;
		echo "\t</dados>\n";	
		
	}elseif(isset($_GET['busca_simples_produto'])){
		$vendaDecimal 			= fnc_sistema('venda_casas_decimais');

		$codigo 		= ltrim($_GET['busca_simples_produto'], "0");
		$codigoOriginal = $_GET['busca_simples_produto'];
		//agora pesquisa com e sem zeros à esquerda...
		$rsProduto 		= mysql_query("SELECT tblproduto.fldId, tblproduto.fldNome, tblproduto.fldValorVenda FROM tblproduto WHERE (fldCodigo = '$codigo' OR fldCodigo = '$codigoOriginal') AND fldExcluido = 0");
		$rowProduto 	= mysql_fetch_array($rsProduto);
		echo mysql_error();
		
		$nomeProduto 	= $rowProduto['fldNome'];
		$valorProduto 	= format_number_out($rowProduto['fldValorVenda'],$vendaDecimal);
		$idProduto 		= $rowProduto['fldId'];
		
		$xml  = "\t\t<nomeProduto>$nomeProduto</nomeProduto>\n";
		$xml .= "\t\t<idProduto>$idProduto</idProduto>\n";
		$xml .= "\t\t<valorProduto>$valorProduto</valorProduto>\n";
		header('Content-Type: application/xml; charset=utf-8');
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
		echo "\t<dados>\n";
		echo $xml;
		echo "\t</dados>\n";	
		
	}elseif(isset($_GET['sel_funcionario_funcao_comissao'])){ #TELA DE FUNCIONARIO - BUSCAR COMISSAO PRE CADASTRADA
		
		$funcao_id 	= $_GET['sel_funcionario_funcao_comissao'];
		$rsFuncao 	= mysql_query("SELECT fldComissao, fldTipo FROM tblfuncionario_funcao WHERE fldId = $funcao_id ");
		$rowFuncao 	= mysql_fetch_array($rsFuncao);
		echo mysql_error();
		
		$comissao 	= format_number_out($rowFuncao['fldComissao']);
		
		$xml = "\t\t<comissao>$comissao</comissao>\n";
		header('Content-Type: application/xml; charset=utf-8');
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
		echo "\t<dados>\n";
		echo $xml;
		echo "\t</dados>\n";	
		
	}elseif(isset($_GET['sel_funcionario_funcao'])){ #TELA DE FUNCIONARIO - BUSCAR COMISSAO PRE CADASTRADA
		
		$funcao_id 	= $_GET['sel_funcionario_funcao'];
		$rowFuncao 	= mysql_fetch_array(mysql_query("SELECT fldTipo FROM tblfuncionario_funcao WHERE fldId = $funcao_id "));
		$tipo 		= $rowFuncao['fldTipo'];
		
		$rsSelect 	= mysql_query("SELECT * FROM tblfuncionario_funcao WHERE fldTipo != $tipo ");
		echo '<option value="0">selecionar</option>';
		while($rowSelect 	= mysql_fetch_array($rsSelect)){
			echo '<option value="'.$rowSelect['fldId'].'">'.$rowSelect['fldFuncao'].'</option>';
		}	
	}
	
	elseif(isset($_GET['busca_cheque'])){ #TELA DE FUNCIONARIO - BUSCAR COMISSAO PRE CADASTRADA
		
		$cheque_id 	= $_GET['busca_cheque'];
		$rowCheque 	= mysql_fetch_array(mysql_query("SELECT * FROM tblcheque WHERE fldId = $cheque_id "));

		$correntista= $rowCheque['fldNome'];
		$entidade 	= $rowCheque['fldEntidade'];
		$numero 	= $rowCheque['fldNumero'];
		$banco 		= $rowCheque['fldBanco'];
		$conta 		= $rowCheque['fldConta'];
		$agencia 	= $rowCheque['fldAgencia'];
		$valor 		= format_number_out($rowCheque['fldValor']);
		$vencimento	= format_date_out($rowCheque['fldVencimento']);
		$cpf 		= $rowCheque['fldCPF_CNPJ'];
		
		$xml = "\t\t<correntista>$correntista</correntista>\n";
		$xml .= "\t\t<entidade>$entidade</entidade>\n";
		$xml .= "\t\t<numero>$numero</numero>\n";
		$xml .= "\t\t<banco>$banco</banco>\n";
		$xml .= "\t\t<conta>$conta</conta>\n";
		$xml .= "\t\t<agencia>$agencia</agencia>\n";
		$xml .= "\t\t<valor>$valor</valor>\n";
		$xml .= "\t\t<cpf>$cpf</cpf>\n";
		$xml .= "\t\t<vencimento>$vencimento</vencimento>\n";
		header('Content-Type: application/xml; charset=utf-8');
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
		echo "\t<dados>\n";
		echo $xml;
		echo "\t</dados>\n";	
		
	}
	
	elseif(isset($_GET['calcular_tributos'])){
		//posto o ncm do produto, retorno o tributo.
		$ncm 			= $_POST['ncm'];
		$st_trib		= $_POST['st_trib'];
		$origem			= $_POST['origem'];
		$valor			= format_number_in($_POST['valor']);
		$qtd 			= format_number_in($_POST['qtd']);
		$valor_total 	= 0;
		
		/*****************************************************************************************
		** SITUACOES																			**
		** 																						**
		** 16 - Tributada com permissão de crédito												**
		** 17 - Tributada sem permissão de crédito												**
		** 21 - Tributada com permissão de crédito e com cobrança do ICMS por ST				**
		** 22 - Tributada sem permissão de crédito e com cobrança do ICMS por ST				**
		** 23 - Isenção do ICMS para faixa de receita bruta e com cobrança do ICMS por ST		**
		** 24 - ICMS cobrado anteriormente por ST ou por antecipação							**
		** 25 - Outros 																			**
		*****************************************************************************************/
		
		if($st_trib == '16' or $st_trib == '17' or $st_trib == '21' or $st_trib == '22' or $st_trib == '23' or $st_trib == '24' or $st_trib == '25'){
			if($origem != '' and $origem != null){
			
				if($origem == "0" or $origem == "3" or $origem == "4" or $origem == "5"){ $campo = "fldAliqNac"; }
				else{ $campo == "fldAliqImp"; }
						
				$rsNCM 			= mysql_fetch_array(mysql_query("SELECT * FROM tblnfe_ibpt WHERE fldCodigo = $ncm"));
				$tributo 		= $rsNCM[$campo];
				$valor_total 	= ($valor * ($tributo / 100)) * $qtd;
			
			}
		}
		
		echo $valor_total;
	}
	
	elseif(isset($_GET['buscar_bairro_rota'])){
		$bairro_id 	= $_GET['buscar_bairro_rota'];
		$sql 		= mysql_query("select * from tblendereco_bairro where fldIBGE_Id = $bairro_id");
		$count		= mysql_num_rows($sql);
		$dados 		= array();
		if($count > 0){
			//tem bairro
			$sql = mysql_fetch_assoc($sql);
			$dados['bairro_id'] = $sql['fldId'];
			$dados['erro']		= false;
		} else {
			//n tem bairro
			$dados['erro']		= true;
		}
		
		echo $_GET['callback']."(".json_encode($dados).");";
	}
	
	elseif(isset($_GET['casas_decimais'])){
		$venda 	= fnc_sistema('venda_casas_decimais');
		$qtd 	= fnc_sistema('quantidade_casas_decimais');
		$compra	= fnc_sistema('compra_casas_decimais');
		$xml 	= "\t\t<venda>$venda</venda>\n";
		$xml   .= "\t\t<qtd>$qtd</qtd>\n";
		$xml   .= "\t\t<compra>$compra</compra>\n";
		header('Content-Type: application/xml; charset=utf-8');
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
		echo "\t<dados>\n";
		echo $xml;
		echo "\t</dados>\n";	
	}
	
?>