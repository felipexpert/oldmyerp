<link rel="stylesheet" type="text/css" media="screen" href="style/style_pedido.css"></link>

<?php
	if (!isset($_SESSION['logado'])){
		session_start();
	}
	
	$compra_id = $_GET['id'];
?>

<div class="alert_confirm" style="margin-top: 50px">
	<p>Deseja imprimir esta compra?</p>
    <ul>
    	<li style="margin-right:15px;"><a href="compra_imprimir.php?id=<?=$compra_id;?>">Imprimir</a></li>
        <li><a href="index.php?p=compra&mensagem=ok">Finalizar</a></li>
    </ul>
</div>

