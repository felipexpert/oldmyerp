
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
      	<title>myERP - Imprimir Venda</title>
         <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="Robots" content="none" />
    
        <link rel="stylesheet" type="text/css" media="all" href="style/impressao/style_pedido_imprimir_A4_otica.css" />
        <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />
        
	</head>
	<body>
<?
	ob_start();
	session_start();
	//conectar ao db
	require_once("inc/con_db.php");
	require_once("inc/fnc_general.php");
	require_once("inc/fnc_ibge.php");

	$pedido_id  = $_GET['id'];
	$data 		= date("Y-m-d");
	$rsPedido  	= mysql_query("SELECT (SUM((tblpedido_item.fldValor * (tblpedido_item.fldExcluido * -1 + 1)) * tblpedido_item.fldQuantidade) + fldValor_Terceiros)  as fldPedidoValor,
								 tblpedido.*,tblpedido.fldId as fldPedidoId, tblcliente.*, tblcliente.fldId as fldClienteId, tblcliente.fldCodigo as fldClienteCodigo, tblpedido.fldObservacao as fldObservacaoPedido
								 FROM tblpedido 
								 LEFT JOIN tblpedido_item ON tblpedido.fldId = tblpedido_item.fldPedido_Id
								 INNER JOIN tblcliente ON tblpedido.fldCliente_Id = tblcliente.fldId
								 WHERE tblpedido.fldId = $pedido_id GROUP BY tblpedido_item.fldPedido_Id");
	$rowPedido 	= mysql_fetch_array($rsPedido);
	
	$endereco 	= $rowPedido['fldEndereco'];
	$bairro 	= $rowPedido['fldBairro'];
	$municipio 	= fnc_ibge_municipio($rowPedido['fldMunicipio_Codigo']);
	$uf 	 	= fnc_ibge_uf_sigla($rowPedido['fldMunicipio_Codigo']);
	$CPFCNPJ 	= formatCPFCNPJTipo_out($rowPedido['fldCPF_CNPJ'], $rowPedido['fldTipo']);
	
	/*----------------------------------------------------------------------------------------------*/
	$rsEmpresa  		= mysql_query("SELECT * FROM tblempresa_info");
	$rowEmpresa 		= mysql_fetch_array($rsEmpresa);
	$CPFCNPJ_Empresa 	= formatCPFCNPJTipo_out($rowEmpresa['fldCPF_CNPJ'], $rowEmpresa['fldTipo']);
	
	$endereco_empresa 	= $rowEmpresa['fldEndereco'];
	$numero_empresa		= $rowEmpresa['fldNumero'];
	$bairro_empresa 	= $rowEmpresa['fldBairro'];
	$municipio_empresa	= fnc_ibge_municipio($rowEmpresa['fldMunicipio_Codigo']);
	$uf_empresa			= fnc_ibge_uf_sigla($rowEmpresa['fldMunicipio_Codigo']);
	/*----------------------------------------------------------------------------------------------*/
	
	#CRIANDO RODAPE  !!!########################################################################################################################################
	$sSQL = "SELECT SUM(tblpedido_parcela_baixa.fldValor * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldBaixaValor
			FROM tblpedido_parcela 
			LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
			WHERE tblpedido_parcela.fldPedido_Id = $pedido_id GROUP BY tblpedido_parcela.fldPedido_Id";
	$rsPedidoBaixa 	= mysql_query($sSQL);
	$rowPedidoBaixa	= mysql_fetch_array($rsPedidoBaixa);
	
	$total_pedido 			= $rowPedido['fldPedidoValor'];
	$desconto_pedido 		= $rowPedido['fldDesconto'];
	$desconto_reais_pedido 	= $rowPedido['fldDescontoReais'];
	$desconto 				= ($total_pedido * $desconto_pedido) / 100;
	$total_descontos 		= $desconto + $desconto_reais_pedido;
	$total_pedido_apagar 	= ($total_pedido - $total_descontos);
	$total_baixa 			= $rowPedidoBaixa['fldBaixaValor'];
	$valor_devedor 			= $total_pedido_apagar - $rowPedidoBaixa['fldBaixaValor'];

	$rodape = "  
  	<table id='pedido_pagamento'>
		<tr style='height:3cm; margin-bottom:0.5cm'><td>
        	<table class='parcelas' style='height:3.8cm; margin=0'>
				<tr><td><h2>Parcelas</h2></td></tr>
				<tr style='margin:0'><td>
					<table class='parcela_desc'>";
					
						$sSQL = "SELECT tblpedido_parcela.*, tblpagamento_tipo.fldSigla as fldForma_Pagamento, SUM(tblpedido_parcela_baixa.fldValor * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldTotalBaixa
						FROM tblpedido_parcela LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
						LEFT JOIN tblpagamento_tipo ON tblpedido_parcela.fldPagamento_Id = tblpagamento_tipo.fldId
						WHERE tblpedido_parcela.fldPedido_Id = $pedido_id GROUP BY tblpedido_parcela.fldId ORDER BY tblpedido_parcela.fldParcela";
						$rsParcela 	= mysql_query($sSQL);
						$rows 		= mysql_num_rows($rsParcela);
						$limite 	= ceil($rows / 2);
						$x 			= 1;
						$n 			= 1;
						$rsParcela 	= mysql_query($sSQL);
						//VAI VERIFICAR SE X É IGUAL AO LIMITE, METADE DOS REGISTROS, E ENTÃO PULAR PARA A DIV DO LADO
						while($rowParcela = mysql_fetch_array($rsParcela)){
							$parcela = str_pad($rowParcela['fldParcela'], 2, "0", STR_PAD_LEFT);
							$venc	 = format_date_out($rowParcela['fldVencimento']);
							$valor 	 = format_number_out($rowParcela['fldValor']);
							$pago 	 	= format_number_out($pago);
							$forma_pg 	= $rowParcela['fldForma_Pagamento'];
							if($parcela){
								$Col[$x][$n] = array(
									'parcela' 	=> $parcela,
									'venc' 		=> $venc,
									'valor' 	=> $valor,
									'pago' 		=> $pago,
									'forma_pg' 	=> $forma_pg
								);
								if($n == $limite){
									$x = 2;
									$n = 1;
								}else{
									$n ++;
								}
							}
						}
						$n = 1;
						$x = 1;
						$col = count($Col[$x]);
						while($col >= $n){
							if($Col[$x][$n]['parcela'] > 0){
								$rodape .= "  
								<tr>
									<td class='parcela'>".$Col[$x][$n]['parcela']."</td>                
									<td style='width:75px;'>".$Col[$x][$n]['venc']."</td>    
									<td style='width:40px; text-align:right'>".$Col[$x][$n]['valor']."</td>              
									<td style='width:50px; text-align:right'>PG: ".$Col[$x][$n]['pago']."</td>              
									<td style='width:30px; color:green; text-align:center'>".$Col[$x][$n]['forma_pg']."</td>
								</tr>";
							}
							($x == 2) ? $n++ : '';
							$x = ($x == 1) ? 2 : 1;	
						}
						$rodape .= "  	
					</table></td>
				</tr>
			</table></td>
        </tr>
        <tr class='pedido_total'>
            <td><strong>Total: 		</strong><span>R$ ".format_number_out($total_pedido)."</span></td>
            <td><strong>Desconto: 	</strong><span>R$ ".format_number_out($total_descontos)."</span></td>
            <td><strong>Pago: 		</strong><span>R$ ".format_number_out($total_baixa)."</span></td>
            <td><strong>A pagar: 	</strong><span>R$ ".format_number_out($valor_devedor)."</span></td>
        </tr>
    </table>";

	#################################################################################################################################################################
	
	$rowResponsavel = mysql_fetch_array(mysql_query('SELECT fldResponsavel, fldResponsavel_CPF FROM tblcliente_dados_adicionais WHERE fldCliente_Id = '.$rowPedido['fldClienteId']));	
	$rowOtica = mysql_fetch_array(mysql_query("SELECT tblpedido_otica.*, tblmarca.fldNome as fldMarca, tblproduto_otica_lente.fldLente 
											  FROM tblpedido_otica 
											  LEFT JOIN tblmarca ON tblmarca.fldId = tblpedido_otica.fldBifocal_Marca_Id
											  LEFT JOIN tblproduto_otica_lente ON tblproduto_otica_lente.fldId = tblpedido_otica.fldBifocal_Lente_Id
											  WHERE tblpedido_otica.fldPedido_Id = $pedido_id"));
	
	$responsavel 	= ($rowOtica['fldResponsavel'] == 1 ? $rowPedido['fldNome'] : $rowResponsavel['fldResponsavel']);
	$responsavelCPF = ($rowOtica['fldResponsavel'] == 1 ? $CPFCNPJ : formatCPFCNPJTipo_out($rowResponsavel['fldResponsavel_CPF'],1));
	
	
	$otica_valores = array(
					'perto' => array(
						'armacao' 	=>	$rowOtica['fldPerto_Armacao_Item_Id'],
						'material'	=>	$rowOtica['fldPerto_Material_Item_Id']
					),
					'longe' => array(
						'armacao' 	=>	$rowOtica['fldLonge_Armacao_Item_Id'],
						'material'	=>	$rowOtica['fldLonge_Material_Item_Id']
					)
				);
		
?>
    <div id="no-print">
        <div id="impressao_cabecalho">
            <ul id="bts">
                <li><a href="#" onclick ="window.print()"><span>Imprimir</span></a></li>
                <li><a href="index.php?p=pedido&amp;mensagem=ok"><span>Finalizar</span></a></li>
            </ul>
        </div>
	</div>
    <table id="pedido_imprimir">
<?

	//PEGA O STATUS DO PEDIDO -- LUCAS 20121023
	$status_n = $rowPedido['fldStatus'];
	$rsStatus = mysql_query("SELECT * FROM tblpedido_status WHERE fldId = $status_n");
	while($rowStatus = mysql_fetch_array($rsStatus))
	{
		
		$txtStatus = $rowStatus['fldStatus'];
		
	}
	//FIM DA VERIFICACAO DE STATUS

	if(fnc_sistema('impressao_mostrar_dados_empresa') == '1'){
		$empresa_razao_social = $rowEmpresa['fldRazao_Social'];
		$CPFCNPJ_Empresa 	= 'CNPJ '.formatCPFCNPJTipo_out($rowEmpresa['fldCPF_CNPJ'], $rowEmpresa['fldTipo']);
		$endereco_empresa 	= $rowEmpresa['fldEndereco'];
		$numero_empresa		= $rowEmpresa['fldNumero'];
		$bairro_empresa 	= $rowEmpresa['fldBairro'];
		$municipio_empresa	= fnc_ibge_municipio($rowEmpresa['fldMunicipio_Codigo']);
		$uf_empresa			= fnc_ibge_uf_sigla($rowEmpresa['fldMunicipio_Codigo']);
		$empresa_telefone1  = $rowEmpresa['fldTelefone1'];
		$empresa_telefone2  = $rowEmpresa['fldTelefone2'];
		$endereco_completo  = $endereco_empresa.', '.$numero_empresa.' - '.$bairro_empresa.' • '.$municipio_empresa.'/'.$uf_empresa;
	}else{
		$empresa_razao_social = '';
		$CPFCNPJ_Empresa 	= '';
		$endereco_empresa 	= '';
		$numero_empresa		= '';
		$bairro_empresa 	= '';
		$municipio_empresa	= '';
		$uf_empresa			= '';
		$empresa_telefone1  = '';
		$empresa_telefone2  = '';
	}
	
	//linha 204 -- lucas 20121023
	$cabecalho = 
		'<tr><td>
            <table class="cabecalho">
                <tr style="width: 180px;height:100px; float:left;"><td><img src="image/layout/logo_empresa.jpg" alt="logo" /></td></tr>
                <tr>
                    <td>'.$empresa_razao_social.'</td>
                    <td>'.$CPFCNPJ_Empresa.'</td>
                    <td>'.$endereco_completo.'</td>
                    <td>'.$empresa_telefone1.' '.$empresa_telefone2.'</td>
                </tr>
            </table>
		</td></tr>
        <tr><td>
            <table class="pedido_dados">
                <tr class="dados" style="border-bottom: 1px solid #ccc">
                    <td><strong>Cliente: </strong>'.substr(str_pad($rowPedido['fldClienteCodigo'], 4, "0", STR_PAD_LEFT).' '.$rowPedido['fldNome'], 0,100).'</td>
                    <td><strong>N&deg; Servi&ccedil;o: </strong>'.$rowPedido['fldCliente_Pedido'].'</td>
                    <td><strong>Status: '.$txtStatus.' </strong></td>
                    <td style="width: 182px"><strong>C&oacute;d. da Venda: </strong>'.str_pad($rowPedido['fldPedidoId'], 5, "0", STR_PAD_LEFT).'</td>
                    <td style="width: 182px"><strong>Data da Venda: </strong>'.format_date_out($rowPedido['fldPedidoData']).'</td>
                    <td style="width: 182px"><strong>Data Entrega: </strong>'.format_date_out($rowPedido['fldEntregaData']).'</td>
                    <td style="width: 182px"><strong>Total: </strong>R$ '.format_number_out($total_pedido).'</td>
                </tr>
            </table> 
        </td></tr>
        <tr>
        	<td>
                <table class="pedido_imprimir_dados" style="width:16.4cm;border-right:1px dashed #999; margin-top:0.2cm">
                	<tr><td>
                        <table class="pedido_dados" style="width:16.2cm">
                            <tr class="dados">
                                <td style="width: 7.9cm"><strong>Cliente: 				</strong>'.substr(str_pad($rowPedido['fldClienteCodigo'], 4, "0", STR_PAD_LEFT).' '.$rowPedido['fldNome'], 0,90).'</td>
                                <td style="width: 7.9cm"><strong>Respons&aacute;vel: 	</strong>'.$responsavel.'</td>
                                <td style="width: 3.9cm"><strong>Data Venda: 			</strong>'.format_date_out($rowPedido['fldPedidoData']).'</td>
                                <td style="width: 3.9cm"><strong>N&deg; Servi&ccedil;o: </strong>'.$rowPedido['fldCliente_Pedido'].'</td>
                                <td style="width: 3.9cm"><strong>C&oacute;d. Venda: 	</strong>'.str_pad($rowPedido['fldPedidoId'], 5, "0", STR_PAD_LEFT).'</td>
                                <td style="width: 3.9cm"><strong>Data Entrega: 			</strong>'.format_date_out($rowPedido['fldEntregaData']).'</td>
                                <td style="width: 3.9cm"><strong>CPF/CNPJ: 				</strong>'.$CPFCNPJ.'</td>
                                <td style="width: 3.9cm"><strong>RG/IE: 				</strong>'.$rowPedido['fldRG_IE'].'</td>
                                <td style="width: 3.9cm"><strong>Telefone 1: 			</strong>'.$rowPedido['fldTelefone1'].'</td>
                                <td style="width: 3.9cm"><strong>Telefone 2: 			</strong>'.$rowPedido['fldTelefone2'].'</td>
                                <td style="width: 7.9cm"><strong>E-mail: 				</strong>'.$rowPedido['fldEmail'].'</td>
                                <td style="width: 7.9cm"><strong>End.: 					</strong>'.$rowPedido['fldEndereco']. ' '. $rowPedido['fldNumero'].'</td>
                                <td style="width: 3.9cm"><strong>Bairro: 				</strong>'.$rowPedido['fldBairro'].'</td>
                                <td style="width: 3.9cm"><strong>CEP: 					</strong>'.$rowPedido['fldCEP'].'</td>
                                <td style="width: 3.9cm"><strong>Cidade: 				</strong>'.$municipio.'</td>
                                <td style="width: 3.9cm"><strong>UF: 					</strong>'.$uf.'</td>
                            </tr>
                        </table> 
                    </td></tr>';
					echo $cabecalho;
					
					//TAMANHO DOS CAMPOS
					$exibirMarca = mysql_num_rows(mysql_query("SELECT * FROM tblsistema WHERE fldParametro = 'impressao_exibir_marca' AND fldValor = 1"));
					$exibirCategoria = mysql_num_rows(mysql_query("SELECT * FROM tblsistema WHERE fldParametro = 'impressao_exibir_categoria' AND fldValor = 1"));
					if($exibirMarca && $exibirCategoria)
					{	$tamanhoDesc 	= 'width:3.22cm';
						$tamanhoMarca 	= 'width:2.5cm';
						$tamanhoCat		= 'width:2.5cm';	}
					if($exibirMarca && !$exibirCategoria)
					{	$tamanhoDesc 	= 'width:5.5cm';
						$tamanhoMarca 	= 'width:2.8cm';
						$tamanhoCat		= 'width:2.8cm';	}
					if(!$exibirMarca && $exibirCategoria)
					{	$tamanhoDesc 	= 'width:5.5cm';
						$tamanhoMarca 	= 'width:2.8cm';
						$tamanhoCat		= 'width:2.8cm';	}
					if(!$exibirMarca && !$exibirCategoria)
					{	$tamanhoDesc 	= 'width:8.5cm';
						$tamanhoMarca 	= 'width:3.6cm';
						$tamanhoCat		= 'width:3.6cm';	}
					//TAMANHO DOS CAMPOS
					
					$cabecalhoItem = '
					<tr>
                    	<td>
                        	<table class="pedido_descricao" style="width:16.2cm">
                            	<tr><td><h2 style="width:16cm">Produtos:</h2></td></tr>
                                <tr class="descricao" style="width:16.2cm">
                                    <td style="width:1cm">Qtde</td>
                                    <td style="width:1.8cm;">C&oacute;d.</td>
                                    <td style="'.$tamanhoDesc.'">Descric&atilde;o</td>';
									if(mysql_num_rows(mysql_query("SELECT * FROM tblsistema WHERE fldParametro = 'impressao_exibir_marca' AND fldValor = 1"))){
									$cabecalhoItem .= '<td style="'.$tamanhoMarca.'">Marca</td>'; }
									if(mysql_num_rows(mysql_query("SELECT * FROM tblsistema WHERE fldParametro = 'impressao_exibir_categoria' AND fldValor = 1"))){
									$cabecalhoItem .= '<td style="'.$tamanhoCat.'">Categoria</td>'; }
					$cabecalhoItem .= '
                                    <td style="width:1.2cm;">Unit.</td>
                                    <td style="width:1.2cm;">Desc(%)</td>
                                    <td style="width:1.6cm;text-align:right">Total</td>
                                </tr>';
								echo $cabecalhoItem;
								
								$vendaDecimal 		= fnc_sistema('venda_casas_decimais');
								$quantidadeDecimal 	= fnc_sistema('quantidade_casas_decimais');
								$n = 1;
								$rsItem 		= mysql_query("SELECT tblproduto.fldCodigo, tblpedido_item.*, tblmarca.fldNome as fldMarca, tblcategoria.fldNome as fldCategoria
															  FROM tblproduto
															  LEFT JOIN tblmarca ON tblproduto.fldMarca_Id = tblmarca.fldId
															  LEFT JOIN tblcategoria ON tblproduto.fldCategoria_Id = tblcategoria.fldId
															  INNER JOIN tblpedido_item ON tblproduto.fldId = tblpedido_item.fldProduto_Id
															  WHERE tblpedido_item.fldPedido_Id = ".$rowPedido['fldPedidoId']);
								echo mysql_error();
								$rowsItem			= mysql_num_rows($rsItem);
								while($rowItem 		= mysql_fetch_array($rsItem)){
									$x+= 1;
									
									$valor		 	= $rowItem['fldValor'];
									$qtde 			= $rowItem['fldQuantidade'];
									$desconto 		= $rowItem['fldDesconto'];
									$total 			= $valor * $qtde;
									$descontoItem 	= ($total * $desconto) / 100;
									$totalItem 		= $total - $descontoItem;
									
									if($otica_valores['perto']['armacao'] == $rowItem['fldId']){
										$pertoArmacao = $rowItem['fldDescricao'];
									}
									if($otica_valores['perto']['material'] == $rowItem['fldId']){
										$pertoMaterial = $rowItem['fldDescricao'];
									}
									if($otica_valores['longe']['armacao'] == $rowItem['fldId']){
										$longeArmacao = $rowItem['fldDescricao'];
									}
									if($otica_valores['longe']['material'] == $rowItem['fldId']){
										$longeMaterial = $rowItem['fldDescricao'];
									}
									
									//AQUI BUSCAR DADOS DA NFE
									$rowItemNFe = mysql_fetch_array(mysql_query("SELECT * FROM tblpedido_item_fiscal WHERE fldItem_Id= " .$rowItem['fldId']));
									$complemento = (isset($rowItemNFe['fldinformacoes_adicionais'])) ? ' - '.$rowItemNFe['fldinformacoes_adicionais']: '' ;
									
									#FAZER CONTROLE DE QUEBRA DE LINHAS POR CARACTERES DO ITEM
									$limiteLinha = 33; #limite de caracter por linha do item
									$countItem 	 = strlen($rowItem['fldDescricao'].$complemento);
									$linhas 	 += ceil($countItem / $limiteLinha);		
									
									$limiteFolha = 5; #MAXIMO DE LINHAS POR FOLHA - REFERENTE A ITENS - COM RODAPE
									
?>									<tr class="pedido_item" style="width:16.2cm">	
										<td style="width:1cm"><?=format_number_out($rowItem['fldQuantidade'],$quantidadeDecimal)?></td>
                                        <td style="width:1.8cm;"><?=str_pad($rowItem['fldCodigo'], 5, "0", STR_PAD_LEFT)?></td>
                                        <td style="<?=$tamanhoDesc?>"><?=$rowItem['fldDescricao'].$complemento?></td>
										<? if(mysql_num_rows(mysql_query("SELECT * FROM tblsistema WHERE fldParametro = 'impressao_exibir_marca' AND fldValor = 1"))){ ?>
                                        <td style="<?=$tamanhoMarca?>">&nbsp;<?=substr($rowItem['fldMarca'],0,20)?></td>
                                        <? } if(mysql_num_rows(mysql_query("SELECT * FROM tblsistema WHERE fldParametro = 'impressao_exibir_categoria' AND fldValor = 1"))){ ?>
                                        <td style="<?=$tamanhoCat?>">&nbsp;<?=substr($rowItem['fldCategoria'],0,20)?></td>
                                        <? } ?>
                                        <td style="width:1.2cm;"><?=format_number_out($rowItem['fldValor'],$vendaDecimal)?></td>
                                        <td style="width:1.2cm;"><?=format_number_out($desconto)?></td>
                                        <td style="width:1.7cm;text-align:right"><?=format_number_out($totalItem)?></td>
                    				</tr>
<?									$total_item += $totalItem;
									$itemN ++;				

									if($linhas >= 37 || $linhas > $limiteFolha && $rowsItem == $itemN){
										echo '</table></td></tr></table>';
										echo '<table id="pedido_imprimir" style="page-break-before: always">';
										echo $cabecalho;
										echo $cabecalhoItem;
										$linhas = 0;
									}elseif($rowsItem == $itemN && $linhas < $limiteFolha){
										$restante = $limiteFolha - $linhas; $x=1;
										
										while($x <= $restante){
											echo '<tr class="pedido_item" style="width:16.2cm"><td style="height:17px"></td></tr>';
											$x ++;
										}
									}
								}
?>							</table>
						</td>
					</tr>
                    <tr><td style="width:16.2cm">
                        <table id="pedido_otica">
                            <tr>
                                <td colspan="3"><h2>Longe</h2></td>
                                <td style="width:1.3cm">Arma&ccedil;&atilde;o:</td>
                                <td style="width:6.7cm">&nbsp;<?=substr($longeArmacao,0,42)?></td>
                                <td style="width:1.3cm">Material:</td>
                                <td style="width:6.7cm">&nbsp;<?=substr($longeMaterial,0,42)?></td>
                                
                                <td class="dados" style="width:1.08cm"></td>
                                <td class="dados"><strong>Esf&eacute;rico</strong></td>
                                <td class="dados"><strong>Cilindrico</strong></td>
                                <td class="dados"><strong>Eixo</strong></td>
                                <td class="dados" style="border-right:0"><strong>DP</strong></td>
                                
                                <td class="dados" style="width:1.08cm"><strong>O.D</strong></td>
                                <td class="dados"><?=$rowOtica['fldLonge_OD_Esferico']?></td>
                                <td class="dados"><?=$rowOtica['fldLonge_OD_Cilindro']?></td>
                                <td class="dados"><?=$rowOtica['fldLonge_OD_Eixo']?></td>
                                <td class="dados" style="border-right:0"><?=$rowOtica['fldLonge_OD_DP']?></td>
                                
                                <td class="dados" style="width:1.08cm"><strong>O.E</strong></td>
                                <td class="dados"><?=$rowOtica['fldLonge_OE_Esferico']?></td>
                                <td class="dados"><?=$rowOtica['fldLonge_OE_Cilindro']?></td>
                                <td class="dados"><?=$rowOtica['fldLonge_OE_Eixo']?></td>
                                <td class="dados" style="border-right:0"><?=$rowOtica['fldLonge_OE_DP']?></td>
                            </tr>
                            <tr>
                                <td colspan="3"><h2>Perto</h2></td>
                                <td style="width:1.3cm">Arma&ccedil;&atilde;o:</td>
                                <td style="width:6.7cm">&nbsp;<?=substr($pertoArmacao,0,42)?></td>
                                <td style="width:1.3cm">Material:</td>
                                <td style="width:6.7cm">&nbsp;<?=substr($pertoMaterial,0,42)?></td>
                                
                                <td class="dados" style="width:1.09cm"></td>
                                <td class="dados"><strong>Esf&eacute;rico</strong></td>
                                <td class="dados"><strong>Cilindrico</strong></td>
                                <td class="dados"><strong>Eixo</strong></td>
                                <td class="dados" style="border-right:0"><strong>DP</strong></td>
                                
                                <td class="dados" style="width:1.09cm"><strong>O.D</strong></td>
                                <td class="dados"><?=$rowOtica['fldPerto_OD_Esferico']?></td>
                                <td class="dados"><?=$rowOtica['fldPerto_OD_Cilindro']?></td>
                                <td class="dados"><?=$rowOtica['fldPerto_OD_Eixo']?></td>
                                <td class="dados" style="border-right:0"><?=$rowOtica['fldPerto_OD_DP']?></td>
                                
                                <td class="dados" style="width:1.09cm"><strong>O.E</strong></td>
                                <td class="dados"><?=$rowOtica['fldPerto_OE_Esferico']?></td>
                                <td class="dados"><?=$rowOtica['fldPerto_OE_Cilindro']?></td>
                                <td class="dados"><?=$rowOtica['fldPerto_OE_Eixo']?></td>
                                <td class="dados" style="border-right:0"><?=$rowOtica['fldPerto_OE_DP']?></td>
                            </tr>
						</table>
                        <table id="pedido_otica">
                            <tr>
                                <td colspan="3"><h2>Bifocal</h2></td>
                                <td style="width:1.3cm">Tipo:</td>
                                <td style="width:6.7cm">&nbsp<?=substr($rowOtica['fldMarca'],0,42)?></td>
                                <td style="width:1.3cm">Material:</td>
                                <td style="width:6.7cm">&nbsp;<?=substr($rowOtica['fldLente'],0,42)?></td>
                                
                                <td class="dados" style="width:1.1cm"></td>
                                <td class="dados" style="width:2.5cm"><strong>Altura</strong></td>
                                <td class="dados" style="width:2.5cm"><strong>DNP</strong></td>
                                <td class="dados" style="width:1.82cm;border-right:0"><strong>Adi&ccedil;&atilde;o</strong></td>
                                
                                <td class="dados" style="width:1.1cm"><strong>O.D</strong></td>
                                <td class="dados" style="width:2.5cm"><?=$rowOtica['fldBifocal_OD_Altura']?></td>
                                <td class="dados" style="width:2.5cm"><?=$rowOtica['fldBifocal_OD_DNP']?></td>
                                <td class="dados" style="width:1.82cm;border-right:0"><?=$rowOtica['fldBifocal_OD_Adicao']?></td>
                                
                                <td class="dados" style="width:1.1cm"><strong>O.E</strong></td>
                                <td class="dados" style="width:2.5cm"><?=$rowOtica['fldBifocal_OE_Altura']?></td>
                                <td class="dados" style="width:2.5cm"><?=$rowOtica['fldBifocal_OE_DNP']?></td>
                                <td class="dados" style="width:1.82cm;border-right:0"><?=$rowOtica['fldBifocal_OE_Adicao']?></td>
                            </tr>                                
                        </table>
                        <table id="pedido_observacao">
                            <tr>
                                <td>
                                    <p><strong>M&eacute;dico :</strong><?=$rowPedido['fldMedico']?></p>
                                </td>
                                <td>
                                   <p><strong>CRM :</strong><?=$rowPedido['fldMedico_crm']?></p>
                                   <br />
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    <h2>Observa&ccedil;&atilde;o</h2>
                                    <p><?=$rowPedido['fldObservacaoPedido']?></p>
                                </td>
                            </tr>                                    
                        </table>
                	</td></tr>
            		<tr style="width:16.2cm"><td><? echo $rodape;?></td></tr>
        		</table>
            </td>
		</tr>
	</table>
    <table class="pedido_nota" summary="Imprimir Pedido">
        <tr>
            <td colspan="4"> Ao(s) <span class="uppercase"><? $data = new dataExtenso; $data->dtExtenso(format_date_out($rowPedido['fldCadastroData']))?></span> pagarei por esta unica via de NOTA PROMISSORIA a <?=$rowEmpresa['fldRazao_Social']?>, sob o CNPJ - <?=$CPF_CNPJ?> ou a sua ordem, a quantia de (<span class="uppercase"><?= utf8_encode(valorExtenso($total_pedido_apagar,1,"baixa"))?></span>) em moeda corrente deste pais, pagavel em ITAPIRA-SP.</td>
        </tr>        
        <tr><td colspan="4">&nbsp;</td></tr>
        <tr>
        	<td style="width: 7.5cm"><strong><?=$responsavel?></strong></td>
       		<td style="width: 7.5cm"><strong>CPF:</strong> <?=$responsavelCPF?></td>
		</tr>
        <tr>
        	<td style="width: 7.5cm"><?=$rowPedido['fldEndereco']?> <?=$rowPedido['fldNumero']?></td>
			<td style="width: 7.5cm;text-align:left"><?=$rowPedido['fldBairro']?> / <?=$municipio?> - <?=$uf?></td>
		</tr>
        <tr height="30px"><td colspan="4">&nbsp;</td></tr>
        <tr>
            <td style="width: 7.5cm; text-align:center">ITAPIRA, <?=date("d/m/Y")?></td>
            <td style="width: 0.5cm" >&nbsp;</td>
            <td style="text-align:center;border-top:1px solid" colspan="2" ><?=$responsavel?></td>
        </tr>
    </table>
    </body>
</html>