<?php

	//recebendo a data do calendário de período
	if(isset($_POST['txt_calendario_data_inicial']) && isset($_POST['txt_calendario_data_final'])){
	    $_SESSION['txt_data1'] = (isset($_POST['txt_calendario_data_inicial'])) ? $_POST['txt_calendario_data_inicial'] : '';
	    $_SESSION['txt_data2'] = (isset($_POST['txt_calendario_data_final']))	? $_POST['txt_calendario_data_final'] 	: '';
	}
	
	$_SESSION['txt_pedido_consignado_data1'] 	= (isset($_SESSION['txt_pedido_consignado_data1']))	? $_SESSION['txt_pedido_consignado_data1'] : date('d') . '/' . date('m/Y', mktime(0, 0, 0, date('m') - 3));
	$_SESSION['txt_pedido_consignado_data2'] 	= (isset($_SESSION['txt_pedido_consignado_data2']))	? $_SESSION['txt_pedido_consignado_data2'] : date('d') . '/' . date('m') . '/' . date("Y");
	
	
	//memorizar os filtros para exibição nos selects
	if(isset($_POST['btn_limpar'])){
		$_SESSION['txt_pedido_consignado_codigo'] 		= "";
		$_SESSION['sel_pedido_consignado_status'] 		= "status";
		$_SESSION['sel_pedido_consignado_funcionario'] 	= "";
		$_SESSION['sel_pedido_consignado_rota'] 		= "";
		$_SESSION['txt_data1'] 							= date('d') . '/' . date('m/Y', mktime(0, 0, 0, date('m') - 3));
		$_SESSION['txt_data2'] 							= date('d') . '/' . date('m') . '/' . date("Y");
	}else{
		$_SESSION['txt_pedido_consignado_codigo'] 		= (isset($_POST['txt_pedido_consignado_codigo']) 		? $_POST['txt_pedido_consignado_codigo'] 		: $_SESSION['txt_pedido_consignado_codigo']);
		$_SESSION['sel_pedido_consignado_status'] 		= (isset($_POST['sel_pedido_consignado_status']) 		? $_POST['sel_pedido_consignado_status'] 		: $_SESSION['sel_pedido_consignado_status']);
		$_SESSION['sel_pedido_consignado_funcionario'] 	= (isset($_POST['sel_pedido_consignado_funcionario'])	? $_POST['sel_pedido_consignado_funcionario'] 	: $_SESSION['sel_pedido_consignado_funcionario']);
		$_SESSION['sel_pedido_consignado_rota'] 		= (isset($_POST['sel_pedido_consignado_rota']) 			? $_POST['sel_pedido_consignado_rota'] 			: $_SESSION['sel_pedido_consignado_rota']);
		$_SESSION['txt_pedido_consignado_data1'] 		= (isset($_POST['txt_pedido_consignado_data1'])			? $_POST['txt_pedido_consignado_data1']			: $_SESSION['txt_pedido_consignado_data1']);
		$_SESSION['txt_pedido_consignado_data2'] 		= (isset($_POST['txt_pedido_consignado_data2'])			? $_POST['txt_pedido_consignado_data2']			: $_SESSION['txt_pedido_consignado_data2']);
	}	

?>
<form id="frm-filtro" action="index.php?p=pedido&modo=consignado" method="post">
	<fieldset>
  	<legend>Buscar por:</legend>
    <ul>
        <li>
<?			$codigo_consignado = ($_SESSION['txt_pedido_consignado_codigo'] ? $_SESSION['txt_pedido_consignado_codigo'] : "c&oacute;digo");
			($_SESSION['txt_pedido_consignado_codigo'] == "código") ? $_SESSION['txt_pedido_consignado_codigo'] = '' : '';
?>      	<input style="width: 45px" type="text" name="txt_pedido_consignado_codigo" id="txt_pedido_consignado_codigo" onfocus="limpar (this,'c&oacute;digo');" onblur="mostrar (this, 'c&oacute;digo');" value="<?=$codigo_consignado?>"/>
		</li>
        <li>
            <select id="sel_pedido_consignado_status" name="sel_pedido_consignado_status" style="width: 90px" >
            	<option <?=($_SESSION['sel_pedido_consignado_status'] == 'status') 	? 'selected="selected"' : '' ?> 	value="status">status</option>
            	<option <?=($_SESSION['sel_pedido_consignado_status'] == '0')		? 'selected="selected"' : '' ?> 	value="0">em espera</option>
            	<option <?=($_SESSION['sel_pedido_consignado_status'] == '1') 		? 'selected="selected"' : '' ?> 	value="1">finalizado</option>
            </select>
		</li>
        <li>
            <select id="sel_pedido_consignado_funcionario" name="sel_pedido_consignado_funcionario" style="width: 200px" >
	            <option value=''>Funcionario</option>
<?				$rsFuncionario = mysql_query("SELECT fldId, fldNome FROM tblfuncionario WHERE fldDisabled = '0' ORDER BY fldNome ASC");
				while($rowFuncionario = mysql_fetch_array($rsFuncionario)){
?>					<option <?=($_SESSION['sel_pedido_consignado_funcionario'] == $rowFuncionario['fldId']) ? 'selected="selected"' : '' ?> value='<?=$rowFuncionario['fldId']?>'><?=$rowFuncionario['fldNome']?></option>
<?				}
?>			</select>
		</li>
        <li>
<?			
			if($_SESSION["sistema_rota_controle"] == '1'){            
?>  				<select style="width:90px; text-align:left" id="sel_pedido_consignado_rota" name="sel_pedido_consignado_rota" >
              		<option value="">Rota</option>
<?					$rsRota = mysql_query("SELECT * FROM tblendereco_rota WHERE fldExcluido = 0 ORDER BY fldRota+0");
					while($rowRota = mysql_fetch_array($rsRota)){
?>						<option <?=($_SESSION['sel_pedido_consignado_rota'] == $rowRota['fldId']) ? 'selected="selected"' : '' ?> value='<?=$rowRota['fldId']?>'><?=$rowRota['fldRota']?></option>
<?					}
?>				</select>
<?			}
?>		</li>

        <li>
<?			$data1 = ($_SESSION['txt_pedido_consignado_data1'] ? $_SESSION['txt_pedido_consignado_data1'] : "");
?>     		<input title="Data inicial" style="text-align:center;width: 70px" type="text" name="txt_pedido_consignado_data1" id="txt_pedido_consignado_data1" class="calendario-mask" value="<?=$data1?>"/>
      	</li>
		
        <li>
<?			$data2 = ($_SESSION['txt_pedido_consignado_data2'] ? $_SESSION['txt_pedido_consignado_data2'] : "");
?>     		<input title="Data final" style="text-align:center;width: 70px" type="text" name="txt_pedido_consignado_data2" id="txt_pedido_consignado_data2" class="calendario-mask" value="<?=$data2?>"/>
			<a href="calendario_periodo,<?=format_date_in($data1) . ',' . format_date_in($data2)?>,pedido_consignado" id="exibir-calendario" title="Exibir calend&aacute;rio" class="modal calendario-modal" rel="600-320"></a>
      	</li>
        
        <li style="float:right">
	        <button type="submit" name="btn_exibir" style="margin:0" title="Exibir">Exibir</button>
        </li>
        <li style="float:right">
        	<button type="submit" name="btn_limpar" style="margin:0" title="Limpar Filtro">Limpar filtro</button>
        </li>
    </ul>
  </fieldset>
</form>

<? 
	if(($_SESSION['txt_pedido_consignado_codigo']) != ""){
		
		$filtro .= " AND tblpedido_consignado.fldId = '".$_SESSION['txt_pedido_consignado_codigo']."'";
	}
	if(($_SESSION['sel_pedido_consignado_status']) != ""){
		if(($_SESSION['sel_pedido_consignado_status']) != "status"){
			$filtro .= " AND tblpedido_consignado.fldFinalizado = '".$_SESSION['tblpedido_consignado']."'";
		}
	}
	
	if(format_date_in($_SESSION['txt_pedido_consignado_data1']) != "" || format_date_in($_SESSION['txt_pedido_consignado_data2']) != ""){
		if($_SESSION['txt_pedido_consignado_data1'] != "" && $_SESSION['txt_pedido_consignado_data2'] != ""){
			$filtro .= " AND tblpedido_consignado.fldData between '".format_date_in($_SESSION['txt_pedido_consignado_data1'])."' AND '".format_date_in($_SESSION['txt_pedido_consignado_data2'])."'";
		}elseif($_SESSION['txt_pedido_consignado_data1'] != "" && $_SESSION['txt_pedido_consignado_data2'] == ""){
			$filtro .= " AND tblpedido_consignado.fldData >= '".format_date_in($_SESSION['txt_pedido_consignado_data1'])."'";
		}elseif($_SESSION['txt_pedido_consignado_data2'] != "" && $_SESSION['txt_pedido_consignado_data1'] == ""){
			$filtro .= " AND tblpedido_consignado.fldData <= '".format_date_in($_SESSION['txt_pedido_consignado_data2'])."'";
		}
	}
	
	/* ALTERADO A PEDIDO DA SCORPION, PARA BUSCAR APENAS POR FUNCIONARIO 1 130904				
	if(($_SESSION['sel_pedido_consignado_funcionario']) != ""){
		$filtro .= " AND (tblpedido_consignado.fldFuncionario1_Id = '".$_SESSION['sel_pedido_consignado_funcionario']."' OR tblpedido_consignado.fldFuncionario2_Id = '".$_SESSION['sel_pedido_consignado_funcionario']."')";
	}
	*/
	if(($_SESSION['sel_pedido_consignado_funcionario']) != ""){
		$filtro .= " AND tblpedido_consignado.fldFuncionario1_Id = '".$_SESSION['sel_pedido_consignado_funcionario']."'";
	}
	
	if(($_SESSION['sel_pedido_consignado_rota']) != ""){
		$filtro .= " AND tblpedido_consignado.fldRota_Id = '".$_SESSION['sel_pedido_consignado_rota']."'";
	}
	
	//transferir para a sessão
	$_SESSION['filtro_pedido_consignado'] = $filtro;
	

?>