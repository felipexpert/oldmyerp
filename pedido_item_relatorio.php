<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
      	<title>myERP - Relat&oacute;rio de Itens</title>
        <link rel="stylesheet" type="text/css" href="style/style_relatorio.css" />
        <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />
        
	</head>
	<body>
<? 
		ob_start();
		session_start();
		
		require("inc/con_db.php");
		require("inc/fnc_general.php");
		
		$rowDados 		= mysql_fetch_array(mysql_query("select * from tblempresa_info"));
		$CPF_CNPJDados 	= formatCPFCNPJTipo_out($rowDados['fldCPF_CNPJ'], $rowDados['fldTipo']);
		$rowUsuario 	= mysql_fetch_array(mysql_query("SELECT * FROM tblusuario WHERE fldId=".$_SESSION['usuario_id']));
		
		#ABAIXO CRIO O CABECALHO #########################################################################################################################################################
		$rota_cabecalho = ($_SESSION["sistema_rota_controle"] > 0) ? 'Rota' : '' ;
		//$pgTotal 		= $rowsItem / $limite;
		$p = 1;
		
		$tabelaCabecalho =' 
			<tr style="border-bottom: 2px solid">
                <td style="width: 600px"><h1>Relat&oacute;rio de Itens</h1></td>
                <td style="width: 200px"><p class="pag">&nbsp;</p></td>
            </tr>
            <tr>
                <td>
                    <table style="width: 580px" class="table_relatorio_dados" summary="Relat&oacute;rio">
                        <tr>
                            <td style="width: 320px;">Raz&atilde;o Social: '.$rowDados['fldNome'].'</td>
                            <td style="width: 200px;">Nome Fantasia: '.$rowDados['fldNome_Fantasia'].'</td>
                            <td style="width: 320px;">CPF/CNPJ: '.$CPF_CNPJDados.'</td>
                            <td style="width: 200px;">Telefone: '.$rowDados['fldTelefone1'].'</td>
                        </tr>
                    </table>	
                </td>
                <td>        
                    <table class="dados_impressao">
                        <tr>
                            <td><b>Data: </b><span>'.format_date_out(date("Y-m-d")).'</span></td>
                            <td><b>Hora: </b><span>'.format_time_short(date("H:i:s")).'</span></td>
                            <td><b>Usu&aacute;rio: </b><span>'.$rowUsuario['fldUsuario'].'</span></td>
                        </tr>
                    </table>
                </td>
            </tr>';
			$tabelaCabecalho2 ='
            <tr>
                <td style="margin:0">
                    <table class="table_relatorio" summary="Relat&oacute;rio">
                        <tr style="border:none; margin-bottom:20px">
                            <td style="width:100px;text-align:center;margin-right:5px;font-weight:bold"	>'.$rota_cabecalho.'</td>
                            <td style="width:50px;text-align:center;font-weight:bold"					>Data</td>
							<td style="width:60px;text-align:center;margin-right:5px;font-weight:bold"	>Cliente</td>
							<td style="width:265px;font-weight:bold"									>Descri&ccedil;&atilde;o</td>
							<td class="valor" style="text-align:right;font-weight:bold"					>Ref.</td>
							<td class="valor" style="text-align:right;font-weight:bold"					>Valor</td>
							<td class="valor" style="text-align:right;font-weight:bold"					>Qtde</td>
							<td class="valor" style="text-align:right;font-weight:bold"					>Total</td>
						</tr>
					</table>
				</td>
			</tr>';
			$tabelaCabecalho3 ='
			<tr>
				<td>
					<table class="table_relatorio" summary="Relat&oacute;rio">';
	######################################################################################################################################################################################
	# AGORA CRIO VARIAVEIS, ARMAZENANDO OS REGISTROS DE ITENS, ABAIXO APENAS FAÇO IMPRIMIR NA TELA COM AS QUEBRAS DE LINHAS
	# BLOCO DE ITENS LANCADOS ############################################################################################################################################################
		if($_GET['order']){
			switch($_GET['order']){
				case 'fornecedor' 	: $ordem = 'fldFornecedor_Id'; break;
				case 'marca' 		: $ordem = 'fldMarca_Id'; break;
				case 'categoria'	: $ordem = 'fldcategoria_Id'; break;
			}
			$order = $ordem.', tblpedido_item.fldDescricao';
		}else{
			$order = $_SESSION['order_pedido_item'].', tblpedido_item.fldDescricao';
		}
		
					

		$sSQL 			= str_replace('tblpedido_item.fldQuantidade as PedidoQuantidade,','SUM(tblpedido_item.fldQuantidade) as PedidoQuantidade,',$_SESSION['pedido_item_relatorio']) . $_SESSION['txt_pedido_item_data_filtro']." GROUP BY tblpedido_item.fldDescricao, fldCliente_Id ORDER BY ".$order;
		//$sSQL 			= $_SESSION['pedido_item_relatorio'] . $_SESSION['txt_pedido_item_data_filtro']." GROUP BY fldCliente_Id ORDER BY ".$order;
		$rsPedidoItem 	= mysql_query($sSQL);
		$rowsItem	 	= mysql_num_rows($rsPedidoItem);
		echo mysql_error();
		$n1	 			= 1; #DEFINE O NUMERO DO BLOCO
		$countItem1 	= 1; #DEFINE CONTAGEM DE ITENS NO WHILE PARA QUEBRA DE BLOCO AO ATINGIR LIMITE DE 20
		$x				= 1; #DEFINE CONTAGEM DE ITENS TOTAIS, PRA SABER SE JA TERMINOU WHILE MAS AINDA FALTA ESPACO
		$limite 		= 19;
		while($rowPedidoItem = mysql_fetch_array($rsPedidoItem)){
			echo mysql_error();
			if($_SESSION["sistema_rota_controle"] > 0 && $rowPedidoItem['fldRota_Id'] >0){
				
				$rsRota	 = mysql_query("SELECT fldRota FROM tblendereco_rota WHERE fldId = ".$rowPedidoItem['fldRota_Id']);
				$rowRota = mysql_fetch_array($rsRota);
				
				$rota 	 = str_pad($rowRota['fldRota'], 4, "0", STR_PAD_LEFT);
			}
			
			$valor_total = $rowPedidoItem['PedidoValor'] * $rowPedidoItem['PedidoQuantidade'];
			$bloco1[$n1] .= '
			<tr>
				<td style="width:100px;text-align:center;margin-right:5px">'.$rota.'</td>
				<td style="width:50px;text-align:center">'. format_date_out3($rowPedidoItem['PedidoData']).'</td>
				<td style="width:60px;text-align:center;margin-right:5px">'.str_pad($rowPedidoItem['fldCodigoCliente'],6,'0', STR_PAD_LEFT).'</td>
				<td style="width:268px;display:inline; overflow:hidden">'.substr($rowPedidoItem['PedidoItem'],0, 45).'</td>
				<td class="valor" style="text-align:right">'.$rowPedidoItem['fldReferencia'].'</td>
				<td class="valor" style="text-align:right">'.format_number_out($rowPedidoItem['PedidoValor']).'</td>
				<td class="valor" style="text-align:right">'.format_number_out($rowPedidoItem['PedidoQuantidade']).'</td>
				<td class="valor" style="text-align:right">'.format_number_out($valor_total).'</td>
			</tr>';
			
			#SE CHEGAR A 20 LINHAS, MUDA DE 'BLOCO' E RECMECA CONTAGEM
			if($countItem1 == $limite){
				$bloco1[$n1] .='<tr style="border=0; width:800px"></tr>';
				$countItem1 = 1;
				$n1 ++;
			}elseif($rowsItem == $x && $countItem1 < $limite){ #SE JA TERMINOU O WHILE DE REGISTROS MAS AINDA NAO ATINGIU 20 LINHAS, CONTINUAR CRIANDO LINHAS ATE O LIMITE
				while($countItem1 <= $limite){ $bloco1[$n1] .='<tr style="border:0; width:800px"></tr>'; $countItem1++;}
			}else{
				$countItem1 ++;
			}
			$x ++;
		}
	
	# BLOCO DE ITENS PAGOS ############################################################################################################################################################
		$sSQL 			= 	"SELECT tblpedido_item.fldPedido_Id,
									tblpedido_item.fldDescricao as PedidoItem,
									tblpedido_item.fldReferencia,
									tblpedido_item.fldValor as fldItemValor,
									SUM(tblpedido_item_pago.fldValorPago) as fldValorPago,
									SUM(tblpedido_item_pago.fldQuantidade) as fldQuantidade,
									tblpedido_item_pago.fldDataPago,
									tblcliente.fldCodigo as fldCodigoCliente,
									tblendereco_bairro.fldSetor,
									tblendereco_bairro.fldRota_Id
									FROM tblpedido
									LEFT  	JOIN tblpedido_funcionario_servico ON tblpedido_funcionario_servico.fldPedido_Id 		= tblpedido.fldId 						AND tblpedido_funcionario_servico.fldFuncao_Tipo = 1
									LEFT 	JOIN tblpedido_fiscal 	ON tblpedido.fldId 					= tblpedido_fiscal.fldPedido_Id
									INNER 	JOIN tblcliente 		ON tblcliente.fldId 				= tblpedido.fldCliente_Id
									LEFT 	JOIN tblendereco 		ON tblcliente.fldEndereco_Id 		= tblendereco.fldId
									LEFT 	JOIN tblendereco_bairro ON tblendereco_bairro.fldId 		= tblendereco.fldBairro_Id
									INNER 	JOIN tblpedido_item 	ON tblpedido.fldId 					= tblpedido_item.fldPedido_Id
									LEFT 	JOIN tblproduto			ON tblpedido_item.fldProduto_Id 	= tblproduto.fldId
									INNER 	JOIN tblpedido_item_pago ON tblpedido_item_pago.fldItem_Id 	= tblpedido_item.fldId AND tblpedido_item_pago.fldExcluido = 0
									".$_SESSION['filtro_pedido_item']. $_SESSION['txt_pedido_item_data_pago_filtro']." AND tblpedido_item_pago.fldExcluido = 0 
									GROUP BY tblpedido_item.fldProduto_Id, tblpedido_item.fldDescricao, tblpedido.fldCliente_Id ORDER BY ".$order;
							
		$rsPedidoItem 	= mysql_query($sSQL);
		$rowsItem	 	= mysql_num_rows($rsPedidoItem);
		echo mysql_error();
		$n2	 			= 1; #DEFINE O NUMERO DO BLOCO
		$countItem2 	= 1; #DEFINE CONTAGEM DE ITENS NO WHILE PARA QUEBRA DE BLOCO AO ATINGIR LIMITE DE 20
		$x				= 1; #DEFINE CONTAGEM DE ITENS TOTAIS, PRA SABER SE JA TERMINOU WHILE MAS AINDA FALTA ESPACO
		$limite 		= 19;
		while($rowPedidoItem = mysql_fetch_array($rsPedidoItem)){
			echo mysql_error();
			if($_SESSION["sistema_rota_controle"] > 0 && $rowPedidoItem['fldRota_Id'] > 0){
				
				$rsRota	 = mysql_query("SELECT fldRota FROM tblendereco_rota WHERE fldId = ".$rowPedidoItem['fldRota_Id']);
				$rowRota = mysql_fetch_array($rsRota);
				$rota 	 = str_pad($rowRota['fldRota'], 4, "0", STR_PAD_LEFT);
			}
			
			//$valor_total = $rowPedidoItem['fldValorPago'] * $rowPedidoItem['fldQuantidade'];
			$bloco2[$n2] .= '
			<tr>
				<td style="width:100px;text-align:center;margin-right:5px">'.$rota.'</td>
				<td style="width:50px;text-align:center">'. format_date_out3($rowPedidoItem['fldDataPago']).'</td>
				<td style="width:60px;text-align:center;margin-right:5px">'.str_pad($rowPedidoItem['fldCodigoCliente'],6,'0', STR_PAD_LEFT).'</td>
				<td style="width:268px;display:inline; overflow:hidden">'.substr($rowPedidoItem['PedidoItem'],0, 45).'</td>
				<td class="valor" style="text-align:right">'.$rowPedidoItem['fldReferencia'].'</td>
				<td class="valor" style="text-align:right">'.format_number_out($rowPedidoItem['fldItemValor']).'</td>
				<td class="valor" style="text-align:right">'.format_number_out($rowPedidoItem['fldQuantidade']).'</td>
				<td class="valor" style="text-align:right">'.format_number_out($rowPedidoItem['fldValorPago']).'</td>
			</tr>';
			
			#SE CHEGAR A 20 LINHAS, MUDA DE 'BLOCO' E RECMECA CONTAGEM
			if($countItem2 == $limite){
				$bloco2[$n2] .='<tr style="border=0; width:800px"></tr>';
				$countItem2 = 1;
				$n2 ++;
			}elseif($rowsItem == $x && $countItem2 < $limite){ #SE JA TERMINOU O WHILE DE REGISTROS MAS AINDA NAO ATINGIU 20 LINHAS, CONTINUAR CRIANDO LINHAS ATE O LIMITE
				while($countItem2 <= $limite){ $bloco2[$n2] .='<tr style="border=0; width:800px"></tr>'; $countItem2++;}
			}else{
				$countItem2 ++;
			}
			$x ++;
		}
	
	#AGORA MANDO GERAR NA TELA PARA IMPRESSAO ############################################################################################################################################

		$x = 1;
		while($x <= $n1){
			$tabelaCabecalho1 = ($x == 1)? '<table class="relatorio_print" style="page-break-before:avoid">'.$tabelaCabecalho : '<table class="relatorio_print">'.$tabelaCabecalho;
			#PRIMEIRO BLOCO (LANCADOS) ###################################################################################################################################################
				print $tabelaCabecalho1;
				print '
				<tr>
					<td style="margin:0"><h2 style="background:#E1E1E1;width:800px;text-align:center">itens lan&ccedil;ados</h2></td>
				</tr>';
				print $tabelaCabecalho2;
				print $tabelaCabecalho3;
				
				echo $bloco1[$x];
			#SEGUNDO BLOCO (PAGOS) #######################################################################################################################################################
				print '
				<tr style ="border:0">
					<td style="margin:0"><h2 style="background:#E1E1E1;width:800px;text-align:center">itens pagos</h2></td>
				</tr>';
				print $tabelaCabecalho2;
				echo $bloco2[$x];
?>
						</table>
					</td>
				</tr>
			</table>			
<?			$x ++;
		}
?>                
	</body>
</html>	
