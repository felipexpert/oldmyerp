<div id="voltar">
    <p><a href="index.php?p=crm_acompanhamento_cliente">n&iacute;vel acima</a></p>
</div>	

<h2>Novo Chamado</h2>
<div id="principal">
<?	
	//a��es
	if($_SERVER['REQUEST_METHOD'] == 'POST') {
		
		$clienteId 					   = $_POST['hid_cliente_id'];
		$dataChamado 				   = format_date_in($_POST['txt_chamado_data']);
		$horaChamado 				   = $_POST['txt_chamado_hora'];
		$usuarioId  				   = $_POST['hid_usuario_id'];
		$descricaoChamado 			   = nl2br(mysql_real_escape_string($_POST['txt_chamado_descricao']));
		$situacaoChamado 			   = $_POST['sel_chamado_situacao'];
		$prioridadeChamado 			   = $_POST['sel_chamado_prioridade'];
		
		$sql  = "INSERT INTO tblcrm_acompanhamento_cliente_chamado (fldCliente_Id, fldSituacao_Id, fldPrioridade_Id, fldUsuario_Id, fldDescricao, fldData, fldHora)
				VALUES(
				'{$clienteId}',
				'{$situacaoChamado}',
				'{$prioridadeChamado}',
				'{$usuarioId}',
				'{$descricaoChamado}',
				'{$dataChamado}',
				'{$horaChamado}'
				)";
		
		//checar se gravou o chamado e se h� alguma data agendada
		if(mysql_query($sql) && isset($_POST['hid_agendamento'])) {
			
			$ultimoChamado = mysql_fetch_array(mysql_query("SELECT last_insert_id() AS idChamado"));
			$idChamado	   = $ultimoChamado['idChamado'];
			
			$dataChamadoAgendamento 	   = (empty($_POST['txt_chamado_agendamento_data'])) ? null : format_date_in($_POST['txt_chamado_agendamento_data']);
			$horaChamadoAgendamento 	   = (empty($_POST['txt_chamado_agendamento_hora'])) ? "NULL" : "'" . $_POST['txt_chamado_agendamento_hora'] . "'";
			$tipoChamadoAgendamento 	   = ($_POST['sel_chamado_agendamento_tipo'] < 1) ? 0 : $_POST['sel_chamado_agendamento_tipo'];
			$funcionarioChamadoAgendamento = ($_POST['sel_chamado_agendamento_funcionario'] < 1) ? 0 : $_POST['sel_chamado_agendamento_funcionario'];
			$descricaoChamadoAgendamento   = nl2br(mysql_real_escape_string($_POST['txt_chamado_agendamento_desc']));
			
			$sql = "INSERT INTO tblcrm_acompanhamento_agenda (fldChamado_Id, fldTipo_Id, fldFuncionario_Id, fldDescricao, fldData, fldHora, fldUsuario_Id)
					VALUES(
					'{$idChamado}',
					'{$tipoChamadoAgendamento}',
					'{$funcionarioChamadoAgendamento}',
					'{$descricaoChamadoAgendamento}',
					'{$dataChamadoAgendamento}',
					{$horaChamadoAgendamento},
					'{$usuarioId}'
					)";
			
			mysql_query($sql);
			
		}
		
		if(!mysql_error()){
			header("location:index.php?p=crm_acompanhamento_cliente&mensagem=ok");
		}
		else {
?>			<div class="alert">
				<p class="erro">
					N&atilde;o foi poss&iacute;vel gravar os dados!
					<a class="voltar link" href="index.php?p=crm_acompanhamento_novo">Voltar</a>
				</p>
			</div>
<?
			die(mysql_error());
		}
	}
	else {
?>		
        <div class="form">
            <form class="frm_detalhe" style="width:890px" id="frm_acompanhamento_chamado_novo" action="" method="post">
                <ul>
                    <li>
                        <label for="txt_chamado_id">Ticket #</label>
                        <input type="text" style="width:70px; text-align:right" id="txt_chamado_id" name="txt_chamado_id" disabled="disabled" value="novo" />
                    </li>
					
					<li>
                        <label for="txt_cliente_codigo">Cliente</label>
                        <input type="text" style="width:70px; text-align:right" id="txt_cliente_codigo" name="txt_cliente_codigo" value="0" />
                        <a href="cliente_busca" title="Localizar" class="modal" rel="950-380"><img style="margin-left:3px;" src="image/layout/search.gif" alt="localizar" /></a>
                    </li>
                    
					<li>
                        <label for="txt_cliente_nome">&nbsp;</label>
                        <input type="text" 		id="txt_cliente_nome" 	name="txt_cliente_nome" value="Consumidor" style=" width:315px" readonly="readonly" />
                        <input type="hidden" 	id="hid_cliente_id" 	name="hid_cliente_id"	value="0" />
                    </li>
					
					<li>
                        <label for="txt_chamado_data">Data</label>
                        <input type="text" style="width: 80px; text-align: center" class="calendario-mask" id="txt_chamado_data" name="txt_chamado_data" value="<?=date('d/m/Y')?>" />
                        <a href="#" title="Exibir calend&aacute;rio" class="exibir-calendario-data-atual"></a>
                    </li>
                    
					<li>
                        <label for="txt_chamado_hora">Hora</label>
                        <input type="text" style="width: 80px; text-align: center" id="txt_chamado_hora" name="txt_chamado_hora" value="<?=date('H:i:s')?>" />
                    </li>
					
					<li>
                        <label for="txt_usuario">Autor</label>
                        <input type="text" style="width:146px" id="txt_usuario" name="txt_usuario" value="<?=$rowUsuario['fldUsuario']?>" readonly="readonly" />
						<input type="hidden" id="hid_usuario_id" name="hid_usuario_id" value="<?= $_SESSION['usuario_id'] ?>" />
                    </li>
				</ul>
				
				<ul>
					<li>
						<label for="txt_chamado_descricao">Descri&ccedil;&atilde;o</label>
						<textarea style="width: 870px; height: 100px; padding: 5px;" id="txt_chamado_descricao" name="txt_chamado_descricao"></textarea>
					</li>
					
					<li>
						<label for="sel_chamado_situacao">Situa&ccedil;&atilde;o do chamado</label>
						<select id="sel_chamado_situacao" name="sel_chamado_situacao">
<?						$rsChamadoSituacao = mysql_query("SELECT * FROM tblcrm_acompanhamento_cliente_situacao");							
						while($rowChamadoSituacao = mysql_fetch_array($rsChamadoSituacao)) {
?>
							<option value="<?=$rowChamadoSituacao['fldId']?>"><?=$rowChamadoSituacao['fldSituacao_Nome']?></option>
<?						}
?>						</select>
					</li>
					
					<li>
						<label for="sel_chamado_prioridade">Prioridade do chamado</label>
						<select id="sel_chamado_prioridade" name="sel_chamado_prioridade">
<?						$rsChamadoPrioridade = mysql_query("SELECT * FROM tblcrm_acompanhamento_cliente_prioridade");							
						while($rowChamadoPrioridade = mysql_fetch_array($rsChamadoPrioridade)) {
?>
							<option value="<?=$rowChamadoPrioridade['fldId']?>"><?=$rowChamadoPrioridade['fldPrioridade_Nome']?></option>
<?						}
?>						</select>
					</li>
				</ul>
				
				<fieldset style="padding: 10px; width: 860px;" id="acompanhamento_agenda" disabled="disabled">
					<legend>Agendamento:</legend>
					
					<input type="hidden" id="hid_agendamento" name="hid_agendamento" value="true" />
					
					<ul>
						<li>
							<label for="sel_chamado_agendamento_tipo">Tipo</label>
							<select id="sel_chamado_agendamento_tipo" name="sel_chamado_agendamento_tipo">
<?							$rsChamadoTipo = mysql_query("SELECT fldId, fldTipo_Nome FROM tblcrm_acompanhamento_cliente_tipo WHERE fldExcluido = 0 ORDER BY fldTipo_Nome");

							if(mysql_num_rows($rsChamadoTipo) > 0) {
								while($rowChamadoTipo = mysql_fetch_array($rsChamadoTipo)) {
?>
								<option value="<?=$rowChamadoTipo['fldId']?>"><?=$rowChamadoTipo['fldTipo_Nome']?></option>
<?								}
							}
							else {
?>
								<option value="0">Cadastre antes um tipo de agendamento</option>
<?
							}
?>							</select>
							<a href="crm_acompanhamento_cliente_tipo" class="modal btn_add_dados" rel="480-300" title="Adicionar novo Tipo">Adicionar novo Tipo</a>
						</li>

						<li>
							<label for="sel_chamado_agendamento_funcionario">Funcion&aacute;rio encarregado</label>
							<select id="sel_chamado_agendamento_funcionario" name="sel_chamado_agendamento_funcionario">
<?							$rsChamadoFuncionario = mysql_query("SELECT fldId, fldNome FROM tblfuncionario WHERE fldDisabled = '0' ORDER BY fldNome");							
								
							if(mysql_num_rows($rsChamadoFuncionario) > 0) {
								while($rowChamadoFuncionario = mysql_fetch_array($rsChamadoFuncionario)) {
?>
									<option value="<?=$rowChamadoFuncionario['fldId']?>"><?=$rowChamadoFuncionario['fldNome']?></option>
<?								}
							}
							else {
?>
								<option value="0">N&atilde;o h&aacute; funcion&aacute;rios cadastrados!</option>
<?
							}
?>							</select>
						</li>
						
						<li>
							<label for="txt_chamado_agendamento_data">Data</label>
							<input type="text" style="width: 80px; text-align: center" class="calendario-mask" id="txt_chamado_agendamento_data" name="txt_chamado_agendamento_data" value="" />
							<a href="#" title="Exibir calend&aacute;rio" class="exibir-calendario-data-atual"></a>
						</li>
						
						<li>
							<label for="txt_chamado_agendamento_hora">Hora</label>
							<input type="text" style="width: 80px; text-align: center" class="hora-mask" id="txt_chamado_agendamento_hora" name="txt_chamado_agendamento_hora" value="" />
						</li>
						
					</ul>
					<ul>
						<li>
							<label for="txt_chamado_agendamento_desc">Descri&ccedil;&atilde;o</label>
							<textarea style="width: 645px; height: 60px; padding: 5px;" id="txt_chamado_agendamento_desc" name="txt_chamado_agendamento_desc"></textarea>
						</li>
					</ul>
				</fieldset>
				
                <div style="float:right">
                    <input type="submit" class="btn_enviar" name="btn_gravar" id="btn_gravar" value="Gravar" title="Gravar" />
                </div>
            </form>
		</div>
<?	}
?>        
</div>

<script type="text/javascript">
	$('#txt_cliente_codigo').focus();
</script>