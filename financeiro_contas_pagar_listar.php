<?
	require("financeiro_contas_pagar_filtro.php");

	if(isset($_GET['mensagem']) && $_GET['mensagem'] == "ok"){
?>		<div class="alert">
			<p class="ok">Registro gravado com sucesso!<p>
        </div>
<?	}

	if(isset($_GET['mensagem']) && $_GET['mensagem'] == "excluido"){
?>		
        <div class="alert">
			<p class="ok">Registro exclu&iacute;do!<p>
        </div>
<?	}

	if(isset($_POST["btn_imprimir"])){	
?>		<script language="javascript">
			window.open("financeiro_contas_pagar_relatorio.php");
		</script>
<?	}

/**************************** DAR BAIXA EM UMA CONTA PROGRAMADA *********************************/

	if(isset($_POST['txt_valor_baixa'])){
		
		$sSQLBaixaProgramada = "INSERT INTO tblfinanceiro_conta_pagar_programada_baixa (fldContaProgramada_Id, fldValor, fldVencimento) VALUES(
								'" . $_POST['hid_id'] . "',
								'" . format_number_in($_POST['txt_valor_baixa']) . "',
								'" . $_POST['hid_data'] . "'
								)";
								
		if(mysql_query($sSQLBaixaProgramada)){
			$LastId = mysql_fetch_array(mysql_query("SELECT last_insert_id() AS lastID"));
			$ultimo_id = $LastId['lastID'];
			
			//$descricao, $credito, $debito, $entidade, $pagamento_tipo_id, $referencia_id, $movimento_tipo, $marcador, $conta
			fnc_financeiro_conta_fluxo_lancar('', 0, format_number_in($_POST['txt_valor_baixa']), '', $_POST['hid_pagamento_id'], $ultimo_id, 6, $_POST['hid_marcador'], $_POST['sel_conta']);
			
			if(!empty($_SESSION['ref_timestamp'])){ //CASO A SESSAO ESTIVER EM BRANCO, PRA NAO ATUALIZAR TODOS OS CHEQUES 
				/*
				$destino_movimento_id = '6'; //conta programada
				$timestamp			= $_SESSION['ref_timestamp'];
				$destino_id 		= $ultimo_id;
				fnc_cheque_update('', $destino_id, '', '', $destino_movimento_id, $timestamp);
				
				*/
				###############################################################################################################################################
				
				$timestamp	 = $_SESSION['ref_timestamp'];
				$movimento_id= '6'; //conta programada
				$registro_id = $ultimo_id;
				$conta_id	 = $_POST['sel_conta'];
				$saida		 = '1';
				fnc_cheque_movimento_lancar($timestamp, $movimento_id, $registro_id, $conta_id, $saida);
				
			}
			unset($_SESSION['ref_timestamp']);

?>			<div class="alert">
				<p class="ok">Registro gravado com sucesso!<p>
	        </div>
<?
		}
	}

/*****************************************************************************/
	//definindo a matriz que conter� os dados das tabelas de compras e contas programadas
	$contas = array();
	
	//indice da matriz
	$x = 0;
	
	//vari�veis para calcular os valores
	$totalContasProgramadas = 0.00;
	$totalCompras 			= 0.00;
	$totalGeral 			= 0.00;
	
	//datas
	$dataInicial = ($_SESSION['txt_contas_pagar_data_inicial']) ? format_date_in($_SESSION['txt_contas_pagar_data_inicial']) : '1111-11-11';
	$dataFinal 	 = ($_SESSION['txt_contas_pagar_data_final']) ? format_date_in($_SESSION['txt_contas_pagar_data_final']) : date('Y-m-d', mktime(0,0,0,date('m',strtotime(format_date_in($_SESSION['txt_contas_pagar_data_inicial'])))+30, date('d', strtotime(format_date_in($_SESSION['txt_contas_pagar_data_inicial']))), date('Y', strtotime(format_date_in($_SESSION['txt_contas_pagar_data_inicial'])))));
	
	//buscando todas as contas programadas
	$sSQLContaProgramada = "SELECT tblfinanceiro_conta_pagar_programada.*,
							tblsistema_calendario_intervalo.fldNome AS fldCalendarioNome, 
							tblsistema_calendario_intervalo.fldValor AS StringTime, 
							tblfinanceiro_conta_fluxo_marcador.fldMarcador AS Marcador, 
							tblpagamento_tipo.fldTipo AS TipoPagamento
							FROM tblfinanceiro_conta_pagar_programada
							INNER JOIN tblsistema_calendario_intervalo ON tblfinanceiro_conta_pagar_programada.fldIntervalo_Tipo = tblsistema_calendario_intervalo.fldId
							INNER JOIN tblfinanceiro_conta_fluxo_marcador ON tblfinanceiro_conta_pagar_programada.fldMarcador = tblfinanceiro_conta_fluxo_marcador.fldId
							INNER JOIN tblpagamento_tipo ON tblfinanceiro_conta_pagar_programada.fldPagamento_Id = tblpagamento_tipo.fldId
							WHERE tblfinanceiro_conta_pagar_programada.fldDisabled = '0' 
							AND tblfinanceiro_conta_pagar_programada.fldExcluido = '0' 
							AND (tblfinanceiro_conta_pagar_programada.fldData_Termino >= '0000-00-00' OR (tblfinanceiro_conta_pagar_programada.fldData_Termino BETWEEN '$dataInicial' AND '$dataFinal'))
							" . $_SESSION["filtro_contas_pagar_programadas"];
	
	//executando a consulta de contas programadas
	$rowContaProgramada = mysql_query($sSQLContaProgramada);
	echo mysql_error();
	
	if($_SESSION['sel_conta_tipo'] != 'fornecedor'){
		
		//colocando os dados em uma matriz
		while($contaProgramada = mysql_fetch_array($rowContaProgramada)){
			
			//montando a string inicial para usar no calculo da fun��o strtotime()
			$stringTimeCompleta = '+' . $contaProgramada['fldIntervalo_Frequencia'] . ' ' . $contaProgramada['StringTime'];
			
			//data cadastrada como inicial para a 1� parcela
			if($contaProgramada['fldData_Inicio'] >= $dataInicial){
				$vencimento = $contaProgramada['fldData_Inicio'];
			}
			else{
				$vencimento	= date('Y-m-d', strtotime($stringTimeCompleta, strtotime($contaProgramada['fldData_Inicio'])));
			}
			
			while($vencimento <= $dataFinal){
				
				if($vencimento >= $dataInicial and ($vencimento <= $contaProgramada['fldData_Termino'] or $contaProgramada['fldData_Termino'] == '0000-00-00')){
					
					//procedimento para exibir contas programadas que n�o foram dado baixa
					$sSQLBaixaProgramada = "SELECT fldId FROM tblfinanceiro_conta_pagar_programada_baixa 
											WHERE fldExcluido = 0 AND fldContaProgramada_Id = " . $contaProgramada['fldId'] . " AND fldVencimento = '" . $vencimento . "'";
					$baixaProgramada = mysql_num_rows(mysql_query($sSQLBaixaProgramada));
					
					//filtrar por status
					switch($_SESSION['filtro_contas_pagar_status']){
						case "em aberto":
							$filtroStatus = ($baixaProgramada == 0);
						break;
						
						case "pago":
							$filtroStatus = ($baixaProgramada > 0);
						break;
						
						case "todos":
							$filtroStatus = ($baixaProgramada >= 0);
						break;
						default: $filtroStatus = ($baixaProgramada == 0);
					}
					
					if($filtroStatus){
						
						$contas[$x]['codigo'] 	 	 = str_pad($contaProgramada['fldId'], 4, "0", STR_PAD_LEFT);
						//$contas[$x]['nome'] 	 	 = $contaProgramada['fldCalendarioNome'] . " - " . $contaProgramada['fldNome'];
						$contas[$x]['nome'] 	 	 = $contaProgramada['fldNome'];
						$contas[$x]['marcador']  	 = $contaProgramada['Marcador'];
						$contas[$x]['tipoPagamento'] = $contaProgramada['TipoPagamento'];
						
				//******************************************************************************************//
						
						//PEGA O TOTAL DE PARCELAS DAS CONTAS PROGRAMADAS -- LUCAS 20121023
						$rsParcelaProgramada = mysql_query("select * from tblfinanceiro_conta_pagar_programada WHERE fldId = '". $contaProgramada['fldId'] . "'");
						$rsTipoIntervalo = mysql_query("select * from tblsistema_calendario_intervalo WHERE fldId = '". $contaProgramada['fldIntervalo_Tipo'] ."'");
						$rowParcelaProgramada = mysql_fetch_assoc($rsParcelaProgramada);
						$rowTipoIntervalo = mysql_fetch_assoc($rsTipoIntervalo);
						
						switch ($rowTipoIntervalo['fldValor']) //calculos de intervalo de m�s
						{
							
							case "day": //dia
								
								$Q = 86400;
								
							break;
						
							case "week": //semana
								
								$Q = 604800;
								
							break;
						
							case "month": //m�s
								
								$Q = 2592000;
								
							break;
						
							case "year": //ano
								
								$Q = 86400*365;
								
							break;
							
						}
						
						$dataInicio 			= $rowParcelaProgramada['fldData_Inicio'];
						$dataTermino 			= $rowParcelaProgramada['fldData_Termino'];
						$totalMeses				= round(((strtotime($dataTermino) - strtotime($dataInicio)) / $Q) + 1);
						$contaParcelaProgramada = ceil($totalMeses / $contaProgramada['fldIntervalo_Frequencia']);
						$totalParcelaProgramada	= str_pad($contaParcelaProgramada, 2, "0", STR_PAD_LEFT);
						
						//PEGA A PARCELA ATUAL
						$mesesParcelas = round(((strtotime($vencimento) - strtotime($dataInicio)) / $Q) + 1);
						$contaParcelaProgramada = ceil($mesesParcelas / $contaProgramada['fldIntervalo_Frequencia']);
						$atualParcelaProgramada = str_pad($contaParcelaProgramada, 2, "0", STR_PAD_LEFT);
						//PEGA A PARCELA ATUAL
						//PEGA O TOTAL DE PARCELAS DAS CONTAS PROGRAMADAS
						
						if ($dataTermino == "0000-00-00"){ $contas[$x]['totalParcela'] = "--"; }
						else { $contas[$x]['totalParcela'] = $totalParcelaProgramada;}
						
						if ($dataTermino == "0000-00-00"){ $contas[$x]['parcela'] = "--"; }
						else { $contas[$x]['parcela'] = $atualParcelaProgramada; }
						
				//******************************************************************************************//
						
						//subtraindo o valor aproximado da conta programada com o valor da baixa
						$valorBaixa = mysql_fetch_array(mysql_query($sSQLBaixaProgramada));
						if($valorBaixa['fldValor'] > 0){
							$valor	='';
						}else{
							$valor	= $contaProgramada['fldValor'];
						}
						
						$contas[$x]['valor'] 		 = ($valor < 0) ? '0,00' : $valor; //mostrar 0 caso seja um valor negativo
						$contas[$x]['vencimento'] 	 = $vencimento;
						
						//para exibir ou n�o o link para efetuar a baixa na conta
						if($valor <= 0){
							$contas[$x]['darBaixa'] = '';
						}
						else{
							$contas[$x]['darBaixa'] = '<a href="financeiro_contas_pagar_baixa_programada,' . $contaProgramada['fldId'] . ',' . $vencimento . ',' . $contaProgramada['fldPagamento_Id'] . '" class="dar-baixa modal" title="Dar baixa" rel="330-130">Dar baixa</a>';
						}
						
						//somando o total � pagar
						$totalContasProgramadas += ($valor < 0) ? '0,00' : $valor; //mostrar 0 caso seja um valor negativo
					}
				}
				
				//incrementando a data
				//$stringTimeCompleta = '+' . $contaProgramada['fldIntervalo_Frequencia'] * ($x + 1) . ' ' . $contaProgramada['StringTime'];
				//$vencimento	= date('Y-m-d',strtotime($stringTimeCompleta, strtotime($contaProgramada['fldData_Inicio'])));
				$vencimento	= date('Y-m-d',strtotime($stringTimeCompleta, strtotime($vencimento)));
				
				//incrementando o indice
				$x++;
			}
			
		}
	}//end if session
	
/*****************************************************************************/
	//filtrar por status
	switch($_SESSION['filtro_contas_pagar_status']){
		case "em aberto":
			$filtroStatus = 'HAVING (ValorBaixa + ValorDesconto) < tblcompra_parcela.fldValor OR QtdBaixas = 0';
		break;
		
		case "pago":
			$filtroStatus = 'HAVING (ValorBaixa + ValorDesconto) >= tblcompra_parcela.fldValor';
		break;
			
		case "todos":
			$filtroStatus = '';
		break;
		default: $filtroStatus = 'HAVING  (ValorBaixa + ValorDesconto) < tblcompra_parcela.fldValor OR QtdBaixas = 0';
	}
	
	$sSQLCompras = "SELECT tblfornecedor.fldId as ForncedorId, 
					tblfornecedor.fldNomeFantasia as FornecedorNome, 
					tblpagamento_tipo.fldTipo as TipoPagamento,
					tblfinanceiro_conta_fluxo_marcador.fldMarcador as Marcador,
					COUNT(tblcompra_parcela_baixa.fldValor * (tblcompra_parcela_baixa.fldExcluido * -1 + 1)) AS QtdBaixas, 
					SUM(tblcompra_parcela_baixa.fldValor * (tblcompra_parcela_baixa.fldExcluido * -1 + 1)) AS ValorBaixa, 
					SUM(tblcompra_parcela_baixa.fldDesconto * (tblcompra_parcela_baixa.fldExcluido * -1 + 1)) AS ValorDesconto, 
					tblcompra_parcela.* FROM tblcompra_parcela
					INNER JOIN tblcompra ON tblcompra_parcela.fldCompra_Id = tblcompra.fldId
					INNER JOIN tblfornecedor ON tblfornecedor.fldId = tblcompra.fldFornecedor_Id
					INNER JOIN tblpagamento_tipo ON tblpagamento_tipo.fldId = tblcompra_parcela.fldPagamento_Id
					LEFT JOIN tblfinanceiro_conta_fluxo_marcador ON tblfinanceiro_conta_fluxo_marcador.fldId = tblcompra.fldMarcador
					LEFT JOIN tblcompra_parcela_baixa ON tblcompra_parcela.fldId = tblcompra_parcela_baixa.fldParcela_Id
					WHERE tblcompra_parcela.fldExcluido = '0' AND tblcompra_parcela.fldVencimento BETWEEN '$dataInicial' AND '$dataFinal' " . $_SESSION['filtro_contas_pagar_compras'] .
					" GROUP BY tblcompra_parcela.fldId ".$filtroStatus." ORDER BY fldVencimento DESC";
	
	//executando a consulta em compras
	$rowCompra = mysql_query($sSQLCompras);
	echo mysql_error();
	
	if($_SESSION['sel_conta_tipo'] != 'programadas'){
		//colocando os dados em uma matriz
		while($contaFornecedor = mysql_fetch_array($rowCompra)){
			
			$contas[$x]['codigo'] 	 	 = str_pad($contaFornecedor['fldCompra_Id'], 4, "0", STR_PAD_LEFT);
			$contas[$x]['nome'] 	 	 = $contaFornecedor['FornecedorNome'];
			$contas[$x]['marcador']  	 = $contaFornecedor['Marcador'];
			$contas[$x]['tipoPagamento'] = $contaFornecedor['TipoPagamento'];
			$contas[$x]['parcela'] 		 = str_pad($contaFornecedor['fldParcela'], 2, "0", STR_PAD_LEFT);
			$contas[$x]['valor'] 		 = $contaFornecedor['fldValor'] - $contaFornecedor['ValorBaixa'];
			$contas[$x]['vencimento'] 	 = $contaFornecedor['fldVencimento'];
			//PEGA O TOTAL DE PARCELAS -- LUCAS 20121023
			$rsParcela = mysql_query("select * from tblcompra_parcela WHERE fldCompra_Id = '". $contaFornecedor['fldCompra_Id'] . "'");
			
			$rowParcela = mysql_num_rows($rsParcela);
			
			$parcelaTotal = str_pad($rowParcela, 2, "0", STR_PAD_LEFT);
			//PEGA O TOTAL DE PARCELAS
			$contas[$x]['totalParcela']	 = $parcelaTotal;
			
			//para exibir ou n�o o link para efetuar a baixa na conta
			if($contas[$x]['valor'] <= 0){
				$contas[$x]['darBaixa'] = '';
			}
			else{
				$contas[$x]['darBaixa'] = '<a href="?p=fornecedor_detalhe&amp;id='. $contaFornecedor['ForncedorId'] .'&amp;modo=compra_parcela_editar&amp;filtro='. $contaFornecedor['fldId'] .'" class="dar-baixa" title="Dar baixa">Dar baixa</a>';
			}
			
			//somando o total � pagar
			$totalCompras += $contaFornecedor['fldValor'] - $contaFornecedor['ValorBaixa'];
			//incrementando o indice
			$x++;
			
		}
	}

/****************************** ORDENANDO OS REGISTROS *******************************************/
	if(isset($_GET['order'])){
		
		$filtro_order = $_GET['order'];
		
		if($_SESSION['order_contas_pagar'] == $filtro_order." desc"){
			$_SESSION['order_contas_pagar'] = $filtro_order." asc";
		}elseif($_SESSION['order_contas_pagar'] == $filtro_order." asc"){
			$_SESSION['order_contas_pagar'] = $filtro_order." desc";
		}else{
			$_SESSION['order_contas_pagar'] = $filtro_order." asc";
		}
	}
/**************************************************************************************************/

	($_SESSION['order_contas_pagar'] == "") ? $_SESSION['order_contas_pagar'] = 'vencimento asc' : '';
	
	//definir icone para ordem
	$order_sessao = explode(" ", $_SESSION['order_contas_pagar']);
	$raiz = "index.php?p=financeiro_contas_pagar&amp;order=";
	
	if($order_sessao[1] == "desc"){
		$class = 'desc';
	}elseif($order_sessao[1] == "asc"){
		$class = 'asc';
	}
	
	if($class == "asc"){
		function ordenarVencimento($a, $b) {
			if ($a['vencimento'] == $b['vencimento']) {  
				return 0;
			} 
			return ($a['vencimento'] < $b['vencimento']) ? false : true; 
		}
		
		function ordenarNome($a, $b) {
			if ($a['nome'] == $b['nome']) {  
				return 0;
			} 
			return ($a['nome'] < $b['nome']) ? false : true;
		}
		
		function ordenarMarcador($a, $b) {
			if ($a['marcador'] == $b['marcador']) {  
				return 0;
			}
			return ($a['marcador'] < $b['marcador']) ? false : true;  
		}
		
		function ordenarParcela($a, $b) {
			if ($a['parcela'] == $b['parcela']) {  
				return 0;
			}
			return ($a['parcela'] < $b['parcela']) ? false : true;  
		}
		
		function ordenarValor($a, $b) {
			if ($a['valor'] == $b['valor']) {  
				return 0;
			}  
			return ($a['valor'] < $b['valor']) ? false : true;
		}
	}elseif($class == "desc"){
		function ordenarVencimento($a, $b) {
			if ($a['vencimento'] == $b['vencimento']) {  
				return 0;
			} 
			return ($a['vencimento'] > $b['vencimento']) ? false : true; 
		}
		
		function ordenarNome($a, $b) {
			if ($a['nome'] == $b['nome']) {  
				return 0;
			} 
			return ($a['nome'] > $b['nome']) ? false : true;
		}
		
		function ordenarMarcador($a, $b) {
			if ($a['marcador'] == $b['marcador']) {  
				return 0;
			}
			return ($a['marcador'] > $b['marcador']) ? false : true;  
		}
		
		function ordenarParcela($a, $b) {
			if ($a['parcela'] == $b['parcela']) {  
				return 0;
			}
			return ($a['parcela'] > $b['parcela']) ? false : true;  
		}
		
		function ordenarValor($a, $b) {
			if ($a['valor'] == $b['valor']) {  
				return 0;
			}  
			return ($a['valor'] > $b['valor']) ? false : true;
		}
	}
	
	switch($order_sessao[0]){
		case "vencimento":
			usort($contas, 'ordenarVencimento');
		break;
		
		case "descricao":
			usort($contas, 'ordenarNome');
		break;

		case "marcador":
			usort($contas, 'ordenarMarcador');
		break;
		
		case "parcela":
			usort($contas, 'ordenarParcela');
		break;

		case "valor":
			usort($contas, 'ordenarValor');
		break;
	}
	
	$_SESSION['contas_pagar_relatorio'] = $contas;
	$_SESSION['contas_pagar_relatorio_total_programada'] = $totalContasProgramadas;
	$_SESSION['contas_pagar_relatorio_total_compra'] = $totalCompras;
	
#########################################################################################
?>
    <form style="clear: both;" class="table_form" action="index.php?p=financeiro_contas_pagar" method="post">
    	<div id="table">
            <div id="table_cabecalho">
                <ul class="table_cabecalho">
                    <li class="order" style="width:100px; text-align:center">
                    	<a <? ($order_sessao[0] == 'vencimento') ? print "class='$class'" : '' ?> style="width:85px" href="<?=$raiz?>vencimento">Vencimento</a>
                    </li>
                    <li class="order" style="width:310px">
                    	<a <? ($order_sessao[0] == 'descricao') ? print "class='$class'" : '' ?> style="width:295px" href="<?=$raiz?>descricao">Descri&ccedil;&atilde;o</a>
                    </li>
                    <li class="order" style="width:150px">
                    	<a <? ($order_sessao[0] == 'marcador') ? print "class='$class'" : '' ?> style="width:135px" href="<?=$raiz?>marcador">Marcador</a>
                    </li>
                    <li class="order" style="width:70px; text-align:center">
                    	<a <? ($order_sessao[0] == 'parcela') ? print "class='$class'" : '' ?> style="width:55px" href="<?=$raiz?>parcela">Parcela</a>
                    </li>
					<li style="width:115px;">Pagamento</li>
					<li style="width:80px; text-align:center">Baixa</li>
                    <li class="order" style="width:90px; text-align:right">
                    	<a <? ($order_sessao[0] == 'valor') ? print "class='$class'" : '' ?> style="width:75px" href="<?=$raiz?>valor">Valor</a>
                    </li>
                </ul>
            </div>
            <div id="table_container">       
                <table id="table_general" class="table_general" summary="Lista de contas a receber">
                <tbody>
<?					$data_atual = date('Y-m-d');
					$linha = "row";
					foreach($contas as $conta){						
						
?>							<tr class="<?= $linha; ?>">
								<td style="width:100px; text-align:center" <?= ($conta['vencimento'] < $data_atual)? "class='vencido'" : "" ?>><?=format_date_out($conta['vencimento'])?></td>
								<td style="width:320px;"<?=($conta['valor'] <= 0) ? ' class="conta_paga"' : ''?> <?= ($conta['vencimento'] < $data_atual)? "class='vencido'" : "" ?>><?=$conta['nome']?></td>
								<td style="width:150px;"><?=$conta['marcador']?></td>
								<td style="width:70px; text-align:center"><?=$conta['parcela']?>/<?=$conta['totalParcela'];?></td> <!-- LUCAS 20121023 -->
								<td style="width:115px"><?=$conta['tipoPagamento']?></td>
								<td style="width:80px; text-align:center"><?=$conta['darBaixa']?></td>
								<td style="width:90px; text-align:right"<?=($conta['valor'] <= 0) ? ' class="conta_paga"' : ''?>><?=format_number_out($conta['valor'])?></td>
							</tr>
<?						$linha = ($linha == "row" ? "dif-row" : "row");
				    }
					
?>		 		</tbody>
				</table>
            </div>
            
			<div id="table_action" style="margin-left: 190px">
             	<ul id="action_button">
                    <li><button style="margin-top:3px" id="btn_print" name="btn_imprimir" value="imprimir" title="Imprimir relat&oacute;rio" ></button></li>
                </ul>
            </div>
            
			<div class="saldo" style="width: 710px; float: right;">
				<p style="padding: 5px 5px 5px 10px; width: 270px; float: left;">Total contas programadas<span style="margin-left: 10px; width: 70px; text-align: center;" class="credito"><?=format_number_out($totalContasProgramadas)?></span></p>
				<p style="padding: 5px 5px 5px 10px; width: 240px; float: left;">Total fornecedores <span style="margin-left: 10px; width: 70px; text-align: center;" class="credito"><?=format_number_out($totalCompras)?></span></p>
				<p style="padding: 5px 5px 5px 10px;">Total &agrave; pagar <span style="margin-left: 10px; width: 70px; text-align: center;" class="credito"><?=format_number_out($totalContasProgramadas + $totalCompras)?></span></p>
			</div>
        
        </div>
	</form>
	
<?
	unset($contas, $conta);
?>