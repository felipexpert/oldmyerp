<?
	ob_start();
	session_start();
?>
    <link rel="stylesheet" type="text/css" media="screen" href="style/style_index.css"></link>
    <link rel="stylesheet" type="text/css" media="screen" href="style/style_home.css"></link>
    <link rel="stylesheet" type="text/css" media="screen" href="style/style_table.css"></link>
    <link rel="stylesheet" type="text/css" media="screen" href="style/style_form.css"></link>
    <link rel="stylesheet" type="text/css" media="screen" href="style/style_busca.css"></link>
    
	<script type="text/javascript" src="js/jquery.js">
    </script>
	<script type="text/javascript">
	$('document').ready(function(){
			$('#buscar').focus();
			$('#buscar').select();
			$('#loading').hide();
			$('#buscar').click(function(){
					$('#buscar').val('');
			});
			$('#buscar').keyup(function(){
					$('#loading').ajaxStart(function(){
							$('#alvo').hide();
							$('#loading').show();   
					});
					$('#loading').ajaxStop(function(){
							$('#loading').hide();   
					});
					$.post('produto_busca_consulta.php',
					{busca: $('#buscar').val()},
					function(data){
							if ($('#buscar').val()!=''){
									$('#alvo').show();
									$('#alvo').empty().html(data);
							}
							else{
									$('#alvo').empty();
							}
					});
			});
	});
	</script>
    	
<?
	require("inc/con_db.php");
	
	//retornar com produto
	if(isset($_GET["produto_codigo"])){
		$_SESSION["produto_codigo"] = $_GET["produto_codigo"];
?>		<script type="text/javascript">window.close();</script>
<?	}
?>
    <form id="frm_busca">
    <fieldset>
    	<legend>Busca de produto</legend>
            <input type="text" id="buscar" value="Digite o nome do produto" size="50" >
            <div id="loading"><img src="image/layout/carregando.gif"> </div>
            <ul id="busca_cabecalho">
            	<li style="width:100px">c&oacute;digo</li>
                <li style="width:250px">Produto</li>
                <li style="width:200px">Marca</li>
                <li style="width:80px">Valor</li>
            </ul>
            <div id="alvo"></div>
    	</fieldset>
    </form>
              
   