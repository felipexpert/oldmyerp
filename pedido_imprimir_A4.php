<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
      	<title>myERP - Imprimir Venda</title>
         <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="Robots" content="none" />
    
        <link rel="stylesheet" type="text/css" media="all" href="style/impressao/style_pedido_imprimir_A4.css" />
        <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />
        
	</head>
	<body>
<?
	ob_start();
	session_start();
	//conectar ao db
	require_once("inc/con_db.php");
	require_once("inc/fnc_general.php");
	require_once("inc/fnc_ibge.php");
	
	$raiz 		= $_GET['raiz'];
	$pedido_id  = $_GET['id'];
	$data 		= date("Y-m-d");
	$rsPedido  	= mysql_query("SELECT SUM((tblpedido_item.fldValor * (tblpedido_item.fldExcluido * -1 + 1)) * tblpedido_item.fldQuantidade) as fldPedidoValor,
								 tblpedido.*,tblpedido.fldId as fldPedidoId, tblcliente.*, tblcliente.fldId as fldClienteId, tblpedido.fldObservacao as fldObservacaoPedido
								 FROM tblpedido 
								 LEFT JOIN tblpedido_item ON tblpedido.fldId = tblpedido_item.fldPedido_Id
								 INNER JOIN tblcliente ON tblpedido.fldCliente_Id = tblcliente.fldId
								 WHERE tblpedido.fldId = $pedido_id GROUP BY tblpedido_item.fldPedido_Id");
	$rowPedido 	= mysql_fetch_array($rsPedido);
	
	$endereco 	= $rowPedido['fldEndereco'];
	$bairro 	= $rowPedido['fldBairro'];
	$municipio 	= fnc_ibge_municipio($rowPedido['fldMunicipio_Codigo']);
	$uf 	 	= fnc_ibge_uf_sigla($rowPedido['fldMunicipio_Codigo']);
	$CPFCNPJ 	= formatCPFCNPJTipo_out($rowPedido['fldCPF_CNPJ'], $rowPedido['fldTipo']);
	
	if($rowPedido['fldFuncionario_Id']){
		$rsFuncionario  = mysql_query("select * from tblfuncionario where fldId = ".$rowPedido['fldFuncionario_Id']);
		$rowFuncionario = mysql_fetch_array($rsFuncionario);
	}

	/*----------------------------------------------------------------------------------------------*/
	$rsEmpresa  		= mysql_query("SELECT * FROM tblempresa_info");
	$rowEmpresa 		= mysql_fetch_array($rsEmpresa);
	
	if($rowPedido['fldVeiculo_Id']){
		$rsVeiculo = mysql_query("SELECT tblpedido.fldVeiculo_Id,
								 tblcliente_veiculo.*
								 FROM tblcliente_veiculo RIGHT JOIN tblpedido ON tblcliente_veiculo.fldId = tblpedido.fldVeiculo_Id
								 WHERE tblpedido.fldId = $pedido_id");
		
		$rowVeiculo = mysql_fetch_array($rsVeiculo);
		echo mysql_error();
		$veiculo 	= $rowVeiculo['fldVeiculo'];
		$placa 		= $rowVeiculo['fldPlaca'];
	}
	
	/*----------------------------------------------------------------------------------------------*/
	#CRIANDO RODAPE  !!!########################################################################################################################################
	$sSQL = "SELECT SUM(tblpedido_parcela_baixa.fldValor * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldBaixaValor
			FROM tblpedido_parcela 
			LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
			WHERE tblpedido_parcela.fldPedido_Id = $pedido_id GROUP BY tblpedido_parcela.fldPedido_Id";
	$rsPedidoBaixa 	= mysql_query($sSQL);
	$rowPedidoBaixa	= mysql_fetch_array($rsPedidoBaixa);
	
	//Para trazer o desconto dos itens e calcular o subtotal
	$ItemDesconto 			= mysql_fetch_array(mysql_query("SELECT SUM((fldValor * fldQuantidade * fldDesconto) / 100) AS ItemDesconto FROM tblpedido_item WHERE tblpedido_item.fldPedido_Id = ".$rowPedido['fldPedidoId']));
	
	$total_pedido 			= $rowPedido['fldPedidoValor'] + $rowPedido['fldComissao'];
	$subtotal_pedido		= $total_pedido - $ItemDesconto['ItemDesconto']; // considerando o desconto nos itens
	$desconto_pedido 		= $rowPedido['fldDesconto'];
	$desconto_reais_pedido 	= $rowPedido['fldDescontoReais'];
	$desconto 				= ($subtotal_pedido * $desconto_pedido) / 100;
	$total_descontos 		= $desconto + $desconto_reais_pedido;
	$total_pedido_apagar 	= ($subtotal_pedido - $total_descontos);
	$total_baixa 			= $rowPedidoBaixa['fldBaixaValor'];
	$valor_devedor 			= $total_pedido_apagar - $rowPedidoBaixa['fldBaixaValor'];
	
	$rodape = "  
  	<table id='pedido_pagamento'>
		<tr style='height:3cm; margin-bottom:0.5cm'><td>
        	<table class='parcelas' style='padding:0;margin=0'>
				<tr><td><h2 style='width:15cm'>Parcelas</h2></td></tr>
				<tr style='margin:0;'><td>
					<table class='parcela_desc'>";
						$sSQL = "SELECT tblpedido_parcela.*, tblpagamento_tipo.fldSigla as fldForma_Pagamento, SUM(tblpedido_parcela_baixa.fldValor * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldTotalBaixa
						FROM tblpedido_parcela LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
						LEFT JOIN tblpagamento_tipo ON tblpedido_parcela.fldPagamento_Id = tblpagamento_tipo.fldId
						WHERE tblpedido_parcela.fldPedido_Id = $pedido_id GROUP BY tblpedido_parcela.fldId ORDER BY tblpedido_parcela.fldParcela";
						$rsParcela 	= mysql_query($sSQL);
						$rows 		= mysql_num_rows($rsParcela);
						$limite 	= ceil($rows / 2);
						$x 			= 1;
						$n 			= 1;
						$rsParcela 	= mysql_query($sSQL);
						//VAI VERIFICAR SE X É IGUAL AO LIMITE, METADE DOS REGISTROS, E ENTÃO PULAR PARA A DIV DO LADO
						while($rowParcela = mysql_fetch_array($rsParcela)){
							$parcela 	= str_pad($rowParcela['fldParcela'], 2, "0", STR_PAD_LEFT);
							$venc	 	= format_date_out($rowParcela['fldVencimento']);
							$valor 	 	= format_number_out($rowParcela['fldValor']);
							$pago 	 	= format_number_out($pago);
							$forma_pg 	= $rowParcela['fldForma_Pagamento'];
							if($parcela){
								$Col[$x][$n] = array(
									'parcela' 	=> $parcela,
									'venc' 		=> $venc,
									'valor' 	=> $valor,
									'pago' 		=> $pago,
									'forma_pg' 	=> $forma_pg
								);
								if($n == $limite){
									$x = 2;
									$n = 1;
								}else{
									$n ++;
								}
							}
						}
						$n = 1;
						$x = 1;
						$col = count($Col[$x]);
						while($col >= $n){
							if($Col[$x][$n]['parcela'] > 0){
								$rodape .= "  
								<tr>
									<td class='parcela'>".$Col[$x][$n]['parcela']."</td>                
									<td style='width:75px;'>".$Col[$x][$n]['venc']."</td>    
									<td style='width:70px; text-align:right'>".$Col[$x][$n]['valor']."</td>              
									<td style='width:65px; text-align:right'>PG: ".$Col[$x][$n]['pago']."</td>              
									<td style='width:30px; color:green; text-align:center'>".$Col[$x][$n]['forma_pg']."</td>
								</tr>";
							}
							($x == 2) ? $n++ : '';
							$x = ($x == 1) ? 2 : 1;	
						}
						$rodape .= "  	
					</table></td>
				</tr>
			</table></td>
        </tr>
        <tr class='pedido_total'>
            <td><strong>Total: 		</strong><span>R$ ".format_number_out($subtotal_pedido)."	</span></td>
            <td><strong>Desconto: 	</strong><span>R$ ".format_number_out($total_descontos)."</span></td>
            <td><strong>Pago: 		</strong><span>R$ ".format_number_out($total_baixa)."	</span></td>
            <td><strong>A pagar: 	</strong><span>R$ ".format_number_out($valor_devedor)."	</span></td>
        </tr>
    </table>";
?>
    <div id="no-print">
        <div id="impressao_cabecalho">
            <ul id="bts">
                <li><a href="#" onclick ="window.print()"><span>Imprimir</span></a></li>
                <li><a href="index.php?p=pedido&amp;mensagem=ok"><span>Finalizar</span></a></li>
            </ul>
        </div>
	</div>
    <table id="pedido_imprimir">
<?

	//PEGA O STATUS DO PEDIDO
	$status_n 	= $rowPedido['fldStatus'];
	$rsStatus 	= mysql_query("SELECT * FROM tblpedido_status WHERE fldId = $status_n");
	$rowStatus 	= mysql_fetch_array($rsStatus);
	$txtStatus 	= $rowStatus['fldStatus'];
	//FIM DA VERIFICACAO DE STATUS

	if(fnc_sistema('impressao_mostrar_dados_empresa') == '1'){
		$empresa_razao_social = $rowEmpresa['fldRazao_Social'];
		$CPFCNPJ_Empresa 	= 'CNPJ '.formatCPFCNPJTipo_out($rowEmpresa['fldCPF_CNPJ'], $rowEmpresa['fldTipo']);
		$endereco_empresa 	= $rowEmpresa['fldEndereco'];
		$numero_empresa		= $rowEmpresa['fldNumero'];
		$bairro_empresa 	= $rowEmpresa['fldBairro'];
		$municipio_empresa	= fnc_ibge_municipio($rowEmpresa['fldMunicipio_Codigo']);
		$uf_empresa			= fnc_ibge_uf_sigla($rowEmpresa['fldMunicipio_Codigo']);
		$empresa_telefone1  = $rowEmpresa['fldTelefone1'];
		$empresa_telefone2  = $rowEmpresa['fldTelefone2'];
		$endereco_completo  = $endereco_empresa.', '.$numero_empresa.' - '.$bairro_empresa.' • '.$municipio_empresa.'/'.$uf_empresa;
	}else{
		$empresa_razao_social = '';
		$CPFCNPJ_Empresa 	= '';
		$endereco_empresa 	= '';
		$numero_empresa		= '';
		$bairro_empresa 	= '';
		$municipio_empresa	= '';
		$uf_empresa			= '';
		$empresa_telefone1  = '';
		$empresa_telefone2  = '';
	}


	$cabecalho = 
		'<tr><td>
            <table class="cabecalho">';
			if(fnc_sistema('pedido_impressao_exibir_logo') == 1){
				$cabecalho .='
                <tr style="width:200px;float:left"><td class="img"><img src="image/layout/logo_empresa.jpg" alt="logo" /></td></tr>
                <tr>
                    <td>'.$empresa_razao_social.'</td>
                    <td>'.$CPFCNPJ_Empresa.'</td>
                    <td>'.$endereco_completo.'</td>
                    <td>'.$empresa_telefone1.' '.$empresa_telefone2.'</td>
                </tr>';
			}else{
				$cabecalho .='
                <tr style="width:750px">
                    <td style="width:750px">'.$empresa_razao_social.'</td>
                    <td style="width:750px">'.$CPFCNPJ_Empresa.'</td>
                    <td style="width:750px">'.$endereco_completo.'</td>
                    <td style="width:750px">'.$empresa_telefone1.' '.$empresa_telefone2.'</td>
                </tr>';
			}
			$cabecalho .='
            </table>
		</td></tr>
        <tr>
        	<td>
                <table class="pedido_imprimir_dados">
                	<tr><td>
                        <table class="pedido_dados">
                            <tr><td><h2><strong>Funcion&aacute;rio:</strong> '.$rowFuncionario['fldNome'].'</h2></td></tr>
                            <tr class="dados">
                                <td style="width:6.5cm"><strong>C&oacute;d. Venda: 	</strong>'.$rowPedido['fldPedidoId'].'</td>
                                <td style="width:6.5cm"><strong>Data Venda: 		</strong>'.format_date_out($rowPedido['fldPedidoData']).'</td>
								<td style="width:6.5cm"><strong>Status:  </strong>'.$txtStatus.'</td>
								<td style="width:20cm"><strong>Cliente: 			</strong>'.$rowPedido['fldNome'].'</td>';
								if(fnc_sistema('pedido_rgcpf_cliente_impressao') == 1){
                               		$cabecalho .='<td style="width:6.5cm"><strong>CPF/CNPJ: 		</strong>'.$CPFCNPJ.'</td>';
								}
                                $cabecalho .='	<td style="width:6.5cm"><strong>Telefone 1: 		</strong>'.$rowPedido['fldTelefone1'].'</td>
                                				<td style="width:6.5cm"><strong>Telefone 2: 		</strong>'.$rowPedido['fldTelefone2'].'</td>';
								
								if(fnc_sistema('pedido_exibir_endereco') == 1){
                                	$cabecalho .=  '<td style="width:6.5cm"><strong>End.: 			</strong>'.$rowPedido['fldEndereco']. ' '. $rowPedido['fldNumero'].'</td>
													<td style="width:6.5cm"><strong>Bairro: 		</strong>'.$rowPedido['fldBairro'].'</td>
													<td style="width:6.5cm"><strong>CEP: 			</strong>'.$rowPedido['fldCEP'].'</td>
													<td style="width:6.5cm"><strong>Cidade:			</strong>'.$municipio.'</td>
													<td style="width:13cm"><strong>UF: 				</strong>'.$uf.'</td>';
								}								
           						if($_SESSION["sistema_tipo"]=="automotivo"){
       	        					$cabecalho .= 	'<td style="width:6.5cm"><strong>Veiculo: </strong>'.$veiculo.'</td>
													<td style="width:6.5cm"><strong>Placa: </strong>'.$placa.'</td>';
								}
								$cabecalho .='
                            </tr>
                        </table> 
                    </td></tr>';
					echo $cabecalho;
					
					//TAMANHO DOS CAMPOS
					$exibirMarca = mysql_num_rows(mysql_query("SELECT * FROM tblsistema WHERE fldParametro = 'impressao_exibir_marca' AND fldValor = 1"));
					$exibirCategoria = mysql_num_rows(mysql_query("SELECT * FROM tblsistema WHERE fldParametro = 'impressao_exibir_categoria' AND fldValor = 1"));
					if($exibirMarca && $exibirCategoria)
					{	$tamanhoDesc 	= 'width:5.5cm';
						$tamanhoMarca 	= 'width:3cm';
						$tamanhoCat		= 'width:3cm';	}
					if($exibirMarca && !$exibirCategoria)
					{	$tamanhoDesc 	= 'width:7.9cm';
						$tamanhoMarca 	= 'width:3.6cm';
						$tamanhoCat		= 'width:3.6cm';	}
					if(!$exibirMarca && $exibirCategoria)
					{	$tamanhoDesc 	= 'width:7.9cm';
						$tamanhoMarca 	= 'width:3.6cm';
						$tamanhoCat		= 'width:3.6cm';	}
					if(!$exibirMarca && !$exibirCategoria)
					{	$tamanhoDesc 	= 'width:11.64cm';
						$tamanhoMarca 	= 'width:3.6cm';
						$tamanhoCat		= 'width:3.6cm';	}
					//TAMANHO DOS CAMPOS
					   
					$cabecalhoItem = '
					<tr>
                    	<td>
                        	<table class="pedido_descricao">
                            	<tr><td><h2>Produtos:</h2></td></tr>
                                <tr class="descricao">
                                    <td style="width:1.2cm">Qtde</td>
                                    <td style="width:1.8cm">C&oacute;d.</td>';
									if(mysql_num_rows(mysql_query("SELECT * FROM tblsistema_impressao_campo WHERE fldCampo = 'marca' AND fldImpressao = 'venda' AND fldExibir = 1"))){				                                        
										$cabecalhoItem .= '<td style="'.$tamanhoDesc.'">Descric&atilde;o</td>';
										if(mysql_num_rows(mysql_query("SELECT * FROM tblsistema WHERE fldParametro = 'impressao_exibir_marca' AND fldValor = 1"))){
										$cabecalhoItem .= '<td style="'.$tamanhoMarca.'">Marca</td>'; }
										if(mysql_num_rows(mysql_query("SELECT * FROM tblsistema WHERE fldParametro = 'impressao_exibir_categoria' AND fldValor = 1"))){
										$cabecalhoItem .= '<td style="'.$tamanhoCat.'">Categoria</td>'; }
									}elseif(mysql_num_rows(mysql_query("SELECT * FROM tblsistema_impressao_campo WHERE fldCampo = 'fardo' AND fldImpressao = 'venda' AND fldExibir = 1"))){
										$cabecalhoItem .= '<td style="width:8cm">Descric&atilde;o</td>';
										$cabecalhoItem .= '<td style="width:3.5cm">Fardo</td>';
									}else{
										$cabecalhoItem .= '<td style="width:11.8cm">Descric&atilde;o</td>';
									}
                                    $cabecalhoItem .='
                                    <td style="width:1.5cm;text-align:right">Unit.</td>
                                    <td style="width:1.5cm;text-align:right">Desc(%)</td>
                                    <td style="width:1.5cm;text-align:right">Total</td>
                                </tr>';
								echo $cabecalhoItem;
								
								$vendaDecimal 		= fnc_sistema('venda_casas_decimais');
								$quantidadeDecimal 	= fnc_sistema('quantidade_casas_decimais');
								$n = 1;
								$rsItem 		= mysql_query("SELECT tblproduto.fldCodigo, tblpedido_item.*, tblmarca.fldNome as fldMarca, tblcategoria.fldNome as fldCategoria
															  FROM tblproduto
															  LEFT JOIN tblmarca ON tblproduto.fldMarca_Id = tblmarca.fldId
															  LEFT JOIN tblcategoria ON tblproduto.fldCategoria_Id = tblcategoria.fldId
															  INNER JOIN tblpedido_item ON tblproduto.fldId = tblpedido_item.fldProduto_Id
															  WHERE tblpedido_item.fldPedido_Id = ".$rowPedido['fldPedidoId']);
								echo mysql_error();
								$rowsItem		= mysql_num_rows($rsItem);
								while($rowItem 	= mysql_fetch_array($rsItem)){
									$x+= 1;
									
									$valor		 	= $rowItem['fldValor'];
									$qtde 			= $rowItem['fldQuantidade'];
									$desconto 		= $rowItem['fldDesconto'];
									$total 			= $valor * $qtde;
									$descontoItem 	= ($total * $desconto) / 100;
									$totalItem 		= $total - $descontoItem;
									
									//AQUI BUSCAR DADOS DA NFE
									$rowItemNFe = mysql_fetch_array(mysql_query("SELECT * FROM tblpedido_item_fiscal WHERE fldItem_Id= " .$rowItem['fldId']));
									$complemento = (isset($rowItemNFe['fldinformacoes_adicionais'])) ? ' - '.$rowItemNFe['fldinformacoes_adicionais']: '' ;
									
									#FAZER CONTROLE DE QUEBRA DE LINHAS POR CARACTERES DO ITEM
									$limiteLinha = 47; #limite de caracter por linha do item
									$countItem 	 = strlen($rowItem['fldDescricao'].$complemento);
									$linhas 	 += ceil($countItem / $limiteLinha);		
									
									$limiteFolha = 20; #MAXIMO DE LINHAS POR FOLHA - REFERENTE A ITENS - COM RODAPE
									
?>									<tr class="pedido_item">	
										<td style="width:1.2cm"><?=format_number_out($rowItem['fldQuantidade'],$quantidadeDecimal)?></td>
                                        <td style="width:1.8cm"><?=$rowItem['fldCodigo']?></td>
<?										if(mysql_num_rows(mysql_query("SELECT * FROM tblsistema_impressao_campo WHERE fldCampo = 'marca' AND fldImpressao = 'venda' AND fldExibir = 1"))){				                                        
?>                                      	<td style="<?=$tamanhoDesc?>"><?=$rowItem['fldDescricao'].$complemento?></td>
                                            <? if(mysql_num_rows(mysql_query("SELECT * FROM tblsistema WHERE fldParametro = 'impressao_exibir_marca' AND fldValor = 1"))){ ?>
											<td style="<?=$tamanhoMarca?>">&nbsp;<?=substr($rowItem['fldMarca'],0,20)?></td>
                                            <? } if(mysql_num_rows(mysql_query("SELECT * FROM tblsistema WHERE fldParametro = 'impressao_exibir_categoria' AND fldValor = 1"))){ ?>
											<td style="<?=$tamanhoCat?>">&nbsp;<?=substr($rowItem['fldCategoria'],0,20)?></td>
                                            <? } ?>
<?										}elseif(mysql_num_rows(mysql_query("SELECT * FROM tblsistema_impressao_campo WHERE fldCampo = 'fardo' AND fldImpressao = 'venda' AND fldExibir = 1"))){
?>											<td style="width:8cm"><?=$rowItem['fldDescricao'].$complemento?></td>
											<td style="width:3.5cm">&nbsp;<?=$rowItem['fldCodigoFardo']?></td>
<?										}else{
?>											<td style="width:11.8cm"><?=$rowItem['fldDescricao'].$complemento?></td>
<?										}
?>                                      <td style="width:1.5cm;text-align:right"><?=format_number_out($rowItem['fldValor'],$vendaDecimal)?></td>
                                        <td style="width:1.5cm;text-align:right"><?=format_number_out($desconto)?></td>
                                        <td style="width:1.5cm;text-align:right"><?=format_number_out($totalItem)?></td>
                    				</tr>
<?									$total_item += $totalItem;
									$itemN ++;				

									if($linhas >= 37 || $linhas > $limiteFolha && $rowsItem == $itemN){
										echo '</table></td></tr></table>';
										echo '<table id="pedido_imprimir" style="page-break-before: always">';
										echo $cabecalho;
										echo $cabecalhoItem;
										$linhas = 0;
									}elseif($rowsItem == $itemN && $linhas < $limiteFolha){
										$restante = $limiteFolha - $linhas; $x=1;
										while($x <= $restante){
											echo '<tr class="pedido_item"><td style="height:17px"></td></tr>';
											$x ++;
										}
									}
								}
?>							</table>
						</td>
					</tr>
                    <tr><td>
                        <table id="pedido_observacao">
                            <tr><td>
                                <h2>Observa&ccedil;&atilde;o</h2>
                                <p><?=$rowPedido['fldObservacaoPedido']?></p>
                            </td></tr>                                
                        </table>
                	</td></tr>
            		<tr><td><? echo $rodape;?></td></tr>
        		</table>
            </td>
		</tr>
        <tr><td>
        	<table class="pedido_nota">
                <tr>
                    <td><p class="ass_cliente">Assinatura do Cliente</p></td>
                    <td style="width: 50px">&nbsp;</td>
                    <td><p class="ass_funcionario">Assinatura do Funcion&aacute;rio</p></td>
                </tr>
            </table>
        </td></tr>
	</table>
    </body>
</html>