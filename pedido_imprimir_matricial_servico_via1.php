<?
	$cabecalhoItem 	.="--------------------------------------------------------------------------------\r\n";
	$cabecalhoItem 	.= "<b>";
	$cabecalhoItem 	.= format_margem_print('COD',15,'centro').'|';
	$cabecalhoItem 	.= format_margem_print('PRODUTO',30,'esquerda').'|';
	$cabecalhoItem 	.= format_margem_print('QTDE',5,'direita').'|';
	$cabecalhoItem 	.= format_margem_print('VALOR',8,'direita').'|';
	$cabecalhoItem 	.= format_margem_print('DESC',7,'direita').'|';
	$cabecalhoItem 	.= format_margem_print('TOTAL',8,'direita')."\r\n";
	$cabecalhoItem 	.= "</b>";
	$cabecalhoItem 	.="--------------------------------------------------------------------------------\r\n";

	$cabecalhoLinhaItem += 3;
	############################################################################################################################################################
	$limite = 30; #LIMITE DE LINHAS POR FOLHA
	$linhasRestantes = $limite - ($cabecalhoLinha + $cabecalhoLinhaItem);
	$texto .= $cabecalhoItem;
	
	############################################################################################################################################################
	#ITEM
	while($rowItem 	= mysql_fetch_array($rsItem)){
		$quantidade 	= $rowItem['fldQuantidade'];
		$valor 			= $rowItem['fldValor'];
		$total 			= $valor * $quantidade;
		$desconto 		= $rowItem['fldDesconto'];
		$descontoItem 	= ($total * $desconto) / 100;
		$totalItem 		= $total - $descontoItem;

		# GRAVA TXT ########################################################################################################################################
		//AQUI BUSCAR DADOS DA NFE
		$rowItemNFe  	= mysql_fetch_array(mysql_query("SELECT * FROM tblpedido_item_fiscal WHERE fldItem_Id= " .$rowItem['fldId']));
		$rowNCM  	 	= mysql_fetch_array(mysql_query("SELECT fldncm FROM tblproduto_fiscal WHERE fldProduto_Id= " .$rowItem['fldProduto_Id']));
		$complemento 	= (isset($rowItemNFe['fldinformacoes_adicionais'])) ? ' - '.$rowItemNFe['fldinformacoes_adicionais']: '' ;
		$descricao	 	= $rowItem['fldDescricao'].$complemento;
		$limiteDescricao= 30;
		
		//SE VAI EXIBIR DESCRICAO COMPLETA COM QUEBRA DE LINHA, OU SE VAI LIMITAR CARACTERES
		$exibir_descricao = mysql_num_rows(mysql_query("SELECT * FROM tblsistema_impressao_campo WHERE fldCampo = 'descricao_completa' AND fldImpressao = 'venda' AND fldExibir = 1"));
		if(!$exibir_descricao){
			$descricao = substr($descricao,0,$limiteDescricao);
		}
		
		$x = 0;
		$texto	.= format_margem_print(str_pad($rowItem['fldCodigo'], 15, "0", STR_PAD_LEFT),15)."|";
		$texto 	.= format_margem_print(substr(acentoRemover($descricao),$x * $limiteDescricao, $limiteDescricao),$limiteDescricao).'|';
		$texto	.= format_margem_print(format_number_out($quantidade),5, 'direita')."|";
		$texto	.= format_margem_print(format_number_out($valor),8, 'direita')."|";
		$texto	.= format_margem_print(format_number_out($desconto),7, 'direita')."|";
		$texto	.= format_margem_print(format_number_out($totalItem),8, 'direita') . "\r\n";
		$linha 	+= 1;
		$item 	+= 1;
		
		if($item == $totalItens && strlen($descricao) <= $limiteDescricao){
			while($linha <= $linhasRestantes){
				$texto .= "               |                              |     |        |       |\r\n";
				$linha  += 1;
			}
		}elseif(strlen(substr($descricao,$x * $limiteDescricao, $limiteDescricao)) > 0 && $linhasRestantes == $linha || $linhasRestantes == $linha){
			$texto .= "\r\n\r\n\r\n";	
			$texto .= $cabecalho;
			$texto .= $cabecalhoItem;
			$linha  = 2;
		}
		
		# AQUI EM BAIXO, SE DESCRICAO DO PRODUTO FOR MT GRANDE, TERMINA DE ESCREVER DEPOIS DOS VALORES, ASSIM CAI ALINHADO ABAIXO
		if(strlen($descricao) > $limiteDescricao){
			$x = 1;
			while(strlen(substr($descricao,$x * $limiteDescricao, $limiteDescricao)) > 0){
				$texto 	.= "               |".format_margem_print(substr(acentoRemover($descricao),$x * $limiteDescricao, $limiteDescricao),$limiteDescricao)."\r\n";
				$linha 	+=1;
				$x 		+=1;
				
				if($item == $totalItens && strlen(substr($descricao,$x * $limiteDescricao, $limiteDescricao)) == 0){
					while($linha < $linhasRestantes){
						$texto .= "               |                              |     |        |       |\r\n";
						$linha += 1;
					}
				
				}elseif(strlen(substr($descricao,$x * $limiteDescricao, $limiteDescricao)) > 0 && $linhasRestantes <= $linha || $linhasRestantes <= $linha){
					$texto .= "\r\n\r\n\r\n";	
					$texto .= $cabecalho;
					$texto .= $cabecalhoItem;
					$linha  = 2;
				}
			}
		}				
	}
	
	$linhasRestantes 	= $linhasRestantes - $linha;
	while($linhasRestantes  > 0){
		$texto .= "\r\n";
		$linhasRestantes  -=1;
	}
	
