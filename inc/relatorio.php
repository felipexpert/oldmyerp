<?php

	require("fpdf/fpdf.php");
	
	class PDF extends FPDF
	{
	
		function Footer()
		{
			$this->SetY(-15);
			$this->SetFont('Arial','I',8);
			$this->Cell(0, 10, $this->PageNo().'/{nb}', 0, 0, 'C');
		}
		
		// Load data
		function LoadData($file)
		{
			// Read file lines
			$lines = file($file);
			$data = array();
			foreach($lines as $line) $data[] = explode(';',trim($line));
			return $data;
		}
		
		function Table($header, $data, $w, $align, $index = false)
		{
			// Header
			$this->SetFillColor(204, 204, 204);
			$this->SetTextColor(0, 0, 0);
			for($i=0;$i<count($header);$i++) $this->Cell($w[$i], 7, $header[$i], 1, 0, 'C', true);
			$this->Ln();
			
			// Data
			foreach($data as $row)
			{
				if($index == false){
					$row = array_values($row);
					for($i = 0; $i < count($row); $i++){ $this->Cell($w[$i], 6, $row[$i], 'LRB', 0, $align[$i]); }
					$this->Ln();
				} else {
					for($i = 0; $i < count($index); $i++){ $this->Cell($w[$i], 6, $row[$index[$i]], 'LRB', 0, $align[$i]); }
					$this->Ln();
				}
			}
		}
	}

?>