<?php
	//Classe para conectar ao banco de dados
	class Conexao extends PDO {
		
		private $dsn	= "mysql:host=localhost;dbname=myerp"; //;charset=utf-8
		private $login	= "myerp";
		private $senha	= "senha753951";
		
		function __construct(){
			try{
				//Construtor do PDO
				$conect = parent::__construct($this->dsn, $this->login, $this->senha);
				//retornar erro em consultas sql
				$this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				
				return $conect;
			}
			catch(PDOException $er){
				//se houver problema exibe mensagem
				echo 'Erro ao conectar! :( <br>'. $er->getMessage();
				return false;
			}
		}
	}
?>