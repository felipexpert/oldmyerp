<?
	function formata_data_extenso($strDate)	{
		// Array com os dia da semana em português;
		$arrDaysOfWeek = array('Domingo','Segunda-feira','Terca-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sabado');
		// Array com os meses do ano em português;
		$arrMonthsOfYear = array(1 => 'Janeiro','Fevereiro','Marco','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro');
		// Descobre o dia da semana
		$intDayOfWeek = date('w',strtotime($strDate));
		// Descobre o dia do mês
		$intDayOfMonth = date('d',strtotime($strDate));
		// Descobre o mês
		$intMonthOfYear = date('n',strtotime($strDate));
		// Descobre o ano
		$intYear = date('Y',strtotime($strDate));
		// Formato a ser retornado
		return $intDayOfMonth . ' de ' . $arrMonthsOfYear[$intMonthOfYear] . ' de ' . $intYear;
	}
	
	function format_time_short($time){
		if(!$time)
			$time = "00:00";
		else
			$time = explode(':', $time);
			$time = $time[0].':'.$time[1];
			
		return $time;
	}
	function format_time_in($time){
		if(!$time)
			$time = "00:00:00";
		else
			$time = $time.':00';
			
		return $time;
	}
	
	function format_date_in($data){
		if(!checkdate(substr($data,3,2), substr($data,0,2), substr($data,6,4))){
			$data = "";
		}
		else{
			$data = substr($data,6,4) . "-" . substr($data,3,2) . "-" . substr($data,0,2);
		}
		return $data;
	}
	
	function format_date_out($data){
		if(!checkdate(substr($data,5,2), substr($data,8,2), substr($data,0,4)))
			$data = "";
		else
			$data = substr($data,8,2) . "/" . substr($data,5,2) . "/" . substr($data,0,4);
		return $data;
	}
	
	function format_date_out2($data){
		if(!checkdate(substr($data,5,2), substr($data,8,2), substr($data,0,4)))
			$data = "";
		else
			$data = substr($data,8,2) . "-" . substr($data,5,2) . "-" . substr($data,0,4);
		return $data;
	}
	
	function format_date_out3($data){
		if(!checkdate(substr($data,5,2), substr($data,8,2), substr($data,0,4)))
			$data = "";
		else
			$data = substr($data,8,2) . "/" . substr($data,5,2) . "/" . substr($data,2,2);
		return $data;
	}
	
	
	function format_date_out4($data){
		if(!checkdate(substr($data,5,2), substr($data,8,2), substr($data,0,4)))
			$data = "";
		else
			$data = substr($data,8,2) . "/" . substr($data,5,2);
		return $data;
	}
	
	function format_date_enbr($data){
		$data = substr($data,3,2) . "/" . substr($data,0,2) . "/" . substr($data,6,4);
		return $data;
	}
	
	function format_number_in($num){
		$num = str_replace(",",".",str_replace(".","",$num));
		return $num;
	}
	
	function format_number_out($num, $decimal='2'){
		$num = bcmul($num, '100', $decimal);
		$num = bcdiv($num, '100', $decimal);
		$num = number_format($num, $decimal, ",", ".");
		return $num;
	}
	
	function format_number_zero_retornar_branco($num){
		$num = ($num > 0 ? $num : '');
		return $num;
	}
	
	function format_number_branco_retornar_zero($num){
		$num = ($num > 0 ? $num : '0.00');
		return $num;
	}
	
	/* funcao para deixar apenas caracteres numericos na string de um cpf */
	function format_cpf_in($cpf){
		if($cpf != ''){
			$cpf = str_replace(".", "", $cpf);
			$cpf = str_replace("-", "", $cpf);
			return $cpf;
		}
	}
	
	/* funcao para deixar apenas caracteres numericos na string de um cnpj */
	function format_cnpj_in($cnpj)
	{
		$cnpj = str_replace(".", "", $cnpj);
		$cnpj = str_replace("/", "", $cnpj);
		$cnpj = str_replace("-", "", $cnpj);
		return $cnpj;
	}
	
	function format_cpf_out($cpf){
		if($cpf != ''){
			$cpf = substr($cpf,0,3).'.'.substr($cpf ,3,3).'.'.substr($cpf ,6,3).'-'.substr($cpf ,9,2);
			return $cpf;
		}
	}
	
	function format_cnpj_out($cnpj){
		$cnpj = substr($cnpj,0,2).'.'.substr($cnpj ,2,3).'.'.substr($cnpj ,5,3).'/'.substr($cnpj ,8,4).'-'.substr($cnpj ,12,2);
		return $cnpj;
	}
	
	/*
	function ValidaData($data){
		$data = explode("-","$data"); // fatia a string $dat em pedados, usando / como referência
		$y = $data[0];
		$m = $data[1];
		$d = $data[2];
		
	
		// verifica se a data é válida!
		// 1 = true (válida)
		// 0 = false (inválida)
		$res = checkdate($y,$m,$d);
		if ($res == 1){
		   echo "data ok!";
		} else {
		   echo "data inválida!";
		}
	}
	*/

	/* função data por extenso */
		function dataSemiExtenso($mes)
		{
			switch($mes){
				case 1:
					$mes = "Janeiro ";
					break;
				case 2:
					$mes = "Fevereiro ";
					break;
				case 3:
					$mes = "Mar&ccedil;o ";
					break;
				case 4:
					$mes = "Abril ";
					break;
				case 5:
					$mes = "Maio ";
					break;
				case 6:
					$mes = "Junho ";
					break;
				case 7:
					$mes = "Julho ";
					break;
				case 8:
					$mes = "Agosto ";
					break;
				case 9:
					$mes = "Setembro ";
					break;
				case 10:
					$mes = "Outubro ";
					break;
				case 11:
					$mes = "Novembro ";
					break;
				case 12:
					$mes = "Dezembro ";
					break;
			}
			
			return date("d"). " de " .$mes. "de ". date("Y");
		}
		
		/* escrevendo valores por extenso */
		function valorExtenso($valor = 0, $tipo = 0, $caixa = "alta")
		{
			$valor = strval($valor);
			$valor = str_replace(",", ".", $valor);
			
			if($tipo == 1)
			{
				$singular = array("centavo", "real", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
				$plural = array("centavos", "reais", "mil", "milhões", "bilhões", "trilhões", "quatrilhões");
			}
			else
			{
				$pos   = strpos($valor, ".");
				$valor = substr($valor, 0, $pos);
				$singular = array("", "", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
				$plural = array("", "", "mil", "milhões", "bilhões", "trilhões", "quatrilhões");
			}
			
			$c = array("", "cem", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos");
			$d = array("", "dez", "vinte", "trinta", "quarenta", "cinquenta", "sessenta", "setenta", "oitenta", "noventa");
			$d10 = array("dez", "onze", "doze", "treze", "quatorze", "quinze", "dezesseis", "dezesete", "dezoito", "dezenove");
			$u = array("", "um", "dois", "três", "quatro", "cinco", "seis", "sete", "oito", "nove");
			$z=0;
			$valor = number_format($valor, 2, ".", ".");
			$inteiro = explode(".", $valor);
			
			for($i = 0; $i < count($inteiro); $i++)
			{
				for($ii = strlen($inteiro[$i]); $ii < 3; $ii++)
				{
					$inteiro[$i] = "0".$inteiro[$i];
				}
			}
			$fim = count($inteiro) - ($inteiro[count($inteiro)-1] > 0 ? 1 : 2);
			
			for ($i = 0; $i < count($inteiro); $i++)
			{
				$valor = $inteiro[$i];
				$rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
				$rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
				$ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";
				$r = $rc.(($rc && ($rd || $ru)) ? " e " : "").$rd.(($rd && $ru) ? " e " : "").$ru;
				$t = count($inteiro)-1-$i;
				$r .= $r ? " ".($valor > 1 ? $plural[$t] : $singular[$t]) : "";
				if ($valor == "000")$z++; elseif ($z > 0) $z--;
				if (($t==1) && ($z>0) && ($inteiro[0] > 0)) $r .= (($z>1) ? " de " : "").$plural[$t];
				if ($r) $rt = $rt . ((($i > 0) && ($i <= $fim) && ($inteiro[0] > 0) && ($z < 1)) ? ( ($i < $fim) ? " e " : " e ") : " ") . $r;
			}
			
			if($caixa == "alta")
			{
				$rt = strtoupper($rt);
			}
			
			$maiusculas = array("Á","À","Â","Ã","É","Ê","Í","Ó","Ô","Õ","Ú","Û");
			$minusculas = array("á","à","â","ã","é","ê","í","ó","ô","õ","ú","û");
			
			for($i = 0; $i < count($maiusculas); $i++)
			{
				
				$rt = str_replace($minusculas[$i], $maiusculas[$i], $rt);
			}
			
			return trim($rt);
		}
		
		/**
		 * checarUsuarioNoBD()
		 * Função para checar se um usuário já está cadastrado no banco de dados.
		 *
		 * Tipo de retorno -> boolean [se existir usuário retorna 'true'];
		 * Parâmetros -> $nomeUsuario, $campoTabelaBD ,$tabelaBD;
		 * 	-> $nomeUsuario, nome do usuário que deseja checar;
		 * 	-> $campoTabelaBD, nome do campo na tabela que se encontra o registro;
		 * 	-> $tabelaBD, nome da tabela onde estão os registros;
		 * 	-> $extra, parte de uma string SQL complementar;
		 */
		
		function checarUsuarioNoBD($nomeUsuario, $campoTabelaBD ,$tabelaBD, $extra = null){
			$sql = "SELECT " . mysql_escape_string($campoTabelaBD) . " FROM " . mysql_escape_string($tabelaBD) . " WHERE " . mysql_escape_string($campoTabelaBD) . " = '" . mysql_escape_string($nomeUsuario) . "' " . $extra;
			$resultado = mysql_num_rows(mysql_query($sql));
			if($resultado > 0) {
				return true;
			}
			else {
				return false;
			}
		}
		
		Class dataExtenso 
		{
			var $data;
			var $expl;
			var $retornos;
			var $mes;
			var $ano;
			
			function extenDia($dia){
				switch ($dia)
				{
					case 1: $this->retornos = "Primeiro"; break;
					case 2: $this->retornos = "Segundo"; break;
					case 3: $this->retornos = "Terceiro"; break;
					case 4: $this->retornos = "Quarto"; break;
					case 5: $this->retornos = "Quinto"; break;
					case 6: $this->retornos = "Sexto"; break;
					case 7: $this->retornos = "S&eacute;timo"; break;
					case 8: $this->retornos = "Oitavo"; break;
					case 9: $this->retornos = "Nono"; break;
					case 10: $this->retornos = "D&eacute;cimo"; break;
					case 20: $this->retornos = "Vig&eacute;simo"; break;
					case 30: $this->retornos = "Trig&eacute;simo"; break;
				}
				return $this->retornos;
			}
			
			function dia($dia)
			{
				if(($dia - 30) >= 0)
				{
					if($dia == 30)
					{
						$this->retornos = $this->extenDia($dia);
					}
					else
					{
						$this->retornos = $this->extenDia(30)." ".$this->extenDia($dia - 30);
					}
				} 
				elseif(($dia - 20) >= 0)
				{
					if($dia == 20)
					{
						$this->retornos = $this->extenDia($dia);
					}
					else
					{
						$this->retornos = $this->extenDia(20)." ".$this->extenDia($dia - 20);
					}
				}
				
				elseif(($dia - 10) >= 0)
				{
					if($dia == 10)
					{
						$this->retornos = $this->extenDia($dia);
					}
					else
					{
						$this->retornos = $this->extenDia(10)." ".$this->extenDia($dia - 10);
					}
				}
				
				elseif(($dia - 10) < 0)
				{
					$this->retornos = $this->extenDia(abs($dia));
				} 
				
			return $this->retornos;
			}
			
			function mes($mes)
			{
				$this->mes = array('01'=>'Janeiro', '02'=>'Fevereiro', '03'=>'Mar&ccedil;o', '04'=>'Abril', '05'=>'Maio', '06'=>'Junho', '07'=>'Julho', '08'=>'Agosto', '09'=>'Setembro', '10'=>'Outubro', '11'=>'Novembro', '12'=>'Dezembro');
				$this->retornos = $this->mes[$mes];
			return $this->retornos;
			}
			
			function ano($ano)
			{
				//isso é descaradamente uma gambiarra por causa da falta de tempo, quando puder irei melhora-lo
				switch($ano)
				{
					case 2008: $this->retornos = "Dois mil e oito"; break;
					case 2009: $this->retornos = "Dois mil e nove"; break;
					case 2010: $this->retornos = "Dois mil e dez"; break;
					case 2011: $this->retornos = "Dois mil e onze"; break;
					case 2012: $this->retornos = "Dois mil e doze"; break;
					case 2013: $this->retornos = "Dois mil e treze"; break;
					case 2014: $this->retornos = "Dois mil e quatorze"; break;
					case 2015: $this->retornos = "Dois mil e quinze"; break;
					case 2016: $this->retornos = "Dois mil e dezesseis"; break;
					case 2017: $this->retornos = "Dois mil e dezesete"; break;
					case 2018: $this->retornos = "Dois mil e dezoito"; break;
					case 2019: $this->retornos = "Dois mil e dezenove"; break;
					case 2020: $this->retornos = "Dois mil e vinte"; break;
					case 2021: $this->retornos = "Dois mil e vinte um"; break;
					case 2022: $this->retornos = "Dois mil e vinte dois"; break;
					case 2023: $this->retornos = "Dois mil e vinte tr&ecirc;s"; break;
					case 2024: $this->retornos = "Dois mil e vinte e quatro"; break;
					case 2025: $this->retornos = "Dois mil e vinte e cinco"; break;
					case 2026: $this->retornos = "Dois mil e vinte e seis"; break;
					case 2027: $this->retornos = "Dois mil e vinte e sete"; break;
					case 2028: $this->retornos = "Dois mil e vinte e oito"; break;
					case 2029: $this->retornos = "Dois mil e vinte e nove"; break;
					case 2030: $this->retornos = "Dois mil e trinta"; break;
					case 2031: $this->retornos = "Dois mil e trinta e um"; break;
					case 2032: $this->retornos = "Dois mil e trinta e dois"; break;
					case 2033: $this->retornos = "Dois mil e trinta e tr&ecirc;s"; break;
					case 2034: $this->retornos = "Dois mil e trinta e quatro"; break;
					case 2035: $this->retornos = "Dois mil e trinta e cinco"; break;
					case 2036: $this->retornos = "Dois mil e trinta e seis"; break;
					case 2037: $this->retornos = "Dois mil e trinta e sete"; break;
					case 2038: $this->retornos = "Dois mil e trinta e oito"; break;
					case 2039: $this->retornos = "Dois mil e trinta e nove"; break;
					case 2040: $this->retornos = "Dois mil e quarenta"; break;
				}
				return $this->retornos;
			}
			
			function dtExtenso($data)
			{
				$this->expl = explode("/", $data);
				$this->retornos = $this->dia($this->expl[0]);
				$this->retornos = $this->retornos." dia do m&ecirc;s de ".$this->mes($this->expl[1])." do ano de ".$this->ano($this->expl[2]);
				echo $this->retornos;
			}
			
			//somando datas
			function sumData($data, $dias, $meses, $ano)
			{
				$data = explode("/", $data);
				$newData = date("d/m/Y", mktime(0, 0, 0, $data[1] + $meses,$data[0] + $dias, $data[2] + $ano));
				return $newData;
			}
		
		}
	
	function acentoRemover($str, $enc = 'UTF-8'){
 
		$acentos = array(
				'A' => '/&Agrave;|&Aacute;|&Acirc;|&Atilde;|&Auml;|&Aring;/',
				'a' => '/&agrave;|&aacute;|&acirc;|&atilde;|&auml;|&aring;/',
				'C' => '/&Ccedil;/',
				'c' => '/&ccedil;/',
				'E' => '/&Egrave;|&Eacute;|&Ecirc;|&Euml;|&amp;/',
				'e' => '/&egrave;|&eacute;|&ecirc;|&euml;/',
				'I' => '/&Igrave;|&Iacute;|&Icirc;|&Iuml;/',
				'i' => '/&igrave;|&iacute;|&icirc;|&iuml;/',
				'N' => '/&Ntilde;/',
				'n' => '/&ntilde;/',
				'O' => '/&Ograve;|&Oacute;|&Ocirc;|&Otilde;|&Ouml;/',
				'o' => '/&ograve;|&oacute;|&ocirc;|&otilde;|&ouml;/',
				'U' => '/&Ugrave;|&Uacute;|&Ucirc;|&Uuml;/',
				'u' => '/&ugrave;|&uacute;|&ucirc;|&uuml;/',
				'Y' => '/&Yacute;/',
				'y' => '/&yacute;|&yuml;/',
				'a.'=> '/&ordf;/',
				'o.' => '/&ordm;/'
		);

        return preg_replace($acentos, array_keys($acentos), htmlentities($str,ENT_NOQUOTES, $enc));
	}
	
	function fnc_acento_remover2($str){
		return strtr($str, "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ", "aaaaeeiooouucAAAAEEIOOOUUC=");
	}
	
	function intervalo_data($Data1, $Data2){
		$intervalo = round((strtotime($Data2) - strtotime($Data1)) / 86400);
		return $intervalo;
	}
		
	function fnc_intervalo_hora($horaInicial, $horaFinal, $dataInicial='', $dataFinal=''){
		
		
		$dataInicial 	= ($dataInicial)? $dataInicial 	: date("Y-m-d");
		$dataFinal 		= ($dataFinal)	? $dataFinal 	: date("Y-m-d");

		list($ano, $mes, $dia) 	= explode("-", $dataInicial);
		list($hora, $min, $seg) = explode(":", $horaInicial);
		$tempoInicial 			= mktime($hora + 0, $min + 0, $seg + 0, $mes + 0, $dia + 0, $ano);
	
		list($ano, $mes, $dia) 	= explode("-", $dataFinal);
		list($hora, $min, $seg) = explode(":", $horaFinal);
		$tempoFinal 			= mktime($hora + 0, $min + 0, $seg + 0, $mes + 0, $dia + 0, $ano);
	
		$seconds 	= $tempoFinal - $tempoInicial;
	
		$hours 		= floor($seconds / 3600);
		$seconds 	-= $hours * 3600;
		$minutes 	= floor($seconds / 60);
		$seconds 	-= $minutes * 60;
		
		$intervalo 	= str_pad($hours,2,"0", STR_PAD_LEFT).':'.str_pad($minutes,2,"0", STR_PAD_LEFT).':'.str_pad($seconds,2,"0", STR_PAD_LEFT);  
		
		return $intervalo;
	}
	
	function fnc_soma_hora($soma_horas){
		
		$times	 = explode(',', $soma_horas);
		
		$seconds = 0;
		
		foreach ( $times as $time ){
			list( $g, $i, $s ) = explode( ':', $time );
			$seconds 	+= $g * 3600;
			$seconds 	+= $i * 60;
			$seconds 	+= $s;
		}
	
		$hours 		= floor( $seconds / 3600 );
		$seconds 	-= $hours * 3600;
		$minutes 	= floor( $seconds / 60 );
		$seconds 	-= $minutes * 60;
		
		$soma 	= str_pad($hours,2,"0", STR_PAD_LEFT).':'.str_pad($minutes,2,"0", STR_PAD_LEFT).':'.str_pad($seconds,2,"0", STR_PAD_LEFT);  
		
		return $soma;
	}
	
	function fnc_sistema($parametro){
		$rowValor = mysql_fetch_array(mysql_query("select fldValor from tblsistema where fldParametro = '$parametro'"));
		$valor = $rowValor["fldValor"];
		return $valor;
	}
	
	function fnc_sistema_update($parametro, $valor){
		if(!mysql_query("UPDATE tblsistema SET fldValor = '$valor' WHERE fldParametro = '$parametro'")){
			return mysql_error();
		}
		
	}
	
	function fnc_campo_auxiliar($tabela, $campo){
		$rowValor = mysql_fetch_array(mysql_query("select fldAtivo from $tabela where fldCampo = '$campo'"));
		$valor = $rowValor["fldAtivo"];
		return $valor;
	}

	function porcentagemInversa($valorMaior, $valorMenor){
		if($valorMenor > 0 && $valorMaior > 0){
			$intermedio = $valorMaior - $valorMenor;
			$intermedio = $intermedio * 100;
			$porcentagem = $intermedio / $valorMenor;
			return $porcentagem;
		}
	}

	function fnc_number_only($numero_com_caracteres,$zeros_preencher=0){
		$numero = preg_replace('([^0-9])','',$numero_com_caracteres);
		if($zeros_preencher){
			$numero = str_pad($numero,$zeros_preencher,'0',STR_PAD_LEFT);
		}
		return $numero;
	}

	function fnc_format_dois_zeros_retornar_zero($num){
		$num = ($num > 0 ? $num : '0');
		return $num;
	}

	function fncNullId($Id){
		if($Id == ''){
			$Id = '0';
		}
		return $Id;
	}
	
	
	function formatCPFCNPJTipo_out($cpf_cpnj, $tipo){
		if($cpf_cpnj != ''){
			if($tipo == 1){
				$CPF_CNPJ = format_cpf_out($cpf_cpnj);
			}elseif($tipo == 2){
				$CPF_CNPJ = format_cnpj_out($cpf_cpnj);
			}
		}else{
			$CPF_CNPJ = '';
		}
		return $CPF_CNPJ;
	}
	
	function formatCPFCNPJTipo_in($cpf_cpnj, $tipo){
		if($cpf_cpnj != ''){
			if($tipo == 1){
				$CPF_CNPJ = format_cpf_in($cpf_cpnj);
			}elseif($tipo == 2){
				$CPF_CNPJ = format_cnpj_in($cpf_cpnj);
			}
		}else{
			$CPF_CNPJ = '';
		}
		return $CPF_CNPJ;
	}
	
	function url_exists($url) {
		$hdrs = @get_headers($url);
		return is_array($hdrs) ? preg_match('/^HTTP\\/\\d+\\.\\d+\\s+2\\d\\d\\s+.*$/',$hdrs[0]) : false;
	}
	
	/**
	 *	Função simples para exibir o link correto de vendas ou NFes. O objetivo principal dessa função é centralizar essa rotina, pois muitas telas são afetadas
	 *	@param Int $id com a identificação do tipo de Venda | 1- Venda, 2- Comanda, 3- NFe
	 *	@param String $default com a página padrão, em caso de $id ser falso
	 *	@return String com o nome da página que será redirecionada
	 *//*
	function redirecionarPagina($id = null, $default = 'pedido_detalhe') {
		if(is_null($id)) return $default;
		
		if($id == 3) $paginaLink = 'pedido_nfe_detalhe';
		elseif($id == 2) $paginaLink = 'pedido&modo=rapido_detalhe';
		else $paginaLink = $default;
		
		return $paginaLink;
	}
	*/
	
	function redirecionarPagina($tipo_id, $documento_tipo) {
		if($documento_tipo == 'venda'){
			if($tipo_id == 3) $paginaLink = 'pedido_nfe_detalhe&doc_tipo=1';
			elseif($tipo_id == 2) $paginaLink = 'pedido&modo=rapido_detalhe';
			else $paginaLink = 'pedido_detalhe';
		}elseif($documento_tipo == 'compra'){
			if($tipo_id == 1) $paginaLink = 'compra_detalhe';
			elseif($tipo_id == 3) $paginaLink = 'pedido_nfe_detalhe&doc_tipo=2';
		}
		return $paginaLink;
	}
	
	
	/**
	 *	Função para fixar as casas decimais'
	 *	@param {Float} $valor que terá as casas decimais fixada
	 *	@param {Int} $decimais número de casas decimais
	 *	@param {String} $moeda tipo de moeda: Brasileiro ou Americano
	 */
	function toFixed($valor = null, $decimais = 2, $moeda = 'en-us') {
		
		if(is_null($valor) or is_null($decimais)) return false;
		
		if($moeda == 'pt-br') $separadorDecimal = ',';
		else $separadorDecimal = '.';
		
		$valorMatriz = explode($separadorDecimal, $valor);
		
		return $valorMatriz[0] . $separadorDecimal . substr($valorMatriz[1], 0, $decimais);
	}
	
	/**
	 *  Função simples para limitar a quantidade de caracteres a serem exibidos de um determinado bloco de texto
	 *  @param string $text texto de entrada
	 *  @param int $limit número de caracteres a serem exibidos
	 *  @return string texto limitado
	 */
	function limitarTexto($texto, $limite) {
		$texto = strip_tags($texto);
		
		if(strlen($texto) < $limite) return $texto;
		else return substr($texto, 0, strrpos(substr($texto, 0, $limite), ' ')) . ' ...';
	}
	
	/**
	 *	Função para checar se a é uma requisição via ajax
	 *	@param não recebe parâmetros
	 *	@return boolean
	 */
	function isAjax() {
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') return true;
		else return false;
	}
	
	
	
	######################################################################################################################
	
	function fnc_parcela_credito($cliente_id, $documento_tipo){
		if($documento_tipo == 'venda'){
			$documento 		= 'Pedido';
			$tbl_documento 	= 'tblpedido';
			$fld_tipo	 	= 'fldCliente_Id';
		}else{
			$documento 		= 'Compra';
			$tbl_documento 	= 'tblcompra';
			$fld_tipo	 	= 'fldFornecedor_Id';
		}
		
		$rsCredito  = mysql_query("SELECT SUM(fldValor) as fldValor FROM {$tbl_documento}_parcela
									INNER JOIN {$tbl_documento} ON {$tbl_documento}.fldId = {$tbl_documento}_parcela.fld{$documento}_Id
									WHERE {$tbl_documento}.{$fld_tipo} = '$cliente_id' AND {$tbl_documento}_parcela.fldCredito = '1'");
									
		$rowCredito = mysql_fetch_array($rsCredito);
		echo mysql_error();
		return format_number_branco_retornar_zero($rowCredito['fldValor']);
	}
	
	function fnc_parcela_credito_utilizado($cliente_id, $documento_tipo){
		if($documento_tipo == 'venda'){
			$sSQL = "SELECT SUM((tblpedido_parcela_baixa.fldValor + tblpedido_parcela_baixa.fldJuros + tblpedido_parcela_baixa.fldMulta) - tblpedido_parcela_baixa.fldDesconto) as fldValor  
											FROM tblpedido_parcela_baixa
											INNER JOIN tblfinanceiro_conta_fluxo ON tblfinanceiro_conta_fluxo.fldReferencia_Id = tblpedido_parcela_baixa.fldId
											INNER JOIN tblpedido_parcela ON tblpedido_parcela_baixa.fldParcela_Id = tblpedido_parcela.fldId
											INNER JOIN tblpedido ON tblpedido.fldId = tblpedido_parcela.fldPedido_Id
											WHERE tblpedido.fldCliente_Id = '$cliente_id' AND tblpedido_parcela_baixa.fldExcluido = 0
											AND tblfinanceiro_conta_fluxo.fldPagamento_Tipo_Id = '15' AND tblfinanceiro_conta_fluxo.fldMovimento_Tipo = '3'";
		}else{
			$sSQL = "SELECT SUM((tblcompra_parcela_baixa.fldValor + tblcompra_parcela_baixa.fldJuros + tblcompra_parcela_baixa.fldMulta + tblcompra_parcela_baixa.fldAcrescimo) - tblcompra_parcela_baixa.fldDesconto) as fldValor  
											FROM tblcompra_parcela_baixa
											INNER JOIN tblfinanceiro_conta_fluxo ON tblfinanceiro_conta_fluxo.fldReferencia_Id = tblcompra_parcela_baixa.fldId
											INNER JOIN tblcompra_parcela ON tblcompra_parcela_baixa.fldParcela_Id = tblcompra_parcela.fldId
											INNER JOIN tblcompra ON tblcompra.fldId = tblcompra_parcela.fldCompra_Id
											WHERE tblcompra.fldFornecedor_Id = '$cliente_id' AND tblcompra_parcela_baixa.fldExcluido = 0 
											AND tblfinanceiro_conta_fluxo.fldPagamento_Tipo_Id = '15' AND tblfinanceiro_conta_fluxo.fldMovimento_Tipo = '4'";
		}
		$rsCreditoUtilizado = mysql_query($sSQL);
		$rowCreditoUtilizado = mysql_fetch_array($rsCreditoUtilizado);
						
		return format_number_branco_retornar_zero($rowCreditoUtilizado['fldValor']);
	}
	
		
?>