<?php
	/*
	function fnc_nfe_numero_novo(){
		$sql = 'select max(fldnumero) as fldNumeroMax from tblpedido_fiscal';
		$rsPedido = mysql_query($sql);
		$rowPedido = mysql_fetch_assoc($rsPedido);
		
		return $rowPedido['fldNumeroMax'] + 1;
	}
	*/
	function fnc_nfe_regime(){
		$sql = 'select fldRegime_Tributario_Codigo FROM tblnfe_perfil';
		$rsNFe = mysql_query($sql);
		$rowNFe = mysql_fetch_assoc($rsNFe);
		
		return $rowNFe['fldRegime_Tributario_Codigo'];
	}
	
	function calcula_dv($chave43) {
		$multiplicadores = array(2,3,4,5,6,7,8,9);
		$i = 42;
		while ($i >= 0) {
			for ($m=0; $m<count($multiplicadores) && $i>=0; $m++) {
				$soma_ponderada+= $chave43[$i] * $multiplicadores[$m];
				$i--;
			}
		}
		$resto = $soma_ponderada % 11;
		if ($resto == '0' || $resto == '1') {
			return 0;
		} else {
			return (11 - $resto);
	   }
	}
	
    function monta_chave_nfe($cUF, $AAMM, $CNPJ, $mod, $serie, $nNF, $tpEmis, $cNF) {
        /*
			cUF – Código da UF do emitente do Documento Fiscal
			AAMM – Ano e Mês de emissão da NF-e
			CNPJ – CNPJ do emitente
			mod – Modelo do Documento Fiscal
			serie – Série do Documento Fiscal
			nNF – Número do Documento Fiscal
			tpEmis – forma de emissão da NF-e
			cNF – Código Numérico que compõe a Chave de Acesso
			cDV – Dígito Verificador da Chave de Acesso

         */
		
        // 02 - cUF  - código da UF do emitente do Documento Fiscal
        $chave = sprintf("%02d", $cUF);

        // 04 - AAMM - Ano e Mes de emissão da NF-e
        $chave.= sprintf("%04s", $AAMM);

        // 14 - CNPJ - CNPJ do emitente
        $chave.= sprintf("%014s", $CNPJ);

        // 02 - mod  - Modelo do Documento Fiscal
        $chave.= sprintf("%02d", $mod);

        // 03 - serie - Série do Documento Fiscal
        $chave.= sprintf("%03d", $serie);

        // 09 - nCT  - Número do Documento Fiscal
        $chave.= sprintf("%09d", $nNF);

        // 01 - tpEmis  - Tipo emissão 
        $chave.= sprintf("%01d", $tpEmis);

        // 08 - cCT  - Código Numérico que compõe a Chave de Acesso // diminui 1 digito na versão 2.0
        $chave.= sprintf("%08d", $cNF);

        // 01 - cDV  - Dígito Verificador da Chave de Acesso
        $chave.= calcula_dv($chave);

        return $chave;
    }
	
	################################################################################################################################################
	# ARQUIVO DE EXPORTACAO DA NFE - MODAL #
	
	function test_print($item, $key){
		
		if($key=='id'){
			$saida = "\r\n";
		}
		 if(!intval($key)){ 
			$saida .= $item . '|'; 
			//echo $saida; 
		}
		//gravar no arquivo
		fwrite($GLOBALS['arquivo'],$saida);
	}
	
	function fnc_array_destruir($item, $key){
		$GLOBALS['excluir'][] = $item;
		
		$excluir = array();
		foreach($array as $item ){
			$excluir[] = $item;
		}
		print_r($excluir);
		foreach($excluir as $item){
			unset($item);
		}
	}
	

	################################################################################################################################################
	# INSERIR REGISTRO NFe ####################################################################################################################
		function fnc_nfe_documento_novo($documento_tipo, $destinatario_id, $desconto, $desconto_reais, $observacao, $data_emissao, $marcador, $valor_terceiros, $faturado,
									$veiculo_id='', $veiculo_km='', $dependente_id='', $conta_bancaria_id='', $cliente_pedido=''){
			
			$data				= date("Y-m-d");
			$hora				= $_POST['txt_pedido_hora'];
			$usuario_id 		= $_SESSION['usuario_id'];
			if($documento_tipo == '1'){
				$sqlInsert = "INSERT INTO tblpedido(
					fldUsuario_Id,				fldCliente_Id,
					fldVeiculo_Id,				fldVeiculo_Km,
					fldDependente_Id,			fldContaBancaria_Id,
					fldDesconto,				fldDescontoReais,
					fldObservacao,				fldCadastroData,
					fldCadastroHora,			fldPedidoData,		
					fldCliente_Pedido,			fldStatus,
					fldMarcador_Id,				fldTipo_Id,
					fldValor_Terceiros,			fldFaturado
				)
				VALUES(
					'$usuario_id',				'$destinatario_id', 
					'$veiculo_id',				'$veiculo_km',
					'$dependente_id',			'$conta_bancaria_id', 
					'$desconto',				'$desconto_reais',
					'$observacao',				'$data',
					'$hora',					'$data_emissao',
					'$cliente_pedido',			'4',
					'$marcador',				'3',
					'$valor_terceiros',			'$faturado'
				)";
			}elseif($documento_tipo == '2'){
				
				$sqlInsert = "INSERT INTO tblcompra (
					fldFornecedor_Id, 			fldCadastroData,
					fldCadastroHora, 			fldUsuario_Id, 
					fldDesconto, 				fldDescontoReais, 
					fldCompraData, 				fldObservacao, 
					fldStatus, 					fldFaturado, 
					fldMarcador, 				fldTipo_Id)
					
				VALUES(
					'$destinatario_id',			'$data',
					'$hora', 					'$usuario_id',
					'$desconto', 				'$desconto_reais',
					'$data_emissao',			'$observacao',
					'4',						'$faturado',
					'$marcador',					'3')";
			}
	
			if(mysql_query($sqlInsert)) {
			
				$rsLastId 	= mysql_query("SELECT last_insert_id() as lastId");
				$LastId 	= mysql_fetch_array($rsLastId);
				$last_id 	= $LastId['lastId'];
			}
			
			return $last_id;
	}	
	
	# INSERIR ITEM NFe ####################################################################################################################
	function fnc_nfe_inserir_item($documento_tipo, $documento_id, $produto_id, $item_quantidade, $item_nome, $item_valor, $item_desconto, $item_estoque_id,
									$valor_compra='', $item_fardo='', $item_tabela_preco='',  $tipo_id='', $item_entregue='', $item_entregue_data=''){
			
			if($documento_tipo == '1' or $documento_tipo == 'pedido'){
					$sqlInsertItem = "INSERT INTO tblpedido_item (
						fldProduto_Id, 		fldPedido_Id, 
						fldQuantidade, 		fldValor, 
						fldValor_Compra, 	fldDesconto, 
						fldDescricao, 		fldFardo_Id, 
						fldTabela_Preco_Id, fldEstoque_Id, 
						fldProduto_Tipo_Id, fldEntregue, 
						fldEntregueData)
					VALUES(
						'$produto_id',		'$documento_id',
						'$item_quantidade',	'$item_valor',
						'$valor_compra',	'$item_desconto',
						'$item_nome',		'$item_fardo',
						$item_tabela_preco,	'$item_estoque_id',
						'$tipo_id',			'$item_entregue',
						$item_entregue_data)";
						
			}elseif($documento_tipo == '2' or $documento_tipo == 'compra'){
				
					$sqlInsertItem = "INSERT INTO tblcompra_item (
						fldProduto_Id, 		fldCompra_Id, 
						fldQuantidade, 		fldDesconto, 
						fldValor, 			fldDescricao, 
						fldEstoque_Id)
					VALUES(
						'$produto_id',		'$documento_id',
						'$item_quantidade',	'$item_desconto',
						'$item_valor',		'$item_nome',
						'$item_estoque_id'	)";
						
			}
			
			
			if(mysql_query($sqlInsertItem)) {
				$LastItemId = mysql_fetch_array(mysql_query("SELECT last_insert_id() as lastID"));
				$item_id 	= $LastItemId['lastID'];
			}else{
				echo mysql_error();
			}
			
			return $item_id;
	}

	# UPDATE REGISTRO NFe ####################################################################################################################
	function fnc_nfe_update_registro($documento_tipo, $documento_id, $destinatario_id, $desconto, $desconto_reais, $observacao, $data_emissao, $marcador, $valor_terceiros, $faturado,
									$veiculo_id='', $veiculo_km='', $dependente_id='', $conta_bancaria_id='', $cliente_pedido=''){
		
		if($documento_tipo == 'pedido'){
			$sqlUpdate				= "UPDATE tblpedido SET
				fldCliente_Id 		= '$destinatario_id',	fldDependente_Id	= '$dependente_id',
				fldVeiculo_Id 		= '$veiculo_id',		
				fldVeiculo_Km 		= '$veiculo_km',		fldContaBancaria_Id = '$conta_bancaria_id',	
				fldDesconto 		= '$desconto',			fldDescontoReais 	= '$deconto_reais',	
				fldObservacao 		= '$observacao',						
				fldMarcador_Id		= '$marcador',			fldPedidoData 		= '$data_emissao',		
				fldCliente_Pedido 	= '$cliente_pedido',		
				fldServico			= '$outros_servicos',	fldValor_Terceiros	= '$servicos_terceiros',
				fldFaturado			= '$faturado'
				WHERE fldId 		= $documento_id";
		}else{
			
			
			$sqlUpdate = "UPDATE tblcompra SET
				fldFornecedor_Id 	= '$destinatario_id',
				fldDesconto 		= '$desconto',
				fldDescontoReais 	= '$desconto_reais',
				fldObservacao 		= '$observacao',
				fldCompraData 		= '$data_emissao',
				fldMarcador			= '$marcador'
				WHERE fldId 		= $documento_id";
				
		}
		echo mysql_error();
		return(mysql_query($sqlUpdate));
	}
?>