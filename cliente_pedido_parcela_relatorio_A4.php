<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
        <title>myERP - Relat&oacute;rio de Parcelas</title>
        <link rel="stylesheet" type="text/css" href="style/style_relatorio.css" />
        <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="style/style_filtro.css"></link>
 
	</head>
	<body>
        <div id="no-print">
            <a style="margin-top: 6px" class="print" href="#" onClick="window.print()">imprimir</a>
        </div>
        
<?	
		$clienteId = $_GET['cliente_id'];
		ob_start();
		session_start();
		
		require("inc/con_db.php");
		require("inc/fnc_general.php");
		
		$rowDados 	= mysql_fetch_array(mysql_query("SELECT * FROM tblempresa_info"));
		$rowUsuario = mysql_fetch_array(mysql_query("SELECT * FROM tblusuario WHERE fldId=".$_SESSION['usuario_id']));
		$rowCliente = mysql_fetch_array(mysql_query("SELECT * FROM tblcliente WHERE fldId =".$clienteId));
		echo mysql_error();
		
		//formata se tiver cpf_cnpj cadastrado
		$CPF_CNPJ 	= formatCPFCNPJTipo_out($rowCliente['fldCPF_CNPJ'], $rowCliente['fldTipo']);
		$sqlParcela = $_SESSION['parcela_relatorio'] ;
		$rsParcela 	= mysql_query($sqlParcela);
		echo mysql_error();
		$limiteRegistros = mysql_num_rows($rsParcela);
		
		$tabelaCabecalho ="  
			<tr style='border-bottom: 2px solid'>
				<td style='width: 800px'><h1>Relat&oacute;rio de Parcelas</h1></td>
			</tr>
			<tr>
				<td>
					<table style='width: 550px' name='table_relatorio_dados' class='table_relatorio_dados' summary='Relat&oacute;rio'>
						<tr>
							<td style='width: 320px;'>Raz&atilde;o Social: ".$rowDados['fldNome']."</td>
							<td style='width: 200px;'>Nome Fantasia: ".$rowDados['fldNome_Fantasia']."</td>
							<td style='width: 320px;'>CPF/CNPJ: $CPF_CNPJDados</td>
							<td style='width: 200px;'>Telefone: ".$rowDados['fldTelefone1']."</td>
							<td style='width: 320px;'>Cliente: ".$rowCliente['fldNome']."</td>
							<td style='width: 200px;'>CPF: $CPF_CNPJ</td>
						</tr>
					</table>	
				</td>
				<td>        
					<table class='dados_impressao'>
						<tr>
							<td><b>Data: </b><span>".format_date_out(date('Y-m-d'))."</span></td>
							<td><b>Hora: </b><span>".format_time_short(date('H:i:s'))."</span></td>
							<td><b>Usu&aacute;rio: </b><span>".$rowUsuario['fldUsuario']."</span></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table name='table_relatorio' class='table_relatorio' summary='Relat&oacute;rio'>
						<tr style='border:none; margin:0'>
							<td style='width:51px; text-align:center'>Venda</td>
							<td style='width:49px; text-align:center'>NFE</td>
							<td style='width:38px; text-align:center'>Parc.</td>
							<td style='width:100px;text-align:center'>Vencimento</td>
							<td style='width:100px; text-align:right'>Valor</td>
							<td style='width:400px'>Observa&ccedil;&atilde;o</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table name='table_relatorio' class='table_relatorio' summary='Relat&oacute;rio'>";//cabecalho
		
		$tabelaCabecalho1 	= '<table class="relatorio_print" style="page-break-before:avoid;'.$style.'">'.$tabelaCabecalho;	
		$tabelaCabecalho	= '<table class="relatorio_print" style="'.$style.'">'.$tabelaCabecalho;
		$tabelaFecha 		= "</table></td></tr></table>";
		
		if($_SESSION["sistema_tipo"] == 'otica'){
			$limite = 18;
			$style = 'margin-top:210px;height:500px;';
		}else{
			$limite = 44;
		}
		
		$n = 1;
		while($rowParcela 	= mysql_fetch_array($rsParcela)){	
			$sql			= mysql_query("SELECT COUNT(*) as totalParcelas FROM tblpedido_parcela WHERE fldPedido_Id = ".$rowParcela['fldPedido_Id']);
			$rowsParcela	= mysql_fetch_array($sql);
			$linha ++;
			
			$ultimaData = $rowParcela['fldVencimento'];
			
			#VOU ARMAZENAR TUDO EM VARIAVEIS, COM LIMITE DE REGISTROS POR FOLHA, E ABAIXO MANDO EXIBIR NA TELA FOLHA A FOLHA
			#VERIFICA SE CHEGOU AO LIMITE DE LINHAS PRA TROCAR DE FOLHA
			($linha < $limite)? '': $n ++ ;
			$folha[$n] .= '
				<tr style="background:#DFDFDF">
					<td style="width:50px;	font-weight:bold; text-align:center">&nbsp;'.str_pad($rowParcela['fldPedido_Id'],6,'0', STR_PAD_LEFT).'</td>
					<td style="width:50px;	font-weight:bold; text-align:center">&nbsp;'.str_pad($rowParcela['fldNFe'],4,'0', STR_PAD_LEFT).'</td>
					<td style="width:30px;	font-weight:bold; text-align:center">&nbsp;'.str_pad($rowParcela['fldParcela'], 2, "0", STR_PAD_LEFT).'/'.str_pad($rowsParcela['totalParcelas'], 2, "0", STR_PAD_LEFT).'</td>
					<td style="width:100px;	font-weight:bold; text-align:right">&nbsp;'.format_date_out($rowParcela['fldVencimento']).'</td>
					<td style="width:100px;	font-weight:bold; text-align:right">&nbsp;'.format_number_out($rowParcela['fldValor']).'</td>
					<td style="width:400px">&nbsp;'.$rowParcela['fldObservacao'].'</td>
				</tr>';
						
			$sqlBaixa = "SELECT * FROM tblpedido_parcela_baixa WHERE fldParcela_Id = ".$rowParcela['fldId']." AND fldExcluido = 0 ORDER BY fldId";
			
			$rsBaixa 			= mysql_query($sqlBaixa);
			$totalBaixas 		= mysql_num_rows($rsBaixa);
			echo mysql_error();
			$limiteRegistros	+= $totalBaixas;
			
			$valorDevedor 		= $rowParcela['fldValor'];
			$devedorAnterior 	= 0;
			$parcelaAnterior 	= 0;
			$baixaAnterior 		= 0;
			$valorOld 			= 0;
			
			$saldoParcela += $rowParcela['fldValor'];
			
			#EXIBE AS BAIXAS E VALOR DO JUROS SE HOUVER
			if($totalBaixas > 0 ){
				#VERIFICA SE CHEGOU AO LIMITE DE LINHAS PRA TROCAR DE FOLHA
				($linha < $limite)? '': $n ++ ;
				$folha[$n] .= '	
					<tr style="background:#F3F3F3">
						<td style="width:20px">&nbsp;</td>
						<td style="width:80px; text-align:right">Recebido em</td>
						<td style="width:80px; text-align:right">Juros</td>
						<td style="width:80px; text-align:right">Multa</td>
						<td style="width:80px; text-align:right">Receber</td>
						<td style="width:80px; text-align:right">Desconto</td>
						<td style="width:80px; text-align:right">Pago</td>
						<td style="width:80px; text-align:right">Devedor</td>
						<td style="width:130px">Pagamento</td>
					</tr>';
				$linha ++;
				
				while($rowBaixa = mysql_fetch_array($rsBaixa)){
					
					$ultimaData = $rowBaixa['fldDataRecebido'];
					//confere se ainda esta descrevendo a mesma parcela que anterior, pra pegar o devedor anterior
					if($rowParcela['fldPedido_Id'].$rowParcela['fldParcela'] != $baixaAnterior){
						$devedorAnterior 	= 0;
						$baixaAnterior 		= 0;
					}
					
					$sql		= mysql_query("SELECT SUM(fldValor) as fldValor, SUM(fldDesconto) as fldDesconto, SUM(fldMulta) as fldMulta, SUM(fldJuros) as fldJuros, fldId FROM tblpedido_parcela_baixa 
										WHERE fldParcela_Id = ".$rowBaixa['fldParcela_Id']." AND fldExcluido = 0 AND fldId < ".$rowBaixa['fldId']." ORDER BY fldId");
					$old 		= mysql_fetch_array($sql);
					
					$valorPago 	= $rowBaixa['fldValor'] + $rowBaixa['fldJuros' ] + $rowBaixa['fldMulta'];
					
					#SOMA VALOR PAGO ANTERIORMENTE ATE ESSA BAIXA | TOTAL PAGO ANTERIOR + ATUAL
					$valorOld 	= $old['fldValor'] + $old['fldJuros'] + $old['fldMulta'];
					$totalPago 	= $valorOld + $valorPago;
					
					$totalMulta = $rowBaixa['fldMulta'];
					
					if($devedorAnterior > 0){
						$totalJuros 	= $rowBaixa['fldDevedor'] - (($devedorAnterior + $old['fldDesconto']) - ($rowBaixa['fldValor'] + $rowBaixa['fldMulta'] + $rowBaixa['fldDesconto']));
						$valorReceber 	= $devedorAnterior + $totalJuros + $rowBaixa['fldMulta'];
					}else{
						$totalJuros 	= ($rowBaixa['fldDevedor'] + $rowBaixa['fldValor'] + $rowBaixa['fldDesconto']) - $rowParcela['fldValor'];
						$valorReceber 	= number_format((($totalMulta + $totalJuros + $rowParcela['fldValor']) - $valorOld),2,'.','');
					}
					
					if($rowBaixa['fldJuros'] > 0){
						$totalJuros = $rowBaixa['fldJuros'];
					}
					
					$valorDevedor 	= (($valorReceber + $totalJuros) - $valorPago) - $rowBaixa['fldDesconto'];
					$valorDesconto 	= $rowBaixa['fldDesconto'];
					
					//tipo de pagamento
					$rowPagamentoTipo = mysql_fetch_array(mysql_query("SELECT tblfinanceiro_conta_fluxo.fldPagamento_Tipo_Id, tblpagamento_tipo.* 
								FROM tblfinanceiro_conta_fluxo 
								LEFT JOIN tblpagamento_tipo ON tblfinanceiro_conta_fluxo.fldPagamento_Tipo_Id = tblpagamento_tipo.fldId
								WHERE tblfinanceiro_conta_fluxo.fldReferencia_Id = ".$rowBaixa['fldId']));
					#VERIFICA SE CHEGOU AO LIMITE DE LINHAS PRA TROCAR DE FOLHA
					($linha < $limite)? '': $n ++ ;
					$folha[$n] .= '	
						<tr>
							<td style="width:20px; text-align:center">&nbsp;</td>
							<td style="width:80px;color:#4A4A4A; text-align:right">&nbsp;'.format_date_out($rowBaixa['fldDataRecebido']).'</td>
							<td style="width:80px;color:#4A4A4A; text-align:right">&nbsp;'.format_number_out($totalJuros).'</td>
							<td style="width:80px;color:#4A4A4A; text-align:right">&nbsp;'.format_number_out($totalMulta).'</td>
							<td style="width:80px;color:#4A4A4A; text-align:right">&nbsp;'.format_number_out($valorReceber).'</td>
							<td style="width:80px;color:#4A4A4A; text-align:right">&nbsp;'.format_number_out($valorDesconto).'</td>
							<td style="width:80px;color:#4A4A4A; text-align:right">&nbsp;'.format_number_out($valorPago).'</td>
							<td style="width:80px;color:#4A4A4A; text-align:right">&nbsp;'.format_number_out($valorDevedor).'</td>
							<td style="width:130px">'.substr($rowPagamentoTipo['fldTipo'],0, 20).'</td>
						</tr>';
		
					$devedorAnterior 	= $rowBaixa['fldDevedor'];
					$baixaAnterior 		= $rowBaixa['fldValor'];
					$baixaAnterior 		= $rowParcela['fldPedido_Id'].$rowParcela['fldParcela'];
					
					$saldoReceber 		+= $valorReceber;
					$saldoEncargos		+= $totalJuros + $totalMulta;
					$saldoPago 			+= $valorPago;
				}// end while
			}
			
			#DEIXO UMA LINHA DE REGISTRO PARA EXIBIR O DEVEDOR, CASO AJA JUROS, JA FAZ O CALCULO PARA VALOR NO DIA ################################
			if(fnc_sistema('pedido_juros') > 0 && $valorDevedor > 0){
				if(!$totalBaixas){
					#PRIMEIRA LINHA CABECALHO, SE NAO HOUVER BAIXAS TEM QUE MANDAR EXIBIR 
					($linha < $limite)? '': $n ++ ;
					$folha[$n] .= '	
						<tr style="background:#F3F3F3">
							<td style="width:20px">&nbsp;</td>
							<td style="width:80px; text-align:right">Recebido em</td>
							<td style="width:80px; text-align:right">Juros</td>
							<td style="width:80px; text-align:right">Multa</td>
							<td style="width:80px; text-align:right">Receber</td>
							<td style="width:80px; text-align:right">Desconto</td>
							<td style="width:80px; text-align:right">Pago</td>
							<td style="width:80px; text-align:right">Devedor</td>
							<td style="width:130px">Pagamento</td>
						</tr>';
					$linha ++;
				}
				#######################################################################################################################################
				$juros 			= fnc_sistema('pedido_juros');
				$multa	 		= fnc_sistema('pedido_multa');
				$jurosTipo 		= fnc_sistema('pedido_juros_tipo');
				
				$dias 			= intervalo_data($ultimaData, date("Y-m-d"));
				$juros_ad 		= number_format((($juros / 30) * $dias), 2, '.', ''); 				// pega o juros ao mes, e divide pelo numero de dias que esta atrasado
				$valorJuros 	= number_format((($juros_ad * $valorDevedor) / 100), 2, '.', ''); 	// valor do juros calculado com o ultimo valor da parcela, o valor total antes da ultima baixa
				
				$valorJuros 	= ($valorJuros > 0) ? $valorJuros :'0.00'; //caso nao tenha vencido aina, o juros fica negativo, dando diferença no calculo do devedor
				$valorMulta 	= ($totalMulta) ? 0 : $multa;	//se ja tiver cobrado multa na baixa, nao cobrar de novo, se nao houver baxa ainda, cobrar multa
				$valorDevedor 	= $valorJuros + $valorDevedor + $valorMulta;
			
				($linha < $limite)? '': $n ++ ;
				$folha[$n] .= '	
				<tr>
					<td style="width:20px; text-align:center">&nbsp;</td>
					<td style="width:80px;color:#4A4A4A; text-align:center"> ------------ </td>
					<td style="width:80px;color:#4A4A4A; text-align:right">&nbsp;'.format_number_out($valorJuros).'</td>
					<td style="width:80px;color:#4A4A4A; text-align:right">&nbsp;'.format_number_out($valorMulta).'</td>
					<td style="width:80px;color:#4A4A4A; text-align:right">&nbsp;'.format_number_out($valorDevedor).'</td>
					<td style="width:80px;color:#4A4A4A; text-align:right"> ------------ </td>
					<td style="width:80px;color:#4A4A4A; text-align:right"> ------------ </td>
					<td style="width:80px;color:#4A4A4A; text-align:right">&nbsp;'.format_number_out($valorDevedor).'</td>
					<td style="width:130px"> ------------ </td>
				</tr>';
				$linha 		++;
				$saldoEncargos += $valorJuros + $valorMulta;
			}
			
			#######################################################################################################################################
			$saldoDevedor += $valorDevedor;
		}
		
		$x = 1;
		while($x <= $n){
			($n == 1)? print $tabelaCabecalho1 : print $tabelaCabecalho;
			print $folha[$x];
			print $tabelaFecha;
			$x ++;
		}
?>
        <table name="table_relatorio_rodape" class="table_relatorio_rodape" summary="Relat&oacute;rio">
            <tr>
                <td style="width:40px">&nbsp;</td>
                <td style="width:80px">Parcelas</td>
                <td style="width:80px; text-align:right"><?=format_number_out($saldoParcela)?></td>
                <td style="width:6px; border-right:1px solid">&nbsp;</td>
                <td style="width:80px">Encargos</td>
                <td style="width:80px; text-align:right"><?=format_number_out($saldoEncargos)?></td>
                <td style="width:6px; border-right:1px solid">&nbsp;</td>
                <td style="width:80px">Pago</td>
                <td style="width:80px; text-align:right"><?=format_number_out($saldoPago)?></td>
                <td style="width:6px; border-right:1px solid">&nbsp;</td>
                <td style="width:80px">Devedor</td>
                <td style="width:80px; text-align:right"><?=format_number_out($saldoDevedor)?></td>
            </tr>                   
		</table>
	</body>
</html>
