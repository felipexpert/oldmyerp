<?
	//ações em grupo
	if(isset($_POST["hid_action"])){
		require("produto_vasilhame_tipo_action.php");
	}
	
	if(isset($_POST['btn_salvar'])){
		
		$data 		= date("Y-m-d");
		$nome 		= $_POST['txt_nome'];
		$descricao 	= $_POST['txt_descricao'];
		$quantidade = $_POST['txt_quantidade'];
		$adicionar = $_POST['txt_adicionar'];
		$sel_adicionar = $_POST['sel_adicionar'];
		$vasilhameId = $_POST['hid_id'];
		
		$soma = ($sel_adicionar == 'adicionar')? " + $adicionar":" - $adicionar";
		
		if($vasilhameId != ''){
			$sSQL = "UPDATE tblproduto_vasilhame_tipo SET fldNome = '$nome', fldDescricao = '$descricao', fldQuantidade = (fldQuantidade $soma) WHERE fldId = $vasilhameId";
		}else{
			$sSQL = "INSERT INTO tblproduto_vasilhame_tipo (fldNome, fldDescricao, fldQuantidade, fldData_Cadastro) values(
						'$nome',
						'$descricao',
						'$quantidade',
						'$data')";
		}
		
		if(mysql_query($sSQL)){
?>			<div class="alert">
				<p class="ok">Registro gravado com sucesso!<p>
        	</div>
<?		}else{
			echo mysql_error();
?>		
    	    <div class="alert">
				<p class="erro">N&atilde;o foi poss&iacute;vel gravar o registro!<p>
        	</div>
<?		}
	}
	
/**************************** ORDER BY *******************************************/
	$filtroOrder = 'tblproduto_vasilhame_tipo.fldNome ';
	$class 		 = 'asc';
	$order_sessao = explode(" ", $_SESSION['order_produto_vasilhame_tipo']);
	if(isset($_GET['order'])){
		switch($_GET['order']){
			case 'nome'		:  $filtroOrder = "tblproduto_vasilhame_tipo.fldNome";		break;
		}
		if($order_sessao[0] == $filtroOrder){
			$class = ($order_sessao[1] == 'asc') ? 'desc' : 'asc';
		}
	}
	
	//definir icone para ordem
	$_SESSION['order_produto_vasilhame_tipo'] = (!$_SESSION['order_produto_vasilhame_tipo'] || $_GET['order']) ? $filtroOrder.' '.$class : $_SESSION['order_produto_vasilhame_tipo'];
	$pag	= ($_GET['pagina'])? '&pagina='.$_GET['pagina'] : ''; 
	$raiz	= "index.php?p=produto&modo=vasilhame&exibir=tipo$pag&amp;order=";
	
	$order_sessao = explode(" ", $_SESSION['order_produto_vasilhame_tipo']);
	$filtroOrder  = $order_sessao[0]; //pra poder comparar na listagem e exibir a class
	
/**************************** PAGINAÇÃO *******************************************/
	//	usei o mesmo sistema da verificação de Excluido ou não, pra pegar os que estao emprestados
	$sSQL = "SELECT SUM(tblproduto_vasilhame_movimento.fldQuantidade * (tblproduto_vasilhame_movimento.fldStatus * -1 + 1)) as fldEmprestado,
					tblproduto_vasilhame_tipo.*
					FROM tblproduto_vasilhame_tipo LEFT JOIN tblproduto_vasilhame_movimento ON tblproduto_vasilhame_movimento.fldTipo_Id = tblproduto_vasilhame_tipo.fldId 
					GROUP BY tblproduto_vasilhame_tipo.fldId ORDER BY ".$_SESSION['order_produto_vasilhame_tipo'];
	
	$rsTotal = mysql_query($sSQL);
	$rowsTotal = mysql_num_rows($rsTotal);
	echo mysql_error();
	
	//definição dos limites
	$limite = 150;
	$n_paginas = 7;
	
	$total_paginas = ceil(mysql_num_rows($rsTotal) / $limite);
	if(isset($_GET["pagina"]) && $_GET["pagina"] > $total_paginas){
		$inicio = 0;
	}elseif(isset($_GET['pagina'])){
		$inicio = ($_GET['pagina'] - 1) * $limite;
	}else{
		$inicio = 0;
	}
	
	$sSQL 		.= " limit " . $inicio . "," . $limite;
	$rsVasilhame = mysql_query($sSQL);
	$pagina 	 = (isset($_GET['pagina'])) ? $_GET['pagina'] : "1";
	
#########################################################################################
?>

    <form class="table_form" id="frm_vasilhame" action="" method="post">
    	<div id="table">
            <div id="table_cabecalho">
                <ul class="table_cabecalho">
                    <li class="order" style="width:375px">
                    	<a <?= ($order_sessao[1] == 'fldNome') ? "class='$class'" : '' ?> style="width:360px" href="index.php?p=produto&modo=vasilhame&exibir=tipo&amp;order=nome">Tipo</a>
                    </li>
                    <li style="width:300px">Descricao</li>
                    <li style="width:100px; text-align:right">Emprestado</li>
                    <li style="width:100px; text-align:right">Dispon&iacute;vel</li>
                    <li style="width:27px">&nbsp;</li>
                    <li style="width:20px; text-align:left"><input type="checkbox" name="chk_todos" id="chk_todos" /></li>
                </ul>
            </div>
            <div id="table_container">       
                <table id="table_general" class="table_general" summary="Lista de vasilhames emprestados">
                	<tbody>
<?					
						$id_array = array();
						$n = 0;
						$linha = "row";
						$rows = mysql_num_rows($rsVasilhame);
						while($rowVasilhame = mysql_fetch_array($rsVasilhame)){
							$id_array[$n] = $rowVasilhame["fldId"];
							$n += 1;
							
							$disponivel = $rowVasilhame['fldQuantidade'] - $rowVasilhame['fldEmprestado'];
						
?>							<tr class="<?= $linha; ?>">
                                <td style="width:15px;">&nbsp;</td>
                                <td style="width:355px;"><?=$rowVasilhame['fldNome']?></td>
                                <td style="width:325px;"><?=$rowVasilhame['fldDescricao']?></td>
                                <td class="debito" style="width:95px; text-align:center;"><?=format_number_out($rowVasilhame['fldEmprestado'])?></td>
                                <td class="credito" style="width:93px; text-align:center"><?=format_number_out($disponivel)?></td>
                                <td style="width:auto; text-align:center"><a class="edit modal" href="produto_vasilhame_tipo,<?=$rowVasilhame['fldId']?>" rel="520-150"></a></td>
                                <td style="width:auto"><input type="checkbox" name="chk_vasilhame_tipo_<?=$rowVasilhame['fldId']?>" id="chk_vasilhame_tipo_<?=$rowVasilhame['fldId']?>" title="selecionar o registro posicionado" /></td>
                            </tr>
<?                  		$linha = ($linha == "row") ? "dif-row" : "row";
						}
?>		 			</tbody>
				</table>
            </div>
            <input type="hidden" name="hid_array" id="hid_array" value="<?=urlencode(serialize($id_array))?>" />
            <input type="hidden" name="hid_action" id="hid_action" value="true" />
            
			<div id="table_action">
                <ul id="action_button">
                    <li><a class="btn_novo modal" href="produto_vasilhame_tipo" rel="520-150">novo</a></li>
                    <li><input type="submit" name="btn_action" id="btn_excluir" value="excluir" title="Excluir registro(s) selecionado(s)" onclick="return confirm('Deseja excluir os registros selecionados?')" /></li>
                </ul>
        	</div>
            <div id="table_paginacao">
<?				$paginacao_destino = "?p=produto&modo=vasilhame&exibir=tipo";
				include("paginacao.php")
?>		
            </div>
            <div class="table_registro">
            	<span>Exibindo registros <?=($pagina*$limite-$limite+1).' a '.($pagina*$limite-$limite+$rows)?> do total de <?=$rowsTotal?></span>
            </div>    
        </div>
	</form>