<?php

	//memorizar os filtros para exibição nos selects
	if(isset($_POST['btn_limpar'])){
		$_SESSION['txt_produto_cod'] 					= "";
		$_SESSION['txt_produto'] 						= "";
		$_SESSION['sel_produto_categoria'] 				= "";
		$_SESSION['sel_produto_sub_categoria'] 			= "";
		$_SESSION['sel_produto_marca'] 					= "";
		$_SESSION['sel_produto_tipo'] 					= "";
		$_SESSION['txt_cod_barras'] 					= "";
		$_SESSION['sel_produto_compativel'] 			= "1";
		$_SESSION['txt_produto_estoque_quantidade']		= "";
		$_SESSION['filtro_produto_estoque_quantidade'] 	= "";
		$_SESSION['sel_produto_fornecedor'] 			= "";
		$_SESSION['sel_produto_status']					= "";
		$_POST['chk_produto'] 							= false;
	}
	else{
		$_SESSION['txt_produto_cod'] 				= (isset($_POST['txt_produto_cod']) 				? $_POST['txt_produto_cod'] 				: $_SESSION['txt_produto_cod']);
		$_SESSION['txt_produto'] 					= (isset($_POST['txt_produto']) 					? $_POST['txt_produto'] 					: $_SESSION['txt_produto']); 
		$_SESSION['sel_produto_categoria'] 			= (isset($_POST['sel_produto_categoria']) 			? $_POST['sel_produto_categoria'] 			: $_SESSION['sel_produto_categoria']);
		$_SESSION['sel_produto_sub_categoria'] 		= (isset($_POST['sel_produto_sub_categoria']) 		? $_POST['sel_produto_sub_categoria']	 	: $_SESSION['sel_produto_sub_categoria']);
		$_SESSION['sel_produto_marca'] 				= (isset($_POST['sel_produto_marca']) 				? $_POST['sel_produto_marca'] 				: $_SESSION['sel_produto_marca']);
		$_SESSION['sel_produto_tipo']				= (isset($_POST['sel_produto_tipo']) 				? $_POST['sel_produto_tipo'] 				: $_SESSION['sel_produto_tipo']);
		$_SESSION['txt_cod_barras'] 				= (isset($_POST['txt_cod_barras']) 					? $_POST['txt_cod_barras'] 					: $_SESSION['txt_cod_barras']);
		$_SESSION['sel_produto_compativel']	 		= (isset($_POST['sel_produto_compativel']) 			? $_POST['sel_produto_compativel']	 		: $_SESSION['sel_produto_compativel']);
		$_SESSION['txt_produto_estoque_quantidade'] = (isset($_POST['txt_produto_estoque_quantidade']) 	? $_POST['txt_produto_estoque_quantidade'] 	: $_SESSION['txt_produto_estoque_quantidade']);
		$_SESSION['sel_produto_fornecedor'] 		= (isset($_POST['sel_produto_fornecedor']) 			? $_POST['sel_produto_fornecedor'] 			: $_SESSION['sel_produto_fornecedor']);
		$_SESSION['sel_produto_status']				= (isset($_POST['sel_produto_status']))				? $_POST['sel_produto_status']				: $_SESSION['sel_produto_status'];
	}
	
?>
<form id="frm-filtro" action="index.php?p=produto" method="post">
	<fieldset>
  	<legend>Buscar por:</legend>
    <ul>
		<li style="height:40px">
<?			$codigo = ($_SESSION['txt_produto_cod']) ? $_SESSION['txt_produto_cod'] : "c&oacute;digo";
			($_SESSION['txt_produto_cod'] == "código") ? $_SESSION['txt_produto_cod'] = '' : '';
?>      	<input style="width: 80px" type="text" name="txt_produto_cod" id="txt_produto_cod" onfocus="limpar (this,'c&oacute;digo');" onblur="mostrar (this, 'c&oacute;digo');" value="<?=$codigo?>"/>
		</li>
		<li style="height:40px">
        	<input type="checkbox" name="chk_produto" id="chk_produto" <?=($_POST['chk_produto'] == true) ? print 'checked ="checked"' : ''?> />
<?			$produto = ($_SESSION['txt_produto']) ? $_SESSION['txt_produto'] : "produto";
			($_SESSION['txt_produto'] == "produto") ? $_SESSION['txt_produto'] = '' : '';
?>     		<input style="width: 165px" type="text" name="txt_produto" id="txt_produto" onfocus="limpar (this,'produto');" onblur="mostrar (this, 'produto');" value="<?=$produto?>"/>
			<small>marque para qualquer parte do campo</small>
        </li>
		<li style="height:40px">
            <select style="width:100px" id="sel_produto_tipo" name="sel_produto_tipo" >
                <option value="">Tipo</option>
<?				$rsTipo = mysql_query("select * from tblproduto_tipo ORDER BY fldTipo ASC");
				while($rowTipo= mysql_fetch_array($rsTipo)){
?>					<option <?=($_SESSION['sel_produto_tipo'] == $rowTipo['fldId']) ? 'selected="selected"' : '' ?> value="<?=$rowTipo['fldId']?>"><?=$rowTipo['fldTipo']?></option>
<?				}
?>			</select>
		</li>
		<li style="height:40px">
            <select style="width:125px" id="sel_produto_categoria" name="sel_produto_categoria" class="sel_categoria" >
                <option value="">Categoria</option>
<?				$rsCategoria = mysql_query("SELECT * FROM tblcategoria WHERE fldExcluido = '0' AND fldDisabled = '0' ORDER BY fldNome ASC");
				while($rowCategoria= mysql_fetch_array($rsCategoria)){
?>					<option <?=($_SESSION['sel_produto_categoria'] == $rowCategoria['fldId']) ? 'selected="selected"' : '' ?> value="<?=$rowCategoria['fldId']?>"><?=$rowCategoria['fldNome']?></option>
<?				}
?>			</select>
		</li>
		<li style="height:40px">
            <select style="width:120px" id="sel_produto_sub_categoria" name="sel_produto_sub_categoria" class="sel_sub_categoria" >
                <option value="">Sub categoria</option>
<?				if(isset($_SESSION['sel_produto_categoria'])){
					$rsSubCategoria = mysql_query("SELECT * FROM tblsubcategoria WHERE fldExcluido = '0' AND fldCategoria_Id =".$_SESSION['sel_produto_categoria']." ORDER BY fldNome ASC");
					while($rowSubCategoria= mysql_fetch_array($rsSubCategoria)){
?>						<option <?=($_SESSION['sel_produto_sub_categoria'] == $rowSubCategoria['fldId']) ? 'selected="selected"' : '' ?> value="<?=$rowSubCategoria['fldId'] ?>"><?= $rowSubCategoria['fldNome'] ?></option>
<?					}
				}
?>			</select>
		</li>
		<li style="height:40px">
            <select style="width:120px" id="sel_produto_marca" name="sel_produto_marca" >
                <option value="">Marca</option>
<?				$rsMarca = mysql_query("select * from tblmarca where fldDisabled = '0' ORDER BY fldNome ASC");
				while($rowMarca= mysql_fetch_array($rsMarca)){
?>					<option <?=($_SESSION['sel_produto_marca'] == $rowMarca['fldId']) ? 'selected="selected"' : '' ?> value="<?=$rowMarca['fldId']?>"><?=$rowMarca['fldNome']?></option>
<?				}
?>			</select>
		</li>
		<li style="height:40px">
<?			$codigo_barras = ($_SESSION['txt_cod_barras']) ? $_SESSION['txt_cod_barras'] : "c&oacute;digo de barras";
			($_SESSION['txt_cod_barras'] == "código de barras") ? $_SESSION['txt_cod_barras'] = '' : '';
?>
     		<input style="width: 115px" type="text" name="txt_cod_barras" id="txt_cod_barras" onfocus="limpar (this,'c&oacute;digo de barras');" onblur="mostrar (this, 'c&oacute;digo de barras');" value="<?=$codigo_barras?>"/>
        </li>
        <li style="float:left">
            <select style="width:160px" id="sel_produto_compativel" name="sel_produto_compativel" >
                <option <?=($_SESSION['sel_produto_compativel'] == '1') ? 'selected="selected"' : '' ?> value="1">exibir compat&iacute;veis</option>
                <option <?=($_SESSION['sel_produto_compativel'] == '0') ? 'selected="selected"' : '' ?> value="0">n&atilde;o exibir compat&iacute;veis</option>
			</select>
		</li> 
		<li>
            <select style="width:230px" id="sel_produto_fornecedor" name="sel_produto_fornecedor" >
                <option value="">Fornecedor</option>
<?				$rsFornecedor = mysql_query("select * from tblfornecedor where fldDisabled = '0' ORDER BY fldNomeFantasia ASC");
				while($rowFornecedor= mysql_fetch_array($rsFornecedor)){
?>					<option <?=($_SESSION['sel_produto_fornecedor'] == $rowFornecedor['fldId']) ? 'selected="selected"' : '' ?> value="<?=$rowFornecedor['fldId']?>"><?=$rowFornecedor['fldNomeFantasia']?></option>
<?				}
?>			</select>
		</li>
        <li style="float:left">
            <select style="width:160px" id="sel_produto_status" name="sel_produto_status">
            	<option value="">status</option>
                <option <?=($_SESSION['sel_produto_status'] == '0') ? 'selected="selected"' : '' ?> value="0">Somente habilitados</option>
                <option <?=($_SESSION['sel_produto_status'] == '1') ? 'selected="selected"' : '' ?> value="1">Somente desabilitados</option>
			</select>
		</li> 
		<li style="float:left; margin-right:25px">
     		<input style="width: 100px" type="text" name="txt_produto_estoque_quantidade" id="txt_produto_estoque_quantidade" placeHolder="estoque" value="<?=$_SESSION['txt_produto_estoque_quantidade']?>"/>
        </li>
        
        <li style="float:left">
        	<button type="submit" name="btn_limpar" style="margin:0" title="Limpar Filtro">Limpar filtro</button>
        </li>
        <li style="float:left">
        	<button type="submit" name="btn_exibir" style="margin:0" title="Exibir">Exibir</button>
        </li>
    </ul>
	</fieldset>
</form>

<?
	$filtro = '';
	if($_SESSION['txt_produto_cod'] != ""){
		if($_SESSION['sel_produto_compativel'] == '1'){
			$filtro .= "AND ((tblproduto.fldCodigo = '".$_SESSION['txt_produto_cod']."')
						or (tblproduto.fldId in (SELECT fldProduto_Id FROM tblproduto_compativel RIGHT JOIN tblproduto ON tblproduto_compativel.fldProduto_Compativel_Id = tblproduto.fldId WHERE tblproduto.fldCodigo = '".$_SESSION['txt_produto_cod']."'))
						or (tblproduto.fldId in (SELECT fldProduto_Compativel_Id FROM tblproduto_compativel RIGHT JOIN tblproduto ON tblproduto_compativel.fldProduto_Id = tblproduto.fldId WHERE tblproduto.fldCodigo = '".$_SESSION['txt_produto_cod']."' )))";
		}else{
			$filtro .= " AND tblproduto.fldCodigo = '".$_SESSION['txt_produto_cod']."'"; //funcao no meio para tirar os zeros à esquerda
		}
	}
	
	if($_SESSION['txt_produto'] != ""){
		$produto = addslashes($_SESSION['txt_produto']); // no caso de aspas, pra nao dar erro na consulta
		
		if($_SESSION['sel_produto_compativel'] == '1'){
			if($_POST['chk_produto'] == true){
				$filtro .= " AND ((tblproduto.fldNome like '%$produto%')";
				$filtroNome = "tblproduto.fldNome like '%$produto%'";
			}else{
				$filtro .= " AND ((tblproduto.fldNome like '$produto%')";
				$filtroNome = "tblproduto.fldNome like '$produto%'";
			}
			$filtro .= " or (tblproduto.fldId in (SELECT fldProduto_Id FROM tblproduto_compativel RIGHT JOIN tblproduto ON tblproduto_compativel.fldProduto_Compativel_Id = tblproduto.fldId WHERE $filtroNome))
						or (tblproduto.fldId in (SELECT fldProduto_Compativel_Id FROM tblproduto_compativel RIGHT JOIN tblproduto ON tblproduto_compativel.fldProduto_Id = tblproduto.fldId WHERE $filtroNome)))";
		}else{
			
			if($_POST['chk_produto'] == true){
				$filtro .= " AND tblproduto.fldNome like '%$produto%'";
			}else{
				$filtro .= " AND tblproduto.fldNome like '$produto%'";
			}
		}
	}
			
	if(($_SESSION['sel_produto_categoria']) != ""){
		
		$filtro .= " AND tblproduto.fldCategoria_Id = ".$_SESSION['sel_produto_categoria'];
	}
		
	if(($_SESSION['sel_produto_sub_categoria']) != ""){
		
		$filtro .= " AND tblproduto.fldSubCategoria_Id = ".$_SESSION['sel_produto_sub_categoria'];
	}
		
	if(($_SESSION['sel_produto_marca']) != ""){
	
		$filtro .= " AND tblproduto.fldMarca_Id = ".$_SESSION['sel_produto_marca'];
	}
	
	if(($_SESSION['sel_produto_tipo']) != ""){
	
		$filtro .= " AND tblproduto.fldTipo_Id = ".$_SESSION['sel_produto_tipo'];
	}
				
	if(($_SESSION['txt_cod_barras']) != ""){
	
		$filtro .= " AND tblproduto.fldCodigoBarras = '".$_SESSION['txt_cod_barras']."'";
	}
			
	if(($_SESSION['sel_produto_fornecedor']) != ""){
	
		$filtro .= " AND tblproduto.fldFornecedor_Id = '".$_SESSION['sel_produto_fornecedor']."'";
	}
	
	if(($_SESSION['sel_produto_status']) != ""){
		$filtro .= " AND tblproduto.fldDisabled = '".$_SESSION['sel_produto_status']."'";
	}
	
	
			
	
	if($_SESSION['txt_produto_estoque_quantidade'] > 0){
		$_SESSION['filtro_produto_estoque_quantidade'] = " HAVING (fldEstoqueQuantidade = ".$_SESSION['txt_produto_estoque_quantidade']."  ) ";
	}
	
	//transferir para a sessão
	if(isset($_POST['btn_exibir'])){
		$_SESSION['filtro_produto'] = $filtro;
	}
	elseif(isset($_POST['btn_limpar'])){
		$_SESSION['filtro_produto'] = "";
	}
	
	
?>