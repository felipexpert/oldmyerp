<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
      	<title>myERP - Relat&oacute;rio de Categorias</title>
        <link rel="stylesheet" type="text/css" href="style/style_relatorio.css" />
        <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="style/style_filtro.css" />
        
	</head>
	<body>
   
<? 
		ob_start();
		session_start();
		
		require("inc/con_db.php");
		require("inc/fnc_general.php");
		
		$rsDados = mysql_query("select * from tblempresa_info");
		$rowDados = mysql_fetch_array($rsDados);
		
		$CPF_CNPJDados = formatCPFCNPJTipo_out($rowDados['fldCPF_CNPJ'], $rowDados['fldTipo']);
		
		$rsUsuario = mysql_query("select * from tblusuario where fldId=".$_SESSION['usuario_id']);
		$rowUsuario = mysql_fetch_array($rsUsuario);
		
		$rsCategoria = mysql_query($_SESSION['categoria_relatorio']);
		$rowsCategoria = mysql_num_rows($rsCategoria);
		echo mysql_error();
									
		$limite = 42;
		$n = 1;
		
		$pgTotal = $rowsCategoria / $limite;
		$p = 1;
		
		$tabelaCabecalho = 
		'<tr style="border-bottom: 2px solid">
                <td style="width: 600px"><h1>Relat&oacute;rio de Categorias</h1></td>
                <td style="width: 200px"><p class="pag">p&aacute;g. '.$p.' de '.ceil($pgTotal).'</p></td>
            </tr>
            <tr>
                <td>
                    <table style="width: 580px" name="table_relatorio_dados" class="table_relatorio_dados" summary="Relat&oacute;rio">
                        <tr>
                            <td style="width: 320px;">Raz&atilde;o Social: '.$rowDados['fldNome'].'</td>
                            <td style="width: 200px;">Nome Fantasia: '.$rowDados['fldNome_Fantasia'].'</td>
							<td style="width: 320px;">CPF/CNPJ: '.$CPF_CNPJDados.'</td>
                            <td style="width: 200px;">Telefone: '.$rowDados['fldTelefone1'].'</td>
                        </tr>
                    </table>	
                </td>
                <td>        
                    <table class="dados_impressao">
                        <tr>
                            <td><b>Data: </b><span>'.format_date_out(date("Y-m-d")).'</span></td>
                            <td><b>Hora: </b><span>'.format_time_short(date("H:i:s")).'</span></td>
                            <td><b>Usu&aacute;rio: </b><span>'.$rowUsuario['fldUsuario'].'</span></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table name="table_relatorio" class="table_relatorio" summary="Relat&oacute;rio">
                        <tr style="border:none">
                      		<td class="valor" style="margin-right:10px">C&oacute;d</td>
                            <td style="width:496px">Categoria</td>
                            <td style="width:100px; text-align:right">Subcategorias</td>
                            <td style="width:100px; text-align:right">Produtos</td>
	                	</tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table name="table_relatorio" class="table_relatorio" summary="Relat&oacute;rio">
		';
?>
        <div id="no-print">
            <a class="print" href="#" onClick="window.print()">imprimir</a>
        </div>
            
        
        <table class="relatorio_print" style="page-break-before:avoid">
<?		print $tabelaCabecalho;
		$tabelaCabecalho = '<table class="relatorio_print">'.$tabelaCabecalho;
		while($rowCategoria = mysql_fetch_array($rsCategoria)){
			echo mysql_error();
			$x+= 1;
			
			$rsProduto = mysql_query("SELECT * FROM tblproduto WHERE fldCategoria_Id =". $rowCategoria['fldId']);
			$rowsProduto = mysql_num_rows($rsProduto);
			
			$rsSub = mysql_query("SELECT * FROM tblsubcategoria WHERE fldCategoria_Id =". $rowCategoria['fldId']);
			$rowsSub = mysql_num_rows($rsSub);
?>                    
            <tr>
                <td class="valor"><?=str_pad($rowCategoria['fldId'], 6, "0", STR_PAD_LEFT)?></td>
                <td style="width:496px"><?=$rowCategoria['fldNome']?></td>
                <td style="width:100px; text-align:right"><?=$rowsSub?></td>
                <td style="width:100px; text-align:right"><?=$rowsProduto?></td>
            </tr>
<?
			while($rowSub = mysql_fetch_array($rsSub)){
?>                    
                <tr style=" border: 0; background: #EFEFEF">
                    <td class="valor" style="background: #FFFFFF">&nbsp;</td>
                    <td class="valor"><?=str_pad(($rowSub['fldCategoria_Id']), 2, "0", STR_PAD_LEFT) . str_pad(($rowSub['fldCodigo']), 3, "0", STR_PAD_LEFT)?></td>
                    <td style="width:612px"><?=$rowSub['fldNome']?></td>
                    <td style="width:20px; color:#CCC">sub</td>
                </tr>
<?				$n += 1;
			}
			if(($n == $limite) or ($x == $rowsCategoria)){
?>							</table>    
                        </td>
                    </tr>
                </table>
<?  			$n = 1;
					if($x < $rowsCategoria){
						$p += 1;
						print $tabelaCabecalho;								
					}
				}else{
					$n += 1;
				}
			}
?>         
	</body>
</html>	
