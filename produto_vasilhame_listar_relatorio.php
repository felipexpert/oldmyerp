<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
      	<title>myERP - Relat&oacute;rio de Vasilhames</title>
        <link rel="stylesheet" type="text/css" href="style/style_relatorio.css" />
        <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="style/style_filtro.css" />
        
	</head>
	<body>
<? 
		ob_start();
		session_start();
		
		require("inc/con_db.php");
		require("inc/fnc_general.php");
		
		$rowDados = mysql_fetch_array(mysql_query("select * from tblempresa_info"));
		
		$CPF_CNPJDados = formatCPFCNPJTipo_out($rowDados['fldCPF_CNPJ'], $rowDados['fldTipo']);
		
		$rowUsuario = mysql_fetch_array(mysql_query("SELECT * FROM tblusuario WHERE fldId=".$_SESSION['usuario_id']));
		
		$rsVasilhame = mysql_query($_SESSION['vasilhame_listar_sql']);
		$rowsVasilhame = mysql_num_rows($rsVasilhame);
		echo mysql_error();
									
		$limite = 42;
		$n = 1;
		
		$pgTotal = $rowsVasilhame / $limite;
		$p = 1;
	
		$tabelaCabecalho =' 
			<tr style="border-bottom: 2px solid">
                <td style="width: 600px"><h1>Relat&oacute;rio de Estoque</h1></td>
                <td style="width: 200px"><p class="pag">p&aacute;g. '.$p.' de '.ceil($pgTotal).'</p></td>
            </tr>
            <tr>
                <td>
                    <table style="width: 580px" class="table_relatorio_dados" summary="Relat&oacute;rio">
                        <tr>
                            <td style="width: 320px;">Raz&atilde;o Social: '.$rowDados['fldNome'].'</td>
                            <td style="width: 200px;">Nome Fantasia: '.$rowDados['fldNome_Fantasia'].'</td>
                            <td style="width: 320px;">CPF/CNPJ: '.$CPF_CNPJDados.'</td>
                            <td style="width: 200px;">Telefone: '.$rowDados['fldTelefone1'].'</td>
                        </tr>
                    </table>	
                </td>
                <td>        
                    <table class="dados_impressao">
                        <tr>
                            <td><b>Data: </b><span>'.format_date_out(date("Y-m-d")).'</span></td>
                            <td><b>Hora: </b><span>'.format_time_short(date("H:i:s")).'</span></td>
                            <td><b>Usu&aacute;rio: </b><span>'.$rowUsuario['fldUsuario'].'</span></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="table_relatorio" summary="Relat&oacute;rio">
                        <tr style="border:none">
							<td style="width:2px">&nbsp;</td>
							<td style="width:210px">Cliente</td>
							<td style="width:70px; text-align:center">Sa&iacute;da</td>
							<td style="width:70px; text-align:center">Retorno</td>
							<td style="width:40px">Qtde</td>
							<td style="width:90px; text-align:left">Tipo</td>
							<td style="width:60px; text-align:center">Status</td>
							<td style="width:176px">Observa&ccedil;&atilde;o</td>
					  </tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table class="table_relatorio" summary="Relat&oacute;rio">
		';
?>
   		<table class="relatorio_print" style="page-break-before:avoid">
<?			print $tabelaCabecalho;
			$tabelaCabecalho = '<table class="relatorio_print">'.$tabelaCabecalho;
			while($rowVasilhame = mysql_fetch_array($rsVasilhame)){
				echo mysql_error();
				$x+= 1;
				$status 	 = ($rowVasilhame["fldStatus"] == 0)? 'Pendente' : 'Devolvido';
				$statusClass = ($rowVasilhame["fldStatus"] > 0)? 'credito' : 'debito';
				$retorno 	 = ($rowVasilhame["fldData_Retorno"] > 0) ? format_date_out($rowVasilhame['fldData_Retorno']) : '';
				
				$totalEmprestado += $rowVasilhame['fldQuantidade'];
?>                    
				<tr>
                	<td style="width:2px"></td>
                    <td style="width:210px;"><?=$rowVasilhame['fldClienteNome']?></td>
                    <td style="width:70px;"><?=format_date_out($rowVasilhame['fldData_Saida'])?></td>
                    <td style="width:70px;"><?=$retorno?></td>
                    <td style="width:40px; text-align:right;font-weight:bold"><?=format_number_out($rowVasilhame['fldQuantidade'])?></td>
                    <td style="width:90px; text-align:left"><?=$rowVasilhame['fldTipoNome']?></td>
                    <td style="width:68px; text-align:center"><?=$status?></td>
                    <td style="width:170px;"><?=$rowVasilhame['fldObservacao']?></td>
				</tr>
<?				if(($n == $limite) or ($x == $rowsVasilhame)){
?>							</table>    
                            </td>
                        </tr>
                    </table>
<?					$n = 1;
					if($x < $rowsVasilhame){
						$p += 1;
						print $tabelaCabecalho;
					}
				}else{
					$n += 1;
				}
			}
?>
            <table class="table_relatorio_rodape" summary="Relat&oacute;rio">
                <tr>
                    <td style="width:500px">&nbsp;</td>
                    <td style="width:200px">Quantidade Emprestada</td>
                    <td style="width:60px; text-align:right"><?=format_number_out($totalEmprestado)?></td>
                </tr>
            </table> 
	</body>
</html>	
