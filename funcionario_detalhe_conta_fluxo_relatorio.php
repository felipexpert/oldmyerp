<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
      	<title>myERP - Relatório de Pagamentos</title>
        <link rel="stylesheet" type="text/css" href="style/style_relatorio.css" />
        <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="style/style_filtro.css" />
        
	</head>
	<body>
   		<div id="no-print">
            <ul style="width:800px; display:inline">
                <li><a class="print" href="#" onClick="window.print()">imprimir</a></li>
            </ul>
        </div>
<?
		ob_start();
		session_start();
		
		require("inc/con_db.php");
		require("inc/fnc_general.php");
		
		$rowDados		= mysql_fetch_array(mysql_query("SELECT * FROM tblempresa_info"));
		$CPF_CNPJDados 	= formatCPFCNPJTipo_out($rowDados['fldCPF_CNPJ'], $rowDados['fldTipo']);
		$rowUsuario		= mysql_fetch_array(mysql_query("SELECT * FROM tblusuario WHERE fldId=".$_SESSION['usuario_id']));
		
		
		$rowFuncionario	= mysql_fetch_assoc(mysql_query('SELECT fldNome FROM tblfuncionario WHERE fldId ='. $_GET['id']));
		#ABAIXO CRIO O CABECALHO #########################################################################################################################################################
		$tabelaCabecalho =' 
			<tr style="border-bottom: 2px solid">
                <td style="width: 600px"><h1>Relat&oacute;rio de Pagamentos</h1></td>';
		$tabelaCabecalho2 =' 
            </tr>
            <tr>
                <td>
                    <table style="width: 560px;margin-bottom:0" class="table_relatorio_dados" summary="Relat&oacute;rio">
                        <tr>
                            <td style="width: 320px;">Raz&atilde;o Social: '.$rowDados['fldNome'].'</td>
                            <td style="width: 200px;">Nome Fantasia: '.$rowDados['fldNome_Fantasia'].'</td>
                            <td style="width: 320px;">CPF/CNPJ: '.$CPF_CNPJDados.'</td>
                            <td style="width: 200px;">Telefone: '.$rowDados['fldTelefone1'].'</td>
                        </tr>
                    </table>	
                </td>
                <td>        
                    <table class="dados_impressao" style="margin-bottom:0">
                        <tr>
                            <td><b>Data: 			</b><span>'.format_date_out(date("Y-m-d")).'</span></td>
                            <td><b>Hora: 			</b><span>'.format_time_short(date("H:i:s")).'</span></td>
                            <td><b>Usu&aacute;rio: 	</b><span>'.$rowUsuario['fldUsuario'].'</span></td>
                        </tr>
                    </table>
                </td>
            </tr>
			<tr class="total">
				<td style="width:580px; margin-left:20px">'.$rowFuncionario['fldNome'].'</td>
				<td>Per&iacute;odo '.$_SESSION['funcionario_conta_fluxo_filtro_data1'].' a '.$_SESSION['funcionario_conta_fluxo_filtro_data2'].'</td>
			</tr>
			<tr style="border-top: 1px solid">
				<td style="width:80px;text-align:center;font-weight:bold">Data</td>
				<td style="width:80px;text-align:center;font-weight:bold">Hora</td>
				<td style="width:280px;font-weight:bold">Descri&ccedil;&atilde;o</td>
				<td style="width:60px;text-align:right;font-weight:bold">Refer&ecirc;ncia</td>
				<td style="width:120px;text-align:right;font-weight:bold">Valor</td>
				<td style="width:160px;margin-left:20px;font-weight:bold">Usu&aacute;rio</td>
			</tr>';
			$tabelaCabecalho3 ='
			<tr>
				<td>
					<table class="table_relatorio" summary="Relat&oacute;rio">';
		##########################################################################################################################################################################################
			$rsRelatorio	= mysql_query($_SESSION['funcionario_conta_fluxo_relatorio']);
			$n	 			= 1; #DEFINE O NUMERO DA DO BLOCO
			$countRegistro  = 1; #DEFINE CONTAGEM DE ITENS NO WHILE PARA QUEBRA DE BLOCO AO ATINGIR LIMITE 
			$x				= 1; #DEFINE CONTAGEM DE ITENS TOTAIS, PRA SABER SE JA TERMINOU WHILE MAS AINDA FALTA ESPACO
			$limite 		= 43;
			$total_array	= mysql_num_rows($rsRelatorio);

			$pgTotal 		= ceil($total_array / $limite);
			$p = 1;
			
			while($rowRelatorio = mysql_fetch_assoc($rsRelatorio)){
				$rsEstorno = mysql_query('SELECT fldEstorno FROM tblfuncionario_conta_fluxo WHERE fldEstorno = '.$rowRelatorio['fldId']);
				if(mysql_num_rows($rsEstorno)){
					$class 			= 'credito';
					$descricao 		= $rowRelatorio['fldTipo'].' [ESTORNO]';
					$total_saldo	-= $rowRelatorio['fldValor'];
				}else{
					$class 			= 'debito';
					$descricao 		= $rowRelatorio['fldTipo'];
					$total_saldo 	+= $rowRelatorio['fldValor'];
				}	
				
				$pagina[$n] .='
					<tr>
						<td style="width:80px;text-align:center;margin:0">'.format_date_out3($rowRelatorio['fldData']).'</td>
						<td style="width:80px;text-align:center;margin:0">'.format_time_short($rowRelatorio['fldHora']).'</td>
						<td style="width:280px;margin:0">'.$descricao.'</td>
						<td style="width:60px;text-align:right;margin:0">'.str_pad($rowRelatorio['fldReferencia_Id'], 5,'0', STR_PAD_LEFT).'</td>
						<td style="width:120px;text-align:right;margin:0">'.format_number_out($rowRelatorio['fldValor']).'</td>
						<td style="width:150px;margin-left:20px">'.$rowRelatorio['fldUsuario'].'</td>
					</tr>';
				
				#SE CHEGAR LIMITE, MUDA DE 'BLOCO' E RECMECA CONTAGEM
				if($countRegistro == $limite){
					$countRegistro = 1;
					$n ++;
				}elseif($total_array == $x && $countRegistro < $limite){ #SE JA TERMINOU O WHILE DE REGISTROS MAS AINDA NAO ATINGIU TOTAL DE LINHAS, CONTINUAR CRIANDO LINHAS ATE O LIMITE
					while($countRegistro <= $limite){ $pagina[$n] .='<tr style="border:0; width:800px"></tr>'; $countRegistro++;}
				}else{
					$countRegistro ++;
				}
				$x ++;
			}

		#AGORA MANDO GERAR NA TELA PARA IMPRESSAO ############################################################################################################################################
		$x = 1;
		while($x <= $n){
			$tabelaCabecalho1 = ($x == 1)? '<table class="relatorio_print" style="page-break-before:avoid">'.$tabelaCabecalho : '<table class="relatorio_print">'.$tabelaCabecalho;
			#PRIMEIRO BLOCO (LANCADOS) ###################################################################################################################################################
				print $tabelaCabecalho1;
				
                print '<td style="width: 200px"><p class="pag">'.$p.' de '.$pgTotal.'</p></td>';
				print $tabelaCabecalho2;
				print $tabelaCabecalho3;
				echo  $pagina[$x];
?>
						</table>
					</td >
				</tr>
			</table>			
<?			$x ++;
			$p ++;
		}
?>        
		<table name="table_relatorio_rodape" class="table_relatorio_rodape" summary="Relat&oacute;rio">
            <tr style="margin:1px 10px 0 0; float:right">
                <td style="width:80px; font-weight:bold;margin-left:300px">Total pago:</td>
                <td style="width:60px; text-align:right"><?=format_number_out($total_saldo)?></td>
            </tr>
        </table>

	</body>
</html>