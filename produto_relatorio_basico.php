
<? 
		#o que vai ser exibido, e define o tamanho dos campos
		if($exibir_unidade == 1 && $exibir_valor == 1 && $exibir_fornecedor == 1){
			$width_fornecedor = "195";
			$width_produto = "320";
			$max_caracter 	= "50";
		}else if($exibir_fornecedor == 1 && $exibir_valor == 1){
			$width_produto = "370";
			$width_fornecedor = "200";
			$max_caracter 	= "60";
		}else if($exibir_valor == 1 && $exibir_unidade == 1){
			$width_produto = "533";
			$max_caracter 	= "180";
		}else if($exibir_fornecedor == 1 && $exibir_unidade == 1){
			$width_produto = "430";
			$width_fornecedor = "200";
			$max_caracter 	= "120";
		}else if($exibir_fornecedor == 1){
			$width_produto = "480";
			$width_fornecedor = "200";
			$max_caracter 	= "130";
		}else if($exibir_valor == 1){
			$width_produto 	= "583";
			$max_caracter 	= "200";
		}else if($exibir_unidade == 1){
			$width_produto 	= "643";
			$max_caracter 	= "220";
		}else{
			$width_produto 	= "693";
			$max_caracter 	= "250";
		}
		#ABAIXO CRIO O CABECALHO #########################################################################################################################################################
		$tabelaCabecalho =' 
			<tr style="border-bottom: 2px solid">
                <td style="width: 600px"><h1>Relat&oacute;rio de Produtos</h1></td>';
		$tabelaCabecalho2 =' 
            </tr>
            <tr style="margin-bottom:0">
                <td>
                    <table style="width: 580px;margin-bottom:0" class="table_relatorio_dados" summary="Relat&oacute;rio">
                        <tr>
                            <td style="width: 320px;">Raz&atilde;o Social: '.$rowDados['fldNome'].'</td>
                            <td style="width: 200px;">Nome Fantasia: '.$rowDados['fldNome_Fantasia'].'</td>
                            <td style="width: 320px;">CPF/CNPJ: '.$CPF_CNPJDados.'</td>
                            <td style="width: 200px;">Telefone: '.$rowDados['fldTelefone1'].'</td>
                        </tr>
                    </table>	
                </td>
                <td>        
                    <table class="dados_impressao">
                        <tr>
                            <td><b>Data: 			</b><span>'.format_date_out(date("Y-m-d")).'</span></td>
                            <td><b>Hora: 			</b><span>'.format_time_short(date("H:i:s")).'</span></td>
                            <td><b>Usu&aacute;rio: 	</b><span>'.$rowUsuario['fldUsuario'].'</span></td>
                        </tr>
                    </table>
                </td>
            </tr>
			<tr>
				<td style="margin:0"><h2 style="background:#E1E1E1;width:790px;padding-left:10px">'.$rowProduto['fldNome'].'</h2></td>
			</tr>
            <tr>
                <td style="margin:0">
                    <table class="table_relatorio" summary="Relat&oacute;rio">
                        <tr style="border:none">
							<td style="width:80px;margin-left:3px;font-weight:bold;text-align:right">C&oacute;d.</td>
							<td style="width:'.$width_produto.'px;margin-left:3px;font-weight:bold">Produto</td>'.
							($exibir_fornecedor == 1 ? '<td style="width:'.$width_fornecedor.'px;margin-left:3px;font-weight:bold">Fonecedor</td>' : '').''.
							($exibir_unidade 	== 1 ? '<td style="width:50px;margin:0;font-weight:bold;text-align:right">U.M.</td>' : '').''.
							($exibir_valor 		== 1 ? '<td style="width:95px; font-weight:bold;text-align:right">Valor</td>' : '').'
						</tr> 
					</table>
				</td>
			</tr>';
			$tabelaCabecalho3 ='
			<tr>
				<td>
					<table class="table_relatorio" summary="Relat&oacute;rio">';
	######################################################################################################################################################################################
		//ja esta fazendo a consulta na pagina anterior
		$rowsRelatorio	= mysql_num_rows($rsRelatorio);
		
		$n	 			= 1; #DEFINE O NUMERO DO BLOCO
		$countProduto 	= 1; #DEFINE CONTAGEM DE ITENS NO WHILE PARA QUEBRA DE BLOCO AO ATINGIR LIMITE 
		$x				= 1; #DEFINE CONTAGEM DE ITENS TOTAIS, PRA SABER SE JA TERMINOU WHILE MAS AINDA FALTA ESPACO
		$limite 		= 44;
		
		$pgTotal 		= ceil($rowsRelatorio / $limite);
		$p = 1;
		
		while($rowRelatorio = mysql_fetch_array($rsRelatorio)){
			echo mysql_error();
			
		
			if($rowRelatorio['fldUN_Medida_Id'] != null){
				$rsUnidade 	= mysql_query("SELECT * FROM tblproduto_unidade_medida WHERE fldId=".$rowRelatorio['fldUN_Medida_Id']);
				$rowUnidade = mysql_fetch_array($rsUnidade);
			}
		
			$pagina[$n] .= '
			<tr>
				<td style="width:80px; margin-left: 3px;text-align:right">'.str_pad($rowRelatorio['fldCodigo'], 8, "0", STR_PAD_LEFT).'</td>
				<td style="width:'.$width_produto.'px;margin-left:3px">'.mb_substr($rowRelatorio['fldNomeProduto'], 0, $max_caracter).'</td>'.
				($exibir_fornecedor	== 1 ? '<td style="width:'.$width_fornecedor.'px;margin-left:3px">'.mb_substr($rowRelatorio['fldNomeFantasia'], 0, 30).'</td>' : '').''.
				($exibir_unidade 	== 1 ? '<td style="width:55px;margin:0">'.mb_substr($rowUnidade['fldNome'], 0, 9).'</td>' : '').''.
				($exibir_valor 		== 1 ? '<td style="width:100px;text-align:right">'.format_number_out($rowRelatorio['fldValorVenda']).'</td>' : '').'
			</tr>';
			
		
			#SE CHEGAR A 20 LINHAS, MUDA DE 'BLOCO' E RECMECA CONTAGEM
			if($countProduto == $limite){
				$countProduto = 1;
				$n ++;
			}elseif($rowsRelatorio == $x && $countProduto < $limite){ #SE JA TERMINOU O WHILE DE REGISTROS MAS AINDA NAO ATINGIU 20 LINHAS, CONTINUAR CRIANDO LINHAS ATE O LIMITE
				while($countProduto <= $limite){ $pagina[$n] .='<tr style="border:0; width:800px"></tr>'; $countProduto++;}
			}else{
				$countProduto ++;
			}
			$x ++;
		}
	
	#AGORA MANDO GERAR NA TELA PARA IMPRESSAO ############################################################################################################################################

		$x = 1;
		while($x <= $n){
			$tabelaCabecalho1 = ($x == 1)? '<table class="relatorio_print" style="page-break-before:avoid">'.$tabelaCabecalho : '<table class="relatorio_print">'.$tabelaCabecalho;
			#PRIMEIRO BLOCO (LANCADOS) ###################################################################################################################################################
				print $tabelaCabecalho1;
				
                print '<td style="width: 200px"><p class="pag">'.$p.' de '.$pgTotal.'</p></td>';
				
				print $tabelaCabecalho2;
				print $tabelaCabecalho3;
				echo  $pagina[$x];
?>
						</table>
					</td >
				</tr>
			</table>			
<?			$x ++;
			$p ++;
		}
?>                
	