<script type="text/javascript" language="javascript">
	
	$(document).ready(function () {
		// Ao mover a barra de rolagem da tabela, mover seus cabecalhos e o 'versus'
		$("div#tabela_corpo").scroll(function () {
			$('div#tabela_corpo #table_cabecalho').css('top', $(this).scrollTop());
		});
	});

</script>

<style type="text/css">

	div#tabela_corpo{
		width: 960px;      /* Largura da minha tabela na tela */
		height: 280px;     /* Altura da minha tabela na tela */
		overflow: auto;    /* Barras de rolagem autom�ticas nos eixos X e Y */
		margin: 0 auto;    /* O 'auto' � para ficar no centro da tela */
		position:relative; /* Necess�rio para os cabecalhos fixos */
		top:0;             /* Necess�rio para os cabecalhos fixos */
		left:0;            /* Necess�rio para os cabecalhos fixos */
		border: 1px solid #CCC;
		border-top: 0;
	}

	div#tabela_corpo #table_cabecalho{
		position:absolute; /* Posi��o vari�vel em rela��o ao topo da div#tabela */
		top:0;             /* Posi��o inicial em rela��o ao topo da div#tabela */
		z-index:5;         /* Para ficar por cima da tabela de dados */
	}

	div#tabela_corpo #table_pedido_listar{
		margin-top:25px;  /* 30px de altura do cabecalho horizontal + 2 pixels das bordas do cabecalho + 1 px*/
		z-index:2;		  /* Menor que dos cabecalhos, para que fique por detr�s deles */
	}

</style>


<?
	require("pedido_filtro.php");

	//a��es em grupo
	if(isset($_POST['btn_action']) ||isset($_POST['sel_action_status'])){
		require("pedido_action.php");
	}
	
	//mensagens referentes as a��es
	if(!isset($_POST['hid_action'])){
		if(isset($_SESSION['msg_alert'])){
			echo $_SESSION['msg_alert']['open_markup'];
			echo $_SESSION['msg_alert']['content'];
			echo $_SESSION['msg_alert']['close_markup'];
			unset($_SESSION['msg_alert']);
		}
		if(isset($_SESSION['msg_ok'])){
			echo $_SESSION['msg_ok']['open_markup'];
			echo $_SESSION['msg_ok']['content'];
			echo $_SESSION['msg_ok']['close_markup'];
			
			unset($_SESSION['msg_ok']);
		}
	}
	
	
	if(isset($_GET['mensagem']) && $_GET['mensagem'] == "ok"){
?>		<div class="alert">
			<p class="ok">Registro gravado com sucesso!<p>
        </div>
<?	}
		
/**************************** ORDER BY *******************************************/
	$filtroOrder = 'fldPedidoId';
	$class 		 = 'desc';
	$order_sessao = explode(" ", $_SESSION['order_pedido']);
	if(isset($_GET['order'])){
		switch($_GET['order']){
			
			case 'codigo'		:  $filtroOrder = "fldPedidoId";   		break;
			case 'referencia'	:  $filtroOrder = "fldReferencia";  	break;
			case 'nfe'			:  $filtroOrder = "fldnumero";  		break;
			case 'cliente'		:  $filtroOrder = "fldClienteNome"; 	break;
			case 'funcionario'	:  $filtroOrder = "FuncionarioId";		break;
			case 'data'			:  $filtroOrder = "fldPedidoData";		break;
		}
		if($order_sessao[0] == $filtroOrder){
			$class = ($order_sessao[1] == 'asc') ? 'desc' : 'asc';
		}
	}
	
	//definir icone para ordem
	$_SESSION['order_pedido'] = (!$_SESSION['order_pedido'] || $_GET['order']) ? $filtroOrder.' '.$class : $_SESSION['order_pedido'];
	$pag	= ($_GET['pagina'])? '&pagina='.$_GET['pagina'] : ''; 
	$raiz 	= "index.php?p=pedido$pag&amp;order=";
	
	$order_sessao = explode(" ", $_SESSION['order_pedido']);
	$filtroOrder  = $order_sessao[0]; //pra poder comparar na listagem e exibir a class
	
/**************************** PAGINA��O *******************************************/

	$sSQL	="
		SELECT
		tblpedido_fiscal.*,
		tblnfe_natureza_operacao.fldParcelas,
		tblpedido.fldId as fldPedidoId,
		tblpedido.fldFaturado,
		tblpedido.fldReferencia,
		tblpedido.fldDesconto,
		tblpedido.fldDescontoReais,
		tblpedido.fldPedidoData,
		tblpedido.fldComissao,
		tblpedido.fldTipo_Id,
		tblpedido.fldVeiculo_Id,
		tblpedido.fldValor_Terceiros,
		tblpedido.fldComanda_Numero,
		tblpedido.fldCliente_Id,
		tblpedido.fldDependente_Id,
		tblpedido.fldObservacao AS fldPedidoObs,
		tblpedido.fldStatus,
		tblpedido.fldExcluido, 
		tblpedido.fldMarcador_Id,
		tblpedido.fldPedido_Destino_Nfe_Id,
		tblpedido_status_historico.fldData,
		tblpedido_status_historico.fldId,
		tblcliente_veiculo.fldPlaca,
		
		tblcliente.fldNome as fldClienteNome,
		tblcliente.fldNomeFantasia as fldClienteNomeFantasia,
		tblendereco_rota.fldId as fldRota_Id,
		
		tblNome2.fldNome as fldDependenteNome,
		tblNome2.fldNomeFantasia as fldDependenteNomeFantasia,
		
		(SELECT tblfuncionario.fldId FROM tblfuncionario INNER JOIN tblpedido_funcionario_servico ON tblfuncionario.fldId = tblpedido_funcionario_servico.fldFuncionario_Id
		 AND tblpedido_funcionario_servico.fldFuncao_Tipo = 1   WHERE tblpedido_funcionario_servico.fldPedido_Id = tblpedido.fldId GROUP BY tblpedido_funcionario_servico.fldFuncionario_Id, tblpedido_funcionario_servico.fldPedido_Id LIMIT 1) as fldFuncionarioId,
		 
		(SELECT MAX(fldVencimento) FROM tblpedido_parcela WHERE tblpedido_parcela.fldPedido_Id = tblpedido.fldId AND fldCredito = 0) AS fldVencimento,

		(SELECT SUM( tblpedido_parcela.fldValor	* (tblpedido_parcela.fldExcluido * -1 + 1)) FROM tblpedido_parcela WHERE fldPedido_Id = tblpedido.fldId AND fldCredito = 0) AS fldTotalParcelas,

		(SELECT SUM((tblpedido_parcela_baixa.fldValor - tblpedido_parcela_baixa.fldJuros) * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) FROM tblpedido_parcela_baixa RIGHT JOIN tblpedido_parcela ON tblpedido_parcela_baixa.fldParcela_Id = tblpedido_parcela.fldId WHERE fldPedido_Id = tblpedido.fldId AND fldCredito = 0) as fldValorBaixa,

		(SELECT SUM( tblpedido_parcela_baixa.fldDesconto * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) FROM tblpedido_parcela_baixa RIGHT JOIN tblpedido_parcela ON tblpedido_parcela_baixa.fldParcela_Id = tblpedido_parcela.fldId WHERE fldPedido_Id = tblpedido.fldId AND fldCredito = 0) as fldBaixaDesconto,

		(SELECT IFNULL(REPLACE(FORMAT(SUM(fldValor),2), ',' ,'') ,0) FROM tblpedido_funcionario_servico WHERE fldPedido_Id = tblpedido.fldId AND fldFuncao_Tipo = 2) as fldTotalServico, 

		(SELECT IFNULL(REPLACE(FORMAT(SUM((tblpedido_item.fldValor - ((tblpedido_item.fldDesconto / 100) * tblpedido_item.fldValor)) * tblpedido_item.fldQuantidade),2), ',' ,'') ,0) 
			FROM tblpedido_item WHERE fldPedido_Id = tblpedido.fldId AND fldExcluido = 0) as fldTotalItem
	FROM 
		
		tblpedido
		LEFT  JOIN tblpedido_fiscal				ON tblpedido.fldId 							= tblpedido_fiscal.fldPedido_Id $filtro_pedido_fiscal AND fldDocumento_Tipo = 1
		LEFT  JOIN tblnfe_natureza_operacao		ON tblnfe_natureza_operacao.fldId 			= tblpedido_fiscal.fldnatureza_operacao_id 
		LEFT  JOIN tblcliente 		 			ON tblpedido.fldCliente_Id 	 				= tblcliente.fldId
		LEFT  JOIN tblcliente tblNome2 			ON tblpedido.fldDependente_Id  				= tblNome2.fldId
		LEFT  JOIN tblcliente_veiculo			ON tblpedido.fldVeiculo_Id  				= tblcliente_veiculo.fldId
		LEFT  JOIN tblpedido_status_historico 	ON tblpedido_status_historico.fldPedido_Id 	= tblpedido.fldId
		
		LEFT JOIN tblendereco ON tblcliente.fldEndereco_Id = tblendereco.fldId
		LEFT JOIN tblendereco_bairro ON tblendereco.fldBairro_Id = tblendereco_bairro.fldId
		LEFT JOIN tblendereco_rota ON tblendereco_bairro.fldRota_Id = tblendereco_rota.fldId
		
		WHERE tblpedido.fldExcluido = 0
		". $_SESSION['filtro_pedido']." ORDER BY " . $_SESSION['order_pedido'];
		
	$rsPedido 	= mysql_query($sSQL);
	$_SESSION['pedido_relatorio'] = $sSQL;

	####################################################################################
	echo mysql_error();
	$rowsTotal = mysql_num_rows($rsPedido);
	//defini��o dos limites
	$limite 	= 50;
	$n_paginas 	= 7;

	$total_paginas = ceil($rowsTotal / $limite);
	if(isset($_GET["pagina"]) && $_GET["pagina"] > $total_paginas){
		$inicio = 0;
	}elseif(isset($_GET['pagina'])){
		$inicio = ($_GET['pagina'] - 1) * $limite;
	}else{
		$inicio = 0;
	}
	#####################################################################################
	while($rowTotal = mysql_fetch_array($rsPedido)){
	
		$subTotalPedido		= (($rowTotal['fldTotalItem'] + $rowTotal['fldComissao']) + $rowTotal['fldValor_Terceiros']) + $rowTotal['fldTotalServico'];
		$descontoPedido 	= ($subTotalPedido 	* $rowTotal['fldDesconto']) / 100;
		$totalPedido 		= $subTotalPedido 	- $descontoPedido;
		$totalPedido 		= $totalPedido 	- $rowTotal['fldDescontoReais'];
		if($rowTotal['fldnatureza_operacao_id'] == '2'){
			$rodapePedido 		-= $totalPedido;
			$rodapeItem			-= $rowTotal['fldTotalItem'];
		}elseif($rowTotal['fldParcelas '] == '0'){
			$rodapePedido 		= $rodapePedido;
			$rodapeItem			= $rodapeItem;
			$rodapeServico		= $rodapeServico;
			$rodapeTerceiros	= $rodapeTerceiros;
		}else{
			$rodapePedido 		+= $totalPedido;
			$rodapeItem			+= $rowTotal['fldTotalItem'];
			$rodapeServico		+= $rowTotal['fldTotalServico'];
			$rodapeTerceiros	+= $rowTotal['fldValor_Terceiros'];
		}	
		
	}	
	#####################################################################################
	$sSQL 	 	.= " LIMIT " . $inicio . "," . $limite;
	$rsPedido 	= mysql_query($sSQL);
	$pagina 	= ($_GET['pagina'] ? $_GET['pagina'] : "1");
	
?>

    <form class="table_form" id="frm_pedido" action="" method="post">
        <div id="table">
        
            <div id="tabela_corpo">
            
                 <div id="table_cabecalho" style="width:1170px">
					<ul class="table_cabecalho" style="width:1170px">
                       	<li style="width:15px"><input type="checkbox" name="chk_todos" id="chk_todos" style="width:13px; height:13px;" /></li>
                    	<li class="order" style="width:75px">
                            <a <?= ($filtroOrder == 'fldPedidoId') 		? "class='$class'" : '' ?> style="width:60px; text-align:center;" href="<?=$raiz?>codigo">C&oacute;d.</a>
                        </li>
                        <li class="order" style="width:50px;">
                            <a <?= ($filtroOrder == (($_SESSION["sistema_nfe"] > 0) ? 'fldnumero' : 'fldReferencia')) ? "class='$class'" : '' ?> style="width:35px" href="<?=$raiz?><?=($_SESSION["sistema_nfe"] > 0) ? 'nfe' : 'referencia' ?>" ><?=($_SESSION["sistema_nfe"] > 0) ? 'NFe' : 'Ref.' ?></a>
                        </li>
                        <li class="order" style="width:250px">
                            <a <?= ($filtroOrder == 'fldClienteNome') 	? "class='$class'" : '' ?> style="width:235px" href="<?=$raiz?>cliente">Cliente</a>
                        </li>
						<li class="order" style="width:180px">
<?							if(fnc_sistema('exibir_dependente') > 0){
								echo "Dependente";
							}else{
?>						      	<a <?= ($filtroOrder == 'FuncionarioId') ? "class='$class'" : '' ?> style="width:165px; margin-left:8px;" href="<?=$raiz?>funcionario">Funcion&aacute;rio</a>
<?							}
?>	                    </li>
                 	 	
						<li style="width:45px; text-align:right;">Itens</li>
                        <li style="width:80px; text-align:right;">Total</li>
                        <li style="width:25px; text-align:right;">&nbsp;</li>
                        <li style="width:20px; text-align:right;">&nbsp;</li>
                        <li style="width:20px; text-align:right;">&nbsp;</li>
                        <li class="order" style="width:60px;text-align:center">
                            <a <? ($filtroOrder == 'fldPedidoData') ? print "class='$class'" : '' ?> style="width:45px" href="<?=$raiz?>data">Data</a>
                        </li>
<?						if($_SESSION["sistema_nfe"] > 0){                        
?>                        	<li style="width:50px">&nbsp;</li>
                            <li class="order" style="width:50px;">
                                <a <?= ($filtroOrder == 'fldReferencia')	? "class='$class'" : '' ?> style="width:35px" href="<?=$raiz?>referencia">Ref.</a>
                            </li>
<?						}
?>                      <li style="width:150px; padding-left:5px;">Observa&ccedil;&atilde;o</li>
					</ul>
                </div>
                        
               
                <table id="table_pedido_listar" class="table_general table_nfe_listar" summary="Lista de vendas" style=" width:1170px">
                    <tbody>
<?						$impressao = fnc_sistema('sistema_impressao');
						if(isset($impressao)){
							$rowImpressao = mysql_fetch_array(mysql_query("SELECT * FROM tblsistema_impressao WHERE fldId = $impressao"));
							$impressao  = $rowImpressao['fldModelo'];
							$impressao .= ($rowImpressao['fldVariacao'] != '') ? "_".$rowImpressao['fldVariacao'] : '';
						}
						
						$id_array = array();
						$n = 0;
						
						$linha 	 = "row";
						$rows 	 = mysql_num_rows($rsPedido);
						while($rowPedido = mysql_fetch_array($rsPedido)){
							unset($item);
							unset($subTotalPedido);
							
							$pedido_id 		= $rowPedido['fldPedidoId'];
							$id_array[$n] 	= $pedido_id;
							$n += 1;
						
							$sqlItem 		= mysql_query("SELECT SUM(fldQuantidade) as fldTotalQuantidade FROM tblpedido_item WHERE fldPedido_Id = $pedido_id");
							$rowItem 		= mysql_fetch_array($sqlItem);
							
							$totalItem		= $rowPedido['fldTotalItem'];
							$totalServico	= $rowPedido['fldTotalServico'];
							$itemQtd		= $rowItem['fldTotalQuantidade'];	
							$descPedido 	= $rowPedido['fldDesconto'];
							$descPedidoReais= $rowPedido['fldDescontoReais'];
							
							$subTotalPedido	= (($totalItem 		+ $rowPedido['fldComissao']) + $rowPedido['fldValor_Terceiros']) + $totalServico;
							$descontoPedido = ($subTotalPedido 	* $descPedido) / 100;
							$totalPedido 	= $subTotalPedido 	- $descontoPedido;
							$totalPedido 	= $totalPedido 		- $descPedidoReais;
							$paginaLink 	= redirecionarPagina($rowPedido['fldTipo_Id'], 'venda');
							
							$referencia		= ($_SESSION["sistema_nfe"] > 0 && $rowPedido['fldnumero'] >0)? str_pad($rowPedido['fldnumero'], 6, "0", STR_PAD_LEFT) : (($rowPedido['fldReferencia'] > 0) ? $rowPedido['fldReferencia'] :'');
							
							$faturado_class	= ($rowPedido['fldFaturado'] == 0) ? "nao_faturado"			: "faturado";
							$faturado_titulo= ($rowPedido['fldFaturado'] == 0) ? "n&atilde;o faturado" 	: "faturado";
							
							if($rowPedido['fldnfe_status_id'] > 0){
								$rsNfeStatus 		= mysql_query('SELECT fldStatus FROM tblnfe_pedido_status WHERE fldId ='.$rowPedido['fldnfe_status_id']);
								$rowNfeStatus 		= mysql_fetch_array($rsNfeStatus);
								$nfe_status			= $rowNfeStatus['fldStatus'];
								$transmitir_class 	= 'nfe_transmitir_disabled';
								$danfe_class 		= 'nfe_danfe_disabled';
								#CASO ESTEJA VALIDADO E NAO TRANSMITIDO
								switch($rowPedido['fldnfe_status_id']){
									case 2 :
										$nfe_status 	= 'processo';
									break;
									case 4:
										if($rowPedido['flddanfe'] == '1'){
											$danfe_class 		= 'nfe_danfe_print';
											$danfe_title		= 'Danfe impresso';
										}else{
											$danfe_class 		= 'nfe_danfe';
											$danfe_title		= 'Imprimir Danfe';
										}
									break;
									case 5:
										$transmitir_class 	= 'nfe_transmitir';
									break;
								}
								
							}else{
								$nfe_status 		= 'digitacao';
								$transmitir_class 	= 'nfe_transmitir';
								$danfe_class 		= 'nfe_danfe_disabled';
							}
							
?>							<tr class="<?= $linha; ?>"  id="<?=$rowPedido['fldPedidoId']?>" style="display: table">
								<td style="width:15px"><input type="checkbox" name="chk_pedido_<?=$rowPedido['fldPedidoId']?>" id="chk_pedido_<?=$rowPedido['fldPedidoId']?>" style="width:15px; height:15px; margin-top:3px" title="selecionar o registro posicionado" /></td>
								<td style="width:75px"><span title="<?= fnc_status_pedido($rowPedido['fldPedidoId'], "tblpedido")?>" class="<?=fnc_status_pedido($rowPedido['fldPedidoId'], "tblpedido")?>"><?=str_pad($rowPedido['fldPedidoId'], 5, "0", STR_PAD_LEFT)?></span></td>
								<td style="width:50px;display:inline;float:left;text-align:center; color:#C90"><?=$referencia?></td>
								<td	style="width:255px;float:left;text-align:left"><?=substr($rowPedido['fldClienteNome'],0,38)?></td>
								<td style="width:180px"">
<?
								if(fnc_sistema('exibir_dependente') > 0){ 
									echo substr($rowPedido['fldDependenteNome'],0,20);
								}
								else{
									if(!is_null($rowPedido['fldFuncionarioId']) or $rowPedido['fldFuncionarioId'] != ''){
										$rsFuncionario 	 = mysql_query('SELECT fldNome FROM tblfuncionario WHERE fldId = '.$rowPedido['fldFuncionarioId']);
										$rowFuncionario  = mysql_fetch_array($rsFuncionario);
										$funcionarioNome = $rowFuncionario['fldNome'];
										$func_id 		 = $rowPedido['fldFuncionarioId'];
									} else {
										//vamos checar se ela � uma nfe importada... e pegar o funcionario da importada, se houver, � claro...
										$sql_origem		= mysql_query("SELECT fldId FROM tblpedido WHERE fldPedido_Destino_Nfe_Id = ".$rowPedido['fldPedidoId']);
										$venda_origem 	= mysql_num_rows($sql_origem);
										if($venda_origem > 0){
											$venda_origem = mysql_fetch_array($sql_origem);
											$venda_origem = $venda_origem['fldId'];
										
											$rsFuncionario  = mysql_query("SELECT tblfuncionario.fldNome, tblfuncionario.fldId FROM tblfuncionario 
																			INNER JOIN tblpedido_funcionario_servico ON tblfuncionario.fldId = tblpedido_funcionario_servico.fldFuncionario_Id
																			AND tblpedido_funcionario_servico.fldFuncao_Tipo = 1 
																			WHERE tblpedido_funcionario_servico.fldPedido_Id = $venda_origem 
																			GROUP BY tblpedido_funcionario_servico.fldFuncionario_Id, tblpedido_funcionario_servico.fldPedido_Id LIMIT 1");									
											$rowFuncionario 	= mysql_fetch_array($rsFuncionario);
											$funcionarioNome 	= $rowFuncionario['fldNome'];
											$func_id 			= $rowFuncionario['fldId'];
										} else {
											$funcionarioNome = '';
										}
									}
	
									if(isset($func_id) and $func_id != '' and $func_id != null){
?>										<a href="<?='?p=funcionario_detalhe&id='.$func_id?>" style="padding-left:3px;" title="Visualizar/Editar Funcion&aacute;rio"><?=substr($funcionarioNome, 0, 17)?></a>
<?									}
								} 
								
?>								</td>
                                <td style="width:45px; text-align:right"><?=format_number_out($itemQtd)?></td>
								<td style="width:80px; text-align:right"><?=format_number_out($totalPedido)?></td>
                                
								<td style="width:25px"><span class="<?=$faturado_class?>" title="<?=$faturado_titulo?>">&nbsp;</span></td>
								<td style="width:20px">
                           			<a class="edit" href="index.php?p=<?=$paginaLink;?>&id=<?=$rowPedido['fldPedidoId'].$comanda?>" title="editar" style="margin-top: 2px;"></a>
								</td>
								<td style="width:20px"><a class="print" title="imprimir" rel="externo" href="pedido_imprimir_<?=$impressao?>.php?&amp;id=<?=$rowPedido['fldPedidoId']?>"></a></td>							
								<td style="width:60px; text-align:center;"><?=format_date_out3($rowPedido['fldPedidoData'])?></td>
<?								if($_SESSION["sistema_nfe"] > 0){	
									if($rowPedido['fldTipo_Id'] != 3){
										$transmitir_class 	= 'nfe_transmitir_disabled';
										$danfe_class 		= 'nfe_danfe_disabled';
										$nfe_status 		= 'disabled';
									}
?>									<td style="width:15px"><a href="pedido_nfe_consulta_status,<?=$rowPedido['fldPedidoId']?>" name="nfe_status" class="nfe_status_<?=$nfe_status?> modal" title="<?=$nfe_status?>" rel="780-300"></a></td>
									<td style="width:20px"><a id="modal_transmitir" href="pedido_nfe_transmitir,<?=$rowPedido['fldPedidoId']?>,1" class="modal <?=$transmitir_class?>" title="Transmitir NFe" rel="780-300"></a></td>
									<td style="width:15px"><a href="pedido_nfe_danfe.php?id=<?=$rowPedido['fldPedidoId']?>" target="_blank" class="<?=$danfe_class?>" title="<?=$danfe_title?>"></a></td>
                                    <td style="width:50px; text-align:right"><?=($rowPedido['fldReferencia'] >0 ) ? $rowPedido['fldReferencia'] : ''?></td>
                                    <td style="width:160px;padding-left:5px"><?=substr($rowPedido['fldPedidoObs'],0,28)?></td>
<?								}else{
?>                            		<td style="width:290px;padding-left:5px"><?=substr($rowPedido['fldPedidoObs'],0,55)?></td>
<?								}
?>                            </tr>
<?							$linha = ($linha == "row" )? "dif-row" : "row" ;
							unset($comanda);
						}
?>			 		</tbody>
				</table>
			</div>
            
            
            <!------>
       	 	<div class="saldo" style="width:952px;float:right;">
                <p style="padding:5px;width:150px;float:left; margin-left:280px">Total	<span style="margin-left:6px width:100px;text-align:right" class="credito"><?=format_number_out($rodapePedido)?></span></p>
                <p style="padding:5px;width:150px;float:left">Produtos					<span style="margin-left:6px width:100px;text-align:right" class="credito"><?=format_number_out($rodapeItem)?></span></p>
                <p style="padding:5px;width:150px;float:left">Servi&ccedil;os			<span style="margin-left:6px width:100px;text-align:right" class="credito"><?=format_number_out($rodapeServico)?></span></p>
                <p style="padding:5px;width:155px;float:left">Terceiros					<span style="margin-left:6px width:100px;text-align:right" class="credito"><?=format_number_out($rodapeTerceiros)?></span></p>
			</div>
            
            <input type="hidden" name="hid_array" id="hid_array" value="<?=urlencode(serialize($id_array))?>" />
            <input type="hidden" name="hid_action" id="hid_action" value="true" />
			
            <div id="table_paginacao" style="width: 962px">
                <div class="table_registro" style="float:left;margin:0;text-align:left; padding-left:10px">
                    <span>Exibindo registros <?=($pagina*$limite-$limite+1).' a '.($pagina*$limite-$limite+$rows)?> do total de <?=$rowsTotal?></span>
           		</div>
                <div style="width:400px; height:25px;float:right;display:table;text-align:right;padding-left:20px">
<?					$paginacao_destino = "?p=pedido&modo=listar";
					include("paginacao.php");
?>          	</div>
			</div>   
                 
            <div id="table_action">
                <ul id="action_button">
                    <li><a class="btn_novo" href="index.php?p=pedido_novo" title="novo">novo</a></li> 
                	
<?					if($_SESSION["sistema_nfe"] > 0){					
?>                      <li><a class="btn_nfe_novo" href="index.php?p=pedido_nfe_novo" title="nova NFe">nova NFe</a><a class="btn_nfe_novo_documento modal" href="nfe_documento_novo,pedido" rel="400-120" title="outro documento fiscal"></a></li>
                        <li><input type="submit" name="btn_action" id="btn_alterarnumero"	value="alterar numero"	title="Alterar n&uacute;mero" /></li>
                        <li style="display:none"><a id="alterar_numero_verificado" class="modal btn_importar" href="nfe_alterar_numero,1,<?=$filtro_id?>" rel="520-375" title="alterar numero">alterar</a></li>
                        <li><input type="submit" name="btn_action" id="btn_desfazer" 	value="desfazer importa&ccedil;&atilde;o" title="Desfazer NFe(s) importada(s)" onclick="return confirm('Deseja desfazer NFe(s) selecionada(s)?\nObs.: Apenas NFe(s) originada(s) de vendas comuns ser&aacute;(&atilde;o) desfeita(s)!')" /></li>
                    	<li><input type="submit" name="btn_action" id="btn_download" 	value="download"	title="Fazer download de registro(s) selecionado(s)"/></li>
<?					}                   
?>					<li><input type="submit" name="btn_action" id="btn_excluir" 	value="excluir" 	title="Excluir registro(s) selecionado(s)" onclick="return confirm('Deseja excluir os registros selecionados?')" /></li>
					<li><a id="btn_print" class="modal" href="pedido_relatorio_tipo" rel="350-100" title="Imprimir Relat&oacute;rio">&nbsp;</a></li>
                    <!--<li>
                        <select id="sel_action_status" name="sel_action_status" style="background:#E9E669; width:130px; height:25px">
                            <option value="">alterar status</option>
                            <option value="aprovado">aprovado</option>
                            <option value="finalizado">finalizado</option>
                            <option value="entregue">entregue</option>
                        </select>                        
                    </li>-->
                </ul>
        	</div>
        </div>
	</form>
    
    <ul class="status_legenda" style="margin-top: 20px">
    	<li><span class="orcamento">or&ccedil;amento</span></li>
        <li><span class="andamento">em andamento	</span></li>
        <li><span class="finalizado">finalizado		</span></li>
        <li><span class="entregue">entregue			</span></li>
        <li><span class="recusado">recusado			</span></li>
        <li><span class="convertida" style="font-weight:normal">convertidas</span></li>
    </ul>
    
    
    
	<script type="text/javascript">
	
		$('a#modal_transmitir').click(function(){
			setTimeout(fnc_nfe_consulta, 20000);
			//fnc_nfe_consulta();
		});
		
		$('#sel_action_status').change(function(){
			$('#frm_pedido').submit();
		});
		
	</script>	
    