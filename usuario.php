<div id="voltar">
    <p><a href="index.php">n&iacute;vel acima</a></p>
</div>
<h2>Usuario</h2>
<?

	unset($_SESSION['txt_usuario_novo']);
	unset($_SESSION['txt_senha_novo']);
	
	require("usuario_filtro.php");
	
	//a��es em grupo
	if(isset($_POST['hid_action'])){
		require("usuario_action.php");
	}
	
	if(isset($_GET['mensagem']) && $_GET['mensagem'] == "ok"){
?>		<div class="alert" style="margin-top:5px">
			<p class="ok">Registro gravado com sucesso!<p>
        </div>
<?	}

/**************************** ORDER BY *******************************************/
	$filtroOrder  = 'tblusuario.fldUsuario ';
	$class 		  = 'asc';
	$order_sessao = explode(" ", $_SESSION['order_usuario']);
	if(isset($_GET['order'])){
		switch($_GET['order']){
			
			case 'codigo'		:  $filtroOrder = "tblusuario.fldId";   	break;
			case 'usuario'		:  $filtroOrder = "tblusuario.fldUsuario"; 	break;
			case 'funcionario'	:  $filtroOrder = "tblfuncionario.fldNome";	break;
		}
		if($order_sessao[0] == $filtroOrder){
			$class = ($order_sessao[1] == 'asc') ? 'desc' : 'asc';
		}
	}
	
	//definir icone para ordem
	$_SESSION['order_usuario'] = (!$_SESSION['order_usuario'] || $_GET['order']) ? $filtroOrder.' '.$class : $_SESSION['order_usuario'];
	$pag	= ($_GET['pagina'])? '&pagina='.$_GET['pagina'] : ''; 
	$raiz 	= "index.php?p=usuario$pag&amp;order=";
	
	$order_sessao = explode(" ", $_SESSION['order_usuario']);
	$filtroOrder  = $order_sessao[0]; //pra poder comparar na listagem e exibir a class
/**************************** PAGINA��O *******************************************/
					
	$sSQL 		= "select tblusuario.*, tblfuncionario.fldNome from tblusuario left join tblfuncionario on tblfuncionario.fldId = tblusuario.fldFuncionario_Id ". $_SESSION['filtro_usuario']." order by " . $_SESSION['order_usuario'];
	$rsTotal 	= mysql_query($sSQL);
	$rowsTotal 	= mysql_num_rows($rsTotal);
	
	//defini��o dos limites
	$limite 	= 50;
	$n_paginas 	= 7;
	
	$total_paginas = ceil(mysql_num_rows($rsTotal) / $limite);
	
	if(isset($_GET["pagina"]) && $_GET["pagina"] > $total_paginas){
		$inicio = 0;
	}elseif(isset($_GET['pagina'])){
		$inicio = ($_GET['pagina'] - 1) * $limite;
	}else{
		$inicio = 0;
	}
	
	$sSQL .= " limit " . $inicio . "," . $limite;
	$rsUsuario = mysql_query($sSQL);
	
	$pagina = ($_GET['pagina'] ? $_GET['pagina'] : "1");
	
#########################################################################################
?>

    <form class="table_form" id="frm_usuario" action="" method="post">
    	<div id="table">
            <div id="table_cabecalho">
                <ul class="table_cabecalho">
                    <li class="order" style="width:70px">
                    	<a <?= ($filtroOrder == 'tblusuario.fldId') 		? "class='$class'" : '' ?> style="width:0px" href="<?=$raiz?>codigo">C&oacute;digo</a>
                    </li>
                    <li class="order" style="width:220px">
                    	<a <?= ($filtroOrder == 'tblusuario.fldUsuario') 	? "class='$class'" : '' ?> style="width:205px" href="<?=$raiz?>usuario">Usu&aacute;rio</a>
                    </li>
                    
                    <li class="order" style="width:550px">
                    	<a <?= ($filtroOrder == 'tblfuncionario.fldNome') 	? "class='$class'" : '' ?> style="width:535px" href="<?=$raiz?>funcionario">Funcion&aacute;rio</a>
                    </li>
                  <li style="width:67px"></li>
                  <li style="width:20px"><input type="checkbox" name="chk_todos" id="chk_todos" /></li>
                </ul>
            </div>
            <div id="table_container">       
                <table id="table_general" class="table_general" summary="Lista de usu&aacute;rios">
                <tbody>
<?					
					$id_array = array();
					$n = 0;
					
					$linha = "row";
					$rows = mysql_num_rows($rsUsuario);
					while ($rowUsuario = mysql_fetch_array($rsUsuario)){
						
						$id_array[$n] = $rowUsuario["fldId"];
						$n += 1;
						
?>						<tr class="<?= $linha; ?>">
                            <td class="cod"	style="width:65px; padding-right:10px; text-align:center"><?=$rowUsuario['fldId']?></td>
							<td style="width:220px"><?=$rowUsuario['fldUsuario']?></td>
<?							//verifica se h� um funcionario para o usuario	
                            if($rowUsuario['fldFuncionario_Id'] <> 0){
?>                          	<td style="width:565px"><?=$rowUsuario['fldNome']?></td>
<?							}else{
?>                          	<td style="width:565px">Usu&aacute;rio do sistema</td>
<?							}
?>
							<td style="width:30px"></td>
							<td style="width:auto"><a class="edit" href="index.php?p=usuario_detalhe&amp;id=<?=$rowUsuario['fldId']?>" title="editar"></a></td>
                          	<td style="width:auto"><input type="checkbox" name="chk_usuario_<?=$rowUsuario['fldId']?>" id="chk_usuario_<?=$rowUsuario['fldId']?>" title="selecionar o registro posicionado" /></td>
                        </tr>
<?                      $linha = ($linha == "row" )? "dif-row" : "row" ;
                   }
?>		 		</tbody>
				</table>
            </div>
       	  
            <input type="hidden" name="hid_array" id="hid_array" value="<?=urlencode(serialize($id_array))?>" />
            <input type="hidden" name="hid_action" id="hid_action" value="true" />
            
			<div id="table_action">
                <ul id="action_button">
                    <li><a class="btn_novo" href="index.php?p=usuario_novo">novo</a></li>
                    <li><input type="submit" name="btn_action" id="btn_excluir" value="excluir" title="Excluir registro(s) selecionado(s)" onclick="return confirm('Deseja excluir os registros selecionados?')" /></li>
                </ul>
        	</div>
            <div id="table_paginacao">
<?				$paginacao_destino = "?p=usuario";
				include("paginacao.php")
?>		
            </div>   
            <div class="table_registro">
            	<span>Exibindo registros <?=($pagina*$limite-$limite+1).' a '.($pagina*$limite-$limite+$rows)?> do total de <?=$rowsTotal?></span>
            </div>       
        
        </div>
       
	</form>