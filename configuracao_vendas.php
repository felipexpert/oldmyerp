<div id="principal">
<?

	if(isset($_POST["btn_enviar"])){
		
		// ACAO ############################################################################################################
	
		$juros = format_number_in($_POST['txt_juros']);
		$multa = format_number_in($_POST['txt_multa']);
		
		$status 							= $_POST['sel_status'];
		$multaTipo 							= $_POST['sel_multa_tipo'];
		$jurosTipo 							= $_POST['sel_juros_tipo'];
		$filtro 							= $_POST['sel_filtro'];
		$verificar_limite_credito 			= $_POST['sel_limite_credito'];
		$pedido_observacao_cliente 			= $_POST['sel_observacao_cliente'];
		$pedido_calcular_comissao 			= $_POST['sel_calcular_comissao'];
		$alerta_estoque_negativo			= $_POST['sel_alerta_estoque'];
		$exibir_devedor_anterior			= $_POST['sel_exibir_devedor'];
		$exibir_endereco_impressao			= $_POST['sel_exibir_endereco'];
		$venda_servico_valor 				= format_number_in($_POST['txt_servico_valor']);
		$faturado							= $_POST['sel_fatura'];
		$exibir_rgcpf_cliente_impressao 	= $_POST['sel_exibir_rgcpf_cliente_impressao'];
		$exibir_endereco_entrega 			= $_POST['sel_exibir_end_entrega'];
		$exibir_retirado_por 				= $_POST['sel_exibir_retirado_por'];
		$exibir_produtos 					= $_POST['sel_exibir_produtos'];
		$exibir_marca	 					= $_POST['sel_exibir_marca'];
		$exibir_categoria					= $_POST['sel_exibir_categoria'];

		$venda_rapida_campo_retorno			= $_POST['sel_venda_rapida_campo_retorno'];
		$venda_rapida_inserir_auto			= $_POST['sel_venda_rapida_inserir_automatico'];
		
		$exibir_terceiros 					= $_POST['sel_exibir_terceiros'];
		$exibir_servicos 					= $_POST['sel_exibir_servicos'];
		$exibir_desc_porcent 				= $_POST['sel_exibir_desc_porcent'];
		$exibir_desc_reais   				= $_POST['sel_exibir_desc_reais'];
		$exibir_item_repetido_alerta		= $_POST['sel_exibir_item_repetido_alerta'];
		$exibir_rodape 						= $_POST['sel_exibir_rodape'];
		$obsCarne							= $_POST['txt_obs_carne'];
		$exibir_dados_empresa    		    = $_POST['sel_exibir_dados_empresa'];
		
		$recibo_exibir_multa				= $_POST['sel_recibo_exibir_multa'];
		$recibo_exibir_juros				= $_POST['sel_recibo_exibir_juros'];
		$recibo_exibir_devedor				= $_POST['sel_recibo_exibir_devedor'];
		$recibo_exibir_assinatura			= $_POST['sel_recibo_exibir_assinatura'];
		
		fnc_sistema_update('pedido_multa_tipo',					$multaTipo);
		fnc_sistema_update('pedido_multa', 						$multa);
		fnc_sistema_update('pedido_juros', 						$juros);
		fnc_sistema_update('pedido_status', 					$status);
		fnc_sistema_update('pedido_juros_tipo', 				$jurosTipo);
		fnc_sistema_update('pedido_filtro', 					$filtro);
		fnc_sistema_update('pedido_observacao_cliente',			$pedido_observacao_cliente);
		fnc_sistema_update('venda_alerta_limite_credito', 		$verificar_limite_credito);
		fnc_sistema_update('pedido_comissao', 					$pedido_calcular_comissao);
		fnc_sistema_update('alerta_estoque_negativo', 			$alerta_estoque_negativo);
		fnc_sistema_update('venda_servico_valor', 				$venda_servico_valor);
		fnc_sistema_update('pedido_exibir_devedor', 			$exibir_devedor_anterior);
		fnc_sistema_update('pedido_exibir_endereco',			$exibir_endereco_impressao);
		fnc_sistema_update('venda_faturado', 					$faturado);
		fnc_sistema_update('pedido_rgcpf_cliente_impressao',	$exibir_rgcpf_cliente_impressao);
		fnc_sistema_update('pedido_retirado_por',				$exibir_retirado_por);
		fnc_sistema_update('pedido_produtos_impressao',			$exibir_produtos);
		fnc_sistema_update('pedido_marca_impressao',			$exibir_marca);
		fnc_sistema_update('pedido_categoria_impressao',		$exibir_categoria);

		fnc_sistema_update('venda_rapida_produto_inserir_automatico',$venda_rapida_inserir_auto);
		fnc_sistema_update('venda_rapida_primeiro_campo',			$venda_rapida_campo_retorno);
		
		fnc_sistema_update('pedido_terceiros_impressao',		$exibir_terceiros);
		fnc_sistema_update('pedido_servicos_impressao',			$exibir_servicos);
		fnc_sistema_update('pedido_desconto_porcent_impressao',	$exibir_desc_porcent);
		fnc_sistema_update('pedido_desconto_reais_impressao',	$exibir_desc_reais);
		fnc_sistema_update('pedido_item_repetido_alerta',		$exibir_item_repetido_alerta);
		fnc_sistema_update('rodape_impressao', 					$exibir_rodape);
		fnc_sistema_update('pedido_impressao_obs_carne', 		$obsCarne);
		fnc_sistema_update('impressao_mostrar_dados_empresa', 	$exibir_dados_empresa);
		
		fnc_sistema_update('recibo_exibir_multa', 				$recibo_exibir_multa);
		fnc_sistema_update('recibo_exibir_juros', 				$recibo_exibir_juros);
		fnc_sistema_update('recibo_exibir_devedor', 			$recibo_exibir_devedor);
		fnc_sistema_update('recibo_exibir_assinatura_funcionario', 	$recibo_exibir_assinatura);
		
		$_SESSION["sistema_pedido_juros_tipo"] 					= $jurosTipo;
		$_SESSION["sistema_pedido_multa_tipo"] 					= $multaTipo;
		
		$_SESSION["sistema_venda_alerta_limite_credito"] 		= $verificar_limite_credito;		
			
		$_SESSION["sistema_recibo_exibir_multa"] 				= $recibo_exibir_multa;
		$_SESSION["sistema_recibo_exibir_juros"] 				= $recibo_exibir_juros;
		$_SESSION["sistema_recibo_exibir_devedor"] 				= $recibo_exibir_devedor;
		$_SESSION["sistema_recibo_exibir_assinatura_funcionario"] = $recibo_exibir_assinatura;
		
		//ATUALIZA A ACAO DE ACORDO COM A PARCELA
		//ATUALIZA A PARCELA A VISTA, QUE NAO ESTA DENTRO DO WHILE, PQ NAO FOI CADASTRADA COMO UM TIPO DE PAG
		$txtAcao = $_POST['sel_parcela_acao_AV'];
		fnc_sistema_update('pedido_parcela_acao_AV', $txtAcao);
		echo mysql_error();
		
		//ATUALIZA A ACAO DE ACORDO COM A PARCELA
		$rsPagamentoTipo = mysql_query("SELECT * FROM tblpagamento_tipo WHERE fldExcluido = 0");
		while($rowPagamentoTipo = mysql_fetch_array($rsPagamentoTipo)){
			$sigla		 = $rowPagamentoTipo['fldSigla'].$rowPagamentoTipo['fldSiglaSub'];
			$txtAcao	 = $_POST["sel_parcela_acao_$sigla"];
			$sistemaAcao = fnc_sistema("pedido_parcela_acao_$sigla");
			
			if($sistemaAcao > 0){
				fnc_sistema_update("pedido_parcela_acao_$sigla", $txtAcao);
			}else{
				if($sistemaAcao > 0){
					mysql_query("INSERT INTO tblsistema (fldParametro, fldValor) VALUES (
					'pedido_parcela_acao_$sigla',
					'$txtAcao'
					)");
				}
			}
			echo mysql_error();
			//ATUALIZA A CONTA QUE VAI CAIR DE ACORDO COM A PARCELA
			$txtConta 		= $_POST["sel_parcela_conta_$sigla"];
			$sistemaConta 	= fnc_sistema("pedido_parcela_conta_$sigla");
			
			if($sistemaConta > 0){
				fnc_sistema_update("pedido_parcela_conta_$sigla", $txtConta);
			}else{
				if($sistemaConta > 0){
					mysql_query("INSERT INTO tblsistema (fldParametro, fldValor) VALUES (
					'pedido_parcela_conta_".$sigla."',
					'" . $txtConta . "'
					)");
				}
			}
			echo mysql_error();
		}
		############################################################################################################
		
		if(mysql_error()){
?>		  	<div class="alert">
                <p class="erro">N&atilde;o foi poss&iacute;vel gravar os dados</p>
            </div>
<?			echo mysql_error();
		}else{
?>        	<div class="alert" style="margin-top: 5px">
        	    <p class="ok">Configura&ccedil;&otilde;es atualizadas!</p>
       		</div>
<?		}
	}

	$sistemaJuros 				= fnc_sistema('pedido_juros');
	$sistemaStatus 				= fnc_sistema('pedido_status');
	$sistemaFiltro 				= fnc_sistema('pedido_filtro');
	$sistemaJurosTipo 			= fnc_sistema('pedido_juros_tipo');
	$sistemaObservacao 			= fnc_sistema('pedido_observacao_cliente');
	$sistemaLimite_Credito 		= fnc_sistema('venda_alerta_limite_credito');
	$sistemaItem_Duplicado		= fnc_sistema('pedido_item_repetido_alerta');
	$sistemaMulta 				= fnc_sistema('pedido_multa');
	$sistemaMultaTipo			= fnc_sistema('pedido_multa_tipo');
	$sistemaComissao			= fnc_sistema('pedido_comissao');
	$sistemaAlertaNegativo		= fnc_sistema('alerta_estoque_negativo');
	$sistemaExibirDevedor		= fnc_sistema('pedido_exibir_devedor');
	$sistemaServicoValor		= fnc_sistema('venda_servico_valor');
	$sistemaFatura				= fnc_sistema('venda_faturado');
	$sistemaExibirEndereco		= fnc_sistema('pedido_exibir_endereco');
	$sistemaExibirRGCPFCliente	= fnc_sistema('pedido_rgcpf_cliente_impressao');
	$sistemaExibirRetiradoPor	= fnc_sistema('pedido_retirado_por');
	$sistemaExibirProdutos		= fnc_sistema('pedido_produtos_impressao');
	$sistemaExibirMarca			= fnc_sistema('pedido_marca_impressao');
	$sistemaExibirCategoria		= fnc_sistema('pedido_categoria_impressao');

	$sistemaVendaCampoRetorno	= fnc_sistema('venda_rapida_primeiro_campo');
	$sistemaVendaInserirAuto	= fnc_sistema('venda_rapida_produto_inserir_automatico');
	
	$sistemaExibirTerceiros		= fnc_sistema('pedido_terceiros_impressao');
	$sistemaExibirServicos		= fnc_sistema('pedido_servicos_impressao');
	$sistemaExibirDescPorc		= fnc_sistema('pedido_desconto_porcent_impressao');
	$sistemaExibirDescRs		= fnc_sistema('pedido_desconto_reais_impressao');
	$sistemaExibirRodape		= fnc_sistema('rodape_impressao');
	$sistemaObsCarne			= fnc_sistema('pedido_impressao_obs_carne');
	$sistemaExibirDadosEmpresa  = fnc_sistema('impressao_mostrar_dados_empresa');
	
	
	$sistema_recibo_exibir_multa = fnc_sistema('recibo_exibir_multa');
	$sistema_recibo_exibir_juros = fnc_sistema('recibo_exibir_juros');
	$sistema_recibo_exibir_devedor 		= fnc_sistema('recibo_exibir_devedor');
	$sistema_recibo_exibir_assinatura 	= fnc_sistema('recibo_exibir_assinatura_funcionario');

?>		<div class="form">
			<h3>Configura&ccedil;&otilde;es padr&atilde;o</h3>
            <form id="frm_vendas" class="frm_detalhe" style="width:960px" action="" method="post">
            	<ul>
               		<li style="padding:8px; background: #D7D7D7"> 
                        <label for="sel_filtro">Exibir vendas</label>
                        <select style="width:150px;" id="sel_filtro" name="sel_filtro" class="sel_filtro" >
                            <option <?=($sistemaFiltro == 'todos') 			? 'selected="selected"' : '' ?> value="todos">todos</option>
                            <option <?=($sistemaFiltro == 'aberto') 		? 'selected="selected"' : '' ?> value="aberto">aberto</option>
                            <option <?=($sistemaFiltro == 'pago total') 	? 'selected="selected"' : '' ?> value="total">pago total</option>
                            <option <?=($sistemaFiltro == 'pago parcial') 	? 'selected="selected"' : '' ?> value="parcial">pago parcial</option>
                            <option <?=($sistemaFiltro == 'vencidos') 		? 'selected="selected"' : '' ?> value="vencidos">vencidos</option>
						</select>
                    </li>
                    <li style="padding:8px; background:#D7D7D7"> 
                        <label for="sel_status">Status em nova venda</label>
                        <select style="width:150px;" id="sel_status" name="sel_status" class="sel_status" >
<?							$rsStatus = mysql_query("SELECT * FROM tblpedido_status");
							while($rowStatus = mysql_fetch_array($rsStatus)){                            
?>                   	       <option <?=($sistemaStatus == $rowStatus['fldId']) ? 'selected="selected"' : '' ?> value="<?=$rowStatus['fldId']?>"><?=$rowStatus['fldStatus']?></option>
<?							}
?>                 	</select>
                    </li>
                    <li style=" padding:8px; background:#D7D7D7"> 
                    	<label for="sel_observacao">Exibir observa&ccedil;&atilde;o cliente</label>
                        <select style="width:150px;" id="sel_observacao_cliente" name="sel_observacao_cliente" class="sel_observacao_cliente">
                 	   	  	<option <?=($sistemaObservacao == '1') ? 'selected="selected"' : '' ?> value="1">sim</option>
				  			<option <?=($sistemaObservacao == '2') ? 'selected="selected"' : '' ?> value="2">n&atilde;o</option>
			 			</select>
                    </li>
		    		<li style=" padding:8px; background:#D7D7D7"> 
                        <label for="sel_limite_credito">Alertar limite de cr&eacute;dito</label>
                        <select style="width:150px;" id="sel_limite_credito" name="sel_limite_credito" class="sel_limite_credito">
							<option <?=($sistemaLimite_Credito['fldValor'] == '1') ? 'selected="selected"' : '' ?> value="1">sim</option>
							<option <?=($sistemaLimite_Credito['fldValor'] == '2') ? 'selected="selected"' : '' ?> value="2">n&atilde;o</option>
                 		</select>
                    </li>
                    
		    		<li style=" padding:8px; background:#D7D7D7"> 
                        <label for="sel_calcular_comissao">Calcular comiss&atilde;o</label>
                        <select style="width:150px;" id="sel_calcular_comissao" name="sel_calcular_comissao" class="sel_calcular_comissao">
							<option <?=($sistemaComissao == '0') ? 'selected="selected"' : '' ?> value="0">n&atilde;o</option>
							<option <?=($sistemaComissao == '1') ? 'selected="selected"' : '' ?> value="1">sim (por funcionario)</option>
							<option <?=($sistemaComissao == '2') ? 'selected="selected"' : '' ?> value="2">sim (por produto)</option>
                 		</select>
                    </li>
                	<li style=" padding:8px; background:#D7D7D7"> 
                    	<label for="txt_servico_valor">Valor servi&ccedil;o</label>
                        <input type="text" id="txt_servico_valor" name="txt_servico_valor" value="<?=format_number_out($sistemaServicoValor)?>" style=" width:145px;" />
					</li>
                	<li style=" padding:8px; background:#D7D7D7"> 
                    	<label for="sel_fatura">Faturado</label>
						<select style="width:150px" id="sel_fatura" name="sel_fatura" class="sel_fatura">
							<option <?=($sistemaFatura == '0') ? 'selected="selected"' : '' ?> value="0">n&atilde;o</option>
							<option <?=($sistemaFatura == '1') ? 'selected="selected"' : '' ?> value="1">sim</option>
                 		</select>		
					</li>
                </ul>
                <h3 style="display:table; width:950px">Impressão</h3>
                <ul>
                	<li style=" padding:8px; background:#D7D7D7"> 
                    	<label for="sel_exibir_devedor">Exibir devedor</label>
						<select style="width:150px;" id="sel_exibir_devedor" name="sel_exibir_devedor" class="sel_exibir_devedor">
							<option <?=($sistemaExibirDevedor == '0') ? 'selected="selected"' : '' ?> value="0">n&atilde;o</option>
							<option <?=($sistemaExibirDevedor == '1') ? 'selected="selected"' : '' ?> value="1">sim</option>
                 		</select>					
					</li>
                	<li style=" padding:8px; background:#D7D7D7"> 
                    	<label for="sel_exibir_endereco">Exibir endere&ccedil;o</label>
						<select style="width:150px" id="sel_exibir_endereco" name="sel_exibir_endereco" class="sel_exibir_endereco">
							<option <?=($sistemaExibirEndereco == '0') ? 'selected="selected"' : '' ?> value="0">n&atilde;o</option>
							<option <?=($sistemaExibirEndereco == '1') ? 'selected="selected"' : '' ?> value="1">sim</option>
							<option <?=($sistemaExibirEndereco == '2') ? 'selected="selected"' : '' ?> value="2">sim (sem bairro)</option>
                 		</select>					
					</li>
                	<li style=" padding:8px; background:#D7D7D7"> 
                    	<label for="sel_exibir_rgcpf_cliente_impressao">Exibir RG/CPF do Cliente</label>
						<select style="width:150px" id="sel_exibir_rgcpf_cliente_impressao" name="sel_exibir_rgcpf_cliente_impressao" class="sel_exibir_rgcpf_cliente_impressao">
							<option <?=($sistemaExibirRGCPFCliente == '0') ? 'selected="selected"' : '' ?> value="0">n&atilde;o</option>
							<option <?=($sistemaExibirRGCPFCliente == '1') ? 'selected="selected"' : '' ?> value="1">sim</option>
                 		</select>					
					</li>
					<li style=" padding:8px; background:#D7D7D7"> 
                    	<label for="sel_exibir_retirado_por">Exibir retirado por</label>
						<select style="width:150px" id="sel_exibir_retirado_por" name="sel_exibir_retirado_por" class="sel_exibir_retirado_por">
							<option <?=($sistemaExibirRetiradoPor == '0') ? 'selected="selected"' : '' ?> value="0">n&atilde;o</option>
							<option <?=($sistemaExibirRetiradoPor == '1') ? 'selected="selected"' : '' ?> value="1">sim</option>
                 		</select>					
					</li>
					<li style=" padding:8px; background:#D7D7D7"> 
                    	<label for="sel_exibir_produtos">Exibir produtos</label>
						<select style="width:150px" id="sel_exibir_produtos" name="sel_exibir_produtos" class="sel_exibir_produtos">
							<option <?=($sistemaExibirProdutos == '0') ? 'selected="selected"' : '' ?> value="0">n&atilde;o</option>
							<option <?=($sistemaExibirProdutos == '1') ? 'selected="selected"' : '' ?> value="1">sim</option>
                 		</select>					
					</li>
                    
                    <li style=" padding:8px; background:#D7D7D7">
                        <label for="sel_exibir_marca">Exibir marca</label>
                        <select style="width:150px" name="sel_exibir_marca" class="sel_exibir_marca">
                                <option <?=($sistemaExibirMarca == 0) ? "selected='selected'" : '' ?> value="0">n&atilde;o</option>
                                <option <?=($sistemaExibirMarca  == 1)? "selected='selected'" : '' ?> value="1" >sim</option>
                        </select>
                    </li>
                    <li style=" padding:8px; background:#D7D7D7">
                        <label for="sel_exibir_categoria">Exibir categoria</label>
                        <select style="width:150px" name="sel_exibir_categoria" class="sel_exibir_categoria">
                                <option <?=($sistemaExibirCategoria == 0)? "selected='selected'" : '' ?> value="0">n&atilde;o</option>
                                <option <?=($sistemaExibirCategoria == 1)? "selected='selected'" : '' ?> value="1" >sim</option>
                        </select>
                    </li>
                    
                    
					<li style=" padding:8px; background:#D7D7D7"> 
                    	<label for="sel_exibir_terceiros">Exibir terceiros</label>
						<select style="width:150px" id="sel_exibir_terceiros" name="sel_exibir_terceiros" class="sel_exibir_terceiros">
							<option <?=($sistemaExibirTerceiros == '0') ? 'selected="selected"' : '' ?> value="0">n&atilde;o</option>
							<option <?=($sistemaExibirTerceiros == '1') ? 'selected="selected"' : '' ?> value="1">sim</option>
                 		</select>					
					</li>
					<li style=" padding:8px; background:#D7D7D7"> 
                    	<label for="sel_exibir_servicos">Exibir serviços</label>
						<select style="width:150px" id="sel_exibir_servicos" name="sel_exibir_servicos" class="sel_exibir_servicos">
							<option <?=($sistemaExibirServicos == '0') ? 'selected="selected"' : '' ?> value="0">n&atilde;o</option>
							<option <?=($sistemaExibirServicos == '1') ? 'selected="selected"' : '' ?> value="1">sim</option>
                 		</select>					
					</li>
					<li style=" padding:8px; background:#D7D7D7"> 
                    	<label for="sel_exibir_desc_porcent">Exibir Desconto (%)</label>
						<select style="width:150px" id="sel_exibir_retirado_por" name="sel_exibir_desc_porcent" class="sel_exibir_desc_porcent">
							<option <?=($sistemaExibirDescPorc == '0') ? 'selected="selected"' : '' ?> value="0">n&atilde;o</option>
							<option <?=($sistemaExibirDescPorc == '1') ? 'selected="selected"' : '' ?> value="1">sim</option>
                 		</select>					
					</li>
					<li style=" padding:8px; background:#D7D7D7"> 
                    	<label for="sel_exibir_desc_reais">Exibir Desconto (R$)</label>
						<select style="width:150px" id="sel_exibir_desc_reais" name="sel_exibir_desc_reais" class="sel_exibir_desc_reais">
							<option <?=($sistemaExibirDescRs == '0') ? 'selected="selected"' : '' ?> value="0">n&atilde;o</option>
							<option <?=($sistemaExibirDescRs == '1') ? 'selected="selected"' : '' ?> value="1">sim</option>
                 		</select>					
					</li>
					<li style=" padding:8px; background:#D7D7D7"> 
                    	<label for="sel_exibir_rodape">Exibir Rodapé</label>
						<select style="width:150px" id="sel_exibir_desc_reais" name="sel_exibir_rodape" class="sel_exibir_rodape">
							<option <?=($sistemaExibirRodape == '0') ? 'selected="selected"' : '' ?> value="0">n&atilde;o</option>
							<option <?=($sistemaExibirRodape == '1') ? 'selected="selected"' : '' ?> value="1">sim</option>
                 		</select>					
					</li>
					<li style=" padding:8px; background:#D7D7D7"> 
                    	<label for="txt_obs_carne">Observação para o Carnê <span style="font-size: 10px; font-style: italic;">(Max. de 130 caracteres)</span></label>
						<input id="txt_obs_carne" name="txt_obs_carne" maxlength="130" value="<?=$sistemaObsCarne;?>" style="width: 320px;" />
					</li>
					<li style=" padding:8px; background:#D7D7D7"> 
                    	<label for="txt_obs_carne">Exibir dados da Empresa</label>
						<select style="width:150px" id="sel_exibir_dados_empresa" name="sel_exibir_dados_empresa" class="sel_exibir_dados_empresa">
							<option <?=($sistemaExibirDadosEmpresa == '0') ? 'selected="selected"' : '' ?> value="0">n&atilde;o</option>
							<option <?=($sistemaExibirDadosEmpresa == '1') ? 'selected="selected"' : '' ?> value="1">sim</option>
                 		</select>
					</li>
                </ul>

                <h3 style="display: table; width: 950px">Venda R&aacute;pida</h3>
				<ul>
                	<li style=" padding:8px; background:#D7D7D7"> 
                    	<label for="sel_venda_rapida_campo_retorno">Retornar ao campo</label>
                        <select id="sel_venda_rapida_campo_retorno" class="sel_venda_rapida_campo_retorno" name="sel_venda_rapida_campo_retorno" style="width:145px; margin-left:6px">
                   	   		<option <?=($sistemaVendaCampoRetorno == 'txt_produto_codigo') ? 'selected="selected"' : '' ?> value="txt_produto_codigo">C&oacute;digo</option>
	                   	   	<option <?=($sistemaVendaCampoRetorno == 'txt_produto_quantidade') ? 'selected="selected"' : '' ?> value="txt_produto_quantidade">Quantidade</option>
	                   	   	<option <?=($sistemaVendaCampoRetorno == 'txt_funcionario_codigo') ? 'selected="selected"' : '' ?> value="txt_funcionario_codigo">Funcion&aacute;rio</option>
                 		</select>
					</li>
                    <li style="padding:8px; background:#D7D7D7;margin-right:400px"> 
                        <label for="sel_venda_rapida_inserir_automatico">Inserir autom&aacute;tico</label>
                        <select id="sel_venda_rapida_inserir_automatico" name="sel_venda_rapida_inserir_automatico" class="sel_venda_rapida_inserir_automatico" style="width:145px">
                   	   		<option <?=($sistemaVendaInserirAuto == '1') ? 'selected="selected"' : '' ?> value="1">sim</option>
	                   	   	<option <?=($sistemaVendaInserirAuto == '2') ? 'selected="selected"' : '' ?> value="2">n&atilde;o</option>
                 		</select>
		    		</li>
				</ul>
                
                
                <h3 style="display: table; width: 950px">Parcelas</h3>
				<ul>
                	<li style=" padding:8px; background:#D7D7D7"> 
                    	<label for="txt_multa">Multa</label>
                        <input type="text" id="txt_multa" name="txt_multa" value="<?=format_number_out($sistemaMulta)?>" style="width:80px" />
                        <select id="sel_multa_tipo" name="sel_multa_tipo" class="sel_multa_tipo" style="width:60px; margin-left:6px">
                   	   		<option <?=($sistemaMultaTipo == '1') ? 'selected="selected"' : '' ?> value="1">R$</option>
	                   	   	<option <?=($sistemaMultaTipo == '2') ? 'selected="selected"' : '' ?> value="2">%</option>
                 		</select>
					</li>
                	<li style=" padding:8px; background:#D7D7D7"> 
                    	<label for="txt_juros">Juros (% a/m)</label>
                        <input type="text" id="txt_juros" name="txt_juros" value="<?=format_number_out($sistemaJuros)?>" style=" width:145px" />
					</li>
                    <li style="padding:8px; background:#D7D7D7;margin-right:400px"> 
                        <label for="sel_juros_tipo">Tipo de juros</label>
                        <select id="sel_juros_tipo" name="sel_juros_tipo" class="sel_juros_tipo" style="width:150px">
                   	   		<option <?=($sistemaJurosTipo == '1') ? 'selected="selected"' : '' ?> value="1">simples</option>
	                   	   	<option <?=($sistemaJurosTipo == '2') ? 'selected="selected"' : '' ?> value="2">composto</option>
                 		</select>
		    		</li>
				</ul>
                
                
                <h3 style="display: table; width: 950px">Recibo</h3>
				<ul>
                	<li style=" padding:8px; background:#D7D7D7"> 
                    	<label for="sel_recibo_exibir_multa">Exibir multa</label>
                        <select id="sel_recibo_exibir_multa" name="sel_recibo_exibir_multa" class="sel_recibo_exibir_multa" style="width:145px; margin-left:6px">
                   	   		<option <?=($sistema_recibo_exibir_multa == '1') ? 'selected="selected"' : '' ?> value="1">sim</option>
	                   	   	<option <?=($sistema_recibo_exibir_multa == '0') ? 'selected="selected"' : '' ?> value="0">n&atilde;o</option>
                 		</select>
					</li>
                	<li style=" padding:8px; background:#D7D7D7"> 
                    	<label for="sel_recibo_exibir_juros">Exibir juros</label>
                        <select id="sel_recibo_exibir_juros" name="sel_recibo_exibir_juros" class="sel_recibo_exibir_juros" style="width:145px; margin-left:6px">
                   	   		<option <?=($sistema_recibo_exibir_juros == '1') ? 'selected="selected"' : '' ?> value="1">sim</option>
	                   	   	<option <?=($sistema_recibo_exibir_juros == '0') ? 'selected="selected"' : '' ?> value="0">n&atilde;o</option>
                 		</select>
					</li>
                	<li style=" padding:8px; background:#D7D7D7"> 
                    	<label for="sel_recibo_exibir_devedor">Exibir devedor</label>
                        <select id="sel_recibo_exibir_devedor" name="sel_recibo_exibir_devedor" class="sel_recibo_exibir_devedor" style="width:145px; margin-left:6px">
                   	   		<option <?=($sistema_recibo_exibir_devedor == '1') ? 'selected="selected"' : '' ?> value="1">sim</option>
	                   	   	<option <?=($sistema_recibo_exibir_devedor == '0') ? 'selected="selected"' : '' ?> value="0">n&atilde;o</option>
                 		</select>
					</li>
                	<li style=" padding:8px; background:#D7D7D7"> 
                    	<label for="sel_recibo_exibir_assinatura">Exibir assinatura</label>
                        <select id="sel_recibo_exibir_assinatura" name="sel_recibo_exibir_assinatura" class="sel_recibo_exibir_assinatura" style="width:145px; margin-left:6px">
                   	   		<option <?=($sistema_recibo_exibir_assinatura == '1') ? 'selected="selected"' : '' ?> value="1">sim</option>
	                   	   	<option <?=($sistema_recibo_exibir_assinatura == '0') ? 'selected="selected"' : '' ?> value="0">n&atilde;o</option>
                 		</select>
					</li>
				</ul>
                
                <h3 style="display: table; width: 950px">Produtos</h3>
				<ul>
		    		<li style=" padding:8px; background:#D7D7D7"> 
                        <label for="sel_alerta_estoque">Alertar estoque negativo</label>
                        <select style="width:150px;" id="sel_alerta_estoque" name="sel_alerta_estoque" class="sel_alerta_estoque">
                   	   		<option <?=($sistemaAlertaNegativo == '1') ? 'selected="selected"' : '' ?> value="1">sim</option>
	                   	   	<option <?=($sistemaAlertaNegativo == '2') ? 'selected="selected"' : '' ?> value="2">n&atilde;o</option>
                 		</select>
                    </li>
		    		<li style=" padding:8px; background:#D7D7D7"> 
                        <label for="sel_exibir_item_repetido_alerta">Alertar item repetido</label>
                        <select style="width:150px;" id="sel_exibir_item_repetido_alerta" name="sel_exibir_item_repetido_alerta" class="sel_exibir_item_repetido_alerta">
							<option <?=($sistemaItem_Duplicado['fldValor'] == '0') ? 'selected="selected"' : '' ?> value="0">n&atilde;o</option>
							<option <?=($sistemaItem_Duplicado['fldValor'] == '1') ? 'selected="selected"' : '' ?> value="1">sim</option>
                 		</select>
                    </li>
				</ul>
                
                <h3 style="display:table; width: 950px">Pagamento/Conta</h3>
                <ul>
                	<li style="width:420px; padding:8px; background:#D7D7D7"> 
<?						$sistemaAcaoAV = fnc_sistema('pedido_parcela_acao_AV');
?>	                    <label for="sel_parcela_acao_AV">Parcela &agrave; vista</label>
                        <select style="width:200px; float:left" id="sel_parcela_acao_AV" name="sel_parcela_acao_AV" class="sel_parcela_acao_AV" >
							<option value="0">selecionar</option>
                           <option <?=($sistemaAcaoAV == '1') ? 'selected="selected"' : '' ?> value="1">gravar como parcela</option>
                           <option <?=($sistemaAcaoAV == '2') ? 'selected="selected"' : '' ?> value="2">creditar em caixa</option>
                        </select>
                    </li>
                        
<?					$rsPagamentoTipo = mysql_query("SELECT * FROM tblpagamento_tipo WHERE fldExcluido = 0 ORDER BY fldTipo");
					while($rowPagamentoTipo = mysql_fetch_array($rsPagamentoTipo)){
						$sigla = $rowPagamentoTipo['fldSigla'].$rowPagamentoTipo['fldSiglaSub'];
						$sistemaAcao = fnc_sistema('pedido_parcela_acao_'.$sigla);
?>
                        <li style=" padding:8px; background: #D7D7D7"> 
                            <label for="sel_parcela_acao_<?=$sigla?>"><?=$rowPagamentoTipo['fldTipo']?></label>
                            <select style="width:200px; float:left" id="sel_parcela_acao_<?=$sigla?>" name="sel_parcela_acao_<?=$sigla?>" >
                               <option value="0">selecionar</option>
                               <option <?=($sistemaAcao == '1') ? 'selected="selected"' : '' ?> value="1">gravar como parcela</option>
                               <option <?=($sistemaAcao == '2') ? 'selected="selected"' : '' ?> value="2">creditar em caixa</option>
                            </select>
                            
                            <select style="width:200px; float:left; margin-left: 20px" id="sel_parcela_conta_<?=$sigla?>" name="sel_parcela_conta_<?=$sigla?>" >
                               	<option value="0">selecionar</option>
<?								$rsConta = mysql_query("SELECT * FROM tblfinanceiro_conta");
								while($rowConta = mysql_fetch_array($rsConta)){
									$sistemaAcaoConta = fnc_sistema('pedido_parcela_conta_'.$sigla);
?>                            	   	<option <?=($sistemaAcaoConta == $rowConta['fldId']) ? 'selected="selected"' : '' ?> value="<?=$rowConta['fldId']?>"><?=$rowConta['fldNome']?></option>
<?								}
?>                          </select>
                        </li>
<?					}
?>        		</ul>
            	<input style="float:right; margin-right: 20px" type="submit" class="btn_enviar" name="btn_enviar" value="Gravar" >
			</form>
		</div>
<?
		require('configuracao_vendas_marcador.php');
?>        
	</div>