<?	

	##############################################################################################################
	############################################## REQUIRES ######################################################
	require("inc/relatorio.php");
	require("inc/con_db.php");
	require("inc/fnc_general.php");
	
	$pdf = new PDF('P', 'mm', 'A4');
	//pode ser P (portrait) ou L (landscape)
	
	$pdf->AliasNbPages();
	//isso faz com que funcione o total de paginas
	
	$pdf->SetMargins(11, 5);
	//defino as margens do documento...
	
	$pdf->AddPage();
	//comando para add a pagina
	
	######################
	## periodo ###########	
	$d1 = $_GET['d1'];
	$d2 = $_GET['d2'];
	######################
	
	//cabeçalho
	session_start();
	$dados_empresa 	= mysql_fetch_assoc(mysql_query("SELECT * FROM tblempresa_info"));
	$usuario 		= mysql_fetch_assoc(mysql_query("SELECT * FROM tblusuario WHERE fldId = '".$_SESSION['usuario_id']."'"));
	$usuario 		= $usuario['fldUsuario'];
	
	$pdf->SetFont('Arial', '', 8);
	$pdf->Cell(50);
	$pdf->Cell(60, 5, "nome da empresa: {$dados_empresa['fldNome_Fantasia']} (".format_cnpj_out($dados_empresa['fldCPF_CNPJ']).")", 0, 0, 'R');
	$pdf->Cell(30, 5, "usuario: $usuario", 0, 0, 'R');
	$pdf->Cell(50, 5, "gerado em: ".date('d/m/Y h:i:s'), 0, 0, 'R');
	$pdf->Ln(6);
	
	######################### TITULO DO RELATORIO #####################################################
	$pdf->SetTextColor(255, 255, 255);
	$pdf->SetFillColor(0, 0, 0);
	$pdf->SetFont('Arial', 'B', 14);
	$pdf->Cell(130, 8, "Fechamento de Caixa", 1, 0, 'L', true);
	$pdf->SetFont('Arial', '', 8);
	$pdf->Cell(60, 8, "Período selecionado: ".format_date_out($d1)." - ".format_date_out($d2), 1, 0, 'L', true);
	$pdf->SetTextColor(0, 0, 0); //ja reseto a cor da fonte para nao ter problema
	$pdf->Ln();
	###################################################################################################
	
	if(!$_GET['resumo'] && $_GET['resumo'] != 'ok'){
	
		##############################################################################################################
		################################################# VENDAS #####################################################
		$pdf->SetFont('Arial', '', 12);
		$pdf->SetFillColor(220, 220, 220);
		$pdf->SetTextColor(108, 108, 108);
		$pdf->Cell(190, 8, "Vendas", 1, 0, 'L', true);
		$pdf->Ln();
		
		################## SQL #######################################################################################
		# fields: total bruto de produtos, total dos descontos nos produtos, total de servicos, total de terceiros, ##
		# total bruto das vendas, total dos descontos das vendas, total liquido das vendas ###########################
		##############################################################################################################
		$venda_produto_bruto 		= 0;
		$venda_produto_desconto		= 0;
		$venda_servico				= 0;
		$venda_terceiro				= 0;
		$venda_bruto				= 0;
		$venda_descontos			= 0;
		$venda_liquido				= 0;
		
		$venda_sql = mysql_query("SELECT
		tblpedido.fldId,
		SUM(tblpedido_item.fldValor * tblpedido_item.fldQuantidade) as fldProduto_Bruto,
		SUM(tblpedido_item.fldDesconto * tblpedido_item.fldQuantidade) as fldProduto_Desconto,
		SUM(tblpedido_funcionario_servico.fldComissao) as fldPedido_Servico,
		SUM(tblpedido.fldValor_Terceiros) as fldPedido_Terceiros,
		tblpedido.fldDesconto as fldPedido_Desconto_Porcentagem,
		tblpedido.fldDescontoReais as fldPedido_Desconto_Reais
		
		FROM tblpedido
		
		LEFT JOIN tblpedido_item ON tblpedido.fldId = tblpedido_item.fldPedido_Id
		LEFT JOIN tblpedido_funcionario_servico ON tblpedido.fldId = tblpedido_funcionario_servico.fldPedido_Id AND tblpedido_funcionario_servico.fldFuncao_Tipo = '2'
		
		WHERE tblpedido.fldPedidoData BETWEEN '$d1' AND '$d2'
		AND 	tblpedido.fldPedido_Destino_Nfe_Id is null
		AND		tblpedido.fldStatus NOT IN ('1, 5')
		
		GROUP BY tblpedido.fldId") or die(mysql_error());
	
		while($venda_row = mysql_fetch_assoc($venda_sql)){
			$atual_venda_produto_bruto 		= $venda_row['fldProduto_Bruto'];
			$atual_venda_produto_desconto 	= $venda_row['fldProduto_Desconto'];
			$atual_venda_servico 			= $venda_row['fldPedido_Servico'];
			$atual_venda_terceiro 			= $venda_row['fldPedido_Terceiros'];
			$atual_venda_bruto 				= ($atual_venda_produto_bruto - $atual_venda_produto_desconto) + $atual_venda_servico + $atual_venda_terceiro;
			$atual_venda_descontos 			= ($atual_venda_bruto * ($venda_row['fldPedido_Desconto_Porcentagem'] / 100)) + $venda_row['fldPedido_Desconto_Reais'];
			$atual_venda_liquido 			= $atual_venda_bruto - $atual_venda_descontos;
			###############################################################################		
			$venda_produto_bruto 	+= $atual_venda_produto_bruto;
			$venda_produto_desconto += $atual_venda_produto_desconto;
			$venda_servico 			+= $atual_venda_servico;
			$venda_terceiro 		+= $atual_venda_terceiro;
			$venda_bruto 			+= $atual_venda_bruto;
			$venda_descontos 		+= $atual_venda_descontos;
			$venda_liquido 			+= $atual_venda_liquido;
		}
	
		##reset formatacao
		$pdf->SetFont('Arial', '', 12);
		$pdf->SetFillColor(255, 255, 255);
		$pdf->SetTextColor(0, 0, 0);
		
		##total bruto dos produtos!
		$pdf->Cell(160, 6, "Total bruto dos produtos", 1, 0, 'L', true);
		$pdf->Cell(30, 6, format_number_out($venda_produto_bruto), 1, 0, 'R', true);
		$pdf->Ln();
		
		##total desconto dos produtos!
		$pdf->Cell(160, 6, "Total de desconto em produtos", 1, 0, 'L', true);
		$pdf->Cell(30, 6, format_number_out($venda_produto_desconto), 1, 0, 'R', true);
		$pdf->Ln();
		
		##total de serviços!
		$pdf->Cell(160, 6, "Total de serviços", 1, 0, 'L', true);
		$pdf->Cell(30, 6, format_number_out($venda_servico), 1, 0, 'R', true);
		$pdf->Ln();
		
		##total de terceiros!
		$pdf->Cell(160, 6, "Total de terceiros", 1, 0, 'L', true);
		$pdf->Cell(30, 6, format_number_out($venda_terceiro), 1, 0, 'R', true);
		$pdf->Ln();
		
		##total bruto das vendas!
		$pdf->Cell(160, 6, "Total bruto de vendas", 1, 0, 'L', true);
		$pdf->Cell(30, 6, format_number_out($venda_bruto), 1, 0, 'R', true);
		$pdf->Ln();
		
		##total de desconto das vendas!
		$pdf->Cell(160, 6, "Total de desconto em vendas", 1, 0, 'L', true);
		$pdf->Cell(30, 6, format_number_out($venda_descontos), 1, 0, 'R', true);
		$pdf->Ln();
		
		##total liquido das vendas!
		$pdf->Cell(160, 6, "Total liquido de vendas", 1, 0, 'L', true);
		$pdf->Cell(30, 6, format_number_out($venda_liquido), 1, 0, 'R', true);
		$pdf->Ln();
		
		##total liquido das vendas a vista!
		$pdf->Cell(160, 6, "Total a vista", 1, 0, 'L', true);
		$pdf->Cell(30, 6, "aaa", 1, 0, 'R', true);
		$pdf->Ln();
		
		##total liquido das vendas a prazo!
		$pdf->Cell(160, 6, "Total a prazo", 1, 0, 'L', true);
		$pdf->Cell(30, 6, "xas", 1, 0, 'R', true);
		$pdf->Ln();
		
		##############################################################################################################
		################################################# COMPRAS ####################################################
		$pdf->SetFont('Arial', '', 12);
		$pdf->SetFillColor(220, 220, 220);
		$pdf->SetTextColor(108, 108, 108);
		$pdf->Cell(190, 8, "Compras", 1, 0, 'L', true);
		$pdf->Ln();
		
		################## SQL #######################################################################################
		# fields: total bruto de produtos, total dos descontos nos produtos, total de transporte #####################
		# total bruto das compras, total dos descontos das compras, total liquido das compras ########################
		##############################################################################################################
		$compra_produto_bruto 		= 0;
		$compra_produto_desconto 	= 0;
		$compra_transporte 			= 0;
		$compra_bruto 				= 0;
		$compra_desconto 			= 0;
		$compra_liquido 			= 0;
		
		$compra_sql = mysql_query("SELECT
	
		tblcompra.fldId,
		SUM(tblcompra_item.fldValor * tblcompra_item.fldQuantidade) as fldProduto_Bruto,
		SUM((tblcompra_item.fldValor * tblcompra_item.fldQuantidade) * (tblcompra_item.fldDesconto / 100)) as fldProduto_Desconto,
		tblcompra.fldTransporte,
		tblcompra.fldDesconto as fldCompra_Desconto_Porcentagem,
		tblcompra.fldDescontoReais as fldCompra_Desconto_Reais
		
		FROM tblcompra
		
		LEFT JOIN tblcompra_item ON tblcompra.fldId = tblcompra_item.fldCompra_Id
		
		WHERE tblcompra.fldCompraData BETWEEN '$d1' AND '$d2'
		AND tblcompra.fldExcluido != '1'
		
		GROUP BY tblcompra.fldId") or die(mysql_error());
	
		while($compra_row = mysql_fetch_assoc($compra_sql)){
			$atual_compra_produto_bruto 	= $compra_row['fldProduto_Bruto'];
			$atual_compra_produto_desconto 	= $compra_row['fldProduto_Desconto'];
			$atual_compra_transporte		= $compra_row['fldTransporte'];
			$atual_compra_bruto 			= ($atual_compra_produto_bruto - $atual_compra_produto_desconto) + $atual_compra_transporte;
			$atual_compra_descontos			= ($atual_compra_bruto * ($compra_row['fldCompra_Desconto_Porcentagem'] / 100)) + $compra_row['fldCompra_Desconto_Reais'];
			$atual_compra_liquido 			= $atual_compra_bruto - $atual_compra_descontos;
			###############################################################################		
			$compra_produto_bruto 		+= $atual_compra_produto_bruto;
			$compra_produto_desconto 	+= $atual_compra_produto_desconto;
			$compra_transporte 			+= $atual_compra_transporte;
			$compra_bruto 				+= $atual_compra_bruto;
			$compra_desconto 			+= $atual_compra_descontos;
			$compra_liquido 			+= $atual_compra_liquido;
		}
		
		##reset formatacao
		$pdf->SetFont('Arial', '', 12);
		$pdf->SetFillColor(255, 255, 255);
		$pdf->SetTextColor(0, 0, 0);
		
		##total bruto dos produtos!
		$pdf->Cell(160, 6, "Total bruto dos produtos", 1, 0, 'L', true);
		$pdf->Cell(30, 6, format_number_out($compra_produto_bruto), 1, 0, 'R', true);
		$pdf->Ln();
		
		##total desconto dos produtos!
		$pdf->Cell(160, 6, "Total de deconto em produtos", 1, 0, 'L', true);
		$pdf->Cell(30, 6, format_number_out($compra_produto_desconto), 1, 0, 'R', true);
		$pdf->Ln();
		
		##total de transporte!
		$pdf->Cell(160, 6, "Total de transporte", 1, 0, 'L', true);
		$pdf->Cell(30, 6, format_number_out($compra_transporte), 1, 0, 'R', true);
		$pdf->Ln();
		
		##total de compras
		$pdf->Cell(160, 6, "Total bruto de compras", 1, 0, 'L', true);
		$pdf->Cell(30, 6, format_number_out($compra_bruto), 1, 0, 'R', true);
		$pdf->Ln();
		
		##total de desconto de compras
		$pdf->Cell(160, 6, "Total de desconto em compras", 1, 0, 'L', true);
		$pdf->Cell(30, 6, format_number_out($compra_desconto), 1, 0, 'R', true);
		$pdf->Ln();
		
		##total liquido de compras
		$pdf->Cell(160, 6, "Total liquido de compras", 1, 0, 'L', true);
		$pdf->Cell(30, 6, format_number_out($compra_liquido), 1, 0, 'R', true);
		$pdf->Ln();
		
		##total liquido das compras a vista!
		$pdf->Cell(160, 6, "Total a vista", 1, 0, 'L', true);
		$pdf->Cell(30, 6, "aaa", 1, 0, 'R', true);
		$pdf->Ln();
		
		##total liquido das compras a prazo!
		$pdf->Cell(160, 6, "Total a prazo", 1, 0, 'L', true);
		$pdf->Cell(30, 6, "xas", 1, 0, 'R', true);
		$pdf->Ln();
		
		##############################################################################################################
		########################################## CONTAS A RECEBER ##################################################
		$pdf->SetFont('Arial', '', 12);
		$pdf->SetFillColor(220, 220, 220);
		$pdf->SetTextColor(108, 108, 108);
		$pdf->Cell(190, 8, "Contas a receber", 1, 0, 'L', true);
		$pdf->Ln();
		
		################## SQL #######################################################################################
		# fields: total bruto contas a receber, total desconto, total de juros, total de multa, total liquido #######
		##############################################################################################################
		$creceber_bruto_previsto	= 0;
		$creceber_bruto				= 0;
		$creceber_desconto		 	= 0;
		$creceber_juros	 			= 0;
		$creceber_multa				= 0;
		$creceber_liquido 			= 0;
		
		$creceber_sql = mysql_query("SELECT
	
		tblpedido_parcela.fldPedido_Id,
		SUM(tblpedido_parcela.fldValor) as fldParcela_Bruto_Previsto,
		SUM(tblpedido_parcela_baixa.fldValor) as fldParcela_Bruto,
		SUM(tblpedido_parcela_baixa.fldDesconto) as fldParcela_Desconto,
		SUM(tblpedido_parcela_baixa.fldJuros) as fldParcela_Juros,
		SUM(tblpedido_parcela_baixa.fldMulta) as fldParcela_Multa
		
		FROM tblpedido_parcela
		
		LEFT JOIN tblpedido ON tblpedido_parcela.fldPedido_Id = tblpedido.fldId
		LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id AND tblpedido_parcela_baixa.fldDataRecebido BETWEEN '$d1' AND '$d2'
		
		WHERE tblpedido_parcela.fldVencimento BETWEEN '$d1' AND '$d2'
		AND tblpedido_parcela.fldExcluido != '1'
		AND	tblpedido.fldPedido_Destino_Nfe_Id is null
		
		GROUP BY tblpedido_parcela.fldPedido_Id") or die(mysql_error());
		
		while($creceber_row = mysql_fetch_assoc($creceber_sql)){
			$atual_creceber_bruto_previsto	= $creceber_row['fldParcela_Bruto_Previsto'];
			$atual_creceber_desconto		= $creceber_row['fldParcela_Desconto'];
			$atual_creceber_juros	 		= $creceber_row['fldParcela_Juros'];
			$atual_creceber_multa			= $creceber_row['fldParcela_Multa'];
			$atual_creceber_bruto			= $creceber_row['fldParcela_Bruto'] + $atual_creceber_desconto;
			$atual_creceber_liquido 		= $atual_creceber_bruto - $atual_creceber_desconto + $atual_creceber_juros + $atual_creceber_multa;
			
			##########################################
			$creceber_bruto_previsto	+= $atual_creceber_bruto_previsto;
			$creceber_bruto	 			+= $atual_creceber_bruto;
			$creceber_desconto			+= $atual_creceber_desconto;
			$creceber_juros	 			+= $atual_creceber_juros;
			$creceber_multa				+= $atual_creceber_multa;
			$creceber_liquido 			+= $atual_creceber_liquido;
		}
	
		##reset formatacao
		$pdf->SetFont('Arial', '', 12);
		$pdf->SetFillColor(255, 255, 255);
		$pdf->SetTextColor(0, 0, 0);
		
		##total de contas a receber
		$pdf->Cell(160, 6, "Total previsto de contas a receber", 1, 0, 'L', true);
		$pdf->Cell(30, 6, format_number_out($creceber_bruto_previsto), 1, 0, 'R', true);
		$pdf->Ln();
		
		##total de contas a receber
		$pdf->Cell(160, 6, "Total bruto de contas recebidas", 1, 0, 'L', true);
		$pdf->Cell(30, 6, format_number_out($creceber_bruto), 1, 0, 'R', true);
		$pdf->Ln();
		
		##total desconto recebido
		$pdf->Cell(160, 6, "Total de descontos no recebimento", 1, 0, 'L', true);
		$pdf->Cell(30, 6, format_number_out($creceber_desconto), 1, 0, 'R', true);
		$pdf->Ln();
		
		##total juros recebido
		$pdf->Cell(160, 6, "Total de juros no recebimento", 1, 0, 'L', true);
		$pdf->Cell(30, 6, format_number_out($creceber_juros), 1, 0, 'R', true);
		$pdf->Ln();
		
		##total multa recebido!
		$pdf->Cell(160, 6, "Total de multa no recebimento", 1, 0, 'L', true);
		$pdf->Cell(30, 6, format_number_out($creceber_multa), 1, 0, 'R', true);
		$pdf->Ln();
		
		##total liquido recebido!
		$pdf->Cell(160, 6, "Total liquido recebido", 1, 0, 'L', true);
		$pdf->Cell(30, 6, format_number_out($creceber_liquido), 1, 0, 'R', true);
		$pdf->Ln();
		
		##total em aberto!
		$pdf->Cell(160, 6, "Total em aberto", 1, 0, 'L', true);
		$pdf->Cell(30, 6, format_number_out($creceber_bruto_previsto - $creceber_liquido), 1, 0, 'R', true);
		$pdf->Ln();
		
		##############################################################################################################
		######################################### CONTAS A PAGAR #####################################################
		$pdf->SetFont('Arial', '', 12);
		$pdf->SetFillColor(220, 220, 220);
		$pdf->SetTextColor(108, 108, 108);
		$pdf->Cell(190, 8, "Contas a pagar", 1, 0, 'L', true);
		$pdf->Ln();
		
		################## SQL #######################################################################################
		# fields: total bruto de contas, total dos descontos, total de juros, total de multa #########################
		# total de acrescimos, total liquido #########################################################################
		##############################################################################################################
		$cpagar_bruto_previsto		= 0;
		$cpagar_bruto 				= 0;
		$cpagar_desconto 			= 0;
		$cpagar_juros 				= 0;
		$cpagar_multa 				= 0;
		$cpagar_acrescimo 			= 0;
		$cpagar_liquido 			= 0;
		
		$cpagar_sql = mysql_query("SELECT
		
		tblcompra_parcela.fldCompra_Id,
		SUM(tblcompra_parcela.fldValor) as fldParcela_Bruto_Previsto,
		SUM(tblcompra_parcela_baixa.fldValor) as fldParcela_Bruto,
		SUM(tblcompra_parcela_baixa.fldDesconto) as fldParcela_Desconto,
		SUM(tblcompra_parcela_baixa.fldJuros) as fldParcela_Juros,
		SUM(tblcompra_parcela_baixa.fldMulta) as fldParcela_Multa,
		SUM(tblcompra_parcela_baixa.fldAcrescimo) as fldParcela_Acrescimo
		
		FROM tblcompra_parcela
		
		LEFT JOIN tblcompra ON tblcompra_parcela.fldCompra_Id = tblcompra.fldId
		LEFT JOIN tblcompra_parcela_baixa ON tblcompra_parcela.fldId = tblcompra_parcela_baixa.fldParcela_Id AND tblcompra_parcela_baixa.fldDataPago BETWEEN '$d1' AND '$d2'
		
		WHERE tblcompra_parcela.fldVencimento BETWEEN '$d1' AND '$d2'
		AND tblcompra_parcela.fldExcluido != '1'
		
		GROUP BY tblcompra_parcela.fldCompra_Id") or die(mysql_error());
		
		while($cpagar_row = mysql_fetch_assoc($cpagar_sql)){
			$atual_cpagar_bruto_previsto	= $cpagar_row['fldParcela_Bruto_Previsto'];
			$atual_cpagar_desconto 			= $cpagar_row['fldParcela_Desconto'];
			$atual_cpagar_bruto 			= $cpagar_row['fldParcela_Bruto'] + $atual_cpagar_desconto;
			$atual_cpagar_juros 			= $cpagar_row['fldParcela_Juros'];
			$atual_cpagar_multa 			= $cpagar_row['fldParcela_Multa'];
			$atual_cpagar_acrescimo 		= $cpagar_row['fldParcela_Acrescimo'];
			$atual_cpagar_liquido 			= $atual_cpagar_bruto - $atual_cpagar_desconto + $atual_cpagar_juros + $atual_cpagar_multa + $atual_cpagar_acrescimo;
			########################################
			$cpagar_bruto_previsto 	+= $atual_cpagar_bruto_previsto;
			$cpagar_bruto 			+= $atual_cpagar_bruto;
			$cpagar_desconto 		+= $atual_cpagar_desconto;
			$cpagar_juros 			+= $atual_cpagar_juros;
			$cpagar_multa 			+= $atual_cpagar_multa;
			$cpagar_acrescimo 		+= $atual_cpagar_acrescimo;
			$cpagar_liquido 		+= $atual_cpagar_liquido;
		}
	
		##reset formatacao
		$pdf->SetFont('Arial', '', 12);
		$pdf->SetFillColor(255, 255, 255);
		$pdf->SetTextColor(0, 0, 0);
	
		##total de contas a pagar
		$pdf->Cell(160, 6, "Total previsto de contas a pagar", 1, 0, 'L', true);
		$pdf->Cell(30, 6, format_number_out($cpagar_bruto_previsto), 1, 0, 'R', true);
		$pdf->Ln();
		
		##total de contas a pagar
		$pdf->Cell(160, 6, "Total bruto de contas pagas", 1, 0, 'L', true);
		$pdf->Cell(30, 6, format_number_out($cpagar_bruto), 1, 0, 'R', true);
		$pdf->Ln();
	
		##total desconto pagar
		$pdf->Cell(160, 6, "Total de descontos no pagamento", 1, 0, 'L', true);
		$pdf->Cell(30, 6, format_number_out($cpagar_desconto), 1, 0, 'R', true);
		$pdf->Ln();
	
		##total juros pagar
		$pdf->Cell(160, 6, "Total de juros no pagamento", 1, 0, 'L', true);
		$pdf->Cell(30, 6, format_number_out($cpagar_juros), 1, 0, 'R', true);
		$pdf->Ln();
	
		##total multo dos pagar!
		$pdf->Cell(160, 6, "Total de multa no pagamento", 1, 0, 'L', true);
		$pdf->Cell(30, 6, format_number_out($cpagar_multa), 1, 0, 'R', true);
		$pdf->Ln();
		
		##total multo dos pagar!
		$pdf->Cell(160, 6, "Total de acrescimos no pagamento", 1, 0, 'L', true);
		$pdf->Cell(30, 6, format_number_out($cpagar_acrescimo), 1, 0, 'R', true);
		$pdf->Ln();
	
		##total bruto dos pagar!
		$pdf->Cell(160, 6, "Total liquido pago", 1, 0, 'L', true);
		$pdf->Cell(30, 6, format_number_out($cpagar_liquido), 1, 0, 'R', true);
		$pdf->Ln();
		
		##total em aberto!
		$pdf->Cell(160, 6, "Total em aberto", 1, 0, 'L', true);
		$pdf->Cell(30, 6, format_number_out($cpagar_bruto_previsto - $cpagar_liquido), 1, 0, 'R', true);
		$pdf->Ln();
		
		/*##############################################################################################################
		######################################### COMISSOES ##########################################################
		$pdf->SetFont('Arial', '', 12);
		$pdf->SetFillColor(220, 220, 220);
		$pdf->SetTextColor(108, 108, 108);
		$pdf->Cell(190, 8, "Comissões", 1, 0, 'L', true);
		$pdf->Ln();
		
		################## SQL #######################################################################################
		# fields: nome do funcionario, total de comissao, total pago #################################################
		##############################################################################################################
		$comissao_funcionario		= 'okesss';
		$comissao_total				= 50;
		$comissao_pago	 			= 10;
		
		## entradas
		$pdf->SetFont('Arial', '', 9);
		$pdf->SetFillColor(240, 240, 240);
		$pdf->SetTextColor(108, 108, 108);
		$pdf->Cell(190, 5, "$comissao_funcionario", 1, 0, 'R', true);
		$pdf->Ln();
	
		##reset formatacao
		$pdf->SetFont('Arial', '', 12);
		$pdf->SetFillColor(255, 255, 255);
		$pdf->SetTextColor(0, 0, 0);
	
		##total de contas a pagar
		$pdf->Cell(170, 6, "Total a receber", 1, 0, 'L', true);
		$pdf->Cell(20, 6, "$comissao_total", 1, 0, 'R', true);
		$pdf->Ln();
		
		##total de contas a pagar
		$pdf->Cell(170, 6, "Total pago", 1, 0, 'L', true);
		$pdf->Cell(20, 6, "$comissao_pago", 1, 0, 'R', true);
		$pdf->Ln();*/

	}
	
	##############################################################################################################
	##################################### MOVIMENTACAO DE CX #####################################################
	$pdf->SetFont('Arial', '', 12);
	$pdf->SetFillColor(220, 220, 220);
	$pdf->SetTextColor(108, 108, 108);
	$pdf->Cell(190, 8, "Movimento caixa", 1, 0, 'L', true);
	$pdf->Ln();
	
	################## SQL #######################################################################################
	# fields: nome do tipo, credito, e debito ####################################################################
	##############################################################################################################
	$mov_caixa_sql = mysql_query("SELECT

		tblpagamento_tipo.fldId as fldPagamento_Id,
		tblpagamento_tipo.fldTipo as fldTipo,
		SUM(tblfinanceiro_conta_fluxo.fldCredito) as fldCredito,
		SUM(tblfinanceiro_conta_fluxo.fldDebito) as fldDebito
		
		FROM tblpagamento_tipo
		
		RIGHT JOIN tblfinanceiro_conta_fluxo ON tblpagamento_tipo.fldId = tblfinanceiro_conta_fluxo.fldPagamento_Tipo_Id
		
		WHERE 	tblpagamento_tipo.fldExcluido != '1'
		AND 	tblfinanceiro_conta_fluxo.fldData BETWEEN '$d1' AND '$d2'
		
		GROUP BY tblpagamento_tipo.fldId");
	
	$arr_mov_caixa = array();
	while($mov_caixa_row = mysql_fetch_assoc($mov_caixa_sql)){
		$arr = array("tipo" => $mov_caixa_row['fldTipo'], "credito" => $mov_caixa_row['fldCredito'], "debito" => $mov_caixa_row['fldDebito']);
		array_push($arr_mov_caixa, $arr);	}
		
	## entradas
	$pdf->SetFont('Arial', '', 9);
	$pdf->SetFillColor(240, 240, 240);
	$pdf->SetTextColor(108, 108, 108);
	$pdf->Cell(190, 5, "entradas", 1, 0, 'R', true);
	$pdf->Ln();
	
	$total_credito = 0;
	foreach($arr_mov_caixa as $mov_caixa){

		$tipo 		= $mov_caixa['tipo'];
		$credito 	= $mov_caixa['credito'];
		
		if($credito > 0){
			##reset formatacao
			$pdf->SetFont('Arial', '', 12);
			$pdf->SetFillColor(255, 255, 255);
			$pdf->SetTextColor(0, 0, 0);
			##total bruto dos pagar!
			$pdf->Cell(160, 6, "Total recebido em $tipo", 1, 0, 'L', true);
			$pdf->Cell(30, 6, format_number_out($credito), 1, 0, 'R', true);
			$pdf->Ln();
			$total_credito += $credito;
		}

	}
	
	## saidas
	$pdf->SetFont('Arial', '', 9);
	$pdf->SetFillColor(240, 240, 240);
	$pdf->SetTextColor(108, 108, 108);
	$pdf->Cell(190, 5, "saidas", 1, 0, 'R', true);
	$pdf->Ln();
	
	$total_debito = 0;
	foreach($arr_mov_caixa as $mov_caixa){
	
		$tipo 		= $mov_caixa['tipo'];
		$debito 	= $mov_caixa['debito'];
		
		if($debito > 0){
			##reset formatacao
			$pdf->SetFont('Arial', '', 12);
			$pdf->SetFillColor(255, 255, 255);
			$pdf->SetTextColor(0, 0, 0);
			##total bruto dos pagar!
			$pdf->Cell(160, 6, "Total pago em $tipo", 1, 0, 'L', true);
			$pdf->Cell(30, 6, format_number_out($debito), 1, 0, 'R', true);
			$pdf->Ln();
			$total_debito += $debito;
		}

	}
	
	##############################################################################################################
	##################################### TOTAL ##################################################################
	$pdf->SetFont('Arial', '', 12);
	$pdf->SetFillColor(220, 220, 220);
	$pdf->SetTextColor(108, 108, 108);
	$pdf->Cell(160, 8, "Total de entrada - Total de saída:", 1, 0, 'L', true);
	
	$pdf->SetFont('Arial', '', 12);
	$pdf->SetFillColor(220, 220, 220);
	$pdf->SetTextColor(108, 108, 108);
	$pdf->Cell(30, 8, format_number_out($total_credito - $total_debito), 1, 0, 'R', true);
	$pdf->Ln();

	$pdf->Output();

?>