<div id="voltar">
    <p><a href="index.php">n&iacute;vel acima</a></p>
</div>	

<h2>transportadores</h2>
<?
	require("inc/con_db.php");
	
	require("transportador_filtro.php");
	//a��es em grupo
	if(isset($_POST['hid_action'])){
		require("transportador_action.php");
	}
	
	if(isset($_GET['mensagem']) && $_GET['mensagem'] == "ok"){
?>		<div class="alert">
			<p class="ok">Registro gravado com sucesso!<p>
        </div>
<?	}
		
/**************************** ORDER BY *******************************************/
	$filtroOrder = 'fldNomeFantasia ';
	$class 		  = 'asc';
	$order_sessao = explode(" ", $_SESSION['order_transportador']);
	if(isset($_GET['order'])){
		switch($_GET['order']){
			
			case 'codigo'		:  $filtroOrder = "fldId";   		 break;
			case 'nome'			:  $filtroOrder = "fldNome"; 		 break;
			case 'nomeFantasia'	:  $filtroOrder = "fldNomeFantasia"; break;
			case 'data'			:  $filtroOrder = "fldCadastroData"; break;
		}
		if($order_sessao[0] == $filtroOrder){
			$class = ($order_sessao[1] == 'asc') ? 'desc' : 'asc';
		}
	}
	
	//definir icone para ordem
	$_SESSION['order_transportador'] = (!$_SESSION['order_transportador'] || $_GET['order']) ? $filtroOrder.' '.$class : $_SESSION['order_transportador'];
	$pag	= ($_GET['pagina'])? '&pagina='.$_GET['pagina'] : ''; 
	$raiz	= "index.php?p=transportador$pag&amp;order=";	
	
	$order_sessao = explode(" ", $_SESSION['order_transportador']);
	$filtroOrder  = $order_sessao[0]; //pra poder comparar na listagem e exibir a class
	
/**************************** PAGINA��O *******************************************/
	$sSQL = "SELECT * FROM tbltransportador ". $_SESSION['filtro_transportador']." ORDER BY " . $_SESSION['order_transportador'];
	
	$_SESSION['transportador_relatorio'] = $sSQL;
	$rsTotal = mysql_query($sSQL);
	$rowsTotal = mysql_num_rows($rsTotal);

	
	//defini��o dos limites
	$limite = 50;
	$n_paginas = 7;
	$total_paginas = ceil(mysql_num_rows($rsTotal) / $limite);
	if(isset($_GET["pagina"]) && $_GET["pagina"] > $total_paginas){
		$inicio = 0;
	}elseif(isset($_GET['pagina'])){
		$inicio = ($_GET['pagina'] - 1) * $limite;
	}else{
		$inicio = 0;
	}
	$sSQL .= " limit " . $inicio . "," . $limite;
	$rsTransportador = mysql_query($sSQL);
	$pagina = ($_GET['pagina'] ? $_GET['pagina'] : "1");
	
#########################################################################################
?>
    <form class="table_form" action="" method="post">
    	<div id="table">
            <div id="table_cabecalho">
                <ul class="table_cabecalho">
                    <li class="order" style="width:70px;">
                    	<a <?= ($filtroOrder == 'fldId') 			? "class='$class'" : '' ?> style="width:55px" href="<?=$raiz?>codigo">C&oacute;digo</a>
                    </li>
                    <li class="order" style="width:200px;">
                    	<a <?= ($filtroOrder == 'fldNome') 			? "class='$class'" : '' ?> style="width:185px" href="<?=$raiz?>nome">Nome</a>
                    </li>
                    <li class="order" style="width:220px;">
                    	<a <?= ($filtroOrder == 'fldNomeFantasia') 	? "class='$class'" : '' ?> style="width:205px" href="<?=$raiz?>nomeFantasia">Nome Fantasia</a>
                    </li>
                    <li class="order" style="width:80px;">
                    	<a <?= ($filtroOrder == 'fldCadastroData') 	? "class='$class'" : '' ?> style="width:65px" href="<?=$raiz?>data">Cadastro</a>
                    </li>
                    <li style="width:110px; text-align:center;">CNPJ</li>
                    <li style="width:90px; text-align:center;">Telefone</li>
                    <li style="width:90px; text-align:center;">Telefone (2)</li>
                    <li style="width:30px"></li>
                    <li style="width:20px"><input type="checkbox" name="chk_todos" id="chk_todos" /></li>
                </ul>
            </div>
            <div id="table_container">       
                <table id="table_general" class="table_general" summary="Lista de transportadores">
                <tbody>
<?					
					$id_array = array();
					$n = 0;
					
					$linha = "row";
					$rows = mysql_num_rows($rsTransportador);
					while ($rowTransportador = mysql_fetch_array($rsTransportador)){
						$id_array[$n] = $rowTransportador["fldId"];
						$n += 1;
							
						//formatar cpf_cnpj
						$CPF_CNPJ = formatCPFCNPJTipo_out($rowTransportador['fldCPF_CNPJ'], $rowTransportador['fldTipo']);


						
?>						<tr class="<?= $linha; ?>">
<?				 	 		$icon = ($rowTransportador["fldDisabled"] ? "bg_disable" : "bg_enable");
							$title = ($rowTransportador["fldDisabled"] ? "desabilitado" : "habilitado");
?>							<td style="width:0;text-align:center;"><img src="image/layout/<?=$icon?>.gif" alt="status" title="<?=$title?>" /></td>
							<td class="cod"	style="width:52px;text-align:left"><?=str_pad($rowTransportador['fldId'], 4, "0", STR_PAD_LEFT)?></td>
							<td style="width:200px;"><?=$rowTransportador['fldNome']?></td>
                            <td style="width:217px;padding-left:5px"><?=$rowTransportador['fldNomeFantasia']?></td>
							<td style="width:80px; text-align:center"><?=format_date_out($rowTransportador['fldCadastroData'])?></td>
							<td style="width:110px; text-align:center"><?=$CPF_CNPJ?></td>
							<td style="width:90px; text-align:center"><?=$rowTransportador['fldTelefone1']?></td>
							<td style="width:90px; text-align:center"><?=$rowTransportador['fldTelefone2']?></td>
							<td style="width:10px;"></td>
                            <td style="width:auto"><a class="edit" href="index.php?p=transportador_detalhe&amp;id=<?=$rowTransportador['fldId']?>" title="editar"></a></td>
                            <td style="width:auto"><input type="checkbox" name="chk_transportador_<?=$rowTransportador['fldId']?>" id="chk_transportador_<?=$rowTransportador['fldId']?>" title="selecionar o registro posicionado" /></td>
                        </tr>
<?                      if($linha == "row"){
							$linha = "dif-row";
						}
						else{
							$linha = "row";
						}
                   }
?>		 		</tbody>
				</table>
            </div>
       	  
            <input type="hidden" name="hid_array" id="hid_array" value="<?=urlencode(serialize($id_array))?>" />
            <input type="hidden" name="hid_action" id="hid_action" value="true" />
            
			<div id="table_action">
                <ul id="action_button">
                    <li><a class="btn_novo" href="index.php?p=transportador_novo">novo</a></li>
                    <li><input 	type="submit" name="btn_action" id="btn_excluir" 	value="excluir" 	title="Excluir registro(s) selecionado(s)" onclick="return confirm('Deseja excluir os registros selecionados?')" /></li>
                    <li><input 	type="submit" name="btn_action" id="btn_habilitar" 	value="habilitar" 	title="Habilitar registro(s) selecionado(s)" /></li>
                    <li><input 	type="submit" name="btn_action" id="btn_desabilitar"value="desabilitar" title="Desabilitar registro(s) selecionado(s)" /></li>
                </ul>
        	</div>
            <div id="table_paginacao">
<?				$paginacao_destino = "?p=transportador&modo=cadastro";
				include("paginacao.php")
?>		
            </div>  
            <div class="table_registro">
            	<span>Exibindo registros <?=($pagina*$limite-$limite+1).' a '.($pagina*$limite-$limite+$rows)?> do total de <?=$rowsTotal?></span>
            </div>            
        
        </div>
       
	</form>