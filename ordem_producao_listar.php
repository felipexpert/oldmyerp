<?
	require("inc/con_db.php");
	require("ordem_producao_filtro.php");
	
	//a��es em grupo
	if(isset($_POST['hid_action'])){
		require("produto_action.php");
	}
	
	if(isset($_GET['mensagem']) && $_GET['mensagem'] == "ok"){
?>		<div class="alert">
			<p class="ok">Produto "<?= $_GET['produto'] ?>" com o c&oacute;digo "<?= $_GET['codigo'] ?>" foi gravado com sucesso!<p>
        </div>
<?	}

	
/**************************** ORDER BY *******************************************/
	$filtroOrder = 'tblop.fldAbertura ';
	$class 		 = 'asc';
	$order_sessao = explode(" ", $_SESSION['order_ordem_producao']);
	if(isset($_GET['order'])){
		switch($_GET['order']){
			
			case 'codigo'		:  $filtroOrder = "tblop.fldId";			break;
			case 'abertura'		:  $filtroOrder = "tblop.fldAbertura"; 		break;
			case 'previsao'		:  $filtroOrder = "tblop.fldPrevisao";		break;
			case 'prioridade' 	:  $filtroOrder = "tblop.fldPrioridade";	break;
		}
		if($order_sessao[0] == $filtroOrder){
			$class = ($order_sessao[1] == 'asc') ? 'desc' : 'asc';
		}
	}
	
	//definir icone para ordem
	$_SESSION['order_ordem_producao'] = (!$_SESSION['order_ordem_producao'] || $_GET['order']) ? $filtroOrder.' '.$class : $_SESSION['order_ordem_producao'];
	$pag	= ($_GET['pagina'])? '&pagina='.$_GET['pagina'] : ''; 
	$raiz 	= "index.php?p=ordem_producao$pag&amp;order=";
	
	$order_sessao = explode(" ", $_SESSION['order_ordem_producao']);
	$filtroOrder  = $order_sessao[0]; //pra poder comparar na listagem e exibir a class
/**************************** PAGINA��O *******************************************/
	
	
	$sSQL =  "SELECT
	tblop.*,
	tblop.fldId AS fldIdOP,
	(SELECT GROUP_CONCAT(DISTINCT fldDesc SEPARATOR ', ') AS fldDesc FROM tblop_item WHERE fldOP_Id = tblop.fldId AND fldExcluido = 0) AS fldDesc
	FROM tblop
	WHERE tblop.fldExcluido = 0 ".$_SESSION['filtro_op']."
	ORDER BY " . $_SESSION['order_ordem_producao']; //� usado id > 0 para poder usar os filtros sem problemas...

	$_SESSION['op_sql'] = $sSQL;
	
	$rsTotal 	= mysql_query($sSQL);
	echo mysql_error();
	$rowsTotal 	= mysql_num_rows($rsTotal);
	
	//defini��o dos limites
	$limite 	= 150;
	$n_paginas 	= 7;
	
	$total_paginas = ceil(mysql_num_rows($rsTotal) / $limite);
	
	if(isset($_GET["pagina"]) && $_GET["pagina"] > $total_paginas){
		$inicio = 0;
	}elseif(isset($_GET['pagina'])){
		$inicio = ($_GET['pagina'] - 1) * $limite;
	}else{
		$inicio = 0;
	}
	
	$sSQL .= " limit " . $inicio . "," . $limite;
	$rsOP = mysql_query($sSQL);
	$pagina = (isset($_GET['pagina'])) ? $_GET['pagina'] : "1";
	
	//echo $sSQL;
	
#########################################################################################
	
?>
    <form class="table_form" id="frm_produto" action="" method="post">
    	<div id="table">
            <div id="table_cabecalho">
                <ul class="table_cabecalho">
					<li style="width:20px"></li>
                    <li class="order" style="width:50px;">
                    	<a <?= ($filtroOrder == 'tblop.fldId') 	 ? "class='$class'" : '' ?> style="width:0; padding:1px 5px; margin:0" href="<?=$raiz?>codigo">C&oacute;digo</a>
                    </li>
                    <li style="width:360px; padding:5px 0 0 10px;">Produtos</li>
                    <li style="width:270px; padding:5px 0 0 12px;">Observa&ccedil;&atilde;o</li>
                    <li class="order" style="width:68px;">
                    	<a <?= ($filtroOrder == 'tblop.fldAbertura') ? "class='$class'" : '' ?> style="width:0; padding:1px 10px; margin:0" href="<?=$raiz?>abertura">Abertura</a>
                    </li>
                    <li class="order" style="width:70px;">
                    	<a <?= ($filtroOrder == 'tblop.fldPrevisao') ? "class='$class'" : '' ?> style="width:0; padding:1px 10px; margin:0" href="<?=$raiz?>previsao">Previs&atilde;o</a>
                    </li>
                    <li class="order" style="width:20px">
						<a <?= ($filtroOrder == 'tblop.fldPrioridade') ? "class='$class'" : '' ?> style="width:0; padding:1px 6px; margin:0" href="<?=$raiz?>prioridade" title="Prioridade">P</a>
                    </li>
                    <li style="width:15px"></li>
                    <li style="width:15px"><input type="checkbox" name="chk_todos" id="chk_todos" style="margin-top:2px;" /></li>
                </ul>
            </div>
            <div id="table_container">       
                <table id="table_general" class="table_general" summary="Lista de produtos">
                	<tbody>
<?					
						$id_array = array();
						$n = 0;
						$linha 	= "row";
						$rows 	= mysql_num_rows($rsOP);
						while ($rowOP = mysql_fetch_array($rsOP)){

							$id_array[$n] = $rowOP["fldId"];
							$n += 1;
							
?>							<tr class="<?= $linha; ?>">
								<td style="width:20px">
									<span title="<?= fnc_status_pedido($rowOP['fldIdOP'], "tblop")?>" class="<?=fnc_status_pedido($rowOP['fldIdOP'], "tblop")?>" style="display:block; width:20px; height:20px; margin:5px 0 0 0; padding:0;"></span>
								</td>
                                <td class="cod"	style="width:50px;text-align:center;"><?=str_pad($rowOP['fldIdOP'], 6, "0", STR_PAD_LEFT)?></td>
                                <td style="width:368px; padding-left:9px;"><?=substr($rowOP['fldDesc'], 0, 50)?></td>
								<td style="width:271px"><?=substr($rowOP['fldObs'], 0, 32)?></td>
								<td style="width:68px; text-align:center"><?=format_date_out($rowOP['fldAbertura']);?></td>
								<td style="width:68px; text-align:center"><?=format_date_out($rowOP['fldPrevisao']);?></td>
								<td><span title="prioridade <?=fnc_prioridade_op($rowOP['fldIdOP'], "tblop")?>" class="prioridade_<?=fnc_prioridade_op($rowOP['fldIdOP'], "tblop")?>"></span></td>
                                <td style="width:0;"><a class="edit" href="index.php?p=ordem_producao_detalhe&amp;id=<?=$rowOP['fldIdOP']?>" style='margin-top:3px;' title="editar"></a></td>
                                <td style="width:0"><input type="checkbox" name="chk_produto_<?=$rowOP['fldIdOP']?>" id="chk_produto_<?=$rowOP['fldIdOP']?>" title="selecionar o registro posicionado" style="margin-top:3px" /></td>
    	                    </tr>
<?	                     	$linha = ($linha == "row") ? "dif-row" : "row";
                  		}
?>		 			</tbody>
				</table>
            </div>
       	  
            <input type="hidden" name="hid_array" id="hid_array" value="<?=urlencode(serialize($id_array))?>" />
            <input type="hidden" name="hid_action" id="hid_action" value="true" />
            
			<div id="table_action">
                <ul id="action_button">
                    <li><a class="btn_novo" href="index.php?p=ordem_producao_novo">novo</a></li>
                    <li><input type="submit" name="btn_action" id="btn_excluir" 	value="excluir" 	title="Excluir registro(s) selecionado(s)" onclick="return confirm('Deseja excluir os registros selecionados?')" /></li>
				</ul>
        	</div>
            <div id="table_paginacao" style="width:230px;">
<?				$paginacao_destino = "?p=produto";
				include("paginacao.php")
?>          </div>  
            <div class="table_registro">
            	<span>Exibindo registros <?=($pagina*$limite-$limite+1).' a '.($pagina*$limite-$limite+$rows)?> do total de <?=$rowsTotal?></span>
            </div>      
        </div>
       
	</form>