<?php
	require_once('inc/pdo.php');

	$estoque_id = (!isset($_GET['estoque_id'])) ? "1" : $_GET['estoque_id'];
	$nivel_id 	= (!isset($_GET['nivel_id'])) ? "0" : $_GET['nivel_id'];
	
	($_SESSION['sel_estoque_id_enderecamento_novo'] == '') ? $_SESSION['sel_estoque_id_enderecamento_novo'] = 1 : ''; //SE NAO FOI SELECIONADO ESTOQUE AINDA
	$_SESSION['sel_estoque_id_enderecamento_novo'] = (isset($_GET['estoque_id'])) ? $_GET['estoque_id'] : $_SESSION['sel_estoque_id_enderecamento_novo'];

	//guardo o id do pai em uma sessao, para caso troque o estoque este id nao se perca
	($_SESSION['pai_id'] == '') ? $_SESSION['pai_id'] = 0 : '';
	$_SESSION['pai_id']  = (isset($_GET['pai_id'])) ? $_GET['pai_id'] : $_SESSION['pai_id'];

	//verifica se o pai_id existe, senao poe 0.
	$checar_pai_id = mysql_num_rows(mysql_query("SELECT * FROM tblestoque_enderecamento WHERE fldId = ".$_SESSION['pai_id']." AND fldEstoque_Id = ".$_SESSION['sel_estoque_id_enderecamento_novo']));
	if(!$checar_pai_id){$_SESSION['pai_id'] = 0;}
	
	$conn = new Conexao();

	if(isset($_POST['btn_editar_enderecamento'])){ //*editando nivel
		$fld_elemento 	= $_POST['txt_enderecamento_editar_nome'];
		$fld_sigla 		= $_POST['txt_enderecamento_editar_sigla'];
		$fld_id 		= $_POST['txt_enderecamento_editar_id'];

		$query_editar = mysql_query("UPDATE tblestoque_enderecamento SET
									fldElemento = '$fld_elemento',
									fldSigla 	= '$fld_sigla'
									WHERE fldId = '$fld_id'");

		if($query_editar){ 
			header('Location: index.php?p=estoque&modo=enderecamento_novo&estoque_id='.$_SESSION['sel_estoque_id_enderecamento_novo'].'&pai_id='.$fld_id);
		}
		else{ ?>
		<div class="alert">
			<p class="erro">Erro! <?=mysql_error();?><p>
        </div>
<?		}
	}

	if(isset($_POST['btn_excluir'])){ //*excluindo nivel
		$fldelemento_id = $_POST['txt_enderecamento_editar_id'];
		//primeiro pega o pai deste id
		$sql_pai_id = mysql_fetch_assoc(mysql_query("SELECT fldPai_Id FROM tblestoque_enderecamento WHERE fldId = '$fldelemento_id'"));
		$pai_id = $sql_pai_id['fldPai_Id'];
		$ids = $fldelemento_id.getSubNiveis_Id($fldelemento_id, $_SESSION['sel_estoque_id_enderecamento_novo']);
		echo $ids;

		//deleta os produtos
		$update_produtos = mysql_query("DELETE FROM tblproduto_estoque_enderecamento WHERE fldEnderecamento_Id IN ($ids)");

		$delete_niveis = mysql_query("DELETE FROM tblestoque_enderecamento WHERE fldId IN ($ids)") or die(mysql_error());

		if($delete_niveis){
			header("Location: index.php?p=estoque&modo=enderecamento_novo&estoque_id=".$_SESSION['sel_estoque_id_enderecamento_novo']."&pai_id=".$pai_id);
		}
	}

	
	if(isset($_POST['btn_reiniciar'])){
		unset($_SESSION['hierarquia']);
	}
	
	if(isset($_POST['btn_next'])){
		$nome 		= $_POST['txt_nome'];
		$quantidade = $_POST['txt_quantidade'];
		$sigla 		= $_POST['txt_sigla'];
		
		$hierarquia = isset($_SESSION['hierarquia']) ? $_SESSION['hierarquia'] : array();
		
		$hierarquia = $hierarquia + array("$nome" => array("$quantidade", "$sigla"));
		
		$_SESSION['hierarquia'] = $hierarquia;
	}
	
	if(isset($_POST['btn_finalizar'])){
		$hierarquia = isset($_SESSION['hierarquia']) ? $_SESSION['hierarquia'] : array();
		//variaveis controladoras
		$pai_id = 0;
		$estoque = $estoque_id;
		$pai = $_SESSION['pai_id'];
		$ids = array();
		while (list($key, list($val, $sigla_array)) = each($hierarquia)){
		//percorre a $hierarquia, que � onde fica gravado o nome do nivel e sua quantidade.
			if($pai_id){
			//caso $pai_id for diferente de 0, significa que j� n�o � o primeiro nivel, ou seja, haver� algum id referencial
				$id_controle = $ids;
				$ids = array();
				//guardamos os ids dentro de uma outra variavel para que o la�o de repeti��o n�o se perca e ap�s isso resetamos a variavel que recebe os ids
				foreach($id_controle as $pai){
					//controla quantas vezes o nivel ter� de ser gravado, de acordo com o nivel anterior (referencial, o pai)
					for($n=1;$n<=$val;$n++){
						//grava o nivel em si
						$elemento 	= $key.$n;
						$sigla 		= $sigla_array.$n;
						//"nome + controle"
						$sql = "insert into tblestoque_enderecamento (fldElemento, fldSigla, fldEstoque_Id, fldPai_Id) values (:elemento, :sigla, :estoque, :pai_id)";
						$consulta = $conn->prepare($sql);
						//carregar par�metros nos placeholders
						$consulta->bindParam(':elemento', $elemento, PDO::PARAM_STR);
						$consulta->bindParam(':sigla', $sigla, PDO::PARAM_STR);
						$consulta->bindParam(':estoque', $estoque_id, PDO::PARAM_INT);
						$consulta->bindParam(':pai_id', $pai, PDO::PARAM_INT);
						//executar a consulta
						$consulta->execute();
						$pai_id = $conn->lastInsertId();
						//ultimo id inserido						
						array_push($ids,$pai_id); 
						//acrescenta no array (que estar� limpo) o id pai para servir de referencia para o proximo nivel
					}
				}
			}
			else{
			//caso o pai_id for 0, significa que � o primeiro nivel, ent�o o procedimento utilizado para $id_controle � descartado
				for($n=1;$n<=$val;$n++){
					$elemento 	= $key.$n;
					$sigla 		= $sigla_array.$n;
					$sql = "insert into tblestoque_enderecamento (fldElemento, fldSigla, fldEstoque_Id, fldPai_Id) values (:elemento, :sigla, :estoque, :pai_id)";
					$consulta = $conn->prepare($sql);
					//carregar par�metros nos placeholders
					$consulta->bindParam(':elemento', $elemento, PDO::PARAM_STR);
					$consulta->bindParam(':sigla', $sigla, PDO::PARAM_STR);
					$consulta->bindParam(':estoque', $estoque_id, PDO::PARAM_INT);
					$consulta->bindParam(':pai_id', $pai, PDO::PARAM_INT);
					//executar a consulta
					$consulta->execute();
					$pai_id = $conn->lastInsertId();
					array_push($ids,$pai_id);
					//acrescenta no array (que estar� limpo) o id pai para servir de referencia para o proximo nivel
				}
			}
		}
	}
?>

	<script type="text/javascript">
		$(document).ready(function(){
			/*---------------------------------------------------------------*/
			//bloqueia a sele��o quando der 2 clicks
			if (typeof document.onselectstart!="undefined")
			document.onselectstart=function(){return false}
			else //FF
			document.getElementById('tela_ramificacao').style.MozUserSelect = "none";
			document.getElementById('table_cabecalho').style.MozUserSelect = "none";
			/*---------------------------------------------------------------*/
			
			var timerIn, iconCarregar, ultimoIdCarregado;
					
			$(".sub_normal").hide().end(); //esconde todos os subcomponentes

			$('.linkCarregar').dblclick(function(event) {
					$('.submenu:first', this).toggle("fast"); //exibe os subcomponentes (componente do componente)
					$('.folder:first', this).toggleClass("openFolder"); //troca a classe de pastinha fechada para pastinha aberta
			event.stopPropagation()});
			
			$('.linkCarregar').click(function(event){ //CARREGA A TABELA
			if (this.id != ultimoIdCarregado) //evita repeti��o de carregamento
			{
				//troca pai_id do btn_editar
				var url = "estoque_enderecamento_editar,"+this.id;
				$('#btn_editar_nivel').attr('href', url);
				//***************************************
				$("#hid_nivel_selecionado").val(this.id);
			}
			event.stopPropagation()});
			
			$('.titulo_tela').click(function(event){
				$("#tela_ramificacao li").find('span:first').css('background-color', 'transparent'); //limpa tudo, mostra os componentes do produto principal
			})
			
			$('#tela_ramificacao li').click(function(event){
				$("#tela_ramificacao li").find('span:first').css('background-color', 'transparent'); //limpa tudo, mostra os componentes do produto principal
				$(this).find('span:first').css('background-color', '#DDFFDD'); //adiciona o bg no atual
			})

			$('#tela_ramificacao li#<?=$_SESSION["pai_id"]?>').find('span:first').css('background-color', '#DDFFDD'); //adiciona o bg no atual
			
			$('#mostrar_tudo').click( //mostra todos os subcomponentes
				function(event) { 
					$('.submenu').slideDown("fast");
					$('.folder', '#tela_ramificacao').addClass("openFolder");
					event.stopPropagation()
				}
			);
			
			$('#esconder_tudo').click( //esconde todos os subcomponentes
				function(event) {
					$('.submenu').slideUp("fast");
					$('.openFolder', '#tela_ramificacao').attr("class", "folder");
					event.stopPropagation()
				}
			);

			$('#selecionar_nivel').click( //esconde todos os subcomponentes
				function(event) {
					confirmar = confirm("Deseja mesmo trocar o local de criacao?");
					if (confirmar){
						location.href="index.php?p=estoque&modo=enderecamento_novo&estoque_id=<?=$_SESSION['sel_estoque_id_enderecamento_novo']?>&pai_id="+$('#hid_nivel_selecionado').val();
					}
					else{
						return;
					}
				}
			);
		});
	</script>

   	<form id="frm_estoque_id"  class="table_form bg_blue" action="#" method="post" style="display:table">
    	<ul>
    		<li style="width:280px; float:left">
            	<select class="config" style="margin-left:13px; width: 240px" id="sel_estoque_id_enderecamento_novo" name="sel_estoque_id_enderecamento_novo">
<?					$rsEstoqueId = mysql_query("SELECT * FROM tblproduto_estoque WHERE fldExcluido = 0");
          			while($rowEstoqueId = mysql_fetch_array($rsEstoqueId)){
?>      				<option <?=($_SESSION['sel_estoque_id_enderecamento_novo'] == $rowEstoqueId['fldId']) ? 'selected="selected"' : '' ?> value="<?=$rowEstoqueId['fldId']?>"><?=$rowEstoqueId['fldNome']?></option>
<?					}
?>				</select>
			</li>
    	</ul>
    </form>

	<form class="frm_detalhe" style="width:230px; margin:0 5px; float:left" name="estoque_enderecamento_assistente" id="estoque_enderecamento_assistente" action="" method="post">
		<ul>
			<li>
				<label for="txt_nome">Nome do N&iacute;vel</label>
				<input type="text" name="txt_nome" id="txt_nome" value="" style="width:206px" autofocus>
			</li>
			<li>
				<label for="txt_sigla">Sigla</label>
				<input type="text" name="txt_sigla" id="txt_sigla" style="width:206px">
			</li>
			<li>
				<label for="txt_quantidade">Quantidade</label>
				<input type="text" name="txt_quantidade" id="txt_quantidade" style="width:206px">
			</li>
		</ul>
		<input type="hidden" name="nivel" value="">
		<div id="table_action" style="margin: 5px 0 0 3px">
			<button type="submit" class="btn_enviar" name="btn_next" id="btn_next">adicionar n&iacute;vel</button>
			<button type="submit" name="btn_finalizar" id="btn_finalizar">finalizar</button>
			<button type="submit" name="btn_reiniciar" id="btn_reiniciar">reiniciar</button>
		</div>
<?		if($_SESSION['hierarquia'] != ''){ ?>
		<div id="table_action" style="margin: 20px 0 0 3px">
			<ul>
				<li style="width:100%; color:black; font-size:12px">fila de cria&ccedil;&atilde;o:</li>
<?				$array_dados = $_SESSION['hierarquia'];
				$ids = array();
				while (list($elemento, list($qtd, $sigla_array)) = each($array_dados)){ ?>
				<li style="width:100%; font-size:11px"><?=$elemento?> (<?=$sigla_array?>)</li>
<?				} ?>
			</ul>
		</div>
<?		} ?>
	</form>

    <div style="float:left;">
        <div id="table_cabecalho" style="width:740px; margin-top:10px; ">
            <ul class="table_cabecalho" style="width:740px; float:left;">
                <li style="display:block; padding:5px; width:100%;">
                    <span style="color:#666; float:left; padding: 1px 0 0 0;" id="0" class="linkCarregar titulo_tela">Local de cria&ccedil;&atilde;o:</span>
                    <span id="mostrar_tudo" title="Expandir tudo" style="margin-right:8px"></span>
                    <span id="esconder_tudo" title="Recolher tudo"></span>
                    <input type="hidden" id="hid_nivel_selecionado" name="hid_nivel_selecionado" value="<?=$_SESSION['pai_id']?>" />
                </li>
            </ul>
        </div>
        <div id="table_container" style="width:730px; height:300px; border:1px solid #CCC; background:white; clear:both; padding:5px;">
            <ul id="tela_ramificacao">
            	<?=getRamificacao_Novo(0, $estoque_id, $_SESSION['pai_id'])?>
            </ul>
        </div>
    </div>

    <div id="table_action" style="float:right">
     	<ul id="action_button">
     		<li><a style="margin:0;" class="btn_enviar modal" rel="250-170" href="estoque_enderecamento_editar,<?=$_SESSION['pai_id']?>" name="btn_editar_nivel" id="btn_editar_nivel" title="edita o nivel selecionado">editar nivel</a></li>
     		<li><a class="btn_novo" name="selecionar_nivel" id="selecionar_nivel">selecionar nivel</a></li>
        </ul>
    </div>