<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
      	<title>myERP - Imprimir C&oacute;digo de Barras</title>
        <link rel="stylesheet" type="text/css" href="style/style_produto_etiqueta.css" />
        <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />

	</head>
	<body>
    
    <div id="no-print">
        <a class="print" href="#" onClick="window.print()">imprimir</a>
    </div>
    
<?	ob_start();
	session_start();
    
	require("inc/con_db.php");
	require("inc/fnc_general.php");
	 
	if(isset($_GET['filtro'])){
		$filtro = ' AND tblproduto.fldId IN ('.$_GET['filtro'].')';
	}
	$sSQL = $_SESSION['produto_barcode'].$filtro." GROUP BY tblproduto.fldId ".
			$_SESSION['filtro_produto_estoque_quantidade']." ORDER BY " . $_SESSION['order_produto'].", tblproduto.fldDescricao";
	$rsProduto = mysql_query($sSQL);
	echo mysql_error();

	$rowsProduto = mysql_num_rows($rsProduto);
	
	$get_linha  = $_GET['linha'];
	$get_coluna = $_GET['coluna'];
	$get_qtd	= $_GET['qtd'];
	
	if(fnc_sistema('produto_barcode_tamanho') == '11'){
		require("produto_imprimir_barcode_01.php");
	}else{ 
		require("produto_imprimir_barcode_02.php");
	}
?>		       	
	</body>
</html>

