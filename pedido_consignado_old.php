<?
	require("pedido_consignado_filtro.php");
	
	//a��es em grupo
	if(isset($_POST['btn_action'])){
		require("pedido_consignado_action.php");
	}
	
	if(isset($_GET['mensagem']) && $_GET['mensagem'] == "ok"){
?>		<div class="alert">
			<p class="ok">Registro gravado com sucesso!<p>
        </div>
<?	}
	
/**************************** ORDER BY *******************************************/
	$filtroOrder = 'fldId';
	$class 		 = 'asc';
	$order_sessao = explode(" ", $_SESSION['order_pedido_consignado']);
	if(isset($_GET['order'])){
		switch($_GET['order']){
			
			case 'codigo'		:  $filtroOrder = "fldId";   			break;
			case 'rota'			:  $filtroOrder = "fldRota"; 			break;
			case 'funcionario1'	:  $filtroOrder = "fldFuncionario1";	break;
			case 'funcionario2'	:  $filtroOrder = "fldFuncionario2";	break;
			case 'data'			:  $filtroOrder = "fldData";			break;
		}
		if($order_sessao[0] == $filtroOrder){
			$class = ($order_sessao[1] == 'asc') ? 'desc' : 'asc';
		}
	}
	
	//definir icone para ordem
	$_SESSION['order_pedido_consignado'] = (!$_SESSION['order_pedido_consignado'] || $_GET['order']) ? $filtroOrder.' '.$class : $_SESSION['order_pedido_consignado'];
	$pag	= ($_GET['pagina'])? '&pagina='.$_GET['pagina'] : ''; 
	$raiz 	= "index.php?p=pedido&modo=consignado$pag&amp;order=";
	
	$order_sessao = explode(" ", $_SESSION['order_pedido_consignado']);
	$filtroOrder  = $order_sessao[0]; //pra poder comparar na listagem e exibir a class
	
/**************************** PAGINA��O *******************************************/

	$sSQL	="
			SELECT tblpedido_consignado.*, 
			SUM(tblpedido_consignado_item.fldQuantidade) as rowItens,
			tblendereco_rota.fldRota,
			tblFuncionario1.fldNome as fldFuncionario1,
			tblFuncionario2.fldNome as fldFuncionario2
			FROM tblpedido_consignado 
			LEFT JOIN tblpedido_consignado_item ON tblpedido_consignado.fldId = tblpedido_consignado_item.fldConsignado_Id
			LEFT JOIN tblendereco_rota ON tblpedido_consignado.fldRota_Id = tblendereco_rota.fldId
			LEFT JOIN tblfuncionario tblFuncionario1 ON tblpedido_consignado.fldFuncionario1_Id = tblFuncionario1.fldId
			LEFT JOIN tblfuncionario tblFuncionario2 ON tblpedido_consignado.fldFuncionario2_Id = tblFuncionario2.fldId
			WHERE tblpedido_consignado.fldExcluido = 0
			".$_SESSION['filtro_pedido_consignado']."
			GROUP BY tblpedido_consignado.fldId ORDER BY ". $_SESSION['order_pedido_consignado'];
	
	$_SESSION['relatorio_pedido_consignado'] = $sSQL;
	$rsConsignado	= mysql_query($sSQL);
	//$_SESSION['pedido_relatorio'] = $sSQL;
	
	####################################################################################
	echo mysql_error();
	$rowsTotal = mysql_num_rows($rsConsignado);
	//defini��o dos limites
	$limite 	= 50;
	$n_paginas 	= 7;

	$total_paginas = ceil($rowsTotal / $limite);
	if(isset($_GET["pagina"]) && $_GET["pagina"] > $total_paginas){
		$inicio = 0;
	}elseif(isset($_GET['pagina'])){
		$inicio = ($_GET['pagina'] - 1) * $limite;
	}else{
		$inicio = 0;
	}
	#####################################################################################
	$sSQL 	 		.= " LIMIT " . $inicio . "," . $limite;
	$rsConsignado	= mysql_query($sSQL);
	$pagina 		= ($_GET['pagina'] ? $_GET['pagina'] : "1");
	
?>
    <form class="table_form" id="frm_pedido" action="" method="post">
    	<div id="table">
            <div id="table_cabecalho">
                <ul class="table_cabecalho">
                    <li class="order" style="width:80px">
                    	<a <? ($filtroOrder == 'fldId') ? print "class='$class'" : '' ?> style="width:65px; text-align:right" href="<?=$raiz?>codigo">C&oacute;d.</a>
                    </li>
					<li class="order" style="width:90px">
                    	<a <? ($filtroOrder == 'fldData') ? print "class='$class'" : '' ?> style="width:65px; text-align:center" href="<?=$raiz?>data">Data</a>
                    </li>
                    <li class="order" style="width:60px">
                    	<a <? ($filtroOrder == 'fldRota_Id') ? print "class='$class'" : '' ?> style="width:35px" href="<?=$raiz?>rota">Rota</a>
                    </li>
                    <li class="order" style="width:270px">
                    	<a <? ($filtroOrder == 'fldFuncionario1') ? print "class='$class'" : '' ?> style="width:255px" href="<?=$raiz?>funcionario1">Funcionario 1</a>
                    </li>
                    <li class="order" style="width:270px">
                    	<a <? ($filtroOrder == 'fldFuncionario2') ? print "class='$class'" : '' ?> style="width:255px" href="<?=$raiz?>funcionario2">Funcionario 2</a>
                    </li>
                    <li style="width:80px; text-align:right">Itens</li>
                    <li style="width:20px">&nbsp;</li>
                    <li style="width:20px">&nbsp;</li>
                    <li style="width:15px"><input type="checkbox" name="chk_todos" id="chk_todos" /></li>
                </ul>
            </div>
            <div id="table_container">       
                <table id="table_general" class="table_general" summary="Lista de consignados">
                	<tbody>
<?					
						$id_array 	= array();
						$n 			= 0;
						$linha 	 	= "row";
						$rows		= mysql_num_rows($rsConsignado);
						
						while($rowConsignado = mysql_fetch_array($rsConsignado)){
							$consignado_id	 = $rowConsignado['fldId'];
							$id_array[$n] 	 = $consignado_id;
							$n++;
							$status = ($rowConsignado['fldFinalizado'] > 0 )? 'finalizado' : 'andamento';
?>							<tr class="<?= $linha; ?>">
								<td style="width:80px; text-align:right"><span title="<?=$status?>" class="<?=$status?>"><?=str_pad($rowConsignado['fldId'], 5, "0", STR_PAD_LEFT)?></span></td>
								<td	style="width:80px; padding-left:10px; text-align:center;"><?=format_date_out($rowConsignado['fldData'])?></td>
								<td style="width:60px; text-align:right"><?=$rowConsignado['fldRota']?></td>
                                <td style="width:270px; padding-left:10px"><?=$rowConsignado['fldFuncionario1']?></td>
								<td style="width:270px;"><?=$rowConsignado['fldFuncionario2']?></td>
								<td style="width:80px; text-align:right;"><?=format_number_out($rowConsignado['rowItens'])?></td>
								<td style="width:20px"><a class="edit modal" href="pedido_consignado_detalhe,<?=$rowConsignado['fldId']?>" rel="780-570" title="finalizar" style="margin-top:2px"></a></td>
								<td style="width:0"><a class="print" title="imprimir" rel="externo" href="pedido_consignado_imprimir_A4.php?&amp;id=<?=$rowConsignado['fldId']?>"></a></td>							
                  	       		<td style="width:15px"><input type="checkbox" name="chk_pedido_consignado_<?=$rowConsignado['fldId']?>" id="chk_pedido_consignado_<?=$rowConsignado['fldId']?>" title="selecionar o registro posicionado" /></td>
                    	    </tr>
<?							$linha = ($linha == "row" )?"dif-row":"row";
						}
?>			 		</tbody>
				</table>
            </div>

            <input type="hidden" name="hid_array" id="hid_array" value="<?=urlencode(serialize($id_array))?>" />
            <input type="hidden" name="hid_action" id="hid_action" value="true" />
			
            <div id="table_paginacao" style="width: 962px">
                <div class="table_registro" style="float:left;margin:0;text-align:left; padding-left:10px">
                    <span>Exibindo registros <?=($pagina*$limite-$limite+1).' a '.($pagina*$limite-$limite+$rows)?> do total de <?=$rowsTotal?></span>
           		</div>
                <div style="width:400px; height:25px;float:right;display:table;text-align:right;padding-right:20px">
<?					$paginacao_destino = "?p=pedido&modo=listar";
					include("paginacao.php");
?>          	</div>
			</div>   
            <div id="table_action">
                <ul id="action_button">
                    <li><a class="btn_novo modal" href="pedido_consignado_novo" rel="750-520" title="novo">novo</a></li>
                   	<li><input 	type="submit" name="btn_action" id="btn_excluir" 	value="excluir"	title="Excluir registro(s) selecionado(s)" onclick="return confirm('Deseja excluir os registros selecionados?')" /></li>
					<li><a id="btn_print" name="btn_print" class="btn_print modal" href="pedido_consignado_relatorio_tipo" rel="300-100" target="_blank" title="Imprimir Relat&oacute;rio">&nbsp;</a></li>

                </ul>
        	</div>
        </div>
	</form>
    