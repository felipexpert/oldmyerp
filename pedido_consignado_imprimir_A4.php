<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
        <meta name="description" content="my ERP" />
        <meta name="author" content="Luana Le&atilde;o e Ivan de Le&atilde;o" />
        <!--<meta name="verify-v1" content="G6sJVQ4Jh6+rcjEIpAyciU1HfDtac2QK2iduaOe3Rb0=" /> -->
       	
      	<title>myERP - Imprimir Consignado</title>
         <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="Robots" content="none" />
    
        <link rel="stylesheet" type="text/css" media="all" href="style/impressao/style_pedido_imprimir_A4.css" />
        <link rel="stylesheet" type="text/css" media="print" href="style/impressao/style_imprimir_print.css" />
        
	</head>
	<body>
<?
	ob_start();
	session_start();
	//conectar ao db
	require_once("inc/con_db.php");
	require_once("inc/fnc_general.php");
	require_once("inc/fnc_ibge.php");
	
	$consignado_id  = $_GET['id'];
	$data 		= date("Y-m-d");
	
	$rsConsignado 	= mysql_query("SELECT tblpedido_consignado.*, tblendereco_rota.fldRota
								  FROM tblpedido_consignado LEFT JOIN tblendereco_rota ON tblpedido_consignado.fldRota_Id = tblendereco_rota.fldId
								  WHERE tblpedido_consignado.fldId = $consignado_id");
	$rowConsignado 	= mysql_fetch_array($rsConsignado);
	
	if($rowConsignado['fldFuncionario1_Id']){
		$rsFuncionario  = mysql_query("SELECT fldNome FROM tblfuncionario WHERE fldId = ".$rowConsignado['fldFuncionario1_Id']);
		$rowFuncionario = mysql_fetch_array($rsFuncionario);
		$funcionario1 	= $rowFuncionario['fldNome'];
	}
	if($rowConsignado['fldFuncionario2_Id']){
		$rsFuncionario  = mysql_query("SELECT fldNome FROM tblfuncionario WHERE fldId = ".$rowConsignado['fldFuncionario2_Id']);
		$rowFuncionario = mysql_fetch_array($rsFuncionario);
		$funcionario2 	= $rowFuncionario['fldNome'];
	}

	/*----------------------------------------------------------------------------------------------*/
	$rsEmpresa  		= mysql_query("SELECT * FROM tblempresa_info");
	$rowEmpresa 		= mysql_fetch_array($rsEmpresa);
	$CPFCNPJ_Empresa 	= formatCPFCNPJTipo_out($rowEmpresa['fldCPF_CNPJ'], $rowEmpresa['fldTipo']);
	$endereco_empresa 	= $rowEmpresa['fldEndereco'];
	$numero_empresa		= $rowEmpresa['fldNumero'];
	$bairro_empresa 	= $rowEmpresa['fldBairro'];
	$municipio_empresa	= fnc_ibge_municipio($rowEmpresa['fldMunicipio_Codigo']);
	$uf_empresa			= fnc_ibge_uf_sigla($rowEmpresa['fldMunicipio_Codigo']);
	
	/*----------------------------------------------------------------------------------------------*/
?>
    <div id="no-print">
        <div id="impressao_cabecalho">
            <ul id="bts">
                <li><a href="#" onclick ="window.print()"><span>Imprimir</span></a></li>
                <li><a href="index.php?p=pedido&amp;mensagem=ok"><span>Finalizar</span></a></li>
            </ul>
        </div>
	</div>
    <table id="pedido_imprimir">
<?
	$vendaDecimal 		= fnc_sistema('venda_casas_decimais');
	$quantidadeDecimal 	= fnc_sistema('quantidade_casas_decimais');	
	$cabecalho = 
		'<tr><td>
            <table class="cabecalho">
                <tr style="width: 200px;height:120px; float:left;"><td><img src="image/layout/logo_empresa.jpg" alt="logo" /></td></tr>
                <tr>
                    <td>'.$rowEmpresa['fldRazao_Social'].'</td>
                    <td>CNPJ '.$CPFCNPJ_Empresa.'</td>
                    <td>'.$endereco_empresa.', '.$numero_empresa.' - '.$bairro_empresa.' • '.$municipio_empresa.'/'.$uf_empresa.'</td>
                    <td>Fone: '.$rowEmpresa['fldTelefone1'].' '.$rowEmpresa['fldTelefone2'].'</td>
                </tr>
            </table>
		</td></tr>
        <tr>
        	<td>
                <table class="pedido_imprimir_dados">
                	<tr><td>
                        <table class="pedido_dados">
                            <tr><td><h2 style="margin-bottom:0px"><strong>Funcion&aacute;rio 1:</strong> '.$funcionario1.'</h2></td></tr>
                            <tr><td><h2><strong>Funcion&aacute;rio 2:</strong> '.$funcionario2.'</h2></td></tr>
                            <tr class="dados">
                                <td style="width:9.8cm">					<strong>C&oacute;digo: 	</strong>'.str_pad($rowConsignado['fldId'], 5, '0', STR_LEFT).'	</td>
                                <td style="width:10cm; text-align: right">	<strong>Data: 			</strong>'.format_date_out($rowConsignado['fldData']).'			</td>
                                <td style="width:9.8cm">					<strong>Rota: 			</strong>'.str_pad($rowConsignado['fldRota'], 5, '0', STR_LEFT).'</td>
                            </tr>
                        </table> 
                    </td></tr>';
					
					echo $cabecalho;
					$cabecalhoItem = '
					<tr>
                    	<td>
                        	<table class="pedido_descricao" style="border:1px solid #CCC; margin-top:5px; min-height:300px; display:block">
                            	<tr><td><h2 style="margin-top:0; text-align:center">Itens sa&iacute;da</h2></td></tr>
							   	<tr class="descricao" style="margin-bottom: 5px;">
                                    <td style="width:1.6cm; text-align:right;margin-right:0.2cm">C&oacute;d.</td>
									<td style="width:7.5cm">Descric&atilde;o</td>
                                    <td style="width:1.6cm; text-align:right">Sa&iacute;da (qtd)</td>
                                    <td style="width:1.6cm; text-align:right">Retorno (qtd)</td>
                                    <td style="width:1.6cm;text-align:right">Valor</td>
                                    <td style="width:1.6cm;text-align:right">Total</td>
                                    <td style="width:1.6cm;text-align:right">A prazo</td>
                                    <td style="width:1.6cm;text-align:right">A vista</td>
                                </tr>';
								echo $cabecalhoItem;
								$rsItem		 	= mysql_query("SELECT tblpedido_consignado_item.*, tblproduto.fldCodigo
															  FROM tblpedido_consignado_item INNER JOIN tblproduto ON tblpedido_consignado_item.fldProduto_Id = tblproduto.fldId
															  WHERE fldConsignado_Id = $consignado_id");
								while($rowItem 		= mysql_fetch_array($rsItem)){
									$x+= 1;
									
									$valor	 		= $rowItem['fldValor'];
									$qtde 			= $rowItem['fldQuantidade'];
									$qtdeAprazo		= $rowItem['fldQuantidade_Aprazo'];
									$qtdeAvista		= $rowItem['fldQuantidade_Avista'];
									
									$totalItem		= $valor * $qtde;
									$totalAprazo	= $valor * $qtdeAprazo;
									$totalAvista	= $valor * $qtdeAvista;
									
									$qtdRetorno		= $qtde - ($qtdeAprazo + $qtdeAvista);
									
									$total_item_saida 	+= $totalItem;
									$total_item_aprazo 	+= $totalAprazo;
									$total_item_avista	+= $totalAvista;
									
									#FAZER CONTROLE DE QUEBRA DE LINHAS POR CARACTERES DO ITEM
									$limiteLinha = 47; #limite de caracter por linha do item
									$limiteFolha = 40; #MAXIMO DE LINHAS POR FOLHA - REFERENTE A ITENS - COM RODAPE
									$linhas 	 += ceil($countItem / $limiteLinha);		
									$countItem 	 = strlen($rowItem['fldDescricao'].$complemento);
									
?>									<tr class="pedido_item">	
										<td style="width:1.6cm;text-align:right;margin-right:0.2cm"><?=$rowItem['fldCodigo']?></td>
										<td style="width:7.5cm"><?=$rowItem['fldDescricao']?></td>
                                      	<td style="width:1.6cm; text-align:right"><?=format_number_out($rowItem['fldQuantidade'],$quantidadeDecimal)?></td>
                                      	<td style="width:1.6cm; text-align:right"><?=format_number_out($qtdRetorno,$quantidadeDecimal)?></td>
                                        <td style="width:1.6cm;text-align:right"><?=format_number_out($rowItem['fldValor'],$vendaDecimal)?></td>
                                        <td style="width:1.6cm;text-align:right"><?=format_number_out($totalItem)?></td>
                                        <td style="width:1.6cm;text-align:right"><?=format_number_out($totalAprazo)?></td>
                                        <td style="width:1.6cm;text-align:right"><?=format_number_out($totalAvista)?></td>
                    				</tr>
<?									
									$itemN ++;				
									if($linhas >= 37 || $linhas > $limiteFolha && $rowsItem > $itemN){
										echo '</table></td></tr></table>';
										echo '<table id="pedido_imprimir" style="page-break-before: always">';
										echo $cabecalho;
										echo $cabecalhoItem;
										$linhas = 0;
									}
								}
								
?>							</table>
						</td>
					</tr>
<?					$cabecalhoItem = '
					<tr>
                    	<td>
                        	<table class="pedido_descricao" style="border:1px solid #CCC; margin-top:5px; min-height:300px; display:block">
                            	<tr><td><h2 style="margin-top:0; text-align:center">Itens recebidos</h2></td></tr>
								<tr class="descricao" style="margin-bottom: 5px;">
									<td style="width:1.8cm; text-align:right;margin-right:0.2cm">C&oacute;d.</td>
									<td style="width:11.3cm">Descric&atilde;o</td>
									<td style="width:2cm; text-align:right">Qtde</td>
									<td style="width:2cm;text-align:right">Valor</td>
									<td style="width:2cm;text-align:right">Total</td>
								</tr>';
								echo $cabecalhoItem;
								$rsItem		 	= mysql_query("SELECT tblpedido_consignado_item_recebido.*, tblproduto.fldCodigo
															  FROM tblpedido_consignado_item_recebido INNER JOIN tblproduto ON tblpedido_consignado_item_recebido.fldProduto_Id = tblproduto.fldId
															  WHERE fldConsignado_Id = $consignado_id");
								while($rowItem 	= mysql_fetch_array($rsItem)){
									$x+= 1;
									
									$valor	 	= $rowItem['fldValor'];
									$qtde 		= $rowItem['fldQuantidade'];
									$totalItem	= $valor * $qtde;
									$total_item_recebido += $totalItem;
									
									#FAZER CONTROLE DE QUEBRA DE LINHAS POR CARACTERES DO ITEM
									$limiteLinha = 47; #limite de caracter por linha do item
									$limiteFolha = 40; #MAXIMO DE LINHAS POR FOLHA - REFERENTE A ITENS - COM RODAPE
									$linhas 	 += ceil($countItem / $limiteLinha);		
									$countItem 	 = strlen($rowItem['fldDescricao'].$complemento);
									
?>									<tr class="pedido_item">	
                                        <td style="width:1.8cm;text-align:right;margin-right:0.2cm"><?=$rowItem['fldCodigo']?></td>
                                        <td style="width:11.3cm"><?=$rowItem['fldDescricao']?></td>
                                        <td style="width:2cm; text-align:right"><?=format_number_out($rowItem['fldQuantidade'],$quantidadeDecimal)?></td>
                                        <td style="width:2cm;text-align:right"><?=format_number_out($rowItem['fldValor'],$vendaDecimal)?></td>
                                        <td style="width:2cm;text-align:right"><?=format_number_out($totalItem)?></td>
                                    </tr>
<?									
									$itemN ++;				

									if($linhas >= 37 || $linhas > $limiteFolha && $rowsItem == $itemN){
										echo '</table></td></tr></table>';
										echo '<table id="pedido_imprimir" style="page-break-before: always">';
										echo $cabecalho;
										echo $cabecalhoItem;
										$linhas = 0;
									}
								}
?>								
            				</table>
						</td>
					</tr>
<?
					#CRIANDO RODAPE  !!!########################################################################################################################################
					$valor_saida 			= $rowConsignado['fldValor_Saida'];
					$valor_retorno	 		= $rowConsignado['fldValor_Retorno'];
					$valor_descontos	 	= $rowConsignado['fldDesconto'];
					$despesa_funcionario1 	= $rowConsignado['fldFuncionario1_Despesa'];
					$despesa_funcionario2 	= $rowConsignado['fldFuncionario2_Despesa'];
					$despesa_outros		 	= $rowConsignado['fldDespesa_Outros'];
					
					$total_vendido			= $total_item_aprazo + $total_item_avista;
					$total_valor_saldo 		= $valor_saida + $total_item_avista + $total_item_recebido - $despesa_funcionario1 - $despesa_funcionario2 - $despesa_outros - $valor_descontos;
					$total_valor_diferenca	= $total_valor_saldo - $valor_retorno;
		
					$rodape = "  
					<table id='pedido_pagamento'>
						<tr class='pedido_total' style='float:left; margin-right:45px'>
							<td><strong>Troco:		 		</strong><span>R$ ".format_number_out($valor_saida)."			</span></td>
							<td><strong>Total Itens: 		</strong><span>R$ ".format_number_out($total_item_saida)."		</span></td>
							<td><strong>Total Vendido: 		</strong><span>R$ ".format_number_out($total_vendido)."			</span></td>
							<td><strong>Total a prazo: 		</strong><span>R$ ".format_number_out($total_item_aprazo)."		</span></td>
							<td><strong>Total a vista: 		</strong><span>R$ ".format_number_out($total_item_avista)."		</span></td>
						</tr>
						<tr class='pedido_total' style='float:left; margin-right:45px'>
							<td><strong>Recebimentos: 		</strong><span>R$ ".format_number_out($total_item_recebido)."	</span></td>
							<td><strong>Despesa Func. 1: 	</strong><span>R$ ".format_number_out($despesa_funcionario1)."	</span></td>
							<td><strong>Despesa Func. 2: 	</strong><span>R$ ".format_number_out($despesa_funcionario2)."	</span></td>
							<td><strong>Despesa Outros: 	</strong><span>R$ ".format_number_out($despesa_outros)."		</span></td>
						</tr>
						<tr class='pedido_total' style='float:left'>
							<td><strong>Descontos: 			</strong><span>R$ ".format_number_out($valor_descontos)."		</span></td>
							<td><strong>Saldo Final: 		</strong><span>R$ ".format_number_out($total_valor_saldo)."		</span></td>
							<td><strong>Valor Retorno: 		</strong><span>R$ ".format_number_out($valor_retorno)."			</span></td>
							<td><strong>Diferen&ccedil;a: 	</strong><span>R$ ".format_number_out($total_valor_diferenca)."	</span></td>
						</tr>
					</table>";
?>
                    <tr><td><? echo $rodape;?></td></tr>
        		</table>
            </td>
		</tr>
	</table>
    </body>
</html>
