    /**
     * Testa uma data e retorna no formato YYYY-MM-DD
     * @param string $value
     * @return boolean|string
     */
    protected function formatDateToDB($value) {
        //verifica o formato da data
        $dia = 0;
        $mes = 0;
        $ano = 0;
        if (strstr($value, '/')) {
            $date = explode('/', $value);
            $dia = $date[0];
            $mes = $date[1];
            $ano = $date[2];
        } elseif (strstr($value, '-')) {
            $date = explode('-', $value);
            $dia = $date[2];
            $mes = $date[1];
            $ano = $date[0];
        }

        if (checkdate($mes, $dia, $ano)) {
            return 'teste';
            //return $date[2] . '-' . $date[1] . '-' . $date[0];
        } else {
            echo "Data inválida. Recebido: $value" . PHP_EOL;
            return false;
        }
    }