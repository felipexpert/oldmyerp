<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>MyERP OOP</title>
    </head>
    <body>
        <?php
        $tabela = 'vwPedidoItemComponente';

        echo '<pre>';
        echo 'Gerador de lista de campos e propriedades para MySQL ' . PHP_EOL;
        echo "Exibindo tabela: $tabela";

        require('../utils/UserAgent.class.php');
        require('../config.php');
        require('../libs/Conexao.class.php');


        $db = Conexao::getConexao();

        $campos = array();



        //preparar parâmetros para consulta
        $sql = "SHOW FULL COLUMNS FROM $tabela";
        $consulta = $db->query($sql);
        $fields = $consulta->fetchAll(pdo::FETCH_ASSOC);



        foreach ($fields as $field) {
            $campos[$field['Field']]['type'] = $field["Type"];
            $campos[$field['Field']]['value'] = null;
            $campos[$field['Field']]['comment'] = $field["Comment"];
            $campos[$field['Field']]['field'] = $field["Field"];
        }

        //print_r($campos);
        // montar coleção fields no formato:
        // 'id'                => array('tipo' => 'integer'),

        echo PHP_EOL;
        foreach ($campos as $campoNoBd => $propriedades) {
            
            if(substr($campoNoBd, 0, 3) == 'fld'){
                $campo = substr($campoNoBd, 3);
            }
            else{
                $campo = $campoNoBd;
            }
            
            $palavras = explode('_', $campo);
            
            $campoFormatado = '';
            foreach($palavras as $palavra){
                $campoFormatado .= ucfirst($palavra);
            }
            
            echo "'" . lcfirst($campoFormatado) . "'";

            // calcular quantidade de tabs de acordo com quantidade de caracteres do nome do campo
            $n_tabs = 6 - (floor((strlen($campoFormatado) + 2) / 8));
            for ($i = 1; $i < $n_tabs; $i++) {
                echo "\t";
            }
            echo "=> array('tipo' => ";

            echo "'" . ajustarTipo($propriedades['type']) . "'";

            
            
            
            if(substr($campo, -2) == 'Id') {
                echo "\t ,\t";
                
                $estrangeiroTabela = strtolower(substr($campo, 0, -3));
                
                $palavras = explode('_', $estrangeiroTabela);
            
                $tabelaFormatada = '';
                foreach($palavras as $palavra){
                    $tabelaFormatada .= ucfirst($palavra);
                }
                
                echo "'estrangeiroClass' => '$tabelaFormatada',";
                
                echo "\t";

                for ($i = 1; $i < $n_tabs -2; $i++) {
                    echo "\t";
                }
                
                echo "'estrangeiroCampo' => 'nome'";
                
            }
            
            
            
            
            echo "\t),";
            

            if ($propriedades['comment']) {
                echo "\t comment: " . $propriedades['comment'];
            }

            echo PHP_EOL;
        }

        // ajustar (simplificar) tipos do mysql para o php
        function ajustarTipo($tipoDB) {
            if (strstr($tipoDB, 'int')) {
                $tipo = 'integer';
            } elseif (strstr($tipoDB, 'decimal') or strstr($tipoDB, 'float') or strstr($tipoDB, 'double') or strstr($tipoDB, 'real')) {
                $tipo = 'double';
            } elseif (strstr($tipoDB, 'char') or strstr($tipoDB, 'text')) {
                $tipo = 'string';
            } elseif (strstr($tipoDB, 'date')) {
                $tipo = 'date';
            } elseif (strstr($tipoDB, 'time')) {
                $tipo = 'time';
            } else {
                $tipo = 'não tratado: ' . $tipoDB;
            }
            return $tipo;
        }
        ?>
    </body>
</html>