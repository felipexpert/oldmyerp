<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>MyERP OOP</title>
    </head>
    <body>
        <?php
        $tabela = 'tblpedido_item';

        echo '<pre>';
        echo "View para tabela: $tabela";
        echo '<hr>';

        require('../utils/UserAgent.class.php');
        require('../config.php');
        require('../libs/Conexao.class.php');


        $db = Conexao::getConexao();

        $campos = array();



        //preparar parâmetros para consulta
        $sql = "SHOW FULL COLUMNS FROM $tabela";
        $consulta = $db->query($sql);
        $fields = $consulta->fetchAll(pdo::FETCH_ASSOC);



        foreach ($fields as $field) {
            $campos[$field['Field']]['type'] = $field["Type"];
            $campos[$field['Field']]['value'] = null;
            $campos[$field['Field']]['comment'] = $field["Comment"];
            $campos[$field['Field']]['field'] = $field["Field"];
        }


        echo PHP_EOL;
        
        $view = 'SELECT ';
        foreach ($campos as $campoNoBd => $propriedades) {
            
            $view .= $campoNoBd;
            
            $view .= ' AS ';
            
            $campo = substr($campoNoBd, 3);
            
            $palavras = explode('_', $campo);
            
            
            $campoFormatado = '';
            foreach($palavras as $palavra){
                $campoFormatado .= ucfirst($palavra);
            }
            
            $view .=  lcfirst($campoFormatado);
            
            $view .= ', ';
            
            $view .= PHP_EOL;
        }
        
        $view = substr($view, 0, -4);
        
        $view .= PHP_EOL;
        
        $view .= 'from ' . $tabela;
        
        echo $view;

        ?>
    </body>
</html>