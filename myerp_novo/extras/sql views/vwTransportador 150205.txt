SELECT 

fldId AS id, 
fldNome AS nome, 
fldNomeFantasia AS nomeFantasia, 
fldTipo AS tipo, fldCPF_CNPJ AS cpfCnpj, 
fldIE AS ie, 
fldNascimento_Abertura AS nascimentoAbertura, 
fldTelefone1 AS telefone1, 
fldTelefone2 AS telefone2, 
fldFax AS fax, 
fldEmail AS email, 
fldWebsite AS website, 
fldEndereco AS endereco, 
fldNumero AS numero, 
fldComplemento AS complemento, 
fldBairro AS bairro, 
fldMunicipio_Codigo AS municipioCodigo, 
fldCEP AS cep, 
fldObservacao AS observacao, 
fldCadastroData AS cadastroData, 
fldDisabled AS disabled

FROM tbltransportador