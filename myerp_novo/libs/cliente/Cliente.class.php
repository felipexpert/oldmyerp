<?php

/**
 * Description of Cliente
 *
 * @author Ivan
 */
class Cliente extends TRecord {
    
    protected $dataSource   = 'vwCliente';
    
    protected $fields = array(
        'id'					=> array('tipo' => 'integer'	),
        'codigo'				=> array('tipo' => 'string'	),
        'nome'					=> array('tipo' => 'string'	),
        'nomeFantasia'				=> array('tipo' => 'string'	),
        'tipo'					=> array('tipo' => 'integer'	),
        'cpfCnpj'				=> array('tipo' => 'string'	),
        'rgIe'					=> array('tipo' => 'string'	),
        'cadastroData'				=> array('tipo' => 'date'	),
        'genero'				=> array('tipo' => 'integer'	),
        'email'					=> array('tipo' => 'string'	),
        'website'				=> array('tipo' => 'string'	),
        'nascimentoAbertura'			=> array('tipo' => 'date'	),
        'endereco'				=> array('tipo' => 'string'	),
        'enderecoId'				=> array('tipo' => 'integer'	 ,	'estrangeiroTabela' => 'Endereco',			'estrangeiroCampo' => 'nome'	),
        'numero'				=> array('tipo' => 'integer'	),
        'complemento'				=> array('tipo' => 'string'	),
        'bairro'				=> array('tipo' => 'string'	),
        'municipioCodigo'			=> array('tipo' => 'integer'	),
        'cep'					=> array('tipo' => 'string'	),
        'telefone1'				=> array('tipo' => 'string'	),
        'telefone2'				=> array('tipo' => 'string'	),
        'fax'					=> array('tipo' => 'string'	),
        'observacao'				=> array('tipo' => 'string'	),
        'funcionarioId'				=> array('tipo' => 'integer'	 ,	'estrangeiroTabela' => 'Funcionario',			'estrangeiroCampo' => 'nome'	),
        'creditoLimite'				=> array('tipo' => 'decimal'	),
        'beneficioNum1'				=> array('tipo' => 'string'	),
        'beneficioTipo1'			=> array('tipo' => 'integer'	),
        'beneficioNum2'				=> array('tipo' => 'string'	),
        'beneficioTipo2'			=> array('tipo' => 'integer'	),
        'rgEmissaoData'				=> array('tipo' => 'date'	),
        'rgEmissaoOrgao'			=> array('tipo' => 'string'	),
        'naturalidadeMunicipioCodigo'		=> array('tipo' => 'integer'	),
        'nomePai'				=> array('tipo' => 'string'	),
        'nomeMae'				=> array('tipo' => 'string'	),
        'tabelaPrecoId'				=> array('tipo' => 'integer'	 ,	'estrangeiroTabela' => 'TabelaPreco',			'estrangeiroCampo' => 'nome'	),
        'estadoCivilId'				=> array('tipo' => 'integer'	 ,	'estrangeiroTabela' => 'EstadoCivil',			'estrangeiroCampo' => 'nome'	),
        'pagamentoPerfilId'			=> array('tipo' => 'integer'	 ,	'estrangeiroTabela' => 'PagamentoPerfil',		'estrangeiroCampo' => 'nome'	),
        'suframaInscricao'			=> array('tipo' => 'string'	),
        'nfeEmail'				=> array('tipo' => 'string'	),
        'statusId'				=> array('tipo' => 'integer'	 ,	'estrangeiroTabela' => 'Status',			'estrangeiroCampo' => 'nome'	),
        'origemId'				=> array('tipo' => 'integer'	 ,	'estrangeiroTabela' => 'Origem',			'estrangeiroCampo' => 'nome'	),
        'sistemaVersao'				=> array('tipo' => 'integer'	),
        'modificacaoDataHora'			=> array('tipo' => 'time'	),
    );

}
