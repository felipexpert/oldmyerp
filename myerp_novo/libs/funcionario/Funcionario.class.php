<?php
/**
 * Description of Funcionario
 *
 * @author Ivan
 */
class Funcionario extends TRecord {
    
    protected $dataSource   = 'tblfuncionario';
    protected $dataShow     = 'vwFuncionario';
    
    protected $fields = array(
        'id'					=> array('tipo' => 'integer'	),
        'nome'					=> array('tipo' => 'string'	),
    );
}
