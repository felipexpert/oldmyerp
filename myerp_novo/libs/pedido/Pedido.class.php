<?php

/**
 * Description of Pedido
 *
 * @author Ivan
 */
class Pedido extends TRecord {
    
    protected $dataSource   = 'vwPedido';
    protected $dataShow     = 'vwPedidoShow';

    protected $fields = array(
        'id'					=> array('tipo' => 'integer'	),
        'referencia'				=> array('tipo' => 'integer'	),
        'usuarioId'				=> array('tipo' => 'integer'	 ,	'estrangeiroTabela' => 'Usuario',			'estrangeiroCampo' => 'nome'	),
        'clienteId'				=> array('tipo' => 'integer'	 ,	'estrangeiroTabela' => 'Cliente',			'estrangeiroCampo' => 'nome'	),
        'dependenteId'				=> array('tipo' => 'integer'	 ,	'estrangeiroTabela' => 'Dependente',			'estrangeiroCampo' => 'nome'	),
        'contaBancariaId'			=> array('tipo' => 'integer'	 ,	'estrangeiroTabela' => 'Contabancaria',                 'estrangeiroCampo' => 'nome'	),
        'veiculoId'				=> array('tipo' => 'integer'	 ,	'estrangeiroTabela' => 'Veiculo',			'estrangeiroCampo' => 'nome'	),
        'veiculoKm'				=> array('tipo' => 'string'	),
        'servico'				=> array('tipo' => 'string'	),
        'cadastroData'				=> array('tipo' => 'date'	),
        'cadastroHora'				=> array('tipo' => 'time'	),
        'pedidoData'				=> array('tipo' => 'date'	),
        'entregaData'				=> array('tipo' => 'date'	),
        'endereco'				=> array('tipo' => 'string'	),
        'desconto'				=> array('tipo' => 'decimal'	),
        'descontoReais'				=> array('tipo' => 'decimal'	),
        'valorTerceiros'			=> array('tipo' => 'decimal'	),
        'observacao'				=> array('tipo' => 'string'	),
        'retiradoPor'				=> array('tipo' => 'string'	),
        'status'				=> array('tipo' => 'integer'	),
        'excluido'				=> array('tipo' => 'integer'	),
        'clientePedido'				=> array('tipo' => 'integer'	),
        'tipoId'				=> array('tipo' => 'integer'	 ,	'estrangeiroTabela' => 'Tipo',                          'estrangeiroCampo' => 'nome'	),
        'comissao'				=> array('tipo' => 'decimal'	),
        'comandaNumero'				=> array('tipo' => 'integer'	),
        'comandaFechamento'			=> array('tipo' => 'date'	),
        'marcadorId'				=> array('tipo' => 'integer'	 ,	'estrangeiroTabela' => 'Marcador',			'estrangeiroCampo' => 'nome'	),
        'pedidoDestinoNfeId'			=> array('tipo' => 'integer'	 ,	'estrangeiroTabela' => 'PedidoDestinoNfe',		'estrangeiroCampo' => 'nome'	),
        'faturado'				=> array('tipo' => 'integer'	),
        'codigoCliente'				=> array('tipo' => 'string'	),
        
        'clienteNome'				=> array('tipo' => 'string'	),
        'itemId'				=> array('tipo' => 'integer'	),
        'itemDescricao'				=> array('tipo' => 'string'	),
        'itemQuantidade'                        => array('tipo' => 'decimal'	),
        
        'total'                                 => array('tipo' => 'money'	),

    );
    
    
    /*
     * métodos específicos da lógica de negócios
     */
    

    
    
    
    public function getItens() {
        $repositorio = new Repository(new PedidoItem);
        
        $registros = $repositorio->listar('*', "pedidoId = {$this->id}");

        return $registros;
        
    }
    
    
}