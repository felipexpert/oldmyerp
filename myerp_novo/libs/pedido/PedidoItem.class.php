<?php

/**
 * Description of PedidoItem
 *
 * @author Ivan
 */
class PedidoItem extends TRecord {
    
    protected $dataSource   = 'vwPedidoItem';
    protected $dataShow     = 'vwPedidoItem';


    protected $fields = array(
        'id'					=> array('tipo' => 'integer'	),
        'produtoId'				=> array('tipo' => 'integer'	 ,	'estrangeiroTabela' => 'Produto',			'estrangeiroCampo' => 'nome'	),
        'pedidoId'				=> array('tipo' => 'integer'	 ,	'estrangeiroTabela' => 'Pedido',			'estrangeiroCampo' => 'nome'	),
        'valor'					=> array('tipo' => 'money'	),
        'valorCompra'				=> array('tipo' => 'money'	),
        'desconto'				=> array('tipo' => 'money'	),
        'quantidade'				=> array('tipo' => 'money'	),
        'subtotal'				=> array('tipo' => 'money'	),
        'descricao'				=> array('tipo' => 'string'	),
        'excluido'				=> array('tipo' => 'integer'	),
        'tabelaPrecoId'				=> array('tipo' => 'integer'	 ,	'estrangeiroTabela' => 'TabelaPreco',			'estrangeiroCampo' => 'nome'	),
        'fardoId'				=> array('tipo' => 'integer'	 ,	'estrangeiroTabela' => 'Fardo',                         'estrangeiroCampo' => 'nome'	),
        'estoqueId'				=> array('tipo' => 'integer'	 ,	'estrangeiroTabela' => 'Estoque',			'estrangeiroCampo' => 'nome'	),
        'referencia'				=> array('tipo' => 'string'	),
        'entregue'				=> array('tipo' => 'integer'	),
        'entregueData'				=> array('tipo' => 'date'	),
        'exibicaoOrdem'				=> array('tipo' => 'integer'	),
        'lote'					=> array('tipo' => 'string'	),
        'produtoTipoId'				=> array('tipo' => 'integer'	 ,	'estrangeiroTabela' => 'ProdutoTipo',			'estrangeiroCampo' => 'nome'	),
    );
}
