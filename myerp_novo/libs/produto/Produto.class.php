<?php

/**
 *
 * @author Ivan
 */

class Produto extends TRecord {

    /**
     * é uma view temporaria para uso na especificacao
     */
    protected $dataSource   = 'vwProduto';
    protected $fields       = array(
        'id'					                => array('tipo' => 'integer'	 ),
        'codigo'				                => array('tipo' => 'string'	     ),
        'nome'					                => array('tipo' => 'string'      ),
        'tipo_id'                               => array('tipo' => 'integer'     ),
        'unidadeMedidaId'			            => array('tipo' => 'integer'	 ),
        'unidadeMedidaNome'			            => array('tipo' => 'string'	     ),
        'clienteNome'                           => array('tipo' => 'string'	     ),
        'preco'                                 => array('tipo' => 'string'      ),
        'categoria'                             => array('tipo' => 'string'      ),
        'clienteProdutoCodigo'                  => array('tipo' => 'string'	     ),
        'maquina'				                => array('tipo' => 'string'	     ),
        'maquinaId'				                => array('tipo' => 'integer'     ,       'estrangeiroClass' => 'Maquina',                                        'estrangeiroCampo' => 'nome'),
        'papel'					                => array('tipo' => 'string'	     ),
        'papelId'                               => array('tipo' => 'integer'     ,       'estrangeiroClass' => 'Produto',                                        'estrangeiroCampo' => 'nome'),
        'largura'				                => array('tipo' => 'decimal'	 ),
        'altura'				                => array('tipo' => 'decimal'	 ),
        'portaClicheNumero'			            => array('tipo' => 'string'	     ),
        'papelLargura'				            => array('tipo' => 'decimal'	 ),
        'papelMaquinaQtd'			            => array('tipo' => 'decimal'	 ),
        'regua'					                => array('tipo' => 'decimal'	 ),
        'papelCalculoTamanho'			        => array('tipo' => 'decimal'	 ),
        'maquinaVelocidade'			            => array('tipo' => 'decimal'	 ),
        'facaNumero'				            => array('tipo' => 'string'	     ),
        'carreiraNumero'			            => array('tipo' => 'decimal'	 ),
        'clicheRepeticao'			            => array('tipo' => 'decimal'	 ),
        'clicheCodigo'				            => array('tipo' => 'string'	     ),
        'papelAcerto'				            => array('tipo' => 'decimal'	 ),
        'producaoTempo'				            => array('tipo' => 'decimal'	 ),
        'roloId'				                => array('tipo' => 'integer'	 ,         'estrangeiroClass' => 'ProdutoEspecificacaoRolo',                     'estrangeiroCampo' => 'nome'	),
        'rolo'                                  => array('tipo' => 'string'      ),
        'verniz'				                => array('tipo' => 'integer'	 ),
        'vernizQualidade'			            => array('tipo' => 'integer'	 ),
        'etiquetaRoloQtd'			            => array('tipo' => 'decimal'	 ),
        'etiquetaCortada'			            => array('tipo' => 'integer'	 ),
        'acertoTempo'				            => array('tipo' => 'string'	     ),
        'rebobinamentoTipoId'			        => array('tipo' => 'integer'	 ,         'estrangeiroClass' => 'ProdutoEspecificacaoRebobinamentoTipo',		'estrangeiroCampo' => 'nome'	),
        'rebobinamentoMaquinaId'		        => array('tipo' => 'integer'	 ,         'estrangeiroClass' => 'Maquina',                                     'estrangeiroCampo' => 'nome'	),
        'rebobinamentoPapelLargura'		        => array('tipo' => 'decimal'	 ),
        'rebobinamentoCarreiraNumero'		    => array('tipo' => 'decimal'	 ),
        'rebobinamentoMaquinaVelocidade'	    => array('tipo' => 'decimal'	 ),
        'rebobinamentoFacaNumero'		        => array('tipo' => 'string'	     ),
        'rebobinamentoMaquinaAcerto'		    => array('tipo' => 'decimal'	 ),
        'rebobinamentoTubete'			        => array('tipo' => 'string'	     ),
        'rebobinamentoRoloDiametro'		        => array('tipo' => 'string'	     ),
        'rebobinamentoRoloMetroQuantidade'	    => array('tipo' => 'decimal'	 ),
        'rebobinamentoRoloPlastificado'         => array('tipo' => 'integer'	 ),
        'rebobinamentoEtiquetaMetroQuantidade'	=> array('tipo' => 'decimal'	 ),
        'rebobinamentoIr'			            => array('tipo' => 'string'	     ), 
        'rebobinamentoCa'			            => array('tipo' => 'string'	     ),
        'rebobinamentoTempo'			        => array('tipo' => 'decimal'	 ),
        'rebobinamentoData'			            => array('tipo' => 'date'	     ),
        'rebobinamentoObservacao'		        => array('tipo' => 'string'	     ),
        'embalagemRoloDiametro'			        => array('tipo' => 'string'	     ),
        'embalagemRoloAltura'                   => array('tipo' => 'string'	     ),
        'embalagemRoloPlastificado'		        => array('tipo' => 'integer'	 ),
        'embalagemCaixaId'			            => array('tipo' => 'integer'	 ,         'estrangeiroClass' => 'Produto',                                      'estrangeiroCampo' => 'nome'	),
        'embalagemTransportadorId'		        => array('tipo' => 'integer'	 ,         'estrangeiroClass' => 'Transportador',                                'estrangeiroCampo' => 'nome'	),
        'embalagemQuantidadeExata'		        => array('tipo' => 'string'	     ),
        'embalagemEnchimentoId'			        => array('tipo' => 'integer'     ,         'estrangeiroClass' => 'Produto',                                      'estrangeiroCampo' => 'nome'	),
        'embalagemData'				            => array('tipo' => 'date'	     ),
        'embalagemObservacao'			        => array('tipo' => 'string'	     ),
    );

    //retorna array com objetos do tipo componente
    function getComponentes(){
        $componenteRepository   = new Repository(new ProdutoComponente);
        $componenteRepository   ->setWhere("produtoId = '{$this->id}'");
        $componentes            = $componenteRepository->listar();
        return $componentes;
    }

}