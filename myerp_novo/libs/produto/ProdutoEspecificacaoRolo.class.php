<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of OrdemDeProducaoStatus
 *
 * @author Ivan
 */
class ProdutoEspecificacaoRolo extends TRecord {
    
    protected $dataSource = 'tblProdutoEspecificacaoRolo';
    
    protected $fields = array(
        'id'					=> array('tipo' => 'integer'	),
        'nome'                                  => array('tipo' => 'string'	),
    );
}
