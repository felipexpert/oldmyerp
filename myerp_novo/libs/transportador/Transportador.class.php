<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of OrdemDeProducaoStatus
 *
 * @author Ivan
 */
class Transportador extends TRecord {
    
    protected $dataSource = 'vwTransportador';
    
    protected $fields = array(
        'id'					=> array('tipo' => 'integer'	),
        'nome'					=> array('tipo' => 'string'	),
        'nomeFantasia'				=> array('tipo' => 'string'	),
        'tipo'					=> array('tipo' => 'integer'	),
        'cpfCnpj'				=> array('tipo' => 'string'	),
        'ie'					=> array('tipo' => 'string'	),
        'nascimentoAbertura'			=> array('tipo' => 'date'	),
        'telefone1'				=> array('tipo' => 'string'	),
        'telefone2'				=> array('tipo' => 'string'	),
        'fax'					=> array('tipo' => 'string'	),
        'email'					=> array('tipo' => 'string'	),
        'website'				=> array('tipo' => 'string'	),
        'endereco'				=> array('tipo' => 'string'	),
        'numero'				=> array('tipo' => 'integer'	),
        'complemento'				=> array('tipo' => 'string'	),
        'bairro'				=> array('tipo' => 'string'	),
        'municipioCodigo'			=> array('tipo' => 'integer'	),
        'cep'					=> array('tipo' => 'string'	),
        'observacao'				=> array('tipo' => 'string'	),
        'cadastroData'				=> array('tipo' => 'date'	),
        'disabled'				=> array('tipo' => 'integer'	),
    );
}
