<?php

/**
 * Description of DB
 *
 * @author Ivan
 */

/**
 * Esta classe provê os metodos necessarios para persistir e recuperar
 * objetos da base de dados (Active Record)
 */
abstract class TRecord {

    use Validacao;
    use TRecordFormatacao;
    use Fields;

    /**
     * Método getEstrangeiroLista
     * @param string $estrangeiroChave
     * @param string $estrangeiroCampo
     * @return array composto de [id, valor, selecionado] */
    public function getEstrangeiroLista($estrangeiroChave, $estrangeiroCampo = 'nome', $where = null){
        $estrangeiroClass = $this->fields[$estrangeiroChave]['estrangeiroClass'];
        $repositorio = new Repository(new $estrangeiroClass());
        if($where){ $repositorio->setWhere($where); }
        //$campoDoValor = $this->campoEncontrar($estrangeiroCampo);
        $registros = $repositorio->listar();
        $lista = array();
        
        foreach ($registros as $registro){
            $lista[] = array('id' => $registro->id, 'valor' => $registro->$estrangeiroCampo);
        }
        
        return $lista;
    }

    /**
     * Alimenta os atributos a partir de uma ID
     * @param int $id
     * @return boolean */
    public function load($id, $field = 'id'){
        //pega a tabela da classe em uso
        $table  = $this->getDataShow();
        $db     = Conexao::getConexao();
        $sql    = "select * from {$table} where $field = $id";
        $sth    = $db->prepare($sql);

        $sth->execute();
        //carregar os valores no objeto
        $row = $sth->fetchObject(get_class($this));
        if(!$row){ return false; }
        foreach ($row as $field => $value){ $this->$field = $value; }
        return true;
    }
    
    /**
     * Insere um registro no banco de dados
     * @return boolean */
    public function insert(){
		$this->formatarParaBd();
        //montar campos e valores para set seguindo os fields da classe usada
        foreach ($this->fields as $campo => $propriedades) {
            //coletar apenas campos alimentados, ignorando os demais
            if (isset($propriedades['value'])){
                $sqlSet[] = "$campo = '{$propriedades['value']}'"; 
            }
        }

        $sqlSet = implode(',', $sqlSet);
        $sql = 'INSERT INTO ' . $this->getDataSource() . " SET $sqlSet";
        $db = Conexao::getConexao();
        return $db->query($sql); //true - false
    }

    public function update($where = null){
    	$this->formatarParaBd();
        //montar campos e valores para update seguindo os fields da classe usada
        foreach ($this->fields as $campo => $propriedades){
            //coletar apenas campos alimentados, ignorando os demais
            if(isset($propriedades['value'])){
                if(is_null($where) && $campo == 'id'){ $where = "id = '{$propriedades['value']}'"; }
                else{ $sql[] = "$campo = '{$propriedades['value']}'"; }
            }
        }

        $sql = implode(',', $sql);
        $sql = 'UPDATE ' . $this->getDataSource() . " SET $sql where $where";
        $db = Conexao::getConexao();
        return $db->query($sql);
    }

    public function getDataSource(){
        return $this->dataSource;
    }

    public function getDataShow(){
        if(property_exists($this, 'dataShow')){ return $this->dataShow; }
        else{ return $this->dataSource; }
    }
    
    /** Retorna o ultimo ID inserido */
    public function getLastInsertId(){
        $db = Conexao::getConexao();
        return $db->lastInsertId();
    }
}
