<?php

/**
 * OrdemDeProducaoComItens usa uma view mais complexa pronta para listagem com dados estrangeiros
 *
 * @author Ivan
 */
class OrdemDeProducaoComItens extends TRecord {
    
    protected $dataSource = 'vwOrdemDeProducaoComItens';
    
    protected $fields = array(
        'id'					=> array('tipo' => 'integer'	),
        'aberturaData'				=> array('tipo' => 'date'	),
        'cadastroTimeStamp'                     => array('tipo' => 'date'	),
        'previsaoData'				=> array('tipo' => 'date'	),
        'conclusaoData'				=> array('tipo' => 'date'	),
        'statusId'				=> array('tipo' => 'integer'	,	'estrangeiroClass' => 'OrdemDeProducaoStatus',			'estrangeiroCampo' => 'nome'	),
        'statusNome'				=> array('tipo' => 'string'	),
        'prioridadeId'				=> array('tipo' => 'integer'	,	'estrangeiroClass' => 'OrdemDeProducaoPrioridade',		'estrangeiroCampo' => 'nome'	),
        'prioridadeNome'                        => array('tipo' => 'string'	),
        'impressaoLiberada'			=> array('tipo' => 'boolean'	),
        'observacao'				=> array('tipo' => 'string'	),
        'lote'					=> array('tipo' => 'integer'	),
        'usuarioId'				=> array('tipo' => 'integer'	),
        'excluido'				=> array('tipo' => 'integer'	),
        'itemPedidoId'				=> array('tipo' => 'integer'	 ,	'estrangeiroClass' => 'Itempedido',                            'estrangeiroCampo' => 'nome'	),
        'itemProdutoId'				=> array('tipo' => 'integer'	 ,	'estrangeiroClass' => 'Itemproduto',                           'estrangeiroCampo' => 'nome'	),
        'itemDescricao'                         => array('tipo' => 'string'	),
        'itemQuantidade'			=> array('tipo' => 'decimal'	),
        'itemPedidoCliente'			=> array('tipo' => 'string'	),
        'itemMaquina'                           => array('tipo' => 'string'	),
    );
    
}
