<?php

/**
 * OrdemDeProducao usa uma view simples para ser usada ao inserir e atualizar registros
 *
 * @author Ivan
 */
class OrdemDeProducao extends TRecord {
    
    protected $dataSource   = 'tblOrdemDeProducao';
    protected $dataShow     = 'vwOrdemDeProducaoComItens';
    
    protected $fields = array(
        'id'					=> array('tipo' => 'integer'	),
        'aberturaData'				=> array('tipo' => 'date'	),
        'cadastroTimeStamp'                     => array('tipo' => 'date'	),
        'previsaoData'				=> array('tipo' => 'date'	),
        'conclusaoData'				=> array('tipo' => 'date'	),
        'statusId'				=> array('tipo' => 'integer'	,	'estrangeiroClass' => 'OrdemDeProducaoStatus',			'estrangeiroCampo' => 'nome'	),
        'statusNome'				=> array('tipo' => 'string'	),
        'prioridadeId'				=> array('tipo' => 'integer'	,	'estrangeiroClass' => 'OrdemDeProducaoPrioridade',		'estrangeiroCampo' => 'nome'	),
        'prioridadeNome'                        => array('tipo' => 'string'	),
        'impressaoLiberada'			=> array('tipo' => 'boolean'	),
        'observacao'				=> array('tipo' => 'string'	),
        'lote'					=> array('tipo' => 'integer'	),
        'usuarioId'				=> array('tipo' => 'integer'	),
        'excluido'				=> array('tipo' => 'integer'	),
        'itemPedidoId'				=> array('tipo' => 'integer'	 ,	'estrangeiroTabela' => 'Itempedido',                            'estrangeiroCampo' => 'nome'	),
        'itemProdutoId'				=> array('tipo' => 'integer'	 ,	'estrangeiroTabela' => 'Itemproduto',                           'estrangeiroCampo' => 'nome'	),
        'itemDescricao'                         => array('tipo' => 'string'	),
        'itemQuantidade'			=> array('tipo' => 'decimal'	),
        'itemPedidoCliente'			=> array('tipo' => 'string'	),
        'itemPedidoClienteId'			=> array('tipo' => 'integer'	),
        'itemMaquina'                           => array('tipo' => 'string'	),
    );
    
    /**
     * Retorna uma array com os produtos da OP
     * @return array com produtos */
    public function getItens(){

        $itens = new Repository(new OrdemDeProducaoItem());
        $itens->setWhere("ordemDeProducaoId = '{$this->id}' AND excluido = '0'");
        return $itens->listar();

    }
    
}
