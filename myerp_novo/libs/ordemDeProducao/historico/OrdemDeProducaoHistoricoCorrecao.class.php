<?php

/**
 * OrdemDeProducao usa uma view simples para ser usada ao inserir e atualizar registros
 *
 * @author Ivan
 */
class OrdemDeProducaoHistoricoCorrecao extends TRecord {
    
    protected $dataSource   = 'tblOrdemDeProducaoHistoricoCorrecao';
    protected $dataShow     = 'vwOrdemDeProducaoHistoricoCorrecao';
    
    protected $fields = array(
        'id'					=> array('tipo' => 'integer'	),
        'historicoId'				=> array('tipo' => 'integer'	 ,	'estrangeiroClass' => 'Historic',			'estrangeiroCampo' => 'nome'	),
        'ordemDeProducaoId'			=> array('tipo' => 'integer'	 ,	'estrangeiroClass' => 'Ordemdeproduca',		'estrangeiroCampo' => 'nome'	),
        'ordemDeProducaoStatus'			=> array('tipo' => 'string'	),
        'ordemDeProducaoPrioridade'		=> array('tipo' => 'string'	),
        'ordemDeProducaoObservacao'		=> array('tipo' => 'string'	),
        'historicoCadastroData'			=> array('tipo' => 'date'	),
        'historicoFuncionarioId'		=> array('tipo' => 'integer'	 ,	'estrangeiroClass' => 'Historicofuncionari',	'estrangeiroCampo' => 'nome'	),
        'historicoFuncionarioNome'		=> array('tipo' => 'string'	),
        'historicoDescricao'			=> array('tipo' => 'string'	),
        'usuarioId'				=> array('tipo' => 'integer'	 ,	'estrangeiroClass' => 'Usuari',			'estrangeiroCampo' => 'nome'	),
        'usuarioNome'				=> array('tipo' => 'string'	),
        'correcaoData'				=> array('tipo' => 'date'	),
        'excluido'				=> array('tipo' => 'integer'	),
    );
    
}
