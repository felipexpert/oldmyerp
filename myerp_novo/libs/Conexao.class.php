<?php
/**
 * Conexao por PDO
 *
 * @author Ivan
 */
class Conexao {
    private static $cnx;
    
    /**
     * @return PDO
     */
    public static function getConexao(){
        if(!self::$cnx){
            self::open();
            
        }
        return self::$cnx;
    }

    public static function open() {
        $dsn = 'mysql:host='.DB_HOST.';dbname='.DB_NAME;
        $username = DB_USER_NAME;
        $passwd = DB_PASSWORD;
        $options = array(PDO::MYSQL_ATTR_INIT_COMMAND=>'SET NAMES utf8');
        self::$cnx = new PDO($dsn, $username, $passwd, $options);
    }

}
