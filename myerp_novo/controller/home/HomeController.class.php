<?php

class HomeController implements Controller {
	/**
	 * @return	boolean
	 * @see		Controller::canHandle()
	 */
	public function canHandle() {
		return !isset($_GET['m']) || $_GET['m'] == 'home';
	}

	/**
	 * @see		Controller::handle()
	 */
	public function handle() {
            echo "HOME -> <a href='index.php?m=ordemDeProducao'>OP</a> -> "
            . "<a href='index.php?m=produto'>Produto</a> -> <a href='index.php?m=ordemDeProducaoHistorico'>OP Historico</a>";
	}
}

?>