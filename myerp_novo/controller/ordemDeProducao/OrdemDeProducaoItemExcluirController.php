<?php

    chdir('../..');
    include('Config.php');
    
    $itens  = filter_input_array(INPUT_POST);
    $erro   = false;

    foreach($itens['ItemId'] as $key => $value){
        $item               = new OrdemDeProducaoItem();
        $item->id           = $value;
        $item->excluido     = 1;
        $item->update();
        
        $itemComponentes    = new Repository(new OrdemDeProducaoItemComponente());
        $itemComponentes->setWhere("itemId = '$value'");
        $itemComponentes    = $itemComponentes->listar();
        if(!is_null($itemComponentes)){
            foreach($itemComponentes as $componente){
                $itemComponente             = new OrdemDeProducaoItemComponente();
                $itemComponente->id         = $componente->id;
                $itemComponente->excluido   = 1;
                $itemComponente->update();
            }
        }
        
    }
