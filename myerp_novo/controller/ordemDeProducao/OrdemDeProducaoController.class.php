<?php

class OrdemDeProducaoController implements Controller {

        private $controllerManager;

        /* @return	boolean
	 * @see		Controller::canHandle() */
	public function canHandle(){
            $modulo         = filter_input(INPUT_GET, 'm');
            return $modulo == 'ordemDeProducao';
	}

	/**
	 * @see         Controller::handle()
	 */
	public function handle(){
            $request = filter_input(INPUT_SERVER, 'HTTP_X_REQUESTED_WITH');
            if($request != 'XMLHttpRequest'){ require('view/ordemDeProducao/ordemDeProducao.php'); }
            $this->controllerManager = ControllerManager::getInstance();
            $this->controllerManager->addController(new OrdemDeProducaoListarController());
            $this->controllerManager->addController(new OrdemDeProducaoRegistroController());
            $this->controllerManager->addController(new OrdemDeProducaoItemEditarController());
            $this->controllerManager->addController(new OrdemDeProducaoItemPedidoVincularController());
            $this->controllerManager->handle();
	}
}

?>