<?php

    chdir('../..');
    include('Config.php');
    
    $ordemDeProducaoHistorico                       = new OrdemDeProducaoHistorico();

    $ordemDeProducaoHistorico->ordemDeProducaoId    = filter_input(INPUT_POST, 'opId');
    $ordemDeProducaoHistorico->funcionarioId        = filter_input(INPUT_POST, 'funcionarioId');
    $ordemDeProducaoHistorico->cadastroData         = filter_input(INPUT_POST, 'historicoData');
    $ordemDeProducaoHistorico->usuarioId            = '1';
    $ordemDeProducaoHistorico->descricao            = filter_input(INPUT_POST, 'historicoDescricao');
    $ordemDeProducaoHistorico->excluido             = '0';

    if($ordemDeProducaoHistorico->insert()){
        echo $ordemDeProducaoHistorico->getLastInsertId();
    } else {
        echo "Erro ao gravar as informações no banco de dados <HistoricoRegistroSalvar>";
    }