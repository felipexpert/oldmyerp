<?php

class OrdemDeProducaoHistoricoController implements Controller {

        private $controllerManager;

        /* @return	boolean
	 * @see		Controller::canHandle() */
	public function canHandle(){
            $modulo         = filter_input(INPUT_GET, 'm');
            return $modulo == 'ordemDeProducaoHistorico';
	}

	/**
	 * @see         Controller::handle()
	 */
	public function handle(){
            if($_SERVER['REQUEST_METHOD'] == "POST"){
                $opId               = filter_input(INPUT_POST, 'opId');
                $ordemDeProducao    = new OrdemDeProducao();
                $ordemDeProducao->load($opId);
                $ordemDeProducao->formatar();

                $funcionarioId      = filter_input(INPUT_POST, 'funcionarioId');
                $funcionario        = new Funcionario();
                $funcionario->load($funcionarioId);
                $funcionario->formatar();

                $dataAtual = date("d/m/Y H:i:s");
                $usuarioId = 1;
                require('./view/ordemDeProducao/historico/ordemDeProducaoHistoricoInserir.php');
            } else {
                require('./view/ordemDeProducao/historico/ordemDeProducaoHistorico.php');
            }
	}
}

?>