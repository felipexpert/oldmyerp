<?php

class OrdemDeProducaoListarController extends OrdemDeProducaoController {
    
    /**
     * @see Controller::canHandle()
     */
    public function canHandle(){
        $submodulo = filter_input(INPUT_GET, 's');
        return (parent::canHandle()) && ($submodulo == 'listar' || !isset($submodulo));
    }

    /**
     * @see Controller::handle()
     */
    public function handle(){
        $ordemDeProducaoListarFiltro    = new OrdemDeProducaoListarFiltroController();
        $ordemDeProducaoRepository      = new Repository(new OrdemDeProducao());
        $ordemDeProducaoRepository->setWhere("excluido = '0'");
        $ordemDeProducaoRepository->setFiltro($ordemDeProducaoListarFiltro);
        $ordemDeProducaoLista           = $ordemDeProducaoRepository->listar();
        #########################################################################################
        $selOptStatus                   = new SelectOptions($ordemDeProducaoListarFiltro, 'statusId');
        $selOptPrioridade               = new SelectOptions($ordemDeProducaoListarFiltro, 'prioridadeId');
        require("view/ordemDeProducao/ordemDeProducaoListar.php");
    }
}

?>