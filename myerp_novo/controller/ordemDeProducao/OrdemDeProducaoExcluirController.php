<?php

    chdir('../..');
    include('Config.php');
    
    $opIds  = filter_input_array(INPUT_POST);
    $erro   = false;

    foreach($opIds['OpId'] as $key => $value){
        $op               = new OrdemDeProducao();
        $op->id           = $value;
        $op->excluido     = 1;
        $op->update();
        
        $item   = new Repository(new OrdemDeProducaoItem());
        $item   ->setWhere("ordemDeProducaoId = '$value'");
        $itens  = $item->listar();
        if(!is_null($itens)){
            foreach($itens as $_item){
                $item           = new OrdemDeProducaoItem();
                $item->id       = $_item->id;
                $item->excluido = 1;
                $item->update();
                
                $itemComponentes    = new Repository(new OrdemDeProducaoItemComponente());
                $itemComponentes->setWhere("itemId = '{$_item->id}'");
                $itemComponentes    = $itemComponentes->listar();
                if(!is_null($itemComponentes)){
                    foreach($itemComponentes as $componente){
                        $itemComponente             = new OrdemDeProducaoItemComponente();
                        $itemComponente->id         = $componente->id;
                        $itemComponente->excluido   = 1;
                        $itemComponente->update();
                    }
                }
            }
        }
        
    }
