<?php

class OrdemDeProducaoItemPedidoVincularController extends OrdemDeProducaoController {
    
	/**
	 * @return	boolean
	 * @see		Controller::canHandle()
	 */
	public function canHandle() {
        $submodulo = filter_input(INPUT_GET, 's');
        return (parent::canHandle()) && $submodulo == 'ItemPedidoVincular';
	}

	/**
	 * @see		Controller::handle()
	 */
	public function handle(){
        $pedidoRepository 	= new Repository(new Pedido);
        $pedidoRepository	->setJoin("LEFT JOIN vwOrdemDeProducaoItem ON vwPedidoShow.itemId = vwOrdemDeProducaoItem.itemId");
        $pedidoRepository	->setWhere("vwOrdemDeProducaoItem.id is null");
        $pedidoRepository 	->setLimit(15);
        $pedidoLista 		= $pedidoRepository->listar(); 
        require("./view/ordemDeProducao/ordemDeProducaoItemPedidoVincular.php");
    }
}

?>