<?php

class OrdemDeProducaoRegistroController extends OrdemDeProducaoController {
    
	/**
	 * @return	boolean
	 * @see		Controller::canHandle()
	 */
	public function canHandle() {
            $submodulo = filter_input(INPUT_GET, 's');
            return (parent::canHandle()) && ($submodulo == 'novo' || $submodulo == 'editar');
	}

	/**
	 * @see		Controller::handle()
	 */
	public function handle() {

            $paginaModo = filter_input(INPUT_GET, 's');
            
            if($paginaModo == 'editar'){ 
                $this->editar();
            }else{
                $this->novo();
            }
            
	}
        
        
        private function editar() {
            $ordemDeProducao = new OrdemDeProducao();
            $id = filter_input(INPUT_GET, 'id');
            $ordemDeProducao->load($id);
            $ordemDeProducao->setFormatarParaFormulario(true);
            $ordemDeProducao->formatar();
            $ordemDeProducaoItens = $ordemDeProducao->getItens();
            $selOptStatus           = new SelectOptions($ordemDeProducao, 'statusId');
            $selOptPrioridade       = new SelectOptions($ordemDeProducao, 'prioridadeId');
            $chkImpressaoLiberada   = new Checkbox('impressaoLiberada', $ordemDeProducao->impressaoLiberada);
            require("view/ordemDeProducao/ordemDeProducaoRegistro.php");
        }
        
        private function novo() {
            $ordemDeProducao                = new OrdemDeProducao();
            $ordemDeProducao->aberturaData  = date('d/m/Y');
            $selOptStatus                   = new SelectOptions($ordemDeProducao, 'statusId');
            $selOptPrioridade               = new SelectOptions($ordemDeProducao, 'prioridadeId');
            $chkImpressaoLiberada           = new Checkbox('impressaoLiberada', $ordemDeProducao->impressaoLiberada);
            $ordemDeProducaoItens           = array();
            require("view/ordemDeProducao/ordemDeProducaoRegistro.php");
        }
}

?>