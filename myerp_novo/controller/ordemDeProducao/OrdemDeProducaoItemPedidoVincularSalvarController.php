<?php

    chdir('../..');
    include('Config.php');

    $opId   = filter_input(INPUT_GET, 'opId');
    $itens  = filter_input_array(INPUT_POST);
    foreach($itens['ItemId'] as $key => $value){
        $pedidoItemId                           = $value;
        $ordemDeProducaoItem                    = new OrdemDeProducaoItem();
        $ordemDeProducaoItem->ordemDeProducaoId = $opId;

        //Pegar outros dados do item do pedido
        $pedidoItem = new PedidoItem();
        $pedidoItem->load($pedidoItemId);
        $pedidoItem->formatar();

        $ordemDeProducaoItem->pedidoId          = $pedidoItem->pedidoId;
        $ordemDeProducaoItem->itemId            = $pedidoItem->id;
        $ordemDeProducaoItem->produtoId         = $pedidoItem->produtoId;
        $ordemDeProducaoItem->produtoNome       = $pedidoItem->descricao;
        $ordemDeProducaoItem->produtoLote       = $pedidoItem->lote;
        $ordemDeProducaoItem->produtoQuantidade = $pedidoItem->quantidade;

        //Pegar dados do produto
        $produto = new Produto();
        $produto->load($ordemDeProducaoItem->produtoId);
        $produto->formatar();

        $ordemDeProducaoItem->produtoUnidadeMedidaId        = $produto->unidadeMedidaId;
        $ordemDeProducaoItem->produtoCodigo                 = $produto->codigo;
        $ordemDeProducaoItem->produtoFacaNumero             = $produto->facaNumero;
        $ordemDeProducaoItem->produtoLargura                = $produto->largura;
        $ordemDeProducaoItem->produtoAltura                 = $produto->altura;
        $ordemDeProducaoItem->produtoVerniz                 = $produto->verniz;
        $ordemDeProducaoItem->produtoVernizQualidade        = $produto->vernizQualidade;
        $ordemDeProducaoItem->produtoClicheCodigo           = $produto->clicheCodigo;
        $ordemDeProducaoItem->produtoPapelId                = $produto->papelId;
        $ordemDeProducaoItem->produtoPapelLargura           = $produto->papelLargura;

        $papelCalculoTamanho                                = $produto->papelCalculoTamanho;
        $carreiraNumero                                     = $produto->carreiraNumero;

        $ordemDeProducaoItem->produtoPapelComprimento       = ($pedidoItem->quantidade * ($papelCalculoTamanho / 100)) / $carreiraNumero;
        $ordemDeProducaoItem->produtoPapelComprimentoFinal  = ($pedidoItem->quantidade * ($papelCalculoTamanho / 100)) / $carreiraNumero;
        $ordemDeProducaoItem->produtoPapelQuantidadeM2      = ($produto->papelLargura / 100) * (($pedidoItem->quantidade * ($papelCalculoTamanho / 100)) / $carreiraNumero);


        $ordemDeProducaoItem->produtoMaquinaId              = $produto->maquinaId;
        $ordemDeProducaoItem->produtoPapelAcerto            = $produto->papelAcerto;
        
        if($ordemDeProducaoItem->insert()){
            //agora salvamos os componentes.
            $opItemId               = $ordemDeProducaoItem->getLastInsertId();
            $itemId                 = $pedidoItem->produtoId;
            $componentesRepository  = new Repository(new PedidoItemComponente());
            $componentesRepository  ->setWhere("itemId = '$itemId'");
            $componentesLista       = $componentesRepository->listar();
            if(!is_null($componentesLista)){
                foreach($componentesLista as $componente){
                    $componenteId                   = $componente->componenteId;
                    $itemComponente                 = new OrdemDeProducaoItemComponente();
                    $itemComponente->itemId         = $opItemId;
                    $itemComponente->componenteId   = $componenteId;
                    $itemComponente->excluido       = '0';
                    $itemComponente->insert();
                }
            }
            
            echo $itemId;
        }
        
        else{ echo "Erro ao gravar as informações no banco de dados <ItemVincular>"; }
    }