<?php

class ProdutoController implements Controller {

        private $controllerManager;

        /**
	 * @return	boolean
	 * @see		Controller::canHandle()
	 */
	public function canHandle(){
            $modulo         = filter_input(INPUT_GET, 'm');
            return $modulo == 'produto';
	}

	/**
	 * @see         Controller::handle()
	 */
	public function handle(){
            $request = filter_input(INPUT_SERVER, 'HTTP_X_REQUESTED_WITH');
            if($request != 'XMLHttpRequest'){ require('view/produto/produto.php'); }
            $this->controllerManager = ControllerManager::getInstance();
            $this->controllerManager->addController(new ProdutoListarController());
            $this->controllerManager->addController(new ProdutoEditarController());
            $this->controllerManager->handle();
	}
}

?>