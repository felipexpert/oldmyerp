<?php

class ProdutoEditarController extends ProdutoController {

        private $controllerManager;

        /**
	 * @return	boolean
	 * @see		Controller::canHandle()
	 */
	public function canHandle(){
            $submodulo = filter_input(INPUT_GET, 's');
            $produtoId = filter_input(INPUT_GET, 'produtoId');
            return (parent::canHandle()) && $submodulo == 'editar' && $produtoId;
	}

	/**
	 * @see         Controller::handle()
	 */
	public function handle(){
            $produtoId  = filter_input(INPUT_GET, 'produtoId');
            $produto    = new Produto();
            if(!$produto->load($produtoId)){ echo 'prod nao encontrado'; }
            $produtoEditarEspecificacaoController = new ProdutoEditarEspecificacaoController();
            if($produtoEditarEspecificacaoController->canHandle()){ $produtoEditarEspecificacaoController->handle(); }
	}
}

?>