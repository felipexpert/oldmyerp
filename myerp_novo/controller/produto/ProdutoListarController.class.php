<?php

class ProdutoListarController extends ProdutoController {

        /**
	 * @return	boolean
	 * @see		Controller::canHandle()
	 */
	public function canHandle(){
            $submodulo = filter_input(INPUT_GET, 's');
            return (parent::canHandle()) && (!isset($submodulo) || $submodulo == 'listar');
	}

	/**
	 * @see         Controller::handle()
	 */
	public function handle(){
            #echo '<a href="index.php?m=produto&s=editar&produtoId=1">editar produto id 1</a>';
	}
}

?>