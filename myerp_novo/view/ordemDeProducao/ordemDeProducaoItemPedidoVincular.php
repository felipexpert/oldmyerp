<div class="row">
    <form id="frmOrdemDeProducaoPedidoVincularFiltro" method="post" class="clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="panel panel-warning">
                <div class="panel-heading">Filtrar</div>

                <div class="panel-body">
                    <div class="form-group col-lg-2 col-md-2 col-sm-2">
                        <label for="pedidoId" class="small">ID Pedido</label>
                        <input type="text" class="form-control input-sm" name="pedidoId" id="pedidoId" value="">
                    </div>

                    <div class="form-group col-lg-4 col-md-4 col-sm-4">
                        <label for="clienteNome" class="small">Cliente</label>
                        <input type="text" class="form-control input-sm" name="clienteNome" id="clienteNome" value="">
                    </div>

                    <div class="form-group col-lg-4 col-md-4 col-sm-4">
                        <label for="produtoNome" class="small">Produto</label>
                        <input type="text" class="form-control input-sm" name="produtoNome" id="produtoNome" value="">
                    </div>

                    <div class="pull-right" style="padding:23px 15px 0 0">
                        <div class="btn-group">
                            <button type="button" class="btn btn-warning btn-sm" id="btnFiltrar">
                                <span class="glyphicon glyphicon-search"></span>
                                Filtrar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
    </form>

    <form id="frmOrdemDeProducaoPedidoVincular" method="post" style="margin-top:20px">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="panel panel-primary">
                <div class="panel-heading">Vincular novos itens de pedidos</div>

            <?  if(!is_null($pedidoLista)){ ?>
                <div class="table-responsive">
                    <table id="ordemDeProducaoItens" class="table table-hover table-striped table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center">Emissão</th>
                                <th class="text-center" title="ID do pedido">ID</th>
                                <th>Cliente</th>
                                <th>Produto</th>
                                <th class="text-center">Quantidade</th>
                                <th style="width:10px"></th>
                            </tr>
                        </thead>

                        <tbody id="listPedido">
                        <?  foreach($pedidoLista as $registro){ 
                            $registro->formatar(); ?>
                            <tr>
                                <td class="text-center"><?=$registro->pedidoData?></td>
                                <td class="text-center"><?=$registro->id?></td>
                                <td><?=$registro->clienteNome?></td>
                                <td><?=$registro->itemDescricao?></td>
                                <td><?=$registro->itemQuantidade?></td>
                                <td><input type="checkbox" name="ItemId[]" value="<?=$registro->itemId;?>"></td>
                            </tr>
                        <?  } ?>
                        </tbody>
                    </table>
                </div>
            <?  } else { ?>
                <div class="panel-body text-center">
                    Nenhum pedido disponível.
                </div>
            <?  } ?>
            </div>

            <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn btn-danger" id="btnIncluirItensCancelar" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove-sign"></span>
                        Cancelar
                    </button>
                </div>
                
                <div class="btn-group">
                    <button type="button" class="btn btn-success" id="btnIncluirItens">
                        <span class="glyphicon glyphicon-plus-sign"></span>
                        Vincular selecionados
                    </button>
                </div>
            </div>
        </div>   
    </form>
</div>

<script>
    $(document).ready(function(){
        // Enviar o form para salvar por Ajax
        $("#btnIncluirItens").click(function() {
            //Salvar os dados principais
            $.post("controller/ordemDeProducao/OrdemDeProducaoRegistroSalvarController.php", $("#frmOrdemDeProducaoRegistro").serialize())
                .done(function( data ) {
                opId = data;
                //Salvar itens da OP
                $.post("controller/ordemDeProducao/OrdemDeProducaoItemPedidoVincularSalvarController.php?opId=" + opId, $("#frmOrdemDeProducaoPedidoVincular").serialize())
                .done(function(){
                    alert("Item vinculado com sucesso!");
                    location.href = "index.php?m=ordemDeProducao&s=editar&id="+opId;
                });
            });
        });

        $("#btnFiltrar").click(function() {
            //Salvar os dados principais
            $.post("controller/ordemDeProducao/OrdemDeProducaoItemPedidoVincularFiltroController.php", $("#frmOrdemDeProducaoPedidoVincularFiltro").serialize())
                .done(function( data ) {
                $('#listPedido').html(data);
            });
        });
    });
</script>