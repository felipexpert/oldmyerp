<?php
    
    $cliente = new Cliente();
    $cliente->load(54);
    //$cliente->formatar();

    $funcionarioLista = $cliente->getEstrangeiroLista('funcionarioId');
    $selFuncionario = '<select class="form-control" id="selFuncionario">';
    foreach($funcionarioLista as $funcionario){
        $selFuncionario .= "<option value='{$funcionario['id']}'";
        $selFuncionario .= ($funcionario['id'] == $cliente->funcionarioId) ? ' selected ' : '';
        $selFuncionario .= ">{$funcionario['valor']}</option>";
    }
    $selFuncionario .= '</select>';
    
    
    echo $selFuncionario;
    
    
    echo '<hr>';
    
    
    //$cliente->nome = "testando 123";
    
    
?>


        <div class="container-fluid">
            <div class="row">
                barra topo
            </div>
            <div class="row">
                <div class="col-md-2">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="panel-heading">
                                painel
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">Produto XYZ</h4>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <ul class="nav nav-tabs">
                                    <li role="presentation" class="active"><a href="#">Home</a></li>
                                    <li role="presentation"><a href="#">Profile</a></li>
                                    <li role="presentation"><a href="#">Messages</a></li>
                                </ul>
                            </div>
                            <div class="panel-body">
                                <form class="form" role="form">
                                    <div class="form-group" style="display: inline-block; width: 100pt">
                                        <label for="txtId" class="control-label">Id</label>
                                        <input type="text" class="form-control" id="txtId" value="<?=$cliente->id?>">
                                    </div>

                                    <div class="form-group" style="display: inline-block;">
                                        <label for="txtCadastrado" class="control-label">Cadastrado</label>
                                        <input type="date" class="form-control" id="txtCadastrado" value="<?=$cliente->cadastroData?>">
                                    </div>

                                    <div class="form-group" style="display: inline-block">
                                        <label for="txtNome">Nome</label>
                                        <input type="text" class="form-control" id="txtNome" placeholder="Nome" value="<?=$cliente->nome?>">
                                    </div>

                                    <div class="form-group" style="display: inline-block; width: 30%">
                                        <label for="txtEndereco">Endereço</label>
                                        <input type="text" class="form-control" id="txtEndereco" placeholder="Endereço" value="<?=$cliente->endereco?>">
                                    </div>

                                    <div class="form-group" style="display: inline-block">
                                        <label for="txtCreditoLimite">Limite de Crédito</label>
                                        <input type="text" class="form-control" id="txtCreditoLimite" value="<?=$cliente->creditoLimite?>">
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="selFuncionario">Funcionário</label>
                                        <?=$selFuncionario?>
                                    </div>

                                    <button type="submit" class="btn btn-default">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


        </div>



        