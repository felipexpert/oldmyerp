    <div class="container-fluid" style="padding-bottom:40px; margin-top:10px">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="panel panel-danger">
                    <div class="panel-heading clearfix">
                        <span class="glyphicon glyphicon-exclamation-sign"></span>
                        Ops!
                    </div>
                    <div class="panel-body" style="background: #fcfcfc">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <i>ERRO!</i>
                        </div>
                        
                        <div class="col-lg-9 col-md-9 col-sm-9">
                            <h1>Nossos robôs detectaram uma URL inválida.</h1>
                            <h3>Entre em contato conosco caso o problema persista.</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>