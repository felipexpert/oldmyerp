<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>MyERP OOP</title>

        <!-- Bootstrap -->
        <link href="view/style/css/bootstrap.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <script src="view/style/js/jquery.min.js"></script>
        <script src="view/style/js/bootstrap.min.js"></script>
        
        <style>
            
            body{
               background:#fafafa; 
            }
            
            .table-primary thead {
                background-color:#ccc !important;
            }

            .table thead {
                background-color:#efefef;
            }

            #footer {
                background: rgba(0, 0, 0, 0.8); 
                padding:8px 15px 0 15px;
            }
            
            .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
                background-color: yellow;
            }
     
            .tr-selected{
                background: #FF9494 !important;
            }
            
            .modal-header{
                padding-top:8px; 
                padding-bottom: 5px; 
                background:#eee; }
        </style>
        
        <script>
            
            function loadModal(action){
                var id = $('.activeModal').length + 1;                
                
                $.get("./view/outros/modal.php", function(response){
                    $('body').append(response); 
                    var modal   = $('body').find('.modalVazio');
                    modal.addClass('activeModal').removeClass("modalVazio").attr("id", "modal-"+id);
                    modal.find('.modal-body').load(action, function(){
                        if(modal.find('.modal-title').length > 0){ modal.find('.modal-header').append(modal.find('.modal-title')); }
                        modal.modal('show');
                    });
                    modal.on('hidden.bs.modal', function(){ $(this).remove(); });
                });
            }
            
            $.fn.extend({
                clickable: function(action, type) {
                    var pageAction = action;
                    if (typeof type === "undefined" || type === null){ type = "normal"; }

                    $(this).find('td').css('cursor', 'pointer');

                    $(this).find('td').click(function(e){
                        var id          = $(this).parents('tr').attr('id');
                        var urlAction   = pageAction.replace('#', id);

                        if($(this).find('input:checkbox').length > 0){
                            var chk = $(this).find("input:checkbox").get(0);
                            if(e.target !== chk){ $(this).find("input:checkbox").trigger('click'); }
                            return;
                        } else {
                            if(type === 'modal'){
                                loadModal(urlAction);
                            } else {
                                $(location).attr('href', urlAction);
                            }
                        }
                    });

                    $(this).find('input:checkbox').change(function(){
                        if($(this).is(':checked')){
                            $(this).parents('tr').addClass('tr-selected');
                        } else {
                            $(this).parents('tr').removeClass('tr-selected');
                        }
                    });
                }
            });
        </script>
    </head>
    
    <body>
        <?php
            require_once('Application.php');
            Application::start();
        ?>
    </body>
</html>