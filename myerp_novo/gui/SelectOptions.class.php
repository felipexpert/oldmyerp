<?php


/**
 * Description of Select
 *
 * @author Ivan
 */
class SelectOptions {
    private $html;
    
    /**
     * @param object $objeto Objeto que contém a chave estrangeira
     * @param string $chaveEstrangeira O campo id estrangeiro. Ex: ProdutoId
     * @param boolean $exibirPlaceHolder [optional] Exibe a option: SELECIONE
     */
    public function __construct($objeto, $chaveEstrangeira, $exibirPlaceHolder = true, $where = null) {
        /***
        o where foi adicionado dia 9/4/15, foi a maneira mais rapida de resolver o problema de selecionar todos
        os itens de uma classe (produto no caso) (e menos elegante)
        ***/

        $lista = $objeto->getEstrangeiroLista($chaveEstrangeira, 'nome', $where);
        $options = '';
        if($exibirPlaceHolder){ $options = "<option value='' >SELECIONE</option>"; }
        foreach($lista as $item){
            $options .= "<option value='{$item['id']}'";
            $options .= ($item['id'] == $objeto->$chaveEstrangeira) ? ' selected ' : '';
            $options .= ">{$item['valor']}</option>";
        }
        
        $this->html = $options;
    }
    
    
    public function __toString() {
        return $this->html;
    }
}
