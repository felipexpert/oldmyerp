<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html language="pt-br">
    <head>
        <title>myERP - Imprimir Recibo</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="Robots" content="none" />
    
        <title>Impress&atilde;o de recibo</title>
        
    </head>
	<body>

<?Php
		ob_start();
		session_start();
		
		require("inc/con_db.php");
		require("inc/fnc_general.php");
		require_once("inc/fnc_imprimir.php");
		require_once("inc/fnc_identificacao.php");
		
		$impressao_local = fnc_estacao_impressora($_SESSION['remote_name']);
		if(!$impressao_local){
			$impressao_local = fnc_estacao_impressora('todos');
		}
		
		$texto 		= $impressao_local." \r\n";
		
		$filtro 	= $_GET['filtro'];
		$id_parcela = $_GET['id'];
		
		$rsEmpresa 	= mysql_query("select * from tblempresa_info");
		$rowEmpresa = mysql_fetch_array($rsEmpresa);
		
									
		$sql = "SELECT tblcliente.*, tblcliente.fldId as clienteId
				FROM tblpedido 
				INNER JOIN tblpedido_parcela ON tblpedido_parcela.fldPedido_Id 	= tblpedido.fldId
				INNER JOIN tblcliente 		 ON tblcliente.fldId 				= tblpedido.fldCliente_Id
				WHERE tblpedido_parcela.fldId IN ($id_parcela)";
	
		$CPF_CNPJ 	= formatCPFCNPJTipo_out($rowEmpresa['fldCPF_CNPJ'], $rowEmpresa['fldTipo']);
		
		$rsPedido	= mysql_query($sql);
		$rowPedido	= mysql_fetch_array($rsPedido);
		
		$cliente_id = $rowPedido['clienteId'];
		$cliente_nome = $rowPedido['fldNome'];
			
		if(isset($_GET['cliente'])){
			$cliente_id = $_GET['cliente'];
			$rsCliente  = mysql_query("SELECT * FROM tblcliente WHERE fldId = ".$cliente_id);
			$rowCliente = mysql_fetch_array($rsCliente);
			$cliente_nome = $rowCliente['fldNome'];
		}
		
		$data 		= date("Y-m-d");
		$hora 		= date("H:i:s");
		
		$empresa 	= strlen($rowEmpresa['fldNome_Fantasia']);
		$limite 	= 40;
		$espaco 	= $limite - $empresa;
		$espaco 	= $espaco / 2;
		$espaco 	= number_format($espaco , 0, '.', '');
		
		while($espaco > 0){
			$margem .=" "; 
			$espaco -= 1;		
		}
		
		$usuario_sessao = $_SESSION['usuario_id'];
		$remote_name 	= gethostbyaddr(gethostbyname($REMOTE_ADDR));
		$identificacao 	= fnc_identificacao($remote_name);
		$identificacao 	= acentoRemover($identificacao);
		
		$texto .= $margem . acentoRemover($rowEmpresa['fldNome_Fantasia'])." \r \r\n";
		$texto .="Fone Fax: ". $rowEmpresa['fldTelefone1']." \r\n";
		$texto .="Data: ".format_date_out(date("Y-m-d"))." Hora: ".date("H:i:s")." \r\n";
		$texto .="Estacao de trabalho: ".$identificacao." \r\n";
		$texto .="Cliente: ".acentoRemover($cliente_nome);
		if(fnc_sistema('pedido_exibir_endereco') > 0){
			$endereco = $rowPedido['fldEndereco'].", ".$rowPedido['fldNumero']."\r\n";
			$endereco .= $rowPedido['fldBairro'];
			$texto .= "\r\n".strtoupper(acentoRemover($endereco));
		}
	
		//###############################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################
		if(isset($filtro)){
			$rsParcelaBaixa = mysql_query("SELECT SUM(tblpedido_parcela_baixa.fldValor  * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldValorBaixa,
										SUM(tblpedido_parcela_baixa.fldJuros 		* (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldJuros,
										SUM(tblpedido_parcela_baixa.fldDesconto 	* (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldDesconto,
										SUM(tblpedido_parcela_baixa.fldMulta 		* (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldMulta,
										MAX(tblpedido_parcela_baixa.fldId) as fldUltimo_Id,
										tblpedido_parcela_baixa.fldParcela_Id as fldParcela_Id,
										tblpedido_parcela.fldValor as fldParcelaValor,
										tblpedido_parcela.fldPedido_Id,
										tblpedido_parcela.*
										FROM tblpedido_parcela LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela_baixa.fldParcela_Id = tblpedido_parcela.fldId
										WHERE tblpedido_parcela.fldStatus = '1' AND tblpedido_parcela_baixa.fldId IN ($filtro) GROUP BY tblpedido_parcela_baixa.fldParcela_Id");
		}else{
			//pega os dados das baixas de todas as parcelas DA MESMA VENDA
			$rsParcelaBaixa = mysql_query("SELECT SUM(tblpedido_parcela_baixa.fldValor  * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldValorBaixa,
										SUM(tblpedido_parcela_baixa.fldJuros 		* (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldJuros,
										SUM(tblpedido_parcela_baixa.fldDesconto 	* (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldDesconto,
										SUM(tblpedido_parcela_baixa.fldMulta 		* (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldMulta,
										tblpedido_parcela_baixa.fldParcela_Id as fldParcela_Id,
										tblpedido_parcela.fldValor as fldParcelaValor,
										tblpedido_parcela.fldPedido_Id,
										tblpedido_parcela.*
										FROM tblpedido_parcela LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela_baixa.fldParcela_Id = tblpedido_parcela.fldId
										WHERE tblpedido_parcela.fldStatus = '1' AND tblpedido_parcela.fldId IN ($id_parcela) GROUP BY tblpedido_parcela.fldId");
		}
		//########################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################
		//$rsBaixa = mysql_query($sSQL);
		
		
		#if($_SESSION["sistema_recibo_exibir_multa"] == '1' && $_SESSION["sistema_recibo_exibir_juros"] == '1'){
			#$texto .="\r\n\r\nvenc.  valor juros multa  desc   pago\r\n";
			
		#}elseif($_SESSION["sistema_recibo_exibir_multa"] == '1' && $_SESSION["sistema_recibo_exibir_juros"] == '0'){
			#$texto .="\r\n\r\nvenda  venc. valor multa  desc   pago\r\n";
	
		#}elseif($_SESSION["sistema_recibo_exibir_multa"] == '0' && $_SESSION["sistema_recibo_exibir_juros"] == '1'){
			#$texto .="\r\n\r\nvenda  venc. valor juros  desc   pago\r\n";
		#}else{
			$texto .="\r\n\r\nvenda  parc. venc. valor  desc   pago\r\n";
		#}
			$texto .="----------------------------------------\r\n";
		
			
		while($rowParcelaBaixa = mysql_fetch_array($rsParcelaBaixa)){
			/*** pegar o valor da última baixa (18/02/2013) ***/
			$ultimaBaixa 	  	 = mysql_fetch_array(mysql_query("SELECT SUM(fldValor) as fldBaixa_Anterior, SUM(fldJuros) as fldJuros_Anterior, SUM(fldMulta) AS fldMulta_Anterior FROM tblpedido_parcela_baixa WHERE fldParcela_Id = " . $rowParcelaBaixa['fldParcela_Id'] . " AND fldExcluido = 0 AND fldId <  ". $rowParcelaBaixa['fldUltimo_Id']));
			$ultimaBaixaValor	= $ultimaBaixa['fldValor'];

			$sqlTotalParcela = mysql_query("SELECT COUNT(fldId) AS fldQtdParcela FROM tblpedido_parcela WHERE fldPedido_Id = ".$rowParcelaBaixa['fldPedido_Id']);
			$rowTotalParcela = mysql_fetch_assoc($sqlTotalParcela);

			$pago 			= $rowParcelaBaixa['fldValorBaixa'] + $rowParcelaBaixa['fldJuros'] + $rowParcelaBaixa['fldMulta'];
			$valor 			= $rowParcelaBaixa['fldParcelaValor'] - $ultimaBaixa['fldBaixa_Anterior'] ;
			$juros 			= $rowParcelaBaixa['fldJuros'] - $ultimaBaixa['fldJuros_Anterior'] ;
			$multa 			= $rowParcelaBaixa['fldMulta'] - $ultimaBaixa['fldMulta_Anterior'] ;
			$parcela		= str_pad($rowParcelaBaixa['fldParcela'], 2, "0", STR_PAD_LEFT).'/'.str_pad($rowTotalParcela['fldQtdParcela'], 2, "0", STR_PAD_LEFT);
			
			/*** gravando no txt ********************************************************/
			if($_SESSION["sistema_recibo_exibir_multa"] == '1' && $_SESSION["sistema_recibo_exibir_juros"] == '1'){

				$texto	.= format_margem_print(format_date_out4($rowParcelaBaixa['fldVencimento']),6, 'direita');
				$texto	.= format_margem_print(format_number_out($valor),8, 'direita');
				$texto	.= format_margem_print(format_number_out($juros),6, 'direita');
				$texto	.= format_margem_print(format_number_out($multa),6, 'direita');

			}elseif($_SESSION["sistema_recibo_exibir_multa"] == '1' && $_SESSION["sistema_recibo_exibir_juros"] == '0'){

				$texto	.= format_margem_print(str_pad($rowParcelaBaixa['fldPedido_Id'], 6, "0", STR_PAD_LEFT),6, 'direita');
				$texto	.= format_margem_print(format_date_out4($rowParcelaBaixa['fldVencimento']),6, 'direita');
				$texto	.= format_margem_print(format_number_out($valor),8, 'direita');
				$texto	.= format_margem_print(format_number_out($multa),6, 'direita');

			}elseif($_SESSION["sistema_recibo_exibir_multa"] == '0' && $_SESSION["sistema_recibo_exibir_juros"] == '1'){

				$texto	.= format_margem_print(str_pad($rowParcelaBaixa['fldPedido_Id'], 6, "0", STR_PAD_LEFT),6, 'direita');
				$texto	.= format_margem_print(format_date_out4($rowParcelaBaixa['fldVencimento']),6, 'direita');
				$texto	.= format_margem_print(format_number_out($valor),8, 'direita');
				$texto	.= format_margem_print(format_number_out($juros), 6,'direita');

			}else{

				$texto	.= format_margem_print(str_pad($rowParcelaBaixa['fldPedido_Id'], 6, "0", STR_PAD_LEFT),6, 'direita');
				$texto	.= format_margem_print($parcela,6, 'direita');
				$texto	.= format_margem_print(format_date_out4($rowParcelaBaixa['fldVencimento']),6, 'direita');
				$texto	.= format_margem_print(format_number_out($valor),8, 'direita');

			}

			$texto	.= format_margem_print(format_number_out($rowParcelaBaixa['fldDesconto']),6, 'direita');
			$texto	.= format_margem_print(format_number_out($pago),8, 'direita')."\r\n";

			$totalParcelas 	+= $valor;
			$totalJuros 	+= $rowParcelaBaixa['fldJuros'];
			$totalDesconto 	+= $rowParcelaBaixa['fldDesconto'];
			$totalMulta 	+= $rowParcelaBaixa['fldMulta'];
			$totalPago	 	+= $pago;

		}
	
		$totalValor = $totalParcelas + $totalJuros + $totalMulta;
		
		
		/*** gravando no txt ********************************************************/
		
		$texto 	.= "---------------------------------------- \r\n";
		$texto  .= $textoPagoEm;
		$texto 	.= "                  Total  ";
		$texto	.= format_margem_print(format_number_out($totalParcelas),15, 'direita')."\r\n";
		
		if($_SESSION["sistema_recibo_exibir_multa"] == '1' && $_SESSION["sistema_recibo_exibir_juros"] == '1'){
			
			$texto 	.= "                 Multa + ";
			$texto	.= format_margem_print(format_number_out($totalMulta),15, 'direita')."\r\n";
			$texto 	.= "                 Juros + ";
			$texto	.= format_margem_print(format_number_out($totalJuros),15, 'direita')."\r\n";
			
		}elseif($_SESSION["sistema_recibo_exibir_multa"] == '1' && $_SESSION["sistema_recibo_exibir_juros"] == '0'){
			
			$texto 	.= "                 Multa + ";
			$texto	.= format_margem_print(format_number_out($totalMulta),15, 'direita')."\r\n";
		
		}elseif($_SESSION["sistema_recibo_exibir_multa"] == '0' && $_SESSION["sistema_recibo_exibir_juros"] == '1'){
			
			$texto 	.= "                 Juros + ";
			$texto	.= format_margem_print(format_number_out($totalJuros),15, 'direita')."\r\n";
			
		}
		
		$texto 	.= "              Desconto - ";
		$texto	.= format_margem_print(format_number_out($totalDesconto),15, 'direita')."\r\n";
		$texto 	.= "---------------------------------------- \r\n";
		$texto 	.= "         Total a pagar = ";
		$texto	.= format_margem_print(format_number_out($totalValor - $totalDesconto),15, 'direita')."\r\n";
		$texto 	.= "            Total pago = ";
		$texto	.= format_margem_print(format_number_out($totalPago),15, 'direita')."\r\n";
		$texto  .= "      Restante Devedor = ";
				$devedor = number_format($totalValor,2) - number_format($totalDesconto,2) - number_format($totalPago,2);
		$texto  .= format_margem_print(format_number_out($devedor),15, 'direita')."\r\n";
		
		if($_SESSION["sistema_recibo_exibir_assinatura_funcionario"] == '1'){
			$texto 	.= "\r\n\r\n\r\n\r\n     ------------------------------      \r\n";
			$texto 	.= "              funcionario";
		}
		#APARECER DEVEDOR DE OUTRAS VENDAS  #####################################################################################################################
		if($_SESSION["sistema_recibo_exibir_devedor"] > 0 ){
			$sSQL = "SELECT tblpedido_parcela.*,
				SUM(tblpedido_parcela_baixa.fldValor * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldValorBaixa,
				SUM(tblpedido_parcela_baixa.fldJuros * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldJuros,
				SUM(tblpedido_parcela_baixa.fldMulta * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldMulta,
				SUM(tblpedido_parcela_baixa.fldDesconto * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldDesconto,
				tblpedido.fldId as fldPedidoId
				FROM 
				(tblpedido_parcela LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id)
				INNER JOIN tblpedido ON tblpedido.fldId = tblpedido_parcela.fldPedido_Id
				WHERE tblpedido.fldCliente_Id = $cliente_id
				AND tblpedido_parcela.fldStatus = '1' 
				AND tblpedido_parcela.fldExcluido = '0'
				AND (tblpedido.fldPedido_Destino_Nfe_Id IS NULL OR tblpedido.fldPedido_Destino_Nfe_Id <= 0) GROUP BY tblpedido_parcela.fldId";
			$rsParcela = mysql_query($sSQL);
			while($rowParcela = mysql_fetch_array($rsParcela)){	
				$valorBaixa = $rowParcela['fldValorBaixa'] + $rowParcela['fldJuros'] + $rowParcela['fldMulta'];
				
				$rsDevedor = mysql_query("SELECT fldDevedor FROM tblpedido_parcela_baixa WHERE fldParcela_Id = ".$rowParcela['fldId']." AND fldExcluido = 0 ORDER BY fldId desc LIMIT 1 ");
				$rowDevedor = mysql_fetch_array($rsDevedor);
				if(mysql_num_rows($rsDevedor)){
					$devedor = $rowDevedor['fldDevedor'];
				}else{
					$devedor = $rowParcela['fldValor'];
				}
				
				$pedido_id 		= format_number_out($rowParcela['fldPedidoId']);  
				$valor	 		= format_number_out($rowParcela['fldValor']); 
				$valorEncargo	= format_number_out($rowParcela['fldJuros'] + $rowParcela['fldMulta']); 
				
				$totalDevedor 	+= $devedor;
				$pedido_id 		= str_pad($rowParcela['fldPedidoId'], 5, "0", STR_PAD_LEFT);
				
				if($devedor > 0){
					
					/*** montando o devedor ********************************************************/
					$rodape_devedor.= format_margem_print($pedido_id,5,'direita');
					$rodape_devedor.= format_margem_print(format_date_out4($rowParcela['fldVencimento']),6,'direita');
					$rodape_devedor.= format_margem_print($valor,7, 'direita');
					$rodape_devedor.= format_margem_print($valorEncargo,6, 'direita');
					$rodape_devedor.= format_margem_print(format_number_out($valorBaixa),8, 'direita');
					$rodape_devedor.= format_margem_print(format_number_out($devedor),8, 'direita')."\r\n";
					
				}
			}
			
			if($totalDevedor > 0 ){
				$texto 	.="\r\n\r\n\r\n********** OUTRAS PENDENCIAS ***********\r\n";
				$texto 	.="\r\nvenda venc   valor encar.   pago devedor\r\n";
				$texto 	.= "----------------------------------------\r\n";
				$texto	.=$rodape_devedor."\r\n\r\n\r\n";	
			}
		}
		
		$texto .="\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n";
		
		/*****************************************************************************************************************************************************************************************/
			$timestamp  = date("Ymd_His");
			$local_file = "impressao///inbox///imprimir_recibo_$timestamp.txt"; // Definimos o local para salvar o arquivo de texto
			$fp			= fopen($local_file, "w+"); //utilizamos o operador w+ para criar o arquivo imprimir.txt, e APAGAR tudo que já existe nele, caso ele já exista.
			$salva 		= fwrite($fp, $texto);
			fclose($fp);
			
?>    
		<script type="text/javascript">
			var local = '<?= $local_file ?>';
			window.location=local;
			window.close();
        </script>

	</body>
</html>