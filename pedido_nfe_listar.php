<?
	require('pedido_nfe_filtro.php');
	require('pedido_nfe_action.php');
	
	//mensagens referentes as ações
	if(!isset($_POST['hid_action'])){
		if(isset($_SESSION['msg_alert'])){
			echo $_SESSION['msg_alert']['open_markup'];
			echo $_SESSION['msg_alert']['content'];
			echo $_SESSION['msg_alert']['close_markup'];
			unset($_SESSION['msg_alert']);
		}
		if(isset($_SESSION['msg_ok'])){
			echo $_SESSION['msg_ok']['open_markup'];
			echo $_SESSION['msg_ok']['content'];
			echo $_SESSION['msg_ok']['close_markup'];
			
			unset($_SESSION['msg_ok']);
		}
	}
	
	if(isset($_GET['mensagem']) && $_GET['mensagem'] == "ok"){
?>		<div class="alert">
			<p class="ok">Registro gravado com sucesso!<p>
        </div>
<?	}

/**************************** ORDER BY *******************************************/
	$filtroOrder = 'tblpedido.fldId ';
	$class = 'desc';
	$order_sessao = explode(" ", $_SESSION['order_pedido_nfe']);
	if(isset($_GET['order'])){
		switch($_GET['order']){
			
			case 'codigo'		:  $filtroOrder = 'tblpedido.fldId';   			break;
			case 'nota'			:  $filtroOrder = 'tblpedido_fiscal.fldnumero';	break;
			case 'cliente'		:  $filtroOrder = 'fldClienteNome';		 		break;
			case 'funcionario'	:  $filtroOrder = 'fldFuncionarioNome';			break;
			case 'data'			:  $filtroOrder = 'tblpedido.fldPedidoData';	break;
		}
		if($order_sessao[0] == $filtroOrder){
			$class = ($order_sessao[1] == 'asc') ? 'desc' : 'asc';
		}
	}
	
	//definir icone para ordem
	$_SESSION['order_pedido_nfe'] = $filtroOrder.' '.$class;
	$raiz = 'index.php?p=pedido_nfe&amp;order=';
	
	$order_sessao = explode(' ', $_SESSION['order_produto']);
	$filtroOrder  = $order_sessao[0]; //pra poder comparar na listagem e exibir a class

/**************************** PAGINAÇÃO *******************************************/
	$sSQL = 'SELECT 
	tblpedido_fiscal.flddata_exportacao,
	tblpedido_fiscal.fldnumero,
	tblpedido_fiscal.fldnfe_total,
	tblpedido_fiscal.fldnfe_status_id,
	tblpedido_fiscal.flddanfe,
	tblpedido_fiscal.fldchave,
	tblpedido.fldTipo_Id,
	tblpedido.fldDesconto,
	tblpedido.fldDescontoReais,
	tblpedido.fldPedidoData,
	tblpedido.fldComissao,
	tblpedido.fldValor_Terceiros,
	tblpedido.fldObservacao AS fldPedidoObs,
	tblpedido.fldId AS fldPedidoId, 
	tblpedido.fldStatus AS fldStatus,
	tblpedido.fldFaturado,
	tblpedido.fldPedido_Destino_Nfe_Id,
	
	tblNome1.fldNome as fldClienteNome,
	tblNome2.fldNome as fldDependenteNome,
	SUM(tblpedido_parcela_baixa.fldValor	* (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldValorBaixa,
	SUM(tblpedido_parcela_baixa.fldDesconto * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) as fldBaixaDesconto,
	
	(SELECT SUM(fldValor) FROM
		tblpedido_funcionario_servico WHERE tblpedido_funcionario_servico.fldFuncao_Tipo = 2 
		AND tblpedido_funcionario_servico.fldPedido_Id = tblpedido.fldId) AS fldValorServico,
	
	(SELECT fldFuncionario_Id FROM
		tblpedido_funcionario_servico WHERE tblpedido_funcionario_servico.fldFuncao_Tipo = 1 
		AND tblpedido_funcionario_servico.fldPedido_Id = tblpedido.fldId) AS fldFuncionarioId,
		
	(SELECT fldNome FROM
		tblfuncionario WHERE tblfuncionario.fldId = tblpedido_funcionario_servico.fldFuncionario_Id) AS fldFuncionarioNome,
	
	(SELECT tblpedido_parcela.fldVencimento FROM tblpedido_parcela
		LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id
		WHERE tblpedido_parcela.fldExcluido <> 1 AND
		tblpedido_parcela.fldPedido_Id = tblpedido.fldId
		ORDER BY fldVencimento LIMIT 1) AS fldVencimento,
	
	SUM(tblpedido_parcela.fldValor	* (tblpedido_parcela.fldExcluido * -1 + 1)) AS fldTotalParcelas
	
	FROM (tblpedido_parcela LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela.fldId = tblpedido_parcela_baixa.fldParcela_Id) 
		RIGHT 	JOIN tblpedido ON tblpedido.fldId 						= tblpedido_parcela.fldPedido_Id 
		INNER 	JOIN tblpedido_fiscal ON tblpedido.fldId 				= tblpedido_fiscal.fldpedido_id 
		LEFT 	JOIN tblpedido_funcionario_servico ON tblpedido.fldId 	= tblpedido_funcionario_servico.fldPedido_Id
		INNER 	JOIN tblcliente tblNome1 ON tblpedido.fldCliente_Id 	= tblNome1.fldId
		LEFT 	JOIN tblcliente tblNome2 ON tblpedido.fldDependente_Id 	= tblNome2.fldId
		
	WHERE tblpedido.fldExcluido = 0 AND tblpedido.fldTipo_Id = 3
	AND (tblpedido.fldPedido_Destino_Nfe_Id IS NULL OR tblpedido.fldPedido_Destino_Nfe_Id <= 0)'. $_SESSION['filtro_pedido'].' ORDER BY ' . $_SESSION['order_pedido_nfe'];
	
	$_SESSION['pedido_nfe_relatorio'] = $sSQL;
	
	$rsTotal = mysql_query($sSQL);
	echo mysql_error();
	$rowsTotal = mysql_num_rows($rsTotal);
	//definição dos limites
	$limite 	= 50;
	$n_paginas 	= 7;
	
	$total_paginas = ceil(mysql_num_rows($rsTotal) / $limite);
	
	if(isset($_GET['pagina']) && $_GET['pagina'] > $total_paginas){
		$inicio = 0;
	}elseif(isset($_GET['pagina'])){
		$inicio = ($_GET['pagina'] - 1) * $limite;
	}else{
		$inicio = 0;
	}
	
	$sSQL .= ' limit ' . $inicio . ',' . $limite;
	$rsPedido = mysql_query($sSQL);
	
	$pagina = ($_GET['pagina'] ? $_GET['pagina'] : '1');
	
#########################################################################################
?>
    <form class="table_form" id="frm_pedido_nfe_listar" action="" method="post">
    	<div id="table">
            <div id="table_cabecalho">
                <ul class="table_cabecalho">
                    <li class="order" style="width:50px;">
                    	<a <?= ($filtroOrder == 'tblpedido.fldId') ?  "class='$class'" : '' ?> style="width:40px" href="<?=$raiz?>codigo">C&oacute;d.</a>
                    </li>
                    <li class="order" style="width:45px;">
                    	<a <?= ($filtroOrder == 'tblpedido_fiscal.fldnumero') ? "class='$class'" : '' ?> style="width:35px;" href="<?=$raiz?>nota">NFe</a>
                    </li>
                    <li class="order" style="width:210px;">
                    	<a <?= ($filtroOrder == 'fldClienteNome') ? "class='$class'" : '' ?> style="width:195px" href="<?=$raiz?>cliente">Cliente</a>
                    </li>
                    
<? 					if(fnc_sistema('exibir_dependente') > 0){
?>						<li style="width:103px;">Dependente</li>
<?					}else{
?>                      <li class="order" style="width:103px;">
                            <a <?= ($filtroOrder == 'fldFuncionarioNome') ? "class='$class'" : '' ?> style="width:90px; margin-left:8px;" href="<?=$raiz?>funcionario">Funcion&aacute;rio</a>
                        </li>
<?					}
?>                  <li style="width:150px; margin-left:3px;">Observa&ccedil;&atilde;o</li>
                    <li class="order" style="width:70px;">
                    	<a <?= ($filtroOrder == 'tblpedido.fldPedidoData') ? "class='$class'" : '' ?> style="width:55px;" href="<?=$raiz?>data">Emiss&atilde;o</a>
                    </li>
                    <li style="width:55px; text-align:right;">Total</li>
                    <li style="width:50px; text-align:center;">Parc.</li>
                    <li style="width:30px">&nbsp;</li>
                    <li style="width:30px">&nbsp;</li>
                    <li style="width:30px">&nbsp;</li>
                    <li style="width:30px">&nbsp;</li>
                    <li style="width:15px"><input type="checkbox" name="chk_todos" id="chk_todos" /></li>
                </ul>
            </div>
            <div id="table_container">       
                <table id="table_nfe_listar" class="table_general" summary="Lista de NFe">
                <tbody>
<?					
					$id_array = array();
					$n = 0;
					
					$impressao = fnc_sistema('sistema_impressao');
					if(isset($impressao)){
						$rowImpressao = mysql_fetch_array(mysql_query('SELECT * FROM tblsistema_impressao WHERE fldId ='.$impressao));
					
						$impressao = $rowImpressao['fldModelo'];
						if($rowImpressao['fldVariacao'] != ''){
							$impressao .= '_'.$rowImpressao['fldVariacao'];
						}
					}
					
					$linha 	= 'row';
					$rows 	= mysql_num_rows($rsPedido);
								
					$sqlItem = 'SELECT SUM((fldValor * fldQuantidade) - (((fldValor * fldQuantidade) * fldDesconto) / 100)) AS fldTotalItem, COUNT(fldQuantidade) as fldTotalQuantidade FROM tblpedido_item WHERE fldPedido_Id = ';
					while($rowPedido = mysql_fetch_array($rsPedido)){
						$pedido_id 		= $rowPedido['fldPedidoId'];
						$id_array[$n]	= $pedido_id;
						$n += 1;
						
						unset($item);
						unset($subTotalPedido);
						
						$rowItem 			= mysql_fetch_array(mysql_query($sqlItem.$pedido_id));
						$totalItem			= $rowItem['fldTotalItem'];
							
						$descPedido 		= $rowPedido['fldDesconto'];
						$descPedidoReais 	= $rowPedido['fldDescontoReais'];
						
						$subTotalPedido		= (($rowPedido['fldnfe_total'] + $rowPedido['fldComissao']) + $rowPedido['fldValor_Terceiros']) + $rowPedido['fldValorServico'];
						$descontoPedido 	= ($subTotalPedido 	* $descPedido) / 100;
						$totalPedido 		= $subTotalPedido 	- $descontoPedido;
						$totalPedido 		= $totalPedido 		- $descPedidoReais;
						
						$rowParcela 	= mysql_num_rows(mysql_query('SELECT * FROM tblpedido_parcela where fldPedido_Id = ' . $rowPedido['fldPedidoId']));
						$dataExportacao = explode(' ',$rowPedido['flddata_exportacao']);
						$data = format_date_out3($dataExportacao[0]);
						$hora = format_time_short($dataExportacao[1]);
						
						$faturado_class		= ($rowPedido['fldFaturado'] == 0) ? 'nao_faturado' 		: 'faturado';
						$faturado_titulo	= ($rowPedido['fldFaturado'] == 0) ? 'n&atilde;o faturado' 	: 'faturado';
						
						if($rowPedido['fldnfe_status_id'] > 0){
							$rsNfeStatus 	= mysql_query('SELECT fldStatus FROM tblnfe_pedido_status WHERE fldId ='.$rowPedido['fldnfe_status_id']);
							$rowNfeStatus 	= mysql_fetch_array($rsNfeStatus);
							$nfe_status		= $rowNfeStatus['fldStatus'];
							$transmitir_class 	= 'nfe_transmitir_disabled';
							$danfe_class 		= 'nfe_danfe_disabled';
							#CASO ESTEJA VALIDADO E NAO TRANSMITIDO
							if($rowPedido['fldnfe_status_id'] == 2){
								$nfe_status 	= 'processo';
							}
							if($rowPedido['fldnfe_status_id'] == 4){
									
								#se danfe ja impresso
								if($rowPedido['flddanfe'] == '1'){
									$danfe_class 		= 'nfe_danfe_print';
									$danfe_title		= 'Danfe impresso';
								}else{
									$danfe_class 		= 'nfe_danfe';
									$danfe_title		= 'Imprimir Danfe';
								}
								
							}elseif($rowPedido['fldnfe_status_id'] == 5){
								$transmitir_class 	= 'nfe_transmitir';
							}
							
							
						}else{
							$nfe_status 		= 'digitacao';
							$transmitir_class 	= 'nfe_transmitir';
							$danfe_class 		= 'nfe_danfe_disabled';
						}
						
?>						<tr class="<?= $linha; ?>" id="<?=$rowPedido['fldPedidoId']?>">
							<td style="width:50px"><span title="<?= fnc_status_pedido($rowPedido['fldPedidoId'], "tblpedido")?>" class="<?=fnc_status_pedido($rowPedido['fldPedidoId'], "tblpedido")?>"><?=str_pad($rowPedido['fldPedidoId'], 4, "0", STR_PAD_LEFT)?></span></td>
                            <td	style="width:47px; text-align:center; color:#C90"><?=str_pad($rowPedido['fldnumero'], 4, "0", STR_PAD_LEFT)?></td>
                            <td	style="width:210px; padding-left:10px; text-align:left;"><?=$rowPedido['fldClienteNome']?></td>
							<td style="width:105px;">
<?							if(fnc_sistema('exibir_dependente') > 0){ echo substr($rowPedido['fldDependenteNome'],0,20); 
							}else{
								$funcionarioId = $rowPedido['fldFuncionarioNome'];
								if($funcionarioId != '') {	
?>									<a href="<?='?p=funcionario_detalhe&id='.$rowPedido['fldFuncionarioId'];?>" title="Visualizar/Editar Funcion&aacute;rio"><?=str_pad($funcionarioId, 4, "0", STR_PAD_LEFT)?></a>
<?								}	
							}	
?>							</td>
							<td style="width:160px;"><?=substr($rowPedido['fldPedidoObs'],0,30)?></td>
							<td style="width:70px; text-align:center"><?=format_date_out3($rowPedido['fldPedidoData'])?></td>
							<td style="width:55px; text-align:right;"><?=format_number_out($totalPedido)?></td>
							<td style="width:46px; padding-right: 5px; text-align:center"><?=$rowParcela;?></td>
							<td><span class="<?=$faturado_class?>" title="<?=$faturado_titulo?>">&nbsp;</span></td>
                            <td><a class="edit" href="index.php?p=pedido_nfe_detalhe&id=<?=$rowPedido['fldPedidoId']?>" title="editar" style="margin-top:3px;"></a></td>
							
<?							if($impressao == 'lpt1'){
?>								<td><a class="print" title="imprimir" href= javascript:abrirPOPUP('pedido_imprimir_LPT1.php?&amp;id=<?=$rowPedido['fldPedidoId']?>','350','100');></a></td>							
<?							}else{
?>								<td><a class="print" title="imprimir" rel="externo" href="pedido_imprimir_<?=$impressao?>.php?&amp;id=<?=$rowPedido['fldPedidoId']?>"></a></td>							
<?							}
?> 							<td><a href="pedido_nfe_consulta_status,<?=$rowPedido['fldPedidoId']?>" name="nfe_status" class="nfe_status_<?=$nfe_status?> modal" title="<?=$nfe_status?>" rel="780-300"></a></td>
                            <td><a id="modal_transmitir" href="pedido_nfe_transmitir,<?=$rowPedido['fldPedidoId']?>" class="modal <?=$transmitir_class?>" title="Transmitir NFe" rel="780-300"></a></td>
                            <td><a href="pedido_nfe_danfe.php?id=<?=$rowPedido['fldPedidoId']?>" target="_blank" class="<?=$danfe_class?>" title="<?=$danfe_title?>"></a></td>
                            <td><input type="checkbox" name="chk_pedido_nfe_<?=$rowPedido['fldPedidoId']?>" id="chk_pedido_nfe_<?=$rowPedido['fldPedidoId']?>" title="selecionar o registro posicionado"  style="margin-top:0px;" /></td>
                        </tr>
<?                      $linha = ($linha == 'row' )?'dif-row':'row';
						$valorTotal 	+= $totalPedido;
						$valorServico 	+= $rowPedido['fldValorServico'];
						$valorTerceiros	+= $rowPedido['fldValor_Terceiros'];
						$valorProduto 	+= $totalItem;
                   }
?>		 		</tbody>
				</table>
            </div>
       	  	<div class="saldo" style="width:952px;float:right;">
                <p style="padding:5px;width:150px;float:left; margin-left:300px">Total<span style="margin-left:6px width:100px;text-align:right" class="credito"><?=format_number_out($valorTotal)?></span></p>
                <p style="padding:5px;width:150px;float:left">Produtos<span style="margin-left:6px width:100px;text-align:right" class="credito"><?=format_number_out($valorProduto)?></span></p>
                <p style="padding:5px;width:150px;float:left">Servi&ccedil;os<span style="margin-left:6px width:100px;text-align:right" class="credito"><?=format_number_out($valorServico)?></span></p>
                <p style="padding:5px;width:150px;float:left">Terceiros<span style="margin-left:6px width:100px;text-align:right" class="credito"><?=format_number_out($valorTerceiros)?></span></p>
			</div>
            
            <input type="hidden" name="hid_array" id="hid_array" value="<?=urlencode(serialize($id_array))?>" />
            <input type="hidden" name="hid_action" id="hid_action" value="true" />
            
            <div id="table_paginacao" style="width: 962px">
                <div class="table_registro" style="float:left;margin:0;text-align:left; padding-left:10px">
                    <span>Exibindo registros <?=($pagina*$limite-$limite+1).' a '.($pagina*$limite-$limite+$rows)?> do total de <?=$rowsTotal?></span>
           		</div>
                <div style="width:400px; height:25px;float:right;display:table;text-align:right;padding-right:20px">
<?					$paginacao_destino = '?p=pedido_nfe';
					include('paginacao.php');
?>          	</div>
			</div>   
            
			<div id="table_action">
                <ul id="action_button" style="float:left">
                    <li><a class="btn_novo" href="index.php?p=pedido_nfe_novo" title="novo">novo</a></li>
                    <li><input type="submit" name="btn_action" id="btn_alterarnumero"	value="alterar numero"	title="Alterar n&uacute;mero" /></li>
					<li style="display:none"><a id="alterar_numero_verificado" class="modal btn_importar" href="nfe_alterar_numero,<?=$filtro_id?>" rel="520-375" title="alterar numero">alterar</a></li>
					<!--
                    <li><input type="submit" name="btn_action" id="btn_excluir" 	value="excluir" 	title="Excluir registro(s) selecionado(s)" onclick="return confirm('Deseja excluir NFe(s) selecionada(s)?\nLembre-se, se houver vendas comuns que foram importadas para NFe, elas tamb&eacute;m ser&atilde;o exclu&iacute;das!')" /></li>
                    <li><input type="submit" name="btn_action" id="btn_exportar" 	value="exportar" 	title="Exportar registro(s) selecionado(s)" /></li>
                    -->
                    <li><input type="submit" name="btn_action" id="btn_desfazer" 	value="desfazer importa&ccedil;&atilde;o" title="Desfazer NFe(s) importada(s)" onclick="return confirm('Deseja desfazer NFe(s) selecionada(s)?\nObs.: Apenas NFe(s) originada(s) de vendas comuns ser&aacute;(&atilde;o) desfeita(s)!')" /></li>
                	<li><input type="submit" name="btn_action" id="btn_excluir" 	value="excluir"		title="Excluir registro(s) selecionado(s)" onclick="return confirm('Deseja excluir os registros selecionados?')" /></li>
                    <li><input type="submit" name="btn_action" id="btn_download" 	value="download"	title="Fazer download de registro(s) selecionado(s)"/></li>
                    <li><button 			 name="btn_action" id="btn_print" 		value="imprimir"	title="Imprimir relat&oacute;rio de registro(s) selecionado(s)" ></button></li>					
                </ul>
        	</div>    
        </div>
	</form>
        
	<script type="text/javascript">
	
		$('a#modal_transmitir').click(function(){
			setTimeout(fnc_nfe_consulta, 20000);
			//fnc_nfe_consulta();
		});
		
	</script>	
    