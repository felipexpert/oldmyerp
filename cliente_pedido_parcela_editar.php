
<div id="principal">
<?
	$cliente_id = $_GET["id"];
	$parcela_id = $_GET["filtro"];
	$rsParcela = mysql_query("SELECT tblpedido_parcela.*,
							 SUM(tblpedido_parcela_baixa.fldValor* (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) AS fldValorBaixa,
							 SUM(tblpedido_parcela_baixa.fldDesconto * (tblpedido_parcela_baixa.fldExcluido * -1 + 1)) AS fldValorDesconto 
							 FROM tblpedido_parcela LEFT JOIN tblpedido_parcela_baixa ON tblpedido_parcela_baixa.fldParcela_Id = tblpedido_parcela.fldId
							 WHERE tblpedido_parcela.fldId IN($parcela_id) GROUP BY tblpedido_parcela.fldId ORDER BY tblpedido_parcela.fldVencimento");
	
	echo mysql_error();
	if($_POST['btn_action']){
		require("cliente_pedido_parcela_action.php");
	}
	
	$remote_ip = gethostbyname($REMOTE_ADDR);
	$_SESSION['ref_timestamp'] = $remote_ip.date("YmdHis");

?>	<h3>Parcela baixa</h3>
   	<form class="table_form" id="frm_parcela_editar" name="frm_parcela" action="" method="post">
    	<div id="table">
            <div id="table_cabecalho">
                <ul class="table_cabecalho">
					<li style="width:30px; text-align:center">Parc.</li>
					<li style="width:35px; text-align:right">Venda</li>
					<li style="width:70px; text-align:center">Vencnto</li>
					<li style="width:60px; text-align:right">Valor</li>
					<li style="width:60px; text-align:right">Receber</li>
					<li style="width:60px; text-align:right">Recebido</li>
					<li style="width:50px; text-align:right">Juros</li>
					<li style="width:45px; text-align:center">Multa</li>
					<li style="width:60px; text-align:right">Desc.(R$)</li>
					<li style="width:70px; text-align:center">Baixa</li>
					<li style="width:50px; text-align:right">Devedor</li>
					<li style="width:70px; text-align:center">Pagto</li>
					<li style="width:130px">Observa&ccedil;&atilde;o</li>
					<li style="width:90px;">Conta</li>
                </ul>
            </div>
            <div id="table_container">       
				<table id="table_general" class="table_general baixa_parcela" summary="Lista de parcelas do cliente">
                <tbody>
<?			
				$linha = "row";
				$juros_tipo = $_SESSION["sistema_pedido_juros_tipo"];
				$multa_tipo = $_SESSION["sistema_pedido_multa_tipo"];
				
				$juros = fnc_sistema("pedido_juros");
				$multa = fnc_sistema("pedido_multa");
				while($rowParcela = mysql_fetch_array($rsParcela)){
					
					$pedidosFiltro .= $rowParcela['fldPedido_Id'].'+';
					$x += 1;
					//juros
					$valorBaixa 	= $rowParcela['fldValor'] - ($rowParcela['fldValorBaixa'] + $rowParcela['fldValorDesconto']);// subtrai as baixas
					//puxa a multa apenas se tiver atrasado
					$multa			= ($rowParcela['fldVencimento'] < date("Y-m-d"))? $multa : '0.00';
					$valor_multa 	= ($rowParcela['fldVencimento'] < date("Y-m-d"))? $multa : '0.00';
					
					$rowDevedor = mysql_fetch_array(mysql_query("SELECT fldDevedor, fldDataRecebido FROM tblpedido_parcela_baixa WHERE fldExcluido = '0' and fldParcela_Id =".$rowParcela['fldId']." ORDER BY fldId DESC LIMIT 1"));
					//verifica se esta atrasada
					if(($rowParcela['fldVencimento'] < date("Y-m-d")) && ($juros > 0)){
						
						$valorRecebido = $rowParcela['fldValorBaixa'];
						//at� a versao 110914 usava dessa maneira abaixo, depois inserimos o campo 'devedor', entao as parcelas dali em diante nao precisam dess
						if($rowDevedor['fldDevedor'] == 0 && $valorBaixa > 0){
							//pega a ultima baixa para poder calcular o juros 
							$rsBaixas = mysql_query("SELECT * FROM tblpedido_parcela_baixa WHERE fldParcela_Id = ".$rowParcela['fldId']." AND fldExcluido = 0");
							while($rowBaixas = mysql_fetch_array($rsBaixas)){
								//pega o juros do que ja foi pago, ou seja 
								//de 50,00, no dia 10/10 ficou para 60,00 por causa do juros, o cliente pagou s� 50,00 - entao sobrou 10 de juros que � desse 50 que ele ja pagou, 
								//por isso faz a conta pra pegar esse juros que nao foi pago
								$dias = intervalo_data($rowParcela['fldVencimento'], $rowBaixas['fldDataRecebido']);
								$valorBaixas = $rowBaixas['fldValor'];
								
								$juros_ad = ($juros / 30) * $dias; // pega o juros ao mes, e divide pelo numero de dias que esta atrasado
								$valor_juros = ($juros_ad * $valorBaixas) / 100; // valor do juros calculado com o ultimo valor da parcela, o valor total antes da ultima baixa
								$valorJurosBaixas += $valor_juros;
							}
							
							//verifica se tem baixa, pega data da ul�tima baixa, se nao pega data de vencimento
							if($rowDevedor['fldDataRecebido'] > 0){
								$dataJuros = $rowDevedor['fldDataRecebido'];
							}else{
								$dataJuros = $rowParcela['fldVencimento'];
							}
							
							$valorJurosBaixas = number_format($valorJurosBaixas, 2, '.', ''); 		// valor de juros das baixas anteriores
							$dias 			= intervalo_data($dataJuros, date("Y-m-d")); 			// conta o juros a partir da ultima baixa
							$juros_ad 		=  number_format((($juros / 30) * $dias), 2, '.', ''); 	// pega o juros ao mes, e divide pelo numero de dias que esta atrasado
							
							$valor_receber 	= $valorJurosBaixas + $rowParcela['fldValor'] - $rowParcela['fldValorDesconto'];
							$devedor 		= $valor_receber - $rowParcela['fldValorBaixa'];
							
							$valor_juros 	= number_format((($juros_ad * $devedor) / 100), 2, '.', ''); //juros calculado de acordo com o devedor j� com os juros atradasos
							
							if($multa_tipo == '2'){ // 1= reais, 2=porcentagem
								$valor_multa	=  number_format((($multa * $devedor) / 100), 2, '.', '');
							}
							$valor_baixa 	= $devedor + $valor_juros;
							$valorRecebido 	= $rowParcela['fldValorBaixa'];
							
							//##########################################################################################################################################
						}elseif($rowDevedor['fldDevedor'] > 0){
							$sql 		= mysql_query("SELECT SUM(fldJuros) as fldJuros, SUM(fldMulta) as fldMulta FROM tblpedido_parcela_baixa WHERE fldParcela_Id =".$rowParcela['fldId']." AND fldExcluido = 0");
							$rowJuros 	= mysql_fetch_array($sql);
							$devedor 		= $rowDevedor['fldDevedor'];
							$dias 			= intervalo_data($rowDevedor['fldDataRecebido'], date("Y-m-d"));
							$juros_ad 		= number_format((($juros / 30) * $dias), 2, '.', ''); 			// pega o juros ao mes, e divide pelo numero de dias que esta atrasado
							$valor_juros 	= number_format((($juros_ad * $devedor) / 100), 2, '.', '');	 // valor do juros calculado com o ultimo valor da parcela, o valor total antes da ultima baixa
							
							$total_receber 	= $devedor + $rowJuros['fldJuros'] + $rowJuros['fldMulta'];
							$valor_receber 	= $valor_juros + $total_receber + $rowParcela['fldValorBaixa'];
							
							if($multa_tipo == '2'){ // 1= reais, 2=porcentagem
								$valor_multa	=  number_format((($multa * $devedor) / 100), 2, '.', '');
							}
							$valorRecebido 	= $rowParcela['fldValorBaixa'] + $rowJuros['fldJuros'] + $rowJuros['fldMulta'];
							$valor_baixa 	= $devedor + $valor_juros;
						}
						
						//verifica se o juros eh composto
						$juros_tipo = fnc_sistema('pedido_juros_tipo');
						if($juros_tipo == 2) /*(composto)*/{
							
							$valor_juros = number_format((($juros_ad * $valor_baixa) / 100), 2, '.', '');
							$valor_baixa = number_format(($valor_baixa + $valor_juros), 2, '.', '');
						}
					}else{
						
						$juros 			= '0.00';
						$juros_ad 		= '0.00';
												
						$valor_receber 	= $valorBaixa;
						$valor_baixa 	= $valorBaixa;
						$devedor 		= $valorBaixa;
						$valorRecebido 	= $rowParcela['fldValor'] - $valorBaixa;
					}
				
					$rsPagamento 	= mysql_query("SELECT * FROM tblpagamento_tipo WHERE fldId = ".$rowParcela['fldPagamento_Id']);
					$rowPagamento	= mysql_fetch_array($rsPagamento);
					$parcelaConta 	= fnc_sistema("pedido_parcela_conta_".$rowPagamento['fldSigla']);
					
					//echo $valor_baixa;
					
					$valor_baixa = $valor_baixa + $valor_multa;
					
?>					<tr  id="parcelas" class="<?= $linha; ?> ">
						<td style="width:30px; text-align:center" title="parcela"><?=$rowParcela['fldParcela']?></td>
						<td style="width:35px; text-align:right"><a href="index.php?p=pedido_detalhe&id=<?=$rowParcela['fldPedido_Id']?>" title="ir para venda"><?= str_pad($rowParcela['fldPedido_Id'], 5, "0", STR_PAD_LEFT)?></a></td>
						<td style="width:70px; text-align:center" class="cod" title="vencimento"><?=format_date_out3($rowParcela['fldVencimento'])?></td>
						<td style="width:60px; text-align:right" class="total" title="valor original da parcela"><?=format_number_out($rowParcela['fldValor'])?></td>
                        <td style="width:60px; text-align:right; color: #963" class="total" title="total a pagar"><?=format_number_out($valor_receber)?></td>
                        <td style="width:60px; text-align:right" class="total" title="recebido total"><?=format_number_out($valorRecebido)?></td>
                        
                        <input type="hidden" name="hid_valor_recebido_<?=$rowParcela['fldId']?>"id="hid_valor_recebido_<?=$rowParcela['fldId']?>" 	value="<?=$rowParcela['ValorBaixa']?>" />
                        <input type="hidden" name="hid_valor_parcela_<?=$rowParcela['fldId']?>" id="hid_valor_parcela_<?=$rowParcela['fldId']?>" 	value="<?=$rowParcela['fldValor']?>" />
                        <input type="hidden" name="hid_baixa_valor_<?=$rowParcela['fldId']?>"	id="hid_baixa_valor_<?=$rowParcela['fldId']?>" 		value="<?=$devedor?>" />
                        <input type="hidden" name="hid_juros_<?=$rowParcela['fldId']?>" 		id="hid_juros_<?=$rowParcela['fldId']?>" 			value="<?=$juros_ad?>" />
						<input type="hidden" name="hid_multa_<?=$rowParcela['fldId']?>"			id="hid_multa_<?=$rowParcela['fldId']?>" 			value="<?=$multa?>" />
						<input type="hidden" name="hid_baixa_valor_calculado_<?=$rowParcela['fldId']?>" id="hid_baixa_valor_calculado_<?=$rowParcela['fldId']?>" value="<?=format_number_out($valor_baixa)?>" />
                        
						<td style="width:43px; text-align:right"><input style="width:48px;background:#FDEEEE" 	class="form_baixa txt_juros" 	type="text" name="txt_juros_<?=$rowParcela['fldId']?>" 		 id="txt_juros_<?=$rowParcela['fldId']?>" 		value="<?=format_number_out($juros_ad)?>" title="Juros" /></td>
						<td style="width:45px; text-align:right"><input style="width:43px;background:#FDEEEE" 	class="form_baixa txt_multa" 	type="text" name="txt_multa_<?=$rowParcela['fldId']?>" 		 id="txt_multa_<?=$rowParcela['fldId']?>" 		value="<?=format_number_out($multa)?>" title="Multa" /></td>
						<td style="width:60px; text-align:right"><input style="width:58px;background:#E0FEF5" 	class="form_baixa txt_desconto" type="text" name="txt_desconto_<?=$rowParcela['fldId']?>" 	 id="txt_desconto_<?=$rowParcela['fldId']?>"	value="0,00" title="Desconto da parcela" /></td>
						<td style="width:70px; text-align:right"><input style="width:68px;background:#FF9" 		class="form_baixa valor_baixa" 	type="text" name="txt_valor_baixa_<?=$rowParcela['fldId']?>" id="txt_valor_baixa_<?=$rowParcela['fldId']?>"	value="<?=format_number_out($valor_baixa)?>" title="Valor da baixa" /></td>
						<td style="width:50px; text-align:right"><input style="width:48px" 						class="form_baixa txt_devedor" 	type="text" name="txt_valor_devedor_<?=$rowParcela['fldId']?>" 	value="0,00" title="Valor devedor" readonly="readonly" /></td>
                        <td style="width:70px; text-align:right">
                        	<select class="form_baixa" style="width:70px" id="sel_pagamento_tipo_<?=$rowParcela['fldId']?>"	name="sel_pagamento_tipo_<?=$rowParcela['fldId']?>" title="Forma de Pagamento">
<?								$rsPagamento = mysql_query("SELECT * FROM tblpagamento_tipo WHERE fldExcluido = 0 ORDER BY fldTipo");
								while($rowPagamento= mysql_fetch_array($rsPagamento)){
?>									<option style="text-align:left" <?=($rowPagamento['fldId'] == $rowParcela['fldPagamento_Id']) ? 'selected="selected"' : '' ?> value="<?= $rowPagamento['fldId'] ?>"><?= $rowPagamento['fldTipo'] ?></option>
<?								}
?>              	      	</select>
                        </td>                    
						<td style="width:130px; text-align:left"><input class="form_baixa" style="width:128px;text-align: left" type="text" name="txt_observacao_<?=$rowParcela['fldId']?>" value="<?=$rowParcela['fldObservacao']?>" title="observa&ccedil;&atilde;o" /></td>
						<td style="width:100px; text-align:left">
                        	<select class="form_baixa" style="width:100px" id="sel_conta_<?=$rowParcela['fldId']?>" name="sel_conta_<?=$rowParcela['fldId']?>" title="Creditar na conta">
<?								$rsConta = mysql_query("select * from tblfinanceiro_conta");
								while($rowConta = mysql_fetch_array($rsConta)){
?>									<option <?=($parcelaConta == $rowConta['fldId'])? "selected='selected'" :'' ?> style="text-align:left" value="<?=$rowConta['fldId']?>"><?=$rowConta['fldNome'] ?></option>
<?								}
?>              	      	</select> 
                        </td>                       
					</tr>
<?					$linha = ($linha == "row" ? "dif-row" : "row");
					
					//totais
					$valor_total 		+= $rowParcela['fldValor'];
					$baixa_valor_total 	+= $valor_baixa;
					$totalReceber 		+= $valor_receber;
				}
				$pedidosFiltro = substr($pedidosFiltro, 0 ,strlen($pedidosFiltro) - 1);	
?>		 		</tbody>
				</table>
                
<?
				#BUSCO O CREDITO SE HOUVER, DESCONTANDO O QUE JA FOI UTILIZADO
				$credito_total 		= fnc_parcela_credito($cliente_id, 'venda');
				$credito_utilizado 	= fnc_parcela_credito_utilizado($cliente_id, 'venda');
				$credito_disponivel = number_format($credito_total - $credito_utilizado, 2, '.', '');
?>                
                <input type="hidden" name="hid_juros_tipo" 		id="hid_juros_tipo" 	value="<?=$juros_tipo?>" />
                <input type="hidden" name="hid_multa_tipo" 		id="hid_multa_tipo"		value="<?=$multa_tipo?>" />
                <input type="hidden" name="hid_credito_disponivel"	id="hid_credito_disponivel"	value="<?=$credito_disponivel?>" />
            </div>
			<div id="table_cabecalho">
                <ul class="table_cabecalho">
					<li style="width:80px; text-align:right">Recebido em</li>
					<li style="width:80px; padding-top:2px"><input style="width:80px;margin:0px;text-align:center;background:#FBDAC6" class="calendario-mask form_baixa" type="text" name="txt_data_recebido" value="<?=date("d/m/Y")?>" title="data de recebimento" /></li>
					<li style="width:280px;">&nbsp;</li>
					<li style="width:60px; text-align:right"><span>Parcelas</span></li>
					<li style="width:60px; text-align:right"><input class="form_baixa_total" type="text" name="txt_total" id="txt_total" value="<?=format_number_out($valor_total)?>" readonly="readonly" /></li>
					<li style="width:75px; text-align:right"><span>A receber</span></li>
					<li style="width:60px; text-align:right; margin: 0 20px 0 0;"><input class="form_baixa_total" type="text" name="txt_receber" id="txt_receber" value="<?=format_number_out($baixa_valor_total)//format_number_out($totalReceber)?>" readonly="readonly" /></li>
                    <li style="width:80px; text-align:right"><span>Valor baixa</span></li>
					<li style="width:80px; text-align:right; padding-top: 3px;"><input style="width:80px;margin:0px;text-align:right;background:#ffff99;" class="form_baixa" type="text" name="txt_total_baixa" id="txt_total_baixa" value="<?=format_number_out($baixa_valor_total)?>" /></li>
                </ul>
            </div>
       	  
            <input type="hidden" name="hid_filtro" 		id="hid_filtro" value="<?=$_GET['filtro']?>" />
            <input type="hidden" name="hid_action" 		id="hid_action" value="true" />
            <input type="hidden" name="hid_cliente_id"	id="hid_cliente_id" value="<?=$_GET['id']?>" />
            
			<div id="table_action">
                <ul id="action_button">
                	<li><input type="submit" class="btn_general" name="btn_action" id="btn_baixa" value="confirmar" title="Dar baixa em registro(s) selecionado(s)" /></li>
                	<li><a href="index.php?p=cliente_detalhe&id=<?=$_GET["id"]?>&modo=pedido_parcela" class="btn_cancel">cancelar</a></li>
                    <li><a style="margin:0" class="modal btn_cheque" name="btn_cheque" id="btn_cheque" title="cheques" href="financeiro_cheque_listar,3,<?=$pedidosFiltro?>" rel="780-410">exibir cheques</a></li>
				</ul>
        	</div>

        </div>
	</form>
