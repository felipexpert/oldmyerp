<?	

	##############################################################################################################
	############################################## REQUIRES ######################################################
	require("inc/relatorio.php");
	require("inc/con_db.php");
	require("inc/fnc_general.php");
	
	$pdf = new PDF('P', 'mm', 'A4');
	//pode ser P (portrait) ou L (landscape)
	
	$pdf->AliasNbPages();
	//isso faz com que funcione o total de paginas
	
	$pdf->SetMargins(11, 5);
	//defino as margens do documento...
	
	$pdf->AddPage();
	//comando para add a pagina
	
	//cabeçalho
	session_start();
	$dados_empresa 	= mysql_fetch_assoc(mysql_query("SELECT * FROM tblempresa_info"));
	$usuario 		= mysql_fetch_assoc(mysql_query("SELECT * FROM tblusuario WHERE fldId = '".$_SESSION['usuario_id']."'"));
	$usuario 		= $usuario['fldUsuario'];
	
	$pdf->SetFont('Arial', '', 8);
	$pdf->Cell(50);
	$pdf->Cell(60, 5, "nome da empresa: {$dados_empresa['fldNome_Fantasia']} (".format_cnpj_out($dados_empresa['fldCPF_CNPJ']).")", 0, 0, 'R');
	$pdf->Cell(30, 5, "usuario: $usuario", 0, 0, 'R');
	$pdf->Cell(50, 5, "gerado em: ".date('d/m/Y h:i:s'), 0, 0, 'R');
	$pdf->Ln(6);
		
	$tabela_id 	= $_GET['tabela'];
	$marca 		= (!isset($_GET['fornecedor']) or $_GET['marca'] == '0') ? "" : "where tblproduto.fldFornecedor_Id = '".$_GET['fornecedor']."'";

	if($tabela_id == 0){

		$sql = "SELECT 
					tblproduto.fldCodigo,
					tblproduto.fldNome as fldNomeProduto,
					tblmarca.fldNome as fldNomeMarca,
					tblcategoria.fldNome as fldNomeCategoria,
					tblproduto.fldValorVenda
				FROM tblproduto 
				LEFT JOIN tblmarca ON tblproduto.fldMarca_Id = tblmarca.fldId 
				LEFT JOIN tblcategoria ON tblproduto.fldCategoria_Id = tblcategoria.fldId
				$marca
				ORDER BY tblproduto.fldNome ASC";
				
		$nome_tbl = "Comum";
			
	} else {
	
		$sql_tabela = "SELECT * FROM tblproduto_tabela WHERE fldId = '$tabela_id'";
		$sql_tabela = mysql_fetch_array(mysql_query($sql_tabela));
		$acr_tipo	= $sql_tabela['fldAcrescimo_Tipo'];
		$modo		= ($sql_tabela['fldModo'] == '1') ? 'fldValorCompra' : 'fldValorVenda';
		
		$acr 		= $sql_tabela['fldAcrescimo'];
		$nome_tbl	= $sql_tabela['fldNome'];
		
		if($acr_tipo == '1'){ #porcentagem
		
			$complementar = "tblproduto.$modo + (tblproduto.$modo * ($acr / 100))";
		
		} else { #dinheiro

			$complementar = "tblproduto.$modo + $acr";
			
		}
		
		$sql = "SELECT 
			tblproduto.fldCodigo,
			tblproduto.fldNome as fldNomeProduto,
			tblmarca.fldNome as fldNomeMarca,
			tblcategoria.fldNome as fldNomeCategoria,
			($complementar) as fldValorVenda
		FROM tblproduto 
		LEFT JOIN tblmarca ON tblproduto.fldMarca_Id = tblmarca.fldId 
		LEFT JOIN tblcategoria ON tblproduto.fldCategoria_Id = tblcategoria.fldId
		$marca
		ORDER BY tblproduto.fldNome ASC";

	}
	
	######################### TITULO DO RELATORIO #####################################################
	$pdf->SetTextColor(255, 255, 255);
	$pdf->SetFillColor(0, 0, 0);
	$pdf->SetFont('Arial', 'B', 14);
	$pdf->Cell(130, 8, "Tabela de Preço", 1, 0, 'L', true);
	$pdf->SetFont('Arial', '', 8);
	$pdf->Cell(60, 8, "Tabela selecionada: $nome_tbl", 1, 0, 'L', true);
	$pdf->SetTextColor(0, 0, 0); //ja reseto a cor da fonte para nao ter problema
	$pdf->Ln();
	###################################################################################################
	
	$rsRelatorio = mysql_query($sql);
	
	$pdf->SetFillColor(220, 220, 220);
	$pdf->SetTextColor(108, 108, 108);
	$pdf->Cell(20, 8, 'codigo', 1, 0, 'C', true);
	$pdf->Cell(71, 8, 'nome', 1, 0, 'L', true);
	$pdf->Cell(37, 8, 'categoria', 1, 0, 'L', true);
	$pdf->Cell(37, 8, 'fornecedor', 1, 0, 'L', true);
	$pdf->Cell(25, 8, 'valor', 1, 0, 'R', true);
	$pdf->Ln();
	
	$pdf->SetFont('Arial', '', 12);
	$pdf->SetFillColor(255, 255, 255);
	$pdf->SetTextColor(0, 0, 0);
	
	while($rowProduto = mysql_fetch_array($rsRelatorio)){
	
		$pdf->Cell(20, 8, $rowProduto['fldCodigo'], 1, 0, 'C', true);
		$pdf->Cell(71, 8, mb_substr($rowProduto['fldNomeProduto'], 0, 80), 1, 0, 'L', true);
		$pdf->Cell(37, 8, mb_substr($rowProduto['fldNomeCategoria'], 0, 25), 1, 0, 'L', true);
		$pdf->Cell(37, 8, mb_substr($rowProduto['fldNomeMarca'], 0, 25), 1, 0, 'L', true);
		$pdf->Cell(25, 8, format_number_out($rowProduto['fldValorVenda']), 1, 0, 'R', true);
		$pdf->Ln();
	
	}

	$pdf->Output();

?>