<div id="voltar">
    <p><a href="index.php?p=financeiro_contas_pagar&modo=programada">n&iacute;vel acima</a></p>
</div>	

<h2>Editar conta programada</h2>

<div id="principal">
<?	
	
	$conta_programada_id = $_GET['id'];
	
	//SELECIONANDO O REGISTRO A PARTIR DO ID
	$rsContaProgramada  = mysql_query("SELECT * FROM tblfinanceiro_conta_pagar_programada WHERE fldId = '" . $conta_programada_id . "'");
	$rowContaProgramada = mysql_fetch_array($rsContaProgramada);
	
	//SELECIONANDO O TIPO DE INTERVALO PARA EXIBIR NO FORMUL�RIO
	$rsTipoFrequencia  = mysql_query("SELECT fldId, fldNome FROM tblsistema_calendario_intervalo");
	
	//SELECIONANDO OS MARCADORES PARA EXIBIR NO FORMUL�RIO
	$rsMarcador  = mysql_query("SELECT * FROM tblfinanceiro_conta_fluxo_marcador");
	
	//SELECIONANDO O TIPO DE PAGAMENTO PARA EXIBIR NO FORMUL�RIO
	$rsPagamento  = mysql_query("SELECT * FROM tblpagamento_tipo");
	
	//CHECANDO SE A DATA DE T�RMINO � V�LIDA
	$dataDividida 	= explode('-',$rowContaProgramada['fldData_Termino']);
	$dataValida		= checkdate($dataDividida[1], $dataDividida[2], $dataDividida[0]);
	($dataValida) ? $class = 'visible' : $class = 'hidden';
	
	//ATUALIZAR O REGISTRO
	if(isset($_POST["btn_enviar"])){
		
		$sqlUpdate = "update tblfinanceiro_conta_pagar_programada set
		fldNome = '" . mysql_real_escape_string($_POST['txt_nome']) . "', 
		fldDescricao = '" . mysql_real_escape_string($_POST['txt_descricao']) . "',
		fldMarcador = '" . $_POST['sel_marcador'] . "',
		fldIntervalo_Tipo = '" . $_POST['sel_tipo_frequencia'] . "',
		fldIntervalo_Frequencia = '" . (($_POST['txt_intervalo'] == '' or $_POST['txt_intervalo'] == 0) ? 1 : $_POST['txt_intervalo']) . "',
		fldData_Inicio = '" . format_date_in($_POST['txt_data_inicio']) . "',
		fldData_Termino = '" . (($_POST['sel_data_termino']) ? format_date_in($_POST['txt_data_termino']) : '0000-00-00') . "',
		fldPagamento_Id = '" . $_POST['sel_pagamento'] . "',
		fldValor = '" . format_number_in($_POST['txt_valor']) . "'
		where fldId = " . $conta_programada_id;
		
		if (mysql_query($sqlUpdate)){
			header("location:index.php?p=financeiro_contas_pagar&modo=programada&mensagem=ok");
		}
		else {
?>			<div class="alert">
				<p class="erro">N&atilde;o foi poss&iacute;vel gravar os dados</p>
				<a class="voltar" href="javascript:history.back();">voltar</a>
			</div>
<?			
		}
	}
	else{
?>		<div class="form">
            <form class="frm_detalhe" id="frm_conta_programada" action="" method="post">
                <ul>
                    <li>
                        <label for="txt_codigo">C&oacute;digo</label>
                        <input type="text" style="width:50px; text-align: center;" id="txt_codigo" name="txt_codigo" value="<?=$conta_programada_id?>" readonly="readonly"/>
                    </li>
                    <li>
						<label for="txt_nome">Nome</label>
						<input style="width: 215px;" type="text" id="txt_nome" name="txt_nome" value="<?=$rowContaProgramada['fldNome']?>" />
					</li>
					<li>
						<label for="txt_descricao">Descri&ccedil;&atilde;o</label>
						<input style="width: 440px;" type="text" id="txt_descricao" name="txt_descricao" value="<?=$rowContaProgramada['fldDescricao']?>" />
					</li>
					<li>
						<label for="sel_marcador">Marcador</label>
						<select id="sel_marcador" name="sel_marcador">
							<? while($rowMarcador = mysql_fetch_array($rsMarcador)){ ?>
							<option <?=($rowMarcador['fldId'] == $rowContaProgramada['fldMarcador']) ? ' selected="selected"' : ''?> title="<?=$rowMarcador['fldDescricao']?>" value="<?=$rowMarcador['fldId']?>"><?=$rowMarcador['fldMarcador']?></option>
							<? } ?>
						</select>
					</li>
					<li>
						<label for="sel_tipo_frequencia">Frequ&ecirc;ncia</label>
						<select id="sel_tipo_frequencia" name="sel_tipo_frequencia">
							<? while($rowTipoFrequencia = mysql_fetch_array($rsTipoFrequencia)){ ?>
							<option <?=($rowTipoFrequencia['fldId'] == $rowContaProgramada['fldIntervalo_Tipo']) ? ' selected="selected"' : ''?> value="<?=$rowTipoFrequencia['fldId']?>"><?=$rowTipoFrequencia['fldNome']?></option>
							<? } ?>
						</select>
					</li>
					<li>
						<label for="txt_intervalo">Intervalo</label>
						<input style="width: 100px; text-align: center;" type="text" id="txt_intervalo" name="txt_intervalo" value="<?=$rowContaProgramada['fldIntervalo_Frequencia']?>" />
					</li>
					<li>
						<label for="txt_data_inicio">In&iacute;cio em</label>
						<input style="width: 80px; text-align: center" type="text" class="calendario-mask" id="txt_data_inicio" name="txt_data_inicio" value="<?=format_date_out($rowContaProgramada['fldData_Inicio'])?>" />
						<a href="#" title="Exibir calend&aacute;rio" class="exibir-calendario-data-atual"></a>
					</li>
					<li>
						<label for="sel_data_termino">Termina em</label>
						<select id="sel_data_termino" name="sel_data_termino">
							<option <?=($dataValida) ? '' : 'selected="selected"'?> value="0">N&atilde;o termina</option>
							<option <?=($dataValida) ? 'selected="selected"' : ''?> value="1">Escolher uma data</option>
						</select>
						<input style="width: 100px; margin: 0 0 0 10px;  text-align: center" class="<?=$class?> calendario-mask" type="text" id="txt_data_termino" name="txt_data_termino" value="<?=($dataValida) ? format_date_out($rowContaProgramada['fldData_Termino']) : ''?>" />
						<a href="#" id="hidden-calendar" title="Exibir calend&aacute;rio" class="<?=$class?> exibir-calendario-data-atual"></a>
					</li>
				</ul>
				<ul style="width: 800px;">
					<li>
						<label for="sel_pagamento">Tipo de pagamento</label>
						<select style="width: 125px;" id="sel_pagamento" name="sel_pagamento">
							<?php
							while($rowPagamento = mysql_fetch_array($rsPagamento)): ?>
							<option <?=($rowPagamento['fldId'] == $rowContaProgramada['fldPagamento_Id']) ? ' selected="selected"' : ''?> value="<?= $rowPagamento['fldId'] ?>"><?= $rowPagamento['fldTipo'] ?></option>
							<?php endwhile; ?>
						</select>
					</li>
					<li>
						<label for="txt_valor">Valor aproximado</label>
						<input style="width: 110px; text-align: center" type="text" id="txt_valor" name="txt_valor" value="<?=format_number_out($rowContaProgramada['fldValor'])?>" />
					<li>
				</ul>
				<ul style="float: right;">
					<li><input type="submit" style="margin-top: 12px" class="btn_enviar" name="btn_enviar" id="btn_enviar" value="Salvar" title="Salvar" /></li>
					<li><a style="margin-top: 12px;" class="btn_cancel" href="?p=financeiro_contas_pagar&amp;modo=programada" title="Cancelar opera&ccedil;&atilde;o">Cancelar</a></li>
				</ul>
            </form>
        </div>
<?  } ?>
</div>