<?php
	$filtro = '';

	//memorizar os filtros para exibição nos selects
	if(isset($_POST['btn_limpar'])){
		$_SESSION['txt_fornecedor_id'] 	 = "";
		$_SESSION['txt_nome_fantasia']   = "";
		$_SESSION['txt_razao_social']	 = "";
		$_SESSION['txt_fornecedor_cnpj'] = "";
		$_POST['chk_fornecedor'] 		 =  false;
		$_POST['chk_fornecedor_rs'] 	 =  false;
	}
	else{
		$_SESSION['txt_fornecedor_id']   = (isset($_POST['txt_fornecedor_id'])  ? $_POST['txt_fornecedor_id'] 	 : $_SESSION['txt_fornecedor_id']);
		$_SESSION['txt_nome_fantasia'] 	 = (isset($_POST['txt_nome_fantasia']) 	 ? $_POST['txt_nome_fantasia'] 	 : $_SESSION['txt_nome_fantasia']);
		$_SESSION['txt_razao_social'] 	 = (isset($_POST['txt_razao_social']) 	 ? $_POST['txt_razao_social'] 	 : $_SESSION['txt_razao_social']);
		$_SESSION['txt_fornecedor_cnpj'] = (isset($_POST['txt_fornecedor_cnpj']) ? $_POST['txt_fornecedor_cnpj'] : $_SESSION['txt_fornecedor_cnpj']);
	}

?>
<form id="frm-filtro" action="index.php?p=fornecedor&modo=cadastro" method="post">
	<fieldset>
  	<legend>Buscar por:</legend>
    <ul>
    	<li>
<?			$codigo = ($_SESSION['txt_fornecedor_id'] ? $_SESSION['txt_fornecedor_id'] : "c&oacute;digo");
			($_SESSION['txt_fornecedor_id'] == "código") ? $_SESSION['txt_fornecedor_id'] = '' : '';
?>      	<input style="width: 100px" type="text" name="txt_fornecedor_id" id="txt_fornecedor_id" onfocus="limpar (this,'c&oacute;digo');" onblur="mostrar (this, 'c&oacute;digo');" value="<?=$codigo?>"/>
		</li>
    	<li>
        	<input type="checkbox" name="chk_fornecedor" id="chk_fornecedor" <?=($_POST['chk_fornecedor'] == true ? print 'checked ="checked"' : '')?>/>
<?			$nomeF = ($_SESSION['txt_nome_fantasia'] ? $_SESSION['txt_nome_fantasia'] : "nome fantasia");
			($_SESSION['txt_nome_fantasia'] == "nome fantasia") ? $_SESSION['txt_nome_fantasia'] = '' : '';
?>     		<input style="width: 165px" type="text" name="txt_nome_fantasia" id="txt_nome_fantasia" onfocus="limpar (this,'nome fantasia');" onblur="mostrar (this, 'nome fantasia');" value="<?=$nomeF?>"/>
			<small>marque para qualquer parte do campo</small>
        </li>
        <li>
        	<input type="checkbox" name="chk_fornecedor_rs" id="chk_fornecedor_rs" <?=($_POST['chk_fornecedor_rs'] == true ? print 'checked ="checked"' : '')?>/>
<?			$razaoS = ($_SESSION['txt_razao_social'] ? $_SESSION['txt_razao_social'] : "raz&atilde;o social");
			($_SESSION['txt_razao_social'] == "razão social") ? $_SESSION['txt_razao_social'] = '' : '';
?>     		<input style="width: 165px" type="text" name="txt_razao_social" id="txt_razao_social" onfocus="limpar (this,'raz&atilde;o social');" onblur="mostrar (this, 'raz&atilde;o social');" value="<?=$razaoS?>"/>
			<small>marque para qualquer parte do campo</small>
        </li>
        <li>
<?			$cnpj = ($_SESSION['txt_fornecedor_cnpj'] ? $_SESSION['txt_fornecedor_cnpj'] : "CPF/CNPJ");
			($_SESSION['txt_fornecedor_cnpj'] == "CPF/CNPJ") ? $_SESSION['txt_fornecedor_cnpj'] = '' : '';
?>     		<input style="width: 120px" type="text" name="txt_fornecedor_cnpj" id="txt_fornecedor_cnpj" onfocus="limpar (this,'CPF/CNPJ');" onblur="mostrar (this, 'CPF/CNPJ');" value="<?=$cnpj?>"/>
			<small style="margin-left: 40px"> *apenas n&uacute;meros</small>
		</li>
        <li>
        	<button type="submit" name="btn_limpar" title="Limpar Filtro">Limpar filtro</button>
        </li>
        <li>
	        <button type="submit" name="btn_exibir" title="Exibir">Exibir</button>
        </li>
    </ul>
  </fieldset>
</form>

<?
	/** inicializando a string sql para realizar a consulta ($filtro)
	 * assim sempre exitirá a clásula where, mesmo sendo irrelevante neste ponto
	 */
	
	$filtro = 'WHERE fldId > 0 ';

	if(($_SESSION['txt_fornecedor_id']) != ""){
		
		$filtro .= "and fldId = '".$_SESSION['txt_fornecedor_id']."'";
	}

	if(($_SESSION['txt_nome_fantasia']) != ""){
		$fornecedor = addslashes($_SESSION['txt_nome_fantasia']); // no caso de aspas, pra nao dar erro na consulta
		if($_POST['chk_fornecedor'] == true){
			$filtro .= "and fldNomeFantasia like '%".$fornecedor."%'";
		}else{
			$filtro .= "and fldNomeFantasia like '".$fornecedor."%'";
		}
	}
	
	if(($_SESSION['txt_razao_social']) != ""){
		$fornecedor = addslashes($_SESSION['txt_razao_social']); // no caso de aspas, pra nao dar erro na consulta
		if($_POST['chk_fornecedor_rs'] == true){
			$filtro .= "and fldRazaoSocial like '%".$fornecedor."%'";
		}else{
			$filtro .= "and fldRazaoSocial like '".$fornecedor."%'";
		}
	}
	
	if(($_SESSION['txt_fornecedor_cnpj']) != ""){
		
		$filtro .= "and fldCPF_CNPJ = '".$_SESSION['txt_fornecedor_cnpj']."'";
	}
			

	//transferir para a sessão
	if(isset($_POST['btn_exibir'])){
		$_SESSION['filtro_fornecedor'] = $filtro;
	}
	elseif(isset($_POST['btn_limpar'])){
		$_SESSION['filtro_fornecedor'] = "";
	}

?>
