<div id="voltar">
    <p><a href="index.php?p=financeiro&modo=conta_fluxo">n&iacute;vel acima</a></p>
</div>	

<h2>Transfer&ecirc;ncia de conta</h2>

<div id="principal">
<?	
	
	//ações
	if(isset($_POST["btn_enviar"])){
		
		$conta = $_POST['sel_conta'];
		$valor = format_number_in($_POST['txt_valor']);
		
		$data1 = fnc_caixa_data($conta);
		$data2 = fnc_caixa_data($_SESSION['sel_conta_id']);
		
		$hora = date("H:i:s");
		
		$pagamento_tipo = $_POST['sel_pagamento'];
	
		//tipo de movimentação (por id)
		//1 = Saldo Inicial
		//2 = Transferência de conta
		//3 = Recebimento de parcela
		//4 = Pagamento de parcela
		//5 = Movimento caixa
		
		$movimentacao_tipo = '2';
	
		$sqlInsertCredito = mysql_query("insert into tblfinanceiro_conta_fluxo
		(fldData, fldHora, fldUsuario_Id, fldCredito, fldConta_Id, fldConta_Origem, fldPagamento_Tipo_Id, fldMovimento_Tipo)
		values(
		'" . $data1 . "',
		'" . $hora . "',
		'" . $_SESSION['usuario_id'] . "',
		'" . $valor . "',
		'" . $conta . "',
		'" . $_SESSION['sel_conta_id'] . "',
		'".$pagamento_tipo."',
		'".$movimentacao_tipo."'
		)");
		
		echo mysql_error();
		
		$sqlInsertDebito = mysql_query("insert into tblfinanceiro_conta_fluxo
		(fldData, fldHora, fldUsuario_Id, fldDebito, fldConta_Id, fldConta_Destino, fldPagamento_Tipo_Id, fldMovimento_Tipo)
		values(
		'" . $data2 . "',
		'" . $hora . "',
		'" . $_SESSION['usuario_id'] . "',
		'" . $valor . "',
		'" . $_SESSION['sel_conta_id'] . "',
		'" . $conta . "',
		'".$pagamento_tipo."',
		'".$movimentacao_tipo."'
		)");
		
		echo mysql_error();
	}
	
	if($sqlInsertDebito){
		header("location: index.php?p=financeiro&mensagem=transferencia");
	}	
	
?>	<div class="form">
        <form class="frm_detalhe" style="float:left" id="frm_financeiro_transferencia" action="" method="post">
            <ul>
                <li>
                    <label for="txt_valor">Valor</label>
                    <input type="text" style="width:200px" id="txt_valor" name="txt_valor" value="" />
                </li> 
                <li>
                	<label for="sel_pagamento">Forma pag.</label>
					<select style="width:200px" id="sel_pagamento" name="sel_pagamento" >
<?						$rsPagamento = mysql_query("select * from tblpagamento_tipo");
                        while($rowPagamento= mysql_fetch_array($rsPagamento)){
?>							<option value="<?=$rowPagamento['fldId'] ?>"><?= $rowPagamento['fldTipo']?></option>
<?						}
?> 					</select>
            	</li>
            	<li>
                	<label for="sel_conta">Mover para conta</label>
					<select style="width:180px" id="sel_conta" name="sel_conta" >
<?						$rsConta = mysql_query("select * from tblfinanceiro_conta where fldId !=".$_SESSION['sel_conta_id']);
                        while($rowConta= mysql_fetch_array($rsConta)){
?>							<option value="<?=$rowConta['fldId'] ?>"><?=$rowConta['fldNome'] ?></option>
<?						}
?> 					</select>
            	</li>
                <li style="float:right; margin-right:10px; margin-top:0">
                    <input type="submit" style="margin-top:16px" class="btn_enviar" name="btn_enviar" id="btn_enviar" value="gravar" title="Gravar" />
                </li>
             </ul>
        </form>
	</div>
</div>

<script type="text/javascript">
	
	/*
	 *	Não deixar fazer transfrência com valores negativos
	 */
	$('#txt_valor').change(function() {
		checarValorNegativo($(this));
	});
	
	function checarValorNegativo(seletor) {
		if(br2float($(seletor).val()) < 0) {
			alert('Você não pode realizar uma transferência com valores negativos!');
			$(seletor).val('0,00');
			$(seletor).val(float2br(br2float($(seletor).val()).toFixed(2)));
			
			return true;
		}
		
		$(seletor).val(float2br(br2float($(seletor).val()).toFixed(2)));
		return false;
	}
	
</script>