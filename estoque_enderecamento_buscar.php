<?
	require('inc/fnc_estoque_enderecamento.php');
	require('inc/fnc_general.php');
	require('inc/fnc_financeiro.php'); //status saldo estoque
	require('inc/con_db.php');
	ob_start();
	session_start();
	
	if(isset($_GET['click_estoque_subnivel'])){
		$antecessor_id = $_GET['click_estoque_subnivel'];
		$estoque_id = $_GET['estoque_id'];
		getRamificacao($antecessor_id, $estoque_id);
		
	}elseif(isset($_GET['click_estoque_endereco_tabela'])){
		#PREENCHE A TABELA COM OS PRODUTOS
		$endereco_id 	= $_GET['click_estoque_endereco_tabela'];
		$estoque_id 	= $_GET['estoque_id'];
		$linha 	= "row";
		
		$sql = 'SELECT tblproduto.*, tblproduto.fldId AS fldProduto_Id, tblestoque_enderecamento.fldEstoque_Id, tblproduto.fldNome AS fldNome,
				tblmarca.fldNome AS fldMarca FROM tblproduto 
				LEFT JOIN tblmarca ON tblproduto.fldMarca_Id = tblmarca.fldId 
				INNER JOIN tblproduto_estoque_enderecamento ON tblproduto_estoque_enderecamento.fldProduto_Id = tblproduto.fldId
				INNER JOIN tblestoque_enderecamento ON tblproduto_estoque_enderecamento.fldEnderecamento_Id = tblestoque_enderecamento.fldId
				WHERE tblproduto_estoque_enderecamento.fldEnderecamento_Id = '.$endereco_id.' AND tblproduto_estoque_enderecamento.fldEstoque_Id = '.$estoque_id;
		$rsProduto = mysql_query($sql);	
		while($rowProduto = mysql_fetch_array($rsProduto)){
			$icon  = ($rowProduto["fldDisabled"] ? "bg_disable" 	: "bg_enable");
			$title = ($rowProduto["fldDisabled"] ? "desabilitado" 	: "habilitado");

			echo '
			<tr class="'.$linha.'">
				<td	style="width:0"><img src="image/layout/'.$icon.'.gif" alt="status" title="'.$title.'" /></td>
				<td style="width:75px"><a href="index.php?p=produto_detalhe&id='.$rowProduto['fldProduto_Id'].'">'.str_pad($rowProduto['fldCodigo'], 6, "0", STR_PAD_LEFT).'</a></td>
				<td	style="width:228px;">'.substr($rowProduto['fldNome'],0, 35).'</td>
				<td	style="width:145px; padding:0 0 0 10px">'.substr($rowProduto['fldMarca'],0, 30).'</td>
				<td style="width:85px;text-align:center;" class="'. fnc_status_saldo($estoque).'">'.format_number_out($estoque, fnc_sistema('quantidade_casas_decimais')).'</td>
			</tr>';
			$linha = ($linha == "row") ? "dif-row"  : "row";

		}
	}elseif(isset($_POST['estoque_enderecamento_sigla'])){
			$enderecamento_id = $_POST['estoque_enderecamento_sigla'];
			$estoque_id = $_POST['estoque_id'];

			function checkSubNivel_ids($id){
				$rsSubNivel_ids = mysql_query('SELECT * FROM tblestoque_enderecamento WHERE fldId = '.$id);
				$row = mysql_num_rows($rsSubNivel_ids);
				if($row){return true;} else {return false;}
				
			}
			function getSubNivel_id($antecessor_id, $estoque_id){
				$rsSubNivel_ids = mysql_query('SELECT * FROM tblestoque_enderecamento WHERE fldId = '.$antecessor_id.' AND fldEstoque_Id = '.$estoque_id);
				while($rowSubNivel_id = mysql_fetch_array($rsSubNivel_ids)){
					$ids = $rowSubNivel_id['fldSigla'];
					if(checkSubNivel_ids($rowSubNivel_id['fldPai_Id'])){$ids = $ids.','.getSubNivel_id($rowSubNivel_id['fldPai_Id'], $estoque_id);}
				}
				$ids = explode(',', $ids);
				$ids = array_reverse($ids);
				$ids = implode(' / ', $ids);
				return $ids;
			}

			echo getSubNivel_id($enderecamento_id, $estoque_id);
	}elseif(isset($_POST['busca_modal'])){
		
			
		$estoque_id		= $_POST['estoque_id'];
		$endereco_nome 	= $_POST['busca'];
		
		echo getRamificacao(0, $estoque_id, $endereco_nome);
	}
	
	
	
	?>