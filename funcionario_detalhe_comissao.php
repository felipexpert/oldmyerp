<?	
	require("funcionario_detalhe_comissao_filtro.php");
	
	//a��es em grupo
	if(isset($_POST['hid_action'])){
		require("funcionario_detalhe_comissao_action.php");
	}
	
/**************************** PAGINA��O *******************************************/

	#ALTERACOES 30/07/13 - LUANA
	$sSQL =("CREATE TEMPORARY TABLE tblcomissao_temp AS (
	SELECT 		

	tblpedido.fldCliente_Id, 
	tblpedido.fldId as fldPedidoId, 
	tblpedido.fldDesconto,
	tblpedido.fldDescontoReais,  
	tblpedido.fldValor_Terceiros, 
	tblpedido.fldStatus as fldPedido_Status,
	tblpedido.fldCadastroData as fldPedidoData, 
	tblpedido_funcionario_servico.fldComissao,

	tblpedido_funcionario_servico.fldFuncao_Tipo,
	tblpedido_funcionario_servico.fldFuncionario_Id,
	
	(SELECT fldData FROM tblpedido_status_historico WHERE fldPedido_Id = tblpedido.fldId ".$_SESSION['funcionario_filtro_data_status']." GROUP BY tblpedido_status_historico.fldPedido_Id) as fldData_Historico,
			
	REPLACE(FORMAT(SUM(tblpedido_funcionario_servico.fldValor),2), ',' ,'') as fldTotalValor,
	REPLACE(FORMAT(SUM(tblpedido_funcionario_servico.fldTempo),2), ',' ,'') as fldTotalTempo,
	REPLACE(FORMAT(SUM(((tblpedido_funcionario_servico.fldComissao / 100) * tblpedido_funcionario_servico.fldValor) + tblpedido_funcionario_servico.fldComissao_Reais),2), ',' ,'') as fldComissaoTotal,

	IFNULL((SELECT REPLACE(FORMAT(SUM(tblpedido_funcionario_servico.fldValor),2), ',' ,'') FROM tblpedido_funcionario_servico
			WHERE fldFuncionario_Id = $funcionario_id AND fldFuncao_Tipo = 2 AND fldPedido_Id = tblpedido.fldId ) ,0) AS fldTotalServico,
	
	IFNULL((SELECT REPLACE(FORMAT(SUM((fldValor - ((fldDesconto / 100) * fldValor)) * fldQuantidade),2), ',' ,'') FROM tblpedido_item 
		WHERE fldExcluido = 0 AND fldPedido_Id = tblpedido.fldId GROUP BY fldPedido_Id) ,0) as fldTotalItem,
	
	(SELECT SUM(fldValor) FROM tblfuncionario_conta_fluxo 
		WHERE fldFuncionario_Id = $funcionario_id AND fldEstorno = '0' AND fldId NOT IN (SELECT fldEstorno FROM tblfuncionario_conta_fluxo WHERE fldFuncionario_Id = $funcionario_id AND fldEstorno >0)
		AND fldFuncao_Tipo = tblpedido_funcionario_servico.fldFuncao_Tipo AND tblfuncionario_conta_fluxo.fldReferencia_Id = tblpedido.fldId) as fldComissaoPago,
	
	(SELECT MAX(fldData_Cadastro) FROM tblfuncionario_conta_fluxo 
		WHERE fldFuncionario_Id = $funcionario_id AND fldEstorno = '0' AND fldId NOT IN (SELECT fldEstorno FROM tblfuncionario_conta_fluxo WHERE fldFuncionario_Id = $funcionario_id AND fldEstorno >0)
		AND fldFuncao_Tipo = tblpedido_funcionario_servico.fldFuncao_Tipo AND tblfuncionario_conta_fluxo.fldReferencia_Id = tblpedido.fldId) as fldDataPago
	
	FROM tblpedido_funcionario_servico
	INNER JOIN	tblpedido ON tblpedido_funcionario_servico.fldPedido_Id 	= tblpedido.fldId
	 	
	WHERE tblpedido_funcionario_servico.fldFuncionario_Id = '$funcionario_id' AND tblpedido.fldExcluido = '0' AND (tblpedido.fldStatus > 1 AND tblpedido.fldStatus < 5)
	".$_SESSION['funcionario_filtro_data_pedido']."
	GROUP BY tblpedido_funcionario_servico.fldPedido_Id
	ORDER BY fldPedidoId DESC)");
	
	//CRIO A TABELA TEMPORARIA COM OS DADOS NECESSARIOS E JOGO EM UMA SESSAO PARA O RELATORIO
	$_SESSION['funcionario_comissao_temp_relatorio'] = $sSQL;
	$sSQL = mysql_query($sSQL);
	
	echo mysql_error();

	//anteriormente ja trazia os totais de parcelas na qry, mas qnd nao tinha parcelas lan�adas na venda, omitia a comissao

		/* CONSULTA ANTIGA: 
	
		$sSQL = 'SELECT tblcomissao_temp.*,
			tblcliente.fldNome,
			SUM(tblpedido_parcela_baixa.fldValor	* (tblpedido_parcela_baixa.fldExcluido 	* -1 + 1)) as fldParcelaValorBaixa,
			SUM(tblpedido_parcela_baixa.fldDesconto * (tblpedido_parcela_baixa.fldExcluido 	* -1 + 1)) as fldParcelaBaixaDesconto,
			SUM(tblpedido_parcela.fldValor			* (tblpedido_parcela.fldExcluido 		* -1 + 1)) AS fldTotalParcelas
			
			FROM tblcomissao_temp
			
			INNER JOIN tblcliente 				ON tblcliente.fldId 			= tblcomissao_temp.fldCliente_Id
			LEFT  JOIN tblpedido_parcela 		ON tblcomissao_temp.fldPedidoId = tblpedido_parcela.fldPedido_Id
			LEFT  JOIN tblpedido_parcela_baixa 	ON tblpedido_parcela.fldId 		= tblpedido_parcela_baixa.fldParcela_Id AND tblpedido_parcela_baixa.fldExcluido = 0
			'.$_SESSION["filtro_funcionario_comissao"] .' 
			ORDER BY fldPedidoId DESC'; */
			
		$sSQL = 'SELECT tblcomissao_temp.*,
			tblcliente.fldNome,
			(SELECT CONVERT(GROUP_CONCAT(DISTINCT fldId SEPARATOR ",") USING "utf8") FROM tblpedido_parcela WHERE fldPedido_Id = tblcomissao_temp.fldPedidoId) as fldParcelas_Id
			
			FROM tblcomissao_temp
			
			INNER JOIN tblcliente ON tblcliente.fldId = tblcomissao_temp.fldCliente_Id

			'.$_SESSION["filtro_funcionario_comissao"] .' 
			ORDER BY fldPedidoId DESC';
			
	$_SESSION['funcionario_comissao_relatorio'] = $sSQL;
	
	#######################################################################					
	$rsTotais 		= mysql_query($sSQL);
	$rowsTotal		= mysql_num_rows($rsTotais);
	#######################################################################
	
	echo mysql_error();
	//defini��o dos limites
	$limite 	= 50;
	$n_paginas 	= 7;
	$total_paginas = ceil($rowsTotal/ $limite);
	
	if(isset($_GET["pagina"]) && $_GET["pagina"] > $total_paginas){
		$inicio = 0;
	}elseif(isset($_GET['pagina'])){
		$inicio = ($_GET['pagina'] - 1) * $limite;
	}else{
		$inicio = 0;
	}
	
	#COMENTANDO A PAGINACAO POR ENQUANTO
	//$sSQL 		.= " LIMIT " . $inicio . "," . $limite;
	$rsComissao = mysql_query($sSQL);
	$pagina 	= ($_GET['pagina'] ? $_GET['pagina'] : "1");	
	
####################################################################################
	$raiz = "index.php?p=funcionario_detalhe&modo=comissao&id=$funcionario_id";
?>

    <form class="table_form" id="frm_funcionario_comissao" action="" method="post">
    	<div id="table">
            <div id="table_cabecalho">
                <ul class="table_cabecalho">
                    <li style="width:80px; text-align:center">Venda</li>
                    <li style="width:220px">Cliente</li>
                    <li style="width:50px">Tipo</li>
                    <li style="width:50px;text-align:right">Tempo</li>
                    <li style="width:70px;text-align:right">Valor</li>
                    <li style="width:80px;text-align:right">Comiss&atilde;o</li>
                    <li style="width:100px;text-align:right">Venda recebido</li>
                    <li style="width:80px;text-align:right">Liberado</li>
                    <li style="width:80px;text-align:right">Pago</li>
                    <li style="width:75px;text-align:right">&Uacute;ltimo pag.</li>
                    <li style="width:15px"><input type="checkbox" name="chk_todos" id="chk_todos" /></li>
                </ul>
            </div>
            <div id="table_container">       
                <table id="table_general" class="table_general" summary="Lista de comiss&otilde;es">
                <tbody>
<?					
					$id_array 	= array();
					$n 			= 0;
					$linha 		= "row";
					while($rowComissao 	= mysql_fetch_assoc($rsComissao)){
					
						# logo no inicio ja adiciono os campos: fldParcelaValorBaixa, fldParcelaValorDesconto, fldTotalParcelas... assim � preciso mudar nada no restante
						/*
						SUM(tblpedido_parcela_baixa.fldDesconto * (tblpedido_parcela_baixa.fldExcluido 	* -1 + 1)) as fldParcelaBaixaDesconto,
						SUM(tblpedido_parcela.fldValor			* (tblpedido_parcela.fldExcluido 		* -1 + 1)) AS fldTotalParcelas
						*/

						if($rowComissao['fldParcelas_Id'] != null){
							$rsValor_Baixa 	= mysql_query("SELECT 
																SUM(fldValor * (fldExcluido * -1 +1)) as fldValor_Baixa,
																SUM(fldDesconto * (fldExcluido * -1 +1)) as fldValor_Desconto
															FROM tblpedido_parcela_baixa WHERE fldParcela_Id IN ({$rowComissao['fldParcelas_Id']})");
							$rsValor_Baixa 	= mysql_fetch_assoc($rsValor_Baixa);
							$rowComissao['fldParcelaValorBaixa'] 	= $rsValor_Baixa['fldValor_Baixa'];
							$rowComissao['fldParcelaBaixaDesconto'] = $rsValor_Baixa['fldValor_Desconto'];
							
							$rsTotal_Parcela = mysql_query("SELECT SUM(fldValor * (fldExcluido * -1 +1)) as fldTotal_Parcela FROM tblpedido_parcela WHERE fldId IN ({$rowComissao['fldParcelas_Id']})");
							$rsTotal_Parcela = mysql_fetch_assoc($rsTotal_Parcela);
							$rowComissao['fldTotalParcelas'] = $rsTotal_Parcela['fldTotal_Parcela'];
							
						} else {
							$rowComissao['fldParcelaValorBaixa'] 	= 0;
							$rowComissao['fldParcelaBaixaDesconto'] = 0;
							$rowComissao['fldTotalParcelas']		= 0;
						}
						
						//fldParcelaBaixaDesconto
						 
						$id_array[$n] 	= $rowComissao["fldPedidoId"].','.$rowComissao['fldFuncao_Tipo'];
						$n += 1;
						
						$pedido_id 		= $rowComissao["fldPedidoId"];
						$venda_total 	= $rowComissao['fldTotalItem'] + $rowComissao['fldTotalServico'] + $rowComissao['fldValor_Terceiros'];
					
						####################################################################################################################################################################################################################################################################################################################################################################
					
						if($_SESSION['sel_funcionario_comissao_desconto'] > 0){
				
							$porcentagem 		= ((100 / ($venda_total)) * $rowComissao['fldDescontoReais']); 	
							$desconto_comissao 	= ($porcentagem / 100) * $rowComissao['fldComissaoTotal']; 		
							
							$comissaoTotal 		= $rowComissao['fldComissaoTotal'] - $desconto_comissao;														
							$desconto	 		= ($rowComissao['fldDesconto'] / 100); 
							$comissaoTotal 		= ($comissaoTotal - ($desconto * $comissaoTotal));
							
							if($rowComissao['fldTotalParcelas'] > 0){ // pra nao dar erro de divizao por zero
								//CALCULAR COMISSAO LIBERADA
								$comissaoLiberada 	= (100 / $rowComissao['fldTotalParcelas']) * $rowComissao['fldParcelaValorBaixa'];											
								$comissaoLiberada 	= ($comissaoLiberada / 100) * $comissaoTotal;												
							}else{
								$comissaoLiberada 	= 0;
							}
							
						}else{
							$comissaoTotal 		= $rowComissao['fldComissaoTotal'];
							$comissaoLiberada 	= (100 / ($venda_total)) * $rowComissao['fldParcelaValorBaixa'];	
							$comissaoLiberada 	= ($comissaoLiberada / 100) * $rowComissao['fldComissaoTotal'];	
						}
						
						#############################################################################################################################################################################################################################################################################################################################################################################################################################################################
						
						$funcao_tipo = ($rowComissao['fldFuncao_Tipo'] == 1)? 'venda' : 'servi&ccedil;o';
						if($rowComissao['fldComissaoPago'] >= $comissaoTotal){
							$status_comissao = 'pago';
						}elseif($rowComissao['fldComissaoPago'] > 0 && $comissaoTotal > $rowComissao['fldComissaoPago']){
							$status_comissao = 'parcial';
						}elseif($comissaoTotal > $rowComissao['fldComissaoPago']){
							$status_comissao = 'aberto';
						}
						
						$status = ($rowComissao['fldComissaoPago'] > 0)? 'credito' : 'debito';
						
						if(fnc_status_pedido($rowComissao['fldPedidoId'], "tblpedido") == 'convertida'){
							$status = "nfe";
							$sql_id = mysql_fetch_assoc(mysql_query("SELECT fldPedido_Destino_Nfe_Id FROM tblpedido WHERE fldId = '".$rowComissao['fldPedidoId']."'"));
							$id		= $sql_id['fldPedido_Destino_Nfe_Id'];
						} else {
							$status = fnc_status_pedido($rowComissao['fldPedidoId'], "tblpedido");
							$id 	= $rowComissao['fldPedidoId'];
						}
?>						<tr class="<?= $linha; ?>">
							<td class="cod"	style="width:80px;"><a href="index.php?p=pedido_detalhe&id=<?=$id;?>"><span title="<?=$status?>" class="<?=$status?>"><?=str_pad($id, 6, "0", STR_PAD_LEFT)?></span></a></td>
                            <td style="width:220px"><?=substr($rowComissao['fldNome'],0,35)?></td>
                            <td class="cod" style="width:50px"><?=$funcao_tipo?></td>
                            <td class="cod" style="width:50px;text-align:right"><?=format_number_out($rowComissao['fldTotalTempo'])?></td>
                            <td style="width:70px; text-align:right"><?=format_number_out($rowComissao['fldTotalValor'])?></td>
							<td style="width:80px; text-align:right"><?=format_number_out($comissaoTotal)?></td>
                            <td style="width:100px; text-align:right"><?=format_number_out($rowComissao['fldParcelaValorBaixa'])?></td>
							<td style="width:80px; text-align:right" class="liberado"><?=format_number_out($comissaoLiberada)?></td>
							<td style="width:80px; text-align:right" class="<?=$status?>"><?=format_number_out($rowComissao['fldComissaoPago'])?></td>
							<td style="width:75px; text-align:right"><?=format_date_out($rowComissao['fldDataPago'])?></td>
                            <td style="width:15px"><input type="checkbox" class="<?=$status_comissao?>" name="chk_comissao_<?=$rowComissao['fldPedidoId'].','.$rowComissao['fldFuncao_Tipo']?>" id="chk_comissao_<?=$rowComissao['fldPedidoId']?>" title="selecionar o registro posicionado" /></td>
                        </tr>
<?                      $linha = ($linha == "row") ? "dif-row" : "row";
						
						$totalPedido		+= $rowComissao['fldTotalValor'];
						$totalTempo			+= $rowComissao['fldTotalTempo'];
						$totalComissao		+= $comissaoTotal;
						$totaPedidoPago		+= $rowComissao['fldParcelaValorBaixa'];
						$totalParcial		+= $comissaoLiberada;
						$totalComissaoPago	+= $rowComissao['fldComissaoPago'];
                   }
				   echo mysql_error();
?>		 		</tbody>
				</table>
            </div>
			<div class="saldo" style="width:952px; float:right;">
				<p style="padding:4px;float:left; margin-left:3px;">Vendas	 <span style="margin-left:4px;width:60px;text-align:right" class="credito"><?=format_number_out($totalPedido)?>			</span></p>
				<p style="padding:4px;float:left;">Tempo	 				 <span style="margin-left:4px;width:60px;text-align:right" class="credito"><?=format_number_out($totalTempo)?>			</span></p>
				<p style="padding:4px;float:left;">Comiss&otilde;es 		 <span style="margin-left:4px;width:60px;text-align:right" class="credito"><?=format_number_out($totalComissao)?>		</span></p>
				<p style="padding:4px;float:left;">Vendas recebidas 		 <span style="margin-left:4px;width:60px;text-align:right" class="credito"><?=format_number_out($totaPedidoPago)?>		</span></p>
				<p style="padding:4px;float:left;">Comiss&otilde;es liberadas<span style="margin-left:4px;width:60px;text-align:right" class="credito"><?=format_number_out($totalParcial)?>		</span></p>
				<p style="padding:4px;float:left;">Comiss&otilde;es pagas	 <span style="margin-left:4px;width:60px;text-align:right" class="credito"><?=format_number_out($totalComissaoPago)?>	</span></p>
			</div>
       	  
            <input type="hidden" name="hid_array" 			id="hid_array" 			value="<?=urlencode(serialize($id_array))?>" />
            <input type="hidden" name="hid_action" 			id="hid_action" 		value="true" />
            <input type="hidden" name="hid_funcionario_id" 	id="hid_funcionario_id" value="<?=$funcionario_id?>" />
            
			<div id="table_action">
                <ul id="action_button">
                	<li><input type="submit" name="btn_action" id="btn_baixa" 	value="dar baixa" 	title="Dar baixa em registro(s) selecionado(s)" class="btn_general" /></li>
                	<li><input type="submit" name="btn_action" id="btn_estorno" value="estorno" 	title="Estornar registro(s) selecionado(s)"  onclick="return confirm('Ser&aacute; estornada &uacute;ltima baixa desta parcela. Deseja continuar?')" /></li>
                	<li><input type="submit" name="btn_action" id="btn_recibo" 	value="recibo" 		title="Imprimir recibo de registro(s) selecionado(s))" /></li>
                	<li><a class="btn_print" name="btn_print" id="btn_print" title="imprimir relat&oacute;rio" href="funcionario_detalhe_comissao_relatorio.php?id=<?=$funcionario_id?>" target="_blank"></a></li>
				</ul>
        	</div>
            
            <div id="table_paginacao">
<?				//$paginacao_destino =  $raiz;
				//include("paginacao.php")
?>	        </div>    
  
            <div class="table_registro">
            	<span>Total de registros exibidos: </span><span class="total"><?=$n?> de <?=$rowsTotal?></span>
            </div>       
        </div>
	</form>