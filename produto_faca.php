<?
	
	if(isset($_POST['hid_action'])){
		require("produto_faca_action.php");
	}
	
	if(isset($_POST["txt_nome"])){
		$Nome 			= mysql_real_escape_string($_POST['txt_nome']);
		$Largura 		= $_POST['txt_largura'];
		$Altura 		= $_POST['txt_altura'];
		$PortaCliche 	= $_POST['txt_porta_cliche'];
		$TamanhoCalc 	= $_POST['txt_tamanho_calculo'];
		$NumCarreiras	= $_POST['txt_numero_carreiras'];
		$DataCadastro 	= date("Y-m-d");
		
		//CONFERE SE ESTÁ SENDO ADICIONADO OU É EDITADO
		if($_POST['hid_edit'] != ''){
			$sSQL = "UPDATE tblproduto_faca SET
			fldNome = '$Nome', 
			fldLargura = '$Altura',
			fldAltura = '$Largura', 
			fldPorta_Cliche = '$PortaCliche', 
			fldTamanho_Calculo = '$TamanhoCalc', 
			fldNumero_Carreiras = '$NumCarreiras'
			WHERE fldId = ".$_POST['hid_edit'];
		}
		else{
			$sSQL = "INSERT INTO tblproduto_faca
			(fldNome, fldLargura, fldAltura, fldPorta_Cliche, fldTamanho_Calculo, fldNumero_Carreiras, fldData_Cadastro)
			VALUES(
			'$Nome',
			'$Largura',
			'$Altura',
			'$PortaCliche',
			'$TamanhoCalc',
			'$NumCarreiras',
			'$DataCadastro'
			)";
		}
		mysql_query($sSQL);
		echo mysql_error();
	}
		
/**************************** ORDER BY *******************************************/
	$filtroOrder = 'fldNome';
	$class 		 = 'asc';
	$order_sessao = explode(" ", $_SESSION['order_produto_faca']);
	if(isset($_GET['order'])){
		switch($_GET['order']){
			
			case 'codigo'	:  $filtroOrder = "fldId";  	break;
			case 'nome'		:  $filtroOrder = "fldNome"; 	break;
		}
		if($order_sessao[0] == $filtroOrder){
			$class = ($order_sessao[1] == 'asc') ? 'desc' : 'asc';
		}
	}
	
	//definir icone para ordem
	$_SESSION['order_produto_faca'] = (!$_SESSION['order_produto_faca'] || $_GET['order']) ? $filtroOrder.' '.$class : $_SESSION['order_produto_faca'];
	$pag	= ($_GET['pagina'])? '&pagina='.$_GET['pagina'] : ''; 
	$raiz 	= "index.php?p=produto&modo=faca$pag&amp;order=";
	
	$order_sessao = explode(" ", $_SESSION['order_produto_faca']);
	$filtroOrder  = $order_sessao[0]; //pra poder comparar na listagem e exibir a class
	
/**************************** PAGINAÇÃO *******************************************/
	$sSQL 		= "SELECT * FROM tblproduto_faca WHERE fldExcluido = '0' order by " . $_SESSION['order_produto_faca'];
	$rsTotal 	= mysql_query($sSQL);
	$rowsTotal 	= mysql_num_rows($rsTotal);
	
	//definição dos limites
	$limite = 150;
	$n_paginas = 7;
	
	$total_paginas = ceil(mysql_num_rows($rsTotal) / $limite);
	
	if(isset($_GET["pagina"]) && $_GET["pagina"] > $total_paginas){
		$inicio = 0;
	}elseif(isset($_GET['pagina'])){
		$inicio = ($_GET['pagina'] - 1) * $limite;
	}else{
		$inicio = 0;
	}
	
	$sSQL .= " limit " . $inicio . "," . $limite;

	$rsFaca = mysql_query($sSQL);
	$pagina = (isset($_GET['pagina'])) ? $_GET['pagina'] : "1";
	
#########################################################################################
?>

    <form class="table_form" id="frm_faca" action="" method="post">
    	<div id="table">
            <div id="table_cabecalho">
                <ul class="table_cabecalho">
                    <li style="width:10px">&nbsp;</li>
                    <li class="order" style="width:90px">
                    	<a <?= ($filtroOrder == 'fldId') 	? "class='$class'" : '' ?> style="width:75px" href="<?=$raiz?>codigo">C&oacute;d.</a>
                    </li>
                    <li class="order" style="width:450px">
                    	<a <?= ($filtroOrder == 'fldNome') 	? "class='$class'" : '' ?> style="width:435px" href="<?=$raiz?>faca">Nome</a>
                    </li>
					<li style="width:100px; text-align:right">Largura</li>
                    <li style="width:100px; text-align:right">Altura</li>
                    <li style="width:110px; text-align:right">Clichê</li>
                    <li style="width:38px">&nbsp;</li>
                    <li style="width:25px; text-align:left"><input type="checkbox" name="chk_todos" id="chk_todos" /></li>
                </ul>
            </div>
            <div id="table_container">       
                <table id="table_general" class="table_general" summary="Lista de Facas">
                	<tbody>
<?					
					$id_array = array();
					$n = 0;
					
					$linha 	= "row";
					$rows 	= mysql_num_rows($rsFaca);
					while($rowFaca = mysql_fetch_array($rsFaca)){
						
						$id_array[$n] = $rowFaca["fldId"];
						$n += 1;
				
?>						<tr class="<?= $linha; ?>">
							<td class="cod" style="width:100px;text-align:center;padding-right:10px"><?=str_pad($rowFaca['fldId'], 5, "0", STR_PAD_LEFT)?></td>
                           <td style="width:450px;"><?=$rowFaca['fldNome']?></td>
                            <td style="width:100px;text-align:right"><?=$rowFaca['fldLargura']?></td>
                            <td style="width:100px;text-align:right"><?=$rowFaca['fldAltura']?></td>
							<td style="width:110px; text-align:right"><?=$rowFaca['fldPorta_Cliche']?></td>
                            <td style="width:40px; text-align:center"><a class="edit modal" href="produto_faca,<?=$rowFaca['fldId']?>" rel="465-180"></a></td>
                            <td style="width:auto"><input type="checkbox" name="chk_faca_<?=$rowFaca['fldId']?>" id="chk_faca_<?=$rowFaca['fldId']?>" title="selecionar o registro posicionado" /></td>
                        </tr>
<?                  	$linha = ($linha == "row") ? "dif-row" : "row"; 
					}
?>		 			</tbody>
				</table>
            </div>
       	  
            <input type="hidden" name="hid_array"	id="hid_array" 	value="<?=urlencode(serialize($id_array))?>" />
            <input type="hidden" name="hid_action" 	id="hid_action" value="true" />
            
			<div id="table_action">
                <ul id="action_button">
                    <li><a class="btn_novo modal" rel="465-180" href="produto_faca">novo</a></li>
                    <li><input type="submit" name="btn_action" id="btn_excluir" 	value="excluir" 	title="Excluir registro(s) selecionado(s)" onclick="return confirm('Deseja excluir os registros selecionados?')" /></li>
                </ul>
        	</div>
            <div id="table_paginacao">
<?				$paginacao_destino = "?p=produto&modo=faca";
				include("paginacao.php")
?>		
            </div>
            <div class="table_registro">
            	<span>Exibindo registros <?=($pagina*$limite-$limite+1).' a '.($pagina*$limite-$limite+$rows)?> do total de <?=$rowsTotal?></span>
            </div>    
        </div>
	</form>