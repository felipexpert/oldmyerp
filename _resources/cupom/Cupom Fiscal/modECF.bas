Attribute VB_Name = "modECF"
Public sPrinter As String


'////////////////////////////////////////////////////////////////////////////////
'//
'// Fun��o:
'//    VerificaRetornoFuncaoImpressora
'// Objetivo:
'//    Verificar o retorno da impressora e da fun��o utilizada
'// Retorno:
'//    True para OK
'//    False para n�o OK
'//
'////////////////////////////////////////////////////////////////////////////////
Function VerificaRetornoFuncaoImpressora(ByVal iRetorno As Integer) As Boolean

   Dim cMSGErro As Variant
   Dim iACK As Integer
   Dim iST1 As Integer
   Dim iST2 As Integer
   
   iACK = 0: iST1 = 0: iST2 = 0
   
    cMSGErro = ""
    VerificaRetornoFuncaoImpressora = False
    Select Case iRetorno
        Case 0:
           cMSGErro = "Erro de Comunica��o !"
        Case -1:
            cMSGErro = "Erro de execu��o na Fun��o !"
        Case -2:
            cMSGErro = "Par�metro inv�lido na Fun��o !"
        Case -3:
            cMSGErro = "Al�quota n�o Programada !"
        Case -4:
            cMSGErro = "Arquivo BEMAFI32.INI n�o Encontrado !"
        Case -5:
            cMSGErro = "Erro ao abrir a Porta de Comunica��o !"
        Case -6:
            cMSGErro = "Impressora Desligada ou Cabo de Comunica��o Desconectado !"
        Case -7:
            cMSGErro = "C�digo do Banco n�o encontrado no arquivo BEMAFI32.INI !"
        Case -8:
            cMSGErro = "Erro ao criar ou gravar arquivo STATUS.TXT ou RETORNO.TXT !"
        Case -27:
            cMSGErro = "Status diferente de 6, 0, 0 !"
        Case -30:
            cMSGErro = "Fun��o incompat�vel com a impressora fiscal YANCO !"
    End Select

    If cMSGErro <> "" Then 'IF1
        Call Mega_FI_FinalizaModoTEF
        VerificaRetornoFuncaoImpressora = False
    End If

    cMSGErro = ""
    If iRetorno = 1 Then 'IF2
        Select Case sPrinter
          Case Is = "bematech"
            Call Bematech_FI_RetornoImpressora(iACK, iST1, iST2)
          Case Is = "daruma"
            Call Daruma_FI_RetornoImpressora(iACK, iST1, iST2)
        End Select
        
        If iACK = 21 Then 'IF2-1
            Call Mega_FI_FinalizaModoTEF
            MsgBox "A Impressora retornou NAK !" & vbCrLf & _
                                       "Erro de Protocolo de Comunica��o !", vbOKOnly, _
                                       "Aten��o"
            VerificaRetornoFuncaoImpressora = False
        
        Else 'ELSEIF2-1
            If (iST1 <> 0) Or (iST2 <> 0) Then 'IF2-1-1
                  ' Analisa ST1
                If (iST1 >= 128) Then 'IF2-1-1-1
                    iST1 = iST1 - 128
                    cMSGErro = cMSGErro & "Fim de Papel" & vbCrLf
                End If 'ENDIF2-1-1-1
                If (iST1 >= 64) Then 'IF2-1-1-2
                    iST1 = iST1 - 64
                    cMSGErro = cMSGErro & "Pouco Papel" & vbCrLf
                    VerificaRetornoFuncaoImpressora = True
                    Exit Function
                End If 'ENDIF2-1-1-2
                If (iST1 >= 32) Then 'IF2-1-1-3
                    iST1 = iST1 - 32
                    cMSGErro = cMSGErro & "Erro no Rel�gio" & vbCrLf
                End If 'ENDIF2-1-1-3
                If (iST1 >= 16) Then 'IF2-1-1-4
                    iST1 = iST1 - 16
                    cMSGErro = cMSGErro & "Impressora em Erro" & vbCrLf
                End If 'ENDIF2-1-1-4
                If (iST1 >= 8) Then 'IF2-1-1-5
                    iST1 = iST1 - 8
                    cMSGErro = cMSGErro & "Primeiro Dado do Comando n�o foi ESC" & vbCrLf
                End If 'ENDIF2-1-1-5
                If iST1 >= 4 Then 'IF2-1-1-6
                    iST1 = iST1 - 4
                    cMSGErro = cMSGErro & "Comando Inexistente" & vbCrLf
                End If 'ENDIF2-1-1-6
                If iST1 >= 2 Then 'IF2-1-1-7
                    iST1 = iST1 - 2
                    cMSGErro = cMSGErro & "Cupom Fiscal Aberto" & vbCrLf
                End If 'ENDIF2-1-1-7
                If iST1 >= 1 Then 'IF2-1-1-8
                    iST1 = iST1 - 1
                    cMSGErro = cMSGErro & "N�mero de Par�metros Inv�lidos" & vbCrLf
                End If 'ENDIF2-1-1-8
                'Analisa ST2
                If iST2 >= 128 Then 'IF2-1-1-9
                    iST2 = iST2 - 128
                    cMSGErro = cMSGErro & "Tipo de Par�metro de Comando Inv�lido" & vbCrLf
                End If 'ENDIF2-1-1-9
                If iST2 >= 64 Then 'IF2-1-1-10
                    iST2 = iST2 - 64
                    cMSGErro = cMSGErro & "Mem�ria Fiscal Lotada" & vbCrLf
                End If 'ENDIF2-1-1-10
                If iST2 >= 32 Then 'IF2-1-1-11
                    iST2 = iST2 - 32
                    cMSGErro = cMSGErro & "Erro na CMOS" & vbCrLf
                End If 'ENDIF2-1-1-11
                If iST2 >= 16 Then 'IF2-1-1-12
                    iST2 = iST2 - 16
                    cMSGErro = cMSGErro & "Al�quota n�o Programada" & vbCrLf
                End If 'ENDIF2-1-1-12
                If iST2 >= 8 Then 'IF2-1-1-13
                    iST2 = iST2 - 8
                    cMSGErro = cMSGErro & "Capacidade de Al�quota Program�veis Lotada" & vbCrLf
                End If 'ENDIF2-1-1-13
                If iST2 >= 4 Then 'IF2-1-1-14
                     iST2 = iST2 - 4
                     cMSGErro = cMSGErro & "Cancelamento n�o permitido" & vbCrLf
                End If 'ENDIF2-1-1-14
                If iST2 >= 2 Then 'IF2-1-1-15
                    iST2 = iST2 - 2
                    cMSGErro = cMSGErro & "CGC/IE do Propriet�rio n�o Programados" & vbCrLf
                End If 'ENDIF2-1-1-15
                If iST2 >= 1 Then 'IF2-1-1-16
                    iST2 = iST2 - 1
                    cMSGErro = cMSGErro & "Comando n�o executado" & vbCrLf
                End If 'ENDIF2-1-1-16
                If (cMSGErro <> "") Then 'IF2-1-1-17
                    Call Mega_FI_FinalizaModoTEF
                    MsgBox cMSGErro, vbOKOnly + vbExclamation, "Aten��o"
                    If VerificaRetornoFuncaoImpressora = True Then
                        VerificaRetornoFuncaoImpressora = False
                    End If
                End If 'ENDIF2-1-1-17
            Else
                VerificaRetornoFuncaoImpressora = True
            End If 'ENDIF2-1-1
        End If 'ENDIF2-1
    End If 'ENDIF2

End Function

Public Function Mega_FI_AbreCupom(CPF_ou_CNPJ As String)
  Select Case sPrinter
    Case Is = "bematech"
      Mega_FI_AbreCupom = Bematech_FI_AbreCupom(CPF_ou_CNPJ)
    Case Is = "daruma"
      Mega_FI_AbreCupom = Daruma_FI_AbreCupom(CPF_ou_CNPJ)
  End Select
End Function

Public Function Mega_FI_VerificaFormasPagamento(FormasPagamento As String) As Integer
  Select Case sPrinter
    Case Is = "bematech"
      Mega_FI_VerificaFormasPagamento = Bematech_FI_VerificaFormasPagamentoMFD(FormasPagamento)
    Case Is = "daruma"
      Mega_FI_VerificaFormasPagamento = Daruma_FI_VerificaFormasPagamento(FormasPagamento)
  End Select
End Function

Public Function Mega_FI_VendeItem(ByVal Codigo As String, ByVal Descricao As String, ByVal Aliquota As String, ByVal TipoQuantidade As String, ByVal Quantidade As String, ByVal CasasDecimais As Integer, ByVal ValorUnitario As String, ByVal TipoDesconto As String, ByVal Desconto As String) As Integer
  Select Case sPrinter
    Case Is = "bematech"
      Mega_FI_VendeItem = Bematech_FI_VendeItem(Codigo, Descricao, Aliquota, TipoQuantidade, Quantidade, CasasDecimais, ValorUnitario, TipoDesconto, Desconto)
    Case Is = "daruma"
      Mega_FI_VendeItem = Daruma_FI_VendeItem(Codigo, Descricao, Aliquota, TipoQuantidade, Quantidade, CasasDecimais, ValorUnitario, TipoDesconto, Desconto)
  End Select
End Function

Public Function Mega_FI_IniciaFechamentoCupom(AcrescimoDesconto As String, TipoAcrescimoDeconto As String, ValorAcrescimoDesconto As String) As Integer
  Select Case sPrinter
    Case Is = "bematech"
      Mega_FI_IniciaFechamentoCupom = Bematech_FI_IniciaFechamentoCupom(AcrescimoDesconto, TipoAcrescimoDeconto, ValorAcrescimoDesconto)
    Case Is = "daruma"
      Mega_FI_IniciaFechamentoCupom = Daruma_FI_IniciaFechamentoCupom(AcrescimoDesconto, TipoAcrescimoDeconto, ValorAcrescimoDesconto)
  End Select
End Function

Public Function Mega_FI_NumeroCupom(NumeroCupom As String) As Integer
  Select Case sPrinter
    Case Is = "bematech"
      Bematech_FI_NumeroCupom NumeroCupom
    Case Is = "daruma"
      Daruma_FI_NumeroCupom NumeroCupom
  End Select
End Function

Public Function Mega_FI_EfetuaFormaPagamento(FormaPagamento As String, ValorFormaPagamento As String) As Integer
  Select Case sPrinter
    Case Is = "bematech"
      Mega_FI_EfetuaFormaPagamento = Bematech_FI_EfetuaFormaPagamento(FormaPagamento, ValorFormaPagamento)
    Case Is = "daruma"
      Mega_FI_EfetuaFormaPagamento = Daruma_FI_EfetuaFormaPagamento(FormaPagamento, ValorFormaPagamento)
  End Select
End Function

Public Function Mega_FI_TerminaFechamentoCupom(Mensagem As String) As Integer
  Select Case sPrinter
    Case Is = "bematech"
      Mega_FI_TerminaFechamentoCupom = Bematech_FI_TerminaFechamentoCupom(Mensagem)
    Case Is = "daruma"
      Mega_FI_TerminaFechamentoCupom = Daruma_FI_TerminaFechamentoCupom(Mensagem)
  End Select
End Function

Public Function Mega_FI_IniciaModoTEF()
  Select Case sPrinter
    Case Is = "bematech"
      Mega_FI_IniciaModoTEF = Bematech_FI_IniciaModoTEF
    Case Is = "daruma"
      Mega_FI_IniciaModoTEF = Daruma_TEF_TravarTeclado(1)
  End Select
End Function

Public Function Mega_FI_FinalizaModoTEF() As Integer
  Select Case sPrinter
    Case Is = "bematech"
      Bematech_FI_FinalizaModoTEF
    Case Is = "daruma"
      Daruma_TEF_TravarTeclado (0)
  End Select
End Function

Public Function Mega_FI_FechaComprovanteNaoFiscalVinculado()
  Select Case sPrinter
    Case Is = "bematech"
      Mega_FI_FechaComprovanteNaoFiscalVinculado = Bematech_FI_FechaComprovanteNaoFiscalVinculado
    Case Is = "daruma"
      Mega_FI_FechaComprovanteNaoFiscalVinculado = Daruma_FI_FechaComprovanteNaoFiscalVinculado
  End Select
End Function


Public Function Mega_FI_VerificaImpressoraLigada() As Integer
  Do
    mdiMain.stbMain.Panels(1).Text = "Comunicando com a impressora..."
    Select Case sPrinter
      Case Is = "bematech"
        Mega_FI_VerificaImpressoraLigada = Bematech_FI_VerificaImpressoraLigada
      Case Is = "daruma"
        Mega_FI_VerificaImpressoraLigada = Daruma_FI_VerificaImpressoraLigada
    End Select
    If Mega_FI_VerificaImpressoraLigada = 0 Then
      mdiMain.stbMain.Panels(1).Text = "Erro de comunica��o com a impressora"
      iRetorno = MsgBox("Erro de comunica��o com a impressora fiscal." & vbCrLf & "Favor verificar se est� ligada e corretamente conectada", vbRetryCancel + vbExclamation)
    End If
  Loop Until Mega_FI_VerificaImpressoraLigada = 1 Or iRetorno = vbCancel
  If Mega_FI_VerificaImpressoraLigada = 1 Then
    mdiMain.stbMain.Panels(1).Text = "Comunica��o estabelecida com a impressora"
  End If
End Function

Public Function Mega_FI_ReducaoZ() As Integer
  Dim iRetorno As Integer
  Screen.MousePointer = vbHourglass
  Select Case sPrinter
    Case Is = "bematech"
      iRetorno = Bematech_FI_ReducaoZ(Format(Date, "dd/mm/yy"), Time)
    Case Is = "daruma"
      iRetorno = Daruma_FI_ReducaoZ(Date, Time)
  End Select
  VerificaRetornoFuncaoImpressora iRetorno
  Screen.MousePointer = vbNormal
End Function

Public Function Mega_FI_LeituraX() As Integer
  Dim iRetorno As Integer
  Select Case sPrinter
    Case Is = "bematech"
      iRetorno = Bematech_FI_LeituraX
    Case Is = "daruma"
      iRetorno = Daruma_FI_LeituraX
  End Select
  VerificaRetornoFuncaoImpressora iRetorno
End Function

Public Function Mega_FI_CancelaCupom() As Integer
    Dim iRetorno As Integer
    Select Case sPrinter
    Case Is = "bematech"
        iRetorno = Bematech_FI_CancelaCupom
    Case Is = "daruma"
        iRetorno = Daruma_FI_CancelaCupom
    End Select
    Mega_FI_CancelaCupom = iRetorno
End Function

Public Function Mega_FI_ProgramaFormaPagamento(FormaPagto As String) As Integer
  Dim iRetorno As Integer
  Select Case sPrinter
  Case Is = "bematech"
    iRetorno = Bematech_FI_ProgramaFormaPagamentoMFD(FormaPagto, "0")
  Case Is = "daruma"
    iRetorno = Daruma_FI_ProgramaFormasPagamento(FormaPagto)
  End Select
  Mega_FI_ProgramaFormaPagamento = iRetorno
End Function
