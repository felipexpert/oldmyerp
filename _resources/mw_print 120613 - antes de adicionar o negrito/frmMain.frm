VERSION 5.00
Begin VB.Form frmMain 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "MW PRINT"
   ClientHeight    =   1515
   ClientLeft      =   -15
   ClientTop       =   375
   ClientWidth     =   2115
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   1515
   ScaleWidth      =   2115
   Visible         =   0   'False
   Begin VB.CommandButton cmdOcultar 
      Caption         =   "Ocultar"
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   840
      Width           =   1815
   End
   Begin VB.Timer tmrPRINT_inbox 
      Interval        =   1000
      Left            =   120
      Top             =   120
   End
   Begin VB.Label lblMessage 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label1"
      Height          =   615
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1815
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Definir bandeja do sistema
Private WithEvents m_frmSysTray As frmSysTray
Attribute m_frmSysTray.VB_VarHelpID = -1

Private Sub PRINT_inbox_ler()
    Dim nFile           As Long
    Dim strFileName     As String
    Dim strFileLog      As String
    Dim strComando      As String
    Dim strParametros   As String
    Dim arrPar() As String
        
    Dim objFSO                  As Scripting.FileSystemObject
    Dim objPasta                As Object
    Dim objArquivo              As Object
    Dim objArquivosExistentes   As Object
    Dim strArquivo As String
    Dim strPath As String
    Dim intControle As Byte
    
    Dim nFile2          As Long
    Dim strPorta As String
    
    
    
    strPath = App.Path & "\inbox\"
    
    Set objFSO = New Scripting.FileSystemObject
    Set objPasta = objFSO.GetFolder(strPath)
    Set objArquivosExistentes = objPasta.Files
    
    'ler diret�rio em busca de arquivos
    For Each objArquivo In objArquivosExistentes
      strFileName = GetShortPath(strPath) & objArquivo.Name
      strFileLog = GetShortPath(strPath) & "log\" & objArquivo.Name
    Next
    
    lblMessage.Caption = Time
    
    If strFileName = "" Then Exit Sub
    
    'Processar conte�do do arquivo
    nFile = FreeFile
    nFile2 = FreeFile + 1
    
    'arquivo com conte�do a ser impresso
    Open strFileName For Input As #nFile
    
    Line Input #nFile, strPorta
    
    
    'sa�da de impress�o
    
    On Error GoTo Erro
    'strPorta = "\\pc03\generica"
    Open strPorta For Output As #nFile2
    
    
    
    Do Until EOF(nFile)
        
        'L� uma linha e pega o comando
        Line Input #nFile, strComando
        
        'Print #1, Chr(27) + "M"
      
        
        Print #nFile2, strComando
      
      
        
    Loop
    
    
    Close nFile
    
    Close nFile2
    
    Kill strFileName
    
    Screen.MousePointer = vbNormal
    
    Exit Sub
Erro:
    FileCopy strFileName, strFileLog
    lblMessage.Caption = "Erro ao acessar porta de impress�o"
    Resume Next
    
End Sub








Private Sub cmdOcultar_Click()
    Me.Visible = False
End Sub

Private Sub Form_Load()
    
    Me.Left = Screen.Width - Me.Width
    Me.Top = Screen.Height - Me.Height - 500
    
    
    Adicionar_Systray
End Sub

Private Sub Form_Resize()
    If Me.WindowState = vbMinimized Then Me.Visible = False
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Cancel = True
    Me.WindowState = vbMinimized
End Sub

Private Sub tmrPRINT_inbox_Timer()
    PRINT_inbox_ler
End Sub






Private Sub m_frmSysTray_MenuClick(ByVal lIndex As Long, ByVal sKey As String)
   Select Case sKey
   Case "open"
      Me.WindowState = vbNormal
      Me.Show
      Me.Top = Screen.Height - Me.Height - 500
      Me.ZOrder
   Case "close"
      Unload frmMain
      Remover_SysTray
      End
   Case "executar"
      Me.Show
      Me.ZOrder
   Case Else
      MsgBox "Clicked item with key " & sKey, vbInformation
   End Select
    
End Sub

Private Sub m_frmSysTray_SysTrayDoubleClick(ByVal eButton As MouseButtonConstants)
    On Error Resume Next
    m_frmSysTray_MenuClick 0, "open"
End Sub

Private Sub m_frmSysTray_SysTrayMouseDown(ByVal eButton As MouseButtonConstants)
    If (eButton = vbRightButton) Then
        m_frmSysTray.ShowMenu
    End If
End Sub

Private Sub Adicionar_Systray()
Set m_frmSysTray = New frmSysTray
  With m_frmSysTray
      .AddMenuItem "&Exibir MW PRINT", "open", True
      '.AddMenuItem "-"
      '.AddMenuItem "&Executar", "executar"
      .AddMenuItem "&Fechar", "close"
      
      .ToolTip = "MW PRINT"
  End With
  'm_frmSysTray.IconHandle = Principal.imgIcon(0).Picture.Handle
End Sub

Private Sub Remover_SysTray()
  Unload m_frmSysTray
  Set m_frmSysTray = Nothing
End Sub
