Attribute VB_Name = "modUtil"
'fnc_config_ler
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Private Declare Function GetPrivateProfileInt Lib "kernel32" Alias "GetPrivateProfileIntA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal nDefault As Long, ByVal lpFileName As String) As Long
Private Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As String, ByVal lpFileName As String) As Long
'GetShortPath
Public Declare Function GetShortPathName Lib "kernel32" Alias "GetShortPathNameA" (ByVal lpszLongPath As String, ByVal lpszShortPath As String, ByVal lBuffer As Long) As Long

Public Function fnc_config_ler(Chave As String) As String
    Dim buf As String * 256
    Dim length As Long

    length = GetPrivateProfileString( _
        "CONFIG", Chave, "erro", _
        buf, Len(buf), App.Path & "\config.ini")
    
    fnc_config_ler = Left(buf, length)
End Function

Public Function fnc_config_gravar(Chave As String, Valor As String) As Boolean
    'grava o valor da chave
    'no arquivo config.ini
    On Error GoTo ex
    
    WritePrivateProfileString _
        "CONFIG", Chave, _
        Valor, App.Path & "\config.ini"
                               
ex:
    If Err.Number = 0 Then
        ConfigGravar = True
    Else
        ConfigGravar = False
        MsgBox Err.Description
    End If
        
End Function

Public Function GetShortPath(strFileName As String) As String
  'KPD-Team 1999
  'URL: http://www.allapi.net/
  'E-Mail: KPDTeam@Allapi.net
  Dim lngRes As Long, strPath As String
  'Create a buffer
  strPath = String$(165, 0)
  'retrieve the short pathname
  lngRes = GetShortPathName(strFileName, strPath, 164)
  'remove all unnecessary chr$(0)'s
  GetShortPath = Left$(strPath, lngRes)
End Function
