<?php

	$rsStatusFiltro = mysql_query("SELECT * FROM tblsistema WHERE fldParametro = 'pedido_filtro'");
	$rowStatusFiltro = mysql_fetch_array($rsStatusFiltro);
	$statusFiltro = $rowStatusFiltro['fldValor'];
	
	// pra pegar os valores padrao do bd
	$_SESSION['sel_cliente_pedido_status_pagamento'] = (isset($_SESSION['sel_cliente_pedido_status_pagamento'])) 	? $_SESSION['sel_cliente_pedido_status_pagamento'] 	: $statusFiltro;
	$_SESSION['sel_cliente_pedido_convertido'] 		 = (isset($_SESSION['sel_cliente_pedido_convertido']))			? $_SESSION['sel_cliente_pedido_convertido']		: '1';

	//recebendo a data do calend�rio de per�odo
	if(isset($_POST['txt_calendario_data_inicial']) && isset($_POST['txt_calendario_data_final'])){
	    $_SESSION['txt_data_calendario1'] = (isset($_POST['txt_calendario_data_inicial'])) 	? $_POST['txt_calendario_data_inicial'] : ''; //txt_data_calendario1
	    $_SESSION['txt_data_calendario2'] = (isset($_POST['txt_calendario_data_final'])) 	? $_POST['txt_calendario_data_final'] : ''; //txt_data_calendario2
	}
	
	//memorizar os filtros para exibi��o nos selects
	if(isset($_POST['btn_limpar'])){
		$_SESSION['txt_cliente_pedido_cod_venda']	= "";
		$_SESSION['txt_cliente_pedido_nfe_numero']  = "";
		$_SESSION['sel_cliente_pedido_status']		= "todos";
		$_SESSION['sel_cliente_pedido_status_pagamento'] = $statusFiltro; 
		$_SESSION['txt_cliente_pedido_cliente'] 	= "";
		$_SESSION['sel_cliente_pedido_funcionario'] = "";
		$_SESSION['sel_cliente_pedido_convertido'] 	= "1";
		$_SESSION['sel_cliente_pedido_veiculo'] 	= "";
		$_SESSION['txt_data_calendario1'] 			= "";
		$_SESSION['txt_data_calendario2'] 			= "";
		$_POST['chk_cliente_selecionado'] 			=  false;
	}
	else{
		$_SESSION['txt_cliente_pedido_cod_venda'] 			= (isset($_POST['txt_cliente_pedido_cod_venda']) 		? $_POST['txt_cliente_pedido_cod_venda'] 		: $_SESSION['txt_cliente_pedido_cod_venda']);
		$_SESSION['txt_cliente_pedido_nfe_numero'] 			= (isset($_POST['txt_cliente_pedido_nfe_numero']) 		? $_POST['txt_cliente_pedido_nfe_numero'] 		: $_SESSION['txt_cliente_pedido_nfe_numero']);
		$_SESSION['sel_cliente_pedido_status'] 				= (isset($_POST['sel_cliente_pedido_status']) 			? $_POST['sel_cliente_pedido_status'] 			: $_SESSION['sel_cliente_pedido_status']);
		$_SESSION['sel_cliente_pedido_status_pagamento'] 	= (isset($_POST['sel_cliente_pedido_status_pagamento']) ? $_POST['sel_cliente_pedido_status_pagamento'] : $_SESSION['sel_cliente_pedido_status_pagamento']);
		$_SESSION['txt_cliente_pedido_cliente'] 			= (isset($_POST['txt_cliente_pedido_cliente']) 			? $_POST['txt_cliente_pedido_cliente'] 			: $_SESSION['txt_cliente_pedido_cliente']);
		$_SESSION['sel_cliente_pedido_funcionario'] 		= (isset($_POST['sel_cliente_pedido_funcionario']) 		? $_POST['sel_cliente_pedido_funcionario'] 		: $_SESSION['sel_cliente_pedido_funcionario']);
		$_SESSION['sel_cliente_pedido_convertido'] 			= (isset($_POST['sel_cliente_pedido_convertido']) 		? $_POST['sel_cliente_pedido_convertido'] 		: $_SESSION['sel_cliente_pedido_convertido']);
		$_SESSION['sel_cliente_pedido_veiculo'] 			= (isset($_POST['sel_cliente_pedido_veiculo']) 			? $_POST['sel_cliente_pedido_veiculo'] 			: $_SESSION['sel_cliente_pedido_veiculo']);
		$_SESSION['txt_data_calendario1'] 					= (isset($_POST['txt_data_calendario1']) 				? $_POST['txt_data_calendario1']				: $_SESSION['txt_data_calendario1']);
		$_SESSION['txt_data_calendario2'] 					= (isset($_POST['txt_data_calendario2']) 				? $_POST['txt_data_calendario2'] 				: $_SESSION['txt_data_calendario2']);
	}

?>
<form id="frm-filtro" action="index.php?p=cliente_detalhe&id=<?=trim($_GET['id']);?>&modo=pedido&aba=listar" method="post">
	<fieldset>
  	<legend>Buscar por:</legend>
    <ul>
    	<li>
<?			$codigo_pedido = ($_SESSION['txt_cliente_pedido_cod_venda'] ? $_SESSION['txt_cliente_pedido_cod_venda'] : "c&oacute;digo");
?>      	<input style="width: 80px" type="text" name="txt_cliente_pedido_cod_venda" id="txt_cliente_pedido_cod_venda" onfocus="limpar (this,'c&oacute;digo');" onblur="mostrar (this, 'c&oacute;digo');" value="<?=$codigo_pedido?>"/>
		</li>
<?		if($_SESSION["sistema_nfe"] > 0){                    
?>        
  			<li>
<?				$numero_nota = ($_SESSION['txt_cliente_pedido_nfe_numero'] ? $_SESSION['txt_cliente_pedido_nfe_numero'] : "NFe");
				($_SESSION['txt_cliente_pedido_nfe_numero'] == "NFe") ? $_SESSION['txt_cliente_pedido_nfe_numero'] = '' : '';
?>      		<input style="width: 80px" type="text" name="txt_cliente_pedido_nfe_numero" id="txt_cliente_pedido_nfe_numero" onfocus="limpar (this,'NFe');" onblur="mostrar (this, 'NFe');" value="<?=$numero_nota?>"/>
			</li>
<?		}
?>
        <li>
            <select id="sel_cliente_pedido_status_pagamento" name="sel_cliente_pedido_status_pagamento" style="width: 120px" >
                <option <?=($_SESSION['sel_cliente_pedido_status_pagamento'] == "aberto")	? 'selected="selected"' : '' ?> value="aberto">	 em aberto</option>
                <option <?=($_SESSION['sel_cliente_pedido_status_pagamento'] == "total") 	? 'selected="selected"' : '' ?> value="total">	 pago total</option>
                <option <?=($_SESSION['sel_cliente_pedido_status_pagamento'] == "parcial") 	? 'selected="selected"' : '' ?> value="parcial"> pago parcial</option>
                <option <?=($_SESSION['sel_cliente_pedido_status_pagamento'] == "vencidos") ? 'selected="selected"' : '' ?> value="vencidos">vencidos</option>
                <option <?=($_SESSION['sel_cliente_pedido_status_pagamento'] == "todos") 	? 'selected="selected"' : '' ?> value="todos">	 todos</option>
			</select>
		</li>
        <li>
            <select style="width:180px" id="sel_cliente_pedido_funcionario" name="sel_cliente_pedido_funcionario" >
                <option value="">Funcion&aacute;rio</option>
<?				$rsFuncionario = mysql_query("select * from tblfuncionario ORDER BY fldNome ASC");
				while($rowFuncionario= mysql_fetch_array($rsFuncionario)){
?>					<option <?=($_SESSION['sel_cliente_pedido_funcionario'] == $rowFuncionario['fldId']) ? 'selected="selected"' : '' ?> value="<?= $rowFuncionario['fldId'] ?>"><?= $rowFuncionario['fldNome'] ?></option>
<?				}
?>			</select>
		</li>
        <li>
      		<label for="txt_data_calendario1">Per&iacute;odo: </label>
<?			$data1 = ($_SESSION['txt_data_calendario1'] ? $_SESSION['txt_data_calendario1'] : "");
?>     		<input title="Data inicial" style="text-align:center;width: 70px" type="text" name="txt_data_calendario1" id="txt_data_calendario1" class="calendario-mask" value="<?=$data1?>"/>
      	</li>
		<li>
<?			$data2 = ($_SESSION['txt_data_calendario2'] ? $_SESSION['txt_data_calendario2'] : "");
?>     		<input title="Data final" style="text-align:center;width: 70px" type="text" name="txt_data_calendario2" id="txt_data_calendario2" class="calendario-mask" value="<?=$data2?>"/>
			<a href="pedido_calendario_periodo,<?=format_date_in($data1) . ',' . format_date_in($data2)?>,pedido, <?=$_GET['id'];?>,pedido&aba=listar" id="exibir-calendario" title="Exibir calend&aacute;rio" class="modal calendario-modal" rel="600-320"></a>
      	</li>
<?		if($_SESSION["sistema_nfe"] > 0){                    
?>			<li>
                <select style="background:#FEFAC0;width:150px" id="sel_cliente_pedido_convertido" name="sel_cliente_pedido_convertido" >
                    <option <?=($_SESSION['sel_cliente_pedido_convertido'] == '1')? 'selected="selected"' : '' ?> value="1">vendas ativas</option>
                    <option <?=($_SESSION['sel_cliente_pedido_convertido'] == '2')? 'selected="selected"' : '' ?> value="2">vendas convertidas</option>
                    <option <?=($_SESSION['sel_cliente_pedido_convertido'] == '3')? 'selected="selected"' : '' ?> value="3">vendas NFe</option>
                    <option <?=($_SESSION['sel_cliente_pedido_convertido'] == '4')? 'selected="selected"' : '' ?> value="4">todas</option>
                </select>
            </li>
<?		}
		if($_SESSION['sistema_tipo'] == 'automotivo'){        
?> 			<li>
                <select style="width:150px" id="sel_cliente_pedido_veiculo" name="sel_cliente_pedido_veiculo" >
                    <option value="">selecionar placa</option>
<?					$rsVeiculo = mysql_query("SELECT * FROM tblcliente_veiculo WHERE fldCliente_Id = $cliente_id ORDER BY fldPlaca ASC");
					while($rowVeiculo = mysql_fetch_array($rsVeiculo)){
?>	            		<option <?=($_SESSION['sel_cliente_pedido_veiculo'] == $rowVeiculo['fldId'])? 'selected="selected"' : '' ?> value="<?=$rowVeiculo['fldId']?>"><?=$rowVeiculo['fldPlaca']?></option>
<?					}
?>				</select>
			</li>
<?		}
?>		
        <li style="float:right">	
        	<button type="submit" name="btn_exibir" title="Exibir">Exibir</button>
		</li>
        <li style="float:right">
	        <button type="submit" name="btn_limpar" title="Limpar Filtro">Limpar filtro</button>
		</li>
    </ul>
  </fieldset>
</form>

<?
	if(format_date_in($_SESSION['txt_data_calendario1']) != ""){

		if(format_date_in($_SESSION['txt_data_calendario2']) != ""){
			$filtro .= " and tblpedido.fldPedidoData between '".format_date_in($_SESSION['txt_data_calendario1'])."' and '".format_date_in($_SESSION['txt_data_calendario2'])."'";
		}
		else{
			$filtro .= " and tblpedido.fldPedidoData = '".format_date_in($_SESSION['txt_data_calendario1'])."'";
		}
	}
	
	if(is_numeric($_SESSION['txt_cliente_pedido_cod_venda'])){

		$filtro .= " and tblpedido.fldId = '".$_SESSION['txt_cliente_pedido_cod_venda']."'";
	}
	
	if(($_SESSION['txt_cliente_pedido_nfe_numero']) != ""){
		$filtro .= "AND (tblNota1.fldNumero = '".$_SESSION['txt_cliente_pedido_nfe_numero']."' OR tblNota2.fldNumero = '".$_SESSION['txt_cliente_pedido_nfe_numero']."')";
		
	}
	
	if(($_SESSION['sel_cliente_pedido_status']) != ""){
		if(($_SESSION['sel_cliente_pedido_status']) != "todos"){
			$filtro .= " and tblpedido.fldStatus = '".$_SESSION['sel_cliente_pedido_status']."'";
		}
	}

	if(($_SESSION['sel_cliente_pedido_status_pagamento']) != ""){
		//$filtro_excluido .= " and (tblpedido_parcela.fldExcluido is null or tblpedido_parcela.fldExcluido = 0)";
		switch($_SESSION['sel_cliente_pedido_status_pagamento']){
			case "aberto":
				$filtro_status = " HAVING (fldValorBaixa = 0 or fldValorBaixa is NULL)";
			break;
			
			case "total":
				$filtro_status = " HAVING (fldValorBaixa + fldBaixaDesconto) >= fldTotalParcelas";
			break;

			case "parcial":
				$filtro_status = " HAVING fldTotalParcelas > fldValorBaixa AND fldValorBaixa > 0";
			break;

			case "vencidos":
				$filtro_status = " HAVING ((fldTotalParcelas > fldValorBaixa) or fldValorBaixa is Null) AND fldVencimento < '".date("Y-m-d")."'";
			break;

			case "todos":
				$filtro_status = "";
			break;
		}
	}
	
	switch($_SESSION['sel_cliente_pedido_convertido']){
		
		case '1':
			$sel_bg	 = '#FC0';
			$filtro .= "AND tblpedido.fldTipo_Id != 3 AND (tblpedido.fldPedido_Destino_Nfe_Id IS NULL OR tblpedido.fldPedido_Destino_Nfe_Id <= 0)";
		break;
		case '2':
			$filtro .= "AND tblpedido.fldTipo_Id != 3 AND (tblpedido.fldPedido_Destino_Nfe_Id > 0)";
		break;
		case '3':
			$filtro .= "AND tblpedido.fldTipo_Id = 3";
		break;
		case '4':
			$filtro .= "";
		break;
	}
	
	if(($_SESSION['txt_cliente_pedido_cliente']) != ""){
		$cliente = addslashes($_SESSION['txt_cliente_pedido_cliente']); // no caso de aspas, pra nao dar erro na consulta
		
		if($_POST['chk_cliente_selecionado'] == true){
			$filtro .= "and (tblcliente.fldNome like '%".$cliente."%'";
			$filtro .= " or tblcliente.fldNomeFantasia like '%".$cliente."%')";
		}else{
			$filtro .= "and (tblcliente.fldNome like '".$cliente."%'";
			$filtro .= " or tblcliente.fldNomeFantasia like '".$cliente."%')";
		}
	}
				
	if(($_SESSION['sel_cliente_pedido_funcionario']) != ""){
		$filtro .= "AND tblpedido_funcionario_servico.fldFuncionario_Id = '".$_SESSION['sel_cliente_pedido_funcionario']."'";
	}
				
	if(($_SESSION['sel_cliente_pedido_veiculo']) != ""){
		$filtro .= "AND tblpedido.fldVeiculo_Id = '".$_SESSION['sel_cliente_pedido_veiculo']."'";
	}
	
	$filtro .= " GROUP BY tblpedido.fldId $filtro_status";

	//transferir para a sess�o
	$_SESSION['filtro_cliente_pedido'] = $filtro;

?>