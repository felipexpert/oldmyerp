<?
	require("cliente_pedido_item_filtro.php");
	
	if(isset($_POST['btn_action'])){
		require("cliente_pedido_item_action.php");
	}

/**************************** ORDER BY *******************************************/
	$filtroOrder  = 'tblpedido.fldPedidoData';
	$class 		 = 'asc';
	$order_sessao = explode(" ", $_SESSION['order_cliente_pedido_item']);
	if(isset($_GET['order'])){
		switch($_GET['order']){
			
			case 'codigo'	:  $filtroOrder = "tblproduto.fldCodigo+0"; 		break;
			case 'item'		:  $filtroOrder = "tblpedido_item.fldDescricao";	break;
			case 'venda'	:  $filtroOrder = "tblpedido.fldId";				break;
			case 'data'		:  $filtroOrder = "tblpedido.fldPedidoData";		break;
		}
		if($order_sessao[0] == $filtroOrder){
			$class = ($order_sessao[1] == 'asc') ? 'desc' : 'asc';
		}
	}
	
	//definir icone para ordem
	$_SESSION['order_cliente_pedido_item'] = (!$_SESSION['order_cliente_pedido_item'] || $_GET['order']) ? $filtroOrder.' '.$class : $_SESSION['order_cliente_pedido_item'];
	$pag	= ($_GET['pagina'])? '&pagina='.$_GET['pagina'] : ''; 
	$raiz 	= "index.php?p=cliente_detalhe&id=$cliente_id&modo=pedido&aba=item$pag&amp;order=";
	
	$order_sessao = explode(" ", $_SESSION['order_cliente_pedido_item']);
	$filtroOrder  = $order_sessao[0]; //pra poder comparar na listagem e exibir a class
/**************************** PAGINA��O *******************************************/
	$sSQL = "SELECT tblpedido_item.*,
			SUM(tblpedido_item_pago.fldQuantidade) as fldQuantidadePaga,
			tblpedido.fldPedidoData,
			tblpedido.fldTipo_Id,
			tblproduto.fldCodigo
			FROM tblpedido_item
			
			RIGHT JOIN tblpedido ON tblpedido.fldId = tblpedido_item.fldPedido_Id
			LEFT  JOIN tblpedido_item_pago ON tblpedido_item_pago.fldItem_Id = tblpedido_item.fldId AND tblpedido_item_pago.fldExcluido = 0
			INNER JOIN tblproduto ON tblproduto.fldId = tblpedido_item.fldProduto_Id
			WHERE tblpedido.fldCliente_Id = $cliente_id 
			
			AND (tblpedido.fldPedido_Destino_Nfe_Id IS NULL OR tblpedido.fldPedido_Destino_Nfe_Id <= 0)
			AND tblpedido.fldExcluido = '0' ".$_SESSION['filtro_cliente_pedido_item']." GROUP BY tblpedido_item.fldId, tblpedido_item.fldProduto_Id, tblpedido_item.fldPedido_Id
			$filtro_having
			ORDER BY ".$_SESSION['order_cliente_pedido_item'];
			
		
	$rsTotal = mysql_query($sSQL);
	echo mysql_error();
	$rowsTotal = mysql_num_rows($rsTotal);

	//defini��o dos limites
	$limite 	= 90;
	$n_paginas 	= 7;
	$total_paginas = ceil(mysql_num_rows($rsTotal) / $limite);
	
	if(isset($_GET["pagina"]) && $_GET["pagina"] > $total_paginas){
		$inicio = 0;
	}elseif(isset($_GET['pagina'])){
		$inicio = ($_GET['pagina'] - 1) * $limite;
	}else{
		$inicio = 0;
	}
	
	$sSQL 			.= " limit " . $inicio . "," . $limite;
	$rsPedidoItem 	= mysql_query($sSQL);
	$pagina 		= ($_GET['pagina'] ? $_GET['pagina'] : "1");
	
#########################################################################################
?>
    <form class="table_form" id="frm_pedido" action="" method="post">
    	<div id="table">
            <div id="table_cabecalho">
                <ul class="table_cabecalho">
                    <li class="order" style="width:80px; text-align:right">
                    	<a <?= ($filtroOrder == 'tblproduto.fldCodigo+0') 		? "class='$class'" : '' ?> style="width:65px" href="<?=$raiz?>&order=codigo">C&oacute;d.</a>
                    </li>
                    <li class="order" style="width:535px">
                    	<a <?= ($filtroOrder == 'tblpedido_item.fldDescricao') 	? "class='$class'" : '' ?> style="width:520px" href="<?=$raiz?>&order=item">Item</a>
                    </li>
                    <li class="order" style="width:60px">
                    	<a <?= ($filtroOrder == 'tblpedido.fldId') 				? "class='$class'" : '' ?> style="width:45px" href="<?=$raiz?>&order=venda">Venda</a>
                    </li>
                    <li style="width:60px; text-align:right">Qtde</li>
                    <li style="width:60px; text-align:right">Pago</li>
                    <li class="order" style="width:80px">
                    	<a <?= ($filtroOrder == 'tblpedido.fldPedidoData') 		? "class='$class'" : '' ?> style="width:65px" href="<?=$raiz?>&order=data">Emiss&atilde;o</a>
                    </li>
                    <li style="width:20px">&nbsp;</li>
                    <li style="width:20px"><input type="checkbox" name="chk_todos" id="chk_todos" /></li>
                </ul>
            </div>
            <div id="table_container">       
                <table id="table_general" class="table_general" summary="Lista de itens">
                	<tbody>
<?						$id_array = array();
						$n = 0;
                        $linha = "row";
                        while ($rowPedidoItem = mysql_fetch_array($rsPedidoItem)){
						
							$item_id = $rowPedidoItem['fldId'];
							$status  = fnc_status_item_pago($item_id);
							
							
							//linkar para o detalhe da venda ou NFe
							$paginaLink = redirecionarPagina($rowPedidoItem['fldTipo_Id'], 'venda');
							
?>							<tr class="<?= $linha; ?>">
                                <td style="width:80px;text-align:right" class="cod">
									<span title="<?=$status?>" class="item_<?=$status?>" ><?=$rowPedidoItem['fldCodigo']?></span>
								</td>
                                <td style="width:530px;padding-left:10px"><?=$rowPedidoItem['fldDescricao']?></td>
                                <td style="width:60px;text-align:right"><a href="index.php?p=<?=$paginaLink;?>&id=<?=$rowPedidoItem['fldPedido_Id']?>" title="abrir venda"><?=str_pad($rowPedidoItem['fldPedido_Id'],5, "0", STR_PAD_LEFT)?></a></td>
                                <td style="width:60px;text-align:right">
									<span title="<?=$status?>" class="item_<?=$status?>" ><?=format_number_out($rowPedidoItem['fldQuantidade'])?></span></<br />
								</td>
                                <td style="width:60px;text-align:right">
									<span title="<?=$status?>" class="item_<?=$status?>" ><?=format_number_out($rowPedidoItem['fldQuantidadePaga'])?></span></<br />
								</td>
                                <td style="width:80px;text-align:center"><?=format_date_out($rowPedidoItem['fldPedidoData'])?></td>
                                <td style="width:20px">
<?									if($status != 'pago'){                                
?>	                                	<a class="dar-baixa modal" href="cliente_pedido_item_baixa,<?=$item_id.','.$cliente_id?>" rel="450-220" title="baixar"></a>
<?									}								
?>                              </td>
								<td style="width:20px;text-align:center"><input type="checkbox" name="chk_item_<?=$item_id?>" id="chk_item_<?=$item_id?>" title="selecionar o registro posicionado" /></td>
                       		</tr>
<?                     		$linha = ($linha == "row") ? "dif-row" : "row";
							$id_array[$n] = $item_id;
							$n += 1;
                		}
?>		 			</tbody>
				</table>
            </div>
            <input type="hidden" name="hid_array" id="hid_array" value="<?=urlencode(serialize($id_array))?>" />
            <input type="hidden" name="hid_action" id="hid_action" value="true" />
            
            <div id="table_action">
                <ul id="action_button">
                	<li><input type="submit" name="btn_action" id="btn_estorno" value="estorno" title="Estornar registro(s) selecionado(s)"  onclick="return confirm('O valor desse item tamb&eacute;m ser&aacute; estornado. Deseja continuar?')" /></li>
				</ul>
        	</div>
            
            <div id="table_paginacao">
<?				$paginacao_destino = "?p=cliente_detalhe&id=$cliente_id&modo=pedido&aba=item";
				include("paginacao.php")
?>          </div>   
            <div class="table_registro">
            	<span>Exibindo registros <?=($pagina*$limite-$limite+1).' a '.($pagina*$limite-$limite+$rows)?> do total de <?=$rowsTotal?></span>
            </div>     
        </div>
	</form>