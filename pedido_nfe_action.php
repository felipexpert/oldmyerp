<?Php
		$id_array = unserialize(urldecode($_POST["hid_array"]));
		$n = 0;
		$id_query_array = array();
		while($id_array[$n]){
			if($_POST["chk_pedido_nfe_".$id_array[$n]]){
				$id_query_array[$n] = $id_array[$n];
				$validate = true;
				$filtro_id .= $id_array[$n] . ", ";
			}
			$n += 1;
		}

		$filtro_id = substr($filtro_id, 0 ,strlen($filtro_id) - 2);
		if(!$validate && $_POST["btn_action"] && $_POST["btn_action"] != 'imprimir' && $_POST['btn_action'] != 'confirmar'){
?>			<div class="alert">
				<p class="alert">voc&ecirc; deve selecionar algum registro antes de solicitar uma a&ccedil;&atilde;o</p>
			</div>
<?		}elseif (count($id_query_array) > 1 && $_POST["btn_action"] && $_POST["btn_action"] == 'alterar numero'){
?>			<div class="alert">
				<p class="alert">selecione apenas um registro</p>
			</div>
<?		}else {
			switch($_POST["btn_action"]) {
/*****************************************************************/
				case "excluir":
					
					//Verificar se não possui alguma parcela paga da nfe
					$msgSingular 	= 'NFe {nNota}, n&atilde;o foi exclu&iacute;da, pois já possui parcelas pagas!';
					$msgPlural   	= 'NFes {nNota}, n&atilde;o foram exclu&iacute;das, pois já possuem parcelas pagas!';
					$id_query_array = verificaNFeParcela($id_query_array, $msgSingular, $msgPlural);
					
					if($id_query_array != false) {
						
						//VERIFICA SE O PEDIDO NAO ESTAVA EM ORCAMENTO PRA DEVOLVER OS ITENS NO ESTOQUE
						$rsPedido = mysql_query("SELECT * FROM tblpedido WHERE fldId in (" . join(",", $id_query_array) . ")");
						while($rowPedido = mysql_fetch_array($rsPedido)) {
								//devolver o produto para estoque para pedidos que forem excluidos
								$rsItem = mysql_query("SELECT tblpedido_item.fldQuantidade, tblpedido_item.fldPedido_Id, tblpedido_item.fldId as fldItem_Id, tblpedido_item.fldEstoque_Id, tblproduto.fldId
													  FROM tblpedido_item INNER JOIN tblproduto ON tblpedido_item.fldProduto_Id = tblproduto.fldId
													  WHERE tblpedido_item.fldPedido_Id = " . $rowPedido['fldId']);
								
								//número da nota para inserir junto com a movimentação do estoque
								$nNota = mysql_fetch_array(mysql_query("SELECT fldnumero FROM tblpedido_fiscal WHERE fldpedido_id = " . $rowPedido['fldId']));
								
								while($rowItem = mysql_fetch_array($rsItem)) {
										
									$notaId   	= $nNota['fldnumero'];
									$itemId 	= $rowItem['fldItem_Id'];
									$produtoId  = $rowItem['fldId'];
									$quantidade = $rowItem['fldQuantidade'];
									$estoque_id = $rowItem['fldEstoque_Id'];
									
									//$tipo_id = 9 //'nfe excluída'
									fnc_estoque_movimento_lancar($produtoId, '', $quantidade, '', 9, $notaId, $estoque_id, $estoque_id, '', $itemId);
									
								}
						}
						
						//rotina para pegar todos os ids, da nova NFe e das vendas de origens para poder "excluir"
						$rsPedidoOrigem = mysql_query("SELECT fldId FROM tblpedido WHERE fldPedido_Destino_Nfe_Id IN (" . join(",", $id_query_array) . ")");
						$x = 0;
						while($rowPedidoOrigem = mysql_fetch_array($rsPedidoOrigem)) {
							$pedidoOrigem[$x] = $rowPedidoOrigem['fldId'];
							$x++;
						}
						
						//verificação para evitar erros com NFes 'diretas' (não possuem origens)
						if(count($pedidoOrigem) > 0) $id_pedidos = implode(',', array_merge($pedidoOrigem, $id_query_array));
						else $id_pedidos = implode(',', $id_query_array);
						
						mysql_query("UPDATE tblpedido set fldExcluido = '1' WHERE fldId in ($id_pedidos)");
						mysql_query("UPDATE tblpedido_item set fldExcluido = '1' WHERE fldPedido_Id in ($id_pedidos)");
						mysql_query("UPDATE tblpedido_parcela set fldExcluido = '1' WHERE fldPedido_Id in ($id_pedidos)");
						//mysql_query("DELETE FROM tblpedido_fiscal WHERE fldPedido_id in ($id_pedidos)");
						
						unset($rsPedidoOrigem, $x, $rowPedidoOrigem, $pedidoOrigem, $id_pedidos, $notaId, $produtoId, $quantidade, $estoque_id);
						
					}
					
					header("location:index.php?p=pedido_nfe");
					
				break;
/*****************************************************************/
				case "imprimir":
?>					<script language="javascript">
						window.open("pedido_nfe_relatorio.php");
					</script>
<?				break;
/*****************************************************************/
				case "exportar":
					$data = strtotime("now");
					$data = date('Y-m-d H:i:s',$data);
					mysql_query("UPDATE tblpedido_fiscal SET flddata_exportacao = '$data' WHERE fldpedido_id in (" . join(",", $id_query_array) . ")");
?>					<script language="javascript">
						window.open("pedido_nfe_exportar.php?ids=<?=join(",", $id_query_array)?>");
					</script>
<?				break;
/*****************************************************************/
				case "transmitir":
?>					<a id="modal_transmitir" href="pedido_nfe_transmitir,<?=join(";", $id_query_array)?>" class="modal" rel="780-220"></a>
					<script language="javascript">
						winModal($('#modal_transmitir'));
					</script>
<?				break;
/*****************************************************************/
				case "danfe":
?>					<script language="javascript">
						var id_nfe = '<?= $id_query_array?>';
						window.open("pedido_nfe_danfe.php?id="+id_nfe);
					</script>
<?				break;
				
/***************************************************************/
				case "alterar numero": 
?>
					<script type="text/javascript">
                        $(document).ready(function() {
                            $('#alterar_numero_verificado').trigger('click'); 
                        });
                    </script>
<?						
				break;
/***************************************************************/
				case "confirmar":
						
						$numero_atual 			= $_POST['hid_nfe_numero_atual'];
						$motivo_alteracao		= $_POST['sel_alterar_motivo'];
						$pedido_id				= $_POST['hid_pedido_id'];
						$observacao_alteracao	= $_POST['txt_alterar_observacao'];
						if(!is_null($_POST['rad_nnfe_n_perdido'])) { $numeroAusenteNFe = $_POST['rad_nnfe_n_perdido']; }
						$nfe_numero				= (isset($numeroAusenteNFe)) ? $numeroAusenteNFe : fnc_sistema('nfe_numero_nota');
						if($numeroAusenteNFe == fnc_sistema('nfe_numero_nota')){$numeroAusenteNFe = null;}
						$data_atual = date("Y-m-d");
						$hora_atual = date("H:i:s");
						$usuario_id = $_SESSION['usuario_id'];
						
						$queryInutiliza = mysql_query("INSERT INTO tblpedido_nfe_numero_inutilizado (fldNFE_Numero, fldMotivo, fldObservacao, fldData, fldHora, fldUsuario_Id) VALUES ('$numero_atual', '$motivo_alteracao', '$observacao_alteracao', '$data_atual', '$hora_atual', '$usuario_id')");
						$queryUpdate = mysql_query("UPDATE tblpedido_fiscal SET fldnumero = '$nfe_numero' WHERE fldpedido_id = '$pedido_id'");
						
						if (!$queryInutiliza){ 
?>
                            <div class="alert">
                                <p class="alert">erro ao inutilizar o numero atual</p>
                            </div>
<?
                        }elseif (!$queryUpdate){ 
?>
							<div class="alert">
								<p class="alert">erro ao salvar registros no banco</p>
							</div>
								
<?						}else{
							if(!isset($numeroAusenteNFe)){
								$nfe_numero += 1;
								fnc_sistema_update('nfe_numero_nota', $nfe_numero);
							}
?>
                            <div class="alert">
                                <p class="ok">registro salvo com sucesso</p>
                            </div>
<?                        }
				break;
/***************************************************************/
				case "desfazer importação":
					
					//verificar se não possui alguma parcela paga da nfe
					$msgSingular 	= 'NFe {nNota}, n&atilde;o pode ser desfeita, pois já possui parcelas pagas!';
					$msgPlural   	= 'NFes {nNota}, n&atilde;o podem ser desfeitas, pois já possuem parcelas pagas!';
					$id_query_array = verificaNFeParcela($id_query_array, $msgSingular, $msgPlural);
					
					if($id_query_array != false) {
						
						//rotina para pegar todos os ids, da nova nota e das vendas de origens para poder "excluir"
						$rsPedidoOrigem = mysql_query("SELECT fldId, fldPedido_Destino_Nfe_Id FROM tblpedido WHERE fldPedido_Destino_Nfe_Id IN (" . join(",", $id_query_array) . ")");
						
						#ARMAZENA A ORIGEM E O DESTINO(NFE)
						$x = 0;
						while($rowPedidoOrigem = mysql_fetch_array($rsPedidoOrigem)) {
							$pedidoOrigem[$x]  = $rowPedidoOrigem['fldId'];
							$pedidoDestino[$x] = $rowPedidoOrigem['fldPedido_Destino_Nfe_Id'];
							$x++;
						}
						
						if(is_array($pedidoDestino)) {
							$NFeSemOrigem = array_diff($id_query_array, $pedidoDestino);
						}
						
						//exibir erro caso o registro selecionado não possua origem para ser desfeito
						if(!isset($pedidoDestino) || count($NFeSemOrigem) > 0) {
							$rsNFeSemOrigemNumero = mysql_query("SELECT fldnumero FROM tblpedido_fiscal WHERE fldpedido_id IN (" . join(",", $NFeSemOrigem) . ")");
							$x = 0;
							while($rowNFeSemOrigemNumero = mysql_fetch_array($rsNFeSemOrigemNumero)) {
								$NFeNumero[$x]  = $rowNFeSemOrigemNumero['fldnumero'];
								$x++;
							}
							
							//exibir mensagem no singular ou plural
							if(count($NFeNumero) > 1) $msg = 'NFes ' . implode(',', $NFeNumero) . ' não foram desfeitas, pois não foram geradas a partir de vendas comuns!';
							else $msg = 'NFe ' . implode(',', $NFeNumero) . ' não foi desfeita, pois não foi gerada a partir de vendas comuns!';
							
							$_SESSION['msg_alert'] = array('open_markup'  => '<div class="alert">',
														   'close_markup' => '</div>',
														   'content'	  => '<p class="alert">'. $msg .'</p>'
														  );
						}
						
						#APENAS SETA COMO NULO, PARA NAO HAVER MAIS DESTINO DE NOTA
						if(mysql_query("UPDATE tblpedido SET fldPedido_Destino_Nfe_Id = NULL WHERE fldId IN (". implode(',', $pedidoOrigem) .")")) {
							
							//para deletar apenas as NFes, não as origens das mesmas
							$ids_nota = implode(',', $pedidoDestino);
							
							mysql_query("UPDATE tblpedido 			SET fldExcluido = '1' WHERE fldId in ($ids_nota)");
							mysql_query("UPDATE tblpedido_item 		SET fldExcluido = '1' WHERE fldPedido_Id in ($ids_nota)");
							mysql_query("UPDATE tblpedido_parcela 	SET fldExcluido = '1' WHERE fldPedido_Id in ($ids_nota)");
						}
						unset($rsPedidoOrigem, $x, $i, $ids_nota, $indice, $rowPedidoOrigem, $pedidoOrigem, $produto_id, $entrada, $saida, $pedido_id, $estoque_id, $ItemOrigem, $ItemAtual, $arrayControle, $sqlItensAtuais, $sqlItensOrigens, $msgSingular, $msgPlural, $msg);
					}
					
					header("location:index.php?p=pedido_nfe");
					
				break;
/***************************************************************/
				case "download": 
?>					<a id="modal_download" href="pedido_nfe_download,<?=join(";", $id_query_array)?>" class="modal" rel="250-180"></a>
					<script language="javascript">
						winModal($('#modal_download'));
					</script>
<?				break;
				
			}
		}
?>

<?php

	/**
	 * Função para verificar se NFe não possui alguma parcela paga
	 *
	 * @param Array $ids - matriz com os IDs das NFes que serão checadas
	 * @param String $msgSingular - string com a mensagem de retorno no singular. Lembrando da string especial {nNota} que será substituida pelos números da NFe
	 * @param String $msgPlural - string com a mensagem de retorno no plural. Lembrando da string especial {nNota} que será substituida pelos números da NFe
	 * @return Array - matriz com os IDs filtrados. IDs que possuem parcelas pagas são removidos da matriz.
	 */
	function verificaNFeParcela($ids, $msgSingular, $msgPlural) {
		
		$rsParcelasPagas = mysql_query("SELECT tblpedido.fldId, tblpedido_fiscal.fldnumero FROM tblpedido
										INNER JOIN tblpedido_fiscal ON tblpedido_fiscal.fldpedido_id = tblpedido.fldId 
										LEFT JOIN tblpedido_parcela ON tblpedido_parcela.fldPedido_Id = tblpedido.fldId 
										INNER JOIN tblpedido_parcela_baixa ON tblpedido_parcela_baixa.fldParcela_Id = tblpedido_parcela.fldId 
										WHERE tblpedido.fldId IN (" . join(",", $ids) . ") AND tblpedido_parcela_baixa.fldExcluido = 0");
		
		$x = 0;
		while($rowParcelasPagas = mysql_fetch_array($rsParcelasPagas)) {
			$pedidoId[$x]   = $rowParcelasPagas['fldId'];
			$pedidoNota[$x] = $rowParcelasPagas['fldnumero'];
			
			//remover da matriz com os ids de exclusão, as notas que já possuem algum pagamento
			$key = array_search($pedidoId[$x], $ids);
			unset($ids[$key]);
			
			$x++;
		}
		
		//gerando mensagem de erro caso haja alguma NFe com parcela paga
		if(isset($pedidoId) and isset($pedidoNota)) {
			$nNota = implode(',', $pedidoNota);
			
			if(count($pedidoNota) > 1) $msg = $msgPlural;
			else $msg = $msgSingular;
			
			$_SESSION['msg_alert'] = array('open_markup'  => '<div class="alert">',
										   'close_markup' => '</div>',
										   'content'	  => '<p class="alert">'. str_replace('{nNota}', $nNota, $msg) .'</p>'
										  );
		}
		
		unset($rsParcelasPagas, $rowParcelasPagas, $parcelas, $key, $x, $msg, $pedidoId, $pedidoNota, $nNota);
		return (!empty($ids)) ? $ids : false;
		
	}

?>