<? 
		#ABAIXO CRIO O CABECALHO #########################################################################################################################################################
		$tabelaCabecalho =' 
			<tr style="border-bottom: 2px solid">
                <td style="width: 600px"><h1>Relat&oacute;rio de Produtos</h1></td>';
		$tabelaCabecalho2 =' 
            </tr>
            <tr style="margin-bottom:0">
                <td>
                    <table style="width: 580px;margin-bottom:0" class="table_relatorio_dados" summary="Relat&oacute;rio">
                        <tr>
                            <td style="width: 320px;">Raz&atilde;o Social: '.$rowDados['fldNome'].'</td>
                            <td style="width: 200px;">Nome Fantasia: '.$rowDados['fldNome_Fantasia'].'</td>
                            <td style="width: 320px;">CPF/CNPJ: '.$CPF_CNPJDados.'</td>
                            <td style="width: 200px;">Telefone: '.$rowDados['fldTelefone1'].'</td>
                        </tr>
                    </table>	
                </td>
                <td>        
                    <table class="dados_impressao">
                        <tr>
                            <td><b>Data: 			</b><span>'.format_date_out(date("Y-m-d")).'</span></td>
                            <td><b>Hora: 			</b><span>'.format_time_short(date("H:i:s")).'</span></td>
                            <td><b>Usu&aacute;rio: 	</b><span>'.$rowUsuario['fldUsuario'].'</span></td>
                        </tr>
                    </table>
                </td>
            </tr>
			<tr>
				<td style="margin:0"><h2 style="background:#E1E1E1;width:790px;padding-left:10px">'.$rowProduto['fldNome'].'</h2></td>
			</tr>
            <tr>
                <td style="margin:0">
                    <table name="table_relatorio" class="table_relatorio" summary="Relat&oacute;rio">
						<tr style="border:none">
							<td class="valor">C&oacute;d</td>
							<td style="width:235px">Produto</td>
							<td class="qtd">Qtd</td>
							<td class="valor">Vl Unit.</td>
							<td class="valor">Total</td>
							<td class="valor">Vl Compra</td>
							<td class="valor">Total</td>
							<td class="valor">Lucro</td>
							<td class="valor">Lucro (%)</td>
						</tr>
					</table>
				</td>
			</tr>';
			$tabelaCabecalho3 ='
			<tr>
				<td>
					<table class="table_relatorio" summary="Relat&oacute;rio">';
	######################################################################################################################################################################################
		//ja esta fazendo a consulta na pagina anterior
		if(isset($data_inicio) && isset($data_final)){
			$filtro = '  AND tblpedido.fldExcluido = 0 AND (tblpedido.fldPedidoData BETWEEN "'.$data_inicio.'" AND "'.$data_final.'") ';
		}
		
		($produtos == 'todos') ? $filtro_join = $filtro : $filtro_where = $filtro;
		
		$replaceFROM = ',
		tblpedido.fldPedido_Destino_Nfe_Id,
		tblpedido.fldCadastroData,
		tblpedido_item.fldDescricao,
		tblproduto.fldNome,
		(tblpedido_item.fldQuantidade * (tblpedido_item.fldExcluido * -1 + 1)) AS fldQuantidade,
		(tblpedido_item.fldValor * (tblpedido_item.fldExcluido * -1 + 1)) as fldValor_Venda, 
		(tblpedido_item.fldValor_Compra * (tblpedido_item.fldExcluido * -1 + 1)) as fldValor_Compra, 
		(tblpedido_item.fldDesconto * (tblpedido_item.fldExcluido * -1 + 1)) as fldProdutoDesconto
		
		FROM tblproduto
		
		LEFT JOIN tblpedido_item ON tblproduto.fldId = tblpedido_item.fldProduto_Id
		LEFT JOIN tblpedido  ON tblpedido.fldId = tblpedido_item.fldPedido_Id '.$filtro_join;
		
		$replaceWHERE	= 'WHERE tblpedido_item.fldExcluido = 0 AND (tblpedido.fldPedido_Destino_Nfe_Id IS NULL OR tblpedido.fldPedido_Destino_Nfe_Id <= 0) '.$filtro_where.' AND';
		
		//$sSQL 			= str_replace('FROM tblproduto ', $replaceFROM, $_SESSION['produto_relatorio_sql']);
		
		#DEIXAR COM ESSA FORMATACAO PARA PODER PEGAR DA LISTAGEM CORRETAMENTE
		$replace_listar = ',
					 SUM(fldEntrada - fldSaida) as fldEstoqueQuantidade
					 FROM tblproduto 
					 LEFT JOIN tblproduto_estoque_movimento ON tblproduto_estoque_movimento.fldProduto_Id = tblproduto.fldId AND tblproduto_estoque_movimento.fldEstoque_Id > 0
					 ';
		$sSQL 			= str_replace($replace_listar, $replaceFROM, $_SESSION['produto_relatorio_sql']);
					 
		$sSQL 			= str_replace('WHERE', $replaceWHERE, $sSQL);
		$sSQL 			= str_replace('LEFT JOIN tblproduto_compativel ON tblproduto_compativel.fldProduto_Id = tblproduto.fldId', '', $sSQL);
		$sSQL 			= str_replace('GROUP BY tblproduto.fldId', 'GROUP BY tblpedido_item.fldId', $sSQL);
		 
		 
		$rsRelatorio 	= mysql_query($sSQL);
		$rowsRelatorio	= mysql_num_rows($rsRelatorio);
		echo mysql_error();
		$n	 			= 1; #DEFINE O NUMERO DO BLOCO
		$countRegistro 	= 1; #DEFINE CONTAGEM DE ITENS NO WHILE PARA QUEBRA DE BLOCO AO ATINGIR LIMITE 
		$x				= 1; #DEFINE CONTAGEM DE ITENS TOTAIS, PRA SABER SE JA TERMINOU WHILE MAS AINDA FALTA ESPACO
		$limite 		= 44;
		
		//$pgTotal 		= ceil($rowsRelatorio / $limite);
		$p = 1;
		
		while($rowRelatorio = mysql_fetch_array($rsRelatorio)){
			echo mysql_error();
			
			unset($lucro_bruto);
			unset($lucro);
			
			if($produtos == 'todos'){
				$venda_valor 	=  $rowRelatorio['fldValor_Venda'];
				$compra_valor 	=  $rowRelatorio['fldValor_Compra'];
				
				if($venda_valor > 0){
					$lucro_bruto = $venda_valor - $compra_valor;
					$lucro 		= ($lucro_bruto / $venda_valor) * 100;
				}
				$quantidade = "0";
				
			}else{

				$venda_valor 	= $rowRelatorio['fldValor_Venda'];
				$compra_valor 	= $rowRelatorio['fldValor_Compra'];
				
				$compra_desconto = ($compra_valor * $rowCompraValor['fldProdutoDesconto']) / 100;
				$compra_valor 	= $compra_valor - $compra_desconto;
				$compra_total	= number_format($compra_valor,2, '.', '') * $rowRelatorio['fldQuantidade'];
				/////////////////////////////////////////////////////////////////////////////////
			
				$VendaDesconto 	= ($rowRelatorio['fldValor_Venda'] * $rowRelatorio['fldProdutoDesconto']) / 100;
				$venda_valor 	= $venda_valor - $VendaDesconto;
				$vendaTotal 	= number_format($venda_valor,2, '.', '') * $rowRelatorio['fldQuantidade'];
		
				if($vendaTotal > 0){
					$lucro_bruto = $vendaTotal - $compra_total;
					$lucro 		= ($lucro_bruto / $vendaTotal) * 100;
				}
				
				if($rowRelatorio['fldQuantidade'] > 0){
					$quantidade = $rowRelatorio['fldQuantidade'];
				}else{
					$quantidade = "0";
				}
			}
			
			$style = (isset($detalhes) && $detalhes == 'detalhado') ? "style='background:#DFDFDF'" : '';
			
			#CONFERE SE AINDA HE O MESMO PRODUTO OU NAO PARA EXIBIR O TOTAL
			if(isset($produto_codigo) && $produto_codigo != $rowRelatorio['fldCodigo']){
				$item_rows = 0;
				unset($saldoLucroItem);
				unset($saldoQuantidadeItem);
			}else{
				$item_rows += 1;
			}	
			
			//soma total para rodape ########################//
			$saldoVendaTotal 	+= $vendaTotal;
			$saldoCompraTotal 	+= $compra_total;
			$saldoLucroBruto 	+= $lucro_bruto;
			$totalQuantidade 	+= $quantidade;
			
			//rodape por item ########################//
			
			$saldoLucroItem 		+= $lucro_bruto;
			$saldoQuantidadeItem 	+= $quantidade;
			//##############################################//
			
			#CASO ITEM DIFERENTE, IMPRIME TOTAL E ADICIONA AS LINHAS 
			if($item_rows == 0){
				$countRegistro ++;
				$rowsRelatorio ++;
				## CASO DETALHE FAZ O MESMO PROCESSO DE CONTAGEM DAS LINHAS E ADICIONA O CABECALHO ##############################################################################################
				$pagina[$n] .= $armazena;
				if($countRegistro == $limite){
					$countRegistro = 1;
					$n ++;
				}
				$x ++;
			}
			
			#ARMAZENA O RODAPE/TOTAL DO PRODUTO PARA EXIBIR NA ULTIMA LINHA (EH ARMAZENADO SEMPRE O REGISTRO ANTERIOR AO QUE ESTA SENDO EXIBIDO NA LINHA)
			$armazena = '
						<tr '.$style.'>
							<td class="valor">&nbsp;</td>
							<td style="width:218px"><b>'.substr($rowRelatorio['fldNome'],0, 28).'</b></td>
							<td class="valor" style="width:50px">'.format_number_out($saldoQuantidadeItem).'</td>
							<td style="width:355px;text-align:right">'.format_number_out($saldoLucroItem).'</td>
							<td class="valor">&nbsp;</td>
						</tr>';
						
			#SE CHEGAR LIMITE DE LINHAS, MUDA DE 'BLOCO' E RECMECA CONTAGEM, CASO CONTRARIO SÓ REGISTRA O BLOCO
			if($countRegistro == $limite){
				$countRegistro = 1;
				$n ++;
			}elseif($rowsRelatorio == $x && $countRegistro < $limite){ #SE JA TERMINOU O WHILE DE REGISTROS MAS AINDA NAO ATINGIU LIMITE DE LINHAS, CONTINUAR CRIANDO LINHAS ATE O LIMITE
				while($countRegistro <= $limite){ $pagina[$n] .='<tr style="border:0; width:800px"></tr>'; $countRegistro++;}
			}else{
			
				$pagina[$n] .= '
				<tr>
					<td class="valor">'.str_pad($rowRelatorio['fldCodigo'], 6, '0', STR_PAD_LEFT).'</td>
					<td style="width:218px">'.substr($rowRelatorio['fldNome'],0, 28).'</td>
					<td class="valor" style="width:50px">'.format_number_out($quantidade).'</td>
					<td class="valor">'.format_number_out($venda_valor).'</td>
					<td class="valor">'.format_number_out($vendaTotal).'</td>
					<td class="valor">'.format_number_out($compra_valor).'</td>
					<td class="valor">'.format_number_out($compra_total).'</td>
					<td class="valor">'.format_number_out($lucro_bruto).'</td>
					<td class="valor">'.format_number_out($lucro).'</td>
				</tr>';
				$countRegistro ++;
				$x ++;
				
				if($rowsRelatorio == $x){
					$pagina[$n] .= $armazena;					
				}
			}
			$produto_codigo = $rowRelatorio['fldCodigo'];		
		}#END WHILE RELATORIO
	
	
	
	
	#AGORA MANDO GERAR NA TELA PARA IMPRESSAO ############################################################################################################################################
		$x = 1;
		while($x <= $n){
			$tabelaCabecalho1 = ($x == 1)? '<table class="relatorio_print" style="page-break-before:avoid">'.$tabelaCabecalho : '<table class="relatorio_print">'.$tabelaCabecalho;
			#PRIMEIRO BLOCO (LANCADOS) ###################################################################################################################################################
				print $tabelaCabecalho1;
                print '<td style="width: 200px"><p class="pag">p&aacute;g. '.$p.'</p></td>';
				
				print $tabelaCabecalho2;
				print $tabelaCabecalho3;
				echo  $pagina[$x];
?>
						</table>
					</td >
				</tr>
			</table>			
<?			$x ++;
			$p ++;
		}
?>                
    <table name="table_relatorio_rodape" class="table_relatorio_rodape" summary="Relat&oacute;rio">
        <tr>
            <td style="width:50px">&nbsp;</td>
            <td style="width:60px">Quantidade</td>
            <td style="width:80px; text-align:right"><?=format_number_out($totalQuantidade)?></td>
            <td style="width:3px; border-left:1px solid">&nbsp;</td>
            <td style="width:80px">Val. Vendas</td>
            <td style="width:80px; text-align:right"><?=format_number_out($saldoVendaTotal)?></td>
            <td style="width:3px; border-left:1px solid">&nbsp;</td>
            <td style="width:80px">Val. Compras</td>
            <td style="width:80px; text-align:right"><?=format_number_out($saldoCompraTotal)?></td>
            <td style="width:3px; border-left:1px solid">&nbsp;</td>
            <td style="width:70px">Lucro</td>
            <td style="width:80px; text-align:right"><?=format_number_out($saldoLucroBruto)?></td>
        </tr>
    </table>