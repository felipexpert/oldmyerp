<?php
	/** vari�veis que devem ser criadas foras desse arquivo para o calend�rio funcionar - o ideal seria bolar uma classe ou mesmo fun��o para isso
	 * $url 	-> endere�o de links
	 * $datas 	-> um array com as datas no formato americano
	 **/
	
	//classes html para destacar certos dias
	$dataAtualClass	= 'class="dia-atual"';
	$eventoClass	= 'class="evento"';
	
	//processando as datas recebidas pela vari�vel $datas
	$totalDatas = count($datas);
	if($totalDatas > 0 and (!isset($_GET['data']) or $_GET['data'] == 'atual')) {
		
		//pegando somente o dia
		for($y=0; $y<$totalDatas; $y++){
			$evento = explode('-', $datas[$y]);
			$diaEvento[$y] = $evento[2];
			$_SESSION['diaEvento'] = $diaEvento;
		}
		
	}
	
	//destruindo a vari�vel de sess�o respons�vel por armazenar os dias com eventos
	if($totalDatas <= 0) { unset($_SESSION['diaEvento']); }
	
	//--------------------------------------------------------------------------------------------------------
	//data par�metro para navegar entre os meses
	$data = $dataParametro = ($_SESSION['dataParametro'] == null) ? date('Y-m-d') : $_SESSION['dataParametro'];
	
	//navegando entre os meses - dataCalendario, para passar para o arquivo que possui a consulta SQL
	if(isset($_GET['ant'])) {
		$data = date('Y-m-d',strtotime( "-1 month", strtotime($dataParametro)));
	}
	elseif(isset($_GET['prox'])) {
		$data = date('Y-m-d',strtotime( "+1 month", strtotime($dataParametro)));
	}
	//--------------------------------------------------------------------------------------------------------
	
	//destruindo a vari�vel de sess�o quando solicitado
	if(isset($_GET['data']) and $_GET['data'] == 'atual') {
		
		unset($_SESSION['dataParametro']);
		$data = date('Y-m-d');
		
	}
	
	//salvando na sess�o a nova data de par�metro
	$_SESSION['dataParametro'] = $data;
	
	$data = explode('-', $data);
	$dia = $data[2];
	$mes = $data[1];
	$ano = $data[0];
	
	//para mostrar o m�s por extenso
	function meses($a) {
		switch($a) {
			case 1:  $mes = "Janeiro";   	break;
			case 2:  $mes = "Fevereiro"; 	break;
			case 3:  $mes = "Mar&ccedil;o"; break;
			case 4:  $mes = "Abril";     	break;
			case 5:  $mes = "Maio";      	break;
			case 6:  $mes = "Junho";     	break;
			case 7:  $mes = "Julho";     	break;
			case 8:  $mes = "Agosto";    	break;
			case 9:  $mes = "Setembro";  	break;
			case 10: $mes = "Outubro";   	break;
			case 11: $mes = "Novembro";  	break;
			case 12: $mes = "Dezembro";  	break;
		}
		
		return $mes;
	}
?>

<ul id="calendario-opcoes">
	<li class="res"><a href="<?=$url?>&amp;data=atual" title="Voltar para a data atual">Data atual</a></li>
	<li class="fec"><a href="#" title="Fechar">X</a></li>
</ul>

<table border="0" summary="Calend&aacute;rio" id="calendario_contas">
	<caption><a href="<?=$url?>&amp;ant=<?=$ano.'-'.$mes?>" title="M&ecirc;s anterior" id="ant">&laquo;</a> <?=meses($mes) . " " . $ano ?> <a href="<?=$url?>&amp;prox=<?=$ano.'-'.$mes?>" title="Pr&oacute;ximo m&ecirc;s" id="pro">&raquo;</a></caption>
	<thead>
	<tr>
		<th abbr="Domingo" title="Domingo" class="domingo">D</th>
		<th abbr="Segunda" title="Segunda">S</th>
		<th abbr="Ter&ccedil;a" title="Ter&ccedil;a">T</th>
		<th abbr="Quarta" title="Quarta">Q</th>
		<th abbr="Quinta" title="Quinta">Q</th>
		<th abbr="Sexta" title="Sexta">S</th>
		<th abbr="S&aacute;bado" title="S&aacute;bado">S</th>
	</tr>
	</thead>
	<tbody>
	<?php
		
		$data		  = strtotime($mes."/".$dia."/".$ano);
		$diaSemana	  = date('w', strtotime(date('n/\0\1\/Y', $data)));
		$totalDiasMes = date('t', $data);
		
		for($i=1,$d=1;$d<=$totalDiasMes;) {
			
			echo("<tr>");
			
			for($x=1;$x<=7 && $d <= $totalDiasMes;$x++,$i++) {
				
				if ($i > $diaSemana) {
					
					$destaque = '';
					
					if ($d == $dia) { $destaque = $dataAtualClass; }
					if (($x == 1) && ($d == $dia)) { $destaque = $dataAtualClass; }
					
					if(@in_array($d, ($diaEvento ? $diaEvento : $_SESSION['diaEvento']))){
						echo('<td '. $eventoClass .' title="Clique aqui para visualizar os eventos desse dia"><a href="'. $url .'&amp;data='. $ano.'-'.$mes.'-'. str_pad($d, 2, "0", STR_PAD_LEFT) .'">'.$d++.'</a></td>');
					}
					else{
						echo("<td $destaque>".$d++."</td>");
					}
					
				}
				
				else { echo("<td></td>"); }
				
			}
			
			for(;$x<=7;$x++) { echo("<td></td>"); }
			echo("</tr>");
			
		}
		
		unset($url, $datas);
		
	?>
	</tbody>
</table>