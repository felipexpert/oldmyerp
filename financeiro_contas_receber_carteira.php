<?
	require("financeiro_contas_receber_action.php");

	if(isset($_GET['msg']) && $_GET['msg'] == 'ok' && $_SERVER['REQUEST_METHOD'] != "POST")
	{
?>
		
	<div class="alert">
		<p class="ok">Registro gravado com sucesso</p>
	</div>
		
<?php
	}
	else if(isset($_GET['msg']) && $_GET['msg'] == 'erro_criar' && $_SERVER['REQUEST_METHOD'] != "POST")
	{
?>
	
	<div class="alert">
		<p class="alert">Erro ao criar a carteira</p>
	</div>
	
<?php
	}
	else if(isset($_GET['msg']) && $_GET['msg'] == 'erro_nome' && $_SERVER['REQUEST_METHOD'] != "POST")
	{
?>
	
	<div class="alert">
		<p class="alert">Erro ao criar carteira: Nome duplicado</p>
	</div>	
		
<?php	
	}
	else if(isset($_GET['msg']) && $_GET['msg'] == 'error' && $_SERVER['REQUEST_METHOD'] != "POST")
	{
?>

	<div class="alert">
		<p class="alert">Erro ao atualizar os registros</p>
	</div>	

<?php
	}

/**************************** BUSCA NO BANCO DE DADOS ***********************************/
	
	//SELECIONANDO TODAS AS CARTEIRAS
	$sSQL = "SELECT * FROM tblpedido_parcela_carteira ORDER BY fldDesc ASC";
	
	$rsCarteira = mysql_query($sSQL);
	
	echo mysql_error();
	
#########################################################################################
?>
    <form class="table_form" action="" method="post" name="form" style="clear: both;">
    	<div id="table">
            <div id="table_cabecalho">
                <ul class="table_cabecalho">
                
                	<li style="width:60px; text-align:center;">C&oacute;digo</li>
					
					<li style="width:200px; text-align:center;">Descri&ccedil;&atilde;o</li>
					
					<li style="width:634px; text-align:left; padding-left:20px;">Observa&ccedil;&atilde;o</li>
					
					<li style="width:15px"><input type="checkbox" name="chk_todos" id="chk_todos" /></li>

                </ul>
            </div>
            <div id="table_container">       
                <table id="table_general" class="table_general" summary="Lista de contas a receber">
                <tbody>
										
<?

					$id_array_carteira = array();
					$n = 0;

					$linha = "row";
					
?>
					
						<tr class="<?= $linha; ?>">
							<td style="text-align:center;"><img src="image/layout/bg_enable.gif" alt="status" title="habilitado"/></td>
							<td class="cod" style="width:43px; text-align:center;">0000</td>
							<td style="width:200px; text-align:center;">Padr&atildeo</td>
							<td style="width:620px; padding-left:10px; text-align:left;">Carteira padr&atildeo do sistema</a>
						</tr>
	
<?php
			
					while($rowCarteira = mysql_fetch_array($rsCarteira)){
						
						$linha = ($linha == "row" ? "dif-row" : "row");
						
						$id_array_carteira[$n] = $rowCarteira["fldId"];
						$n += 1;
						
						$icon = ($rowCarteira['fldStatus'] == 0 ? "bg_disable" : "bg_enable");
						$title = ($rowCarteira['fldStatus'] == 0 ? "desabilitado" : "habilitado");
						
?>

						<tr class="<?= $linha; ?>">
							<td style="text-align:center;"><img src="image/layout/<?=$icon?>.gif" alt="status" title="<?=$title?>"/></td>
							<td class="cod" style="width:43px; text-align:center;"><?=str_pad($rowCarteira['fldId'], 4, "0", STR_PAD_LEFT)?></td>
							<td style="width:200px; text-align:center;" <?=$title;?>><?=$rowCarteira['fldDesc']; ?></td>
							<td style="width:620px; padding-left:10px; text-align:left;"><?=$rowCarteira['fldObs'];?></a>
							<td style="width:20px"><a class="edit modal" href="financeiro_contas_receber_editar_carteira.php?carteira_id=<?=$rowCarteira['fldId']?>" style="margin-top:3px;" rel="550-210"></a></td>
							<td style="width:15px"><input type="checkbox" name="chk_carteira_<?php echo $rowCarteira['fldId']; ?>" class="<?= fnc_status_parcela($rowParcela2['fldId'], "tblpedido_parcela_carteira")?>" id="chk_carteira_<?php echo $rowCarteira['fldId']; ?>" title="selecionar o registro posicionado" /></td>
						</tr>
<?
				   }
				   
?>
				   
		 		</tbody>
				</table>
            </div>
			
			<input type="hidden" name="hid_array" id="hid_array" value="<?=urlencode(serialize($id_array_carteira))?>" />
			
			<div id="table_action" style="margin-top:3px">
             	<ul id="action_button">
					<li><a class="btn_novo modal" rel="550-210" href="financeiro_contas_receber_nova_carteira" title="novo">nova carteira</a></li>
					<li><input type="submit" name="btn_action" id="btn_excluir" value="excluir" title="Excluir registro(s) selecionada(s)" onclick="return confirm('Todas as contas desta carteira ser&atilde;o movidas para a carteira Padr&atilde;o, deseja continuar?')" /></li>
                    <li><input type="submit" name="btn_action" id="btn_habilitar" value="habilitar" title="Habilitar registro(s) selecionado(s)" /></li>
                    <li><input type="submit" name="btn_action" id="btn_desabilitar" value="desabilitar" title="Desabilitar registro(s) selecionado(s)" /></li>
                </ul>
            </div>
        
        </div>
       
	</form>